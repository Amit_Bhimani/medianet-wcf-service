﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Medianet.BusinessLayer.Common
{
    public enum RecordType
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember]
        MediaOutlet = 1,
        [EnumMember]
        MediaContact = 2,
        [EnumMember]
        PrnOutlet = 3,
        [EnumMember]
        PrnContact = 4,
        [EnumMember]
        OmaOutlet = 5,
        [EnumMember]
        OmaContactNoOutlet = 6,
        [EnumMember]
        OmaContactAtMediaOutlet = 7,
        [EnumMember]
        OmaContactAtPrnOutlet = 8,
        [EnumMember]
        OmaContactAtOmaOutlet = 9
    }
    public static class RecordTypeExtensions
    {
        public static bool IsOmaContact(this RecordType val)
        {
            return (val == RecordType.OmaContactAtMediaOutlet) || (val == RecordType.OmaContactAtOmaOutlet) || (val == RecordType.OmaContactAtPrnOutlet) || (val == RecordType.OmaContactNoOutlet);
        }

        public static bool IsMediaType(this RecordType val)
        {
            return (val == RecordType.MediaContact) || (val == RecordType.MediaOutlet);
        }

        public static bool IsPrnType(this RecordType val)
        {
            return (val == RecordType.PrnOutlet) || (val == RecordType.PrnContact);
        }

        public static bool IsOmaType(this RecordType val)
        {
            return (val == RecordType.OmaOutlet) || val.IsOmaContact();
        }

        public static bool IsContact(this RecordType val)
        {
            return (val == RecordType.MediaContact) || (val == RecordType.PrnContact) || (val.IsOmaContact());
        }

        public static bool IsOutlet(this RecordType val)
        {
            return (val == RecordType.MediaOutlet) || (val == RecordType.OmaOutlet) || (val == RecordType.PrnOutlet);
        }
    }
}
