﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

namespace Medianet.BusinessLayer.Common
{
    public class WordCount
    {
        public static int CountWords(string str)
        {
            int i = 0;
            int Count = 0;
            int StrLen = 0;
            bool inword = false;
            bool inspecial = false;
            bool innumber = false;
            string c = null;
            bool isdelimeter = false;

            i = 1;
            Count = 0;
            StrLen = Strings.Len(str);
            inword = false;
            inspecial = false;
            innumber = false;

            for (i = 1; i <= StrLen; i++)
            {
                c = Strings.Mid(str, i, 1);

                //Valid delimiters are <CR> <LF> <space> <comma> <semi-colon> <double quote> <colon> <single quote> and <period>.
                //In case of an e-mail adress or URL the <period> is not considered as a delimiter
                Regex regex = new Regex("[" + Strings.Chr(13) + Strings.Chr(10) + " ;\":]");

                if (regex.IsMatch(c))
                {
                    inword = false;
                    inspecial = false;
                    innumber = false;
                }
                else if (c == ",")
                {
                    //if the previous char is a number then this is probably a comma in a number.
                    if (i > 1)
                    {
                        if (Information.IsNumeric(Strings.Mid(str, i - 1, 1)))
                        {
                            innumber = true;
                        }
                    }

                    if (!innumber)
                    {
                        inword = false;
                        inspecial = false;
                    }
                }
                else if (c == ".")
                {
                    //if the previous char is a number then this is probably a decimal place in a number.
                    if (i > 1)
                    {
                        if (Information.IsNumeric(Strings.Mid(str, i - 1, 1)))
                        {
                            innumber = true;
                        }
                    }
                    //Check if this is part of an URL (should start WWW.) and
                    //do not consider "." as a delimiter until another delimiter is encountered

                    if (i > 3)
                    {
                        if (Strings.UCase(Strings.Mid(str, i - 3, 3)) == "WWW")
                        {
                            inspecial = true;
                        }
                    }

                    if (!inspecial & !innumber)
                    {
                        inword = false;
                    }
                }
                else
                {
                    isdelimeter = false;

                    if (innumber)
                    {
                        //if this char isnt a number then we werent in a number
                        if (!Information.IsNumeric(c))
                        {
                            if (!inspecial)
                            {
                                inword = false;
                            }
                        }
                        //stop checking.
                        innumber = false;
                    }

                    if (c == "'")
                    {
                        //Single quote is a special case. If it is surrounded by non-delimiters
                        //then it is being used as an apostrophe and should not be taken as a
                        //delimiter otherwise it should be taken as a delimeter.
                        isdelimeter = true;

                        if (i > 1)
                        {
                            c = Strings.Mid(str, i - 1, 1);
                            //check if not in the pattern
                            regex = new Regex("[!" + Strings.Chr(13) + Strings.Chr(10) + " ,;\":'.]");
                            if (regex.IsMatch(c))
                            {
                                if (i < StrLen)
                                {
                                    c = Strings.Mid(str, i + 1, 1);
                                    //check if not in the pattern
                                    if (regex.IsMatch(c))
                                    {
                                        isdelimeter = false;
                                    }
                                }
                            }
                        }

                        if (isdelimeter)
                        {
                            inword = false;
                            inspecial = false;
                        }

                    }
                    else if (c == "/" & !inspecial)
                    {
                        //Check if this is part of an URL (should start with HTTP:// or HTTPS:// or FTP:// or www.) and
                        //do not consider "." as a delimiter until another delimiter is encountered

                        if (i > 5)
                        {
                            if (Strings.UCase(Strings.Mid(str, i - 5, 5)) == "FTP:/")
                            {
                                inspecial = true;
                            }
                            else if (i > 6)
                            {
                                if (Strings.UCase(Strings.Mid(str, i - 6, 6)) == "HTTP:/")
                                {
                                    inspecial = true;
                                }
                                else if (i > 7)
                                {
                                    if (Strings.UCase(Strings.Mid(str, i - 7, 7)) == "HTTPS:/")
                                    {
                                        inspecial = true;
                                    }
                                }
                            }
                        }

                        //reduce the count by one as the ":" in the string would have increased it
                        if (inspecial)
                        {
                            Count = Count - 1;
                            if (Count < 0)
                                Count = 0;
                            //count cannot be negative
                        }
                    }
                    else if (c == "@" & !inspecial)
                    {
                        //This might be part of an e-mail address.
                        //Do not consider "." as a delimiter until another delimiter is encountered
                        inspecial = true;

                        //adjust the count if there were any periods before the "@" (eg. firstName.secondName@somewhere.com)
                        Count = Count - PeriodsBeforeAtInEmail(str, i);
                        if (Count < 0)
                            Count = 0;
                        //count cannot be negative
                    }

                    if (!inword & !isdelimeter)
                    {
                        Count = Count + 1;
                        inword = true;
                    }
                }
            }
            return Count;
        }

        private static int PeriodsBeforeAtInEmail(string str, int start)
        {
            int i = 0;
            int Count = 0;
            bool Done = false;
            bool nonDelimeterExists = false;
            string c = null;

            Count = 0;
            i = start - 1;
            Done = false;
            nonDelimeterExists = false;
            while (!Done & i > 0)
            {
                c = Strings.Mid(str, i, 1);
                Regex regex = new Regex("[" + Strings.Chr(13) + Strings.Chr(10) + " ,;\":']");

                if (regex.IsMatch(c))
                {
                    Done = true;
                }
                else if (c == ".")
                {
                    if (nonDelimeterExists)
                    {
                        Count = Count + 1;
                    }

                    nonDelimeterExists = false;
                }
                else
                {
                    nonDelimeterExists = true;
                }

                i = i - 1;
            }
            return Count;
        }
    }
}