﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.BusinessLayer.Model
{
    public class Pagination
    {
        public int PageSize { get; set; }

        public int PageNumber { get; set; }

        public string SortColumn { get; set; }

        public string SortDirection { get; set; }

        public Pagination()
        {
            PageSize = 10;
            PageSize = 0;
        }
    }
}
