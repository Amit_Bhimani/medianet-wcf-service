﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.DataLayer.Common
{
    public class Constants
    {
        // This is used for Linq to entity queries as we can't generate the string on the fly.
        public const string ROWSTATUS_ACTIVE = "A"; //RowStatusType.Active
        public const string ROWSTATUS_INACTIVE = "I"; //RowStatusType.Inactive
        public const string ROWSTATUS_CHANGING_PASSWORD = "P"; //RowStatusType.Deleted
        public const string ROWSTATUS_DELETED = "D"; //RowStatusType.Deleted

        public const int COUNTRY_ID_AUSTRALIA = 210;
    }
}
