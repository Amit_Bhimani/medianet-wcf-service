﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medianet.DataLayer.Common
{
    public enum DraftQueueStatus
    {
        Approved = 'A',
        Declined = 'D',
        Deleted = 'R',
        Added = 'N',
        Unchanged 
    }
}
