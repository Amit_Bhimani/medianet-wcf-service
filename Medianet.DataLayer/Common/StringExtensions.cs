﻿using System;
using System.Collections.Generic;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Text;
using Medianet.Model.Entities;

namespace Medianet.DataLayer.Common
{
    public class StringExtensions
    {
        /// <summary>
        /// Remove white space, but not new lines.
        /// Useful when parsing user input such as phone numbers, prices.
        /// eg int.Parse("1 000 000".RemoveSpaces() ...
        /// </summary>
        /// <param name="s">Input string</param>
        public static string StripExtraSpaces(string s)
        {
            return s.Replace(" ", "~# ").Replace(" ~#","").Replace("~#","").Trim();
        }
    }
}
