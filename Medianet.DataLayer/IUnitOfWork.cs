﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.DataLayer
{
    public interface IUnitOfWork : IDisposable
    {
        int Save();
    }
}
