﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class AAPCityRepository
    {
        private MedianetContext context;

        public AAPCityRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public void Add(AAPCity entity) {
            context.AAPCities.Add(entity);
            context.SaveChanges();
        }

        public AAPCity GetById(int id) {
            return (from c in context.AAPCities
                    where c.Id == id
                    select c).AsNoTracking().FirstOrDefault();
        }

        public AAPCity GetByMediaAtlasCityId(int mediaAtlasCityID)
        {
            return (from e in context.AAPCities
                    where e.MediaAtlasCityId == mediaAtlasCityID
                    select e).AsNoTracking().FirstOrDefault();
        }
    }
}
