﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class AAPEnvironmentRepository
    {
        private MedianetContext context;

        public AAPEnvironmentRepository(UnitOfWork uow) {
            context = uow.Context;
        }

        public AAPEnvironment GetByType(string type) {
            return (from e in context.AAPEnvironments
                    where e.Type == type
                    select e).AsNoTracking().FirstOrDefault();
        }

        public string GetValue(string type, string application)
        {
            return (from e in context.AAPEnvironments
                    where e.Type == type && e.Application == application
                    select e).AsNoTracking().FirstOrDefault().Value;
        }

        public List<AAPEnvironment> GetAllByTypeStartsWith(string type)
        {
            return (from e in context.AAPEnvironments
                    where e.Type.StartsWith(type)
                    select e).AsNoTracking().ToList();
        }

        public string Add(AAPEnvironment entity) {
            context.AAPEnvironments.Add(entity);
            context.SaveChanges();

            return entity.Type;
        }

        public void Update(AAPEnvironment entity) {
            List<Model.Entities.AAPEnvironment> entities = context.AAPEnvironments.Local.Where(e => e.Type == entity.Type).ToList();

            // If the items is already being tracked then update it's values. Otherwise add it as modified.
            if (entities.Count() > 0)
                context.Entry(entities[0]).CurrentValues.SetValues(entity);
            else
                context.Entry(entity).State = System.Data.EntityState.Modified;

            context.SaveChanges();
        }

        public void Delete(string type) {
            Model.Entities.AAPEnvironment entity = new Model.Entities.AAPEnvironment() { Type = type };
            List<Model.Entities.AAPEnvironment> entities = context.AAPEnvironments.Local.Where(e => e.Type == entity.Type).ToList();

            // If the items is already being tracked then delete the tracked item. Otherwise add a new entry as deleted.
            if (entities.Count() > 0)
                context.Entry(entities[0]).State = System.Data.EntityState.Deleted;
            else
                context.Entry(entity).State = System.Data.EntityState.Deleted;

            context.SaveChanges();
        }
    }
}
