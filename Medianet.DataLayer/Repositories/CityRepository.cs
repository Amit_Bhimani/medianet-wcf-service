﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace Medianet.DataLayer.Repositories
{
    public class CityRepository
    {
        private MedianetContext context;

        public CityRepository(UnitOfWork uow) {
            context = uow.Context;
        }

        public City GetById(int id) {
            var idParam = new SqlParameter("@CityId", id);
            var entity = context.Database.SqlQuery<City>("exec City_Get @CityId", idParam).FirstOrDefault();

            return entity;
        }

        public List<City> GetAll() {
            var entities = context.Database.SqlQuery<City>("exec City_GetAll");

            return entities.ToList();
        }

        public List<City> GetAllByAny(int? continentId, int? countryId, int? stateId, string name) {
            var continentParam = new SqlParameter("@ContinentId", continentId.HasValue ? continentId.Value : 0);
            var countryParam = new SqlParameter("@CountryId", countryId.HasValue ? countryId.Value : 0);
            var stateParam = new SqlParameter("@StateId", stateId.HasValue ? stateId.Value : 0);
            var nameParam = new SqlParameter("@CityName", name == null ? string.Empty : name);
            var entities = context.Database.SqlQuery<City>("exec City_Search @ContinentId, @CountryId, @StateId, @CityName", continentParam, countryParam, stateParam, nameParam);

            return entities.ToList();
        }
    }
}
