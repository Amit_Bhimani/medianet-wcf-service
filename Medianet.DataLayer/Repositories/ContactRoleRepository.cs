﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class ContactRoleRepository
    {
        private MedianetContext context;

        public ContactRoleRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public ContactRole Get(int id)
        {
            return (from e in context.ContactRoles
                    where e.RoleId == id
                    select e).AsNoTracking().First();
        }

        public List<ContactRole> GetAll()
        {
            return (from e in context.ContactRoles
                    select e).AsNoTracking().ToList();
        }
    }
}
