﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class ContinentRepository
    {
        private MedianetContext context;

        public ContinentRepository(UnitOfWork uow) {
            context = uow.Context;
        }

        public Continent GetById(int id) {
            return (from c in context.Continents
                    where c.Id == id
                    select c).AsNoTracking().FirstOrDefault();
        }

        public List<Continent> GetAll() {
            return (from c in context.Continents
                    select c).AsNoTracking().ToList();
        }
    }
}
