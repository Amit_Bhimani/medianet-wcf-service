﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace Medianet.DataLayer.Repositories
{
    public class CountryRepository
    {
        private MedianetContext context;

        public CountryRepository(UnitOfWork uow) {
            context = uow.Context;
        }

        public Country GetById(int id) {
            var idParam = new SqlParameter("@CountryId", id);
            var entity = context.Database.SqlQuery<Country>("exec Country_Get @CountryId", idParam).FirstOrDefault();

            return entity;
        }

        public List<Country> GetAll() {
            //var entities = context.Database.SqlQuery<Country>("select vc.MediaAtlasCountryId as Id, vc.CountryName as Name, vc.MediaAtlasContinentId as ContinentId, dm.Id as DataModuleId from vw_prn_country vc with (NOEXPAND) inner join mn_data_modules dm on dm.MediaAtlasContinentId = vc.MediaAtlasContinentId order by CountryName asc");
            var entities = context.Database.SqlQuery<Country>("Country_GetAll");

            return entities.ToList();
        }
    }
}
