﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class CustomerBillingCodeRepository
    {
        private MedianetContext context;

        public CustomerBillingCodeRepository(UnitOfWork uow) {
            context = uow.Context;
        }

        public int Add(CustomerBillingCode entity) {
            context.CustomerBillingCodes.Add(entity);
            context.SaveChanges();

            return entity.Id;
        }

        public void Update(CustomerBillingCode entity) {
            List<Model.Entities.CustomerBillingCode> dbEntities = context.CustomerBillingCodes.Local.Where(s => s.Id == entity.Id).ToList();

            // If the items is already being tracked then update it's values. Otherwise add it as modified.
            if (dbEntities.Count() > 0)
                context.Entry(dbEntities[0]).CurrentValues.SetValues(entity);
            else
                context.Entry(entity).State = System.Data.EntityState.Modified;

            context.SaveChanges();
        }

        public void Delete(int id) {
            var entity = new Model.Entities.CustomerBillingCode() { Id = id };
            context.Entry(entity).State = System.Data.EntityState.Deleted;
        }

        public CustomerBillingCode GetById(int id) {
            return (from c in context.CustomerBillingCodes
                    where c.Id == id
                    select c).AsNoTracking().FirstOrDefault();
        }

        public List<CustomerBillingCode> GetAllByDebtorNumber(string debtorNumber) {
            return (from c in context.CustomerBillingCodes
                    where c.DebtorNumber == debtorNumber
                    select c).AsNoTracking().ToList();
        }
    }
}
