﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class CustomerCustomPricingRepository
    {
        private MedianetContext context;

        public CustomerCustomPricingRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }
        public CustomerCustomPricing GetById(int id)
        {
            return (from u in context.CustomerCustomPricings
                    where u.Id == id
                    select u).AsNoTracking().FirstOrDefault();
        }
        public List<CustomerCustomPricing> GetCustomerCustomPricingByDebtorNumber(string debtorNumber)
        {
            return (from c in context.CustomerCustomPricings
                    where c.DebtorNumber == debtorNumber && c.StartDate <= DateTime.Now && (c.EndDate == null || (c.EndDate != null && DateTime.Now >= c.EndDate))
                    select c).AsNoTracking().ToList();
        }
    }
}
