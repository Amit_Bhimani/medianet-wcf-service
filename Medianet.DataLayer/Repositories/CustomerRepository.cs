﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class CustomerRepository
    {
        private MedianetContext context;

        public CustomerRepository(UnitOfWork uow) {
            context = uow.Context;
        }

        public void Add(Customer entity) {
            context.Customers.Add(entity);
            context.SaveChanges();
        }

        public void Update(Customer entity) {
            List<Model.Entities.Customer> dbEntities = context.Customers.Local.Where(s => s.DebtorNumber == entity.DebtorNumber).ToList();

            // If the items is already being tracked then update it's values. Otherwise add it as modified.
            if (dbEntities.Count() > 0)
                context.Entry(dbEntities[0]).CurrentValues.SetValues(entity);
            else
                context.Entry(entity).State = System.Data.EntityState.Modified;

            context.SaveChanges();
        }

        public void Delete(string debtorNumber, int userId) {
            var dbEntity = new Model.Entities.Customer() { DebtorNumber = debtorNumber };

            // Attach an empty Customer with the correct DebtorNmber to the DBContext to be tracked.
            context.Customers.Attach(dbEntity);

            // Set it to unchanged but then change the properties we want updated. This way olny those columns get updated.
            context.Entry(dbEntity).State = System.Data.EntityState.Unchanged;
            dbEntity.RowStatus = Common.Constants.ROWSTATUS_DELETED;
            dbEntity.LastModifiedDate = DateTime.Now;
            dbEntity.LastModifiedByUserId = userId;

            context.SaveChanges();
        }

        public Customer GetByDebtorNumber(string debtorNumber) {
            return (from c in context.Customers.Include("Timezone").Include("SalesRegions")
                    where c.DebtorNumber == debtorNumber
                    && c.RowStatus != Common.Constants.ROWSTATUS_DELETED
                    select c).AsNoTracking().FirstOrDefault();
        }
       
        public Customer GetByName(string name) {
            return (from c in context.Customers.Include("Timezone").Include("SalesRegions")
                    where c.Name == name
                    && c.RowStatus == Common.Constants.ROWSTATUS_ACTIVE
                    select c).AsNoTracking().FirstOrDefault();
        }

        public List<Customer> GetAll() {
            return (from c in context.Customers.Include("Timezone").Include("SalesRegions")
                    where c.RowStatus != Common.Constants.ROWSTATUS_ACTIVE
                    select c).AsNoTracking().ToList();
        }

        public void AcknowledgeTermsRead(string debtorNumber, int userId, string emailSentTo, string system)
        {
            var dbEntity1 = context.Customers.Local.FirstOrDefault(c => c.DebtorNumber == debtorNumber);
            if (dbEntity1 == null)
            {
                dbEntity1 = new Model.Entities.Customer()
                {
                    DebtorNumber = debtorNumber
                };

                context.Customers.Attach(dbEntity1);
            }
            var dbEntity2 = new Model.Entities.CustomerTermsRead()
            {
                DebtorNumber = debtorNumber,
                ReadByUserId = userId,
                ReadDate = DateTime.Now,
                EmailSentTo = emailSentTo,
                System = system
            };

            if (system == "P") // Contacts
            {
                dbEntity1.HasViewedTermsAndConditionsP = true;
                context.Entry(dbEntity1).Property(x => x.HasViewedTermsAndConditionsP).IsModified = true;
            }
            else if (system == "C") // MessageConnect
            {
                dbEntity1.HasViewedTermsAndConditionsC = true;
                context.Entry(dbEntity1).Property(x => x.HasViewedTermsAndConditionsC).IsModified = true;
            }
            else // Distribution
            {
                dbEntity1.HasViewedTermsAndConditionsM = true;
                context.Entry(dbEntity1).Property(x => x.HasViewedTermsAndConditionsM).IsModified = true;
            }
            context.CustomerTermsRead.Add(dbEntity2);

            context.SaveChanges();
        }
    }
}
