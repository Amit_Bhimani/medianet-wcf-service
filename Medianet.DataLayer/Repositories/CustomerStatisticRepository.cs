﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class CustomerStatisticRepository
    {
        private MedianetContext context;

        public CustomerStatisticRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public CustomerStatistic GetByDebtorNumber(string debtorNumber)
        {
            return (from c in context.CustomerStatistics
                    where c.DebtorNumber == debtorNumber
                    select c).AsNoTracking().FirstOrDefault();
        }
    }
}
