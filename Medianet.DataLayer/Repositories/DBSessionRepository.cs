﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class DBSessionRepository
    {
        private MedianetContext context;

        public DBSessionRepository(UnitOfWork uow) {
            context = uow.Context;
        }

        public DBSession Add(DBSession entity) {
            context.DBSessions.Add(entity);
            context.SaveChanges();

            return GetById(entity.Id);
        }

        public void Update(DBSession entity) {
            List<Model.Entities.DBSession> entities = context.DBSessions.Local.Where(s => s.Id == entity.Id).ToList();

            // If the items is already being tracked then update it's values. Otherwise add it as modified.
            if (entities.Count() > 0)
                context.Entry(entities[0]).CurrentValues.SetValues(entity);
            else
                context.Entry(entity).State = System.Data.EntityState.Modified;

            context.SaveChanges();
        }

        public void Delete(int id) {
            List<Model.Entities.DBSession> entities;

            // Get the DBSession details.
            entities = (from s in context.DBSessions
                        where s.Id == id
                        select s).ToList();

            // Set the status of the DBSession to Inactive. There should only be 1 of these, but loop through just in case.
            foreach (Model.Entities.DBSession dbs in entities) {
                dbs.RowStatus = Common.Constants.ROWSTATUS_INACTIVE;
            }

            context.SaveChanges();
        }

        public DBSession GetById(int id)
        {
            return (from s in context.DBSessions.Include("User")
                        .Include("Customer").Include("Customer.Timezone").Include("Customer.SalesRegions")
                    where s.Id == id
                    && s.User.RowStatus == Common.Constants.ROWSTATUS_ACTIVE
                    && s.Customer.RowStatus == Common.Constants.ROWSTATUS_ACTIVE
                    select s).AsNoTracking().FirstOrDefault();
        }

        public DBSession GetActiveById(int id)
        {
            return (from s in context.DBSessions.Include("User")
                        .Include("Customer").Include("Customer.Timezone").Include("Customer.SalesRegions")
                    where s.Id == id
                    && s.RowStatus == Common.Constants.ROWSTATUS_ACTIVE
                    && s.User.RowStatus == Common.Constants.ROWSTATUS_ACTIVE
                    && s.Customer.RowStatus == Common.Constants.ROWSTATUS_ACTIVE
                    select s).AsNoTracking().FirstOrDefault();
        }

        public List<DBSession> GetAllByUserSystem(int userId, string system)
        {
            return (from s in context.DBSessions.Include("User")
                        .Include("Customer").Include("Customer.Timezone").Include("Customer.SalesRegions")
                    where s.UserId == userId
                    && s.System == system
                    && s.User.RowStatus == Common.Constants.ROWSTATUS_ACTIVE
                    && s.Customer.RowStatus == Common.Constants.ROWSTATUS_ACTIVE
                    select s).AsNoTracking().ToList();
        }

        public List<DBSession> GetAllActiveByCustomerSystem(string debtorNumber, string system)
        {
            return (from s in context.DBSessions.Include("User")
                       .Include("Customer").Include("Customer.Timezone").Include("Customer.SalesRegions")
                    where s.DebtorNumber == debtorNumber
                    && s.System == system
                    && s.RowStatus == Common.Constants.ROWSTATUS_ACTIVE
                    && s.User.RowStatus == Common.Constants.ROWSTATUS_ACTIVE
                    && s.Customer.RowStatus == Common.Constants.ROWSTATUS_ACTIVE
                    select s).AsNoTracking().ToList();
        }
    }
}
