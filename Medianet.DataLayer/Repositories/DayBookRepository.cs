﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class DayBookRepository
    {
        private MedianetContext context;

        public DayBookRepository(UnitOfWork uow) {
            context = uow.Context;
        }

        public DayBook GetLatestByType(int type)
        {
            DateTime now = DateTime.Now;

            return (from d in context.Daybooks
                    join t in context.DayBookTypes on d.DaybookTypeID equals t.Id
                    where t.Id == type
                    && d.Date < now
                    select d).OrderByDescending(d => d.LastModified).AsNoTracking().FirstOrDefault();
        }
    }
}
