﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class DocumentRepository
    {
        private MedianetContext context;

        public DocumentRepository(UnitOfWork uow) {
            context = uow.Context;
        }

        public int Add(Document entity) {
            context.Documents.Add(entity);
            context.SaveChanges();

            return entity.Id;
        }

        //public void Update(Document entity) {
        //    context.Entry(entity).State = System.Data.EntityState.Modified;
        //}

        //public void Delete(int id) {
        //    Document entity = new Model.Entities.Document() { Id = id };
        //    context.Entry(entity).State = System.Data.EntityState.Deleted;
        //}

        public void DeleteAllByReleaseId(int releaseId) {
            List<Model.Entities.Document> entities = null;

            entities = (from d in context.Documents
                        where d.ReleaseId == releaseId
                        select d).ToList();

            foreach (Model.Entities.Document doc in entities) {
                context.Entry(doc).State = System.Data.EntityState.Deleted;
            }

            context.SaveChanges();
        }

        public Document GetById(int id) {
            return (from d in context.Documents
                        where d.Id == id
                        select d).AsNoTracking().FirstOrDefault();
        }

        public List<Document> GetAllByReleaseId(int releaseId) {
            return (from d in context.Documents
                    where d.ReleaseId == releaseId
                    select d).AsNoTracking()
                    .OrderBy(x => x.SequenceNumber).ToList();
        }

        public List<Document> GetAllByReleaseIdWithThumbnails(int releaseId) {
            return (from d in context.Documents.Include("Thumbnails")
                    where d.ReleaseId == releaseId
                    select d).AsNoTracking()
                    .OrderBy(x => x.SequenceNumber).ToList();
        }

        public Document GetPrimaryByReleaseId(int releaseId)
        {
            return (from d in context.Documents
                    where d.ReleaseId == releaseId
                    && d.CanDistributeViaFax
                    && d.CanDistributeViaWeb
                    && !d.IsDerived
                    select d).AsNoTracking().FirstOrDefault();
        }

        public Document GetByReleaseIdSequenceNumber(int releaseId, int sequenceNumber)
        {
            return (from d in context.Documents
                    where d.ReleaseId == releaseId
                    && d.SequenceNumber == sequenceNumber
                    select d).AsNoTracking().FirstOrDefault();
        }
    }
}
