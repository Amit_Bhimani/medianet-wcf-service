﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.DataLayer.Repositories
{
    public interface IRepository<T> //: IDisposable
    {
        int Add(T entity);
        void Update(T entity);
        void Delete(int id);
        T GetById(int id);
        List<T> GetAll();
    }
}
