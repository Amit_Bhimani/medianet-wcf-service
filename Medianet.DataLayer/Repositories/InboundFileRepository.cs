﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class InboundFileRepository
    {
        private MedianetContext context;

        public InboundFileRepository(UnitOfWork uow) {
            context = uow.Context;
        }

        public int Add(InboundFile entity) {
            context.InboundFiles.Add(entity);
            context.SaveChanges();

            return entity.Id;
        }

        public void Update(InboundFile entity) {
            context.Entry(entity).State = System.Data.EntityState.Modified;
            context.SaveChanges();
        }

        public void Delete(int id) {
            InboundFile entity = new Model.Entities.InboundFile() { Id = id };
            context.Entry(entity).State = System.Data.EntityState.Deleted;
            context.SaveChanges();
        }

        public InboundFile GetById(int id) {
            return (from i in context.InboundFiles
                    where i.Id == id
                    select i).AsNoTracking().FirstOrDefault();
        }

        public List<InboundFile> GetAllByDebtorNumberType(string debtorNumber, string type, string status, int sortOrder, int startPos, int pageSize) {
            // Get all the imbound items owned by this DebtorNumber.
            var dbQuery = (from i in context.InboundFiles
                           join u in context.Users on i.OwnerUserId equals u.Id
                           where u.DebtorNumber == debtorNumber
                           && i.Type == type
                           && i.Status == status
                           select i);

            switch (sortOrder) {
                case 1: //LastModifiedDate
                    dbQuery = dbQuery.OrderByDescending(x => x.LastModifiedDate);
                    break;
                case 2: //Caller
                    dbQuery = dbQuery.OrderBy(x => x.Caller).ThenByDescending(x => x.Id);
                    break;
                case 3: //DistributionType
                    dbQuery = dbQuery.OrderBy(x => x.DistributionType).ThenByDescending(x => x.Id);
                    break;
                case 4: //InboundNumber
                    dbQuery = dbQuery.OrderBy(x => x.InboundNumber).ThenByDescending(x => x.Id);
                    break;
                default:
                    dbQuery = dbQuery.OrderByDescending(x => x.Id);
                    break;
            }

            if (startPos > 0)
                dbQuery = dbQuery.Skip(startPos);

            return dbQuery.Take(pageSize).AsNoTracking().ToList();
        }

        public int GetCountByDebtorNumberType(string debtorNumber, string type, string status) {
            // Get all the imbound items owned by this DebtorNumber.
            return (from i in context.InboundFiles
                    join u in context.Users on i.OwnerUserId equals u.Id
                    where u.DebtorNumber == debtorNumber
                    && i.Type == type
                    && i.Status == status
                    select i.Id).Count();
        }

        public void AddUsageLog(int inboundFileId, int releaseId, int userId) {
            var dbEntity = new Model.Entities.InboundFileHistory() {
                InboundFileId = inboundFileId,
                ReleaseId = releaseId,
                Comment = string.Format("Web Job {0} included Message {1}. Created by user {2}", releaseId, inboundFileId, userId),
                CreatedDate = DateTime.Now
            };

            context.InboundFileHistories.Add(dbEntity);
            context.SaveChanges();
        }
    }
}
