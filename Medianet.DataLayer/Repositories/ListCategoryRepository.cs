﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class ListCategoryRepository
    {
        private MedianetContext context;

        public ListCategoryRepository(UnitOfWork uow) {
            context = uow.Context;
        }

        public ListCategory GetById(int id) {
            return (from lc in context.ListCategories
                    where lc.Id == id
                    && lc.IsHidden != "T"
                    select lc).AsNoTracking().FirstOrDefault();
        }

        public List<ListCategory> GetAll() {
            return (from lc in context.ListCategories
                    where lc.IsHidden != "T"
                    select lc).AsNoTracking().ToList();
        }
    }
}
