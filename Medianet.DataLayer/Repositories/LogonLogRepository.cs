﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class LogonLogRepository
    {
        private MedianetContext context;

        public LogonLogRepository(UnitOfWork uow) {
            context = uow.Context;
        }

        public void Add(LogonLog entity) {
            context.LogonLogs.Add(entity);
            context.SaveChanges();
        }
    }
}
