﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class MailoutRecipientRepository
    {
        private MedianetContext context;

        public MailoutRecipientRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public void Add(MailoutRecipient entity) {
            context.MailoutRecipients.Add(entity);
            context.SaveChanges();
        }

        public MailoutRecipient GetById(int id) {
            return (from c in context.MailoutRecipients
                    where c.Id == id
                    select c).AsNoTracking().FirstOrDefault();
        }
    }
}
