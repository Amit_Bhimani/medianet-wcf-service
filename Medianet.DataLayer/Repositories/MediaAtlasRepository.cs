﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class MediaAtlasRepository
    {
        private MedianetContext context;

        public MediaAtlasRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public MediaAtlas GetByPrefix(string prefix)
        {
            return (from e in context.MediaAtlas
                    where e.Prefix == prefix
                    select e).AsNoTracking().FirstOrDefault();
        }

        public void Update(MediaAtlas entity)
        {
            context.Entry(entity).State = System.Data.EntityState.Modified;
        }

        private string GenerateNewID(string prefix)
        {
            MediaAtlas ma = (from e in context.MediaAtlas where e.Prefix == prefix select e).FirstOrDefault();
            if (ma == null || ma.Id <= 0)
            {
                throw new Exception("Unable to generate new ID. Failed to fetch ID seed for prefix '" + prefix + "'.");
            }
            else
            {
                ma.Id++;
            }

            Update(ma);

            return ma.Prefix.Trim() + ma.Id.ToString();
        }

        public string GetNewContactID() {
            return GenerateNewID("AAPC");
        }

        public string GetNewOutletID()
        {
            return GenerateNewID("AAPO");
        }
    }
}
