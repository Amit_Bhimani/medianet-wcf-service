﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using Medianet.Model;
using Medianet.Model.Entities;
using System.Data;

namespace Medianet.DataLayer.Repositories
{
    public class MediaContactDraftRepository
    {
        private MedianetContext context;

        public MediaContactDraftRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public MediaContactDraft GetById(int id)
        {
            return (from e in context.MediaContactDrafts
                    where e.Id == id
                    select e).AsNoTracking().FirstOrDefault();
        }

        public MediaContactDraft GetByQueueId(int queueId)
        {
            return (from e in context.MediaContactDrafts.Include("DraftQueue")
                    where e.QueueId == queueId
                    select e).AsNoTracking().FirstOrDefault();
        }

        public MediaContactDraft GetByContactId(string contactId)
        {
            string status = ((char)Common.DraftQueueStatus.Added).ToString();

            return (from c in context.MediaContactDrafts.Include("DraftQueue")
                    where c.ContactId == contactId && c.DraftQueue.Status == status && c.DraftQueue.OutletId == null
                    orderby c.DraftQueue.Id descending
                    select c).AsNoTracking().FirstOrDefault();
        }

        public void Add(MediaContactDraft entity)
        {
            entity.DraftQueue = new MediaDraftQueue
            {
                ContactId = entity.ContactId,
                CreatedDate = DateTime.Now,
                Status = ((char)Common.DraftQueueStatus.Added).ToString(),
                OutletId = null,
                JournalistComment = entity.DraftQueue?.JournalistComment,
                LastModifiedDate = DateTime.Now
            };

            context.MediaContactDrafts.Add(entity);
        }

        public void Update(MediaContactDraft entity, bool isDeleted)
        {
            context.Entry(entity).State = isDeleted ? EntityState.Deleted : EntityState.Modified;
        }
    }
}
