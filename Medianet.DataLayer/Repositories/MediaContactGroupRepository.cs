﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class MediaContactGroupRepository
    {
        private MedianetContext context;

        private const byte GROUP_TYPE_SAVEDSEARCH = 0;
        private const byte GROUP_TYPE_LIST = 1;
        private const byte GROUP_TYPE_TASK = 2;

        public MediaContactGroupRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public MediaContactGroup GetById(int id)
        {
            var query = (from g in context.MediaContactGroups.Include("CreatedByUser")
                    where g.Id == id
                          && g.IsActive == 1
                    select g);
            var queryWithInclude = query.Include("CreatedByUser"); // Seperate the Include. Otherwise it ignores it

            return queryWithInclude.AsNoTracking().FirstOrDefault();
        }

        public List<MediaContactGroup> GetAll(int userId)
        {
            var query = (from g in context.MediaContactGroups.Include("CreatedByUser")
                    join u1 in context.Users on g.CreatedByUserId equals u1.Id
                    join u2 in context.Users on u1.DebtorNumber equals u2.DebtorNumber
                    where u2.Id == userId
                          && g.IsActive == 1
                    select g);
            var queryWithInclude = query.Include("CreatedByUser"); // Seperate the Include. Otherwise it ignores it

            return queryWithInclude.AsNoTracking().ToList();
        }

        public List<MediaContactGroup> GetAllByType(byte type, int userId)
        {
            List<int> groupIds;

            // Display only groups with saved searches/lists/tasks in them
            switch(type) {
                case GROUP_TYPE_SAVEDSEARCH:
                    groupIds = (from s in context.MediaContactSavedSearches
                                join u1 in context.Users on s.OwnerUserId equals u1.Id
                                join u2 in context.Users on u1.DebtorNumber equals u2.DebtorNumber
                                where u2.Id == userId
                                      && s.IsActive == 1
                                      && (s.OwnerUserId == userId || s.IsPrivate == false)
                                select s.GroupId).Distinct().ToList();
                    break;

                case GROUP_TYPE_LIST:
                    groupIds = (from l in context.MediaContactLists
                                join u1 in context.Users on l.OwnerUserId equals u1.Id
                                join u2 in context.Users on u1.DebtorNumber equals u2.DebtorNumber
                                where u2.Id == userId
                                      && l.IsActive == true
                                      && (l.OwnerUserId == userId || l.IsPrivate == false)
                                select l.GroupId).Distinct().ToList();
                    break;

                case GROUP_TYPE_TASK:
                    groupIds = (from s in context.MediaContactTasks
                                join u1 in context.Users on s.OwnerUserId equals u1.Id
                                join u2 in context.Users on u1.DebtorNumber equals u2.DebtorNumber
                                where u2.Id == userId
                                      && s.IsActive == true
                                      &&
                                      (s.OwnerUserId == userId || s.CreatedByUserId == userId || s.IsPrivate == false)
                                select s.GroupId).Distinct().ToList();
                    break;

                default:
                    throw new Exception("Unknown type " + type.ToString() + ".");
            }

            var query = (from g in context.MediaContactGroups
                         where g.GroupType == type
                               && g.IsActive == 1
                               && groupIds.Contains(g.Id)
                         select g);
            var queryWithInclude = query.Include("CreatedByUser"); // Seperate the Include. Otherwise it ignores it

            return queryWithInclude.AsNoTracking().ToList();
        }

        public int Add(MediaContactGroup entity)
        {
            context.MediaContactGroups.Add(entity);
            context.SaveChanges();

            return entity.Id;
        }

        public void Update(MediaContactGroup entity)
        {
            context.Entry(entity).State = System.Data.EntityState.Modified;
            context.SaveChanges();
        }
    }
}
