﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer.Common;

namespace Medianet.DataLayer.Repositories
{
    public class MediaContactJobHistoryRepository
    {
        private MedianetContext context;

        public MediaContactJobHistoryRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public List<MediaContactJobHistory> GetByContactId(string contactId)
        {
            return (from e in context.MediaContactJobHistories
                    where e.ContactId == contactId
                    select e).AsNoTracking().ToList();
        }

        public MediaContactJobHistory GetById(int id)
        {
            return (from e in context.MediaContactJobHistories
                    where e.Id == id
                    select e).AsNoTracking().FirstOrDefault();
        }

        public int Add(MediaContactJobHistory entity)
        {
            context.MediaContactJobHistories.Add(entity);

            return entity.Id;
        }

        public void Delete(int id)
        {
            MediaContactJobHistory dbEntity = new MediaContactJobHistory { Id = id };

            context.Entry(dbEntity).State = System.Data.EntityState.Deleted;
        }

        public void UpdateBulk(ICollection<MediaContactJobHistory> entities, string contactId, int userId)
        {
            var originalHistories = context.MediaContactJobHistories.Where(h => h.ContactId.Equals(contactId)).ToList();
            var copyOfHistories = originalHistories.ToList();

            // First loop to see if we need to delete anything
            foreach (var item in copyOfHistories)
            {
                MediaContactJobHistory his = null;

                //Is the Job History still there?
                if (entities != null)
                    his = entities.SingleOrDefault(i => i.Id == item.Id);

                if (his == null)
                    context.Entry(item).State = System.Data.EntityState.Deleted;
                // No. It looks like that job history was deleted.
                //originalHistories.Remove(item);
            }

            // Second loop to determine if we have anything we need to Add.
            if (entities != null)
            {
                foreach (var item in entities)
                {
                    var his = originalHistories.FirstOrDefault(s => s.Id == item.Id);

                    //Check if this Subject exists already, if not, add it.
                    if (his == null)
                    {
                        // The job history doesnt exist in the database. Lets add it.
                        item.ContactId = contactId;
                        item.CreatedDate = DateTime.Now;
                        item.CreatedBy = userId;
                        item.LastModifiedDate = DateTime.Now;
                        item.LastModifiedBy = item.CreatedBy;
                        context.MediaContactJobHistories.Add(item);
                    }
                    else
                    {
                        if (his.ContactId != contactId ||
                            his.JobTitle != item.JobTitle ||
                            his.StartYear != item.StartYear ||
                            his.StartMonth != item.StartMonth ||
                            his.EndYear != item.EndYear ||
                            his.EndMonth != item.EndMonth ||
                            his.OutletName != item.OutletName ||
                            his.OutletId != item.OutletId)
                        {
                            his.ContactId = contactId;
                            his.JobTitle = item.JobTitle;
                            his.StartYear = item.StartYear;
                            his.StartMonth = item.StartMonth;
                            his.EndYear = item.EndYear;
                            his.EndMonth = item.EndMonth;
                            his.OutletName = item.OutletName;
                            his.OutletId = item.OutletId;
                            his.LastModifiedDate = DateTime.Now;
                            his.LastModifiedBy = userId;

                            context.Entry(his).State = System.Data.EntityState.Modified;
                        }
                    }
                }
            }
        }
    }
}
