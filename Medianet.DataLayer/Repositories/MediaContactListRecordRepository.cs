﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Medianet.Model;
using Medianet.Model.Entities;

namespace Medianet.DataLayer.Repositories
{
    public class MediaContactListRecordRepository
    {

        private MedianetContext context;

        public MediaContactListRecordRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public List<MediaContactListRecordDetails> GetByListIdAndListSet(int listId, int listSet, int userid, int maxRowsCount)
        {
            var listIdParam = new SqlParameter("@listid", listId);
            var listSetParam = new SqlParameter("@ListSetId", listSet);
            var useridParam = new SqlParameter("@User_ID", userid);
            var maxRowsCountParam = new SqlParameter("@MaxRowcount", maxRowsCount);
            var entities = context.Database.SqlQuery<MediaContactListRecordDetails>("exec e_GetListRecordsAllWithListset @listid, @ListSetId, @User_ID, @MaxRowcount", listIdParam, listSetParam, useridParam, maxRowsCountParam);

            return entities.ToList();
        }

        public int AddAndDelete(int listId, int listsetId, string idsToAdd, string recordIdsToDelete, bool createNewVersion, int userId)
        {
            var listIdParam = new SqlParameter("@ListId", listId);
            var listsetIdParam = new SqlParameter("@ListsetId", listsetId);
            var idsToAddParam = new SqlParameter("@IdsToAdd", idsToAdd);
            var recordIdsToDeleteParam = new SqlParameter("@recordIdsToDelete", recordIdsToDelete);
            var createNewVersionParam = new SqlParameter("@CreateNewVersion", createNewVersion);
            var userIdParam = new SqlParameter("@UserId", userId);

            var entities = context.Database.SqlQuery<int>("exec ListMaster_Update @ListId, @ListsetId, @IdsToAdd, @RecordIdsToDelete, @CreateNewVersion, @UserId", listIdParam, listsetIdParam, idsToAddParam, recordIdsToDeleteParam, createNewVersionParam, userIdParam);
            return entities.FirstOrDefault();
        }
    }
}
