﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class MediaContactListRepository
    {
        private MedianetContext context;

        public MediaContactListRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public MediaContactList GetById(int id)
        {
            var query = (from g in context.MediaContactLists
                         where g.Id == id
                               && g.IsActive == true
                         select g);
            var queryWithInclude = query.Include("Group").Include("OwnerUser"); // Seperate the Include. Otherwise it ignores it

            return queryWithInclude.AsNoTracking().FirstOrDefault();
        }

        public List<MediaContactList> GetAll(int userId)
        {
            var query = GetAllQuery(userId);

            return query.AsNoTracking().ToList();
        }

        public List<MediaContactList> GetRecent(int userId, int takeCount)
        {
            var query = GetAllQuery(userId);

            return query.OrderByDescending(q => q.LastModifiedDate).Take(takeCount).AsNoTracking().ToList();
        }

        private IQueryable<MediaContactList> GetAllQuery(int userId)
        {
            var query = (from g in context.MediaContactLists
                         join u1 in context.Users on g.OwnerUserId equals u1.Id
                         join u2 in context.Users on u1.DebtorNumber equals u2.DebtorNumber
                         where u2.Id == userId
                               && g.IsActive == true
                               && (g.OwnerUserId == userId || g.IsPrivate == false)
                         select g);

            return query.Include("Group").Include("OwnerUser"); // Seperate the Include. Otherwise it ignores it
        }

        public int Add(MediaContactList entity)
        {
            context.MediaContactLists.Add(entity);
            context.SaveChanges();

            return entity.Id;
        }

        public void Update(MediaContactList entity)
        {
            context.Entry(entity).State = System.Data.EntityState.Modified;
            context.SaveChanges();
        }
    }
}
