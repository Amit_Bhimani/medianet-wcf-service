﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class MediaContactListVersionRepository
    {
        private MedianetContext context;

        public MediaContactListVersionRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public List<MediaContactListVersion> GetAllByListId(int listId)
        {
            var query = (from lv in context.MediaContactListVersions
                         where lv.ListId == listId
                         select lv).OrderByDescending(lv => lv.Version).AsNoTracking().ToList();
            
            return query.ToList();
        }

        public void Restore(int listId, int listSetId, int userId)
        {
            context.Database.ExecuteSqlCommand(string.Format("exec e_ListRestore {0},{1},{2}", listSetId, listId, userId));
        }
    }
}
