﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class MediaContactLivingListRepository
    {
        private MedianetContext context;

        public MediaContactLivingListRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public List<MediaContactLivingListContactReplacement> GetReplacements(int userId = 0, int listId = 0, int maxRows = 6)
        {
            var userIdParam = new SqlParameter("@UserId", userId);
            var listIdParam = new SqlParameter("@ListId", listId);
            var maxRowsParam = new SqlParameter("@MaxRows", maxRows);

            var entities = context.Database.SqlQuery<MediaContactLivingListContactReplacement>("exec LivingListGet @UserId,@ListId,@MaxRows", userIdParam, listIdParam, maxRowsParam);
            return entities.ToList();
        }

        public int AcceptReplacement(int recordId, int userId,int listSetId = 0)
        {
            Model.Entities.MediaContactListRecord dbEntity = null;
            var primaryContactFound = false;
            string idsToAdd = string.Empty;

            dbEntity = (from t in context.MediaContactListRecords
                          where t.Id == recordId
                          select t).FirstOrDefault();

            if (dbEntity != null)
            {
                //AAP OUTLET?
	            //check if it's aap/oma contact with aap outlet AND the primary contact exists for this aap outlet
                if (!string.IsNullOrWhiteSpace(dbEntity.MediaContactId) || //aap contact
                    (dbEntity.OmaContactId != null && dbEntity.MediaOutletId != null)) //oma contact + aap outlet
                {
                    var primaryContactId =
                        context.MediaEmployees.Where(
                            e =>
                            e.OutletId == dbEntity.MediaOutletId && e.IsPrimaryNewsContact &&
                            e.RowStatus == Common.Constants.ROWSTATUS_ACTIVE)
                               .Select(e => e.ContactId).FirstOrDefault();

                    if (primaryContactId != null)
                    {
                        //we have a primary contact for this aap outlet
                        idsToAdd = string.Format("{0}#{1}####", dbEntity.MediaOutletId, primaryContactId);

                        primaryContactFound = true;
                    }
                }
                
                //PRN OUTLET?
                //we haven't found our primary contact yet, it may be Prn Contact
                if (!primaryContactFound && (dbEntity.PrnContactId != null || 
                    (dbEntity.OmaContactId != null && dbEntity.PrnOutletId != null)))
                {
                    var primaryContactId =
                        context.MediaPrnEmployees.Where(e => e.OutletId == dbEntity.PrnOutletId && e.IsPrimaryNewsContact == true)
                               .Select(e => e.ContactId).FirstOrDefault();

                    if (primaryContactId != null && primaryContactId != 0)
                    {
                        idsToAdd = string.Format("####{0}#{1}", dbEntity.PrnOutletId, primaryContactId);
                        primaryContactFound = true;
                    }
                }

               return AddOrDelete(dbEntity.ListId, listSetId, idsToAdd, recordId.ToString(), userId);
            }
            return 0;
        }

        public int RejectReplacement(int recordId, int userId, int listSetId = 0)
        {
            Model.Entities.MediaContactListRecord dbEntity = null;

            // Get the ListRecord details.
            dbEntity = (from t in context.MediaContactListRecords
                          where t.Id == recordId
                        select t).FirstOrDefault();

            if (dbEntity != null)
            {
               return AddOrDelete(dbEntity.ListId, listSetId, "", recordId.ToString(), userId);
            }

            return 0;
        }

        public List<MediaContactLivingListContacts> GetOutletContactsByRecordId(int recordId, int maxRow = 500)
        {
            Model.Entities.MediaContactListRecord dbEntity = null;

            dbEntity = (from t in context.MediaContactListRecords
                          where t.Id == recordId
                          select t).FirstOrDefault();

            if (dbEntity != null)
            {
                List<MediaContactLivingListContacts> livingList = null;

                if (dbEntity.MediaOutletId != null)
                {
                    //Fetch all Contacts for the AAP Outlet.
                    var query = (from e in context.MediaEmployees
                                      join c in context.MediaContacts on e.ContactId equals c.Id
                                      where e.OutletId == dbEntity.MediaOutletId &&
                                            (e.ContactId != dbEntity.MediaContactId || dbEntity.MediaContactId == null) &&
                                            e.RowStatus == Common.Constants.ROWSTATUS_ACTIVE &&
                                            c.RowStatus == Common.Constants.ROWSTATUS_ACTIVE
                                 select new { ContactId = c.Id, c.FirstName, c.MiddleName, c.LastName, e.JobTitle }).Take(maxRow).ToList();
                    if (query.Any())
                    {
                        livingList = new List<MediaContactLivingListContacts>();

                        foreach (var item in query)
                        {
                            var lv = new MediaContactLivingListContacts();
                            lv.ContactId = string.IsNullOrEmpty(item.ContactId) ? "" : item.ContactId;
                            lv.ContactName = Common.StringExtensions.StripExtraSpaces(string.Format("{0} {1} {2}",
                                         string.IsNullOrEmpty(item.FirstName) ? string.Empty : item.FirstName,
                                         string.IsNullOrEmpty(item.MiddleName) ? string.Empty : item.MiddleName,
                                         string.IsNullOrEmpty(item.LastName) ? string.Empty : item.LastName));
                            lv.JobTitle = string.IsNullOrEmpty(item.JobTitle) ? string.Empty : item.JobTitle;
                            livingList.Add(lv);
                        }
                        return livingList.OrderBy(c => c.ContactName).ToList();
                    }     
                }
                else if (dbEntity.PrnOutletId != null && dbEntity.PrnOutletId != 0)
                {
                    //Fetch all Contacts for the PRN Outlet.
                    var query = (from c in context.MediaPrnContacts
                                join e in context.MediaPrnEmployees on c.ContactId equals e.ContactId
                                where e.OutletId == dbEntity.PrnOutletId &&
                                (e.ContactId != dbEntity.PrnContactId || dbEntity.PrnContactId == null)
                                select new {c.ContactId, c.FirstName, c.MiddleName, c.LastName, e.JobTitle}).Take(maxRow).ToList();
 
                    if (query.Any())
                    {
                        livingList = new List<MediaContactLivingListContacts>();

                        foreach (var item in query)
                        {
                            var lv = new MediaContactLivingListContacts();
                            lv.ContactId = item.ContactId.ToString();
                            lv.ContactName = Common.StringExtensions.StripExtraSpaces(string.Format("{0} {1} {2}",
                                         string.IsNullOrEmpty(item.FirstName) ? string.Empty : item.FirstName,
                                         string.IsNullOrEmpty(item.MiddleName) ? string.Empty : item.MiddleName,
                                         string.IsNullOrEmpty(item.LastName) ? string.Empty : item.LastName));
                            lv.JobTitle = string.IsNullOrEmpty(item.JobTitle) ? string.Empty : item.JobTitle;
                            livingList.Add(lv);
                        }

                        return livingList.OrderBy(c => c.ContactName).ToList();
                    }                  
                }
                else if (dbEntity.OmaOutletId != null && dbEntity.OmaOutletId != 0)
                {
                    var query = (from oc in context.MediaOmaContacts
                                where oc.OmaOutletId == dbEntity.OmaOutletId &&
                                oc.ContactId != dbEntity.OmaContactId
                                && oc.RowStatus == 1
                                select new { oc.ContactId, oc.FirstName, oc.MiddleName, oc.LastName, oc.JobTitle }).Take(maxRow).ToList();

                    if (query.Any())
                    {
                        livingList = new List<MediaContactLivingListContacts>();

                        foreach (var item in query)
                        {
                            var lv = new MediaContactLivingListContacts();
                            lv.ContactId = item.ContactId.ToString();
                            lv.ContactName = Common.StringExtensions.StripExtraSpaces(string.Format("{0} {1} {2}",
                                         string.IsNullOrEmpty(item.FirstName) ? string.Empty : item.FirstName,
                                         string.IsNullOrEmpty(item.MiddleName) ? string.Empty : item.MiddleName,
                                         string.IsNullOrEmpty(item.LastName) ? string.Empty : item.LastName));
                            lv.JobTitle = string.IsNullOrEmpty(item.JobTitle) ? string.Empty : item.JobTitle;
                            livingList.Add(lv);
                        }

                        return livingList.OrderBy(c => c.ContactName).ToList();
                    }
                }
            }

            return null;
        }

        public void ReplaceContact(int recordId, string newContactId, int userId)
        {
            Model.Entities.MediaContactListRecord dbEntity = null;
            string idsToAdd = string.Empty;

            dbEntity = (from t in context.MediaContactListRecords
                          where t.Id == recordId
                          select t).FirstOrDefault();

            if (dbEntity != null)
            {
                //Is it AAP outlet?
                if (dbEntity.MediaOutletId != null)
                {
                    //If new contact is already in the list, we just need to delete the record otherwise replace contact
                    if (!context.MediaContactListRecords.Any(l => l.ListId == dbEntity.ListId &&
                                                                   l.MediaOutletId == dbEntity.MediaOutletId
                                                                   && l.MediaContactId == newContactId))
                    {
                        idsToAdd = string.Format("{0}#{1}####", dbEntity.MediaOutletId, newContactId);
                    }
                }
                //Is it PRN outlet?
                else if (dbEntity.PrnOutletId != null && dbEntity.PrnOutletId != 0)
                {
                    var prnContact = decimal.Parse(newContactId);
                    //If new contact is already in the list, we just need to delete the record otherwise replace contact
                    if (!context.MediaContactListRecords.Any(l => l.ListId == dbEntity.ListId &&
                                                                   l.PrnOutletId == dbEntity.PrnOutletId
                                                                   && l.PrnContactId == prnContact))
                    {
                        idsToAdd = string.Format("####{0}#{1}", dbEntity.PrnOutletId, newContactId);
                    }
                }
                //Is it OMA outlet?
                else if (dbEntity.OmaOutletId != null && dbEntity.OmaOutletId != 0)
                {
                    var omaContactId = int.Parse(newContactId);
                    //If new contact is already in the list, we just need to delete the record otherwise replace contact
                    if (!context.MediaContactListRecords.Any(l => l.ListId == dbEntity.ListId &&
                                                                  l.OmaOutletId == dbEntity.OmaOutletId
                                                                  && l.OmaContactId == omaContactId))
                    {
                        idsToAdd = string.Format("##{0}#{1}##", dbEntity.OmaOutletId, newContactId);
                    }
                }

                AddOrDelete(dbEntity.ListId, 0, idsToAdd, recordId.ToString(), userId);
            }
        }

        private int AddOrDelete(int listId, int listsetId, string idsToAdd, string recordIdsToDelete, int userId)
        {
            var listIdParam = new SqlParameter("@ListId", listId);
            var listsetIdParam = new SqlParameter("@ListsetId", listsetId) {Direction = ParameterDirection.InputOutput};
            var idsToAddParam = new SqlParameter("@IdsToAdd", idsToAdd);
            var recordIdsToDeleteParam = new SqlParameter("@recordIdsToDelete", recordIdsToDelete);
            var createNewVersionParam = new SqlParameter("@CreateNewVersion", true);
            var userIdParam = new SqlParameter("@UserId", userId);

            context.Database.ExecuteSqlCommand("exec @ListsetId = ListMaster_Update @ListId, @ListsetId, @IdsToAdd, @RecordIdsToDelete, @CreateNewVersion, @UserId", listIdParam, listsetIdParam, idsToAddParam, recordIdsToDeleteParam, createNewVersionParam, userIdParam);
            return (int)listsetIdParam.Value;
        }
    }
}
