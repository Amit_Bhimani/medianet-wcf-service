﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class MediaContactProfileRepository
    {
        private MedianetContext context;

        public MediaContactProfileRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public MediaContactProfile GetByContactId(string contactId)
        {
            return (from e in context.MediaContactProfiles
                    where e.MediaContact.Id == contactId
                    select e).AsNoTracking().FirstOrDefault();
        }

        public int Add(MediaContactProfile entity)
        {
            context.MediaContactProfiles.Add(entity);
            context.SaveChanges();

            return entity.Id;
        }

        public void Update(MediaContactProfile entity)
        {
            context.Entry(entity).State = System.Data.EntityState.Modified;
            context.SaveChanges();
        }
    }
}
