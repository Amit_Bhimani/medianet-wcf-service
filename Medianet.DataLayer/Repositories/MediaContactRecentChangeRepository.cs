﻿using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;


namespace Medianet.DataLayer.Repositories
{
    public class MediaContactRecentChangeRepository
    {
        private MedianetContext context;

        public MediaContactRecentChangeRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }
        public List<MediaContactRecentAdd> GetRecentlyAdded(int total)
        {
            var param = new SqlParameter("@Total", total);
            var entities = context.Database.SqlQuery<MediaContactRecentAdd>("exec Contact_RecentlyAdded_Get @Total", param);

            return entities.ToList();
        }
        public List<MediaContactRecentUpdate> GetRecentlyUpdated(int total)
        {
            var param = new SqlParameter("@Total", total);
            var entities = context.Database.SqlQuery<MediaContactRecentUpdate>("exec Contact_RecentlyUpdated_Get @Total", param);

            return entities.ToList();
        }
    }
}
