﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;
using Medianet.BusinessLayer.Model;
using Medianet.BusinessLayer.Common;
using System.Data.Objects.SqlClient;

namespace Medianet.DataLayer.Repositories
{
    public class MediaContactRepository
    {
        private MedianetContext context;

        public MediaContactRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public MediaContact GetById(string id)
        {
            return (from e in context.MediaContacts.Include("User").Include("Profile")
                    where e.Id == id
                    select e).AsNoTracking().FirstOrDefault();
        }

        public string Add(MediaContact entity)
        {
            context.MediaContacts.Add(entity);

            return entity.Id;
        }

        //public void UpdateSnapshot(string id)
        //{
        //    context.Database.ExecuteSqlCommand(string.Format("exec denormalize_aap_contacts {0}", id));
        //}

        public void Update(MediaContact entity)
        {
            MediaContact originalContact = context.MediaContacts.Include("Profile").Single(mc => mc.Id == entity.Id);
            var createdDate = originalContact.CreatedDate;

            // Set the LastModifiedDate in the OriginalValues for concurrency checking to work.
            originalContact.LastModifiedDate = entity.LastModifiedDate;
            context.Entry(originalContact).OriginalValues.SetValues(originalContact);

            //Update te properties of the contact, but keep the original CreatedDate.
            context.Entry(originalContact).CurrentValues.SetValues(entity);
            originalContact.CreatedDate = createdDate;
            originalContact.LastModifiedDate = DateTime.Now;

            var originalProfile = originalContact.Profile;

            if (originalProfile != null)
            {
                if (entity.Profile != null)
                    context.Entry(originalProfile).CurrentValues.SetValues(entity.Profile);
            }
            else
                if (entity.Profile != null)
                originalContact.Profile = entity.Profile;
        }

        public void UpdateIsDirtyFlag(string id)
        {
            var contact = context.MediaContacts.FirstOrDefault(c => c.Id == id);
            if (contact != null)
                contact.IsDirty = true;
        }
    }
}
