﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class MediaContactSavedSearchRepository
    {
        private MedianetContext context;

        public MediaContactSavedSearchRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public MediaContactSavedSearch GetById(int id)
        {
            var query = (from g in context.MediaContactSavedSearches
                         where g.Id == id
                               && g.IsActive == 1
                         select g);
            var queryWithInclude = query.Include("Group").Include("OwnerUser"); // Seperate the Include. Otherwise it ignores it

            return queryWithInclude.AsNoTracking().FirstOrDefault();
        }

        public List<MediaContactSavedSearch> GetAll(int userId)
        {
            var query = GetAllQuery(userId);

            return query.AsNoTracking().ToList();
        }

        public List<MediaContactSavedSearch> GetRecent(int userId, int takeCount)
        {
            var query = GetAllQuery(userId);

            return query.OrderByDescending(q => q.LastModifiedDate).Take(takeCount).AsNoTracking().ToList();
        }

        private IQueryable<MediaContactSavedSearch> GetAllQuery(int userId)
        {
            var query = (from g in context.MediaContactSavedSearches
                         join u1 in context.Users on g.OwnerUserId equals u1.Id
                         join u2 in context.Users on u1.DebtorNumber equals u2.DebtorNumber
                         where u2.Id == userId
                               && g.IsActive == 1
                               && (g.OwnerUserId == userId || g.IsPrivate == false)
                         select g);

            return query.Include("Group").Include("OwnerUser"); // Seperate the Include. Otherwise it ignores it
        }

        public int Add(MediaContactSavedSearch entity)
        {
            context.MediaContactSavedSearches.Add(entity);
            context.SaveChanges();

            return entity.Id;
        }

        public void Update(MediaContactSavedSearch entity)
        {
            context.Entry(entity).State = System.Data.EntityState.Modified;
            context.SaveChanges();
        }
    }
}
