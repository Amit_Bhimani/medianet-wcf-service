﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class MediaContactSearchLogRepository
    {
        private MedianetContext context;

        public MediaContactSearchLogRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public int Add(MediaContactSearchLog entity)
        {
            context.MediaContactSearchLog.Add(entity);
            context.SaveChanges();

            return entity.Id;
        }

        public List<MediaContactSearchLog> GetRecentSearchList(int userID, int maxRecord)
        {
            return (from log in context.MediaContactSearchLog
                    where log.UserId == userID && log.System == "P"
                    orderby log.CreatedDate descending
                    select log).Take(maxRecord).ToList();
        }

        public MediaContactSearchLog GetById(int id, int userID)
        {
            return (from log in context.MediaContactSearchLog
                    where log.UserId == userID && log.Id == id && log.System == "P"
                    select log).SingleOrDefault();
        }
    }
}

