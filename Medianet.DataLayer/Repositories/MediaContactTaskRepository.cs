﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class MediaContactTaskRepository
    {
        private MedianetContext context;

        public MediaContactTaskRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public MediaContactTask GetById(int id)
        {
            var query = (from g in context.MediaContactTasks
                         where g.Id == id
                               && g.IsActive == true
                         select g);
            var queryWithInclude = query.Include("Group").Include("OwnerUser").Include("CreatedByUser"); // Seperate the Include. Otherwise it ignores it

            return queryWithInclude.AsNoTracking().FirstOrDefault();
        }

        public List<MediaContactTask> GetAll(int userId)
        {
            var query = (from g in context.MediaContactTasks
                         join u1 in context.Users on g.OwnerUserId equals u1.Id
                         join u2 in context.Users on u1.DebtorNumber equals u2.DebtorNumber
                         where u2.Id == userId
                               && g.IsActive == true
                               && (g.OwnerUserId == userId || g.CreatedByUserId == userId || g.IsPrivate == false)
                         select g);
            var queryWithInclude = query.Include("Group").Include("OwnerUser").Include("CreatedByUser"); // Seperate the Include. Otherwise it ignores it

            return queryWithInclude.AsNoTracking().ToList();
        }

        public int Add(MediaContactTask entity)
        {
            context.MediaContactTasks.Add(entity);
            context.SaveChanges();

            return entity.Id;
        }

        public void Update(MediaContactTask entity)
        {
            context.Entry(entity).State = System.Data.EntityState.Modified;
            context.SaveChanges();
        }
    }
}
