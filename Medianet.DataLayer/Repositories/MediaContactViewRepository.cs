﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.BusinessLayer.Common;
using Medianet.Model;
using Medianet.Model.Entities;

namespace Medianet.DataLayer.Repositories
{
    public class MediaContactViewRepository
    {
        private readonly MedianetContext _context;
        public MediaContactViewRepository(UnitOfWork uow)
        {
            _context = uow.Context;

        }
        public void Add(MediaContactView entity)
        {
            _context.MediaContactView.Add(entity);
        }
        public List<MediaContactAlsoViewed> GetMediaContactViewSummary(string contactId, string outletId, List<int> subjectCodeIds, int maxCount)
        {
            var summary = _context.MediaContactViewSummary.Where(
                s => s.ContactId != contactId && s.OutletId != outletId
                     && subjectCodeIds.Contains(s.SubjectCodeId)).GroupBy(g => new { g.ContactId, g.OutletId })
                .Select(g => new { Key = g.Key, ViewCount = g.Sum(s => s.ViewCount) })
                .OrderByDescending(s => s.ViewCount).Take(maxCount).ToList();

            var alsoViewed = summary.Select(vl => (from e in _context.MediaEmployees
                                                   join c in _context.MediaContacts on e.ContactId equals c.Id
                                                   join o in _context.MediaOutlets on e.OutletId equals o.Id
                                                   where e.ContactId == vl.Key.ContactId && e.OutletId == vl.Key.OutletId
                                                   select new MediaContactAlsoViewed()
                                                   {
                                                       ContactId = e.ContactId,
                                                       OutletId = e.OutletId,
                                                       FirstName = c.FirstName,
                                                       LastName = c.LastName,
                                                       Title = e.JobTitle,
                                                       OutletName = o.Name,
                                                       RecordType = (int)RecordType.MediaContact,
                                                       ViewCount = vl.ViewCount,
                                                       IsGeneric = (bool)(o.IsGeneric.HasValue ? o.IsGeneric : false),
                                                       TwitterHandle = c.Twitter
                                                   }).Single()).ToList();

            return alsoViewed.Any() ? alsoViewed : null;
        }
    }
}
