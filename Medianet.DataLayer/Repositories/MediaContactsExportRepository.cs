﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class MediaContactsExportRepository
    {
        private MedianetContext context;

        public MediaContactsExportRepository(UnitOfWork uow) {
            context = uow.Context;
        }

        public MediaContactsExport GetById(int id) {
            return (from e in context.MediaContactsExports
                    where e.Id == id
                    select e).AsNoTracking().FirstOrDefault();
        }

        public int Add(MediaContactsExport entity) {
            context.MediaContactsExports.Add(entity);
            context.SaveChanges();

            return entity.Id;
        }

        public void Update(MediaContactsExport entity) {
            context.Entry(entity).State = System.Data.EntityState.Modified;
            context.SaveChanges();
        }

        /*public void Delete(int id) {
            MediaContactsExport entity = new Model.Entities.MediaContactsExport() { Id = id };
            context.Entry(entity).State = System.Data.EntityState.Unchanged;
            entity.IsDeleted = true;
            context.SaveChanges();
        }*/

        public List<MediaContactsExport> GetAllByDebtorNumberType(string debtorNumber, int type) {
            return (from e in context.MediaContactsExports
                    where e.DebtorNumber == debtorNumber
                    && e.IsDeleted == false
                    && e.ExportType == type
                    select e).AsNoTracking().ToList();
        }
    }
}
