﻿using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using System.Data.Entity;

namespace Medianet.DataLayer.Repositories
{
    public class MediaDatabaseLogRepository
    {
        private MedianetContext context;

        public MediaDatabaseLogRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public MediaDatabaseLog GetById(int id)
        {
            return (from l in context.MediaDatabaseLog
                    where l.ItemID == id
                    select l).AsNoTracking().FirstOrDefault();
        }

        public int Add(MediaDatabaseLog entity)
        {
            context.MediaDatabaseLog.Add(entity);
            return entity.ItemID;
        }

        public void Update(MediaDatabaseLog entity)
        {
            MediaDatabaseLog original = context.MediaDatabaseLog
            .Single(mo => mo.ItemID == entity.ItemID);

            context.Entry(original).CurrentValues.SetValues(entity);
        }

        public List<MediaDatabaseLog> GetMediaDatabaseLog(string outletId, string contactId)
        {

            List<MediaDatabaseLog> result = null;

            if (!string.IsNullOrEmpty(outletId) & !string.IsNullOrEmpty(contactId))
            {
                result = (from e in context.MediaDatabaseLog.Include("User")
                           where (e.OutletID == outletId)
                           && e.ContactID == contactId
                           orderby e.Accessed descending     
                           select e).Take(50).ToList();
            }
            else if (!string.IsNullOrEmpty(outletId) & string.IsNullOrEmpty(contactId))
            {
                result = (from e in context.MediaDatabaseLog.Include("User")
                           where (e.OutletID == outletId)
                           && (e.ContactID == null || e.ContactID == "")
                           orderby e.Accessed descending     
                           select e).Take(50).ToList();
            }
            else if (string.IsNullOrEmpty(outletId) & !string.IsNullOrEmpty(contactId))
            {
                result = (from e in context.MediaDatabaseLog.Include("User")
                           where (e.ContactID == contactId)
                           && ((e.OutletID == null || e.OutletID == ""))
                           orderby e.Accessed descending
                           select e).Take(50).ToList();
            }

            return result;
        }
        public List<MediaRecentlyViewed> GetMediaRecentlyViewed(int userId)
        { 
            var iParam = new SqlParameter("@UserId",userId);        

            var result = context.Database.SqlQuery<MediaRecentlyViewed>("exec sp_GetMediaRecentlyViewed @UserId", iParam).ToList();

            return result;
        }
    }
}
