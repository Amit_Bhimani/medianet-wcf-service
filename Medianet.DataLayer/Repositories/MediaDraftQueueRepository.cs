﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer.Common;

namespace Medianet.DataLayer.Repositories
{
    public class MediaDraftQueueRepository
    {
        private MedianetContext context;

        public MediaDraftQueueRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public MediaDraftQueue GetById(int id)
        {
            return (from e in context.MediaDraftQueues
                    where e.Id == id
                    select e).AsNoTracking().FirstOrDefault();
        }

        public List<MediaDraftQueueView> GetItemsInQueue(DateTime? startDate, DateTime? endDate, string searchText,
            int pageNumber, int recordsPerPage, string sortColumn, bool ascendingDirection)
        {
            var list = GetList(startDate, endDate, searchText);

            Func<MediaDraftQueueView, object> orderByFunc;

            switch (sortColumn)
            {
                case "OutletName":
                    orderByFunc = item => item.OutletName;
                    break;
                case "ContactName":
                    orderByFunc = item => item.ContactName;
                    break;
                case "LastModifiedDate":
                    orderByFunc = item => item.LastModifiedDate;
                    break;
                default:
                case "CreatedDate":
                    orderByFunc = item => item.CreatedDate;
                    break;
            }
            
            if (ascendingDirection)
                list = list.OrderBy(orderByFunc).AsQueryable();
            else
                list = list.OrderByDescending(orderByFunc).AsQueryable();

            return list.Skip((pageNumber - 1) * recordsPerPage)
                       .Take(recordsPerPage)
                       .ToList();
        }

        public int GetItemsInQueueCount(DateTime? startDate, DateTime? endDate, string searchText)
        {
            var list = GetList(startDate, endDate, searchText);

            return list.Count();
        }

        public int GetPendingDraftsInQueueCount()
        {
            var result = context.MediaDraftQueues.Count(m => m.Status == "N");

            return result;
        }

        private IQueryable<MediaDraftQueueView> GetList(DateTime? startDate, DateTime? endDate, string searchText)
        {
            var list = (from q in context.MediaDraftQueues
                        join c in context.MediaContacts on q.ContactId equals c.Id into contact from c in contact.DefaultIfEmpty()
                        join o in context.MediaOutlets on q.OutletId equals o.Id into outlet from o in outlet.DefaultIfEmpty()
                        where q.Status == "N"
                        select new MediaDraftQueueView
                        {
                            ContactId = q.ContactId,
                            OutletId = q.OutletId,
                            ContactName = c == null ? "" : c.FirstName + " " + c.LastName,
                            CreatedDate = q.CreatedDate,
                            Id = q.Id,
                            LastModifiedBy = q.LastModifiedBy,
                            LastModifiedDate = q.LastModifiedDate,
                            OutletName = o == null ? "" : o.Name,
                            Status = q.Status
                        });

            if (startDate.HasValue)
                list = list.Where(m => m.CreatedDate >= startDate);

            if (endDate.HasValue)
                list = list.Where(m => m.CreatedDate <= endDate);

            if (!string.IsNullOrEmpty(searchText))
                list = list.Where(m => m.OutletName.Contains(searchText) || m.ContactName.Contains(searchText));

            return list;
        }
        
        public void Delete(int id, byte[] rowVersion)
        {
            MediaDraftQueue dbEntity = new MediaDraftQueue { Id = id, RowVersion = rowVersion };
            
            context.Entry(dbEntity).State = System.Data.EntityState.Deleted;
        }

        public void Update(MediaDraftQueue draftQueue, DraftQueueStatus status, bool isAdminUser = false)
        {
            context.MediaDraftQueues.Attach(draftQueue);
            
            draftQueue.LastModifiedDate = DateTime.Now;

            context.Entry(draftQueue).Property(m => m.LastModifiedBy).IsModified = true;
            context.Entry(draftQueue).Property(m => m.LastModifiedDate).IsModified = true;

            if (isAdminUser)
                context.Entry(draftQueue).Property(m => m.Description).IsModified = true;
            else
                context.Entry(draftQueue).Property(m => m.JournalistComment).IsModified = true;

            if (status != DraftQueueStatus.Unchanged)
            {
                draftQueue.Status = ((char)status).ToString();

                context.Entry(draftQueue).Property(m => m.Status).IsModified = true;
            }

            context.Entry(draftQueue).OriginalValues["RowVersion"] = draftQueue.RowVersion;
        }
    }
}
