﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using Medianet.Model;
using Medianet.Model.Entities;
using System.Data;

namespace Medianet.DataLayer.Repositories
{
    public class MediaEmployeeDraftRepository
    {
        private MedianetContext context;

        public MediaEmployeeDraftRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }
        
        public MediaEmployeeDraft GetByQueueId(int queueId)
        {
            return (from e in context.MediaEmployeeDrafts.Include("DraftQueue")
                    where e.QueueId == queueId
                    select e).AsNoTracking().FirstOrDefault();
        }

        public MediaEmployeeDraft Get(string contactId, string outletId)
        {
            string status = ((char)Common.DraftQueueStatus.Added).ToString();
            
            return (from e in context.MediaEmployeeDrafts.Include("DraftQueue")
                    where e.ContactId == contactId && e.OutletId == outletId && e.DraftQueue.Status == status
                    orderby e.Id descending
                    select e).AsNoTracking().FirstOrDefault();
        }

        public void Add(MediaEmployeeDraft entity)
        {
            entity.DraftQueue = new MediaDraftQueue
            {
                ContactId = entity.ContactId,
                CreatedDate = DateTime.Now,
                OutletId = entity.OutletId,
                Status = ((char)Common.DraftQueueStatus.Added).ToString(),
                JournalistComment = entity.DraftQueue?.JournalistComment,
                LastModifiedDate = DateTime.Now
            };

            context.MediaEmployeeDrafts.Add(entity);
        }

        public void Update(MediaEmployeeDraft entity, bool isDeleted)
        {
            context.Entry(entity).State = isDeleted ? EntityState.Deleted : EntityState.Modified;
        }
    }
}
