﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;
using System.Data.Entity.Infrastructure;

namespace Medianet.DataLayer.Repositories
{
    public class MediaEmployeeRepository
    {
        private MedianetContext context;

        public MediaEmployeeRepository(UnitOfWork uow) {
            context = uow.Context;
        }

        public MediaEmployee GetByKey(string outletId, string contactId) {
            return (from e in context.MediaEmployees.Include("User")
                        .Include("MediaContact")
                        .Include("MediaOutlet")
                        .Include("Roles")
                        .Include("Subjects")
                        .Include("AAPCity")
                    where e.OutletId == outletId
                    && e.ContactId == contactId
                    select e).AsNoTracking().FirstOrDefault();
        }

        public List<MediaEmployee> GetAllByContactId(string contactId) {
            return (from e in context.MediaEmployees
                        .Include("User")
                        .Include("MediaContact")
                        .Include("MediaOutlet")
                        .Include("Roles")
                        .Include("Subjects")
                        .Include("AAPCity")
                    where e.ContactId == contactId
                    select e).AsNoTracking().ToList();
        }

        public List<MediaOutlet> GetEmployeesOutletsInfo(string contactId)
        {
            return (from e in context.MediaEmployees
                        .Include("MediaOutlet")
                    where e.ContactId == contactId && e.RowStatus == "A" && e.MediaOutlet.RowStatus == "A" && e.MediaOutlet.SendToOMA == true
                    select e.MediaOutlet).ToList();
        }

        public List<MediaEmployee> GetAllByOutletId(string outletId) {
            return (from e in context.MediaEmployees
                        .Include("User")
                        .Include("MediaContact")
                        .Include("MediaOutlet")
                        .Include("Roles")
                        .Include("Subjects")
                        .Include("AAPCity")
                    where e.OutletId == outletId
                    select e).AsNoTracking().ToList();
        }

        public void Add(MediaEmployee entity) {
            context.MediaEmployees.Add(entity);
        }

        //public void UpdateContactSnapshot(string id)
        //{
        //    context.Database.ExecuteSqlCommand(string.Format("exec denormalize_aap_contacts {0}", id));
        //}

        public void UpdateIsEntityDeletedFlag(string outletId, string contactId)
        {
            context.Database.ExecuteSqlCommand(string.Format("exec sp_UpdateIsEntityDeletedFlag {0}, {1}", outletId, contactId));
        }

        public void Update(MediaEmployee entity) {
            MediaEmployee originalEmployee = context.MediaEmployees
                        .Include("MediaContact")
                        .Include("MediaOutlet")
                        .Include("Roles")
                        .Include("Subjects")
                        .Single(mo => mo.OutletId == entity.OutletId && mo.ContactId == entity.ContactId);
            var createdDate = originalEmployee.CreatedDate;

            // Set the LastModifiedDate in the OriginalValues for concurrency checking to work.
            originalEmployee.LastModifiedDate = entity.LastModifiedDate;
            context.Entry(originalEmployee).OriginalValues.SetValues(originalEmployee);

            //1) Update te properties of the Employee, but keep the original CreatedDate.
            context.Entry(originalEmployee).CurrentValues.SetValues(entity);
            originalEmployee.CreatedDate = createdDate;
            originalEmployee.LastModifiedDate = DateTime.Now;

            // 2) Check what to do with Roles
            List<MediaEmployeeRole> roles = originalEmployee.Roles.ToList();
            foreach (var item in roles) {
                MediaEmployeeRole fmt = null;

                //Is the Role still there?
                if (entity.Roles != null)
                    fmt = entity.Roles.SingleOrDefault(i => i.RoleId == item.RoleId);

                if (fmt == null)
                    // No. It looks like that Role was deleted.
                    originalEmployee.Roles.Remove(item);
                // No need for a modify, its just a table of ID's
            }

            // Second loop to determine if we have anything we need to Add.
            foreach (var item in entity.Roles)
            {
                //Check if this Role exists already, if not, add it.
                if (!roles.Any(i => i.RoleId == item.RoleId))
                {
                    // The Role doesnt exist in the database. Lets add it.
                    originalEmployee.Roles.Add(item);
                }
            }

            // 3) Check what to do with subjects
            List<MediaEmployeeSubject> subjects = originalEmployee.Subjects.ToList();
            foreach (var item in subjects)
            {
                MediaEmployeeSubject fmt = null;

                //Is the Subject still there?
                if (entity.Subjects != null)
                    fmt = entity.Subjects.SingleOrDefault(i => i.SubjectId == item.SubjectId);

                if (fmt == null)
                    // No. It looks like that Subject was deleted.
                    originalEmployee.Subjects.Remove(item);
                // No need for a modify, its just a table of ID's
            }
            // Second loop to determine if we have anything we need to Add.
            foreach (var item in entity.Subjects)
            {
                var sub = subjects.Where(s => s.SubjectId == item.SubjectId).FirstOrDefault();

                //Check if this Subject exists already, if not, add it.
                if (sub == null)
                {
                    // The Subject doesnt exist in the database. Lets add it.
                    originalEmployee.Subjects.Add(item);
                }
                else
                    sub.IsPrimaryContact = item.IsPrimaryContact;
            }
        }

        public void SetIsDirtyFlagOnContact(string contactId)
        {
            var contactEntity = context.MediaContacts.FirstOrDefault(c => c.Id == contactId);
            if (contactEntity != null)
            {
                contactEntity.IsDirty = true;
                context.Entry(contactEntity).State = System.Data.EntityState.Modified;
            }
        }
        
        public MediaEmployeeView GetDetails(string outletId, string contactId, int userId)
        {
            var cmd = context.Database.Connection.CreateCommand();
            cmd.CommandText = $"exec MediaContact_Get {contactId}, {outletId}, {userId}";

            context.Database.Connection.Open();
            var reader = cmd.ExecuteReader();
            var objectContext = ((IObjectContextAdapter)context).ObjectContext;

            MediaEmployeeView result = objectContext.Translate<MediaEmployeeView>(reader).First();
            reader.NextResult();

            result.Roles = objectContext.Translate<KeyValuePair<int, string>>(reader).ToList();
            reader.NextResult();

            result.Subjects = objectContext.Translate<MediaEmployeeSubjectView>(reader).ToList();
            reader.NextResult();
            
            result.BelongsToLists = objectContext.Translate<MediaEmployeeListView>(reader).ToList();
            reader.NextResult();

            reader.NextResult();// Tasks - No longer used

            result.Documents = objectContext.Translate<MediaEmployeeDocumentView>(reader).ToList();
            reader.NextResult();

            result.AtOutlet = objectContext.Translate<MediaEmployeeOutletView>(reader).FirstOrDefault(m => m.OutletId.Trim() == outletId);
            reader.NextResult();

            return result;
        }
    }
}
