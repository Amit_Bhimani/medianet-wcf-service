﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using Medianet.DataLayer.Common;
using Medianet.Model;
using Medianet.Model.Entities;

namespace Medianet.DataLayer.Repositories
{
    public class MediaMovementRepository
    {
        private MedianetContext context;

        public MediaMovementRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public MediaContactMovement GetById(int id)
        {
            return context.MediaContactMovements.Where(m => m.Id == id)
                .Include("MediaEmployee")
                .Include("MediaEmployee.MediaContact")
                .Include("MediaOutlet")
                .Include("User").AsNoTracking().FirstOrDefault();
        }

        public List<MediaContactMovement> GetAll(bool includeHidden, int pageNumber, int recordsPerPage)
        {
            var query = includeHidden ? context.MediaContactMovements : context.MediaContactMovements.Where(m => m.Visible);

            return query.Include("MediaEmployee")
                .Include("MediaEmployee.MediaContact")
                .Include("MediaOutlet")
                .Include("User")
                .OrderBy(p => p.Pinned ? 0 : 1)
                .ThenByDescending(c => c.CreatedDate)
                .Skip((pageNumber - 1) * recordsPerPage)
                .Take(recordsPerPage).AsNoTracking().ToList();
        }

        public int GetAllCount(bool includeHidden)
        {
            IQueryable<MediaContactMovement> query = includeHidden ? context.MediaContactMovements : context.MediaContactMovements.Where(m => m.Visible);

            return query.Count();
        }
        public int GetAllPinned()
        {
            IQueryable<MediaContactMovement> query = context.MediaContactMovements.Where(m => m.Pinned);

            return query.Count();
        }

        public List<MediaContactMovement> GetByOutletId(string outletId, int pageNumber, int recordsPerPage)
        { 
           var query = context.MediaContactMovements.Where(q => q.OutletId == outletId && q.Visible == true);

           query = query.Include("MediaEmployee")
                .Include("MediaEmployee.MediaContact")
                .Include("MediaOutlet")
                .Include("User")
                .OrderByDescending(c => c.CreatedDate);

            query = query.ApplyPaging(pageNumber, recordsPerPage);

            return query.AsNoTracking().ToList();
        }
        public List<MediaContactMovement> GetByContactId(string contactId, int pageNumber, int recordsPerPage)
        {
            var query = context.MediaContactMovements.Where(q => q.ContactId == contactId && q.Visible == true);

            query = query.Include("MediaEmployee")
                .Include("MediaEmployee.MediaContact")
                .Include("MediaOutlet")
                .Include("User")
                .OrderByDescending(c => c.CreatedDate);

                query = query.ApplyPaging(pageNumber, recordsPerPage);

                return query.AsNoTracking().ToList();
        }
        public void Add(MediaContactMovement entity)
        {

            context.MediaContactMovements.Add(entity);
        }


        public void Update(MediaContactMovement entity)
        {
            MediaContactMovement originaContactMovement = context.MediaContactMovements.Single(m => m.Id == entity.Id);
            context.Entry(originaContactMovement).OriginalValues.SetValues(originaContactMovement);

            context.Entry(originaContactMovement).CurrentValues.SetValues(entity);
            
        }

        public void Delete(int id)
        {
            MediaContactMovement originaContactMovement = context.MediaContactMovements.Single(m => m.Id == id);
            context.Entry(originaContactMovement).State = System.Data.EntityState.Deleted;
        }

        public void PinUnpin(int id, bool pinned)
        {
            MediaContactMovement dbEntity = context.MediaContactMovements.FirstOrDefault(e => e.Id == id);

            if (dbEntity != null)
            {
                dbEntity.Pinned = !pinned;
                context.Entry(dbEntity).State = System.Data.EntityState.Modified;
                context.SaveChanges();
            }
        }
    }
}
