﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class MediaOutletProfileRepository
    {
        private MedianetContext context;

        public MediaOutletProfile GetById(string id)
        {
            return (from e in context.MediaOutletProfiles
                    where e.OutletId == id
                    select e).AsNoTracking().FirstOrDefault();
        }

        public MediaOutletProfileRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public string Add(MediaOutletProfile entity)
        {
            context.MediaOutletProfiles.Add(entity);
            context.SaveChanges();

            return entity.OutletId;
        }

        public void Update(MediaOutletProfile entity)
        {
            context.Entry(entity).State = System.Data.EntityState.Modified;
            context.SaveChanges();
        }
    }
}
