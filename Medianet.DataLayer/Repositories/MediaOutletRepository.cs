﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;
using System.Data.Objects.SqlClient;
using Medianet.BusinessLayer.Model;

namespace Medianet.DataLayer.Repositories
{
    public class MediaOutletRepository
    {
        private MedianetContext context;

        private const int _prnOutletActiveStatus = 823;

        public MediaOutletRepository(UnitOfWork uow)

        {
            context = uow.Context;
        }

        public MediaOutlet GetById(string id)
        {
            return (from e in context.MediaOutlets.Include("User")
                        .Include("StationFormats")
                        .Include("Subjects")
                        .Include("WorkingLanguages")
                        .Include("AAPCity")
                        .Include("AAPPostalCity")
                        .Include("RelatedOutlets")
                        .Include("SyndicatedOutlets")
                        .Include("Profile")
                    where e.Id == id
                    select e).AsNoTracking().FirstOrDefault();
        }

        public List<MediaOutlet> GetOutletsByIds(string[] ids)
        {
            return (from e in context.MediaOutlets
                    where ids.Contains(e.Id)
                    select e).AsNoTracking().ToList();
        }

        public List<string> GetOutletIdsByIds(string[] ids)
        {
            return (from e in context.MediaOutlets
                    where ids.Contains(e.Id)
                    && e.RowStatus == "A"
                    select e.Id).AsNoTracking().ToList();
        }

        public List<MediaOutletPartial> GetRelatedOutlets(string outletId)
        {
            return (from r in context.MediaRelatedOutlets
                    where r.OutletId == outletId
                    join e in context.MediaOutlets on r.RelatedOutletId equals e.Id
                    join p in context.ProductTypes on e.ProductTypeId equals p.Id into a
                    from b in a.DefaultIfEmpty(null)
                    select new MediaOutletPartial
                    {
                        Id = r.RelatedOutletId,
                        Name = e.Name,
                        ProductTypeId = e.ProductTypeId,
                        ProductTypeName = b.Name,
                        ProductTypeCategory = b.ProductTypeCategory
                    }).AsNoTracking().ToList<MediaOutletPartial>();
        }

        public List<MediaOutletPartial> GetSyndicatingToOutlets(string outletId)
        {
            return (from s in context.MediaSyndicatedOutlets
                    where s.OutletId == outletId
                    join e in context.MediaOutlets on s.SyndicatedOutletId equals e.Id
                    join p in context.ProductTypes on e.ProductTypeId equals p.Id into a
                    from b in a.DefaultIfEmpty(null)
                    orderby e.Name
                    select new MediaOutletPartial { Id = e.Id, Name = e.Name, ProductTypeId = e.ProductTypeId, ProductTypeName = b.Name }).AsNoTracking().ToList<MediaOutletPartial>();
        }

        public List<MediaOutletPartial> GetSyndicatedFromOutlets(string outletId)
        {
            return (from s in context.MediaSyndicatedOutlets
                    where s.SyndicatedOutletId == outletId
                    join e in context.MediaOutlets on s.OutletId equals e.Id
                    join p in context.ProductTypes on e.ProductTypeId equals p.Id into a
                    from b in a.DefaultIfEmpty(null)
                    orderby e.Name
                    select new MediaOutletPartial { Id = e.Id, Name = e.Name, ProductTypeId = e.ProductTypeId, ProductTypeName = b.Name }).AsNoTracking().ToList<MediaOutletPartial>();
        }

        public List<MediaOutletPartial> GetMediaOwnerBusinesses(string outletId)
        {
            return (from e in context.MediaOutlets
                    where e.ParentId == outletId && e.HasParent == true && e.RowStatus == "A"
                    orderby e.Name
                    select new MediaOutletPartial { Id = e.Id, Name = e.Name }).AsNoTracking().ToList<MediaOutletPartial>();
        }

        public List<MediaOutlet> GetByName(string namePart)
        {
            return (from e in context.MediaOutlets
                        .Include("AAPCity")
                    where e.Name.ToUpper().StartsWith(namePart.ToUpper())
                    select e).AsNoTracking().ToList();
        }

        public List<MediaOutlet> GetByNamePartial(string namePart)
        {
            return (from e in context.MediaOutlets
                        .Include("AAPCity")
                    where e.Name.Contains(namePart.ToUpper())
                    select e).AsNoTracking().ToList();
        }

        public string GetOwnershipById(string id, string defaultParentName)
        {
            return GetOwnershipRecursive(id, new List<string>(), defaultParentName);
        }

        public string Add(MediaOutlet entity)
        {
            context.MediaOutlets.Add(entity);

            // if the ParentId is null or empty set it to the new Id. EF assigns a random 
            if (string.IsNullOrWhiteSpace(entity.ParentId))
                entity.ParentId = entity.Id;

            return entity.Id;
        }

        public void Update(MediaOutlet entity)
        {
            MediaOutlet originalOutlet = context.MediaOutlets.Include("StationFormats")
                        .Include("Subjects")
                        .Include("WorkingLanguages")
                        .Include("Profile")
                        .Include("RelatedOutlets")
                        .Include("SyndicatedOutlets")
                        .Single(mo => mo.Id == entity.Id);
            var createdDate = originalOutlet.CreatedDate;

            // Set the LastModifiedDate in the OriginalValues for concurrency checking to work.
            originalOutlet.LastModifiedDate = entity.LastModifiedDate;
            context.Entry(originalOutlet).OriginalValues.SetValues(originalOutlet);

            //Update te properties of the outlet, but keep the original CreatedDate.
            context.Entry(originalOutlet).CurrentValues.SetValues(entity);
            originalOutlet.CreatedDate = createdDate;
            originalOutlet.LastModifiedDate = DateTime.Now;

            var originalProfile = originalOutlet.Profile;

            // 1) Check what to do with the Profile text.
            if (originalProfile != null)
            {
                if (entity.Profile != null)
                    context.Entry(originalProfile).CurrentValues.SetValues(entity.Profile);
            }
            else
                if (entity.Profile != null)
                originalOutlet.Profile = entity.Profile;

            // 2) Check what to do with Station format
            List<MediaOutletStationFormat> formats = originalOutlet.StationFormats.ToList();
            foreach (var item in formats)
            {
                //Is the station format still there?
                var fmt = entity.StationFormats.SingleOrDefault(i => i.OutletId == item.OutletId && i.StationFormatId == item.StationFormatId);
                if (fmt == null)
                    // No. It looks like that station format was deleted.
                    context.MediaOutletStationFormats.Remove(item);
                // No need for a modify, its just a table of ID's

            }
            // Second loop to determine if we have anything we need to Add.
            foreach (var item in entity.StationFormats)
            {
                //Check if this station/format exists already, if not, add it.
                if (!formats.Any(i => i.OutletId == item.OutletId && i.StationFormatId == item.StationFormatId))
                    // The station format doesnt exist in the database. Lets add it.
                    originalOutlet.StationFormats.Add(item);
            }

            // 3) Check what to do with Working Language
            List<MediaOutletWorkingLanguage> languages = originalOutlet.WorkingLanguages.ToList();
            foreach (var item in languages)
            {
                //Is the working language still there?
                MediaOutletWorkingLanguage fmt = null;
                if (entity.WorkingLanguages != null)
                    fmt = entity.WorkingLanguages.SingleOrDefault(i => i.OutletId == item.OutletId && i.WorkingLanguageId == item.WorkingLanguageId);

                if (fmt == null)
                    // No. It looks like that working language was deleted.
                    context.MediaOutletWorkingLanguages.Remove(item);
                // No need for a modify, its just a table of ID's
            }
            // Second loop to determine if we have anything we need to Add.
            foreach (var item in entity.WorkingLanguages)
            {
                //Check if this working language exists already, if not, add it.
                if (!languages.Any(i => i.OutletId == item.OutletId && i.WorkingLanguageId == item.WorkingLanguageId))
                    // The station format doesnt exist in the database. Lets add it.
                    originalOutlet.WorkingLanguages.Add(item);
            }

            // 4) Check what to do with Subjects
            List<MediaOutletSubject> subjects = originalOutlet.Subjects.ToList();
            foreach (var item in subjects)
            {
                //Is the subject still there?
                MediaOutletSubject fmt = null;
                if (entity.Subjects != null)
                    fmt = entity.Subjects.SingleOrDefault(i => i.OutletId == item.OutletId && i.SubjectId == item.SubjectId);

                if (fmt == null)
                    // No. It looks like that subject was deleted.
                    context.MediaOutletSubjects.Remove(item);
                // No need for a modify, its just a table of ID's
            }
            // Second loop to determine if we have anything we need to Add.
            foreach (var item in entity.Subjects)
            {
                //Check if this working language exists already, if not, add it.
                if (!subjects.Any(i => i.OutletId == item.OutletId && i.SubjectId == item.SubjectId))
                    // The station format doesnt exist in the database. Lets add it.
                    originalOutlet.Subjects.Add(item);
            }

            // 5) Check what to do with Relate Outlets
            List<MediaRelatedOutlet> relateOutlets = originalOutlet.RelatedOutlets.ToList();
            foreach (var item in relateOutlets)
            {
                //Is the related outlet still there?
                MediaRelatedOutlet fmt = null;
                if (entity.RelatedOutlets != null)
                    fmt = entity.RelatedOutlets.SingleOrDefault(i => i.OutletId == item.OutletId && i.RelatedOutletId == item.RelatedOutletId);

                if (fmt == null)
                    // No. It looks like that related outlet was deleted.
                    context.MediaRelatedOutlets.Remove(item);
                // No need for a modify, its just a table of ID's
            }
            // Second loop to determine if we have anything we need to Add.
            foreach (var item in entity.RelatedOutlets)
            {
                //Check if this related outlet exists already, if not, add it.
                if (!relateOutlets.Any(i => i.OutletId == item.OutletId && i.RelatedOutletId == item.RelatedOutletId))
                    // The station format doesnt exist in the database. Lets add it.
                    originalOutlet.RelatedOutlets.Add(item);
            }

            // 6) Check what to do with Syndicated Outlets
            List<MediaSyndicatedOutlet> syndicatedOutlets = originalOutlet.SyndicatedOutlets.ToList();
            foreach (var item in syndicatedOutlets)
            {
                //Is the related outlet still there?
                MediaSyndicatedOutlet fmt = null;
                if (entity.SyndicatedOutlets != null)
                    fmt = entity.SyndicatedOutlets.SingleOrDefault(i => i.OutletId == item.OutletId && i.SyndicatedOutletId == item.SyndicatedOutletId);

                if (fmt == null)
                    // No. It looks like that related outlet was deleted.
                    context.MediaSyndicatedOutlets.Remove(item);
                // No need for a modify, its just a table of ID's
            }
            // Second loop to determine if we have anything we need to Add.
            foreach (var item in entity.SyndicatedOutlets)
            {
                //Check if this related outlet exists already, if not, add it.
                if (!syndicatedOutlets.Any(i => i.OutletId == item.OutletId && i.SyndicatedOutletId == item.SyndicatedOutletId))
                    // The station format doesnt exist in the database. Lets add it.
                    originalOutlet.SyndicatedOutlets.Add(item);
            }

        }

        //public void UpdateSnapshot(string id)
        //{
        //    context.Database.ExecuteSqlCommand(string.Format("exec denormalize_aap_outlet {0}", id));
        //}

        public void UpdateIsEntityDeletedFlag(string outletId)
        {
            context.Database.ExecuteSqlCommand(string.Format("exec sp_UpdateIsEntityDeletedFlag {0}", outletId));
        }

        public void UpdateIsDirtyFlag(string id)
        {
            var outlet = context.MediaOutlets.FirstOrDefault(c => c.Id == id);
            if (outlet != null)
                outlet.IsDirty = true;
        }

        public void SetOutletLogo(string id, string logoFilename, int userId)
        {
            MediaOutlet dbEntity = (from e in context.MediaOutlets
                                    where e.Id == id
                                    select e).FirstOrDefault();

            dbEntity.LogoFileName = logoFilename;
            dbEntity.LastModifiedDate = DateTime.Now;
            dbEntity.LastModifiedByUserId = userId;

            context.SaveChanges();
        }

        public List<int> GetOmaContactIds(int outletId)
        {
            return (from c in context.MediaOmaContacts
                    where c.OmaOutletId == outletId
                    select c.ContactId).ToList();
        }

        private string GetOwnershipRecursive(string id, List<string> outletIds, string defaultParentName)
        {
            MediaOutlet parent;
            string parentName = defaultParentName;

            // To prevent looping infinitely exit as soon as we detect an OutletId we have already processed.
            if (!outletIds.Contains(id))
            {
                parent = (from e in context.MediaOutlets
                          where e.Id == id
                          select e).AsNoTracking().FirstOrDefault();

                if (parent != null)
                {
                    if (string.IsNullOrEmpty(parent.ParentId))
                        parentName = parent.Name;
                    else
                    {
                        outletIds.Add(parent.Id);
                        parentName = GetOwnershipRecursive(parent.Id, outletIds, parent.Name);
                    }
                }
            }

            return parentName;
        }
    }
}
