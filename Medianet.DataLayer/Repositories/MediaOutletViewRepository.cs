﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.BusinessLayer.Common;
using Medianet.Model;
using Medianet.Model.Entities;

namespace Medianet.DataLayer.Repositories
{
    public class MediaOutletViewRepository
    {
        private readonly MedianetContext _context;
        public MediaOutletViewRepository(UnitOfWork uow)
        {
            _context = uow.Context;

        }
        public void Add(MediaOutletView entity)
        {
            _context.MediaOutletView.Add(entity);
        }
    }
}
