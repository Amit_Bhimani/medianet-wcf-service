﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using Medianet.DataLayer.Common;
using Medianet.Model;
using Medianet.Model.Entities;

namespace Medianet.DataLayer.Repositories
{
    public class MediaPriorityNoticeRepository
    {
        private MedianetContext context;

        public MediaPriorityNoticeRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public MediaPriorityNotice GetById(int id)
        {
            return context.MediaPriorityNotices.Where(m => m.Id == id)
                .Include("MediaContact")
                .Include("MediaOutlet")
                .Include("User").AsNoTracking().FirstOrDefault();
        }

        public MediaPriorityNotice GetByContactId(string contactId)
        {
            return context.MediaPriorityNotices.Where(m => m.ContactId == contactId)
                .Include("MediaContact")
                .Include("User").AsNoTracking().FirstOrDefault();
        }

        public MediaPriorityNotice GetByOutletId(string outletId)
        {
            return context.MediaPriorityNotices.Where(m => m.OutletId == outletId)
                .Include("MediaOutlet")
                .Include("User").AsNoTracking().FirstOrDefault();
        }

        public List<MediaPriorityNotice> GetAll(int pageNumber, int recordsPerPage)
        {
            return context.MediaPriorityNotices.Include("MediaContact")
                .Include("MediaOutlet")
                .Include("User")
                .OrderByDescending(c => c.CreatedDate)
                .Skip((pageNumber - 1) * recordsPerPage)
                .Take(recordsPerPage).AsNoTracking().ToList();
        }

        public int GetAllCount()
        {
            return context.MediaPriorityNotices.Count();
        }

        public void Add(MediaPriorityNotice entity)
        {
            context.MediaPriorityNotices.Add(entity);
        }

        public void Update(MediaPriorityNotice entity)
        {
            MediaPriorityNotice originalEntity = context.MediaPriorityNotices.Single(m => m.Id == entity.Id);

            context.Entry(originalEntity).OriginalValues.SetValues(originalEntity);
            context.Entry(originalEntity).CurrentValues.SetValues(entity);
        }

        public void Delete(int id)
        {
            MediaPriorityNotice originalEntity = context.MediaPriorityNotices.Single(m => m.Id == id);
            context.Entry(originalEntity).State = System.Data.EntityState.Deleted;
        }
    }
}
