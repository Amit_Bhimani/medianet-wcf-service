﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class MediaStatisticsRepository
    {
        private MedianetContext context;

        public MediaStatisticsRepository(UnitOfWork uow) {
            context = uow.Context;
        }

        public MediaStatistics Get() {
            return (from ms in context.MediaStatistics
                      select ms).AsNoTracking().FirstOrDefault();
        }
    }
}
