﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Medianet.Model;
using Medianet.Model.Entities;

namespace Medianet.DataLayer.Repositories
{
    public class MnjProfileCategoryRepository
    {
        private MedianetContext context;

        public MnjProfileCategoryRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public List<int> GetCategoriesByProfileId(int profileID)
        {
            return (from e in context.MnjProfileCategories
                    where e.ProfileID == profileID
                    select e.WebCategoryID).ToList<int>();
        }
    }
}
