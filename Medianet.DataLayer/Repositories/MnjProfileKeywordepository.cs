﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Medianet.Model;
using Medianet.Model.Entities;

namespace Medianet.DataLayer.Repositories
{
    public class MnjProfileKeywordRepository
    {
        private MedianetContext context;

        public MnjProfileKeywordRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public List<string> GetKeywordsByProfileId(int profileID)
        {
            return (from e in context.MnjProfileKeywords
                    where e.ProfileID == profileID
                    select e.Keyword).ToList();
        }
    }
}
