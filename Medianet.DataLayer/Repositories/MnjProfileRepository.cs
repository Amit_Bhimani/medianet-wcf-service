﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Medianet.Model;
using Medianet.Model.Entities;
using System.Data.SqlClient;

namespace Medianet.DataLayer.Repositories
{
    public class MnjProfileRepository
    {
        private MedianetContext context;

        public MnjProfileRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public List<MnjProfile> GetByUserId(int userId)
        {
            return (from e in context.MnjProfiles.Include("Categories").Include("Keywords")
                    where e.UserID == userId
                    select e).AsNoTracking().ToList();
        }

        public MnjProfile GetById(int profileId)
        {
            return (from e in context.MnjProfiles.Include("Categories").Include("Keywords")
                    where e.ProfileID == profileId
                    select e).AsNoTracking().FirstOrDefault();
        }

        public MnjProfile GetById(int profileId, int userId)
        {
            return (from e in context.MnjProfiles
                    where e.ProfileID == profileId && e.UserID == userId
                    select e).FirstOrDefault();
        }

        public void DeleteProfile(int id, int userId)
        {
            context.Database.ExecuteSqlCommand(string.Format("exec sp_MNJ_DeleteProfile {0}, {1}", id, userId));
        }

        public int AddProfile(MnjProfile entity)
        {
            return context.Database.SqlQuery<int>("exec sp_MNJ_AddProfile @profileName, @userId, @setDefault, @emailAlert, @emailAddress, @keywords, @categories",
                new SqlParameter("@profileName", entity.ProfileName),
                new SqlParameter("@userId", entity.UserID),
                new SqlParameter("@setDefault", entity.IsDefault),
                new SqlParameter("@emailAlert", entity.EmailAlert),
                new SqlParameter("@emailAddress", entity.EmailAddress),
                new SqlParameter("@keywords", entity.Keywords == null ? "" : string.Join(", ", entity.Keywords.Select(m => m.Keyword)) + ","),
                new SqlParameter("@categories", entity.Categories == null ? "" : string.Join(", ", entity.Categories.Select(m => m.WebCategoryID)) + ",")
                ).FirstOrDefault();
        }

        public void EditProfile(MnjProfile entity)
        {
            context.Database.ExecuteSqlCommand("exec sp_MNJ_UpdateProfile @profileId, @profileName, @setDefault, @emailAlert, @emailAddress, @keywords, @categories",
                new SqlParameter("@profileId", entity.ProfileID),
                new SqlParameter("@profileName", entity.ProfileName),
                new SqlParameter("@setDefault", entity.IsDefault),
                new SqlParameter("@emailAlert", entity.EmailAlert),
                new SqlParameter("@emailAddress", entity.EmailAddress),
                new SqlParameter("@keywords", entity.Keywords == null ? "" : string.Join(", ",  entity.Keywords.Select(m => m.Keyword)) + ","),
                new SqlParameter("@categories", entity.Categories == null ?"" : string.Join(", ",  entity.Categories.Select(m => m.WebCategoryID)) + ","));
        }
    }
}
