﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Medianet.Model;
using Medianet.Model.Entities;
using System.Data.SqlClient;
using System.Transactions;

namespace Medianet.DataLayer.Repositories
{
    public class MnjUserSavedReleasesRepository
    {
        private MedianetContext context;
        private UnitOfWork _uow;

        public MnjUserSavedReleasesRepository(UnitOfWork uow)
        {
            context = uow.Context;
            _uow = uow;
        }

        public List<MnjProfileReleases> GetSavedReleases(int userId, int itemsPerPage, int currentPage)
        {
            var entities = context.Database.SqlQuery<MnjProfileReleases>("exec sp_MNJ_GetSavedReleases @userId, @ItemsPerPage, @page",
                new SqlParameter("@userId", userId),
                new SqlParameter("@ItemsPerPage", itemsPerPage),
                new SqlParameter("@page", currentPage));

            return entities.ToList();
        }

        public void Save(int userId, string releaseIds)
        {
            context.Database.ExecuteSqlCommand("exec sp_MNJ_SaveReleases @userId, @Releases",
                new SqlParameter("@userId", userId),
                new SqlParameter("@Releases", releaseIds));
        }

        public void Delete(int userId, List<int> releaseIds)
        {
            using (TransactionScope tran = new TransactionScope())
            {
                foreach (int id in releaseIds)
                {
                    MnjUserSavedReleases entity = new MnjUserSavedReleases { ReleaseId = id, UserId = userId };
                    context.Entry(entity).State = System.Data.EntityState.Deleted;
                }

                context.SaveChanges();

                tran.Complete();
            }
        }
    }
}
