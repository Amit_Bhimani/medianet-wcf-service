﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class MonitoringAlertRepository
    {
        private MedianetContext context;

        public MonitoringAlertRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public void Add(MonitoringAlert alert)
        {
            context.MonitoringAlerts.Add(alert);
        }

        public void Remove(int alertId)
        {
            MonitoringAlert alert =  context.MonitoringAlerts.Where(c => c.Id == alertId).FirstOrDefault();
            context.MonitoringAlerts.Remove(alert);
        }

        public void Update(MonitoringAlert alert)
        {
            MonitoringAlert existingAlert = context.MonitoringAlerts.Where(c => c.Id == alert.Id).FirstOrDefault();

            if (existingAlert != null)
            {
                context.Entry(existingAlert).CurrentValues.SetValues(alert);
            }
            else
            {
                context.MonitoringAlerts.Attach(alert);
                context.Entry(alert).State = System.Data.EntityState.Modified;
            }
        }

        
    }
}
