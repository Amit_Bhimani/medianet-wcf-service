﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class MonitoringCampaignRepository
    {
        private MedianetContext context;

        public MonitoringCampaignRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public void Add(MonitoringCampaign campaign)
        {
            foreach(MonitoringMediaType mediaType in campaign.SearchCriteria.MediaTypes)
            {
                context.MonitoringMediaTypes.Attach(mediaType);
            }

            context.MonitoringCampaigns.Add(campaign);
        }

        public void Remove(int campaignId)
        {
            MonitoringCampaign campaign =  context.MonitoringCampaigns.Where(c => c.Id == campaignId).FirstOrDefault();
            context.MonitoringCampaigns.Remove(campaign);
        }

        public MonitoringCampaign GetById(int id)
        {
            return (from c in context.MonitoringCampaigns where c.Id == id select c)
                .Include(c => c.SearchCriteria)
                .Include(c => c.SearchCriteria.MediaTypes)
                .Include(c => c.Alerts)
                .AsNoTracking().FirstOrDefault();
        }

        public List<MonitoringCampaign> GetByNameAndCustomer(string name, string debtorNumber)
        {
            return context.MonitoringCampaigns.Where(c => c.CampaignName == name && c.DebtorNumber == debtorNumber).ToList();
        }

        public List<MonitoringCampaign> GetByNameAndUser(string name, int userId)
        {
            return context.MonitoringCampaigns.Where(c => c.CampaignName == name && c.UserId == userId).ToList();
        }

        public List<MonitoringCampaign> GetAll(int userId, List<int> otherUserIds)
        {
            return (from c in context.MonitoringCampaigns where c.UserId == userId || (otherUserIds.Contains(c.UserId) && !c.IsPrivate) select c)
                .Include(c => c.SearchCriteria)
                .Include(c => c.SearchCriteria.MediaTypes)
                .Include(c => c.Alerts)
                .AsNoTracking().ToList();
        }

        public List<MonitoringCampaign> GetAll()
        {
            return (from c in context.MonitoringCampaigns select c)
                .Include(c => c.SearchCriteria)
                .Include(c => c.SearchCriteria.MediaTypes)
                .AsNoTracking().ToList();
        }

        public void Update(MonitoringCampaign campaign)
        {
            MonitoringCampaign existingCampaign = context.MonitoringCampaigns.Where(c => c.Id == campaign.Id)
                                                        .Include(c => c.SearchCriteria)
                                                        .Include(c => c.SearchCriteria.MediaTypes)
                                                        .Include(c => c.Alerts).FirstOrDefault();

            var existingMediaTypes = context.MonitoringMediaTypes.ToList();

            if (campaign != null)
            {
                context.Entry(existingCampaign).CurrentValues.SetValues(campaign);
                context.Entry(existingCampaign.SearchCriteria).CurrentValues.SetValues(campaign.SearchCriteria);
                existingCampaign.SearchCriteria.MediaTypes.Clear();

                foreach (MonitoringMediaType mediaType in campaign.SearchCriteria.MediaTypes)
                {
                    existingCampaign.SearchCriteria.MediaTypes.Add(existingMediaTypes.Where(mt => mt.Id == mediaType.Id).FirstOrDefault());
                }
            }
            else
            {
                context.MonitoringCampaigns.Attach(campaign);
                context.Entry(campaign).State = System.Data.EntityState.Modified;
                context.Entry(campaign.SearchCriteria).State = System.Data.EntityState.Modified;
            }
        }

        public List<MonitoringCampaign> GetAllByAlertFrequency(int frequencyId)
        {
            return context.MonitoringCampaigns.Where(c => c.IsActive)
                                              .Where(c => c.Alerts.Any(a => a.FrequencyId == frequencyId))
                                              .Include(c => c.SearchCriteria)
                                              .Include(c => c.SearchCriteria.MediaTypes)
                                              .Include(c => c.Alerts)
                                              .ToList();
        }
    }
}
