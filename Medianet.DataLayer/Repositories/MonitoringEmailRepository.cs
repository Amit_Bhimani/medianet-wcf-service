﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class MonitoringEmailRepository
    {
        private MedianetContext context;

        public MonitoringEmailRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public void Add(MonitoringEmail email)
        {
            foreach (MonitoringMediaType mediaType in email.SearchCriteria.MediaTypes)
            {
                context.MonitoringMediaTypes.Attach(mediaType);
            }

            context.MonitoringEmails.Add(email);
        }

        public List<MonitoringEmail> GetAllByUser(int userId)
        {
            return (from c in context.MonitoringEmails where c.SearchCriteria.UserId == userId select c)
                .Include(c => c.SearchCriteria)
                .Include(c => c.SearchCriteria.MediaTypes)
                .AsNoTracking().ToList();
        }

        public List<MonitoringEmail> GetAllByCustomer(List<int> otherUserIds)
        {
            return (from c in context.MonitoringEmails where otherUserIds.Contains(c.SearchCriteria.UserId) select c)
                .Include(c => c.SearchCriteria)
                .Include(c => c.SearchCriteria.MediaTypes)
                .AsNoTracking().ToList();
        }
    }
}
