﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class MonitoringSearchRepository
    {
        private MedianetContext context;

        public MonitoringSearchRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public void Add(MonitoringSearch search)
        {
            foreach (MonitoringMediaType mediaType in search.MediaTypes)
            {
                context.MonitoringMediaTypes.Attach(mediaType);
            }

            context.MonitoringSearches.Add(search);
        }

        public MonitoringSearch GetById(int id)
        {
            return (from s in context.MonitoringSearches where s.Id == id select s).Include(s => s.MediaTypes).AsNoTracking().FirstOrDefault();
        }

    }
}
