﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class NewsFocusRepository
    {
        private MedianetContext context;

        public NewsFocusRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public NewsFocus Get(int id)
        {
            return (from e in context.NewsFocuses
                    where e.Id == id
                    select e).AsNoTracking().First();
        }

        public List<NewsFocus> GetAll()
        {
            return (from e in context.NewsFocuses
                    select e).AsNoTracking().ToList();
        }
    }
}
