﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;
using Medianet.DataLayer.Common;

namespace Medianet.DataLayer.Repositories
{
    public class NoteRepository
    {
        private MedianetContext context;

        private int _userId;
        private string _debtorNumber;

        public NoteRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public NoteRepository(UnitOfWork uow, string debtorNumber, int userId)
        {
            context = uow.Context;
            _debtorNumber = debtorNumber;
            _userId = userId;
        }

        public Note Get(int id)
        {
            return (from e in context.Notes.Include("User")
                    where e.Id == id
                    select e).AsNoTracking().First();
        }

        public void Delete(int id)
        {
            Note dbEntity = context.Notes.Where(e => e.Id == id).FirstOrDefault();

            if (dbEntity != null)
            {
                dbEntity.Status = 0;
                context.Entry(dbEntity).State = System.Data.EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void PinUnpin(int id,bool pinned) {
            Note dbEntity = context.Notes.Where(e => e.Id == id).FirstOrDefault();

            if (dbEntity != null)
            {
                dbEntity.Pinned = !pinned ;
                context.Entry(dbEntity).State = System.Data.EntityState.Modified;
                context.SaveChanges();
            }
        }

        public int Add(Note note)
        {
            context.Notes.Add(note);
            context.SaveChanges();
            return note.Id;
        }

        public void Update(Note note)
        {
            Note dbEntity = context.Notes.Where(e => e.Id == note.Id).FirstOrDefault();

            if (dbEntity != null)
            {
                context.Entry(dbEntity).Entity.IsPrivate = note.IsPrivate;
                context.Entry(dbEntity).Entity.Notes = note.Notes;
                context.Entry(dbEntity).Entity.Pinned = note.Pinned;
                context.Entry(dbEntity).Entity.UserId = note.UserId;
            }
            else
                context.Entry(dbEntity).State = System.Data.EntityState.Modified;

            context.SaveChanges();
        }

        public void AddBulk(List<Note> notes)
        {
            foreach (var note in notes)
            {
                context.Notes.Add(note);
            }

            context.SaveChanges();
        }

        public List<Note> GetPrnOutletNotes(long outletId)
        {
            var notes = this.FilterNotes();

            return (from n in notes
                    where n.PrnOutletId == outletId &&
                    (n.OmaContactId == null || n.OmaContactId == 0) &&
                    (n.PrnContactId == null || n.PrnContactId == 0)
                    select n).ToList();

        }

        public List<Note> GetPrnContactNotes(long outletId, long contactId)
        {
            var notes = this.FilterNotes();

            return (from n in notes
                    where n.PrnOutletId == outletId && n.PrnContactId == contactId
                    select n).ToList();
        }

        public List<Note> GetOmaOutletNotes(int outletId)
        {
            var notes = this.FilterNotes();

            return (from n in notes
                    where n.OmaOutletId == outletId &&
                    (n.OmaContactId == null || n.OmaContactId == 0)
                    select n).ToList();
        }

        public List<Note> GetOmaContactNotes(int contactId)
        {
            var notes = this.FilterNotes();

            return (from n in notes
                    where n.OmaContactId == contactId
                    select n).ToList();
        }

        public List<Note> GetMediaOutletNotes(string outletId)
        {
            var notes = this.FilterNotes();

            return (from n in notes
                    where n.MediaOutletId.Trim() == outletId.Trim() &&
                    n.MediaContactId == null &&
                    (n.PrnContactId == null || n.PrnContactId == 0) &&
                    (n.OmaContactId == null || n.OmaContactId == 0)
                    select n).ToList();
        }

        public List<Note> GetMediaContactNotes(string outletId, string contactId)
        {
            var notes = this.FilterNotes();

            return (from n in notes
                    where n.MediaOutletId.Trim() == outletId.Trim() &&
                    n.MediaContactId.Trim() == contactId.Trim()
                    select n).AsNoTracking().ToList();
        }

        private IQueryable<Note> FilterNotes()
        {
            IQueryable<int> users = (from u in context.Users
                               where u.DebtorNumber == _debtorNumber
                                     select u.Id);

            var notes = (from n in context.Notes.Include("User")
                         where n.Status == 1 && (n.User.Id == _userId || (users.Contains(n.User.Id) && n.IsPrivate == false))
                         select n);
            return notes;
        }
    }
}
