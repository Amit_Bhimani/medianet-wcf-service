﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;
using System.Data;
using Medianet.BusinessLayer.Common;

namespace Medianet.DataLayer.Repositories
{
    public class OmaDocumentRepository
    {
        private MedianetContext context;

        public OmaDocumentRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public List<OmaDocumentView> GetList(string outletId, string contactId, RecordType recordType, int userID, string debtorNumber)
        {
            IQueryable<int> users = (from u in context.Users
                                     where u.DebtorNumber == debtorNumber
                                     select u.Id);

            var data = (from c in context.OmaDocumentBelongsTo
                            .Include("OmaDocument")
                        where (c.OmaDocument.UploadedByUserId == userID || (c.OmaDocument.IsPrivate == false && users.Contains(c.OmaDocument.UploadedByUserId))) &&
                            c.OmaDocument.Status == true
                        select new OmaDocumentView
                        {
                            MediaContactId = c.MediaContactId,
                            MediaOutletId = c.MediaOutletId,
                            OmaContactId = c.OmaContactId,
                            OmaOutletId = c.OmaOutletId,
                            PrnContactId = c.PrnContactId,
                            PrnOutletId = c.PrnOutletId,

                            DocumentId = c.OmaDocument.Id,
                            IsPrivate = c.OmaDocument.IsPrivate,
                            Name = c.OmaDocument.Name,
                            Status = c.OmaDocument.Status,
                            UploadedByUserId = c.OmaDocument.UploadedByUserId,
                            CreatedDate = c.OmaDocument.CreatedDate,
                            DocumentType = c.OmaDocument.DocumentType
                        });

            switch (recordType)
            {
                case RecordType.MediaOutlet:
                    data = data.Where(d => d.MediaOutletId == outletId && d.MediaContactId == null);
                    break;

                case RecordType.MediaContact:
                    data = data.Where(d => d.MediaContactId == contactId && d.MediaOutletId == outletId);
                    break;

                case RecordType.PrnOutlet:
                    decimal _id = Convert.ToDecimal(outletId);
                    data = data.Where(d => d.PrnOutletId == _id && d.PrnContactId == null);
                    break;

                case RecordType.PrnContact:
                    decimal _id1 = Convert.ToDecimal(contactId);
                    decimal _id2 = Convert.ToDecimal(outletId);
                    data = data.Where(d => d.PrnContactId == _id1 && d.PrnOutletId == _id2);
                    break;

                case RecordType.OmaOutlet:
                    int _id3 = int.Parse(outletId);
                    data = data.Where(d => d.OmaOutletId == _id3 && d.OmaContactId == null);
                    break;
                case RecordType.OmaContactAtMediaOutlet:
                    int _id4 = int.Parse(contactId);
                    data = data.Where(d => d.OmaContactId == _id4 && d.MediaOutletId == outletId);
                    break;
                case RecordType.OmaContactNoOutlet:
                    int _id6 = int.Parse(contactId);
                    data = data.Where(d => d.OmaContactId == _id6 && d.OmaOutletId == null
                                      && d.MediaOutletId == null && d.PrnOutletId == null);
                    break;
                case RecordType.OmaContactAtPrnOutlet:
                    int _id7 = int.Parse(contactId);
                    decimal _id8 = Convert.ToDecimal(outletId);
                    data = data.Where(d => d.OmaContactId == _id7 && d.PrnOutletId == _id8);
                    break;
                case RecordType.OmaContactAtOmaOutlet:
                    int _id9 = int.Parse(contactId);
                    int _id10 = int.Parse(outletId);
                    data = data.Where(d => d.OmaContactId == _id9 && d.OmaOutletId == _id10);
                    break;
            }

            return data.ToList();
        }


        public OmaDocumentView GetById(int documentId)
        {
            return (from c in context.OmaDocument
                    where c.Id == documentId
                    select new OmaDocumentView
                    {
                        DocumentId = c.Id,
                        IsPrivate = c.IsPrivate,
                        Name = c.Name,
                        Status = c.Status,
                        UploadedByUserId = c.UploadedByUserId,
                        CreatedDate = c.CreatedDate,
                        DocumentType = c.DocumentType
                    }).FirstOrDefault();
        }

        public OmaDocument GetFile(int documentId)
        {
            return (from d in context.OmaDocument
                    where d.Id == documentId
                    select d).FirstOrDefault();
        }

        public void Save(OmaDocumentView model)
        {
            if (model.DocumentId > 0)
            {
                OmaDocument entity = context.OmaDocument.FirstOrDefault(d => d.Id == model.DocumentId);

                entity.UploadedByUserId = model.UploadedByUserId;
                entity.IsPrivate = model.IsPrivate;
                entity.Name = model.Name;

                context.Entry(entity).State = EntityState.Modified;
            }
            else
            {
                OmaDocumentBelongsTo newEntity = new OmaDocumentBelongsTo
                {
                    CreatedDate = DateTime.Now,
                    Status = true,
                    MediaContactId = model.MediaContactId,
                    MediaOutletId = model.MediaOutletId,
                    OmaContactId = model.OmaContactId,
                    OmaOutletId = model.OmaOutletId,
                    PrnContactId = model.PrnContactId,
                    PrnOutletId = model.PrnOutletId,
                    OmaDocument = new OmaDocument
                    {
                        Name = model.Name,
                        IsPrivate = model.IsPrivate,
                        CreatedDate = DateTime.Now,
                        Status = true,
                        UploadedByUserId = model.UploadedByUserId,
                        DocumentType = model.DocumentType,
                        DocumentContent = model.DocumentContent
                    }
                };

                context.OmaDocumentBelongsTo.Add(newEntity);
            }

            context.SaveChanges();
        }

        public void Delete(int documentId, int userId, string debtorNumber)
        {
            IEnumerable<int> userIds = (from u in context.Users
                                        where u.DebtorNumber == debtorNumber
                                        select u.Id).AsEnumerable<int>();

            OmaDocument entity = (from d in context.OmaDocument
                                  where d.Id == documentId &&
                                        (d.UploadedByUserId == userId || (d.IsPrivate == false && userIds.Contains(userId)))
                                  select d).First();

            entity.Status = false;

            context.Entry(entity).State = EntityState.Modified;

            context.SaveChanges();
        }
    }
}
