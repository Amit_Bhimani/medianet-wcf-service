﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class OutletTypeRepository
    {
        private MedianetContext context;

        public OutletTypeRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public OutletType Get(int id)
        {
            return (from e in context.OutletTypes
                    where e.Id == id
                    select e).AsNoTracking().First();
        }

        public List<OutletType> GetAll()
        {
            return (from e in context.OutletTypes
                    select e).AsNoTracking().ToList();
        }

    }
}
