﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class PackageAttachmentRepository
    {
        private MedianetContext context;

        public PackageAttachmentRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }
        
        public PackageAttachment GetByPackageId(int packageId)
        {
            return (from e in context.PackageAttachments
                    where e.PackageId == packageId
                    select e).AsNoTracking().FirstOrDefault();
        }
    }
}
