﻿using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class PackageListRepository
    {
        private MedianetContext context;

        public PackageListRepository(UnitOfWork uow) {
            context = uow.Context;
        }

        public PackageList GetById(int id)
        {
            return (from pl in context.PackageLists
                    where pl.Id == id
                    && pl.RowStatus == Common.Constants.ROWSTATUS_ACTIVE
                    select pl).AsNoTracking().FirstOrDefault();
        }

        public List<PackageList> GetTopValue(string debtorNumber)
        {
            return (from pl in context.PackageLists
                    where pl.RowStatus == Common.Constants.ROWSTATUS_ACTIVE
                    && pl.IsHidden == "F"
                    && pl.DebtorNumber == debtorNumber
                    && (pl.IsTopValue.HasValue ? pl.IsTopValue.Value : false)
                    select pl).AsNoTracking().ToList();
        }

        public List<PackageList> GetAllByDebtorNumberSystem(string debtorNumber, string system)
        {
            return (from pl in context.PackageLists
                    where pl.DebtorNumber == debtorNumber
                    && pl.System == system
                    && pl.IsHidden == "F"
                    && pl.RowStatus == Common.Constants.ROWSTATUS_ACTIVE
                    select pl).AsNoTracking().ToList();
        }

        public List<PackageList> GetAllByDebtorNumberAccountCode(string debtorNumber, string accountCode)
        {
            // Get all the packages owned by this DebtorNumber with the provided accountcode.
            return (from pl in context.PackageLists
                    where pl.DebtorNumber == debtorNumber
                    && pl.AccountCode == accountCode
                    && pl.RowStatus == Common.Constants.ROWSTATUS_ACTIVE
                    select pl).AsNoTracking().ToList();
        }

        public List<ListTicker> GetForTicker(string debtorNumber, string system)
        {
            // Get all the packages by Category1Id.
            var dbEntities = (from c in context.ListCategories
                          join pc in context.ListCategories on c.ParentId equals pc.Id
                          join pl in context.PackageLists on c.Id equals pl.Category1Id
                          where c.IsHidden == "F"
                          && pc.IsHidden == "F"
                          && c.IsRadioListCategory == false
                          && pl.DebtorNumber == debtorNumber
                          && pl.System == system
                          && pl.IsHidden == "F"
                          select new Model.Entities.ListTicker()
                          {
                              ListCategoryId = c.Id,
                              ListCategoryParentSequenceNumber = pc.Id,
                              ListCategorySequenceNumber = c.SequenceNumber,
                              ListCategoryName = c.Name,
                              ListId = pl.Id,
                              ListSequenceNumber = pl.SequenceNumber,
                              ListDistributionType = "P",
                              ListDescription = pl.SelectionDescription
                          })
                          .AsNoTracking().ToList();

            // Get all the packages by Category2Id.
            dbEntities.AddRange((from c in context.ListCategories
                                 join pc in context.ListCategories on c.ParentId equals pc.Id
                                 join pl in context.PackageLists on c.Id equals pl.Category2Id
                                 where c.IsHidden == "F"
                                 && pc.IsHidden == "F"
                                 && c.IsRadioListCategory == false
                                 && pl.DebtorNumber == debtorNumber
                                 && pl.System == system
                                 && pl.IsHidden == "F"
                                 select new Model.Entities.ListTicker()
                                 {
                                     ListCategoryId = c.Id,
                                     ListCategoryParentSequenceNumber = pc.Id,
                                     ListCategorySequenceNumber = c.SequenceNumber,
                                     ListCategoryName = c.Name,
                                     ListId = pl.Id,
                                     ListSequenceNumber = pl.SequenceNumber,
                                     ListDistributionType = "P",
                                     ListDescription = pl.SelectionDescription
                                 })
                                 .AsNoTracking().ToList());

            return dbEntities;
        }

        public ListComment GetCommentById(int id)
        {
            return (from c in context.ListComments
                    where c.PackageId == id
                    select c).AsNoTracking().FirstOrDefault();
        }

        public List<ServiceList> GetServicesById(int id)
        {
            // Get all the services owned by this DebtorNumber.
            return (from x in context.PackageServiceXrefs
                    join sl in context.ServiceLists on x.ServiceId equals sl.Id
                    where x.PackageId == id
                    select sl).AsNoTracking().ToList();
        }

        public List<int> GetServiceIdsById(int id)
        {
            return (from p in context.PackageServiceXrefs
                    where p.PackageId == id
                    select p.ServiceId).ToList();
        }

        public int Add(PackageList entity)
        {
            context.PackageLists.Add(entity);
            context.SaveChanges();

            return entity.Id;
        }

        public void Update(PackageList entity)
        {
            context.Entry(entity).State = System.Data.EntityState.Modified;
        }

        public void UpdateInNopCommerce(PackageList entity)
        {
            var idParam = new SqlParameter("@PackageId", entity.Id);
            var descParam = new SqlParameter("@SelectionDescription", entity.SelectionDescription);
            var introParam = new SqlParameter("@Introduction", entity.Introduction);
            var cat1Param = new SqlParameter("@Category1", entity.Category1Id.HasValue ? entity.Category1Id : 0);
            var cat2Param = new SqlParameter("@Category2", entity.Category2Id.HasValue ? entity.Category2Id : 0);
            var hiddenParam = new SqlParameter("@Hidden", entity.IsHidden);

            context.Database.ExecuteSqlCommand("exec sp_UpdatePackageInNopcommerce @PackageId, @SelectionDescription, @Introduction, @Category1, @Category2, @Hidden", idParam, descParam, introParam, cat1Param, cat2Param, hiddenParam);
        }
    }
}
