﻿using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Text;
using Medianet.BusinessLayer.Common;
using Medianet.Model;
using Medianet.Model.Entities;

namespace Medianet.DataLayer.Repositories
{
    public class PrnContactViewRepository
    {
        private readonly MedianetContext _context;
        public PrnContactViewRepository(UnitOfWork uow)
        {
            _context = uow.Context;

        }
        public void Add(PrnContactView entity)
        {
            _context.PrnContactView.Add(entity);
        }
        public List<MediaContactAlsoViewed> GetPrnContactViewSummary(string contactId, string outletId, List<int> subjectCodeIds, List<int> continentIds, int maxCount)
        {

            decimal contact;
            decimal.TryParse(contactId, out contact);

            decimal outlet;
            decimal.TryParse(outletId, out outlet);

            var summary = _context.PrnContactViewSummary.Where(
                s => s.ContactId != contact && s.OutletId != outlet
                && continentIds.Contains(s.ContinentId)
                && subjectCodeIds.Contains(s.SubjectCodeId)).GroupBy(g => new { g.ContactId, g.OutletId })
                .Select(g => new { Key = g.Key, ViewCount = g.Sum(s => s.ViewCount) })
                .OrderByDescending(s => s.ViewCount).Take(maxCount).ToList();


            var alsoViewed = summary.Select(vl => (from e in _context.MediaPrnEmployees
                                                   join c in _context.MediaPrnContacts on e.ContactId equals c.ContactId
                                                   join o in _context.MediaPrnOutlets on e.OutletId equals o.OutletId
                                                   where e.ContactId == vl.Key.ContactId && e.OutletId == vl.Key.OutletId
                                                   select new MediaContactAlsoViewed()
                                                   {
                                                       ContactId = SqlFunctions.StringConvert(vl.Key.ContactId),
                                                       OutletId = SqlFunctions.StringConvert(vl.Key.OutletId),
                                                       FirstName = c.FirstName,
                                                       LastName = c.LastName,
                                                       RecordType = (int)RecordType.PrnContact,
                                                       ViewCount = vl.ViewCount,
                                                       Title = e.JobTitle,
                                                       OutletName = o.Name,
                                                       IsGeneric = false,
                                                       TwitterHandle = string.Empty
                                                   }).Single()).ToList();

            return alsoViewed.Any() ? alsoViewed : null;
        }
    }
}
