﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class ProductTypeRepository
    {
        private MedianetContext context;

        public ProductTypeRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public ProductType Get(int id)
        {
            return (from e in context.ProductTypes
                    where e.Id == id
                    select e).AsNoTracking().First();
        }

        public List<ProductType> GetAll()
        {
            return (from e in context.ProductTypes
                    select e).AsNoTracking().ToList();
        }

        public List<ProductType> GetAllByOutletType(int outletTypeId)
        {
            return (from e in context.ProductTypes
                    join m in context.MediumTypes on e.Id equals m.ProductTypeId
                    where m.OutletTypeId == outletTypeId
                    select e).AsNoTracking().ToList();
        }
    }
}
