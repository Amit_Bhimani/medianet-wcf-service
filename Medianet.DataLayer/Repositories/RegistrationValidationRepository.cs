﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class RegistrationValidationRepository
    {
        private MedianetContext context;

        public RegistrationValidationRepository(UnitOfWork uow) {
            context = uow.Context;
        }

        public Guid Add(RegistrationValidation entity) {
            context.Registrations.Add(entity);
            context.SaveChanges();

            return entity.Token;
        }

        public RegistrationValidation Get(Guid token) {
            return (from r in context.Registrations.Include("ReleaseUnverified")
                    where r.Token == token
                    select r).AsNoTracking().FirstOrDefault();
        }

        public RegistrationValidation GetByEmail(string email)
        {
            return (from r in context.Registrations.Include("ReleaseUnverified")
                    where r.EmailAddress == email
                    select r).AsNoTracking().FirstOrDefault();
        }
    }
}
