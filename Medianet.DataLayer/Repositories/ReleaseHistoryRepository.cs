﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class ReleaseHistoryRepository
    {
        private MedianetContext context;

        public ReleaseHistoryRepository(UnitOfWork uow) {
            context = uow.Context;
        }

        public int Add(ReleaseHistory entity) {
            context.ReleaseHistories.Add(entity);
            context.SaveChanges();

            return entity.ReleaseId;
        }

        public void Update(ReleaseHistory entity) {
            context.Entry(entity).State = System.Data.EntityState.Modified;
        }

        public void Delete(int id) {
            ReleaseHistory entity = new Model.Entities.ReleaseHistory() { ReleaseId = id };
            context.Entry(entity).State = System.Data.EntityState.Deleted;
        }

        public void DeleteByReleaseId(int releaseId) {
            List<Model.Entities.ReleaseHistory> entities = null;

            entities = (from rh in context.ReleaseHistories
                        where rh.ReleaseId == releaseId
                        select rh).ToList();

            foreach (Model.Entities.ReleaseHistory rh in entities) {
                context.Entry(rh).State = System.Data.EntityState.Deleted;
            }
        }

        public ReleaseHistory GetById(int id) {
            List<Model.Entities.ReleaseHistory> entities = null;

            entities = (from rh in context.ReleaseHistories
                        where rh.ReleaseId == id
                        select rh).AsNoTracking().ToList();

            if (entities == null || entities.Count < 1)
                return null;

            return entities[0];
        }

        public List<ReleaseHistory> GetAll() {
            throw new NotImplementedException();
        }

        public List<ReleaseHistory> GetAllByReleaseId(int releaseId) {
            return (from rh in context.ReleaseHistories
                    where rh.ReleaseId == releaseId
                    select rh).AsNoTracking()
                    .OrderBy(x => x.CreatedDate).ToList();
        }
    }
}
