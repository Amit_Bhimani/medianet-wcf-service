﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class ReleaseRepository
    {
        private MedianetContext context;

        public enum SortOrder
        {
            CreatedDate = 0,
            DistributedDate,
            ReleaseDescription,
            CustomerReference,
            Priority
        }

        public ReleaseRepository(UnitOfWork uow) {
            context = uow.Context;
        }

        public int Add(Release entity)
        {
            context.Releases.Add(entity);
            context.SaveChanges();

            return entity.Id;
        }

        //public void Update(Release entity) {
        //    context.Entry(entity).State = System.Data.EntityState.Modified;
        //}

        public void Delete(int id)
        {
            Release dbEntity = new Release() { Id = id };
            context.Entry(dbEntity).State = System.Data.EntityState.Deleted;
            context.SaveChanges();
        }

        public Release GetById(int id) {
            List<Model.Entities.Release> entities = null;

            entities = (from r in context.Releases.Include("CreatedByUser").Include("WebDetails")
                        where r.Id == id
                        select r).AsNoTracking().ToList();

            if (entities == null || entities.Count < 1)
                return null;

            // Many Releases have a CreatedByUserId of 0 instead of null. If 0 then set it to null.
            if (entities[0].CreatedByUserId == 0) {
                entities[0].CreatedByUserId = null;
                entities[0].CreatedByUser = null;
            }

            return entities[0];
        }

        public Release GetByIdWithDocuments(int id) {
            return (from r in context.Releases.Include("WebDetails").Include("Documents").Include("Documents.Thumbnails")
                    where r.Id == id
                    select r).AsNoTracking().FirstOrDefault();
        }

        public List<Release> GetAllByDebtorNumber(string debtorNo, string system, List<string> statuses, int sortOrder, int startPos, int pageSize) {
            List<Model.Entities.Release> entities = null;

            var dbQuery = (from r in context.Releases.Include("CreatedByUser").Include("WebDetails")
                           where r.DebtorNumber == debtorNo
                           && r.System == system
                           && statuses.Contains(r.Status)
                           select r);

            switch (sortOrder) {
                case  1: // DistributedDate:
                    dbQuery = dbQuery.OrderByDescending(x => x.DistributedDate);
                    break;
                case 2: // ReleaseDescription:
                    dbQuery = dbQuery.OrderBy(x => x.ReleaseDescription).ThenByDescending(x => x.Id);
                    break;
                case 3: // CustomerReference:
                    dbQuery = dbQuery.OrderBy(x => x.CustomerReference).ThenByDescending(x => x.Id);
                    break;
                case 4 : // Priority:
                    dbQuery = dbQuery.OrderByDescending(x => x.Priority).ThenByDescending(x => x.Id);
                    break;
                default:
                    dbQuery = dbQuery.OrderByDescending(x => x.CreatedDate);
                    break;
            }

            if (startPos > 0)
                dbQuery = dbQuery.Skip(startPos);

            entities = dbQuery.Take(pageSize).AsNoTracking().ToList();

            // Many Releases have a CreatedByUserId of 0 instead of null. If 0 then set it to null.
            foreach (Model.Entities.Release release in entities) {
                if (release.CreatedByUserId == 0)
                    release.CreatedByUser = null;
            }

            return entities;
        }

        public int GetCountByDebtorNumber(string debtorNo, string system, List<string> statuses) {
            return (from r in context.Releases
                    where r.DebtorNumber == debtorNo
                    && r.System == system
                    && statuses.Contains(r.Status)
                    select r).Count();
        }

        public void OpenRelease(int id, string status, int userId) {
            Release entity = (from r in context.Releases where r.Id == id select r).First();

            if (entity.IsOpen == 1)
                throw new Exception(string.Format("Release {0} is already open.", id));

            if (entity.Status == status) {
                entity.IsOpen = 1;
                entity.OpenedDate = DateTime.Now;
                context.SaveChanges();
            }
            else
                throw new Exception(string.Format("Unable to open Release {0}. The status has changed from {1} to {2}.", id, entity.Status, status));
        }

        public void CloseRelease(int id) {
            Release entity = (from r in context.Releases where r.Id == id select r).First();

            if (entity.IsOpen == 1) {
                entity.IsOpen = 0;
                context.SaveChanges();
            }
        }

        public List<FeatureRelease> GetAllFeatureReleases(int? webCategoryId) {
            if (webCategoryId.HasValue)
                return (from f in context.FeatureReleases.Include("Release").Include("Release.WebDetails")
                        where f.WebCategoryId == webCategoryId
                        select f).AsNoTracking().ToList();
            else
                return (from f in context.FeatureReleases.Include("Release").Include("Release.WebDetails")
                        where f.WebCategoryId == null
                        select f).AsNoTracking().ToList();
        }

        public List<ReleaseSummaryPublic> GetAllPublicByCriteria(DateTime fromDate, DateTime toDate, int? categoryId, string searchText, string type,
            bool showVideos, bool showAudios, bool showPhotos, bool showAttachments, bool showMultimedia, int pageNumber, int pageSize, bool showUnverified, bool isJournalistsWebsite)
        {

            var fromDateParam = new SqlParameter("@FromDate", fromDate.ToString("dd MMM yyyy"));
            var toDateParam = new SqlParameter("@ToDate", toDate.ToString("dd MMM yyyy"));
            var categoryIdParam = new SqlParameter("@CategoryID", categoryId.HasValue ? categoryId.Value : 0);
            var searchTextParam = new SqlParameter("@Keywords", searchText == null ? string.Empty : searchText);
            var multimediaTypeParam = new SqlParameter("@MultimediaType", type);
            var showVideosParam = new SqlParameter("@ShowVideos", showVideos);
            var showAudiosParam = new SqlParameter("@ShowAudios", showAudios);
            var showPhotosParam = new SqlParameter("@ShowPhotos", showPhotos);
            var showAttachmentsParam = new SqlParameter("@ShowAttachments", showAttachments);
            var showMultimediaParam = new SqlParameter("@ShowMultimedia", showMultimedia);
            var pageNumberParam = new SqlParameter("@Page", pageNumber);
            var pageSizeParam = new SqlParameter("@ItemsPerPage", pageSize);
            var showUnverifiedParam = new SqlParameter("@ShowUnverified", showUnverified);
            var isJournalistsWebsiteParam = new SqlParameter("@IsMNJ", isJournalistsWebsite);

            var entities = context.Database.SqlQuery<ReleaseSummaryPublic>("exec sp_GetWebReleases @FromDate, @ToDate, @CategoryID, @Keywords, @MultimediaType, @ShowVideos, @ShowAudios, @ShowPhotos, @ShowAttachments, @ShowMultimedia, @Page, @ItemsPerPage, @ShowUnverified, @IsMNJ",
                fromDateParam, toDateParam, pageSizeParam, pageNumberParam, categoryIdParam, searchTextParam, multimediaTypeParam, showVideosParam, showAudiosParam, showPhotosParam, showAttachmentsParam, showMultimediaParam, showUnverifiedParam, isJournalistsWebsiteParam);

            return entities.ToList();
        }

        public void UpdateDebtorNumberByUserId(int userId, string debtorNumber) {
            var userIdParam = new SqlParameter("@UserId", userId);
            var debtorNumberParam = new SqlParameter("@DebtorNumber", debtorNumber);

            context.Database.ExecuteSqlCommand("exec sp_UpdateReleasesDebtorNumber @UserId, @DebtorNumber", userIdParam, debtorNumberParam);
        }

        public void RecordView(int id, string clientIp, string userAgent, string website)
        {
            context.Database.ExecuteSqlCommand(string.Format("insert into mn_download_log (Timestamp, ClientIP, JobID, Attachment, DocumentId, UserAgent, SystemType) values ('{0}', '{1}', {2}, null, null, '{3}', '{4}')", 
                DateTime.Now.ToString("dd MMMM yyyy HH:mm:ss.fff"), clientIp, id, userAgent, website));
        }

        public void CancelRelease(int id, int userId)
        {
            var releaseIdParam = new SqlParameter("@jobid", id);
            var userIdParam = new SqlParameter("@UserID", userId);

            context.Database.ExecuteSqlCommand("exec sp_Canceller_CancelJob @jobid, @UserID", releaseIdParam, userIdParam);
        }
    }
}
