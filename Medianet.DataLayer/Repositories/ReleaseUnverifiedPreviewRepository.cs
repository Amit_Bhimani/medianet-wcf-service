﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class ReleaseUnverifiedPreviewRepository
    {
        private MedianetContext context;

        public ReleaseUnverifiedPreviewRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public void Add(ReleaseUnverifiedPreview entity)
        {
            context.ReleaseUnverifiedPreviews.Add(entity);
            context.SaveChanges();
        }

        public ReleaseUnverifiedPreview Get(Guid token)
        {
            return (from r in context.ReleaseUnverifiedPreviews
                    where r.Token == token
                    select r).AsNoTracking().FirstOrDefault();
        }
    }
}
