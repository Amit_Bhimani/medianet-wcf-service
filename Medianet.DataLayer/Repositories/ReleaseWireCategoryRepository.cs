﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class ReleaseWireCategoryRepository
    {
        private MedianetContext context;

        public ReleaseWireCategoryRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public int Add(ReleaseWireCategory entity)
        {
            context.ReleaseWireCategories.Add(entity);
            context.SaveChanges();

            return entity.Id;
        }

    }
}
