﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class ResultRepository : IRepository<Result>
    {
        public const string STATUS_Resulted = "G";

        private MedianetContext context;

        public ResultRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public int Add(Result entity)
        {
            throw new NotImplementedException();
        }

        public void Update(Result entity)
        {
            context.Entry(entity).State = System.Data.EntityState.Modified;
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void DeleteByReleaseId(int releaseId)
        {
            List<Model.Entities.Result> entities = null;

            entities = (from r in context.Results
                        where r.ReleaseId == releaseId
                        select r).ToList();

            foreach (Model.Entities.Result r in entities)
            {
                context.Entry(r).State = System.Data.EntityState.Deleted;
            }
        }

        public Result GetById(int id)
        {
            throw new NotImplementedException();
        }

        public List<Result> GetAll()
        {
            throw new NotImplementedException();
        }

        public List<Result> GetAllByReleaseIdTransactionId(int releaseId, int transactionId)
        {
            var idParam1 = new SqlParameter("@JobId", releaseId);
            var idParam2 = new SqlParameter("@TransId", transactionId);

            var entities = context.Database.SqlQuery<Result>("select r.JobId as ReleaseId,r.GroupNumber,r.TransactionId,r.DistributionId,isnull(o.CompanyName, r.Recipient) as Recipient,r.Address,r.Status,r.ErrorCode,r.ErrorMessage,r.TransmitTime as TransmittedTime,r.CreatedTime as CreatedDate,r.SendDuration,r.OpenCount from mn_results r left outer join mn_service_distribution d on d.DistributionId = r.DistributionId left outer join mn_media_outlet o on o.OutletId = d.OutletId where r.jobid=@JobId and r.TransactionId=@TransId", idParam1, idParam2);
            //return (from r in context.Results.Include("Distribution").Include("Distribution.MediaOutlet")
            //        where r.ReleaseId == releaseId
            //        && r.TransactionId == transactionId
            //        select r).AsNoTracking()
            //        .OrderBy(x => x.GroupNumber).ThenBy(x => x.TransactionId).ThenBy(x => x.CreatedDate).ToList();
            return entities.ToList();
        }

        public void UpdateOpenCount(int releaseId, int transactionId)
        {
            // Entity framework can't track objects with a null in the primary key so call a stored proc instead.
            context.Database.ExecuteSqlCommand(string.Format("update mn_results set OpenCount = OpenCount + 1 where JobId = {0} and TransactionId = {1} and DistributionId is null",
                releaseId, transactionId));
        }

        public void UpdateOpenCountByDistributionId(int releaseId, int transactionId, int distributionId)
        {
            Result dbEntity;

            dbEntity = (from r in context.Results
                        where r.ReleaseId == releaseId
                        && r.TransactionId == transactionId
                        && r.DistributionId == distributionId
                        select r).FirstOrDefault();

            if (dbEntity != null)
            {
                dbEntity.OpenCount += 1;
                context.SaveChanges();
            }
        }

        public bool UpdateStatus(Result r)
        {
            var entity = context.Database.SqlQuery<object>(
               string.Format(@"select r.JobId from mn_results r where r.jobid=@JobId and r.TransactionId=@TransId {0}", r.DistributionId.HasValue ? " and DistributionId = @DistributionId" : ""),
                                   new SqlParameter("@JobId", r.ReleaseId),
                                   new SqlParameter("@TransId", r.TransactionId),
                                   new SqlParameter("@DistributionId", (object)r.DistributionId ?? DBNull.Value)).FirstOrDefault();

            if (entity == null)
                return false;

            string query = string.Format(@"
If Not Exists (Select JobId From mn_results Where Status = '{1}' and ErrorCode = '' and JobId = @JobId and TransactionId = @TransactionId {0})

update mn_results set Status = @Status, ErrorCode = @ErrorCode, ErrorMessage = @ErrorMessage, SendDuration = @SendDuration, TransmitTime = @TransmitTime
       where JobId = @JobId and TransactionId = @TransactionId {0}", r.DistributionId.HasValue ? " and DistributionId = @DistributionId" : "", STATUS_Resulted);

            context.Database.ExecuteSqlCommand(query,
                                    new SqlParameter("@Status", r.Status), 
                                    new SqlParameter("@ErrorCode", r.ErrorCode),
                                    new SqlParameter("@ErrorMessage", r.ErrorMessage),
                                    new SqlParameter("@SendDuration", r.SendDuration),
                                    new SqlParameter("@JobId", r.ReleaseId),
                                    new SqlParameter("@TransactionId", r.TransactionId),
                                    new SqlParameter("@TransmitTime", r.TransmittedTime),
                                    new SqlParameter("@DistributionId", (object)r.DistributionId ?? DBNull.Value));


            return true;
        }
    }
}
