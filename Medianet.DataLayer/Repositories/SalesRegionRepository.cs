﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    // Note: Currently this repository doesn't support adding the full graph in one go
    // so whenever adding anything we need to do a SaveChanges() to get the Identity value
    // so we can then use this when inserting other objects (like Recipients).
    // This should be fixed at some point so all saving is done by the UnitOfWork.
    public class SalesRegionRepository : IRepository<SalesRegion>
    {
        private MedianetContext context;

        public SalesRegionRepository(UnitOfWork uow) {
            context = uow.Context;
        }

        public int Add(SalesRegion entity)
        {
            context.SalesRegions.Add(entity);
            context.SaveChanges();

            return entity.Id;
        }

        public void Update(SalesRegion entity)
        {
            //context.Entry(entity).State = System.Data.EntityState.Modified;
            List<Model.Entities.SalesRegion> dbEntities = context.SalesRegions.Local.Where(s => s.Id == entity.Id).ToList();

            // If the items is already being tracked then update it's values. Otherwise add it as modified.
            if (dbEntities.Count() > 0)
                context.Entry(dbEntities[0]).CurrentValues.SetValues(entity);
            else
                context.Entry(entity).State = System.Data.EntityState.Modified;

            context.SaveChanges();
        }

        public void Delete(int id) {
            SalesRegion entity = new Model.Entities.SalesRegion() { Id = id };
            context.Entry(entity).State = System.Data.EntityState.Deleted;
        }

        public List<SalesRegion> GetAll()
        {
            return (from sr in context.SalesRegions
                    select sr).AsNoTracking().ToList();
        }

        public SalesRegion GetById(int id)
        {
            return (from sr in context.SalesRegions
                    where sr.Id == id
                    select sr).AsNoTracking().FirstOrDefault();
        }

        public SalesRegion GetByName(string name)
        {
            return (from sr in context.SalesRegions
                    where sr.Name == name
                    select sr).AsNoTracking().FirstOrDefault();
        }
    }
}