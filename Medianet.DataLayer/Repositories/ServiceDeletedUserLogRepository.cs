﻿using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using System;

namespace Medianet.DataLayer.Repositories
{
    public class ServiceDeletedUserLogRepository
    {
        private MedianetContext context;

        public ServiceDeletedUserLogRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public List<ServiceDeletedUserLogView> GetList(string outletId, string contactId)
        {
            if (!string.IsNullOrEmpty(contactId))
               return (from log in context.ServiceDeletedUserLog
                        join service in context.ServiceLists on log.ServiceId equals service.Id
                        join user in context.Users on log.DeletedByUserId equals user.Id
                        where log.OutletId == outletId && log.ContactId == contactId
                        orderby log.DeletedTime descending
                        select new ServiceDeletedUserLogView
                        {
                            Id = log.Id,
                            DeletedTime = log.DeletedTime,
                            ServiceName = service.SelectionDescription,
                            DeletedByUser = user.FirstName + " " + user.LastName
                        }).ToList();

            return (from log in context.ServiceDeletedUserLog
                    join service in context.ServiceLists on log.ServiceId equals service.Id
                    join user in context.Users on log.DeletedByUserId equals user.Id
                    where log.OutletId == outletId
                    orderby log.DeletedTime descending
                    select new ServiceDeletedUserLogView
                    {
                        Id = log.Id,
                        DeletedTime = log.DeletedTime,
                        ServiceName = service.SelectionDescription,
                        DeletedByUser = user.FirstName + " " + user.LastName
                    }).ToList();
        }

        public void Add(int serviceId, string outletId, string contactId, int? childServiceId, int deletedByUserId)
        {
            context.Database.ExecuteSqlCommand("exec sp_AddServiceDeletedUserLog @ServiceId, @OutletId, @ContactId, @ChildServiceId, @DeletedByUserId", 
                new SqlParameter("@ServiceId", serviceId),
                new SqlParameter("@OutletId", (object)outletId ?? DBNull.Value),
                new SqlParameter("@ContactId", (object)contactId ?? DBNull.Value),
                new SqlParameter("@ChildServiceId", (object)childServiceId ?? DBNull.Value),
                new SqlParameter("@DeletedByUserId", deletedByUserId));
        }
    }
}

