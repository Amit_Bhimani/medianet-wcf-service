﻿using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    // Note: Currently this repository doesn't support adding the full graph in one go
    // so whenever adding anything we need to do a SaveChanges() to get the Identity value
    // so we can then use this when inserting other objects (like Recipients).
    // This should be fixed at some point so all saving is done by the UnitOfWork.
    public class ServiceListRepository : IRepository<ServiceList>
    {
        private MedianetContext context;

        public ServiceListRepository(UnitOfWork uow) {
            context = uow.Context;
        }

        public ServiceList GetById(int id) {
            List<ServiceList> entities = null;

            // Get the Service details.
            entities = (from sl in context.ServiceLists
                      where sl.Id == id
                      select sl).AsNoTracking().ToList();

            if (entities == null || entities.Count < 1)
                return null;

            return entities[0];
        }

        public int Add(ServiceList entity) {
            context.ServiceLists.Add(entity);
            context.SaveChanges();

            return entity.Id;
        }

        public void Update(ServiceList entity) {
            context.Entry(entity).State = System.Data.EntityState.Modified;
        }

        public void UpdateInNopCommerce(int id)
        {
            var idParam = new SqlParameter("@ServiceId", id);

            context.Database.ExecuteSqlCommand("exec sp_UpdateServiceInNopcommerce @ServiceId", idParam);
        }

        public void Delete(int id) {
            ServiceList entity = new Model.Entities.ServiceList() { Id = id };
            context.Entry(entity).State = System.Data.EntityState.Deleted;
        }

        public List<ServiceList> GetAll() {
            throw new NotImplementedException();
        }

        public List<ServiceList> GetAllByDebtorNumber(string debtorNumber, string system) {
            // Get all the services owned by this DebtorNumber.
            return (from sl in context.ServiceLists
                    where sl.DebtorNumber == debtorNumber
                    && sl.System == system
                    //&& sl.IsHidden == "F"
                    select sl).AsNoTracking().ToList();
        }

        public List<ServiceListSummary> GetAllByDebtorNumberDistributionType(string debtorNumber, string distType, string system, int startPos, int pageSize)
        {
            List<Model.Entities.ServiceListSummary> entities = null;

            // Get all the services owned by this DebtorNumber.
            var dbQuery = (from sl in context.ServiceLists
                           where sl.DebtorNumber == debtorNumber
                                 && sl.System == system
                                 && sl.DistributionType == distType
                           //&& sl.IsHidden == "F"
                           select sl);

            if (startPos > 0)
                dbQuery = dbQuery.Skip(startPos);
            
            dbQuery = dbQuery.Take(pageSize).AsNoTracking();
            
            entities = (from sl in dbQuery 
                    select new Model.Entities.ServiceListSummary() {
                        Id = sl.Id,
                        SelectionDescription = sl.SelectionDescription,
                        DistributionType = sl.DistributionType,
                        SequenceNumber = sl.SequenceNumber,
                        ListNumber = sl.ListNumber
                    }).ToList();

            return entities;
        }

        public List<ServiceList> GetAllByDebtorNumberAccountCode(string debtorNumber, string accountCode)
        {
            // Get all the services owned by this DebtorNumber with the provided accountcode.
            return (from sl in context.ServiceLists
                    where sl.DebtorNumber == debtorNumber
                    && sl.AccountCode == accountCode
                    select sl).AsNoTracking().ToList();
        }

        public int GetCountByDebtorNumberDistributionType(string debtorNumber, string distType, string system)
        {
            // Get all the services owned by this DebtorNumber.
            return (from sl in context.ServiceLists
                           where sl.DebtorNumber == debtorNumber
                                 && sl.System == system
                                 && sl.DistributionType == distType
                           //&& sl.IsHidden == "F"
                           select sl).Count();
        }

        public ServiceList GetByDebtorNumberDistributionTypeListNumber(string debtorNumber, string distType, int listNumber, string system) {
            List<ServiceList> entities = null;

            // Get all the services owned by this DebtorNumber.
            entities = (from sl in context.ServiceLists
                          where sl.DebtorNumber == debtorNumber
                          && sl.System == system
                          && sl.DistributionType == distType
                          && sl.ListNumber == listNumber
                          select sl).AsNoTracking().ToList();

            if (entities == null || entities.Count < 1)
                return null;

            return entities[0];
        }

        public List<ServiceList> GetAllByDebtorNumberOutletId(string debtorNumber, string outletId, string system) {
            // Get all the services owned by this DebtorNumber referencing the outlet specified.
            return (from sl in context.ServiceLists
                    join sd in context.ServiceListDistributions on sl.Id equals sd.ServiceId
                    where sl.DebtorNumber == debtorNumber
                    && sl.System == system
                    && sd.OutletId == outletId
                    select sl).AsNoTracking().ToList();
        }

        public List<ServiceList> GetAllByDebtorNumberOutletIdContactId(string debtorNumber, string outletId, string contactId, string system) {
            // Get all the services owned by this DebtorNumber referencing the outlet and contact specified.
            if (contactId == null)
                return (from sl in context.ServiceLists
                        join sd in context.ServiceListDistributions on sl.Id equals sd.ServiceId
                        where sl.DebtorNumber == debtorNumber
                        && sl.System == system
                        && sd.OutletId == outletId
                        && sd.ContactId == null
                        select sl).AsNoTracking().ToList();
            else
                return (from sl in context.ServiceLists
                        join sd in context.ServiceListDistributions on sl.Id equals sd.ServiceId
                        where sl.DebtorNumber == debtorNumber
                        && sl.System == system
                        && sd.OutletId == outletId
                        && sd.ContactId == contactId
                        select sl).AsNoTracking().ToList();
        }

        public List<ServiceList> GetAllByDebtorNumberContactId(string debtorNumber, string contactId, string system) {
            // Get all the services owned by this DebtorNumber referencing the contact specified.
            return (from sl in context.ServiceLists
                    join sd in context.ServiceListDistributions on sl.Id equals sd.ServiceId
                    where sl.DebtorNumber == debtorNumber
                    && sl.System == system
                    && sd.ContactId == contactId
                    select sl).AsNoTracking().ToList();
        }

        public List<ServiceList> GetAllByServiceIds(string serviceIds)
        {
            var ids = serviceIds.Split(',').ToArray<string>();
            return (from sl in context.ServiceLists.AsNoTracking().AsEnumerable()
                    where ids.Contains(sl.Id.ToString())
                    select sl).ToList();
        }

        public List<ListTicker> GetTickerByDebtorNumber(string debtorNumber, string system) {
            // Get all the services owned by this DebtorNumber.
            return (from c in context.ListCategories
                    join pc in context.ListCategories on c.ParentId equals pc.Id
                    join sl in context.ServiceLists on c.Id equals sl.CategoryId
                    where c.IsHidden == "F"
                    && pc.IsHidden == "F"
                    && c.IsRadioListCategory == false
                    && sl.DebtorNumber == debtorNumber
                    && sl.System == system
                    && sl.IsHidden == "F"
                    select new ListTicker() { ListCategoryId = c.Id,
                                              ListCategoryParentSequenceNumber = pc.Id,
                                              ListCategorySequenceNumber = c.SequenceNumber,
                                              ListCategoryName = c.Name,
                                              ListId = sl.Id,
                                              ListSequenceNumber = sl.SequenceNumber,
                                              ListDistributionType = sl.DistributionType,
                                              ListDescription = sl.SelectionDescription })
                    .AsNoTracking().ToList();
        }

        public bool IsInUseByStatus(int serviceId, string debtorNumber, List<string> statusList) {
            // This is too slow due to issues with non unicode values. .NET framework 4.5 fixes the bug.
            //int count = (from j in context.Releases
            //             join t in context.Transactions on j.Id equals t.ReleaseId
            //             where j.DebtorNumber == debtorNumber
            //             && t.ServiceId == serviceId
            //             && statusList.Contains(j.Status)
            //             select j.Id).Count();

            //return (count > 0);

            // This is a hack to use until we can upgrade to .NET 4.5.
            StringBuilder status = new StringBuilder("");
            string sql;

            foreach (string s in statusList) {
                status.Append(string.Format("{0}'{1}'", (status.Length > 0 ? ", " : ""), s));
            }

            sql = string.Format("select @TransCount = count(t.Id) from mn_job j inner join mn_transaction t on t.JobId = j.Id where j.DebtorNumber='{0}' and t.ServiceId = {1} and j.Status in ({2})", debtorNumber, serviceId, status.ToString());

            SqlParameter param = new SqlParameter("TransCount", System.Data.SqlDbType.Int);
            param.Direction = System.Data.ParameterDirection.Output;

            context.Database.ExecuteSqlCommand(sql, param);

            return (int)param.Value > 0;
        }

        public int GetNextListNumber(string debtorNumber, string distType, string system) {
            int? maxListNumber;

            // Get the max ListNumber.
            maxListNumber = (from sl in context.ServiceLists
                          where sl.DebtorNumber == debtorNumber
                          && sl.System == system
                          && sl.DistributionType == distType
                          select (int?)sl.ListNumber).Max();

            if (maxListNumber.HasValue)
                return maxListNumber.Value + 1;
            else
                return 1;
        }

        public short GetNextSequenceNumber(string debtorNumber, string distType, string system) {
            short? maxSequenceNumber;

            // Get the max SequenceNumber.
            maxSequenceNumber = (from sl in context.ServiceLists
                             where sl.DebtorNumber == debtorNumber
                             && sl.System == system
                             && sl.DistributionType == distType
                             select (short?)sl.SequenceNumber).Max();

            if (maxSequenceNumber.HasValue)
                return (short)(maxSequenceNumber.Value + 1);
            else
                return 1;
        }

        public int GetNextSequenceNumber(int serviceId) {
            int? maxSequenceNumber;

            // Get the max SequenceNumber.
            maxSequenceNumber = (from d in context.ServiceListDistributions
                                 where d.ServiceId == serviceId
                                 select (int?)d.SequenceNumber).Max();

            if (maxSequenceNumber.HasValue)
                return maxSequenceNumber.Value + 1;
            else
                return 1;
        }

        public ListComment GetCommentById(int id) {
            List<ListComment> dbEntities = null;

            // Get all the Comment for this service.
            dbEntities = (from c in context.ListComments
                          where c.ServiceId == id
                          select c).AsNoTracking().ToList();

            if (dbEntities == null || dbEntities.Count < 1)
                return null;

            return dbEntities[0];
        }

        public List<Recipient> GetRecipientsById(int id) {
            List<Recipient> entities = null;

            // Get all the recipients for this service.
            entities = (from servDist in context.ServiceListDistributions
                          join recip in context.Recipients on servDist.RecipientId equals recip.Id
                          where servDist.ServiceId == id
                          orderby servDist.SequenceNumber ascending
                          select recip).AsNoTracking().ToList();

            return entities;
        }

        public List<ServiceList> SearchAapListsByOutletId(string outletId, string aapDebtorNumber)
        {
            return (from sd in context.ServiceListDistributions
                    join s in context.ServiceLists on sd.ServiceId equals s.Id
                    where sd.OutletId.Equals(outletId)
                    && s.IsHidden == "F"
                    && s.AddressCount > 0
                    && s.DebtorNumber == aapDebtorNumber
                    select s).Distinct().AsNoTracking().ToList();
        }

        public int AddRecipient(Recipient entity) {
            context.Recipients.Add(entity);
            context.SaveChanges();

            return entity.Id;
        }

        public int AddServiceListDistribution(ServiceListDistribution entity) {
            context.ServiceListDistributions.Add(entity);
            context.SaveChanges();

            return entity.Id;
        }

        public void UpdateRecipient(Recipient entity) {
            context.Entry(entity).State = System.Data.EntityState.Modified;
        }

        public void UpdateAllRecipients(int serviceId, List<Recipient> recipients) {
            int seqNo = 1;

            //Context.ServiceLists.Add(dbEntity);

            // Delete all existing Recipient and ServiceListDistribution items.
            context.Database.ExecuteSqlCommand(string.Format("exec sp_DeleteServiceDistributions {0}", serviceId));

            foreach (var recip in recipients) {
                var distEntity = new Model.Entities.ServiceListDistribution();

                context.Recipients.Add(recip);
                context.SaveChanges();

                distEntity.ServiceId = serviceId;
                distEntity.RecipientId = recip.Id;
                distEntity.SequenceNumber = seqNo;
                context.ServiceListDistributions.Add(distEntity);

                seqNo += 1;
            }

            context.SaveChanges();
        }

        public void DeleteRecipient(int serviceId, int recipientId) {
            Recipient entity = new Model.Entities.Recipient() { Id = recipientId };
            var dbEntities = context.Recipients.Local.Where(e => e.Id == entity.Id).ToList();
            var dbDistEntities = context.ServiceListDistributions.Where(e => e.RecipientId == recipientId && e.ServiceId == serviceId).ToList();

            foreach (var dist in dbDistEntities) {
                context.Entry(dist).State = System.Data.EntityState.Deleted;
            }
            context.SaveChanges();

            // If the items is already being tracked then delete the tracked item. Otherwise add a new entry as deleted.
            if (dbEntities.Count() > 0)
                context.Entry(dbEntities[0]).State = System.Data.EntityState.Deleted;
            else
                context.Entry(entity).State = System.Data.EntityState.Deleted;

            context.SaveChanges();
        }

        public void DeleteRecipients(int serviceId) {
            // Delete all existing Recipient and ServiceListDistribution items for a ServiceList.
            context.Database.ExecuteSqlCommand(string.Format("exec sp_DeleteServiceDistributions {0}", serviceId));
        }

        public int GetAddressCount(int serviceId) {
            return GetRecipientCountRecursive(serviceId);
        }

        public List<string> GetAllOutletNamesById(int serviceId) {
            return (from servDist in context.ServiceListDistributions
                    join o in context.MediaOutlets on servDist.OutletId equals o.Id
                    where servDist.ServiceId == serviceId
                    select o.Name).ToList();
        }

        public List<string> GetAllListDistributionsById(int serviceId) {
            return (from ld in context.ListDistributions
                    where ld.ServiceId == serviceId
                    select ld.OutletName).ToList();
        }

        public List<string> GetAllRecipientCompaniesById(int serviceId) {
            return (from servDist in context.ServiceListDistributions
                    join recip in context.Recipients on servDist.RecipientId equals recip.Id
                    where servDist.ServiceId == serviceId
                    select recip.Company).ToList();
        }

        public List<int> GetAllChildServiceListsById(int serviceId) {
            return (from servDist in context.ServiceListDistributions
                    where servDist.ServiceId == serviceId
                    && servDist.ChildServiceId != null
                    select servDist.ChildServiceId.Value).ToList();
        }

        public List<ServiceListDistribution> GetOutletOrEmployeeDistributionByType(string outletId, string contactId, string distributionType, string debtorNumber, string system)
        {
            if (contactId == null)
            return (from servDist in context.ServiceListDistributions
                    join s in context.ServiceLists on servDist.ServiceId equals s.Id
                    where s.DistributionType == distributionType && servDist.OutletId == outletId &&
                          servDist.ContactId == null && servDist.RecipientId == null &&
                          s.System == system && s.DebtorNumber == debtorNumber
                    select servDist).AsNoTracking().ToList();
            else
            {
                return (from servDist in context.ServiceListDistributions
                        join s in context.ServiceLists on servDist.ServiceId equals s.Id
                        where s.DistributionType == distributionType && servDist.OutletId == outletId &&
                              servDist.ContactId == contactId && servDist.RecipientId == null &&
                              s.System == system && s.DebtorNumber == debtorNumber
                        select servDist).AsNoTracking().ToList();
            }

        }

        public void UpdateDistributions(ServiceListDistribution entity)
        {
            context.Entry(entity).State = System.Data.EntityState.Modified;
        }

        public void DeleteServiceDistribution(ServiceListDistribution entity)
        {
            context.Entry(entity).State = System.Data.EntityState.Deleted;
            context.SaveChanges();
        }

        public void AddServiceDistribution(ServiceListDistribution entity)
        {
            context.Entry(entity).State = System.Data.EntityState.Added;
            context.SaveChanges();
        }

        public void RecalculateServiceAddresses(int serviceId, bool parentsOnly)
        {
            context.Database.ExecuteSqlCommand(string.Format("exec sp_RecalculateServiceAddresses {0}, {1}", serviceId, parentsOnly));
        }

        #region Private methods

        private int GetRecipientCountRecursive(int id, int depth = 0) {
            List<int> serviceList = null;
            int totalCount;

            if (depth > 10) throw new Exception("Depth of Service is greater than 10 and can not be resolved.");

            // Count all the recipients that aren't child Services.
            totalCount = (from servDist in context.ServiceListDistributions
                          where servDist.ServiceId == id
                          && servDist.ChildServiceId == null
                          select servDist.Id).Count();

            // Now fetch all the Services that are a child of this Service and fetch their Outlets.
            serviceList = (from servDist in context.ServiceListDistributions
                           where servDist.ServiceId == id
                           && servDist.ChildServiceId != null
                           select servDist.ChildServiceId.Value).ToList();

            foreach (int s in serviceList) {
                totalCount += GetRecipientCountRecursive(s, depth + 1);
            }

            return totalCount;
        }

        #endregion
    }
}
