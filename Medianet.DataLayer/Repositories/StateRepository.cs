﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace Medianet.DataLayer.Repositories
{
    public class StateRepository
    {
        private MedianetContext context;

        public StateRepository(UnitOfWork uow) {
            context = uow.Context;
        }

        public State GetById(int id) {
            throw new NotImplementedException();
        }

        public List<State> GetAll() {
            var entities = context.Database.SqlQuery<State>("exec State_GetAll");

            return entities.ToList();
        }
    }
}
