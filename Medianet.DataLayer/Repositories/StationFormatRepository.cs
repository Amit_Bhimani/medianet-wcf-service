﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class StationFormatRepository
    {
        private MedianetContext context;

        public StationFormatRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public StationFormat Get(int id)
        {
            return (from e in context.StationFormats
                    where e.Id == id
                    select e).AsNoTracking().First();
        }

        public List<StationFormat> GetAll()
        {
            return (from e in context.StationFormats
                    select e).AsNoTracking().ToList();
        }
    }
}
