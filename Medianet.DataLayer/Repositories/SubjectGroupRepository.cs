﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class SubjectGroupRepository
    {
        private MedianetContext context;

        public SubjectGroupRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public SubjectGroup Get(int id)
        {
            return (from e in context.SubjectGroups
                    where e.Id == id
                    select e).AsNoTracking().First();
        }

        public List<SubjectGroup> GetAll()
        {
            return (from e in context.SubjectGroups
                    select e).AsNoTracking().ToList();
        }
    }
}
