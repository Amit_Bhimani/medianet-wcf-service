﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class SubjectRepository
    {
        private MedianetContext context;

        public SubjectRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public Subject Get(int id)
        {
            return (from e in context.Subjects.Include("SubjectGroups")
                    where e.Id == id
                    select e).AsNoTracking().First();
        }

        public List<Subject> GetAll()
        {
            return (from e in context.Subjects.Include("SubjectGroups") select e).AsNoTracking().ToList();
        }

        public List<Subject> GetAllByIds(List<int> ids) {
            return (from e in context.Subjects
            where ids.Contains(e.Id)
            select e).AsNoTracking().ToList();
        }
    }
}
