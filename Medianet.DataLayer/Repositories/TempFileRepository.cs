﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class TempFileRepository
    {
        private MedianetContext context;

        public TempFileRepository(UnitOfWork uow) {
            context = uow.Context;
        }

        public int Add(TempFile entity) {
            context.TempFiles.Add(entity);
            context.SaveChanges();

            return entity.Id;
        }

        public void Update(TempFile entity) {
            List<Model.Entities.TempFile> dbEntities = context.TempFiles.Local.Where(r => r.Id == entity.Id).ToList();

            // If the items is already being tracked then update it's values. Otherwise add it as modified.
            if (dbEntities.Count() > 0)
                context.Entry(dbEntities[0]).CurrentValues.SetValues(entity);
            else
                context.Entry(entity).State = System.Data.EntityState.Modified;

            context.SaveChanges();
        }

        public void Delete(int id) {
            Model.Entities.TempFile dbEntity = new Model.Entities.TempFile() { Id = id };
            List<Model.Entities.TempFile> dbEntities = context.TempFiles.Local.Where(e => e.Id == dbEntity.Id).ToList();

            // If the items is already being tracked then delete the tracked item. Otherwise add a new entry as deleted.
            if (dbEntities.Count() > 0)
                context.Entry(dbEntities[0]).State = System.Data.EntityState.Deleted;
            else
                context.Entry(dbEntity).State = System.Data.EntityState.Deleted;

            context.SaveChanges();
        }

        public TempFile GetById(int id) {
            return (from i in context.TempFiles
                    where i.Id == id
                    select i).AsNoTracking().FirstOrDefault();
        }

        public List<TempFile> GetAllByParentId(int parentId) {
            return (from t in context.TempFiles
                    where t.ParentId == parentId
                    select t).AsNoTracking().ToList();
        }
    }
}
