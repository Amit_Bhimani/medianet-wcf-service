﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class ThumbnailRepository
    {
        private MedianetContext context;

        public ThumbnailRepository(UnitOfWork uow) {
            context = uow.Context;
        }

        public int Add(Thumbnail entity) {
            context.Thumbnails.Add(entity);
            context.SaveChanges();

            return entity.Id;
        }

        public void Update(Thumbnail entity) {
            List<Model.Entities.Thumbnail> dbEntities = context.Thumbnails.Local.Where(e => e.Id == entity.Id).ToList();

            // If the items is already being tracked then update it's values. Otherwise add it as modified.
            if (dbEntities.Count() > 0)
                context.Entry(dbEntities[0]).CurrentValues.SetValues(entity);
            else
                context.Entry(entity).State = System.Data.EntityState.Modified;

            context.SaveChanges();
        }

        public void Delete(int id) {
            Model.Entities.Thumbnail dbEntity = new Model.Entities.Thumbnail() { Id = id };
            List<Model.Entities.Thumbnail> dbEntities = context.Thumbnails.Local.Where(e => e.Id == dbEntity.Id).ToList();

            // If the items is already being tracked then delete the tracked item. Otherwise add a new entry as deleted.
            if (dbEntities.Count() > 0)
                context.Entry(dbEntities[0]).State = System.Data.EntityState.Deleted;
            else
                context.Entry(dbEntity).State = System.Data.EntityState.Deleted;

            context.SaveChanges();
        }

        public Thumbnail GetById(int id) {
            return (from t in context.Thumbnails
                    where t.Id == id
                    select t).AsNoTracking().FirstOrDefault();
        }

        public List<Thumbnail> GetAllByDocumentId(int documentId) {
            return (from t in context.Thumbnails
                    where t.DocumentId == documentId
                    select t).AsNoTracking().ToList();
        }
    }
}
