﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    // Note: Currently this repository doesn't support adding the full graph in one go
    // so whenever adding anything we need to do a SaveChanges() to get the Identity value
    // so we can then use this when inserting other objects (like Recipients).
    // This should be fixed at some point so all saving is done by the UnitOfWork.
    public class TimezoneRepository
    {
        private MedianetContext context;

        public TimezoneRepository(UnitOfWork uow) {
            context = uow.Context;
        }

        public Timezone GetByCode(string code) {
            List<Timezone> dbEntities = null;

            // Get the Service details.
            dbEntities = (from t in context.Timezones
                      where t.Code == code
                      select t).AsNoTracking().ToList();

            if (dbEntities == null || dbEntities.Count < 1)
                return null;

            return dbEntities[0];
        }

        public List<Timezone> GetAll() {
            // Get the TempFile details.
            return (from t in context.Timezones
                    select t).AsNoTracking().ToList();
        }
    }
}
