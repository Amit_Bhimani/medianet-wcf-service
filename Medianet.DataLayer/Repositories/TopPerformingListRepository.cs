﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class TopPerformingListRepository
    {
        private MedianetContext context;

        public TopPerformingListRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public List<TopPerformingList> GetAllByPeriod(string period, int maxRows)
        {
            return (from t in context.TopPerformingLists
                    where t.Period == period
                    select t)
                    .OrderByDescending(t => t.PercentageImprovement)
                    .ThenByDescending(t => t.LastPeriodTotal)
                    .ThenBy(t => t.SelectionDescription).Take(maxRows).AsNoTracking().ToList();
        }
    }
}
