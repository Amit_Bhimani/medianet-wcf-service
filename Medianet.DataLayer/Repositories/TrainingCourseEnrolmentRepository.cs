﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class TrainingCourseEnrolmentRepository
    {
        private MedianetContext context;

        public TrainingCourseEnrolmentRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public void Add(TrainingCourseEnrolment entity)
        {
            context.TrainingCourseEnrolments.Add(entity);
            context.SaveChanges();
        }

        public TrainingCourseEnrolment GetById(int id)
        {
            return (from c in context.TrainingCourseEnrolments
                    where c.Id == id
                    select c).AsNoTracking().FirstOrDefault();
        }

        public List<TrainingCourseEnrolment> GetAllByScheduleId(int scheduleId)
        {
            return (from e in context.TrainingCourseEnrolments
                    join s in context.TrainingCourseSchedules on e.ScheduleId equals s.Id
                    select e).AsNoTracking().ToList();
        }
    }
}
