﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class TrainingCoursePromoCodeRepository
    {
        private MedianetContext context;

        public TrainingCoursePromoCodeRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public void Add(TrainingCoursePromoCode entity)
        {
            context.TrainingCoursePromoCodes.Add(entity);
            context.SaveChanges();
        }

        public TrainingCoursePromoCode GetById(int id)
        {
            return (from c in context.TrainingCoursePromoCodes
                    where c.Id == id
                    select c).AsNoTracking().FirstOrDefault();
        }

        public List<TrainingCoursePromoCode> GetAllByCourseId(string courseId)
        {
            return (from e in context.TrainingCoursePromoCodes
                    where e.TrainingCourseId == courseId
                    select e).AsNoTracking().ToList();
        }
    }
}
