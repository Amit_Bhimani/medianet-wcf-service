﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class TrainingCourseRepository
    {
        private MedianetContext context;

        public TrainingCourseRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public void Add(TrainingCourse entity)
        {
            context.TrainingCourses.Add(entity);
            context.SaveChanges();
        }

        public TrainingCourse GetById(string id)
        {
            return (from c in context.TrainingCourses.Include("Contact").Include("Schedules").Include("Schedules.Enrolments").Include("Schedules.Location").Include("Topics").Include("PromoCodes")
                    where c.Id == id
                    select c).AsNoTracking().FirstOrDefault();
        }

        public List<TrainingCourse> GetAll()
        {
            return (from e in context.TrainingCourses.Include("Contact").Include("Schedules").Include("Schedules.Enrolments").Include("Schedules.Location").Include("Topics").Include("PromoCodes")
                    select e).AsNoTracking().ToList();
        }
    }
}
