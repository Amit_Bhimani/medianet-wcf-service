﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class TrainingCourseScheduleRepository
    {
        private MedianetContext context;

        public TrainingCourseScheduleRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public void Add(TrainingCourseSchedule entity)
        {
            context.TrainingCourseSchedules.Add(entity);
            context.SaveChanges();
        }

        public TrainingCourseSchedule GetById(int id)
        {
            return (from c in context.TrainingCourseSchedules.Include("Enrolments")
                    where c.Id == id
                    select c).AsNoTracking().FirstOrDefault();
        }

        public List<TrainingCourseSchedule> GetAllByCourseId(string courseId)
        {
            return (from e in context.TrainingCourseSchedules.Include("Enrolments")
                    where e.TrainingCourseId == courseId
                    select e).AsNoTracking().ToList();
        }
    }
}
