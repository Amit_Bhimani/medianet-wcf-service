﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class TrainingCourseTopicRepository
    {
        private MedianetContext context;

        public TrainingCourseTopicRepository(UnitOfWork uow)
        {
            context = uow.Context;
        }

        public void Add(TrainingCourseTopic entity)
        {
            context.TrainingCourseTopics.Add(entity);
            context.SaveChanges();
        }

        public TrainingCourseTopic GetById(int id)
        {
            return (from c in context.TrainingCourseTopics
                    where c.Id == id
                    select c).AsNoTracking().FirstOrDefault();
        }

        public List<TrainingCourseTopic> GetAllByCourseId(string courseId)
        {
            return (from e in context.TrainingCourseTopics
                    where e.TrainingCourseId == courseId
                    select e).AsNoTracking().ToList();
        }
    }
}
