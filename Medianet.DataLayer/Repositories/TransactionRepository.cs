﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class TransactionRepository
    {
        private MedianetContext context;

        public TransactionRepository(UnitOfWork uow) {
            context = uow.Context;
        }

        public int Add(Transaction entity) {
            context.Transactions.Add(entity);
            context.SaveChanges();

            return entity.Id;
        }

        public void Update(Transaction entity) {
            context.Entry(entity).State = System.Data.EntityState.Modified;
            context.SaveChanges();
        }

        public void Delete(int id) {
            Transaction entity = new Model.Entities.Transaction() { Id = id };
            context.Entry(entity).State = System.Data.EntityState.Deleted;
            context.SaveChanges();
        }

        public void DeleteByReleaseId(int releaseId) {
            List<Transaction> entities = null;

            entities = (from t in context.Transactions
                        where t.ReleaseId == releaseId
                        select t).ToList();

            foreach (Model.Entities.Transaction trans in entities) {
                context.Entry(trans).State = System.Data.EntityState.Deleted;
            }
        }

        public Transaction GetById(int id) {
            List<Transaction> entities = null;

            entities = (from t in context.Transactions
                        where t.Id == id
                        select t).AsNoTracking().ToList();

            if (entities == null || entities.Count < 1)
                return null;

            return entities[0];
        }

        public List<Transaction> GetAll() {
            throw new NotImplementedException();
        }

        public List<Transaction> GetAllByReleaseId(int releaseId) {
            return (from t in context.Transactions
                    where t.ReleaseId == releaseId
                    select t).AsNoTracking()
                    .OrderBy(x => x.SequenceNumber).ToList();
        }

        public List<TransactionStatistics> GetAllStatisticsByReleaseId(int releaseId) {
            List<Model.Entities.TransactionStatistics> statistics = null;

            // Get the Transaction details.
            statistics = (from t in context.Transactions
                         join rel in context.Releases on t.ReleaseId equals rel.Id
                         join r in context.Results on new { t.ReleaseId, TransactionId = t.Id } equals new { r.ReleaseId, r.TransactionId } into tempResults
                         from res in tempResults.DefaultIfEmpty()
                         where t.ReleaseId == releaseId
                         group res by new {
                             t.Id,
                             t.SequenceNumber,
                             t.DebtorNumber,
                             t.DistributionType,
                             t.Status,
                             t.IsSingleRecipient,
                             t.SelectionDescription,
                             t.ListNumber,
                             t.ShouldSendResultsToOwner
                         } into g
                         select new Model.Entities.TransactionStatistics {
                             Id = g.Key.Id,
                             SequenceNumber = g.Key.SequenceNumber,
                             DebtorNumber = g.Key.DebtorNumber,
                             DistributionType = g.Key.DistributionType,
                             Status = g.Key.Status,
                             IsSingleRecipient = g.Key.IsSingleRecipient,
                             SelectionDescription = g.Key.SelectionDescription,
                             ListNumber = g.Key.ListNumber,
                             ShouldSendResultsToOwner = g.Key.ShouldSendResultsToOwner,
                             TotalInProgress = g.Sum(x => (x.Status == "N" || x.Status == "R" || x.Status == "T") ? 1 : 0),
                             TotalSentOk = g.Sum(x => (x.Status == "G" && x.ErrorCode.TrimEnd() == "") ? 1 : 0),
                             TotalFailed = g.Sum(x => (x.Status == "F" || x.Status == "C" || (x.Status == "G" && x.ErrorCode.TrimEnd() != "")) ? 1 : 0),
                             TotalOpened = g.Sum(x => (x.Status == "G" && x.OpenCount > 0) ? 1 : 0),
                             TotalUnopened = g.Sum(x => (x.Status == "G" && x.ErrorCode.TrimEnd() == "" && x.OpenCount <= 0) ? 1 : 0)
                         }
                         ).AsNoTracking()
                         .OrderBy(x => x.SequenceNumber).ToList();

            return statistics;
        }
    }
}
