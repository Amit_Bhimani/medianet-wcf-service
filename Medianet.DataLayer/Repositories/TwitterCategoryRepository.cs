﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class TwitterCategoryRepository
    {
        private MedianetContext context;

        public TwitterCategoryRepository(UnitOfWork uow) {
            context = uow.Context;
        }

        public TwitterCategory GetById(int id) {
            return (from tc in context.TwitterCategories
                    where tc.Id == id
                    select tc).AsNoTracking().FirstOrDefault();
        }

        public List<TwitterCategory> GetAll() {
            return (from tc in context.TwitterCategories
                    select tc).AsNoTracking().ToList();
        }
    }
}
