﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class UserRepository
    {
        private MedianetContext context;

        public UserRepository(UnitOfWork uow) {
            context = uow.Context;
        }

        public int Add(User entity) {
            context.Users.Add(entity);
            context.SaveChanges();

            return entity.Id;
        }

        public void Update(User entity) {
            List<User> entities = context.Users.Local.Where(u => u.Id == entity.Id).ToList();

            // If the items is already being tracked then update it's values. Otherwise add it as modified.
            if (entities.Count() > 0)
                context.Entry(entities[0]).CurrentValues.SetValues(entity);
            else
                context.Entry(entity).State = System.Data.EntityState.Modified;

            context.SaveChanges();
        }

        public User GetById(int id) {
            return (from u in context.Users
                    where u.Id == id
                    select u).AsNoTracking().FirstOrDefault();
        }

        public List<User> GetAllByDebtorNumber(string debtorNumber) {
            return GetAllActive().Where(u => u.DebtorNumber == debtorNumber)
                                 .Where(u => !string.IsNullOrEmpty(u.EmailAddress))
                                 .AsNoTracking().ToList();
        }
      
        public User GetUserByContactId(string contactId)
        {
            var data = GetAllActive().Where(u => u.ContactID == contactId).FirstOrDefault();

            return data;
        }

        public List<User> GetAllByLogonName(string userName) {
            return GetAllActive().Where(u => u.LogonName == userName).AsNoTracking().ToList();
        }

        public List<User> GetAllByLogonDetails(string userName, string companyName)
        {
            return (from u in context.Users
                        join c in context.Customers on u.DebtorNumber equals c.DebtorNumber
                    where u.LogonName == userName
                    && c.LogonName == companyName
                    && u.RowStatus == Common.Constants.ROWSTATUS_ACTIVE
                    && c.RowStatus == Common.Constants.ROWSTATUS_ACTIVE
                    select u).AsNoTracking().ToList();
        }

        public List<User> GetAllByEmailAddress(string emailAddress) {
            return GetAllActive().Where(u => u.EmailAddress == emailAddress).AsNoTracking().ToList();
            //return (from u in context.Users
            //        join c in context.Customers on u.DebtorNumber equals c.DebtorNumber
            //        where u.EmailAddress == emailAddress
            //        && u.RowStatus == Common.Constants.ROWSTATUS_ACTIVE
            //        && c.RowStatus == Common.Constants.ROWSTATUS_ACTIVE
            //        select u).AsNoTracking().ToList();
        }


        public int? GetUserIdByUnsubscribeKey(Guid unsubscribeKey)
        {
            return context.Users.FirstOrDefault(m => m.UnsubscribeKey == unsubscribeKey)?.Id;
        }

        public void AcknowledgeMessageRead(int id)
        {
            User dbEntity = new User() { Id = id, HasViewedDistributionMessage = true };
            context.Users.Attach(dbEntity);
            context.Entry(dbEntity).Property(x => x.HasViewedDistributionMessage).IsModified = true;
            context.SaveChanges();
        }

        public bool IsMnjLogonNameUnique(string logonName)
        {
            var isNotUnique = context.Users.Any(m => m.LogonName == logonName && 
                                                    m.HasJournalistsWebAccess == true &&
                                                    (m.RowStatus == "A" || m.RowStatus == "I"));
            return !isNotUnique;
        }

        private IQueryable<User> GetAllActive()
        {
            return (from u in context.Users
                        join c in context.Customers on u.DebtorNumber equals c.DebtorNumber
                    where u.RowStatus == Common.Constants.ROWSTATUS_ACTIVE
                    && c.RowStatus == Common.Constants.ROWSTATUS_ACTIVE
                    select u);
        }
    }
}
