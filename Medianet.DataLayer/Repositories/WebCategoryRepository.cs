﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class WebCategoryRepository
    {
        private MedianetContext context;

        public WebCategoryRepository(UnitOfWork uow) {
            context = uow.Context;
        }

        public WebCategory GetById(int id) {
            return (from wc in context.WebCategories
                    where wc.Id == id
                    select wc).AsNoTracking().FirstOrDefault();
        }

        public List<WebCategory> GetAll() {
            return (from wc in context.WebCategories
                    select wc).AsNoTracking().ToList();
        }
    }
}
