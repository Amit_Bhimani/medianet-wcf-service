﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class WebReleaseRepository
    {
        private MedianetContext context;
        private UnitOfWork _uow;

        public WebReleaseRepository(UnitOfWork uow)
        {
            context = uow.Context;
            _uow = uow;
        }

        public WebRelease GetById(int id)
        {
            return (from wr in context.WebReleases
                    where wr.ReleaseId == id
                    select wr).AsNoTracking().FirstOrDefault();
        }

        #region Get By ProfileId

        public List<MnjProfileReleases> GetWebReleaseByProfileId(int profileId, int itemsPerPage, int currentPage)
        {
            var entities = context.Database.SqlQuery<MnjProfileReleases>("exec sp_MNJ_GetProfileReleases @ProfileID, @ItemsPerPage, @page",
                new SqlParameter("@ProfileID", profileId),
                new SqlParameter("@ItemsPerPage", itemsPerPage),
                new SqlParameter("@page", currentPage));

            return entities.ToList();
        }

        #endregion

        public int Add(WebRelease entity)
        {
            context.WebReleases.Add(entity);
            context.SaveChanges();

            return entity.ReleaseId;
        }

        public void Update(WebRelease entity)
        {
            context.Entry(entity).State = System.Data.EntityState.Modified;
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            WebRelease entity = new Model.Entities.WebRelease() { ReleaseId = id };
            context.Entry(entity).State = System.Data.EntityState.Deleted;
            context.SaveChanges();
        }
    }
}
