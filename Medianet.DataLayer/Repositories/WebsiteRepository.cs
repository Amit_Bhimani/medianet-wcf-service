﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects;
using System.Data.SqlClient;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer;

namespace Medianet.DataLayer.Repositories
{
    public class WebsiteRepository
    {
        private MedianetContext context;

        public WebsiteRepository(UnitOfWork uow) {
            context = uow.Context;
        }

        public int Add(Website entity) {
            context.Websites.Add(entity);
            context.SaveChanges();

            return entity.Id;
        }

        public void Update(Website entity) {
            context.Entry(entity).State = System.Data.EntityState.Modified;
        }

        public void Delete(int id) {
            Website entity = new Model.Entities.Website() { Id = id };
            context.Entry(entity).State = System.Data.EntityState.Deleted;
        }

        public Website GetById(int id) {
            List<Website> entities = null;

            // Get the web release details.
            entities = (from w in context.Websites
                        where w.Id == id
                        select w).AsNoTracking().ToList();

            if (entities == null || entities.Count < 1)
                return null;

            return entities[0];
        }

        public List<Website> GetAll() {
            return (from w in context.Websites
                    select w).AsNoTracking().ToList();
        }
    }
}
