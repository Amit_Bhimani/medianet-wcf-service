﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
//using System.Transactions;
using Medianet.Model;

namespace Medianet.DataLayer
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly MedianetContext _context;
        private bool disposed = false;
        //private TransactionScope _scope;
        private DbTransaction _transaction;

        internal MedianetContext Context
        {
            get { return _context; }
        }

        public UnitOfWork()
        {
            _context = new MedianetContext();
            _context.Configuration.LazyLoadingEnabled = false;

            //_scope = null;
            _transaction = null;
        }

        //public void BeginTransaction() {
        //_scope = new TransactionScope();

        //if (_context.Database.Connection != null && _context.Database.Connection.State != System.Data.ConnectionState.Open)
        //    _context.Database.Connection.Open();

        //_transaction = _context.Database.Connection.BeginTransaction();
        //((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.Connection
        //}

        //public void CommitTransaction() {
        //_scope.Complete();
        //_transaction.Commit();
        //}

        // public void RollbackTransaction() {
        //_transaction.Rollback();
        //}

        public int Save()
        {
            return _context.SaveChanges();
        }

        //public int SaveAndCommit()
        //{
        //    int saveOutput = _context.SaveChanges();
        //    //CommitTransaction();

        //    return saveOutput;
        //}

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();

                    //if (_scope != null)
                    //    _scope.Dispose();
                    if (_transaction != null)
                        _transaction.Dispose();
                }
            }

            this.disposed = true;
        }
    }
}
