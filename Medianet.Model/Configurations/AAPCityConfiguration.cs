﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    public class AAPCityConfiguration : EntityTypeConfiguration<AAPCity>
    {
        public AAPCityConfiguration()
        {
            this.ToTable("mn_city");
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("CityId");
            this.Property(t => t.Name).HasColumnName("CityName");
            this.Property(t => t.RowStatus).HasColumnName("Status");
        }
    }
}
