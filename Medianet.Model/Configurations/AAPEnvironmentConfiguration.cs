﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class AAPEnvironmentConfiguration : EntityTypeConfiguration<AAPEnvironment>
    {
        public AAPEnvironmentConfiguration() {
            this.ToTable("mn_environment");
            this.HasKey(e => new { UserId = e.UserId, Application = e.Application, Type = e.Type });
        }
    }
}
