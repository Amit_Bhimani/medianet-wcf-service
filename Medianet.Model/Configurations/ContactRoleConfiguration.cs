﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    public class ContactRoleConfiguration : EntityTypeConfiguration<ContactRole>
    {
        public ContactRoleConfiguration()
        {
            this.ToTable("mn_contact_role");
            this.HasKey(e => new { e.RoleId }).Property(o => o.RoleId).HasColumnName("RoleId");
            this.Property(o => o.Name).HasColumnName("RoleName");

        }
    }
}
