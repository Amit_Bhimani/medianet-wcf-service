﻿using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class ContinentConfiguration : EntityTypeConfiguration<Continent>
    {
        public ContinentConfiguration() {
            this.ToTable("mn_data_modules");
            this.HasKey(s => new { s.DataModuleId }).Property(s => s.DataModuleId).HasColumnName("Id");
            this.Property(s => s.Id).HasColumnName("MediaAtlasContinentId");
            this.Property(s => s.Name).HasColumnName("DataModuleName");
        }
    }
}
