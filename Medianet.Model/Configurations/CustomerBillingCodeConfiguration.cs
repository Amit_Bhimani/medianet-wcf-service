﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Configurations
{
    public class CustomerBillingCodeConfiguration : EntityTypeConfiguration<CustomerBillingCode>
    {
        public CustomerBillingCodeConfiguration()
        {
            // Primary Key
            this.HasKey(t => new { t.Id });

            // Properties
            this.Property(t => t.DebtorNumber)
                .IsRequired()
                .HasMaxLength(12);

            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("mn_customer_billing_code");
            this.Property(t => t.DebtorNumber).HasColumnName("DebtorNumber");
            this.Property(t => t.Id).HasColumnName("BillingCodeId");
            this.Property(t => t.Name).HasColumnName("BillingCodeName");

            // Relationships
            this.HasRequired(t => t.Customer)
                .WithMany(t => t.BillingCodes)
                .HasForeignKey(d => d.DebtorNumber);

        }
    }
}
