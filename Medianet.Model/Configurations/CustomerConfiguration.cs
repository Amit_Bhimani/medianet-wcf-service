﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class CustomerConfiguration : EntityTypeConfiguration<Customer>
    {
        public CustomerConfiguration() {
            this.ToTable("mn_customer");
            this.HasKey(c => new { c.DebtorNumber }).Property(p => p.DebtorNumber).HasColumnName("DebtorNumber");
            this.Property(p => p.Name).HasColumnName("CustomerName");
            this.Property(p => p.LogonName).HasColumnName("CompanyLogonName");
            this.Property(p => p.SalesEmailAddress).HasColumnName("SalesEmail");
            this.Property(p => p.DefaultOriginatorEmailAddress).HasColumnName("OriginatorEmail");
            this.Property(p => p.ReportSendMethod).HasColumnName("ReportMethod");
            this.Property(p => p.ReportResultsToSend).HasColumnName("ResultsType");
            this.Property(p => p.ShouldSendResultsData).HasColumnName("SendResultsData");
            this.Property(p => p.ShouldSendOptOutForFax).HasColumnName("OptoutFax");
            this.Property(p => p.ShouldSendOptOutForEmail).HasColumnName("OptoutEmail");
            this.Property(p => p.ShouldSendOptOutForSMS).HasColumnName("OptoutSMS");
            this.Property(p => p.TimezoneCode).HasColumnName("Timezone");
            this.Property(p => p.MediaContactsLicenseCount).HasColumnName("NoOfLicenses");
            this.Property(p => p.MediaContactsMasterUserId).HasColumnName("MasterUserID");
            this.Property(p => p.MediaContactsMasterPassword).HasColumnName("MasterPassword");
            this.Property(u => u.CreatedDate).HasColumnName("CreatedTime");
            this.Property(u => u.CreatedByUserId).HasColumnName("CreatedUserId");
            this.Property(u => u.LastModifiedDate).HasColumnName("ModifiedTime");
            this.Property(u => u.LastModifiedByUserId).HasColumnName("ModifiedUserId");
            this.Property(u => u.RowStatus).HasColumnName("Status");

            this.HasRequired(u => u.Timezone).WithMany().HasForeignKey(u => u.TimezoneCode);
        }
    }
}
