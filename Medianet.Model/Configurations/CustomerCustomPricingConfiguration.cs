﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Configurations
{
    public class CustomerCustomPricingConfiguration : EntityTypeConfiguration<CustomerCustomPricing>
    {
        public CustomerCustomPricingConfiguration()
        {
            // Primary Key
            this.HasKey(t => new { t.Id });

            // Properties
            this.Property(t => t.DebtorNumber)
                .IsRequired();

            this.Property(t => t.BrsContractId)
                .IsRequired();
            
            this.Property(t => t.BrsServiceId)
               .IsRequired();

            this.Property(t => t.CostPerUnit)
               .IsRequired();

            this.Property(t => t.CreatedBy)
               .IsRequired();

            this.Property(t => t.CreatedDate)
             .IsRequired();

            this.Property(t => t.LastModifiedBy)
             .IsRequired();

            this.Property(t => t.LastModifiedDate)
             .IsRequired();

            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("mn_custom_pricing");
            this.Property(t => t.DebtorNumber).HasColumnName("DebtorNumber");

        }
    }
}
