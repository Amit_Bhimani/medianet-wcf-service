﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Configurations
{
    public class CustomerRegionConfiguration : EntityTypeConfiguration<CustomerRegion>
    {
        public CustomerRegionConfiguration()
        {
            this.ToTable("mn_customer_region");
            this.HasKey(t => new { t.DebtorNumber, t.RegionId });
            this.Property(t => t.DebtorNumber).IsRequired().HasMaxLength(12);
            this.Property(t => t.RegionId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            this.Property(t => t.DebtorNumber).HasColumnName("DebtorNumber");
            this.Property(t => t.RegionId).HasColumnName("RegionId");

            // Relationships
            this.HasRequired(t => t.Customer)
                .WithMany(t => t.SalesRegions)
                .HasForeignKey(d => d.DebtorNumber);

        }
    }
}
