﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Configurations
{
    public class CustomerStatisticConfiguration : EntityTypeConfiguration<CustomerStatistic>
    {
        public CustomerStatisticConfiguration()
        {
            this.ToTable("mn_customer_statistics");
            this.HasKey(t => t.DebtorNumber);

            // Relationships
            this.HasRequired(t => t.Customer)
                .WithOptional();
        }
    }
}
