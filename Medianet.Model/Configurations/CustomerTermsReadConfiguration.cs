﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class CustomerTermsReadConfiguration : EntityTypeConfiguration<CustomerTermsRead>
    {
        public CustomerTermsReadConfiguration()
        {
            this.ToTable("mn_customer_terms_read");
            this.HasKey(c => new { c.Id }).Property(p => p.Id).HasColumnName("Id");
            this.Property(l => l.System).HasColumnName("SystemType");
        }
    }
}
