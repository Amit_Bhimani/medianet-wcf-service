﻿using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class DBSessionConfiguration : EntityTypeConfiguration<DBSession>
    {
        public DBSessionConfiguration() {
            this.ToTable("mn_session");
            this.HasKey(s => new { Id = s.Id }).Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None).HasColumnName("SessionID");
            this.Property(s => s.UserId).HasColumnName("UserID");
            this.Property(s => s.System).HasColumnName("SystemType");
            this.Property(s => s.RowStatus).HasColumnName("Status");
            this.Property(s => s.LoginDate).HasColumnName("LoginTime");
            this.Property(s => s.LastAccessedDate).HasColumnName("LastAccessTime");

            this.HasRequired(s => s.Customer).WithMany().HasForeignKey(s => s.DebtorNumber);
            this.HasRequired(s => s.User).WithMany().HasForeignKey(s => s.UserId);
        }
    }
}
