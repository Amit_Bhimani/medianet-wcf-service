﻿namespace Medianet.Model.Configurations
{
    using System.Data.Entity.ModelConfiguration;
    using Medianet.Model.Entities;

    class DayBookConfiguration : EntityTypeConfiguration<DayBook>
    {
        public DayBookConfiguration() {
            this.ToTable("mn_Daybook");
            this.HasKey(c => new { c.Id }).Property(p => p.Id).HasColumnName("DaybookID");
            this.Property(p => p.Text).HasColumnName("Daybook");
            this.Property(p => p.Date).HasColumnName("DaybookDate");
            this.Property(p => p.LastModified).HasColumnName("ModifiedDate");
            this.Property(p => p.LastModifiedByUserId).HasColumnName("ModifiedUserId");
            this.HasRequired(p => p.DaybookType).WithMany(p => p.Daybooks).HasForeignKey(p => p.DaybookTypeID);
        }
    }
}
