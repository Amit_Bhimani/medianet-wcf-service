﻿namespace Medianet.Model.Configurations
{
    using System.Data.Entity.ModelConfiguration;
    using Medianet.Model.Entities;

    class DayBookTypeConfiguration : EntityTypeConfiguration<DayBookType>
    {
        public DayBookTypeConfiguration() {
            this.ToTable("mn_DaybookType");
            this.HasKey(c => new { c.Id }).Property(p => p.Id).HasColumnName("DaybookTypeID");
            this.Property(p => p.Type).HasColumnName("DaybookType");
            this.Property(p => p.URI).HasColumnName("LinkURL");
            this.Property(p => p.IsDateType).HasColumnName("IsDateType");
        }
    }
}
