﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class DocumentConfiguration : EntityTypeConfiguration<Document>
    {
        public DocumentConfiguration() {
            this.ToTable("mn_document");
            this.HasKey(u => new { Id = u.Id }).Property(p => p.Id).HasColumnName("DocumentId");
            this.Property(u => u.ReleaseId).HasColumnName("JobId");
            this.Property(u => u.FileExtension).HasColumnName("FileExt");
            this.Property(u => u.IsDerived).HasColumnName("Derived");
            this.Property(u => u.OriginalFilename).HasColumnName("OriginalFileName");
            this.Property(u => u.CanDistributeViaFax).HasColumnName("DistributionFax");
            this.Property(u => u.CanDistributeViaEmailBody).HasColumnName("DistributionEmailBody");
            this.Property(u => u.CanDistributeViaEmailAttachment).HasColumnName("DistributionEmailAttachment");
            this.Property(u => u.CanDistributeViaWire).HasColumnName("DistributionWire");
            this.Property(u => u.CanDistributeViaVoice).HasColumnName("DistributionVoice");
            this.Property(u => u.CanDistributeViaWeb).HasColumnName("DistributionWeb");
            this.Property(u => u.CanDistributeViaSMS).HasColumnName("DistributionSMS");
            this.Property(u => u.IsMailMergeDataSource).HasColumnName("MailMergeDataSource");
            this.Property(u => u.IsTabulated).HasColumnName("Tabulated");
            this.Property(u => u.CreatedDate).HasColumnName("CreatedTime");
            this.Property(u => u.CreatedByUserId).HasColumnName("CreatedUserId");
            this.Property(u => u.LastModifiedDate).HasColumnName("ModifiedTime");
            this.Property(u => u.LastModifiedByUserId).HasColumnName("ModifiedUserId");

            //this.HasOptional(u => u.Thumbnails).WithRequired(t => t.d);
        }
    }
}
