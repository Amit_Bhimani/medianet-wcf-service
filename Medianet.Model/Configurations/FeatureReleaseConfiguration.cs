﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Configurations
{
    class FeatureReleaseConfiguration : EntityTypeConfiguration<FeatureRelease>
    {
        public FeatureReleaseConfiguration() {
            this.ToTable("mn_feature_release");
            this.HasKey(u => new { Id = u.Id });
            this.Property(u => u.ReleaseId).HasColumnName("JobId");

            this.HasRequired(f => f.Release).WithMany().HasForeignKey(f => f.ReleaseId);
        }
    }
}
