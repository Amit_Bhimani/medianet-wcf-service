﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class InboundFileConfiguration : EntityTypeConfiguration<InboundFile>
    {
        public InboundFileConfiguration() {
            this.ToTable("mn_message");
            this.HasKey(c => new { c.Id }).Property(p => p.Id).HasColumnName("MessageID");
            this.Property(p => p.FileExtension).HasColumnName("FileExt");
            this.Property(p => p.OriginalFilename).HasColumnName("ExternalFileName");
            this.Property(p => p.OwnerUserId).HasColumnName("OwnerUserID");
            this.Property(p => p.LastForwardedDate).HasColumnName("ForwardTime");
            this.Property(p => p.ForwardedCount).HasColumnName("ForwardCount");
            this.Property(p => p.ItemCount).HasColumnName("Itemcount");
            this.Property(p => p.FinanceFileId).HasColumnName("JobFileID");
            this.Property(p => p.FileSize).HasColumnName("Size");
            this.Property(p => p.CreatedDate).HasColumnName("CreatedTime");
            this.Property(p => p.CreatedByUserId).HasColumnName("CreatedUserID");
            this.Property(p => p.LastModifiedDate).HasColumnName("ModifiedTime");
            this.Property(p => p.LastModifiedByUserId).HasColumnName("ModifiedUserID");
        }
    }
}
