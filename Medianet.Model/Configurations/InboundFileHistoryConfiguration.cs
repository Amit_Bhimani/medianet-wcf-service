﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class InboundFileHistoryConfiguration : EntityTypeConfiguration<InboundFileHistory>
    {
        public InboundFileHistoryConfiguration() {
            this.ToTable("mn_InboundHistory");
            this.HasKey(c => new { c.Id }).Property(p => p.Id).HasColumnName("ID");
            this.Property(p => p.InboundFileId).HasColumnName("MessageID");
            this.Property(p => p.ReleaseId).HasColumnName("JobID");
            this.Property(p => p.CreatedDate).HasColumnName("Timestamp");
        }
    }
}
