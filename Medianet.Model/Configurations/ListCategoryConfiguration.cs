﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class ListCategoryConfiguration : EntityTypeConfiguration<ListCategory>
    {
        public ListCategoryConfiguration() {
            this.ToTable("mn_category");
            this.HasKey(u => new { Id = u.Id }).Property(p => p.Id).HasColumnName("Id");
            this.Property(u => u.Name).HasColumnName("CategoryName");
            this.Property(u => u.IsHidden).HasColumnName("Hidden");
            this.Property(u => u.IsRadioListCategory).HasColumnName("Radio");
        }
    }
}