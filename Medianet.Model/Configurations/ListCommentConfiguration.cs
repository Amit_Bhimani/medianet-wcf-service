﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class ListCommentConfiguration : EntityTypeConfiguration<ListComment>
    {
        public ListCommentConfiguration() {
            this.ToTable("mn_comment");
            this.HasKey(c => new { c.Id }).Property(p => p.Id).HasColumnName("CommentId");
            this.Property(c => c.CreatedByUserId).HasColumnName("CreatedUserId");
        }
    }
}
