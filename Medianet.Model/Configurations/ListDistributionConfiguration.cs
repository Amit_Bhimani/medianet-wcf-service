﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class ListDistributionConfiguration : EntityTypeConfiguration<ListDistribution>
    {
        public ListDistributionConfiguration() {
            this.ToTable("mn_distribution_list");
            this.HasKey(d => new { d.Id });
            this.Property(d => d.ServiceId).HasColumnName("ServiceID");
            this.Property(d => d.OutletName).HasColumnName("Recipient");
        }
    }
}
