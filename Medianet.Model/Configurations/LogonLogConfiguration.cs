﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class LogonLogConfiguration : EntityTypeConfiguration<LogonLog>
    {
        public LogonLogConfiguration() {
            this.ToTable("mn_logon_log");
            this.HasKey(l => new { l.LoginDate, l.UserId, l.System });
            this.Property(l => l.LoginDate).HasColumnName("WhenLogged");
            this.Property(l => l.System).HasColumnName("SystemType");
        }
    }
}
