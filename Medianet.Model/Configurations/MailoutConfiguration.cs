﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    public class MailoutConfiguration : EntityTypeConfiguration<Mailout>
    {
        public MailoutConfiguration()
        {
            this.ToTable("mailout");
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.TransmitTime).HasColumnName("TransmitTime");
            this.Property(t => t.RecipientNumber).HasColumnName("RecipientNumber");
            this.Property(t => t.Status).HasColumnName("Status");
        }
    }
}
