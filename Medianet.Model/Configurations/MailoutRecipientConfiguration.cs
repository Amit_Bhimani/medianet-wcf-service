﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    public class MailoutRecipientConfiguration : EntityTypeConfiguration<MailoutRecipient>
    {
        public MailoutRecipientConfiguration()
        {
            this.ToTable("mn_mailout_recipient");
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.MailoutId).HasColumnName("MailoutId");
            this.Property(t => t.ContactId).HasColumnName("ContactId");
            this.Property(t => t.OutletId).HasColumnName("OutletId");
            this.Property(t => t.OpendTime).HasColumnName("OpendTime");
            this.Property(t => t.UpdatedTime).HasColumnName("UpdatedTime");
            this.Property(t => t.ViewedTime).HasColumnName("ViewedTime");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.ErrorMessage).HasColumnName("ErrorMessage");
        }
    }
}
