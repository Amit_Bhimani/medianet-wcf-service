﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Configurations
{
    class MediaAtlasConfiguration : EntityTypeConfiguration<MediaAtlas>
    {
        public MediaAtlasConfiguration()
        {
            this.ToTable("mn_media_atlas_id");
            this.Property(t => t.Prefix).HasColumnName("Prefix");
            this.Property(t => t.Id).HasColumnName("Id");
            // Primary Key
            this.HasKey(t => t.Prefix);

        }
    }
}
