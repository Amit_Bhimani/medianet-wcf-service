﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MediaContactConfiguration : EntityTypeConfiguration<MediaContact>
    {
        public MediaContactConfiguration()
        {
            this.ToTable("mn_media_contact");
            this.HasKey(o => new { o.Id }).Property(o => o.Id).HasColumnName("ContactId");
            this.Property(o => o.CreatedDate).HasColumnName("Added");
            this.Property(o => o.LastModifiedDate).HasColumnName("Updated").IsConcurrencyToken(true);
            this.Property(o => o.LastModifiedByUserId).HasColumnName("LastModifiedBy");
            this.Property(o => o.LockedByUserId).HasColumnName("LockedBy");
            this.Property(o => o.DeletedDate).HasColumnName("Deleted");
            this.Property(o => o.RowStatus).HasColumnName("Status");
            this.Property(o => o.InternalNotes).HasColumnName("InternalNotes");

            // Relationships
            this.HasOptional(s => s.User).WithMany().HasForeignKey(s => s.LastModifiedByUserId);
            this.HasOptional(t => t.Profile).WithRequired(c => c.MediaContact).Map(p => p.MapKey("ContactId"));
        }
    }
}
