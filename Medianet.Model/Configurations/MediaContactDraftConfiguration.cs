﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MediaContactDraftConfiguration : EntityTypeConfiguration<MediaContactDraft>
    {
        public MediaContactDraftConfiguration()
        {
            ToTable("mn_media_contact_draft");

            HasKey(o => new { o.Id }).Property(o => o.Id).HasColumnName("Id");
            Property(o => o.ContactId).HasColumnName("ContactId");
            Property(o => o.QueueId).HasColumnName("QueueId");

            HasRequired(t => t.DraftQueue).WithMany().HasForeignKey(d => d.QueueId).WillCascadeOnDelete(true);
        }
    }
}
