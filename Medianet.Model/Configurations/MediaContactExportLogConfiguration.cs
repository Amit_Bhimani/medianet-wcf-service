﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MediaContactExportLogConfiguration :EntityTypeConfiguration<MediaContactExportLog>
    {
        public MediaContactExportLogConfiguration()
        {
            this.ToTable("mn_export_log");
            this.HasKey(c => new { c.Id }).Property(p => p.Id).HasColumnName("Id");
            this.Property(p => p.DebtorNumber).HasColumnName("DebtorNumber");
            this.Property(p => p.UserId).HasColumnName("UserId");
            this.Property(p => p.ListId).HasColumnName("ListId");
            this.Property(p => p.TaskId).HasColumnName("TaskId");
            this.Property(p => p.CreatedDate).HasColumnName("CreatedDate");
            this.Property(p => p.NoOfExportedRows).HasColumnName("NoOfExportedRows");
            this.Property(p => p.ExportedFields).HasColumnName("ExportedFields");
        }
    
    }
}
