﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MediaContactGroupConfiguration : EntityTypeConfiguration<MediaContactGroup>
    {
        public MediaContactGroupConfiguration() {
            this.ToTable("mn_GroupMaster");
            this.HasKey(g => new { g.Id }).Property(g => g.Id).HasColumnName("GroupId");
            this.Property(g => g.Name).HasColumnName("GroupName");
            this.Property(g => g.GroupType).HasColumnName("GroupTypeId");
            this.Property(g => g.CreatedByUserId).HasColumnName("userid");
            this.Property(g => g.IsActive).HasColumnName("Status");
            this.Property(g => g.LastModifiedDate).HasColumnName("UpdatedDate");

            // Relationships
            this.HasRequired(g => g.CreatedByUser).WithMany().HasForeignKey(g => g.CreatedByUserId);
        }
    }
}
