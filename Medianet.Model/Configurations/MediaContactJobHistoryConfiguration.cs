﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MediaContactJobHistoryConfiguration : EntityTypeConfiguration<MediaContactJobHistory>
    {
        public MediaContactJobHistoryConfiguration()
        {
            this.ToTable("mn_media_contact_job_history");
            this.HasKey(o => new { o.Id }).Property(o => o.Id).HasColumnName("Id");
            this.Property(o => o.ContactId).HasColumnName("ContactId");
            this.Property(o => o.OutletId).HasColumnName("OutletId");

            this.HasRequired(t => t.MediaContact)
                .WithMany(t => t.JobHistory)
                .HasForeignKey(d => d.ContactId);
        }
    }
}
