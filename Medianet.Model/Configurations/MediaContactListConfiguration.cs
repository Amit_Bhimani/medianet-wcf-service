﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MediaContactListConfiguration : EntityTypeConfiguration<MediaContactList>
    {
        public MediaContactListConfiguration() {
            this.ToTable("mn_GroupLists");
            this.HasKey(g => new { g.Id }).Property(g => g.Id).HasColumnName("ListId");
            this.Property(g => g.Name).HasColumnName("ListName");
            this.Property(g => g.OwnerUserId).HasColumnName("UserId");
            this.Property(g => g.IsActive).HasColumnName("Status");
            this.Property(g => g.LastModifiedDate).HasColumnName("UpdatedDate");

            // Relationships
            this.HasRequired(g => g.OwnerUser).WithMany().HasForeignKey(g => g.OwnerUserId);
            this.HasRequired(g => g.Group).WithMany().HasForeignKey(g => g.GroupId);
            //this.HasMany(g => g.Versions).WithRequired().HasForeignKey(g => g.ListId);
        }
    }
}
