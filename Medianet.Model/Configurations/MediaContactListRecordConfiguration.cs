﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MediaContactListRecordConfiguration : EntityTypeConfiguration<MediaContactListRecord>
    {
        public MediaContactListRecordConfiguration() {
            this.ToTable("mn_ListMaster");
            this.HasKey(g => new { g.Id }).Property(g => g.Id).HasColumnName("RecordId");
            this.Property(g => g.CreatedByUserId).HasColumnName("UserId");
        }
    }
}
