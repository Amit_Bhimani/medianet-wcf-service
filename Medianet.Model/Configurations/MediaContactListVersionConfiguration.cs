﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MediaContactListVersionConfiguration : EntityTypeConfiguration<MediaContactListVersion>
    {
        public MediaContactListVersionConfiguration() {
            this.ToTable("mn_List_Versions");
            this.HasKey(g => new { g.Id }).Property(g => g.Id).HasColumnName("ListSetId");
            this.Property(g => g.Name).HasColumnName("ListNameVersioned");
            this.Property(g => g.LastModifiedDate).HasColumnName("UpdatedDate");
        }
    }
}
