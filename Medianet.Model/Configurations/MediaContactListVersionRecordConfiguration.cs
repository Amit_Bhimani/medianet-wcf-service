﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MediaContactListVersionRecordConfiguration : EntityTypeConfiguration<MediaContactListVersionRecord>
    {
        public MediaContactListVersionRecordConfiguration() {
            this.ToTable("mn_List_Versions_Records");
            this.HasKey(g => new { g.Id }).Property(g => g.Id);
            this.Property(t => t.CreatedByUserId).HasColumnName("CreatedUserId");
            this.Property(t => t.LastModifiedDate).HasColumnName("ModifiedDate");
        }
    }
}
