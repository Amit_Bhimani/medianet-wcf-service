﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MediaContactMovementConfiguration : EntityTypeConfiguration<MediaContactMovement>
    {
        public MediaContactMovementConfiguration()
        {
            this.ToTable("mn_media_contact_movement");
            this.HasKey(c => new {c.Id});
            this.Property(o => o.CreatedDate).HasColumnName("Created");
            this.Property(o => o.LastModifiedDate).HasColumnName("Updated");
            this.Property(o => o.CreatedByUserId).HasColumnName("CreatedBy");
            this.Property(o => o.LastModifiedByUserId).HasColumnName("LastModifiedBy");

            // Relationships

            this.HasRequired(s => s.MediaOutlet)
                .WithMany()
                .HasForeignKey(s => s.OutletId);

            this.HasOptional(s => s.MediaEmployee)
                .WithMany(s=>s.Movements)
                .HasForeignKey(s => new { s.OutletId, s.ContactId });

            this.HasOptional(s => s.User).WithMany().HasForeignKey(s => s.CreatedByUserId);
        }
    }
}
