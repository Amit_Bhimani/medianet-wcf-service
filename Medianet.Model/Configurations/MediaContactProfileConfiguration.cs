﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Configurations
{
    public class MediaContactProfileConfiguration : EntityTypeConfiguration<MediaContactProfile>
    {
        public MediaContactProfileConfiguration()
        {
            this.ToTable("mn_media_contact_profile");
            this.Property(t => t.Id).HasColumnName("id");
            //this.Property(t => t.ContactId).HasColumnName("ContactId");
            this.Property(t => t.Content).HasColumnName("Profile");
            this.Property(t => t.Updated).HasColumnName("Updated");
            // Primary Key
            this.HasKey(t => t.Id);

        }
    }
}
