﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MediaContactReminderTypeConfiguration : EntityTypeConfiguration<MediaContactReminderType>
    {
        public MediaContactReminderTypeConfiguration() {
            this.ToTable("mn_ReminderType");
            this.HasKey(g => new { g.Id });
            this.Property(g => g.Name).HasColumnName("RemType");
            this.Property(g => g.IsActive).HasColumnName("Status");
        }
    }
}
