﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MediaContactSavedSearchConfiguration : EntityTypeConfiguration<MediaContactSavedSearch>
    {
        public MediaContactSavedSearchConfiguration() {
            this.ToTable("SavedSearchMaster");
            this.HasKey(g => new { g.Id }).Property(g => g.Id).HasColumnName("SearchId");
            this.Property(g => g.Name).HasColumnName("SearchName");
            this.Property(g => g.OwnerUserId).HasColumnName("UserId");
            this.Property(g => g.Context).HasColumnName("ContextText");
            this.Property(g => g.IsPrivate).HasColumnName("Visibility");
            this.Property(g => g.IsActive).HasColumnName("Status");
            this.Property(g => g.LastModifiedDate).HasColumnName("ModifiedDate");

            // Relationships
            this.HasRequired(g => g.OwnerUser).WithMany().HasForeignKey(g => g.OwnerUserId);
            this.HasRequired(g => g.Group).WithMany().HasForeignKey(g => g.GroupId);
        }
    }
}
