﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MediaContactSearchLogConfiguration :EntityTypeConfiguration<MediaContactSearchLog>
    {
        public MediaContactSearchLogConfiguration()
        {
            this.ToTable("mn_search_log");
            this.HasKey(c => new { c.Id }).Property(p => p.Id).HasColumnName("Id");
            this.Property(p => p.DebtorNumber).HasColumnName("DebtorNumber");
            this.Property(p => p.UserId).HasColumnName("UserId");
            this.Property(p => p.SearchCriteria).HasColumnName("SearchCriteria");
            this.Property(p => p.ResultsCount).HasColumnName("ResultsCount");
            this.Property(p => p.SearchType).HasColumnName("SearchType");
            this.Property(p => p.CreatedDate).HasColumnName("CreatedDate");
            this.Property(p => p.SavedSearchId).HasColumnName("SavedSearchId");
            this.Property(p => p.System).HasColumnName("SystemType");
        }
    
    }
}
