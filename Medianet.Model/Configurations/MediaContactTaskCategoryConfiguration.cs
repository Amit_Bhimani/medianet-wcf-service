﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MediaContactTaskCategoryConfiguration : EntityTypeConfiguration<MediaContactTaskCategory>
    {
        public MediaContactTaskCategoryConfiguration() {
            this.ToTable("mn_TaskCategory");
            this.HasKey(g => new { g.Id }).Property(g => g.Id).HasColumnName("CategoryId");
            this.Property(g => g.Name).HasColumnName("CategoryName");
            this.Property(g => g.OwnerUserId).HasColumnName("UserId");
            this.Property(g => g.IsActive).HasColumnName("Status");
            this.Property(g => g.LastModifiedDate).HasColumnName("UpdatedDate");
        }
    }
}
