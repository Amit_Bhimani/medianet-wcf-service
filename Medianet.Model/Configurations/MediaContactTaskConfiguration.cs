﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MediaContactTaskConfiguration : EntityTypeConfiguration<MediaContactTask>
    {
        public MediaContactTaskConfiguration()
        {
            this.ToTable("mn_Tasks");
            this.HasKey(g => new { g.Id });
            this.Property(g => g.Name).HasColumnName("TaskName");
            this.Property(g => g.Description).HasColumnName("Taskdescription");
            this.Property(g => g.ReminderTypeId).HasColumnName("ReminderDateTimeId");
            this.Property(g => g.StatusType).HasColumnName("StatusId");
            this.Property(g => g.IsEmailSent).HasColumnName("EmailStatus");
            this.Property(g => g.IsSmsSent).HasColumnName("SmsStatus");
            this.Property(g => g.OwnerUserId).HasColumnName("AssignedTo");
            this.Property(g => g.IsPrivate).HasColumnName("isPrivate");
            this.Property(g => g.IsActive).HasColumnName("Status");
            this.Property(g => g.CreatedByUserId).HasColumnName("AssignedBy");
            this.Property(g => g.LastModifiedDate).HasColumnName("UpdatedDate");

            // Relationships
            this.HasRequired(g => g.OwnerUser).WithMany().HasForeignKey(g => g.OwnerUserId);
            this.HasRequired(g => g.CreatedByUser).WithMany().HasForeignKey(g => g.CreatedByUserId);
            this.HasRequired(g => g.Group).WithMany().HasForeignKey(g => g.GroupId);
            this.HasRequired(g => g.ReminderType).WithMany().HasForeignKey(g => g.ReminderTypeId);
            this.HasOptional(g => g.Category).WithMany().HasForeignKey(g => g.CategoryId);
        }
    }
}
