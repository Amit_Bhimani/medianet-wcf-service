﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    public class MediaContactViewConfiguration : EntityTypeConfiguration<MediaContactView>
    {
        public MediaContactViewConfiguration()
        {
            this.ToTable("mn_media_contact_view");
            this.HasKey(c => new { c.Id });
            this.Property(o => o.ContactId).HasColumnName("ContactId");
            this.Property(o => o.OutletId).HasColumnName("OutletId");
            this.Property(o => o.UserId).HasColumnName("UserId");
            this.Property(o => o.IpAddress).HasColumnName("IpAddress");
            this.Property(o => o.DateViewed).HasColumnName("DateViewed");
        }
    }
}
