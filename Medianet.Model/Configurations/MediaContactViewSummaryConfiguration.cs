﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    public class MediaContactViewSummaryConfiguration : EntityTypeConfiguration<MediaContactViewSummary>
    {
        public MediaContactViewSummaryConfiguration()
        {
            this.ToTable("mn_media_contact_view_summary");
            this.HasKey(c => new { c.Id });
            this.Property(o => o.ContactId).HasColumnName("ContactId");
            this.Property(o => o.OutletId).HasColumnName("OutletId");
            this.Property(o => o.SubjectCodeId).HasColumnName("SubjectCodeId");
            this.Property(o => o.ViewCount).HasColumnName("ViewCount");
        }
    }
}
