﻿using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MediaContactsExportConfiguration : EntityTypeConfiguration<MediaContactsExport>
    {
        public MediaContactsExportConfiguration() {
            this.ToTable("mn_ContactsExport");
            this.HasKey(e => new { Id = e.Id }).Property(e => e.Id).HasColumnName("ExportId");
        }
    }
}
