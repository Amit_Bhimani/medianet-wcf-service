﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    public class MediaDatabaseLogConfiguration : EntityTypeConfiguration<MediaDatabaseLog>
    {
        public MediaDatabaseLogConfiguration()
        {
            this.ToTable("mn_OMAReport");
            this.Property(t => t.ItemID).HasColumnName("ItemID");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.Accessed).HasColumnName("Accessed");
            this.Property(t => t.OutletID).HasColumnName("OutletID");
            this.Property(t => t.ContactID).HasColumnName("ContactID");
            this.Property(t => t.FieldsUpdated).HasColumnName("FieldsUpdated");
            this.HasKey(t => t.ItemID);

            this.HasRequired(s => s.User).WithMany().HasForeignKey(s => s.UserID);
        }
    }
}
