﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MediaDraftQueueConfiguration : EntityTypeConfiguration<MediaDraftQueue>
    {
        public MediaDraftQueueConfiguration()
        {
            this.ToTable("mn_media_draft_queue");
            this.HasKey(o => new { o.Id }).Property(o => o.Id).HasColumnName("Id");
            this.Property(o => o.ContactId).HasColumnName("ContactId");
            this.Property(o => o.OutletId).HasColumnName("OutletId");
        }
    }
}
