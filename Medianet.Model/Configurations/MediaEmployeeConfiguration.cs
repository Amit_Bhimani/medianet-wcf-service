﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MediaEmployeeConfiguration : EntityTypeConfiguration<MediaEmployee>
    {
        public MediaEmployeeConfiguration() {
            this.ToTable("mn_outlet_contact_xref");
            this.HasKey(e => new {e.OutletId, e.ContactId });
            this.Property(o => o.LastModifiedByUserId).HasColumnName("LastModifiedBy");
            this.Property(o => o.HasReceivedLegals).HasColumnName("LegalsSent");
            this.Property(o => o.CreatedDate).HasColumnName("Added");
            this.Property(o => o.LastModifiedDate).HasColumnName("Updated").IsConcurrencyToken(true);
            this.Property(o => o.DeletedDate).HasColumnName("Deleted");
            this.Property(o => o.RowStatus).HasColumnName("Status");

            this.HasRequired(s => s.MediaContact).WithMany().HasForeignKey(s => s.ContactId);
            this.HasRequired(s => s.MediaOutlet).WithMany().HasForeignKey(s => s.OutletId);
            this.HasOptional(s => s.User).WithMany().HasForeignKey(s => s.LastModifiedByUserId);
            this.HasRequired(t => t.AAPCity).WithMany().HasForeignKey(d => d.CityId);
        }
    }
}
