﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MediaEmployeeDraftConfiguration : EntityTypeConfiguration<MediaEmployeeDraft>
    {
        public MediaEmployeeDraftConfiguration()
        {
            ToTable("mn_media_employee_draft");
            HasKey(o => new { o.Id }).Property(o => o.Id).HasColumnName("Id");
            Property(o => o.ContactId).HasColumnName("ContactId");
            Property(o => o.OutletId).HasColumnName("OutletId");
            
            HasRequired(t => t.DraftQueue).WithMany().HasForeignKey(d => d.QueueId);
        }
    }
}
