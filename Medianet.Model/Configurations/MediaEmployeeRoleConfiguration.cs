﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    public class MediaEmployeeRoleConfiguration : EntityTypeConfiguration<MediaEmployeeRole>
    {
        public MediaEmployeeRoleConfiguration()
        {
            this.ToTable("mn_role_contact_xref");
            this.HasKey(e => new { OutletId = e.OutletId, ContactId = e.ContactId, RoleId = e.RoleId });

            this.HasRequired(t => t.MediaEmployee)
                .WithMany(t => t.Roles)
                .HasForeignKey(d => new { OutletId = d.OutletId, ContactId = d.ContactId });
        }
    }
}
