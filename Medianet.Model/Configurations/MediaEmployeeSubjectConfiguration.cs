﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    public class MediaEmployeeSubjectConfiguration : EntityTypeConfiguration<MediaEmployeeSubject>
    {
        public MediaEmployeeSubjectConfiguration()
        {
            this.ToTable("mn_contact_subject_xref");
            this.HasKey(t => new { OutletId = t.OutletId, ContactId = t.ContactId, SubjectId = t.SubjectId });
            this.Property(t => t.SubjectId).HasColumnName("SubjectCodeId");
            this.Property(t => t.IsPrimaryContact).HasColumnName("IsPrimaryContact");

            this.HasRequired(t => t.MediaEmployee)
                .WithMany(t => t.Subjects)
                .HasForeignKey(d => new { d.OutletId, d.ContactId });
        }
    }
}
