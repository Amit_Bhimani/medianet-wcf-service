﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MediaOmaContactConfiguration : EntityTypeConfiguration<MediaOmaContact>
    {
        public MediaOmaContactConfiguration()
        {
            this.ToTable("mn_oma_contact");
            this.HasKey(e => new { ContactId = e.ContactId });

            this.Property(o => o.OmaOutletId).HasColumnName("OmaOutletId");

            this.Property(o => o.MediaOutletId).HasColumnName("MediaOutletId");

            this.Property(o => o.PrnOutletId).HasColumnName("PrnOutletId");
            
            this.Property(o => o.Prefix).HasColumnName("Prefix");

            this.Property(o => o.FirstName).HasColumnName("FirstName");

            this.Property(o => o.MiddleName).HasColumnName("MiddleName");

            this.Property(o => o.LastName).HasColumnName("LastName");

            this.Property(o => o.JobTitle).HasColumnName("JobTitle");

            this.Property(e => e.RowStatus).HasColumnName("Status");

            this.Property(e => e.OwnedBy).HasColumnName("OwnedBy");
        }
    }
}
