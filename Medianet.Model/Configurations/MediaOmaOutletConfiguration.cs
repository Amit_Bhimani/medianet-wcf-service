﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MediaOmaOutletConfiguration : EntityTypeConfiguration<MediaOmaOutlet>
    {
        public MediaOmaOutletConfiguration() {
            this.ToTable("mn_oma_outlet");
            this.HasKey(e => new { OutletId = e.OutletId });
            this.Property(e => e.CompanyName).HasColumnName("CompanyName");
            this.Property(e => e.Status).HasColumnName("Status");

            this.HasRequired(g => g.ProductType).WithMany().HasForeignKey(g => g.ProductTypeId);
        }
    }
}
