﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MediaOutletConfiguration : EntityTypeConfiguration<MediaOutlet>
    {
        public MediaOutletConfiguration() {
            this.ToTable("mn_media_outlet");
            this.HasKey(o => new { o.Id }).Property(o => o.Id).HasColumnName("OutletId");
            this.Property(o => o.Name).HasColumnName("CompanyName");
            this.Property(o => o.OwnershipCompany).HasColumnName("Ownership");
            this.Property(o => o.CreatedDate).HasColumnName("Added");
            this.Property(o => o.LastModifiedDate).HasColumnName("Updated").IsConcurrencyToken(true);
            this.Property(o => o.LastModifiedByUserId).HasColumnName("LastModifiedBy");
            this.Property(o => o.LockedByUserId).HasColumnName("LockedBy");
            this.Property(o => o.DeletedDate).HasColumnName("Deleted");
            this.Property(o => o.RowStatus).HasColumnName("Status");
            this.Property(o => o.InternalNotes).HasColumnName("InternalNotes");
            this.Property(o => o.HasReceivedLegals).HasColumnName("LegalsSent");
            this.HasOptional(s => s.User).WithMany().HasForeignKey(s => s.LastModifiedByUserId);
            this.HasRequired(t => t.AAPCity).WithMany().HasForeignKey(d => d.CityId);
            this.HasOptional(t => t.Profile).WithRequired(c => c.MediaOutlet);
            this.HasOptional(t => t.AAPPostalCity).WithMany().HasForeignKey(d => d.PostalCityId);
        }
    }
}
