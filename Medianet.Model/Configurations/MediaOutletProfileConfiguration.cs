﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    public class MediaOutletProfileConfiguration : EntityTypeConfiguration<MediaOutletProfile>
    {
        public MediaOutletProfileConfiguration()
        {
            this.ToTable("mn_outlet_profile");
            this.HasKey(o => new { o.OutletId }).Property(o => o.OutletId).HasColumnName("OutletId");
            this.Property(o => o.Content).HasColumnName("Profile");

        }
    }
}
