﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    public class MediaOutletStationFormatConfiguration : EntityTypeConfiguration<MediaOutletStationFormat>
    {
        public MediaOutletStationFormatConfiguration()
        {
            this.ToTable("mn_outlet_station_format_xref");
            this.HasKey(t => new { OutletId = t.OutletId, StationFormatId = t.StationFormatId });

            this.HasRequired(t => t.MediaOutlet)
                .WithMany(t => t.StationFormats)
                .HasForeignKey(d => d.OutletId);
        }
    }
}
