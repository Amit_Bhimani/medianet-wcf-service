﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    public class MediaOutletSubjectConfiguration : EntityTypeConfiguration<MediaOutletSubject>
    {
        public MediaOutletSubjectConfiguration()
        {
            this.ToTable("mn_outlet_subject_xref");
            this.HasKey(t => new { OutletId = t.OutletId, SubjectId = t.SubjectId });
            this.Property(t => t.SubjectId).HasColumnName("SubjectCodeId");

            this.HasRequired(t => t.MediaOutlet)
                .WithMany(t => t.Subjects)
                .HasForeignKey(d => new { d.OutletId });
        }
    }
}
