﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    public class MediaOutletWorkingLanguageConfiguration : EntityTypeConfiguration<MediaOutletWorkingLanguage>
    {
        public MediaOutletWorkingLanguageConfiguration()
        {
            this.ToTable("mn_outlet_working_language_xref");
            this.HasKey(t => new { OutletId = t.OutletId, WorkingLanguageId = t.WorkingLanguageId });

            this.HasRequired(t => t.MediaOutlet)
                .WithMany(t => t.WorkingLanguages)
                .HasForeignKey(d => new { d.OutletId });
        }
    }
}
