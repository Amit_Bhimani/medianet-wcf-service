﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MediaPriorityNoticeConfiguration : EntityTypeConfiguration<MediaPriorityNotice>
    {
        public MediaPriorityNoticeConfiguration()
        {
            this.ToTable("mn_media_priority_notice");
            this.HasKey(c => new {c.Id});
            this.Property(o => o.CreatedDate).HasColumnName("Created");
            this.Property(o => o.LastModifiedDate).HasColumnName("Updated");
            this.Property(o => o.CreatedByUserId).HasColumnName("CreatedBy");
            this.Property(o => o.LastModifiedByUserId).HasColumnName("UpdatedBy");

            // Relationships

            this.HasOptional(s => s.MediaOutlet)
                .WithMany()
                .HasForeignKey(s => s.OutletId);

            this.HasOptional(s => s.MediaContact)
                .WithMany()
                .HasForeignKey(s => s.ContactId);

            this.HasOptional(s => s.User).WithMany().HasForeignKey(s => s.CreatedByUserId);
        }
    }
}
