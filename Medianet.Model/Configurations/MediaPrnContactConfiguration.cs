﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MediaPrnContactConfiguration : EntityTypeConfiguration<MediaPrnContact>
    {
        public MediaPrnContactConfiguration() {
            this.ToTable("PrnContact");
            this.HasKey(c => new { c.ContactId }).Property(c => c.ContactId).HasColumnName("Con_Id");
            this.Property(c => c.Prefix).HasColumnName("Prefix");
            this.Property(c => c.FirstName).HasColumnName("First_Name");
            this.Property(c => c.MiddleName).HasColumnName("Middle_Name");
            this.Property(c => c.LastName).HasColumnName("SurName");
            this.Property(c => c.Suffix).HasColumnName("Suffix");    
        }
    }
}
