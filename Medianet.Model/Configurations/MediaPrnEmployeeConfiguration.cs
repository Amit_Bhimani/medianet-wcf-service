﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MediaPrnEmployeeConfiguration : EntityTypeConfiguration<MediaPrnEmployee>
    {
        public MediaPrnEmployeeConfiguration() {
            this.ToTable("PrnEmployee");
            this.HasKey(e => new { EmpId = e.EmpId }).Property(o => o.EmpId).HasColumnName("EMP_ID");
            this.Property(o => o.OutletId).HasColumnName("OTL_ID");
            this.Property(o => o.ContactId).HasColumnName("CON_ID");
            this.Property(o => o.JobTitle).HasColumnName("JOB_TITLE");
            this.Property(o => o.IsPrimaryNewsContact).HasColumnName("IS_PRIMARY_NEWS_CONTACT");
            this.Property(o => o.CreatedDate).HasColumnName("DATE_CREATED");
            this.Property(o => o.LastModifiedDate).HasColumnName("DATE_UPDATED");
            this.Property(o => o.PhoneNumber).HasColumnName("TELEPHONE");
            this.Property(o => o.FaxNumber).HasColumnName("FAX");
            this.Property(o => o.EmailAddress).HasColumnName("EMAIL");
            this.Property(o => o.AddressId).HasColumnName("ADDRESS_ID");
            this.Property(o => o.PreferredDeliveryMethod).HasColumnName("PREF_DELIVERY_TYPE");
            this.Property(o => o.Profile).HasColumnName("PROFILE");
            this.Property(o => o.SubjectNames).HasColumnName("SubjectNames");
            this.Property(o => o.RoleNames).HasColumnName("RoleNames");


            //this.HasOptional(s => s.MediaContact).WithMany().HasForeignKey(s => s.ContactId);
            //this.HasOptional(s => s.MediaOutlet).WithMany().HasForeignKey(s => s.OutletId);
        }
    }
}
