﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MediaPrnOutletConfiguration : EntityTypeConfiguration<MediaPrnOutlet>
    {
        public MediaPrnOutletConfiguration() {
            this.ToTable("PrnOutlet");
            this.HasKey(c => new { c.OutletId }).Property(c => c.OutletId).HasColumnName("Otl_Id");
            this.Property(c => c.Name).HasColumnName("Name");
            this.Property(c => c.StatusType).HasColumnName("Status_Type");  
        }
    }
}
