﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MediaRelatedOutletConfiguration : EntityTypeConfiguration<MediaRelatedOutlet>
    {
        public MediaRelatedOutletConfiguration()
        {
            this.ToTable("mn_media_related_outlets");
            this.HasKey(o => new {o.OutletId, o.RelatedOutletId });
            this.Property(o => o.OutletId).HasColumnName("OutletId");
            this.Property(o => o.RelatedOutletId).HasColumnName("RelatedOutletId");
            this.HasRequired(o => o.MediaOutlet)
               .WithMany(o => o.RelatedOutlets)
               .HasForeignKey(o => new { o.OutletId });
           
        }
    }
}
