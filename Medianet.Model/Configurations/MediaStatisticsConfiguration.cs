﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MediaStatisticsConfiguration : EntityTypeConfiguration<MediaStatistics>
    {
        public MediaStatisticsConfiguration() {
            this.ToTable("mn_media_statistics");
            this.HasKey(l => l.Id);
            this.Property(o => o.AustralasiaToday).HasColumnName("AusToday");
            this.Property(o => o.AustralasiaLastWeek).HasColumnName("AusLastWeek");
            this.Property(o => o.AustralasiaLastMonth).HasColumnName("AusLastMonth");
            this.Property(o => o.AustralasiaYearToDate).HasColumnName("AusYearToDate");
            this.Property(o => o.InternationalToday).HasColumnName("IntToday");
            this.Property(o => o.InternationalLastWeek).HasColumnName("IntLastWeek");
            this.Property(o => o.InternationalYearToDate).HasColumnName("IntYearToDate");
        }
    }
}
