﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    public class MediaSyndicatedOutletConfiguration : EntityTypeConfiguration<MediaSyndicatedOutlet>
    {
        public MediaSyndicatedOutletConfiguration()
        {
            this.ToTable("mn_media_syndicated_outlets");
            this.HasKey(o => new { o.OutletId, o.SyndicatedOutletId });
            this.Property(o => o.OutletId).HasColumnName("OutletId");
            this.Property(o => o.SyndicatedOutletId).HasColumnName("SyndicatedOutletId");
            this.HasRequired(o => o.MediaOutlet)
               .WithMany(o => o.SyndicatedOutlets)
               .HasForeignKey(o => new { o.OutletId });

        }
    }
}
