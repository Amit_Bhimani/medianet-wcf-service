﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MediumTypeConfiguration : EntityTypeConfiguration<MediumType>
    {
        public MediumTypeConfiguration()
        {
            this.ToTable("mn_medium_type");
            this.HasKey(e => new { OutletTypeId = e.OutletTypeId, ProductTypeId = e.ProductTypeId });
            this.Property(o => o.Name).HasColumnName("MediumTypeName");
        }
    }
}
