﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MnjProfileCategoryConfiguration : EntityTypeConfiguration<MnjProfileCategory>
    {
        public MnjProfileCategoryConfiguration()
        {
            this.ToTable("mnj_profile_category");
            this.HasKey(e => new { e.ProfileID, e.WebCategoryID });

            this.HasRequired(t => t.MnjProfile).WithMany(t => t.Categories).HasForeignKey(d => d.ProfileID);
        }
    }
}
