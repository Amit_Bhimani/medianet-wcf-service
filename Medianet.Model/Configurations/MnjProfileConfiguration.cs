﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MnjProfileConfiguration : EntityTypeConfiguration<MnjProfile>
    {
        public MnjProfileConfiguration() {
            this.ToTable("mnj_profile");
            this.HasKey(e => e.ProfileID);
            this.Property(o => o.ProfileName).HasColumnName("ProfileName");
            this.Property(o => o.IsDefault).HasColumnName("IsDefault");
            this.Property(o => o.EmailAlert).HasColumnName("EmailAlert");
            this.Property(o => o.EmailAddress).HasColumnName("EmailAddress");

            this.HasRequired(s => s.Categories).WithMany().HasForeignKey(s => s.ProfileID);
            this.HasRequired(s => s.Keywords).WithMany().HasForeignKey(s => s.ProfileID);
        }
    }
}
