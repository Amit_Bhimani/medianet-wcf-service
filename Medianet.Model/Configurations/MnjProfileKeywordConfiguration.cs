﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MnjProfileKeywordConfiguration : EntityTypeConfiguration<MnjProfileKeyword>
    {
        public MnjProfileKeywordConfiguration()
        {
            this.ToTable("mnj_profile_Keyword");
            this.HasKey(e => new { e.ProfileID , e.Keyword });

            this.HasRequired(t => t.MnjProfile).WithMany(t => t.Keywords).HasForeignKey(d => d.ProfileID);
        }
    }
}
