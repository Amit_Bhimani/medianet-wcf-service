﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class MnjUserSavedReleasesConfiguration : EntityTypeConfiguration<MnjUserSavedReleases>
    {
        public MnjUserSavedReleasesConfiguration()
        {
            this.ToTable("mnj_User_SavedReleases");
            this.HasKey(e => new { e.ReleaseId, e.UserId });
            this.Property(h => h.ReleaseId).HasColumnName("JobId");
            this.Property(h => h.UserId);
        }
    }
}
