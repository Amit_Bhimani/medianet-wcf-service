﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
	public class MonitoringAlertConfiguration: EntityTypeConfiguration<MonitoringAlert>
	{

		public MonitoringAlertConfiguration ()
		{
				// Primary Key
				this.HasKey(t => t.Id);

                // Properties
                this.Property(t => t.Subject)
                    .HasMaxLength(200);

				// Table & Column Mappings
				this.ToTable("mon_Alert");
				this.Property(t => t.Id).HasColumnName("Id");
				this.Property(t => t.CampaignId).HasColumnName("CampaignId");
				this.Property(t => t.FrequencyId).HasColumnName("FrequencyId");
				this.Property(t => t.Subject).HasColumnName("Subject");
				this.Property(t => t.Blurb).HasColumnName("Blurb");
				this.Property(t => t.RecipientUsers).HasColumnName("RecipientUsers");
				this.Property(t => t.CustomRecipients).HasColumnName("CustomRecipients");

                // Relationships
                this.HasRequired(t => t.Campaign)
                    .WithMany(t => t.Alerts)
                    .HasForeignKey(d => d.CampaignId);

                this.HasRequired(t => t.Frequency)
                    .WithMany(t => t.Alerts)
                    .HasForeignKey(d => d.FrequencyId);
		}
	}
}
