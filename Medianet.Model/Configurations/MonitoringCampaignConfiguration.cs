﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    public class MonitoringCampaignConfiguration : EntityTypeConfiguration<MonitoringCampaign>
    {
        public MonitoringCampaignConfiguration()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.CampaignName)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("mon_Campaign");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ProjectName).HasColumnName("ProjectName");
            this.Property(t => t.CampaignName).HasColumnName("CampaignName");
            this.Property(t => t.MonitoringCampaignId).HasColumnName("MonitoringCampaignId");
            this.Property(t => t.SearchId).HasColumnName("SearchId");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.IsPrivate).HasColumnName("IsPrivate");
            this.Property(t => t.DebtorNumber).HasColumnName("DebtorNumber");
            this.Property(t => t.UserId).HasColumnName("UserId");

            this.HasRequired(t => t.SearchCriteria)
                .WithMany(t => t.Campaigns)
                .HasForeignKey(d => d.SearchId);
        }
    }
}
