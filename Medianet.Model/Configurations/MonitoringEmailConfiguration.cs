﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    public class MonitoringEmailConfiguration : EntityTypeConfiguration<MonitoringEmail>
    {
        public MonitoringEmailConfiguration()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Articles)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("mon_Email");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.EmailDate).HasColumnName("EmailDate");
            this.Property(t => t.SearchId).HasColumnName("SearchId");
            this.Property(t => t.Articles).HasColumnName("Articles");
            this.Property(t => t.RecipientUsers).HasColumnName("RecipientUsers");
            this.Property(t => t.CustomRecipients).HasColumnName("CustomRecipients");
            this.Property(t => t.AlertId).HasColumnName("AlertId");

            this.HasRequired(t => t.SearchCriteria)
               .WithMany(t => t.Emails)
               .HasForeignKey(d => d.SearchId);
        }
    }
}
