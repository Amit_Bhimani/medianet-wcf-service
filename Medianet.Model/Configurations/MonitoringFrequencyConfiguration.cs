﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;
using System.ComponentModel.DataAnnotations;
namespace Medianet.Model.Configurations
{
    public class MonitoringFrequencyConfiguration : EntityTypeConfiguration<MonitoringFrequency>
    {
        public MonitoringFrequencyConfiguration()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            this.Property(t => t.Frequency)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("mon_Frequency");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Frequency).HasColumnName("Frequency");
        }
    }
}
