﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Configurations
{
    public class MonitoringMediaTypeConfiguration : EntityTypeConfiguration<MonitoringMediaType>
    {
        public MonitoringMediaTypeConfiguration()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.MediaTypeName)
                .IsRequired()
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("mon_MediaType");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.MediaTypeName).HasColumnName("MediaTypeName");
        }
    }
}
