﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    public class MonitoringSearchConfiguration : EntityTypeConfiguration<MonitoringSearch>
    {
        public MonitoringSearchConfiguration()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Keywords)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("mon_Search");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.IsFullText).HasColumnName("IsFullText");
            this.Property(t => t.Keywords).HasColumnName("Keywords");
            this.Property(t => t.Sources).HasColumnName("Sources");
            this.Property(t => t.Owners).HasColumnName("Owners");
            this.Property(t => t.IsMostRecent).HasColumnName("IsMostRecent");
            this.Property(t => t.StartDate).HasColumnName("StartDate");
            this.Property(t => t.EndDate).HasColumnName("EndDate");
            this.Property(t => t.CreationDate).HasColumnName("CreationDate");

            this.HasMany(t => t.MediaTypes)
                .WithMany(t => t.Searches)
                .Map(x =>
                {
                    x.ToTable("mon_SearchMediaType");
                    x.MapLeftKey("SearchId");
                    x.MapRightKey("MediaTypeId");
                });

        }
    }
}
