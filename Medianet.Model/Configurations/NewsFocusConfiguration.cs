﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    public class NewsFocusConfiguration : EntityTypeConfiguration<NewsFocus>
    {
        public NewsFocusConfiguration()
        {
            this.ToTable("mn_news_focus");
            this.HasKey(e => new { e.Id }).Property(o => o.Id).HasColumnName("NewsFocusId");
            this.Property(o => o.Name).HasColumnName("NewsFocusName");
        }
    }
}
