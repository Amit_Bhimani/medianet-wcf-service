﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    public class NoteConfiguration : EntityTypeConfiguration<Note>
    {
        public NoteConfiguration()
        {
            this.ToTable("mn_contact_notes");
            this.HasKey(c => new { c.Id }).Property(c => c.Id).HasColumnName("Id");
            this.HasRequired(t => t.User).WithMany().HasForeignKey(d => d.UserId);
        }
    }
}
