﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    public class OmaDocumentBelongsToConfiguration : EntityTypeConfiguration<OmaDocumentBelongsTo>
    {
        public OmaDocumentBelongsToConfiguration()
        {
            this.ToTable("mn_OmaDocument_xref");
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id");
            //this.Property(t => t.ContactId).HasColumnName("ContactId");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            //this.Property(t => t.OutletId).HasColumnName("OutletId");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.MediaOutletId).HasColumnName("MediaOutletId");
            this.Property(t => t.MediaContactId).HasColumnName("MediaContactId");
            this.Property(t => t.OmaOutletId).HasColumnName("OmaOutletId");
            this.Property(t => t.OmaContactId).HasColumnName("OmaContactId");
            this.Property(t => t.PrnOutletId).HasColumnName("PrnOutletId");
            this.Property(t => t.PrnContactId).HasColumnName("PrnContactId");

            this.HasRequired(b => b.OmaDocument).WithOptional().Map(m => m.MapKey("DocumentId"));
        }
    }
}
