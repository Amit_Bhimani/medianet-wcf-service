﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    public class OmaDocumentConfiguration : EntityTypeConfiguration<OmaDocument>
    {
        public OmaDocumentConfiguration()
        {
            this.ToTable("mn_OmaDocument");
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ListId).HasColumnName("ListId");
            this.Property(t => t.UploadedByUserId).HasColumnName("UserId");
            this.Property(t => t.Name).HasColumnName("DocumentName");
            this.Property(t => t.IsPrivate).HasColumnName("IsPrivate");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.DocumentContent).HasColumnName("DocumentContent");
            this.Property(t => t.DocumentType).HasColumnName("DocumentType");
        }
    }
}
