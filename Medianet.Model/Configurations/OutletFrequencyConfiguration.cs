﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    public class OutletFrequencyConfiguration : EntityTypeConfiguration<OutletFrequency>
    {
        public OutletFrequencyConfiguration()
        {
            this.ToTable("mn_outlet_frequency");
            this.HasKey(e => new { e.Id }).Property(o => o.Id).HasColumnName("FrequencyId");
            this.Property(o => o.Name).HasColumnName("FrequencyName");
        }
    }
}
