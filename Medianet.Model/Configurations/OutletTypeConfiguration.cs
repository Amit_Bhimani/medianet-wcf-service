﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class OutletTypeConfiguration : EntityTypeConfiguration<OutletType>
    {
        public OutletTypeConfiguration()
        {
            this.ToTable("mn_outlet_type");
            this.HasKey(e => new { e.Id }).Property(o => o.Id).HasColumnName("OutletTypeId");
            this.Property(o => o.Name).HasColumnName("OutletTypeName");
        }
    }
}
