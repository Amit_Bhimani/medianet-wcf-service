﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    public class PackageAttachmentConfiguration : EntityTypeConfiguration<PackageAttachment>
    {
        public PackageAttachmentConfiguration()
        {
            this.ToTable("mn_package_attachment");
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.FileName).HasColumnName("FileName");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.PackageId).HasColumnName("PackageId");
            this.Property(t => t.CreatedUserId).HasColumnName("CreatedUserId");
        }
    }
}
