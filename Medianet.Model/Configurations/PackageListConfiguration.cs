﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class PackageListConfiguration : EntityTypeConfiguration<PackageList>
    {
        public PackageListConfiguration() {
            this.ToTable("mn_package");
            this.HasKey(c => new { c.Id }).Property(p => p.Id).HasColumnName("Id");
            this.Property(p => p.System).HasColumnName("SystemType");
            this.Property(p => p.RowStatus).HasColumnName("Status");
            this.Property(p => p.ShouldCharge).HasColumnName("Charge");
            this.Property(p => p.ShouldDistribute).HasColumnName("Distribute");
            this.Property(p => p.ShouldSendResultsToOwner).HasColumnName("SendResultsToOwner");
            this.Property(p => p.ShouldUseSpecialAddressText).HasColumnName("UseSpecialAddressText");
            this.Property(p => p.Category1Id).HasColumnName("Category1");
            this.Property(p => p.Category2Id).HasColumnName("Category2");
            this.Property(p => p.IsHidden).HasColumnName("Hidden");
            this.Property(p => p.CreatedByUserId).HasColumnName("CreatedUserId");
            this.Property(p => p.LastModifiedDate).HasColumnName("ModifiedDate");
            this.Property(p => p.LastModifiedByUserId).HasColumnName("ModifiedUserId");
        }
    }
}
