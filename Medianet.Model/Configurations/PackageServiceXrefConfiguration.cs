﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class PackageServiceXrefConfiguration : EntityTypeConfiguration<PackageServiceXref>
    {
        public PackageServiceXrefConfiguration() {
            this.ToTable("mn_package_service_xref");
            this.HasKey(c => new { c.PackageId, c.ServiceId });
        }
    }
}
