﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    public class PrnContactViewConfiguration : EntityTypeConfiguration<PrnContactView>
    {
        public PrnContactViewConfiguration()
        {
            this.ToTable("mn_prn_contact_view");
            this.HasKey(c => new { c.Id });
            this.Property(o => o.ContactId).HasColumnName("ContactId");
            this.Property(o => o.OutletId).HasColumnName("OutletId");
            this.Property(o => o.UserId).HasColumnName("UserId");
            this.Property(o => o.IpAddress).HasColumnName("IpAddress");
            this.Property(o => o.DateViewed).HasColumnName("DateViewed");
        }

}
}
