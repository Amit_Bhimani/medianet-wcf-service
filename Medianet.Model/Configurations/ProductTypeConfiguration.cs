﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class ProductTypeConfiguration : EntityTypeConfiguration<ProductType>
    {
        public ProductTypeConfiguration()
        {
            this.ToTable("mn_product_type");
            this.HasKey(e => new { e.Id }).Property(o => o.Id).HasColumnName("ProductTypeId");
            this.Property(o => o.Name).HasColumnName("ProductTypeName");
        }
    }
}
