﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class RecipientConfiguration : EntityTypeConfiguration<Recipient>
    {
        public RecipientConfiguration() {
            this.ToTable("mn_recipient");
            this.HasKey(c => new { c.Id }).Property(p => p.Id).HasColumnName("RecipientId");
            //this.HasRequired(p => p.DaybookType).WithMany(p => p.Daybooks).HasForeignKey(p => p.DaybookTypeID);
        }
    }
}
