﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class RegistrationValidationConfiguration : EntityTypeConfiguration<RegistrationValidation>
    {
        public RegistrationValidationConfiguration() {
            this.ToTable("mn_registration_validation");
            this.HasKey(c => new { c.Token });
            this.Property(s => s.System).HasColumnName("SystemType");
            this.HasOptional(p => p.ReleaseUnverified);

        }
    }
}
