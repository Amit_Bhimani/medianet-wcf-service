﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class ReleaseConfiguration : EntityTypeConfiguration<Release>
    {
        public ReleaseConfiguration() {
            this.ToTable("mn_job");
            this.HasKey(c => new { c.Id }).Property(p => p.Id).HasColumnName("Id");
            this.Property(p => p.System).HasColumnName("SystemType");
            this.Property(p => p.OMAReference).HasColumnName("OMAReferenceNum");
            this.Property(p => p.IsOpen).HasColumnName("Opened");
            this.Property(p => p.OpenedDate).HasColumnName("OpenTimestamp");
            this.Property(p => p.IsFrozen).HasColumnName("Frozen");
            this.Property(p => p.IsInQueue).HasColumnName("InQueue");
            this.Property(p => p.HasHoldDate).HasColumnName("Hold");
            this.Property(p => p.HoldUntilDate).HasColumnName("HoldUntil");
            this.Property(p => p.HasEmbargoDate).HasColumnName("Embargo");
            this.Property(p => p.EmbargoUntilDate).HasColumnName("EmbargoUntil");
            this.Property(p => p.FaxTextOffsetX).HasColumnName("DocTextX");
            this.Property(p => p.FaxTextOffsetY).HasColumnName("DocTextY");
            this.Property(p => p.FaxTextPage).HasColumnName("DocTextPage");
            this.Property(p => p.WireWords).HasColumnName("Words");
            this.Property(p => p.FinanceFileId).HasColumnName("JobFileId");
            this.Property(p => p.BadTifPages).HasColumnName("BadPages");
            this.Property(p => p.ReceptionMethod).HasColumnName("ReceptionType");
            this.Property(p => p.EmailFromName).HasColumnName("OriginatorName");
            this.Property(p => p.EmailFromAddress).HasColumnName("OriginatorEmail");
            this.Property(p => p.ReportResultsToSend).HasColumnName("ReportResultsType");
            this.Property(p => p.ShouldDeduplicateJobs).HasColumnName("Dedup");
            this.Property(p => p.IsOnHoldIndefinitely).HasColumnName("HoldIndefinite");
            this.Property(p => p.IsResend).HasColumnName("Resend");
            this.Property(p => p.QuoteReferenceId).HasColumnName("QuoteReferenceID");
            this.Property(p => p.CreatedDate).HasColumnName("WhenReceived");
            this.Property(p => p.CreatedByUserId).HasColumnName("SubmittedUserId");
            this.Property(p => p.DistributedDate).HasColumnName("WhenDistributed");
            this.Property(p => p.SecurityKey).HasColumnName("SecurityKey");

            this.HasOptional(p => p.WebDetails).WithRequired();
            this.HasOptional(p => p.CreatedByUser).WithMany().HasForeignKey(p => p.CreatedByUserId);
            //this.HasMany(p => p.Transactions).WithMany(t => t.tr).Map(m => m.MapLeftKey("").HasForeignKey(s => s.DebtorNumber);
        }
    }
}
