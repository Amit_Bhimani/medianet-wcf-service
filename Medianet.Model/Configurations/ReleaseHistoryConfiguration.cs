﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class ReleaseHistoryConfiguration : EntityTypeConfiguration<ReleaseHistory>
    {
        public ReleaseHistoryConfiguration() {
            this.ToTable("mn_job_history");
            this.HasKey(h => new { h.ReleaseId, h.CreatedDate });
            this.Property(h => h.ReleaseId).HasColumnName("JobId");
            this.Property(h => h.CreatedByUserId).HasColumnName("UserId");
        }
    }
}
