﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class ReleaseUnverifiedPreviewConfiguration : EntityTypeConfiguration<ReleaseUnverifiedPreview>
    {
        public ReleaseUnverifiedPreviewConfiguration()
        {
            this.ToTable("mn_release_unverified_preview");
            this.HasKey(c => new { c.Token });

            this.HasRequired(p => p.Registration).WithRequiredDependent(p => p.ReleaseUnverified);
        }
    }
}
