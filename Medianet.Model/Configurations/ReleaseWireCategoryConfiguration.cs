﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class ReleaseWireCategoryConfiguration : EntityTypeConfiguration<ReleaseWireCategory>
    {
        public ReleaseWireCategoryConfiguration()
        {
            this.ToTable("mn_job_wire_release_category");
            this.HasKey(c => new { c.Id }).Property(p => p.Id).HasColumnName("JobWireId");
            this.Property(p => p.ReleaseId).HasColumnName("JobId");
            this.Property(p => p.CategoryId).HasColumnName("CategoryId");
        }
    }
}
