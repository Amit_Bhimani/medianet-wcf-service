﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class ResultConfiguration : EntityTypeConfiguration<Result>
    {
        public ResultConfiguration() {
            this.ToTable("mn_results");
            this.HasKey(s => new { s.ReleaseId, s.TransactionId, s.DistributionId });
            this.Property(s => s.ReleaseId).HasColumnName("JobId");
            this.Property(s => s.TransmittedTime).HasColumnName("TransmitTime");
            this.Property(s => s.CreatedDate).HasColumnName("CreatedTime");
        }
    }
}
