﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class RssLogConfiguration : EntityTypeConfiguration<RssLog>
    {
        public RssLogConfiguration()
        {
            this.ToTable("mn_rss_log");
            this.HasKey(w => new { Id = w.Id }).Property(w => w.Id).HasColumnName("Id");
            this.Property(w => w.UserId).HasColumnName("UserId");
            this.Property(w => w.CreatedDate).HasColumnName("CreatedDate");
            this.Property(w => w.WebCategoryId).HasColumnName("WebCategoryId");
            this.Property(w => w.UserAgent).HasColumnName("UserAgent");
            this.Property(w => w.UrlParams).HasColumnName("UrlParams");
            this.Property(w => w.IPAddress).HasColumnName("IPAddress");
        }
    }
}
