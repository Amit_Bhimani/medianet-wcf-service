﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class SalesRegionConfiguration : EntityTypeConfiguration<SalesRegion>
    {
        public SalesRegionConfiguration() {
            this.ToTable("mn_sales_region");
            this.HasKey(s => new { s.Id }).Property(s => s.Id).HasColumnName("RegionId");
            this.Property(s => s.Name).HasColumnName("RegionName");
            this.Property(s => s.CreatedDate).HasColumnName("CreatedTime");
            this.Property(s => s.CreatedByUserId).HasColumnName("CreatedUserId");
            this.Property(s => s.LastModifiedDate).HasColumnName("ModifiedTime");
            this.Property(s => s.LastModifiedByUserId).HasColumnName("ModifiedUserId");
        }
    }
}
