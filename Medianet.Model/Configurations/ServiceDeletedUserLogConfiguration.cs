﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class ServiceDeletedUserLogConfiguration :EntityTypeConfiguration<ServiceDeletedUserLog>
    {
        public ServiceDeletedUserLogConfiguration()
        {
            this.ToTable("mn_service_deletedUser_Log");
            this.HasKey(c => new { c.Id }).Property(p => p.Id).HasColumnName("Id");
            this.Property(p => p.OutletId).HasColumnName("OutletId");
            this.Property(p => p.ContactId).HasColumnName("ContactId");
            this.Property(p => p.DeletedTime).HasColumnName("DeletedTime");
            this.Property(p => p.ServiceId).HasColumnName("ServiceId");
        }
    
    }
}
