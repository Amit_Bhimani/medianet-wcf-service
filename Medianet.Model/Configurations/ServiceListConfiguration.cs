﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class ServiceListConfiguration : EntityTypeConfiguration<ServiceList>
    {
        public ServiceListConfiguration() {
            this.ToTable("mn_service");
            this.HasKey(s => new { s.Id }).Property(s => s.Id).HasColumnName("Id");
            this.Property(s => s.DebtorNumber).HasColumnName("Owner");
            this.Property(s => s.System).HasColumnName("SystemType");
            this.Property(s => s.AddressCount).HasColumnName("Addresses");
            this.Property(s => s.ShouldUpdateAddresses).HasColumnName("UpdateAddresses");
            this.Property(s => s.ShouldCharge).HasColumnName("Charge");
            this.Property(s => s.ShouldDistribute).HasColumnName("Distribute");
            this.Property(s => s.ShouldSendResultsToOwner).HasColumnName("SendResultsToOwner");
            this.Property(s => s.ShouldUseSpecialAddressText).HasColumnName("UseSpecialAddressText");
            this.Property(s => s.CategoryId).HasColumnName("Category");
            this.Property(s => s.IsHidden).HasColumnName("Hidden");
            this.Property(s => s.IsOnlineDB).HasColumnName("OnlineDB");
            this.Property(s => s.CreatedDate).HasColumnName("CreatedTime");
            this.Property(s => s.CreatedByUserId).HasColumnName("CreatedUserId");
            this.Property(s => s.LastModifiedDate).HasColumnName("ModifiedTime");
            this.Property(s => s.LastModifiedByUserId).HasColumnName("ModifiedUserId");
        }
    }
}
