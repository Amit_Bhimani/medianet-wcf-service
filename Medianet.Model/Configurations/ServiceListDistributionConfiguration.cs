﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class ServiceListDistributionConfiguration : EntityTypeConfiguration<ServiceListDistribution>
    {
        public ServiceListDistributionConfiguration() {
            this.ToTable("mn_service_distribution");
            this.HasKey(c => new { c.Id }).Property(p => p.Id).HasColumnName("DistributionId");
            this.Property(p => p.SequenceNumber).HasColumnName("SequenceNo");

            //this.HasRequired(p => p.DaybookType).WithMany(p => p.Daybooks).HasForeignKey(p => p.DaybookTypeID);
        }
    }
}
