﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    public class StationFormatConfiguration : EntityTypeConfiguration<StationFormat>
    {
        public StationFormatConfiguration()
        {
            this.ToTable("mn_station_format");
            this.HasKey(e => new { e.Id }).Property(o => o.Id).HasColumnName("StationFormatId");
            this.Property(o => o.Name).HasColumnName("StationFormatName");
        }
    }
}
