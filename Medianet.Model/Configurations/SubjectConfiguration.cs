﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    public class SubjectConfiguration : EntityTypeConfiguration<Subject>
    {
        public SubjectConfiguration()
        {
            this.ToTable("mn_subject");
            this.HasKey(e => new { e.Id }).Property(o => o.Id).HasColumnName("SubjectCodeId");
            this.Property(o => o.Name).HasColumnName("SubjectName");
            this.Property(o => o.Hidden).HasColumnName("Hidden");

            // Relationships
            this.HasMany(t => t.SubjectGroups)
                .WithMany(t => t.Subjects)
                .Map(m =>
                {
                    m.ToTable("mn_subjectgroup_xref");
                    m.MapLeftKey("SubjectCodeId");
                    m.MapRightKey("SubjectGroupId");
                });

        }
    }
}
