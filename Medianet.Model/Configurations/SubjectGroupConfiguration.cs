﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    public class SubjectGroupConfiguration : EntityTypeConfiguration<SubjectGroup>
    {
        public SubjectGroupConfiguration()
        {
            this.ToTable("mn_subjectgroup");
            this.HasKey(e => new { e.Id }).Property(o => o.Id).HasColumnName("SubjectGroupId");
            this.Property(o => o.Name).HasColumnName("SubjectGroupName");
        }
    }
}
