﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    public class SubjectGroupXrefConfiguration : EntityTypeConfiguration<SubjectGroupXref>
    {
        public SubjectGroupXrefConfiguration()
        {
            this.ToTable("mn_subjectgroup_xref");
            this.HasKey(c => new { c.GroupId, c.SubjectId });
        }
    }
}
