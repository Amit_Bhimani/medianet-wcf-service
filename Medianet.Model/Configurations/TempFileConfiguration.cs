﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class TempFileConfiguration : EntityTypeConfiguration<TempFile>
    {
        public TempFileConfiguration() {
            this.ToTable("mn_temp_file");
            this.HasKey(t => new { t.Id }).Property(t => t.Id).HasColumnName("TempFileID");
            this.Property(t => t.SessionId).HasColumnName("SessionId");
            this.Property(u => u.FileExtension).HasColumnName("FileExt");
            this.Property(t => t.OriginalFilename).HasColumnName("OriginalFileName");
        }
    }
}
