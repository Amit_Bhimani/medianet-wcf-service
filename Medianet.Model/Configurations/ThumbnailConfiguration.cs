﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class ThumbnailConfiguration : EntityTypeConfiguration<Thumbnail>
    {
        public ThumbnailConfiguration() {
            this.ToTable("mn_thumbnail");
            this.HasKey(u => new { Id = u.Id }).Property(p => p.Id).HasColumnName("ThumbnailId");
            this.Property(u => u.FileExtension).HasColumnName("FileExt");
            this.Property(u => u.Width).HasColumnName("ImageWidth");
            this.Property(u => u.Height).HasColumnName("ImageHeight");
            this.Property(u => u.CreatedDate).HasColumnName("CreatedTime");

            this.HasRequired(t => t.Document)
                .WithMany(t => t.Thumbnails)
                .HasForeignKey(d => new { d.DocumentId });
        }
    }
}
