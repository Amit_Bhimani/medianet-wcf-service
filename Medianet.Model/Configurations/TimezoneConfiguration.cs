﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class TimezoneConfiguration : EntityTypeConfiguration<Timezone>
    {
        public TimezoneConfiguration() {
            this.ToTable("TIMEZONE");
            this.HasKey(t => new { t.Code }).Property(t => t.Code).HasColumnName("TIMEZONE_CD");
            this.Property(t => t.Name).HasColumnName("NAME_TX");
            this.Property(u => u.Description).HasColumnName("DESCRIPTION_TX");
        }
    }
}
