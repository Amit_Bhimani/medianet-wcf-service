﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class TopPerformingListConfiguration : EntityTypeConfiguration<TopPerformingList>
    {
        public TopPerformingListConfiguration()
        {
            this.ToTable("mn_top_performing_lists");
            this.HasKey(t => new { t.Period, t.ServiceId });
        }
    }
}
