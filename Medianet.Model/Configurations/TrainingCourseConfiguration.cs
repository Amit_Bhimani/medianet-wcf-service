﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class TrainingCourseConfiguration : EntityTypeConfiguration<TrainingCourse>
    {
        public TrainingCourseConfiguration()
        {
            this.ToTable("mnTraining_Course");
            this.HasKey(c => new { c.Id }).Property(p => p.Id).HasColumnName("CourseId");
            this.Property(p => p.Title).HasColumnName("CourseTitle");

            this.HasMany(p => p.Schedules).WithRequired().HasForeignKey(p => p.TrainingCourseId);
            this.HasMany(p => p.Topics).WithRequired().HasForeignKey(p => p.TrainingCourseId);
            this.HasMany(p => p.PromoCodes).WithRequired().HasForeignKey(p => p.TrainingCourseId);
        }
    }
}
