﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class TrainingCourseContactConfiguration : EntityTypeConfiguration<TrainingCourseContact>
    {
        public TrainingCourseContactConfiguration()
        {
            this.ToTable("mnTraining_Contacts");
            this.HasKey(c => new { c.Id }).Property(p => p.Id).HasColumnName("ContactId");
            this.Property(p => p.TrainingCourseId).HasColumnName("CourseId");
            //this.HasRequired(c => c.Course).WithOptional(c => c.Contact).Map(m => m.MapKey("CourseId"));
            this.HasRequired(t => t.Course).WithMany(t => t.Contact).HasForeignKey(d => d.TrainingCourseId);
        }
    }
}
