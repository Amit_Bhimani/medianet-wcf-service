﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class TrainingCourseEnrolmentConfiguration : EntityTypeConfiguration<TrainingCourseEnrolment>
    {
        public TrainingCourseEnrolmentConfiguration()
        {
            this.ToTable("mnTraining_Enrolment");
            this.HasKey(c => new { c.Id }).Property(p => p.Id).HasColumnName("EnrolmentId");
            this.Property(u => u.FirstName).HasColumnName("FirsName");
        }
    }
}
