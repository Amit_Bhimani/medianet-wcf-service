﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class TrainingCourseLocationConfiguration : EntityTypeConfiguration<TrainingCourseLocation>
    {
        public TrainingCourseLocationConfiguration()
        {
            this.ToTable("mnTraining_Location");
            this.HasKey(c => new { c.Id }).Property(p => p.Id).HasColumnName("Location");
            this.Property(p => p.Description).HasColumnName("Description");
        }
    }
}
