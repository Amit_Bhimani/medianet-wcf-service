﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class TrainingCoursePromoCodeConfiguration : EntityTypeConfiguration<TrainingCoursePromoCode>
    {
        public TrainingCoursePromoCodeConfiguration()
        {
            this.ToTable("mnTraining_PromoCode");
            this.HasKey(c => new { c.Id }).Property(p => p.Id).HasColumnName("PromoCodeId");
            this.Property(p => p.TrainingCourseId).HasColumnName("CourseId");
        }
    }
}
