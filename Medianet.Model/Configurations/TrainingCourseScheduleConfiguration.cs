﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class TrainingCourseScheduleConfiguration : EntityTypeConfiguration<TrainingCourseSchedule>
    {
        public TrainingCourseScheduleConfiguration()
        {
            this.ToTable("mnTraining_Schedule");
            this.HasKey(c => new { c.Id }).Property(p => p.Id).HasColumnName("ScheduleId");
            this.Property(p => p.TrainingCourseId).HasColumnName("CourseID");
            this.Property(p => p.LocationId).HasColumnName("Location");

            this.HasMany(p => p.Enrolments).WithRequired().HasForeignKey(p => p.ScheduleId);
            this.HasRequired(p => p.Location).WithMany().HasForeignKey(p => p.LocationId);
        }
    }
}
