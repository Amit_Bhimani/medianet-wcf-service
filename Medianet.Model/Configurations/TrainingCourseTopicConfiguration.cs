﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class TrainingCourseTopicConfiguration : EntityTypeConfiguration<TrainingCourseTopic>
    {
        public TrainingCourseTopicConfiguration()
        {
            this.ToTable("mnTraining_Topic");
            this.HasKey(c => new { c.Id }).Property(p => p.Id).HasColumnName("TopicId");
            this.Property(p => p.TrainingCourseId).HasColumnName("CourseId");
        }
    }
}
