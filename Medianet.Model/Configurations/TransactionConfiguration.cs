﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class TransactionConfiguration : EntityTypeConfiguration<Transaction>
    {
        public TransactionConfiguration() {
            this.ToTable("mn_transaction");
            this.HasKey(u => new { Id = u.Id }).Property(p => p.Id).HasColumnName("ID");
            this.Property(u => u.ReleaseId).HasColumnName("JobId");
            this.Property(u => u.DebtorNumber).HasColumnName("Owner");
            this.Property(u => u.System).HasColumnName("SystemType");
            this.Property(u => u.AddressCount).HasColumnName("Addresses");
            this.Property(u => u.ShouldCharge).HasColumnName("Charge");
            this.Property(u => u.ShouldDistribute).HasColumnName("Distribute");
            this.Property(u => u.ShouldSendResultsToOwner).HasColumnName("SendResultsToOwner");
            this.Property(u => u.ShouldUseSpecialAddressText).HasColumnName("UseSpecialAddressText");
            this.Property(u => u.CreatedDate).HasColumnName("CreatedTime");
            this.Property(u => u.CreatedByUserId).HasColumnName("CreatedUserId");
            this.Property(u => u.LastModifiedDate).HasColumnName("ModifiedTime");
            this.Property(u => u.LastModifiedByUserId).HasColumnName("ModifiedUserId");
        }
    }
}
