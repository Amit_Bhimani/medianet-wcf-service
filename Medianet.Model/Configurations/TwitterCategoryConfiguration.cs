﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class TwitterCategoryConfiguration : EntityTypeConfiguration<TwitterCategory>
    {
        public TwitterCategoryConfiguration() {
            this.ToTable("mn_twitter_category");
            this.HasKey(u => new { Id = u.Id });
        }
    }
}
