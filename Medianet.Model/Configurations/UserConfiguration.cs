﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration() {
            this.ToTable("mn_User");
            this.HasKey(u => new { Id = u.Id }).Property(p => p.Id).HasColumnName("Id");
            this.Property(u => u.ShouldDeduplicateJobs).HasColumnName("DeduplicateJobs");
            this.Property(u => u.FirstName).HasColumnName("FullName");
            this.Property(u => u.ReportSendMethod).HasColumnName("ReportMethod");
            this.Property(u => u.ReportResultsToSend).HasColumnName("ResultsType");
            this.Property(u => u.HasDistributeWebAccess).HasColumnName("MedianetWebAccess");
            this.Property(u => u.HasMessageConnectWebAccess).HasColumnName("MessageConnectWebAccess");
            this.Property(u => u.HasMessageConnectAdminAccess).HasColumnName("AdminAccess");
            this.Property(u => u.HasJournalistsWebAccess).HasColumnName("MNJWebAccess");
            this.Property(u => u.HasAdminWebAccess).HasColumnName("AdminSiteWebAccess");
            this.Property(u => u.HasContactsWebAccess).HasColumnName("OMAAccess");
            this.Property(u => u.ContactsAccessRights).HasColumnName("OMAAccessRights");
            this.Property(u => u.HasContactsReportAccess).HasColumnName("OMAReportAccess");
            this.Property(u => u.HasReleaseWatchAccess).HasColumnName("ReleaseWatchAccess");
            this.Property(u => u.CanSendFax).HasColumnName("FaxAccess");
            this.Property(u => u.CanSendEmail).HasColumnName("EmailAccess");
            this.Property(u => u.CanSendSMS).HasColumnName("SMSAccess");
            this.Property(u => u.CanSendVoice).HasColumnName("VoiceAccess");
            this.Property(u => u.CanSendMailMerge).HasColumnName("MailMergeAccess");
            this.Property(u => u.LastLogonDate).HasColumnName("LastLogonTime");
            this.Property(u => u.CreatedByUserId).HasColumnName("CreatedUserId");
            this.Property(u => u.LastModifiedDate).HasColumnName("ModifiedDate");
            this.Property(u => u.LastModifiedByUserId).HasColumnName("ModifiedUserId");
            this.Property(u => u.RowStatus).HasColumnName("Status");
            this.Property(u => u.ReleaseCount).HasColumnName("UnverifiedReleaseCount");
            this.Property(u => u.HasViewedDistributionMessage).HasColumnName("HasViewedDistMessage");
        }
    }
}
