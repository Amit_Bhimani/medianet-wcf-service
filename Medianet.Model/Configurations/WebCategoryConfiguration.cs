﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class WebCategoryConfiguration : EntityTypeConfiguration<WebCategory>
    {
        public WebCategoryConfiguration() {
            this.ToTable("mn_web_category");
            this.HasKey(u => new { Id = u.Id }).Property(p => p.Id).HasColumnName("ID");
            this.Property(u => u.IsHighPriority).HasColumnName("HighPriority");
        }
    }
}
