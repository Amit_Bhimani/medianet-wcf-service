﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class WebReleaseConfiguration : EntityTypeConfiguration<WebRelease>
    {
        public WebReleaseConfiguration() {
            this.ToTable("mn_web_release");
            this.HasKey(u => new { ReleaseId = u.ReleaseId }).Property(u => u.ReleaseId).HasColumnName("JobID");
            this.Property(u => u.PrimaryWebCategoryId).HasColumnName("PrimaryCategory");
            this.Property(u => u.SecondaryWebCategoryId).HasColumnName("SecondaryCategory");
            this.Property(u => u.WasSentToJournalists).HasColumnName("SentToJournalists");
        }
    }
}
