﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class WebsiteConfiguration : EntityTypeConfiguration<Website>
    {
        public WebsiteConfiguration() {
            this.ToTable("rw_Website");
            this.HasKey(w => new { Id = w.Id }).Property(w => w.Id).HasColumnName("WebsiteID");
            this.Property(w => w.Logo).HasColumnName("CompanyImage");
            this.Property(w => w.LogoFilename).HasColumnName("CompanyImageName");
            this.Property(w => w.ShouldIgnoreRobotsFile).HasColumnName("IgnoreRobotsFile");
            this.Property(w => w.DestinationCode).HasColumnName("DESTINATION_CD");
            this.Property(w => w.CreatedDate).HasColumnName("CreatedTime");
            this.Property(w => w.CreatedByUserId).HasColumnName("CreatedUserID");
            this.Property(w => w.LastModifiedDate).HasColumnName("ModifiedTime");
            this.Property(w => w.LastModifiedByUserId).HasColumnName("ModifiedUserID");
        }
    }
}
