﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    class WireReleaseCategoryConfiguration : EntityTypeConfiguration<WireReleaseCategory>
    {
        public WireReleaseCategoryConfiguration()
        {
            this.ToTable("mn_wire_release_category");
            this.HasKey(c => new { c.Id }).Property(p => p.Id).HasColumnName("CategoryId");
            this.Property(p => p.Name).HasColumnName("CategoryName");
            this.Property(p => p.Destination).HasColumnName("DestinationCode");
            this.Property(p => p.Status).HasColumnName("Status");
        }
    }
}
