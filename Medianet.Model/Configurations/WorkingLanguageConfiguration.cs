﻿using System.Data.Entity.ModelConfiguration;
using Medianet.Model.Entities;

namespace Medianet.Model.Configurations
{
    public class WorkingLanguageConfiguration : EntityTypeConfiguration<WorkingLanguage>
    {
        public WorkingLanguageConfiguration()
        {
            this.ToTable("mn_working_language");
            this.HasKey(e => new { e.Id }).Property(o => o.Id).HasColumnName("WorkingLanguageId");
            this.Property(o => o.Name).HasColumnName("WorkingLanguageName");
        }
    }
}
