﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class AAPCity
    {
        public int Id { get; set; }                             // CityId
        public string Name { get; set; }                        // Name
        public string PostCode { get; set; }
        public string AreaCode { get; set; }
        public string StateName { get; set; }
        public Nullable<int> MediaAtlasCityId { get; set; }
        public int MediaAtlasCountryId { get; set; }
        public string RowStatus { get; set; }                   // Status
    }
}
