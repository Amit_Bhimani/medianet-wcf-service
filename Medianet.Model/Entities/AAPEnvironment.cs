﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class AAPEnvironment
    {
        public int UserId { get; set; }
        public string Application { get; set; }
        [Column(TypeName = "varchar")]
        public string Type { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
    }
}
