﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class City
    {
        public int CityId { get; set; }
        public string CityName { get; set; }
        public string PostCode { get; set; }
        public int? StateId { get; set; }
        public int? CountryId { get; set; }
        public int? ContinentId { get; set; }
        public string StateName { get; set; }
        public string CountryName { get; set; }
        public int? AapCityId { get; set; }
    }
}
