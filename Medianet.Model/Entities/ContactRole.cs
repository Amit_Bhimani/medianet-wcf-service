﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class ContactRole
    {
        public int RoleId { get; set; }
        public string Name { get; set; }
    }
}
