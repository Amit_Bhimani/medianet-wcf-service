﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class Country
    {
        public int MediaAtlasCountryId { get; set; }
        public string CountryName { get; set; }
        public int MediaAtlasContinentId { get; set; }
        public int DataModuleId { get; set; }
    }
}
