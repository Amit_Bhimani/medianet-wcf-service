﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class Customer
    {
        public Customer()
        {
            this.SalesRegions = new List<CustomerRegion>();
            this.BillingCodes = new List<CustomerBillingCode>();
        }

        [Column(TypeName = "varchar")] [StringLength(12)]
        public string DebtorNumber { get; set; }
        public string Name { get; set; }                                // CustomerName
        public string LogonName { get; set; }                           // CompanyLogonName
        public string FilerCode { get; set; }
        public string PIN { get; set; }
        public string SalesEmailAddress { get; set; }                   // SalesEmail
        public int? DefaultCoverpageId { get; set; }
        public string DefaultOriginatorEmailAddress { get; set; }       // OriginatorEmail
        public int? DefaultSearchCountry { get; set; }
        public string ReportSendMethod { get; set; }                    // ReportMethod
        public string ReportResultsToSend { get; set; }                 // ResultsType
        public bool ShouldSendResultsData { get; set; }                 // SendResultsData
        public string OptOutMethod { get; set; }
        public bool ShouldSendOptOutForFax { get; set; }                // OptoutFax
        public bool ShouldSendOptOutForEmail { get; set; }              // OptoutEmail
        public bool ShouldSendOptOutForSMS { get; set; }                // OptoutSMS
        public string TimezoneCode { get; set; }                        // Timezone
        public string AccountType { get; set; }
        public bool MustUseCreditcard { get; set; }
        public bool UseMedianetRewrite { get; set; }
        public bool HasTransactions { get; set; }
        public DateTime? TrendWatcherExpiryDate { get; set; }
        public bool? CanExportContacts { get; set; }

        public string MediaDirectorySystem { get; set; }
        public Int16 MediaContactsLicenseCount { get; set; }            // NoOfLicenses
        public string MediaContactsMasterUserId { get; set; }           // MasterUserID
        public string MediaContactsMasterPassword { get; set; }         // MasterPassword
        
        public string Comment { get; set; }
        public bool? HasViewedTermsAndConditionsP { get; set; }
        public bool? HasViewedTermsAndConditionsC { get; set; }
        public bool? HasViewedTermsAndConditionsM { get; set; }

        public DateTime CreatedDate { get; set; }                       // CreatedTime
        public int CreatedByUserId { get; set; }                        // CreatedUserId
        public DateTime LastModifiedDate { get; set; }                  // ModifiedTime
        public int LastModifiedByUserId { get; set; }                   // ModifiedUserId
        [Column(TypeName = "char")]
        public string RowStatus { get; set; }                           // Status

        public virtual Timezone Timezone { get; set; }
      
        public virtual ICollection<CustomerRegion> SalesRegions { get; set; }
        public virtual ICollection<CustomerBillingCode> BillingCodes { get; set; }
    }
}
