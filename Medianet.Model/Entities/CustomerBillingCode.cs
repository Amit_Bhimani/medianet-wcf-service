﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class CustomerBillingCode
    {
        public int Id { get; set; }
        [Column(TypeName = "varchar")]
        [StringLength(12)]
        public string DebtorNumber { get; set; }
        public string Name { get; set; }
        public int SequenceNumber { get; set; }
        public virtual Customer Customer { get; set; }
    }
}
