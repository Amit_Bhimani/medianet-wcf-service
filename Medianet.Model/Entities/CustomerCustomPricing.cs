﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class CustomerCustomPricing
    {
        public int Id { get; set; }
        public string DebtorNumber { get; set; }
        public int BrsContractId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Comment { get; set; }
        public int BrsServiceId { get; set; }
        public int BrsServiceAddressId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string TransactionType { get; set; }
        public decimal CostPerUnit { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
    }
}
