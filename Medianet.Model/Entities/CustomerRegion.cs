﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class CustomerRegion
    {
        [Column(TypeName = "varchar")]
        [StringLength(12)]
        public string DebtorNumber { get; set; }
        public int RegionId { get; set; }
        public virtual Customer Customer { get; set; }
    }
}
