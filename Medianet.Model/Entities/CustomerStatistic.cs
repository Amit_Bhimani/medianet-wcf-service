﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class CustomerStatistic
    {
        [Column(TypeName = "varchar")]
        [StringLength(12)]
        public string DebtorNumber { get; set; }
        public int LastMonthTotal { get; set; }
        public int PreviousMonthTotal { get; set; }
        public decimal AveragePerMonth { get; set; }
        public decimal AveragePerMonthAll { get; set; }
        public decimal PercentageUpSinceLastMonth { get; set; }
        public decimal PercentageUpVsOthers { get; set; }
        public string BusiestDayOfWeek { get; set; }
        public int? DaysSinceLastSent { get; set; }
        public DateTime CreatedDate { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
