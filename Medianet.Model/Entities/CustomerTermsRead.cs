﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class CustomerTermsRead
    {
        public int Id { get; set; }
        [Column(TypeName = "varchar")]
        [StringLength(12)]
        public string DebtorNumber { get; set; }
        public int ReadByUserId { get; set; }
        public DateTime ReadDate { get; set; }
        public string EmailSentTo { get; set; }
        public string System { get; set; }                  // SystemType
    }
}
