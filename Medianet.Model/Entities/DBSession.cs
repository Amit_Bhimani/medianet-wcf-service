﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class DBSession
    {
        public DBSession() {
            CompanyName = string.Empty;
        }

        public int Id { get; set; }                         // SessionID
        public int UserId { get; set; }                     // UserID
        [Column(TypeName = "varchar")] [StringLength(12)]
        public string DebtorNumber { get; set; }
        public string CompanyName { get; set; }
        [Column(TypeName = "char")]
        public string System { get; set; }                  // SystemType
        [Column(TypeName = "char")]
        public string RowStatus { get; set; }               // Status
        public DateTime LoginDate { get; set; }             // LoginTime
        public DateTime LastAccessedDate { get; set; }      // LastAccessTime
        public bool IsImpersonating { get; set; }
        public bool IsAuthorised { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual User User { get; set; }
    }
}
