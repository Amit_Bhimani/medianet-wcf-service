﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class DayBook
    {
        public int Id { get; set; }        
        public string Text { get; set; }
        public DateTime? Date { get; set; }
        public DateTime LastModified { get; set; }
        public int LastModifiedByUserId { get; set; }
        public int DaybookTypeID { get; set; }

        public virtual DayBookType DaybookType { get; set; }
    }
}
