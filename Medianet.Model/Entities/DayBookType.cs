﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class DayBookType
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string URI { get; set; }
        public bool IsDateType { get; set; }

        public virtual ICollection<DayBook> Daybooks { get; set; }
    }
}
