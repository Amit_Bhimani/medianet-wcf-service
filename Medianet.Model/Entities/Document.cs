﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class Document
    {
        public int Id { get; set; }                                     // DocumentId
        public int? ReleaseId { get; set; }                             // JobId
        public int SequenceNumber { get; set; }
        public string FileExtension { get; set; }                       // FileExt
        public bool IsDerived { get; set; }                             // Derived
        public string OriginalFilename { get; set; }                    // OriginalFileName
        public string Description { get; set; }
        public bool CanDistributeViaFax { get; set; }                   // DistributionFax
        public bool CanDistributeViaEmailBody { get; set; }             // DistributionEmailBody
        public bool CanDistributeViaEmailAttachment { get; set; }       // DistributionEmailAttachment
        public bool CanDistributeViaWire { get; set; }                  // DistributionWire
        public bool CanDistributeViaVoice { get; set; }                 // DistributionVoice
        public bool CanDistributeViaWeb { get; set; }                   // DistributionWeb
        public bool CanDistributeViaSMS { get; set; }                   // DistributionSMS
        public bool IsMailMergeDataSource { get; set; }                 // MailMergeDataSource
        public int ItemCount { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public bool IsTabulated { get; set; }                           // Tabulated
        public string MediaReferenceId { get; set; }
        public string ContentType { get; set; }

        public DateTime CreatedDate { get; set; }                       // CreatedTime
        public int CreatedByUserId { get; set; }                        // CreatedUserId
        public DateTime LastModifiedDate { get; set; }                  // ModifiedTime
        public int LastModifiedByUserId { get; set; }                   // ModifiedUserId

        public virtual List<Thumbnail> Thumbnails { get; set; }
    }
}
