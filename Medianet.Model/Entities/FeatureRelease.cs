﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class FeatureRelease
    {
        public int Id { get; set; }
        public int? WebCategoryId { get; set; }
        public int SequenceNumber { get; set; }
        public int ReleaseId { get; set; }      // JobId
        public string Summary { get; set; }

        public virtual Release Release { get; set; }
    }
}
