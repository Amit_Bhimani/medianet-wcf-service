﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class InboundFile
    {
        public int Id { get; set; }                     // MessageID
        public string Caller { get; set; }
        public string DistributionType { get; set; }
        public string FileExtension { get; set; }       // FileExt
        public string OriginalFilename { get; set; }    // ExternalFileName
        public int? OwnerUserId { get; set; }           // OwnerUserID
        public string InboundNumber { get; set; }
        public DateTime? LastForwardedDate { get; set; }// ForwardTime
        public int ForwardedCount { get; set; }         // ForwardCount
        public int ItemCount { get; set; }              // Itemcount
        public int? FinanceFileId { get; set; }         // JobFileID
        public int? FileSize { get; set; }              // Size
        public int? Length { get; set; }                // Length
        [Column(TypeName = "char")]
        public string Status { get; set; }
        [Column(TypeName = "char")]
        public string Type { get; set; }

        public DateTime CreatedDate { get; set; }       // CreatedTime
        public int CreatedByUserId { get; set; }        // CreatedUserID
        public DateTime LastModifiedDate { get; set; }  // ModifiedTime
        public int LastModifiedByUserId { get; set; }   // ModifiedUserID
    }
}
