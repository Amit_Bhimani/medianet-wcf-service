﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class InboundFileHistory
    {
        public int Id { get; set; }
        public int InboundFileId { get; set; }          // MessageID
        public int ReleaseId { get; set; }              // JobId
        public string Comment { get; set; }
        public DateTime CreatedDate { get; set; }       // Timestamp
    }
}
