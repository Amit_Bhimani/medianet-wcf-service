﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class ListCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }                // CategoryName
        public string Comment { get; set; }
        public int? ParentId { get; set; }
        public int SequenceNumber { get; set; }
        [Column(TypeName = "char")]
        public string IsHidden { get; set; }            // Hidden
        public bool IsRadioListCategory { get; set; }   // Radio
    }
}
