﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class ListComment
    {
        public int Id { get; set; }                     // CommentId
        public int? ServiceId { get; set; }
        public int? PackageId { get; set; }
        public string Comment { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedByUserId { get; set; }        // CreatedUserId
    }
}
