﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class ListDistribution
    {
        public int Id { get; set; }
        public int ServiceId { get; set; }          // ServiceID
        public string OutletName { get; set; }      // Recipient
    }
}
