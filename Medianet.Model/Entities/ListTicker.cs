﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class ListTicker
    {
        public int ListCategoryId { get; set; }
        public int ListCategoryParentSequenceNumber { get; set; }
        public int ListCategorySequenceNumber { get; set; }
        public string ListCategoryName { get; set; }

        public int ListId { get; set; }
        public Int16 ListSequenceNumber { get; set; }
        public string ListDistributionType { get; set; }
        public string ListDescription { get; set; }
    }
}
