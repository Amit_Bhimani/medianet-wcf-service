﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class LogonLog
    {
        public DateTime LoginDate { get; set; }             // WhenLogged
        public int UserId { get; set; }
        public string System { get; set; }                  // SystemType
        public int ErrorCode { get; set; }
    }
}
