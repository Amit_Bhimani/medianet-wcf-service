﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medianet.Model.Entities
{
    public class MNJProfileRelease
    {
        public int ReleaseId { get; set; }// JobID

        public string Organisation { get; set; }

        public string Headline { get; set; }

        public int DocumentId { get; set; }

        public int DocumentSequenceNumber { get; set; }

        public string DocumentMediaReferenceId { get; set; }

        public string WieckReference { get; set; }

        public int? WieckThumbnailWidth { get; set; }

        public int? WieckThumbnailHeight { get; set; }

        public int SecurityKey { get; set; }

        public string MultimediaType { get; set; }

        public string ContentType { get; set; }

        public DateTime JournalistsDistributedDate { get; set; }
    }
}
