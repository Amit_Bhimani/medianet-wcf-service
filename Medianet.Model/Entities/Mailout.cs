﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class Mailout
    {
        public int Id { get; set; }                             
        public DateTime TransmitTime { get; set; }
        public int? RecipientNumber { get; set; }
        public string Status { get; set; }
    }
}
