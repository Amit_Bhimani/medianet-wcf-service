﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class MailoutRecipient
    {
        public int Id { get; set; }                             
        public int MailoutId { get; set; }
        public string OutletId { get; set; }
        public string ContactId { get; set; }
        public string Status { get; set; }
        public string ErrorMessage { get; set; }
        public DateTime OpendTime { get; set; }
        public DateTime UpdatedTime { get; set; }
        public DateTime ViewedTime { get; set; }
    }
}
