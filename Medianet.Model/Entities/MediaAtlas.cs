﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MediaAtlas
    {
        public string Prefix { get; set; }
        public decimal Id { get; set; }
    }
}
