﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MediaContact
    {
        public string Id { get; set; }
        public string Prefix { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string InternalNotes { get; set; }
        public string Bio { get; set; }
        //Pitching Profile Fields
        public string BugBears { get; set; }
        public string AlsoKnownAs { get; set; }
        public string PressReleaseInterests { get; set; }
        public string Awards { get; set; }
        public string PersonalInterests { get; set; }
        public string PoliticalParty { get; set; }
        public string NamePronunciation { get; set; }
        public string AppearsIn { get; set; }
//        public string BasedInLocation { get; set; }
//        public string Deadlines { get; set; }
        public string BestContactTime { get; set; }
        public string BusyTimes { get; set; }
        public string AuthorOf { get; set; }
        public bool? GiftsAccepted { get; set; }
        public string CoffeeOrder { get; set; }
        public string PitchingDos { get; set; }
        public string PitchingDonts { get; set; }
        //End Of Pitching Profile Fields

        public int? MediaInfluencerScore { get; set; }
        public bool? IsDirty { get; set; }
        public DateTime CreatedDate { get; set; }               // Added
        public DateTime LastModifiedDate { get; set; }          // Updated
        public int? LastModifiedByUserId { get; set; }          // LastModifiedBy
        public int? LockedByUserId { get; set; }                // LockedBy
        public DateTime? DeletedDate { get; set; }              // Deleted
        public string RowStatus { get; set; }                   // Status
        public string Facebook { get; set; }
        public string LinkedIn { get; set; }
        public string Twitter { get; set; }
        public string Instagram { get; set; }
        public string YouTube { get; set; }
        public string Snapchat { get; set; }
        public string Skype { get; set; }
        public string InterviewLink { get; set; }
        public string InterviewPhotoLink { get; set; }
        public virtual User User { get; set; }
        public virtual MediaContactProfile Profile { get; set; }
        public virtual ICollection<MediaContactJobHistory> JobHistory { get; set; }
    }
}
