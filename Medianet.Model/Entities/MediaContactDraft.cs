﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class MediaContactDraft
    {
        public int Id { get; set; }
        public string ContactId { get; set; }
        public int QueueId { get; set; }
        public string Prefix { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Bio { get; set; }
        public string BugBears { get; set; }
        public string AlsoKnownAs { get; set; }
        public string PressReleaseInterests { get; set; }
        public string Awards { get; set; }
        public string PersonalInterests { get; set; }
        public string PoliticalParty { get; set; }
        public string NamePronunciation { get; set; }
        public string AppearsIn { get; set; }
        public string Profile { get; set; }
        public string BestContactTime { get; set; }
        public string BusyTimes { get; set; }
        public string AuthorOf { get; set; }
        public string GiftsAccepted { get; set; }
        public string CoffeeOrder { get; set; }
        public string PitchingDos { get; set; }
        public string PitchingDonts { get; set; }
        public string Facebook { get; set; }
        public string LinkedIn { get; set; }
        public string Twitter { get; set; }
        public string Instagram { get; set; }
        public string YouTube { get; set; }
        public string Snapchat { get; set; }
        public string Skype { get; set; }
        public virtual MediaDraftQueue DraftQueue { get; set; }
    }
}
