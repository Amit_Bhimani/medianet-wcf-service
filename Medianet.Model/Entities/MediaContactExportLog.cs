﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MediaContactExportLog
    {
        public int Id { get; set; }
        public string DebtorNumber { get; set; }
        public int UserId { get; set; }
        public int? ListId { get; set; }
        public int? TaskId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int NoOfExportedRows { get; set; }
        public string ExportedFields { get; set; }
    }
}
