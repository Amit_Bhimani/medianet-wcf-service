﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class MediaContactGroup
    {
        public int Id { get; set; }                             // GroupId
        public string Name { get; set; }                        // GroupName
        public byte? GroupType { get; set; }                     // GroupTypeId
        public byte IsActive { get; set; }                      // Status
        public DateTime CreatedDate { get; set; }
        public int CreatedByUserId { get; set; }                // userid
        public DateTime LastModifiedDate { get; set; }          // UpdatedDate

        public virtual User CreatedByUser { get; set; }
    }
}
