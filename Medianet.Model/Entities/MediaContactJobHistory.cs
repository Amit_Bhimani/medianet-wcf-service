﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class MediaContactJobHistory
    {
        public int Id { get; set; }
        public string ContactId { get; set; }
        public string OutletId { get; set; }
        public string OutletName { get; set; }
        public string JobTitle { get; set; }
        public int StartYear { get; set; }
        public int? StartMonth { get; set; }

        public int? EndYear { get; set; }
        public int? EndMonth { get; set; }

        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public virtual MediaContact MediaContact { get; set; }
    }
}
