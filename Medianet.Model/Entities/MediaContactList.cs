﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class MediaContactList
    {
        public int Id { get; set; }                             // ListId
        public string Name { get; set; }                        // ListName
        public int GroupId { get; set; }
        public int OwnerUserId { get; set; }                    // UserId
        public bool IsPrivate { get; set; }
        public bool IsActive { get; set; }                      // Status
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }          // UpdatedDate

        public virtual User OwnerUser { get; set; }
        public virtual MediaContactGroup Group { get; set; }
        //public virtual List<MediaContactListVersion> Versions { get; set; }
    }
}
