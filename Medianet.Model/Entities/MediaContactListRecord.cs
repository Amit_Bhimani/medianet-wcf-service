﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class MediaContactListRecord
    {
        public int Id { get; set; }                             // RecordId
        public int ListId { get; set; }
        public string MediaOutletId { get; set; }
        public string MediaContactId { get; set; }
        public int? OmaOutletId { get; set; }
        public int? OmaContactId { get; set; }
        public decimal? PrnOutletId { get; set; }
        public decimal? PrnContactId { get; set; }
        public bool IsEntityDeleted { get; set; }
        public DateTime? EntityDeletedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedByUserId { get; set; }                // UserId
    }
}
