﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MediaContactListRecordDetails
    {
        public int RecordId { get; set; }
        public int RecordType { get; set; }
        public string OutletId { get; set; }
        public int UserId { get; set; }
        public string OutletName { get; set; }
        public string ContactId { get; set; }
        public string FullName { get; set; }
        public string JobTitle { get; set; }
        public string SubjectName { get; set; }
        public string ProductTypeName { get; set; }
        public string RoleName { get; set; }
        public string CityName { get; set; }
        public int IsEntityDeleted { get; set; }
        public int? MediaInfluencerScore { get; set; }
        public int IsGeneric { get; set; }
    }
}
