﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class MediaContactListVersion
    {
        public int Id { get; set; }                             // ListSetId
        public int ListId { get; set; }
        public int Version { get; set; }
        public string Name { get; set; }                        // ListNameVersioned
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }          // UpdatedDate
    }
}
