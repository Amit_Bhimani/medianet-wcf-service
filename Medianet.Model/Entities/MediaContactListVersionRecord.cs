﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class MediaContactListVersionRecord
    {
        public int Id { get; set; }
        public int RecordId { get; set; }
        public int ListSetId { get; set; }
        [Column(TypeName = "char")]
        public string ChangeType { get; set; }
        public string MediaOutletId { get; set; }
        public string MediaContactId { get; set; }
        public int? OmaOutletId { get; set; }
        public int? OmaContactId { get; set; }
        public decimal? PrnOutletId { get; set; }
        public decimal? PrnContactId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedByUserId { get; set; }                // CreatedUserId
        public DateTime LastModifiedDate { get; set; }          // ModifiedDate
    }
}
