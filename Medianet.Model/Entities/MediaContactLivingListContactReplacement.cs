﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MediaContactLivingListContactReplacement
    {
        public int RId { get; set; }
        public int ListId { get; set; }
        public int ListSetId { get; set; }
        public string ListName { get; set; }
        public string OutletIdAap { get; set; }
        public string OutletNameAap { get; set; }
        public string ContactIdAap { get; set; }
        public string ContactNameAap { get; set; }
        public string OutletIdOma { get; set; }
        public string OutletNameOma { get; set; }
        public string ContactIdOma { get; set; }
        public string ContactNameOma { get; set; }
        public string OutletIdPrn { get; set; }
        public string OutletNamePrn { get; set; }
        public string ContactIdPrn { get; set; }
        public string ContactNamePrn { get; set; }
        public string ContactReplacementName { get; set; }

        public string ContactReplacementId { get; set; }

    }
}
