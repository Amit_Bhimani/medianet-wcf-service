﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MediaContactLivingListContacts
    {
        public string ContactId { get; set; }
        public string ContactName { get; set; }
        public string JobTitle { get; set; }
    }
}
