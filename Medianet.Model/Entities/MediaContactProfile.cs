﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MediaContactProfile
    {
        public int Id { get; set; }
        //public string ContactId { get; set; }
        public string Content { get; set; }
        public DateTime Updated { get; set; }
        public virtual MediaContact MediaContact { get; set; }
    }
}
