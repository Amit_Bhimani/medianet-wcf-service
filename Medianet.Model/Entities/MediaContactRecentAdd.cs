﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medianet.Model.Entities
{
    public class MediaContactRecentAdd :MediaContactRecentChange
    {
        public DateTime Added { get; set; }
    }
}
