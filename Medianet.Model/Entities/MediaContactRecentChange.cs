﻿using System;

namespace Medianet.Model.Entities
{
    public class MediaContactRecentChange
    {
        public string OutletId { get; set; }
        public string ContactId { get; set; }
        public int RecordType { get; set; }
        public string Prefix { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string JobTitle { get; set; }
        public string CompanyName { get; set; }
        public string LogoFileName { get; set; }
        public int MediaType { get; set; }
        public string Twitter { get; set; }
    }
}
