﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medianet.Model.Entities
{
    public class MediaContactRecentUpdate: MediaContactRecentChange
    {
        public DateTime Updated { get; set; } 
    }
}
