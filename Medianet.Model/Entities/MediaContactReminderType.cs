﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class MediaContactReminderType
    {
        public int Id { get; set; }
        public string Name { get; set; }                        // RemType
        public int Minutes { get; set; }
        public byte IsActive { get; set; }                      // Status
        public DateTime CreatedDate { get; set; }
    }
}
