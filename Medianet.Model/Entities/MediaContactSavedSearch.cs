﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class MediaContactSavedSearch
    {
        public int Id { get; set; }                             // SearchId
        public string Name { get; set; }                        // SearchName
        public int GroupId { get; set; }
        public int OwnerUserId { get; set; }                    // UserId
        [Column(TypeName = "char")]
        public string Context { get; set; }                     // ContextText
        public string SearchCriteria { get; set; }
        public int? ResultsCount { get; set; }
        public bool IsPrivate { get; set; }                     // Visibility
        public byte IsActive { get; set; }                      // Status
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }          // ModifiedDate

        public virtual User OwnerUser { get; set; }
        public virtual MediaContactGroup Group { get; set; }
    }
}
