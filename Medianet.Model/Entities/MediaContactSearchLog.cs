﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MediaContactSearchLog
    {
        public int Id { get; set; }
        public string DebtorNumber { get; set; }
        public int UserId { get; set; }
        public string SearchCriteria { get; set; }
        public int SearchType { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? SavedSearchId { get; set; }
        public string System { get; set; }
        public string SearchContext { get; set; }
        public int? ResultsCount { get; set; }
    }
}
