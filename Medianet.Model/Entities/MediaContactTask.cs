﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class MediaContactTask
    {
        public int Id { get; set; }
        public string Name { get; set; }                        // TaskName
        public string Description { get; set; }                 // TaskDescription
        public int GroupId { get; set; }
        public int ReminderTypeId { get; set; }                 // ReminderDateTimeId
        public int NotificationType { get; set; }
        public DateTime DueDate { get; set; }
        public int StatusType { get; set; }                     // StatusId
        public int? CategoryId { get; set; }
        public bool IsEmailSent { get; set; }                   // EmailStatus
        public bool IsSmsSent { get; set; }                     // SmsStatus
        public int OwnerUserId { get; set; }                    // AssignedTo
        public bool IsPrivate { get; set; }                     // isPrivate
        public bool IsActive { get; set; }                      // Status
        public DateTime CreatedDate { get; set; }
        public int CreatedByUserId { get; set; }                // AssignedBy
        public DateTime LastModifiedDate { get; set; }          // UpdatedDate

        public virtual User OwnerUser { get; set; }
        public virtual User CreatedByUser { get; set; }
        public virtual MediaContactGroup Group { get; set; }
        public virtual MediaContactReminderType ReminderType { get; set; }
        public virtual MediaContactTaskCategory Category { get; set; }
    }
}
