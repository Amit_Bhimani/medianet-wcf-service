﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class MediaContactTaskCategory
    {
        public int Id { get; set; }                             // CategoryId
        public string Name { get; set; }                        // CategoryName
        public int OwnerUserId { get; set; }                    // UserId
        public bool IsActive { get; set; }                      // Status
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }          // UpdatedDate
    }
}
