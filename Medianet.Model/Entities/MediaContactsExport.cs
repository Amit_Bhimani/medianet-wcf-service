﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class MediaContactsExport
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [Column(TypeName = "varchar")] [StringLength(12)]
        public string DebtorNumber { get; set; }
        public int UserId { get; set; }
        public string ExportFields { get; set; }
        public bool IsPrivate { get; set; }
        public bool IsDeleted { get; set; }
        public int ExportType { get; set; }

        public DateTime CreatedDate { get; set; }
        public int CreatedByUserId { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public int LastModifiedByUserId { get; set; }
    }
}
