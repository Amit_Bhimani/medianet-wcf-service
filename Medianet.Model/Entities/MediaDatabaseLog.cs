﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MediaDatabaseLog
    {
        public int ItemID { get; set; }
        public int UserID { get; set; }
        public System.DateTime Accessed { get; set; }
        public string OutletID { get; set; }
        public string ContactID { get; set; }
        public string FieldsUpdated { get; set; }
        public int? DraftQueueId { get; set; }
        public bool? ProfileViewed { get; set; }

        public virtual User User { get; set; }

    }
}
