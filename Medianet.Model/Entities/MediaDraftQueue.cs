﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class MediaDraftQueue
    {
        public int Id { get; set; }
        public string ContactId { get; set; }
        public string OutletId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public int? LastModifiedBy { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
        
        public string JournalistComment { get; set; }

        public bool SkipSendingEmailToJournalist { get; set; }
    }
}
