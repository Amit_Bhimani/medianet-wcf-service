﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class MediaDraftQueueView
    {
        public int Id { get; set; }
        public string ContactId { get; set; }
        public string OutletId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public int? LastModifiedBy { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string ContactName { get; set; }
        public string OutletName { get; set; }
    }
}
