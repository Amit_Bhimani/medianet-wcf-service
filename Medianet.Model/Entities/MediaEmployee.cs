﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MediaEmployee
    {
        public string OutletId { get; set; }
        public string ContactId { get; set; }
        public string JobTitle { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public int CityId { get; set; }
        public int CountryId { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string MobileNumber { get; set; }
        public string EmailAddress { get; set;}
        public string BlogURL { get; set; }
        public string PreferredDeliveryMethod { get; set; }
        public string OutletName { get; set; }
        public bool IsPrimaryNewsContact { get; set; }
        public bool HasReceivedLegals { get; set; }             // LegalsSent
        public string TrackingReference { get; set; }
        public string CurrentStatus { get; set; }
        public string BasedInLocation { get; set; }
        public string Deadlines { get; set; }
        public string AlternativeEmailAddress { get; set; }
        public string AdditionalMobile { get; set; }
        public string AdditionalPhoneNumber { get; set; }
        public string Website { get; set; }
        public string OfficeHours { get; set; }

        // De-normalised data used by the Contacts website.
        public string SubjectNames { get; set; }
        //public string RoleNames { get; set; }

        public DateTime CreatedDate { get; set; }               // Added
        //public int CreatedByUserId { get; set; }              // CreatedUserId
        public DateTime LastModifiedDate { get; set; }          // Updated
        public int? LastModifiedByUserId { get; set; }          // LastModifiedBy
        public DateTime? DeletedDate { get; set; }              // Deleted
        public string RowStatus { get; set; }                   // Status
        public bool? CurrentlyOutOfOffice { get; set; }
        public DateTime? OutOfOfficeReturnDate { get; set; }
        public DateTime? OutOfOfficeStartDate { get; set; }

        public virtual User User { get; set; }
        public virtual MediaContact MediaContact { get; set; }
        public virtual MediaOutlet MediaOutlet { get; set; }
        public virtual AAPCity AAPCity { get; set; }

        public virtual ICollection<MediaEmployeeRole> Roles { get; set; }
        public virtual ICollection<MediaEmployeeSubject> Subjects { get; set; }
        public virtual ICollection<MediaContactMovement> Movements { get; set; }
    }
}
