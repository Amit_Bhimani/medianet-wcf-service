﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class MediaEmployeeDraft
    {
        public int Id { get; set; }
        public string ContactId { get; set; }
        public int QueueId { get; set; }
        public string OutletId { get; set; }  
        public string JobTitle { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public int? CityId { get; set; }
        public int? StateId { get; set; }
        public int? CountryId { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string MobileNumber { get; set; }
        public string EmailAddress { get; set; }
        public string BlogURL { get; set; }
        public string PreferredDeliveryMethod { get; set; }
        public bool? IsPrimaryNewsContact { get; set; }
        public string TrackingReference { get; set; }
        public string CurrentStatus { get; set; }
        public string BasedInLocation { get; set; }
        public string Deadlines { get; set; }
        public string AlternativeEmailAddress { get; set; }
        public string AdditionalMobile { get; set; }
        public string AdditionalPhoneNumber { get; set; }
        public string Website { get; set; }
        public string OfficeHours { get; set; }
        
        public string Roles { get; set; }
        public string Subjects { get; set; }
        public string AdditionalSubjects { get; set; }
        public bool? CurrentlyOutOfOffice { get; set; }
        public DateTime? OutOfOfficeReturnDate { get; set; }
        public DateTime? OutOfOfficeStartDate { get; set; }

        public virtual MediaDraftQueue DraftQueue { get; set; }
    }
}
