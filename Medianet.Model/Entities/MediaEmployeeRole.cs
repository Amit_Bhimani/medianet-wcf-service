﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MediaEmployeeRole
    {
        public string OutletId { get; set; }
        public string ContactId { get; set; }
        public int RoleId { get; set; }
        public virtual MediaEmployee MediaEmployee { get; set; }
    }
}
