﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MediaEmployeeSubject
    {
        public string OutletId { get; set; }
        public string ContactId { get; set; }
        public int SubjectId { get; set; }                          // SubjectCodeId
        public bool IsPrimaryContact { get; set; }
        public virtual MediaEmployee MediaEmployee { get; set; }
    }
}
