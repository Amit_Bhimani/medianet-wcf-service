﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    [NotMapped]
    public class MediaEmployeeView 
    {
        public string ContactId { get; set; }
        public string Prefix { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string JobTitle { get; set; }
        public string PhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public string FaxNumber { get; set; }
        public string EmailAddress { get; set; }

        public string AlternativeEmailAddress { get; set; }
        public string AdditionalMobile { get; set; }
        public string AdditionalPhoneNumber { get; set; }
        public string OfficeHours { get; set; }

        public string Bio { get; set; }
        public int? MediaInfluencerScore { get; set; }
        public string Blog { get; set; }
        public string Web { get; set; }
        public string Facebook { get; set; }
        public string LinkedIn { get; set; }
        public string Twitter { get; set; }
        public string Instagram { get; set; }
        public string Youtube { get; set; }
        public string SnapChat { get; set; }
        public string Skype { get; set; }
        public string Name { get; set; }

        public string BugBears { get; set; }
        public string AlsoKnownAs { get; set; }
        public string PressReleaseInterests { get; set; }
        public string Awards { get; set; }
        public string PersonalInterests { get; set; }
        public string PoliticalParty { get; set; }
        public string NamePronunciation { get; set; }
        public string AppearsIn { get; set; }
        public string CurrentStatus { get; set; }
        public string BasedInLocation { get; set; }
        public string Deadlines { get; set; }
        public string BestContactTime { get; set; }   
        public string BusyTimes { get; set; }
        public string AuthorOf { get; set; }
        public bool? GiftsAccepted { get; set; }
        public string CoffeeOrder { get; set; }
        public string PitchingDos { get; set; }
        public string PitchingDonts { get; set; }

        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public string State { get; set; }
        public string Country { get; set; }

        public string DeliveryMethod { get; set; }

        public string Notes { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        
        public bool? CurrentlyOutOfOffice { get; set; }
        public DateTime? OutOfOfficeReturnDate { get; set; }
        public DateTime? OutOfOfficeStartDate { get; set; }

        public List<KeyValuePair<int, string>> Roles { get; set; }
        public List<MediaEmployeeSubjectView> Subjects { get; set; }
        public List<MediaEmployeeListView> BelongsToLists { get; set; }
        public List<MediaEmployeeDocumentView> Documents { get; set; }
        public MediaEmployeeOutletView AtOutlet { get; set; }
    }

    public class MediaEmployeeSubjectView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsPrimaryContact { get; set; }
    }

    public class MediaEmployeeListView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int UserId { get; set; }
        public bool IsPrivate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool ListStatus { get; set; }
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public byte GroupStatus { get; set; }
        public byte? GroupType { get; set; }
    }

    public class MediaEmployeeOutletView
    {
        public string Name { get; set; }
        public int OutletRecordType { get; set; }
        public string OutletId { get; set; }
        public string ContactId { get; set; }
        public bool IsGeneric { get; set; }

        public string LogoFileName { get; set; }
    }

    public class MediaEmployeeDocumentView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int UserId { get; set; }
        public bool? Status { get; set; }
        public bool IsPrivate { get; set; }
    }
}
