﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MediaOmaContact
    {
        public int ContactId { get; set; }

        public int? OmaOutletId { get; set; }

        public string MediaOutletId { get; set; }

        public decimal? PrnOutletId { get; set; }

        public string Prefix { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string JobTitle { get; set; }

        public byte RowStatus { get; set; } // Status

        public string EmailAddress { get; set; }

        public string PhoneNumber { get; set; }

        public string OwnedBy { get; set; }
    }
}
