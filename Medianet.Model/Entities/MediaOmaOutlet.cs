﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MediaOmaOutlet
    {
        public int OutletId { get; set; }

        public string CompanyName { get; set; }

        public byte? Status { get; set; }

        public string OwnedBy { get; set; }

        public int ProductTypeId { get; set; }

        public ProductType ProductType { get; set; }
    }
}
