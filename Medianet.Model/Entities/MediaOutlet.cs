﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class MediaOutlet
    {
        public string Id { get; set; }
        public string Name { get; set; }                        // CompanyName
        public bool HasParent { get; set; }
        public string ParentId { get; set; }
        public string OwnershipCompany { get; set; }            // Ownership
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public int CityId { get; set; }
        public int CountryId { get; set; }
        public int ContinentId { get; set; }
        public string PostalAddressLine1 { get; set; }
        public string PostalAddressLine2 { get; set; }
        public int? PostalCityId { get; set; }
        public int? PostalCountryId { get; set; }
        public int? PostalContinentId { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string EmailAddress { get; set; }
        public string WebSite { get; set; }
        public string Facebook { get; set; }
        public string LinkedIn { get; set; }
        public string Twitter { get; set; }
        public string Instagram { get; set; }
        public string YouTube { get; set; }
        public string Snapchat { get; set; }
        public string BlogURL { get; set; }
        public int? OutletTypeId { get; set; }
        public int? ProductTypeId { get; set; }
        public string PreferredDeliveryMethod { get; set; }
        public decimal? Circulation { get; set; }
        public decimal? Audience { get; set; }
        public string CallLetters { get; set; }
        public string YearEstablished { get; set; }
        public string StationFrequency { get; set; }
        public string CopyPrice { get; set; }
        public int? FrequencyId { get; set; }
        public int? NewsFocusId { get; set; }
        public string TrackingReference { get; set; }
        public bool SendToOMA { get; set; }
        public bool? HasReceivedLegals { get; set; }             // LegalsSent
        public string P16 { get; set; }
        public string M16 { get; set; }
        public string F16 { get; set; }
        public string GBs { get; set; }
        public string ABs { get; set; }
        public string InternalNotes { get; set; }
        public string Bio { get; set; }

        // De-normalised data used by the Contacts website.
        public string SubjectNames { get; set; }
        //public string RoleNames { get; set; }

        public string NamePronunciation { get; set; }
        public bool? SubscriptionOnlyPublication { get; set; }
        public string AlsoKnownAs { get; set; }
        public string Deadlines { get; set; }
        public int? Readership { get; set; }
        public string BroadcastTime { get; set; }
        public string AlternativeEmailAddress { get; set; }
        public string Mobile { get; set; }
        public string AdditionalPhoneNumber { get; set; }
        public string AdditionalWebsite { get; set; }
        public string Skype { get; set; }
        public string OfficeHours { get; set; }
        public string PublishedOn { get; set; }
        public int? MediaInfluencerScore { get; set; }
        public string LogoFileName { get; set; }
        public string PressReleaseInterests { get; set; }
        public string RegionsCovered { get; set; }
        public bool? IsGeneric { get; set; }
        public bool? IsDirty { get; set; }

        public DateTime CreatedDate { get; set; }               // Added
        //public int CreatedByUserId { get; set; }                // CreatedUserId
        public DateTime LastModifiedDate { get; set; }          // Updated
        public int? LastModifiedByUserId { get; set; }          // LastModifiedBy
        public int? LockedByUserId { get; set; }                // LockedBy
        public DateTime? DeletedDate { get; set; }              // Deleted
        public string RowStatus { get; set; }                   // Status

        public virtual User User { get; set; }
        public virtual MediaOutletProfile Profile { get; set; }
        [ForeignKey("CityId")]
        public virtual AAPCity AAPCity { get; set; }
        [ForeignKey("PostalCityId")]
        public virtual AAPCity AAPPostalCity { get; set; }

        public virtual ICollection<MediaOutletStationFormat> StationFormats { get; set; }
        public virtual ICollection<MediaRelatedOutlet> RelatedOutlets { get; set; }
        public virtual ICollection<MediaSyndicatedOutlet> SyndicatedOutlets { get; set; }
        public virtual ICollection<MediaOutletSubject> Subjects { get; set; }
        public virtual ICollection<MediaOutletWorkingLanguage> WorkingLanguages { get; set; }
    }
}
