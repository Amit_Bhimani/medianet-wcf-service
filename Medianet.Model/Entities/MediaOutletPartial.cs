﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MediaOutletPartial
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public int? ProductTypeId { get; set; }

        public string ProductTypeName { get; set; }

        public string ProductTypeCategory { get; set; }
    }
}
