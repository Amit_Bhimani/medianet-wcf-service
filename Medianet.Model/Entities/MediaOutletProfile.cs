﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MediaOutletProfile
    {
        public string OutletId { get; set; }
        public string Content { get; set; }
        public virtual MediaOutlet MediaOutlet { get; set; }
    }
}
