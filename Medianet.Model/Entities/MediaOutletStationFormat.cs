﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MediaOutletStationFormat
    {
        public string OutletId { get; set; }
        public int StationFormatId { get; set; }
        public virtual MediaOutlet MediaOutlet { get; set; }
    }
}
