﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MediaOutletSubject
    {
        public string OutletId { get; set; }
        public int SubjectId { get; set; }                      // SubjectCodeId
        public virtual MediaOutlet MediaOutlet { get; set; }
    }
}
