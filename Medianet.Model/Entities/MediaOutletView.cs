﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MediaOutletView
    {
        public long Id { get; set; }
        public string OutletId { get; set; }
        public int UserId { get; set; }
        public string IpAddress { get; set; }
        public DateTime DateViewed { get; set; }      
    }
}
