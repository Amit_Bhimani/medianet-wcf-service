﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MediaOutletWorkingLanguage
    {
        public string OutletId { get; set; }
        public int WorkingLanguageId { get; set; }
        public virtual MediaOutlet MediaOutlet { get; set; }
    }
}
