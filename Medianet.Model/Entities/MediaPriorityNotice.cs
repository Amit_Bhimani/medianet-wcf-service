﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MediaPriorityNotice
    {
        public int Id { get; set; }
        public string ContactId { get; set; }
        public string OutletId { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public int? CreatedByUserId { get; set; }
        public int? LastModifiedByUserId { get; set; }

        public virtual User User { get; set; }
        public virtual MediaContact MediaContact { get; set; }
        public virtual MediaOutlet MediaOutlet { get; set; }
    }
}
