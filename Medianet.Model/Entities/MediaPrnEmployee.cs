﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MediaPrnEmployee
    {
        public decimal EmpId { get; set; }
        public decimal? OutletId { get; set; }
        public decimal? ContactId { get; set; }
        public string JobTitle { get; set; }
        public bool? IsPrimaryNewsContact { get; set; }
        public string CreatedDate { get; set; }               // Added
        public string LastModifiedDate { get; set; }          // Updated
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string EmailAddress { get; set; }
        public decimal? AddressId { get; set; }
        public string PreferredDeliveryMethod { get; set; }
        public string Profile { get; set; }
        public string SubjectNames { get; set; }
        public string RoleNames { get; set; }


        //public virtual WorkingLanguage WorkingLanguage { get; set; }
        //public virtual MediaPrnContact MediaContact { get; set; }
        //public virtual MediaOutlet MediaOutlet { get; set; }

        //public virtual ICollection<MediaEmployeeRole> Roles { get; set; }
        //public virtual ICollection<MediaEmployeeSubject> Subjects { get; set; }
    }
}
