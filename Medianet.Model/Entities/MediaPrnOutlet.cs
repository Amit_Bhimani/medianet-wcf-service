﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MediaPrnOutlet
    {
        public decimal OutletId { get; set; }
        public string Name { get; set; }
        public decimal StatusType { get; set; }       
    }
}
