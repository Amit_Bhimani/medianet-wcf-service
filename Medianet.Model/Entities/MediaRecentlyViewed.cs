﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medianet.Model.Entities
{
    public class MediaRecentlyViewed
    {
        public string OutletId { get; set; }
        public string OutletName { get; set; }
        public string ContactId { get; set; }
        public string FirstName { get; set; }
        public string  LastName { get; set; }
    }
}
