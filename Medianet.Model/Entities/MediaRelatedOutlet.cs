﻿// -----------------------------------------------------------------------
// <copyright file="MediaRelatedOutlet.cs" company="Australian Associated Press">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Medianet.Model.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class MediaRelatedOutlet
    {
        [Key] 
        public string OutletId { get; set; }
        [Key] 
        public string RelatedOutletId { get; set; }
        public virtual MediaOutlet MediaOutlet { get; set; }
    }
}
