﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MediaStatistics
    {
        public int Id { get; set; }
        public int AustralasiaToday { get; set; }           // AusToday
        public int AustralasiaLastWeek { get; set; }        // AusLastWeek
        public int AustralasiaLastMonth { get; set; }        // AusLastMonth
        public int AustralasiaYearToDate { get; set; }      // AusYearToDate
        public int InternationalToday { get; set; }         // IntToday
        public int InternationalLastWeek { get; set; }      // IntLastWeek
        public int InternationalYearToDate { get; set; }    // IntYearToDate
    }
}
