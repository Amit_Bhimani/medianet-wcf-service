﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MediaSyndicatedOutlet
    {

        [Key]
        public string OutletId { get; set; }
        [Key]
        public string SyndicatedOutletId { get; set; }
        public virtual MediaOutlet MediaOutlet { get; set; }
    }
}
