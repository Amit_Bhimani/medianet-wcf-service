﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MediumType
    {
        public int OutletTypeId { get; set; }
        public int ProductTypeId { get; set; }
        public string Name { get; set; }        // MediumTypeName
    }
}
