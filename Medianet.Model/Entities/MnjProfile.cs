﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MnjProfile
    {
        public int ProfileID { get; set; }

        public string ProfileName { get; set; }

        public int UserID { get; set; }

        public bool IsDefault { get; set; }

        public bool EmailAlert { get; set; }

        public string EmailAddress { get; set; }
        
        public virtual List<MnjProfileCategory> Categories { get; set; }
        public virtual List<MnjProfileKeyword> Keywords { get; set; }
    }
}
