﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MnjProfileCategory
    {
        public int ProfileID { get; set; }

        public int WebCategoryID { get; set; }

        public virtual MnjProfile MnjProfile { get; set; }
    }
}
