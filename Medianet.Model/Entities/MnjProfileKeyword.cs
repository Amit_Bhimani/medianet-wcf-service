﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MnjProfileKeyword
    {
        public int ProfileID { get; set; }

        public string Keyword { get; set; }

        public virtual MnjProfile MnjProfile { get; set; }
    }
}
