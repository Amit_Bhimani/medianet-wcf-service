﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MnjProfileReleases
    {
        public int TotalCount { get; set; }
        public int TotalPages { get; set; }
        public DateTime WhenDistributed { get; set; }
        public string Date { get; set; } //JournalistsDistributedDate
        public int JobId { get; set; }
        public string Organisation { get; set; }
        public string Headline { get; set; }

        public int PhotoDocId { get; set; }

        public int PhotoSeqNo { get; set; }

        public string PhotoMediaReferenceId { get; set; }

        public int AudioDocId { get; set; }
        public int AudioSeqNo { get; set; }

        public string AudioMediaReferenceId { get; set; }

        public int VideoDocId { get; set; }

        public int VideoSeqNo { get; set; }

        public string VideoMediaReferenceId { get; set; }

        public int OtherDocId { get; set; }

        public int OtherSeqNo { get; set; }

        public string OtherMediaReferenceId { get; set; }

        public int SecurityKey { get; set; }

        public string MultimediaType { get; set; }
    }
}
