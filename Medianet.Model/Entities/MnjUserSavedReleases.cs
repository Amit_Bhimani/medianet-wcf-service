﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medianet.Model.Entities
{
    public class MnjUserSavedReleases
    {
        public int ReleaseId { get; set; }// JobID

        public int UserId { get; set; }
    }
}
