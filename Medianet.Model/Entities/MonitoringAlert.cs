﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MonitoringAlert
    {
        public int Id { get; set; }
        public int CampaignId { get; set; }
        public int FrequencyId { get; set; }
        public string Subject { get; set; }
        public string Blurb { get; set; }
        public string RecipientUsers { get; set; }
        public string CustomRecipients { get; set; }
                
        public virtual MonitoringCampaign Campaign { get; set; }
        public virtual MonitoringFrequency Frequency { get; set; }
    }
}


