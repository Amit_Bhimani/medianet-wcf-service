﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MonitoringCampaign
    {
        public int Id { get; set; }
        public string ProjectName { get; set; }
        public string CampaignName { get; set; }
        public int MonitoringCampaignId {get; set;}
        public int SearchId { get; set; }
        public bool IsActive { get; set; }
        public bool IsPrivate { get; set; }
        public string DebtorNumber { get; set; }
        public int UserId { get; set; }

        public virtual MonitoringSearch SearchCriteria { get; set; }
        public virtual List<MonitoringAlert> Alerts { get; set; }
    }
}
