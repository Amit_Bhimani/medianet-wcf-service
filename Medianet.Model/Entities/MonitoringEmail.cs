﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MonitoringEmail
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public System.DateTime EmailDate { get; set; }
        public int SearchId { get; set; }
        public string Articles { get; set; }
        public string RecipientUsers { get; set; }
        public string CustomRecipients { get; set; }
        public int AlertId { get; set; }
        
        public virtual User User { get; set; }
        public virtual MonitoringSearch SearchCriteria { get; set; }
    }
}
