﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MonitoringFrequency
    {
        public int Id { get; set; }
        public string Frequency { get; set; }

        public virtual List<MonitoringAlert> Alerts { get; set; }
    }
}
