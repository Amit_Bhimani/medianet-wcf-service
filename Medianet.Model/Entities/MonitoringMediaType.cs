﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class MonitoringMediaType
    {
        public const int PRINT = 1;
        public const int BROADCAST = 2;
        public const int INTERNET = 3;
        public const int TEXT = 4;
        public const int BREAKINGNEWS = 5;

        public const string PRINT_PRODUCT = "PRINT";
        public const string BROADCAST_PRODUCT = "BROADCAST";
        public const string INTERNET_PRODUCT = "INTERNET";
        public const string TEXT_PRODUCT = "TEXT";
        public const string BREAKINGNEWS_PRODUCT = "BREAKINGNEWS";

        public int Id { get; set; }
        public string MediaTypeName { get; set; }
        public virtual List<MonitoringSearch> Searches { get; set; }

    }
}
