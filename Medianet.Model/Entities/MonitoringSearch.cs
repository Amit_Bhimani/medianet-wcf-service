﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public partial class MonitoringSearch
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public bool IsFullText { get; set; }
        public string Keywords { get; set; }
        public string Sources { get; set; }
        public string Owners { get; set; }
        public bool IsMostRecent { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreationDate { get; set; }
        
        public virtual User user { get; set; }
        public virtual List<MonitoringMediaType> MediaTypes { get; set; }
        public virtual List<MonitoringCampaign> Campaigns { get; set; }
        public virtual List<MonitoringEmail> Emails { get; set; }
    }
}
