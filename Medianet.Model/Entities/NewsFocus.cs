﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class NewsFocus
    {
        public int Id { get; set; }         // NewsFocusId
        public string Name { get; set; }    // NewsFocusName
    }
}
