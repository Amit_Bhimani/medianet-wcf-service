﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class Note
    {
        public int Id { get; set; }
        public string Notes { get; set; }
        public DateTime CreatedDate { get; set; }
        public byte Status { get; set; }
        public Boolean IsPrivate { get; set; }
        public int UserId { get; set; }
        public string MediaOutletId { get; set; }
        public string MediaContactId { get; set; }
        public int? OmaOutletId { get; set; }
        public int? OmaContactId { get; set; }
        public decimal? PrnOutletId { get; set; }
        public decimal? PrnContactId { get; set; }
        public Boolean Pinned { get; set; }
        public Boolean AAPNote { get; set; }
        public string DebtorNumber { get; set; }
        public virtual User User { get; set; }
    }
}
