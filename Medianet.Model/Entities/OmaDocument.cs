﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class OmaDocument
    {
        public int Id { get; set; }

        public int? ListId { get; set; }

        public string Name { get; set; }

        public DateTime? CreatedDate { get; set; }

        public int UploadedByUserId { get; set; }

        public bool? Status { get; set; }

        public bool IsPrivate { get; set; }
        
        public byte[] DocumentContent { get; set; }

        public string DocumentType { get; set; }
    }
}
