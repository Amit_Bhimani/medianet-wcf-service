﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class OmaDocumentBelongsTo
    {
        public int Id { get; set; }       
                       
        //public string ContactId { get; set; }

        //public string OutletId { get; set; }

        public DateTime? CreatedDate { get; set; }

        public bool? Status { get; set; }

        public string MediaOutletId { get; set; }

        public string MediaContactId { get; set; }

        public int? OmaOutletId { get; set; }

        public int? OmaContactId { get; set; }

        public decimal? PrnOutletId { get; set; }

        public decimal? PrnContactId { get; set; }
        
        public virtual OmaDocument OmaDocument { get; set; }
    }
}
