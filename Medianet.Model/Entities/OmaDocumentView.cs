﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class OmaDocumentView
    {
        public int DocumentId { get; set; }

        public string Name { get; set; }

        public DateTime? CreatedDate { get; set; }

        public int UploadedByUserId { get; set; }

        public bool? Status { get; set; }

        public bool IsPrivate { get; set; }
        
        public byte[] DocumentContent { get; set; }

        public string DocumentType { get; set; }

        public string MediaOutletId { get; set; }

        public string MediaContactId { get; set; }

        public int? OmaOutletId { get; set; }

        public int? OmaContactId { get; set; }

        public decimal? PrnOutletId { get; set; }

        public decimal? PrnContactId { get; set; }
    }
}
