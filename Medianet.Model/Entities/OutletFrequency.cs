﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class OutletFrequency
    {
        public int Id { get; set; }         // FrequencyId
        public string Name { get; set; }    // FrequencyName
    }
}
