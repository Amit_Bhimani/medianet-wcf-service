﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class OutletType
    {
        public int Id { get; set; }         // OutletTypeId
        public string Name { get; set; }    // OutletTypeName
    }
}
