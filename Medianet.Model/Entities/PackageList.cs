﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class PackageList
    {
        public int Id { get; set; }
        [Column(TypeName = "varchar")] [StringLength(12)]
        public string DebtorNumber { get; set; }
        [Column(TypeName = "char")]
        public string System { get; set; }                      // SystemType
        public Int16 SequenceNumber { get; set; }
        public string TransactionType { get; set; }
        public string AccountCode { get; set; }
        public string SelectionDescription { get; set; }
        public int? ListNumber { get; set; }
        [Column(TypeName = "char")]
        public string RowStatus { get; set; }                   // Status
        public bool ShouldCharge { get; set; }                  // Charge
        public bool ShouldDistribute { get; set; }              // Distribute
        public bool ShouldSendResultsToOwner { get; set; }      // SendResultsToOwner
        public bool ShouldUseSpecialAddressText { get; set; }   // UseSpecialAddressText
        public string Introduction { get; set; }
        public int? Category1Id { get; set; }                   // Category1
        public int? Category2Id { get; set; }                   // Category2
        [Column(TypeName = "char")]
        public string IsHidden { get; set; }                    // Hidden
        public decimal Cost { get; set; }
        public int? CoverPageId { get; set; }
        public bool? IsTopValue { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedByUserId { get; set; }                // CreatedUserId
        public DateTime LastModifiedDate { get; set; }          // ModifiedDate
        public int LastModifiedByUserId { get; set; }           // ModifiedUserId

        public bool? EmailAlert { get; set; }

        //// Generated columns.
        //public int RecipientFaxCount { get; set; }
        //public int RecipientEmailCount { get; set; }
        //public int RecipientSMSCount { get; set; }
        //public int RecipientVoiceCount { get; set; }
        //public int RecipientWireCount { get; set; }
    }
}
