﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class PackageServiceXref
    {
        public int PackageId { get; set; }
        public int ServiceId { get; set; }
    }
}
