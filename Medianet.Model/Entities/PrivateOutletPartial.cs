﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class PrivateOutletPartial
    {
        public string Id { get; set; }

        public string Name { get; set; }
        
        public int RecordType { get; set; }
    }
}
