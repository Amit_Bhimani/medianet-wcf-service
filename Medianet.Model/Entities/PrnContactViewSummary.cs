﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class PrnContactViewSummary
    {
        public long Id { get; set; }
        public decimal ContactId { get; set; }
        public decimal OutletId { get; set; }
        public int SubjectCodeId { get; set; }
        public int ViewCount { get; set; }
        public int ContinentId { get; set; }
    }
}
