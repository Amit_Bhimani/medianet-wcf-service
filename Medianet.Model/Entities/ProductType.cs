﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class ProductType
    {
        public int Id { get; set; }             // ProductTypeId
        public string Name { get; set; }        // ProductTypeName
        public string ProductTypeCategory { get; set; }        // ProductTypeCategory
    }
}
