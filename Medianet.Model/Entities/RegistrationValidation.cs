﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class RegistrationValidation
    {
        public Guid Token { get; set; }
        [Column(TypeName = "char")]
        public string System { get; set; }                  // SystemType
        public string EmailAddress { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        public string TelephoneNumber { get; set; }
        public string Password { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Industry { get; set; }

        public virtual ReleaseUnverifiedPreview ReleaseUnverified { get; set; }
    }
}
