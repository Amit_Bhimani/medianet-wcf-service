﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
//using System.Data.Entity.ModelConfiguration.Conventions;

namespace Medianet.Model.Entities
{
    public class Release
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public int? ChildId { get; set; }
        [Column(TypeName = "varchar")] [StringLength(12)]
        public string DebtorNumber { get; set; }
        [Column(TypeName = "char")]
        public string System { get; set; }                      // SystemType
        [Column(TypeName = "char")]
        public string Status { get; set; }
        public string ReleaseDescription { get; set; }
        public string WireDescription { get; set; }
        public string CustomerReference { get; set; }
        public string OMAReference { get; set; }                // OMAReferenceNum
        public string BillingCode { get; set; }
        public string WieckReference { get; set; }
        public int? WieckThumbnailWidth { get; set; }
        public int? WieckThumbnailHeight { get; set; }

        public Int16 IsOpen { get; set; }                        // Opened
        public DateTime? OpenedDate { get; set; }               // OpenTimestamp
        public bool IsFrozen { get; set; }                      // Frozen
        public Int16 IsInQueue { get; set; }                      // InQueue
        public bool HasHoldDate { get; set; }                   // Hold
        public DateTime? HoldUntilDate { get; set; }            // HoldUntil
        public bool HasEmbargoDate { get; set; }                // Embargo
        public DateTime? EmbargoUntilDate { get; set; }         // EmbargoUntil
        public string Priority { get; set; }

        // Fax text position information.
        public Int16 FaxTextOffsetX { get; set; }                 // DocTextX
        public Int16 FaxTextOffsetY { get; set; }                 // DocTextY
        public Int16 FaxTextPage { get; set; }                    // DocTextPage

        public Int16 WireWords { get; set; }                      // Words
        public string SpecialAddressText { get; set; }
        public string Comment { get; set; }
        public string SMSText { get; set; }

        public int? FinanceFileId { get; set; }                 // JobFileId

        public string BadTifPages { get; set; }                 // BadPages

        public string OCRStatus { get; set; }
        public string ReceptionMethod { get; set; }             // ReceptionType

        // Email from details.
        public bool UseCustomerEmailAddress { get; set; }
        public string EmailFromName { get; set; }               // OriginatorName
        public string EmailFromAddress { get; set; }            // OriginatorEmail

        // Report details.
        public string ReportFaxAddress { get; set; }
        public string ReportEmailAddress { get; set; }
        public string ReportResultsToSend { get; set; }         // ReportResultsType

        public int SecurityKey { get; set; }
        public bool ShouldDeduplicateJobs { get; set; }         // Dedup
        public bool IsOnHoldIndefinitely { get; set; }          // HoldIndefinite
        public bool? IsResend { get; set; }                      // Resend
        [Column(TypeName = "char")]
        public string MultimediaType { get; set; }
        public string NextStatus { get; set; }
        public int? QuoteReferenceId { get; set; }               // QuoteReferenceId

        public DateTime CreatedDate { get; set; }               // WhenReceived
        public int? CreatedByUserId { get; set; }               // SubmittedUserId
        public DateTime? DistributedDate { get; set; }          // WhenDistributed

        public virtual WebRelease WebDetails { get; set; }
        public List<Document> Documents { get; set; }           // Dont use virtual because this forces lazy loading when calling automapper.
        public virtual List<Transaction> Transactions { get; set; }
        public virtual User CreatedByUser { get; set; }
        public string TransactionId { get; set; } //chargeBeeTransactionId
        public string PaymentMethod { get; set; }
    }
}
