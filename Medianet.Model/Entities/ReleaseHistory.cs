﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class ReleaseHistory
    {
        public int ReleaseId { get; set; }                       // JobId
        public string Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedByUserId { get; set; }                // UserId
    }
}
