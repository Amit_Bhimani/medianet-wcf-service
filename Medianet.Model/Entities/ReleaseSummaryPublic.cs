﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class ReleaseSummaryPublic
    {
        public int TotalCount { get; set; }
        public int TotalPages { get; set; }
        public DateTime WhenDistributed { get; set; }
        public int JobId { get; set; }
        public string Organisation { get; set; }
        public string Headline { get; set; }
        public string MultimediaType { get; set; }
        public int? PrimaryWebCategoryId { get; set; }
        public int? SecondaryWebCategoryId { get; set; }
        public string Summary { get; set; }
        public bool HasVideo { get; set; }
        public bool HasAudio { get; set; }
        public bool HasPhoto { get; set; }
        public bool HasOther { get; set; }
        public string WieckReferenceId { get; set; }
        public int? WieckThumbnailWidth { get; set; }
        public int? WieckThumbnailHeight { get; set; }
        public int SecurityKey { get; set; }
        public int? PhotoDocumentId { get; set; }
        public int? PhotoSequenceNumber { get; set; }
        public string PhotoMediaReferenceId { get; set; }
        public int? PhotoThumbnailId { get; set; }
        public int? PhotoThumbnailWidth { get; set; }
        public int? PhotoThumbnailHeight { get; set; }
        public bool? IsVerified { get; set; }
    }
}
