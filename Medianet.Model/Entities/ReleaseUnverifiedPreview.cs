﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class ReleaseUnverifiedPreview
    {
        public Guid Token { get; set; }
        public string Headline { get; set; }
        public string Organisation { get; set; }
        public int? ReleaseTempFileId { get; set; }
        public int? WhitePaperTempFileId { get; set; }
        public int? WebCategoryId { get; set; }

        public virtual RegistrationValidation Registration { get; set; }
    }
}
