﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class ReleaseWireCategory
    {
        public int Id { get; set; }                         // JobWireId
        public int ReleaseId { get; set; }                  // JobID
        public int CategoryId { get; set; }
    }
}
