﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class Result
    {
        public int ReleaseId { get; set; }              // JobId
        public Int16 GroupNumber { get; set; }
        public int TransactionId { get; set; }
        public int? DistributionId { get; set; }
        public string Recipient { get; set; }
        public string Address { get; set; }
        public string Status { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public DateTime? TransmittedTime { get; set; }  // TransmitTime
        public DateTime CreatedDate { get; set; }       // CreatedTime
        public int? SendDuration { get; set; }
        public int OpenCount { get; set; }
    }
}
