﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class RssLog
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? WebCategoryId { get; set; }
        public string UserAgent { get; set; }
        public string UrlParams { get; set; }
        public string IPAddress { get; set; }   
    }
}
