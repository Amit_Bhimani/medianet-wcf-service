﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class SalesRegion
    {
        public int Id { get; set; }                             // RegionId
        public string Name { get; set; }                        // RegionName
        public string SalesEmail { get; set; }
        public string AccountManagerName { get; set; }
        public string AccountManagerImageUrl { get; set; }

        public DateTime CreatedDate { get; set; }               // CreatedTime
        public int CreatedByUserId { get; set; }                // CreatedUserId
        public DateTime LastModifiedDate { get; set; }          // ModifiedTime
        public int LastModifiedByUserId { get; set; }           // ModifiedUserId
    }
}
