﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class ServiceDeletedUserLog
    {
        public int Id { get; set; }

        public string OutletId { get; set; }

        public string ContactId { get; set; }

        public int ServiceId { get; set; }

        public int DeletedByUserId { get; set; }

        public DateTime DeletedTime { get; set; }
    }
}
