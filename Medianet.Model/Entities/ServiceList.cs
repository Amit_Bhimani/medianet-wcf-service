﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class ServiceList
    {
        public int Id { get; set; }
        [Column(TypeName = "varchar")] [StringLength(12)]
        public string DebtorNumber { get; set; }                // Owner
        [Column(TypeName = "char")]
        public string System { get; set; }                      // SystemType
        [Column(TypeName = "char")]
        public string DistributionType { get; set; }
        public Int16 SequenceNumber { get; set; }
        public string TransactionType { get; set; }
        public string AccountCode { get; set; }
        public string SelectionDescription { get; set; }
        public int? ListNumber { get; set; }
        public Int16 AddressCount { get; set; }                 // Addresses
        public bool ShouldUpdateAddresses { get; set; }         // UpdateAddresses
        public bool ShouldCharge { get; set; }                  // Charge
        public bool ShouldDistribute { get; set; }              // Distribute
        public bool ShouldSendResultsToOwner { get; set; }      // SendResultsToOwner
        public bool ShouldUseSpecialAddressText { get; set; }   // UseSpecialAddressText
        public Int16 PageAdjustment { get; set; }
        public string Introduction { get; set; }
        public string Destinations { get; set; }
        public int? CategoryId { get; set; }                    // Category
        public string IsHidden { get; set; }                    // Hidden
        public string EditorialCategory { get; set; }
        public string IsOnlineDB { get; set; }                  // OnlineDB
        public int? CoverPageId { get; set; }
        public bool? IsMediaList { get; set; }

        public DateTime CreatedDate { get; set; }               // CreatedTime
        public int CreatedByUserId { get; set; }                // CreatedUserId
        public DateTime LastModifiedDate { get; set; }          // ModifiedTime
        public int LastModifiedByUserId { get; set; }           // ModifiedUserId

        public bool? EmailAlert { get; set; }
    }
}
