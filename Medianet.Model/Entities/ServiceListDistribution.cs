﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class ServiceListDistribution
    {
        public int Id { get; set; }                 // DistributionId
        public int? ServiceId { get; set; }
        public int SequenceNumber { get; set; }     // SequenceNo
        public int? ChildServiceId { get; set; }
        public string OutletId { get; set; }
        public string ContactName { get; set; }
        public string ContactId { get; set; }
        public int? RecipientId { get; set; }

        //public virtual MediaOutlet MediaOutlet { get; set; } 

    }
}
