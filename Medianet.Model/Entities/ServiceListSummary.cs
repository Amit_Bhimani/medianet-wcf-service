﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class ServiceListSummary
    {
        public int Id { get; set; }
        public string DistributionType { get; set; }
        public Int16 SequenceNumber { get; set; }
        public string SelectionDescription { get; set; }
        public int? ListNumber { get; set; }
    }
}
