﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class State
    {
        public int StateId { get; set; }
        public string StateName { get; set; }
        public decimal MediaAtlasCountryId { get; set; }
        public string CountryName { get; set; }
    }
}
