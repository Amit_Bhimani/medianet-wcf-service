﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class StationFormat
    {
        public int Id { get; set; }         // StationFormatId
        public string Name { get; set; }    // StationFormatName
    }
}
