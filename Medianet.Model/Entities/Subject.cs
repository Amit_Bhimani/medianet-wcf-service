﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class Subject
    {
        public int Id { get; set; }         // SubjectCodeId
        public string Name { get; set; }    // SubjectName
        public bool Hidden { get; set; }

        public virtual List<SubjectGroup> SubjectGroups { get; set; }
    }
}
