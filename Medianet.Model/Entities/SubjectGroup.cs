﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class SubjectGroup
    {
        public int Id { get; set; }         // SubjectGroupId
        public string Name { get; set; }    // SubjectGroupName

        public virtual List<Subject> Subjects { get; set; }
    }
}
