﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class SubjectGroupXref
    {
        public int GroupId { get; set; }    //SubjectGroupId
        public int SubjectId { get; set; }  //SubjectCodeId
    }
}
