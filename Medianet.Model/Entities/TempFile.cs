﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class TempFile
    {
        public int Id { get; set; }                         // TempFileID
        public int SessionId { get; set; }                  // SessionID
        public int? ParentId { get; set; }
        public string FileExtension { get; set; }           // FileExt
        public string OriginalFilename { get; set; }        // OriginalFileName
        public string SavedFile { get; set; }
        public bool IsFaxConversion { get; set; }
        public bool IsWireConversion { get; set; }
        public bool IsWebConversion { get; set; }
        public bool IsMailMergeDataSource { get; set; }
        public int FileSize { get; set; }
        public int ItemCount { get; set; }
        public string ContentType { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
