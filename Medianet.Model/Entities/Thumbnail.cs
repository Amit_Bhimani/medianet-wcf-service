﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class Thumbnail
    {
        public int Id { get; set; }                                     // ThumbnailId
        public int DocumentId { get; set; }
        public string FileExtension { get; set; }                       // FileExt
        public int Width { get; set; }                                  // ImageWidth
        public int Height { get; set; }                                 // ImageHeight
        public DateTime CreatedDate { get; set; }                       // CreatedTime

        public virtual Document Document { get; set; }
    }
}
