﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class Timezone
    {
        public string Code { get; set; }            // TIMEZONE_CD
        public string Name { get; set; }            // NAME_TX
        public string Description { get; set; }     // DESCRIPTION_TX
    }
}
