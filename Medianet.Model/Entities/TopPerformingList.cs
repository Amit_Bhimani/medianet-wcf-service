﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class TopPerformingList
    {
        [Column(TypeName = "char")] [StringLength(1)]
        public string Period { get; set; }
        public int ServiceId { get; set; }
        public string SelectionDescription { get; set; }
        public int LastPeriodTotal { get; set; }
        public int PreviousPeriodTotal { get; set; }
        public decimal PercentageImprovement { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
