﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class TrainingCourse
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Description { get; set; }
        public string Sector { get; set; }
        public decimal Price { get; set; }
        public bool IsFeatured { get; set; }
        public int? PackageId { get; set; }
        public string EventType { get; set; }

        public virtual ICollection<TrainingCourseSchedule> Schedules { get; set; }
        public virtual ICollection<TrainingCourseTopic> Topics { get; set; }
        public virtual ICollection<TrainingCoursePromoCode> PromoCodes { get; set; }
        public virtual ICollection<TrainingCourseContact> Contact { get; set; }
    }
}