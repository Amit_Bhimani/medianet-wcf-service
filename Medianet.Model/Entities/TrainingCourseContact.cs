﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class TrainingCourseContact
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public string Email { get; set; }
        public string TrainingCourseId { get; set; }

        public TrainingCourse Course { get; set; }
    }
}
