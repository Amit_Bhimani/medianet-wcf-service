﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class TrainingCourseEnrolment
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string ABN { get; set; }
        public string ReceiptId { get; set; }
        public int ScheduleId { get; set; }
        public int NumberOfAttendees { get; set; }
        public int Status { get; set; }
        public double TotalCost { get; set; }
    }
}
