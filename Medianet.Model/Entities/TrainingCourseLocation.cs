﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Medianet.Model.Entities
{
    public class TrainingCourseLocation
    {
        public string Id { get; set; }
        public string Description { get; set; }
    }
}
