﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Medianet.Model.Entities
{
    public class TrainingCoursePromoCode
    {
        public int Id { get; set; }
        public string PromoCode { get; set; }
        public decimal Price { get; set; }
        public int PackageId { get; set; }
        public string TrainingCourseId { get; set; }
    }
}