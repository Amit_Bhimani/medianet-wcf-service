﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class TrainingCourseSchedule
    {
        public int Id { get; set; }
        public DateTime CourseDate { get; set; }
        public string CourseTime { get; set; }
        public string LocationId { get; set; }
        public int Seats { get; set; }
        public string Agenda { get; set; }
        public string TrainingCourseId { get; set; }
        public string FormHandler { get; set; }

        public virtual ICollection<TrainingCourseEnrolment> Enrolments { get; set; }
        public virtual TrainingCourseLocation Location { get; set; }
    }
}