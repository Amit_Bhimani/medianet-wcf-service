﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Medianet.Model.Entities
{
    public class TrainingCourseTopic
    {
        public int Id { get; set; }
        public string Heading { get; set; }
        public string Description { get; set; }
        public string TrainingCourseId { get; set; }
    }
}