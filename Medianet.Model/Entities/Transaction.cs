﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class Transaction
    {
        public int Id { get; set; }
        public int ReleaseId { get; set; }                          // JobId
        public int? ParentTransactionId { get; set; }
        public Int16 SequenceNumber { get; set; }
        public Int16 GroupNumber { get; set; }
        public int? PackageId { get; set; }
        public int? ServiceId { get; set; }
        //public int CategoryId { get; set; }
        [Column(TypeName = "varchar")] [StringLength(12)]
        public string DebtorNumber { get; set; }                    // Owner
        public string System { get; set; }                          // SystemType
        public string DistributionType { get; set; }
        public bool IsSingleRecipient { get; set; }
        public bool IsMailMerge { get; set; }
        public string Status { get; set; }
        public string TransactionType { get; set; }
        public string AccountCode { get; set; }
        public string SelectionDescription { get; set; }
        public string PackageDescription { get; set; }
        public int? ListNumber { get; set; }
        public Int16 AddressCount { get; set; }                     // Addresses
        public bool ShouldCharge { get; set; }                      // Charge
        public bool ShouldDistribute { get; set; }                  // Distribute
        public string DistributionSystem { get; set; }
        public bool ShouldSendResultsToOwner { get; set; }          // SendResultsToOwner
        public bool ShouldUseSpecialAddressText { get; set; }       // UseSpecialAddressText
        public Int16 PageAdjustment { get; set; }
        public string Introduction { get; set; }
        public string SingleRecipientAddress { get; set; }
        public string Destinations { get; set; }
        public string EditorialCategory { get; set; }
        public int? CoverPageId { get; set; }
        public string TweetText { get; set; }
        public int? TwitterCategoryId { get; set; }
        public string TwitterId { get; set; }
        public bool? IsMediaList { get; set; }

        public DateTime CreatedDate { get; set; }                   // CreatedTime
        public int CreatedByUserId { get; set; }                    // CreatedUserId
        public DateTime LastModifiedDate { get; set; }              // ModifiedTime
        public int LastModifiedByUserId { get; set; }               // ModifiedUserId
    }
}
