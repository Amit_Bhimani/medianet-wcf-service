﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class TransactionStatistics
    {
        public int Id { get; set; }
        public Int16 SequenceNumber { get; set; }
        public string DebtorNumber { get; set; }
        public string DistributionType { get; set; }
        public string Status { get; set; }
        public bool IsSingleRecipient { get; set; }
        public string SelectionDescription { get; set; }
        public int? ListNumber { get; set; }
        public bool ShouldSendResultsToOwner { get; set; }
        public int TotalInProgress { get; set; }
        public int TotalSentOk { get; set; }
        public int TotalFailed { get; set; }
        public int TotalOpened { get; set; }
        public int TotalUnopened { get; set; }
    }
}
