﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class TwitterCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Handle { get; set; }
        public int SequenceNumber { get; set; }
        public bool IsVisible { get; set; }
        public bool IsDeleted { get; set; }
    }
}
