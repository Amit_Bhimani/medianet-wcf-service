﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Model.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string LogonName { get; set; }
        public string Password { get; set; }
        public string HashedPassword { get; set; }
        public string Salt { get; set; }
        [Column(TypeName = "varchar")] [StringLength(12)]
        public string DebtorNumber { get; set; }
        public string BillerDebtorNumber { get; set; }
        public string OutletId { get; set; }
        public string ContactID { get; set; }
        public string ShortName { get; set; }
        public string IVRPin { get; set; }
        public string DefaultBillingRef { get; set; }
        public string DefaultJobPriority { get; set; }
        public bool ShouldDeduplicateJobs { get; set; }                             // DeduplicateJobs
        public bool? MustChangePassword { get; set; }

        // Contact details.
        public string FirstName { get; set; }                                       // FullName
        public string LastName { get; set; }
        public string Position { get; set; }
        public string EmailAddress { get; set; }
        public string FaxNumber { get; set; }

        public string IndustryCode { get; set; }

        // Report details.
        public string ReportSendMethod { get; set; }                                // ReportMethod
        public string ReportResultsToSend { get; set; }                             // ResultsType

        // Access rights.
        public string MNUserAccessRights { get; set; }
        public bool HasDistributeWebAccess { get; set; }                            // MedianetWebAccess
        public bool HasMessageConnectWebAccess { get; set; }                        // MessageConnectWebAccess
        public bool HasMessageConnectAdminAccess { get; set; }                      // AdminAccess
        public bool HasJournalistsWebAccess { get; set; }                           // MNJWebAccess
        public bool HasAdminWebAccess { get; set; }                                 // AdminSiteWebAccess
        public bool HasContactsWebAccess { get; set; }                                 // OMAAccess
        public string ContactsAccessRights { get; set; }                            // OMAAccessRights
        public bool HasContactsReportAccess { get; set; }                           // OMAReportAccess
        public bool HasReleaseWatchAccess { get; set; }                             // ReleaseWatchAccess

        public bool CanSendFax { get; set; }                                        // FaxAccess
        public bool CanSendEmail { get; set; }                                      // EmailAccess
        public bool CanSendSMS { get; set; }                                        // SMSAccess
        public bool CanSendVoice { get; set; }                                      // VoiceAccess
        public bool CanSendMailMerge { get; set; }                                  // MailMergeAccess

        public bool? MonitorInternet { get; set; }
        public bool? MonitorText { get; set; }
        public bool? MonitorWire { get; set; }
        public Guid ExpiryToken { get; set; }
        public int ReleaseCount { get; set; }                                       // UnverifiedReleaseCount
        public bool? HasViewedDistributionMessage { get; set; }                     // HasViewedDistMessage

        // Logon history.
        public DateTime? LastLogonDate { get; set; }                                // LastLogonTime
        public Int16 LogonFailureCount { get; set; }
        public int? RecentLogonCount { get; set; }

        public DateTime CreatedDate { get; set; }
        public int CreatedByUserId { get; set; }                                    // CreatedUserId
        public DateTime LastModifiedDate { get; set; }                              // ModifiedDate
        public int LastModifiedByUserId { get; set; }                               // ModifiedUserId
        [Column(TypeName = "char")]
        public string RowStatus { get; set; }                                       // Status

        public Guid UnsubscribeKey { get; set; }
        public string RelevantSubjects { get; set; }        
    }
}
