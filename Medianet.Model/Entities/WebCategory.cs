﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class WebCategory
    {
        public int Id { get; set; }                     // ID
        public string Name { get; set; }
        public int? ParentId { get; set; }
        public int SequenceNumber { get; set; }
        public bool IsHighPriority { get; set; }        // HighPriority
        public bool IsDeleted { get; set; }
    }
}
