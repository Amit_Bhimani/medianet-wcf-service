﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class WebRelease
    {
        public int ReleaseId { get; set; }                  // JobID
        public string Headline { get; set; }
        public string Organisation { get; set; }
        public int? PrimaryWebCategoryId { get; set; }      // PrimaryCategory
        public int? SecondaryWebCategoryId { get; set; }    // SecondaryCategory
        public bool? ShouldShowOnPublic { get; set; }
        public bool? ShouldShowOnJournalists { get; set; }
        public DateTime? PublicDistributedDate { get; set; }
        public DateTime? JournalistsDistributedDate { get; set; }
        public string WasSentToJournalists { get; set; }    // SentToJournalists
        public bool? IsWidgetUpdated { get; set; }
        public string ShortUrl { get; set; }
        public string Summary { get; set; }
        public bool? HasVideo { get; set; }
        public bool? HasAudio { get; set; }
        public bool? HasPhoto { get; set; }
        public bool? HasOther { get; set; }
        public string MultimediaType { get; set; }
        public bool? IsVerified { get; set; }
    }
}
