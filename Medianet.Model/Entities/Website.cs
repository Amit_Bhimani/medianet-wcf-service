﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class Website
    {
        public int Id { get; set; }                             // WebsiteID
        public string System { get; set; }
        public byte[] Logo { get; set; }                        // CompanyImage
        public string LogoFilename { get; set; }                // CompanyImageName
        public string CompanyName { get; set; }
        public string StartingURI { get; set; }
        public string Language { get; set; }
        public int MaxCrawlDepth { get; set; }
        public int MaxPageFetches { get; set; }
        public int VisitorsPerDay { get; set; }
        public bool ShouldIgnoreRobotsFile { get; set; }        // IgnoreRobotsFile
        public bool IsMNJWebsite { get; set; }
        public string Status { get; set; }
        public string DestinationCode { get; set; }             // DESTINATION_CD
        public string HomePage { get; set; }
        public string Schedule { get; set; }
        public int CrawlMethodId { get; set; }

        public DateTime CreatedDate { get; set; }               // CreatedTime
        public int CreatedByUserId { get; set; }                // CreatedUserID
        public DateTime LastModifiedDate { get; set; }          // ModifiedTime
        public int LastModifiedByUserId { get; set; }           // ModifiedUserID
    }
}
