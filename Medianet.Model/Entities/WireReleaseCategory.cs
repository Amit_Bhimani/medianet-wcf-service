﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class WireReleaseCategory
    {
        public int Id { get; set; }             // CategoryId
        public string Name { get; set; }        // CategoryName
        public string Destination { get; set; } // Destination Code
        public string Status { get; set; }      // Status
    }
}
