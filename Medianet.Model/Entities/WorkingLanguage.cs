﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Model.Entities
{
    public class WorkingLanguage
    {
        public int Id { get; set; }         // WorkingLanguageId
        public string Name { get; set; }    // WorkingLanguageName
    }
}
