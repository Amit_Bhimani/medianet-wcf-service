﻿namespace Medianet.Model
{
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using Medianet.Model.Configurations;
    using Medianet.Model.Entities;

    public class MedianetContext : DbContext
    {
        public DbSet<AAPEnvironment> AAPEnvironments { get; set; }
        public DbSet<AAPCity> AAPCities { get; set; }
        public DbSet<ContactRole> ContactRoles { get; set; }
        public DbSet<Continent> Continents { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerBillingCode> CustomerBillingCodes { get; set; }
        public DbSet<CustomerRegion> CustomerRegions { get; set; }
        public DbSet<CustomerStatistic> CustomerStatistics { get; set; }
        public DbSet<CustomerTermsRead> CustomerTermsRead { get; set; }
        public DbSet<CustomerCustomPricing> CustomerCustomPricings { get; set; }
        public DbSet<DayBook> Daybooks { get; set; }
        public DbSet<DayBookType> DayBookTypes { get; set; }
        public DbSet<DBSession> DBSessions { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<FeatureRelease> FeatureReleases { get; set; }
        public DbSet<InboundFileHistory> InboundFileHistories { get; set; }
        public DbSet<InboundFile> InboundFiles { get; set; }
        public DbSet<ListCategory> ListCategories { get; set; }
        public DbSet<ListComment> ListComments { get; set; }
        public DbSet<ListDistribution> ListDistributions { get; set; }
        public DbSet<LogonLog> LogonLogs { get; set; }
        public DbSet<MediaAtlas> MediaAtlas { get; set; }
        public DbSet<MediaContact> MediaContacts { get; set; }
        public DbSet<MediaContactGroup> MediaContactGroups { get; set; }
        public DbSet<MediaContactList> MediaContactLists { get; set; }
        public DbSet<MediaContactListRecord> MediaContactListRecords { get; set; }
        public DbSet<MediaContactListVersion> MediaContactListVersions { get; set; }
        public DbSet<MediaContactListVersionRecord> MediaContactListVersionRecords { get; set; }
        public DbSet<MediaContactMovement> MediaContactMovements { get; set; }
        public DbSet<MediaContactProfile> MediaContactProfiles { get; set; }
        public DbSet<MediaContactSavedSearch> MediaContactSavedSearches { get; set; }
        public DbSet<MediaContactsExport> MediaContactsExports { get; set; }
        public DbSet<MediaContactExportLog> MediaContactExportLog { get; set; }
        public DbSet<MediaContactReminderType> MediaContactReminderTypes { get; set; }
        public DbSet<MediaContactSearchLog> MediaContactSearchLog { get; set; }
        public DbSet<MediaContactTask> MediaContactTasks { get; set; }
        public DbSet<MediaContactTaskCategory> MediaContactTaskCategories { get; set; }
        public DbSet<MediaContactView> MediaContactView { get; set; }
        public DbSet<MediaContactViewSummary> MediaContactViewSummary { get; set; }
        public DbSet<PrnContactView> PrnContactView { get; set; }
        public DbSet<PrnContactViewSummary> PrnContactViewSummary { get; set; }
        public DbSet<MediaDatabaseLog> MediaDatabaseLog { get; set; }
        public DbSet<MediaEmployee> MediaEmployees { get; set; }
        public DbSet<MediaEmployeeRole> MediaEmployeeRoles { get; set; }
        public DbSet<MediaEmployeeSubject> MediaEmployeeSubjects { get; set; }
        public DbSet<MediaOmaContact> MediaOmaContacts { get; set; }
        public DbSet<MediaOmaOutlet> MediaOmaOutlets { get; set; }
        public DbSet<MediaOutlet> MediaOutlets { get; set; }
        public DbSet<MediaContactDraft> MediaContactDrafts { get; set; }
        public DbSet<MediaEmployeeDraft> MediaEmployeeDrafts { get; set; }
        public DbSet<MediaDraftQueue> MediaDraftQueues { get; set; }
        public DbSet<MediaContactJobHistory> MediaContactJobHistories { get; set; }
        public DbSet<MediaOutletProfile> MediaOutletProfiles { get; set; }
        public DbSet<MediaOutletStationFormat> MediaOutletStationFormats { get; set; }
        public DbSet<MediaOutletSubject> MediaOutletSubjects { get; set; }
        public DbSet<MediaOutletView> MediaOutletView { get; set; }
        public DbSet<MediaOutletWorkingLanguage> MediaOutletWorkingLanguages { get; set; }
        public DbSet<MediaPriorityNotice> MediaPriorityNotices { get; set; }
        public DbSet<MediaPrnContact> MediaPrnContacts { get; set; }
        public DbSet<MediaPrnEmployee> MediaPrnEmployees { get; set; }
        public DbSet<MediaPrnOutlet> MediaPrnOutlets { get; set; }
        public DbSet<MediaStatistics> MediaStatistics { get; set; }
        public DbSet<MediumType> MediumTypes { get; set; }
        public DbSet<MonitoringAlert> MonitoringAlerts { get; set; }
        public DbSet<MonitoringCampaign> MonitoringCampaigns { get; set; }
        public DbSet<MonitoringEmail> MonitoringEmails { get; set; }
        public DbSet<MonitoringFrequency> MonitoringFrequencies { get; set; }
        public DbSet<MonitoringMediaType> MonitoringMediaTypes { get; set; }
        public DbSet<MonitoringSearch> MonitoringSearches { get; set; }
        public DbSet<NewsFocus> NewsFocuses { get; set; }
        public DbSet<OutletFrequency> OutletFrequencies { get; set; }
        public DbSet<OutletType> OutletTypes { get; set; }
        public DbSet<PackageList> PackageLists { get; set; }
        public DbSet<PackageAttachment> PackageAttachments { get; set; }
        public DbSet<PackageServiceXref> PackageServiceXrefs { get; set; }
        public DbSet<ProductType> ProductTypes { get; set; }
        public DbSet<Recipient> Recipients { get; set; }
        public DbSet<RegistrationValidation> Registrations { get; set; }
        public DbSet<ReleaseUnverifiedPreview> ReleaseUnverifiedPreviews { get; set; }
        public DbSet<Release> Releases { get; set; }
        public DbSet<ReleaseWireCategory> ReleaseWireCategories { get; set; }
        public DbSet<ReleaseHistory> ReleaseHistories { get; set; }
        public DbSet<Result> Results { get; set; }
        public DbSet<RssLog> RssLog { get; set; }
        public DbSet<SalesRegion> SalesRegions { get; set; }
        public DbSet<ServiceList> ServiceLists { get; set; }
        public DbSet<ServiceListDistribution> ServiceListDistributions { get; set; }
        public DbSet<StationFormat> StationFormats { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<SubjectGroup> SubjectGroups { get; set; }
        //public DbSet<SubjectGroupXref> SubjectGroupXrefs { get; set; }
        public DbSet<TempFile> TempFiles { get; set; }
        public DbSet<Thumbnail> Thumbnails { get; set; }
        public DbSet<TopPerformingList> TopPerformingLists { get; set; }
        public DbSet<Timezone> Timezones { get; set; }
        public DbSet<TrainingCourse> TrainingCourses { get; set; }
        public DbSet<TrainingCourseLocation> TrainingCourseLocation { get; set; }
        public DbSet<TrainingCourseSchedule> TrainingCourseSchedules { get; set; }
        public DbSet<TrainingCourseEnrolment> TrainingCourseEnrolments { get; set; }
        public DbSet<TrainingCoursePromoCode> TrainingCoursePromoCodes { get; set; }
        public DbSet<TrainingCourseTopic> TrainingCourseTopics { get; set; }
        public DbSet<TrainingCourseContact> TrainingCourseContact { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<TwitterCategory> TwitterCategories { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<WebCategory> WebCategories { get; set; }
        public DbSet<WebRelease> WebReleases { get; set; }
        public DbSet<WireReleaseCategory> WireReleaseCategories { get; set; }
        public DbSet<Website> Websites { get; set; }
        public DbSet<WorkingLanguage> WorkingLanguages { get; set; }
        public DbSet<MediaRelatedOutlet> MediaRelatedOutlets { get; set; }
        public DbSet<MediaSyndicatedOutlet> MediaSyndicatedOutlets { get; set; }
        public DbSet<Note> Notes { get; set; }
        public DbSet<OmaDocument> OmaDocument { get; set; }
        public DbSet<OmaDocumentBelongsTo> OmaDocumentBelongsTo { get; set; }
        public DbSet<Mailout> Mailouts { get; set; }
        public DbSet<MailoutRecipient> MailoutRecipients { get; set; }
  		public DbSet<MnjProfile> MnjProfiles { get; set; }
        public DbSet<MnjProfileCategory> MnjProfileCategories { get; set; }
        public DbSet<MnjProfileKeyword> MnjProfileKeywords { get; set; }
        public DbSet<MnjUserSavedReleases> MnjUserSavedReleases { get; set; }
      
		public DbSet<ServiceDeletedUserLog> ServiceDeletedUserLog { get; set; }
        
		public MedianetContext() {
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 300;            
        }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            builder.Configurations
                   .Add(new AAPEnvironmentConfiguration())
                   .Add(new AAPCityConfiguration())
                   .Add(new ContactRoleConfiguration())
                   .Add(new NoteConfiguration())
                   .Add(new ContinentConfiguration())
                   .Add(new CustomerConfiguration())
                   .Add(new CustomerBillingCodeConfiguration())
                   .Add(new CustomerRegionConfiguration())
                   .Add(new CustomerStatisticConfiguration())
                   .Add(new CustomerTermsReadConfiguration())
                   .Add(new CustomerCustomPricingConfiguration())
                   .Add(new DayBookConfiguration())
                   .Add(new DayBookTypeConfiguration())
                   .Add(new DBSessionConfiguration())
                   .Add(new DocumentConfiguration())
                   .Add(new FeatureReleaseConfiguration())
                   .Add(new InboundFileHistoryConfiguration())
                   .Add(new InboundFileConfiguration())
                   .Add(new ListCategoryConfiguration())
                   .Add(new ListCommentConfiguration())
                   .Add(new ListDistributionConfiguration())
                   .Add(new LogonLogConfiguration())
                   .Add(new MediaAtlasConfiguration())
                   .Add(new MediaContactConfiguration())
                   .Add(new MediaContactGroupConfiguration())
                   .Add(new MediaContactListConfiguration())
                   .Add(new MediaContactListRecordConfiguration())
                   .Add(new MediaContactListVersionConfiguration())
                   .Add(new MediaContactListVersionRecordConfiguration())
                   .Add(new MediaContactMovementConfiguration())
                   .Add(new MediaContactProfileConfiguration())
                   .Add(new MediaContactSavedSearchConfiguration())
                   .Add(new MediaContactsExportConfiguration())
                   .Add(new MediaContactExportLogConfiguration())
                   .Add(new MediaContactReminderTypeConfiguration())
                   .Add(new MediaContactSearchLogConfiguration())
                   .Add(new MediaContactTaskConfiguration())
                   .Add(new MediaContactTaskCategoryConfiguration())
                   .Add(new MediaContactViewConfiguration())
                   .Add(new PrnContactViewConfiguration())
                   .Add(new MediaContactViewSummaryConfiguration())
                   .Add(new PrnContactViewSummaryConfiguration())
                   .Add(new MediaDatabaseLogConfiguration())
                   .Add(new MediaEmployeeConfiguration())
                   .Add(new MediaEmployeeRoleConfiguration())
                   .Add(new MediaEmployeeSubjectConfiguration())
                   .Add(new MediaOmaContactConfiguration())
                   .Add(new MediaOmaOutletConfiguration())
                   .Add(new MediaOutletConfiguration())
                   .Add(new MediaContactDraftConfiguration())
                   .Add(new MediaEmployeeDraftConfiguration())
                   .Add(new MediaRelatedOutletConfiguration())
                   .Add(new MediaOutletProfileConfiguration())
                   .Add(new MediaOutletStationFormatConfiguration())
                   .Add(new MediaOutletSubjectConfiguration())
                   .Add(new MediaOutletViewConfiguration())
                   .Add(new MediaOutletWorkingLanguageConfiguration())
                   .Add(new MediaPriorityNoticeConfiguration())
                   .Add(new MediaPrnContactConfiguration())
                   .Add(new MediaPrnEmployeeConfiguration())
                   .Add(new MediaPrnOutletConfiguration())
                   .Add(new MediaStatisticsConfiguration())
                   .Add(new MediaSyndicatedOutletConfiguration())
                   .Add(new MediumTypeConfiguration())
                   .Add(new MonitoringMediaTypeConfiguration())
                   .Add(new MonitoringAlertConfiguration())
                   .Add(new MonitoringCampaignConfiguration())
                   .Add(new MonitoringEmailConfiguration())
                   .Add(new MonitoringFrequencyConfiguration())
                   .Add(new MonitoringSearchConfiguration())
                   .Add(new NewsFocusConfiguration())
                   .Add(new OutletFrequencyConfiguration())
                   .Add(new OutletTypeConfiguration())
                   .Add(new PackageListConfiguration())
                   .Add(new PackageAttachmentConfiguration())
                   .Add(new PackageServiceXrefConfiguration())
                   .Add(new ProductTypeConfiguration())
                   .Add(new RecipientConfiguration())
                   .Add(new RegistrationValidationConfiguration())
                   .Add(new ReleaseUnverifiedPreviewConfiguration())
                   .Add(new ReleaseConfiguration())
                   .Add(new ReleaseHistoryConfiguration())
                   .Add(new ResultConfiguration())
                   .Add(new ReleaseWireCategoryConfiguration())
                   .Add(new RssLogConfiguration())
                   .Add(new SalesRegionConfiguration())
                   .Add(new ServiceListConfiguration())
                   .Add(new ServiceListDistributionConfiguration())
                   .Add(new StationFormatConfiguration())
                   .Add(new SubjectConfiguration())
                   .Add(new SubjectGroupConfiguration())
                   //.Add(new SubjectGroupXrefConfiguration())
                   .Add(new TempFileConfiguration())
                   .Add(new ThumbnailConfiguration())
                   .Add(new TimezoneConfiguration())
                   .Add(new TopPerformingListConfiguration())
                   .Add(new TransactionConfiguration())
                   .Add(new TrainingCourseConfiguration())
                   .Add(new TrainingCourseContactConfiguration())
                   .Add(new TrainingCourseLocationConfiguration())
                   .Add(new TrainingCourseEnrolmentConfiguration())
                   .Add(new TrainingCoursePromoCodeConfiguration())
                   .Add(new TrainingCourseScheduleConfiguration())
                   .Add(new TrainingCourseTopicConfiguration())
                   .Add(new TwitterCategoryConfiguration())
                   .Add(new UserConfiguration())
                   .Add(new WebCategoryConfiguration())
                   .Add(new WebReleaseConfiguration())
                   .Add(new WebsiteConfiguration())
                   .Add(new WireReleaseCategoryConfiguration())
                   .Add(new WorkingLanguageConfiguration())
                   .Add(new OmaDocumentConfiguration())
                   .Add(new OmaDocumentBelongsToConfiguration())
                   .Add(new MailoutConfiguration())
                   .Add(new MailoutRecipientConfiguration())
                   .Add(new ServiceDeletedUserLogConfiguration())
				   .Add(new MnjProfileConfiguration())
                   .Add(new MnjProfileCategoryConfiguration())
                   .Add(new MnjProfileKeywordConfiguration())
                   .Add(new MnjUserSavedReleasesConfiguration())
                   .Add(new MediaDraftQueueConfiguration())
                   .Add(new MediaContactJobHistoryConfiguration());
            
        }
    }
}
