﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Medianet.Salesforce.Models;

namespace Medianet.Salesforce.Interfaces
{
    public interface IForceAAPContactService
    {
        Task<List<SalesForceContact>> GetContacts(string contactEmail, string customerNo);

        Task<bool> UpdateSubjectCategory(string contactId, List<string> subjectCategories);
    }

}
