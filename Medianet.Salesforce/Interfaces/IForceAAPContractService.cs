﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Medianet.Salesforce.Models;

namespace Medianet.Salesforce.Interfaces
{
    public interface IForceAAPContractService
    {
        Task<List<AAPContract>> GetAAPContracts(string customerNumber);

    }
}
