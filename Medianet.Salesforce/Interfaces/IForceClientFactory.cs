﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Salesforce.Force;

namespace Medianet.Salesforce.Interfaces
{
    public interface IForceClientFactory
    {
        IForceClient Create();
        void RefreshToken();

        Task<List<T>> QueryObject<T>(string query);

        Task<T> Describe<T>(string objectName);


    }
}
