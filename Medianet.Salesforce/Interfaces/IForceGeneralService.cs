﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medianet.Salesforce.Interfaces
{
    public interface IForceGeneralService
    {
        Task<List<string>> GetPickListValues(string objectName, string fieldName);

    }
}
