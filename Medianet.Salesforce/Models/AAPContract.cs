﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medianet.Salesforce.Models
{
    public class AAPContract
    {
        public const String SObjectTypeName = "AAP_Contracts__c";

        public string Id { get; set; }

        public DateTime? Contract_Start_Date__c { get; set; }

        public DateTime? Contract_End_Date__c { get; set; }
        public string Contract_Product__c { get; set; }
        public decimal? Contract_Term__c { get; set; }
        public string Status__c { get; set; }

        public decimal? Monthly_Revenue__c { get; set; }
        public bool? T_C_s_accepeted__c { get; set; }

        public string Description__c { get; set; }
        public string Name { get; set; }

    }
}
