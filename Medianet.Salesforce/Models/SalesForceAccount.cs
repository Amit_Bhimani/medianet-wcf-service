﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Salesforce.Common.Models;

namespace Medianet.Salesforce.Models
{
    public class SalesForceAccount
    {
        public string Goverment__c { get; set; }
        public SalesForceUser Owner { get; set; }
    }
}
