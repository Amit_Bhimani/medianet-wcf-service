﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medianet.Salesforce.Models
{
    public class SalesForceContact
    {
        public const String SObjectTypeName = "Contact";
        public string Id { get; set; }
        public string Name { get; set; }
        public string Account_Name__c { get; set; }
        public string Customer_No__c { get; set; }
        public string Positions__c { get; set; }
        public string Phone { get; set; }
        public string RecordTypeId { get; set; }
        public string Email { get; set; }
        public SalesForceAccount Account { get; set; }
        public bool Main_Contact__c { get; set; }
        public string Subject_Category_of_Interest__c { get; set; }

    }
}
