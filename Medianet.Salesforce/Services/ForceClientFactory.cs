﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Security.Policy;
using System.Threading.Tasks;
using Medianet.Salesforce.Interfaces;
using Salesforce.Force;

namespace Medianet.Salesforce.Services
{
    public class ForceClientFactory : IForceClientFactory
    {

        private ForceSettings _forceSettings;
        private static string _instanceURL;
        private static string _accessToken;
        private static string _apiVersion;
        private static object _lock = new object();

        public string AccessToken
        {
            get
            {
                if (string.IsNullOrEmpty(_accessToken))
                {
                    RefreshToken();
                }

                return _accessToken;
            }
        }

        public string InstanceUrl
        {
            get
            {
                if (string.IsNullOrEmpty(_instanceURL))
                {
                    RefreshToken();
                }

                return _instanceURL;
            }
        }

        public string ApiVersion
        {
            get
            {
                if (string.IsNullOrEmpty(_apiVersion))
                {
                    RefreshToken();
                }

                return _apiVersion;
            }
        }

        public ForceClientFactory(ForceSettings forceSettings)
        {
            this._forceSettings =  forceSettings;
        }

        public IForceClient Create()
        {
            // Salesforce requires TLS 1.2
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            return new ForceClient(InstanceUrl, AccessToken, ApiVersion);
        }

        public void RefreshToken()
        {
            lock (_lock)
            {
                // Salesforce requires TLS 1.2
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                using (var auth = new global::Salesforce.Common.AuthenticationClient())
                {
                     auth.UsernamePasswordAsync(_forceSettings.ClientId, _forceSettings.ClientSecret, _forceSettings.UserName, _forceSettings.Password, _forceSettings.Url).Wait();
                   
                    _accessToken = auth.AccessToken;
                    _instanceURL = auth.InstanceUrl;
                    _apiVersion = auth.ApiVersion;
                }
            }
        }

        public async Task<List<T>> QueryObject<T>(string query)
        {
            var retry = 1;

            while (true)
            {
                try
                {
                    var sf = this.Create();

                    var obj = new List<T>();
                    var results = await sf.QueryAsync<T>(query);
                    var totalSize = results.TotalSize;


                    obj.AddRange(results.Records);
                    var nextRecordsUrl = results.NextRecordsUrl;

                    if (!string.IsNullOrEmpty(nextRecordsUrl))
                    {

                        while (true)
                        {
                            var continuationResults = await sf.QueryContinuationAsync<T>(nextRecordsUrl);
                            totalSize = continuationResults.TotalSize;

                            obj.AddRange(continuationResults.Records);
                            if (string.IsNullOrEmpty(continuationResults.NextRecordsUrl)) break;

                            //pass nextRecordsUrl back to sf.QueryAsync to request next set of records
                            nextRecordsUrl = continuationResults.NextRecordsUrl;
                        }
                    }


                    return obj;
                }
                catch (Exception ex)
                {
                    retry++;
                    if (retry > 3) { throw; }
                    this.RefreshToken();
                }
            }
        }

        public async Task<T> QueryRestApi<T>(string endpoint, object param)
        {
            var retry = 1;

            while (true)
            {
                try
                {
                    var sf = this.Create();
                    var quote = await sf.ExecuteRestApiAsync<T>(endpoint, param);
                    return quote;
                }
                catch (Exception)
                {
                    retry++;
                    if (retry > 3) { throw; }
                    this.RefreshToken();
                }
            }
        }

        public async Task<T> Describe<T>(string objectName)
        {
            var retry = 1;

            while (true)
            {
                try
                {
                    var sf = this.Create();
                    var value  = await sf.DescribeAsync<T>(objectName);
                    return value;
                }
                catch (Exception ex)
                {
                    retry++;
                    if (retry > 3) { throw; }
                    this.RefreshToken();
                }
            }

        }



    }
}
