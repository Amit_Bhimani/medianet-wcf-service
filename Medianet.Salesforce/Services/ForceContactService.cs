﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Medianet.Salesforce.Interfaces;
using Medianet.Salesforce.Models;
using Newtonsoft.Json.Linq;

namespace Medianet.Salesforce.Services
{
    public class ForceContactService : IForceAAPContactService
    {
        private IForceClientFactory _client;

        public ForceContactService(ForceSettings forceSettings)
        {
            _client = new ForceClientFactory(forceSettings);
        }

        public async Task<List<SalesForceContact>> GetContacts(string contactEmail, string customerNo)
        {
            string qry = $"SELECT Id,Name, Account_Name__c, Customer_No__c, Positions__c,Phone, RecordTypeId," +
                         $" Email, Account.Goverment__c, Account.Owner.Name, Main_Contact__c, Subject_Category_of_Interest__c " +
                         $" FROM {SalesForceContact.SObjectTypeName} WHERE Email = '{contactEmail}' AND Customer_No__c = '{customerNo}' ";

            var contacts = await _client.QueryObject<SalesForceContact>(qry);
            return contacts;

        }

        public async Task<bool> UpdateSubjectCategory(string contactId, List<string> subjectCategories)
        {
            var client = _client.Create();


            var result = await client.UpdateAsync("Contact", contactId,
                new Contact() {Subject_Category_of_Interest__c = string.Join(";", subjectCategories)});

            return result.Success;
        }
    }
}
