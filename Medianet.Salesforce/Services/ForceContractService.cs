﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Medianet.Salesforce.Interfaces;
using Medianet.Salesforce.Models;

namespace Medianet.Salesforce.Services
{
    public class ForceContractService : IForceAAPContractService
    {
        private IForceClientFactory _client;

        public ForceContractService(ForceSettings forceSettings)
        {
            _client = new ForceClientFactory(forceSettings);
        }


        public async Task<List<AAPContract>> GetAAPContracts(string customerNumber)
        {
            string qry = $"SELECT Id,Contract_Start_Date__c,Contract_End_Date__c," +
                         $"Contract_Product__c, Contract_Term__c, Status__c, Monthly_Revenue__c, T_C_s_accepeted__c" +
                         $"  FROM  {AAPContract.SObjectTypeName} WHERE Customer_no__c = '{customerNumber}' ORDER BY Contract_End_Date__c DESC ";

            var contracts = await _client.QueryObject<AAPContract>(qry);

            return contracts;

        }
    }
}
