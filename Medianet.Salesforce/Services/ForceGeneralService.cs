﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Medianet.Salesforce.Interfaces;
using Newtonsoft.Json.Linq;

namespace Medianet.Salesforce.Services
{
    public class ForceGeneralService : IForceGeneralService
    {
        private IForceClientFactory _client;

        public const string ContactObject = "Contact";
        public const string ContactSubjectOfInterestField = "Subject_Category_of_Interest__c";

        public ForceGeneralService(ForceSettings forceSettings)
        {
            _client = new ForceClientFactory(forceSettings);
        }

        public async Task<List<string>> GetPickListValues(string objectName, string fieldName)
        {
            var picklistValues = new List<string>();

            var objectData = await _client.Describe<JObject>(objectName);
            JToken fieldData = objectData["fields"].FirstOrDefault(j => (string)j["name"] == fieldName);
            if (fieldData != null)
            {
                IList<JToken> picklistData = fieldData["picklistValues"].ToList();
                picklistValues.AddRange(picklistData.Select(picklist => picklist["value"].ToString()));
            }

            return picklistValues;

        }
    }
}
