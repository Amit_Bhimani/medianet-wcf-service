﻿namespace Medianet.Salesforce.Services
{
    public class ForceSettings
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string Url { get; set; }
    }
}
