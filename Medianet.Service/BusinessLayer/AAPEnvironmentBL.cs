﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class AAPEnvironmentBL
    {
        private UnitOfWork _uow;

        public AAPEnvironmentBL(UnitOfWork uow) {
            _uow = uow;
        }

        public AAPEnvironment Get(string type) {
            var repo = new AAPEnvironmentRepository(_uow);
            Model.Entities.AAPEnvironment dbEntity;

            dbEntity = repo.GetByType(type);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.AAPEnvironment, AAPEnvironment>(dbEntity);
        }

        public List<AAPEnvironment> GetStartsWith(string type) {
            var repo = new AAPEnvironmentRepository(_uow);
            List<Model.Entities.AAPEnvironment> dbEntities;

            dbEntities = repo.GetAllByTypeStartsWith(type);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.AAPEnvironment>, List<AAPEnvironment>>(dbEntities);
        }
    }
}
