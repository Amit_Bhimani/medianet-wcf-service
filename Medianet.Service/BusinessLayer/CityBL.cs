﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class CityBL
    {
        private UnitOfWork _uow;

        public CityBL(UnitOfWork uow) {
            _uow = uow;
        }

        public City Get(int id) {
            var repo = new CityRepository(_uow);
            Model.Entities.City dbEntity;

            dbEntity = repo.GetById(id);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.City, City>(dbEntity);
        }

        public List<City> GetAll() {
            var repo = new CityRepository(_uow);
            List<Model.Entities.City> dbEntities;

            dbEntities = repo.GetAll();

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.City>, List<City>>(dbEntities);
        }

        public List<City> GetAllBy(int? continentId, int? countryId, int? stateId, string name) {
            var repo = new CityRepository(_uow);
            List<Model.Entities.City> dbEntities;

            dbEntities = repo.GetAllByAny(continentId, countryId, stateId, name);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.City>, List<City>>(dbEntities);
        }
    }
}
