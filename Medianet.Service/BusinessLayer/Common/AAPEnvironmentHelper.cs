﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Dto;
using Medianet.Service.BusinessLayer;
using Medianet.DataLayer;


namespace Medianet.Service.BusinessLayer.Common
{
    public class AAPEnvironmentHelper
    {
        public static string GetSetting(string type) {
            AAPEnvironment env;

            using (var uow = new UnitOfWork()) {
                var listBL = new AAPEnvironmentBL(uow);
                env = listBL.Get(type);

                if (env != null)
                    return env.Value;
                else
                    return string.Empty;
            }
        }

        public static string GetPath(string type) {
            string path = GetSetting(type);
            
            // Make sure it ends with a \
            if (!path.EndsWith("\\")) path = path + "\\";

            if (System.Configuration.ConfigurationManager.AppSettings["EnvironmentType"].ToString() == Service.Common.Constants2.EnvironmentLocal)
            {
                if (type == "TemplatePath")
                    return "TemplatePath\\";
            }            
            return path;
        }

        public static string GetBlock(string type) {
            int i = 1;
            string block = string.Empty;
            List<AAPEnvironment> envList;
            AAPEnvironment env;

            // Get everything from the database.
            using (var uow = new UnitOfWork()) {
                var listBL = new AAPEnvironmentBL(uow);
                envList = listBL.GetStartsWith(type);
            }

            // Now loop through and get in numeric order, e.g. <type>1, <type>2, <type>3 etc.
            do {
                env = envList.Where(e => e.Type == type + i.ToString()).FirstOrDefault();
                if (env != null)
                    block += env.Value;

                i++;
            } while (env != null);

            return block;
        }
    }
}
