﻿using System.Collections.Generic;

namespace Medianet.Service.BusinessLayer.Common
{
    using System.Configuration;

    public static class Constants
    {
        //public static readonly string GenericServiceErrorMessage =
        //    "An error has occured. If this continues, please contact Client Services on 1300 616 813 or at " +
        //    ConfigurationManager.AppSettings["MailDefaultAddress"] + ".";

        //// Should be a URL or the course of action that the client should take.
        //public const string GenericServiceErrorFaultAction = "information unavailable.";

        // Debtor number used for AAP owned objects.
        public const string DEBTOR_NUMBER_AAP = "1";
        
        // Debtor number used for Journalists.
        public const string DEBTOR_NUMBER_Journalists = "51";

        public static List<string> DEBTOR_NUMBERS_INTERNAL = new List<string>()
        {
            "10",
            "20",
            "32"
        };

        public const int DEFAULT_WEB_CATEGORY_ID = 1;

        // The general wire category used for all wire releases
        public const int DEFAULT_WIRE_CATEGORY = 1;

        // File extensions
        public const string FILETYPE_DOC = "doc";
        public const string FILETYPE_DOCX = "docx";
        public const string FILETYPE_CSV = "csv";
        public const string FILETYPE_EXCEL = "xls";
        public const string FILETYPE_PDF = "pdf";
        public const string FILETYPE_TEXT = "txt";
        public const string FILETYPE_HTM = "htm";
        public const string FILETYPE_HTML = "html";
        public const string FILETYPE_TIFF = "tif";

        // Account Codes
        public const string ACCOUNTCODE_MNET_AAPFAX = "FAX";
        public const string ACCOUNTCODE_MNET_CUSTFAX = "FAX";
        public const string ACCOUNTCODE_MNET_AAPEMAIL = "AEML";
        public const string ACCOUNTCODE_MNET_CUSTEMAIL = "CEML";
        public const string ACCOUNTCODE_MNET_AAPSMS = "ASMS";
        public const string ACCOUNTCODE_MNET_CUSTSMS = "CSMS";
        public const string ACCOUNTCODE_MNET_CUSTVOICE = "CVOI";
        public const string ACCOUNTCODE_MNET_AAPVOICE = "AVOI";
        public const string ACCOUNTCODE_MSGCON_CUSTFAX = "FAXMC";
        public const string ACCOUNTCODE_MSGCON_CUSTEMAIL = "CEMLMC";
        public const string ACCOUNTCODE_MSGCON_CUSTSMS = "CSMSMC";
        public const string ACCOUNTCODE_MSGCON_CUSTVOICE = "CVOIMC";
        public const string ACCOUNTCODE_ANR_FEE = "ANRFEE";
        public const string ACCOUNTCODE_ATTACHMENT_FEE = "ATTFEE";
        public const string ACCOUNTCODE_SECWEBCAT_FEE = "AWCFEE";
        public const string ACCOUNTCODE_PRIORITY_FEE = "HPDIS";

        // Defaults
        public const string TIMEZONE_SYD = "SYD";

        // The Id of the OnlineDB service.
        public const int ONLINEDB_SERVICE_ID = 19751;

        public const int TWITTER_SERVICE_ID = 1;
        public const int PUBLIC_WEBSITE_SERVICE_ID = 2;
        public const int MNJ_WEBSITE_SERVICE_ID = 3;

        public const string ROWSTATUS_ACTIVE = "A"; //RowStatusType.Active

        // The code to send to Finannce to identify Medianet.
        public const string FINANCE_SYSTEM_CODE = "MNET";

        public const string FREELANCE_OUTLET = "AAPO999999999";

        public const int CONTINENT_AUSTRALASIA = 30;
    }
}
