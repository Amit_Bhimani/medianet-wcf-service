﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.DataLayer.Common;
using Medianet.Service.BusinessLayer.Exceptions;

namespace Medianet.Service.BusinessLayer.Common
{
    public class MediaDatabaseHelper
    {
        public const int CONTACT_ID_SIZE = 15;
        public const int OUTLET_ID_SIZE = 15;

        public static string GetNewContactId()
        {
            using (var uow = new UnitOfWork()) {
                MediaAtlasRepository repo = new MediaAtlasRepository(uow);
                string NewId = repo.GetNewContactID();
                uow.Save();
                return PadContactId(NewId);
            }
        }

        public static string GetNewOutletId()
        {
            using (var uow = new UnitOfWork()) {
                MediaAtlasRepository repo = new MediaAtlasRepository(uow);
                string NewId = repo.GetNewOutletID();
                uow.Save();
                return PadOutletId(NewId);
            }
        }

        public static string PadContactId(string id) {
            if (id == null)
                return string.Empty.PadRight(CONTACT_ID_SIZE);
            else
                return id.PadRight(CONTACT_ID_SIZE);
        }

        public static string PadOutletId(string id) {
            if (id == null)
                return string.Empty.PadRight(OUTLET_ID_SIZE);
            else
                return id.PadRight(OUTLET_ID_SIZE);
        }

        public static bool IsMediaDatabaseLocked() {
            using (var uow = new UnitOfWork()) {
                var envRepo = new AAPEnvironmentRepository(uow);
                var dbEntity = envRepo.GetByType("MediaAtlasFreezeStatus");

                if (dbEntity != null && dbEntity.Value.Equals("1"))
                    return true;
                else
                    return false;
            }
        }

        public static AAPCity GetAAPCity(int MediaAtlasCityID, UnitOfWork uow) {
            AAPCityRepository repo = new AAPCityRepository(uow);
            AAPCity aapCity = repo.GetByMediaAtlasCityId(MediaAtlasCityID);

            if (aapCity == null) {
                var cityRepo = new CityRepository(uow);
                var city = cityRepo.GetById(MediaAtlasCityID);

                if (city != null) {
                    aapCity = new AAPCity {
                        Name = city.CityName,
                        PostCode = string.Empty, // city.PostCode,
                        AreaCode = string.Empty, // city.AreaCode,
                        StateName = string.Empty, // city.StateName,
                        MediaAtlasCityId = MediaAtlasCityID,
                        MediaAtlasCountryId = (city.CountryId.HasValue ? city.CountryId.Value : DataLayer.Common.Constants.COUNTRY_ID_AUSTRALIA),
                        RowStatus = Constants.ROWSTATUS_ACTIVE
                    };

                    repo.Add(aapCity);
                }
            }

            return aapCity;
        }

        public static string GetSubjectNames(List<int> subjectIds) {
            using (var uow = new UnitOfWork()) {
                var repo = new SubjectRepository(uow);
                var subjects = repo.GetAllByIds(subjectIds);

                return string.Join("#", subjects.Select(s => s.Name.Trim()));
            }
        }

        public static string FormatName(string firstName, string lastName) {
            return (firstName.Trim() + " " + lastName).Trim();
        }

        public static List<Model.Entities.MediaEmployee> SetSubjectPrimaryContact(List<Model.Entities.MediaEmployee> employees, string outletId, string primaryContactId, int subjectId, string currentContactId, int userId) {
            var employeesToUpdate = new List<Model.Entities.MediaEmployee>();
            var nowDate = DateTime.Now;
            bool needToUpdate;

            // Pad the Id with spaces so comparisons work with values from the database.
            primaryContactId = MediaDatabaseHelper.PadContactId(primaryContactId);

            if (primaryContactId == currentContactId || employees.Exists(e => e.ContactId == primaryContactId)) {
                foreach (var emp in employees) {
                    needToUpdate = false;

                    if (emp.ContactId == primaryContactId) {
                        // This Contact is the Primary contact for the Subject.
                        var subject = emp.Subjects.Where(s => s.SubjectId == subjectId).FirstOrDefault();

                        if (subject == null) {
                            /// The Contact doesn't have the Subject. Add it as Primary.
                            subject = new Model.Entities.MediaEmployeeSubject {
                                OutletId = emp.OutletId,
                                ContactId = emp.ContactId,
                                SubjectId = subjectId,
                                IsPrimaryContact = true
                            };
                            emp.Subjects.Add(subject);
                            needToUpdate = true;
                        }
                        else if (!subject.IsPrimaryContact) {
                            // The Contact has the Subject already but it's not Primary. Set it to Primary.
                            subject.IsPrimaryContact = true;
                            needToUpdate = true;
                        }
                    }
                    else {
                        // This Contact should not be the Primary for the Subject.
                        // If they have the Subject and it's Primary then set it to not Primary.
                        var subject = emp.Subjects.Where(s => s.SubjectId == subjectId).FirstOrDefault();

                        if (subject != null && subject.IsPrimaryContact) {
                            subject.IsPrimaryContact = false;
                            needToUpdate = true;
                        }
                    }

                    if (needToUpdate) {
                        emp.LastModifiedByUserId = userId;
                        emp.SubjectNames = MediaDatabaseHelper.GetSubjectNames(emp.Subjects.Select(s => s.SubjectId).ToList());
                        emp.LastModifiedByUserId = userId;
                        employeesToUpdate.Add(emp);
                    }
                }
            }
            else
                throw new UpdateException(string.Format("Contact with Id '{0}' was not found in Outlet.", primaryContactId.Trim()));

            return employeesToUpdate;
        }

        public static void MergeEmployees(List<Model.Entities.MediaEmployee> employeesToUpdate, List<Model.Entities.MediaEmployee> employeesToAppend) {
            foreach (var emp in employeesToAppend) {
                if (!employeesToUpdate.Exists(e => e.ContactId == emp.ContactId))
                    employeesToUpdate.Add(emp);
            }
        }
    }
}
