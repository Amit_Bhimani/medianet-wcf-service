﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace Medianet.Service.BusinessLayer.Common
{
    public class PasswordHelper
    {
        private const int numberOfRounds = 10;

        public static byte[] GenerateSalt()
        {
            using (var randomNumberGenerator = new RNGCryptoServiceProvider())
            {
                var randomNumber = new byte[32];
                randomNumberGenerator.GetBytes(randomNumber);

                return randomNumber;
            }
        }

        public static string HashPassword(string toBeHashed, byte[] salt)
        {
            using (var rfc2898DeriveBytes = new Rfc2898DeriveBytes(System.Text.Encoding.UTF8.GetBytes(toBeHashed), salt, numberOfRounds))
            {
                return Convert.ToBase64String(rfc2898DeriveBytes.GetBytes(32));
            }
        }

        public static bool ComparePasswords(string pswd1, string pswd2)
        {
            try
            {
                return pswd1.Equals(pswd2);

            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
