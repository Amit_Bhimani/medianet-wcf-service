﻿using System;

namespace Medianet.Service.BusinessLayer.Common
{
    public class PathHelper
    {
        #region DocumentFile

        public static string GetDocumentFilePathAndName(int documentId, string fileExt) {
            return string.Format("{0}{1}\\{2}",
                                 GetDocumentFileBasePath(),
                                 GetDocumentFileFolder(documentId),
                                 GetDocumentFileName(documentId, fileExt));
        }

        public static string GetDocumentFileBasePath() {
            return GetFileBasePath("AttachmentPath");
        }

        public static string GetDocumentFileFolder(int documentId) {
            return ((int)(Math.Floor(((double)(documentId)) / 1000))).ToString();
        }

        public static string GetDocumentFileName(int documentId, string fileExt) {
            return string.Format("MD{0}{1}",
                                documentId,
                                (string.IsNullOrWhiteSpace(fileExt) ? string.Empty : "." + fileExt));
        }

        #endregion

        #region ThumbnailFile

        public static string GetThumbnailFilePathAndName(int thumbnailId, string fileExt) {
            return string.Format("{0}{1}\\{2}",
                                 GetThumbnailFileBasePath(),
                                 GetThumbnailFileFolder(thumbnailId),
                                 GetThumbnailFileName(thumbnailId, fileExt));
        }

        public static string GetThumbnailFileBasePath() {
            return GetFileBasePath("ThumbnailPath");
        }

        public static string GetThumbnailFileFolder(int thumbnailId) {
            return ((int)(Math.Floor(((double)(thumbnailId)) / 1000))).ToString();
        }

        public static string GetThumbnailFileName(int thumbnailId, string fileExt) {
            return string.Format("MT{0}{1}",
                                thumbnailId,
                                (string.IsNullOrWhiteSpace(fileExt) ? string.Empty : "." + fileExt));
        }

        #endregion

        #region TempFile

        public static string GetTempFilePathAndName(int fileId, string fileExt) {
            return string.Format("{0}{1}",
                     GetTempFilePath(),
                     GetTempFileName(fileId, fileExt));
        }

        public static string GetTempFilePath() {
            return GetFileBasePath("WorkArea");
        }

        public static string GetTempFileName(int fileId, string fileExt) {
            return string.Format("Preview_{0}{1}",
                                fileId,
                                (string.IsNullOrWhiteSpace(fileExt) ? string.Empty : "." + fileExt));
        }

        #endregion

        #region InboundFile

        public static string GetInboundFilePathAndName(int fileId, string fileExt) {
            return string.Format("{0}{1}\\{2}",
                                 GetInboundFileBasePath(),
                                 GetInboundFileFolder(fileId),
                                 GetInboundFileName(fileId, fileExt));
        }

        public static string GetInboundFileBasePath() {
            return GetFileBasePath("MessagePath");
        }

        public static string GetInboundFileFolder(int documentId) {
            return ((int)(Math.Floor(((double)(documentId)) / 1000))).ToString();
        }

        public static string GetInboundFileName(int fileId, string fileExt) {
            return string.Format("INB{0}{1}",
                                fileId,
                                (string.IsNullOrWhiteSpace(fileExt) ? string.Empty : "." + fileExt));
        }
        #endregion

        #region Logo Images

        /// <summary>
        /// Routine Builds a FileName based on the fileId passed to it.
        /// The last two digits of the logoId are used to build a path.
        /// The original filename is stripped of spaces and also a timestamp (yyyymmddHHMMss) is added to the name to make it unique.
        /// 
        /// </summary>
        /// <param name="logoId"></param>
        /// <param name="originalFileName"></param>
        /// <param name="createSubdirectory"></param>
        /// <param name="subdir"></param>
        /// <returns></returns>
        public static string BuildLogoImageNameAndPath(string logoId, string originalFileName, bool createSubdirectory, out string subdir)
        {
            var pathId = logoId.Trim();
            if (pathId.Length > 2)
            {
                pathId = pathId.Substring(pathId.Length - 2);
            }
            if (createSubdirectory)
            {
                var dirpath = $"{GetContactsLogoEnvironmentPath()}{pathId}";
                if (!System.IO.Directory.Exists(dirpath))
                {
                    System.IO.Directory.CreateDirectory(dirpath);
                }
            }
            var options = new char[] { ' ', ',', '+', '%', '&',';' };
            var storedFileName = originalFileName;
            foreach (char o in options)
            {
                storedFileName = storedFileName.Replace(o, '-');
            }
            var filename = $"{logoId}_{DateTime.Now.ToString("yyyyMMddHHmmss")}_{storedFileName}";
            subdir = pathId;
            return filename;
        }

        

        public static string GetContactsLogoEnvironmentPath()
        {
            return GetFileBasePath("Contacts_LogoPath");
        }
        public static string GetContactsDraftLogoEnvironmentPath()
        {
            return GetFileBasePath("Contacts_LogoPath_Draft");
        }
		
 		public static string GetPackageAttachmentPath()
        {
            return GetFileBasePath("PackageAttachment_Path");
        }
        #endregion

        public static string GetFileBasePath(string envName) {
            string filePath = AAPEnvironmentHelper.GetSetting(envName);

            if (System.Configuration.ConfigurationManager.AppSettings["EnvironmentType"].ToString() != Service.Common.Constants2.EnvironmentLocal)
            {
                if (string.IsNullOrWhiteSpace(filePath))
                {
                    throw new Exception(string.Format("Cannot find environment setting <{0}>.", envName));
                }
            }

            if (!filePath.EndsWith("\\"))
                filePath += "\\";

            return filePath;
        }

    }
}
