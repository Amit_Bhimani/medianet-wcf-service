﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.BusinessLayer.Common
{
    public class UrlHelper
    {
        private string _releaseUrl;
        private string _documentDownloadUrl;
        private string _thumbnailDownloadUrl;
        private string _wieckMultimediaUrl;
        private string _wieckVideoUrl;

        public UrlHelper() {
            _releaseUrl = null;
            _documentDownloadUrl = null;
            _thumbnailDownloadUrl = null;
            _wieckMultimediaUrl = null;
            _wieckVideoUrl = null;
        }

        public string GetWieckReleaseUrl(MultimediaType multimediaType, string wieckRef) {
            string url = string.Empty;

            if (multimediaType == MultimediaType.Video)
                url = string.Format("{0}{1}", WieckVideoUrl, wieckRef);
            else if (multimediaType == MultimediaType.Multimedia)
                url = string.Format("{0}{1}", WieckMultimediaUrl, wieckRef);

            return url;
        }

        public string GetReleaseUrl(int releaseId, string securityKey = null) {
            if (securityKey == null)
                return String.Format("{0}?id={1}", ReleaseUrl, releaseId);
            else
                return String.Format("{0}?id={1}&k={2}", ReleaseUrl, releaseId, securityKey);
        }

        //public string GetDocumentDownloadUrl(int documentId, string securityKey) {
        //    return string.Format("{0}?id={1}&k={1}", DocumentDownloadUrl, documentId, securityKey);
        //}
        public string GetDocumentDownloadUrl(int releaseId, int sequenceNumber, string securityKey) {
            return string.Format("{0}?j={1}&s={2}&k={3}", DocumentDownloadUrl, releaseId, sequenceNumber, securityKey);
        }

        public string GetImageThumbnailUrl(int thumbnailId) {
            //return string.Format("{0}?id={1}", ThumbnailDownloadUrl, thumbnailId);
            return string.Format("{0}?t={1}&file-type=thumb", ThumbnailDownloadUrl, thumbnailId);
        }

        public string GetWieckImageThumbnailUrl(string wieckRef, string mediaRef, int height, int width, string contentType) {
            string prefix;

            if (contentType.StartsWith("image", StringComparison.CurrentCultureIgnoreCase))
                prefix = "/photos/";
            else if (contentType.StartsWith("video", StringComparison.CurrentCultureIgnoreCase))
                prefix = "/videos/";
            else
                prefix = "/";

            return string.Format("{0}{1}{2}{3}-{4}x{5}.jpg", WieckMultimediaUrl, wieckRef, prefix, mediaRef, width, height);
        }

        public string GetWieckReleaseThumbnailUrl(string wieckRef, int height, int width) {
            return string.Format("{0}{1}-{2}x{3}.jpg", WieckMultimediaUrl, wieckRef, width, height);
        }

        public string ReleaseUrl {
            get {
                if (string.IsNullOrWhiteSpace(_releaseUrl))
                    _releaseUrl = AAPEnvironmentHelper.GetSetting("PublicWebsiteUrl");

                return _releaseUrl;
            }
        }

        public string DocumentDownloadUrl {
            get {
                if (string.IsNullOrWhiteSpace(_documentDownloadUrl))
                    _documentDownloadUrl = AAPEnvironmentHelper.GetSetting("DocumentDownloadUrl");

                return _documentDownloadUrl;
            }
        }

        public string ThumbnailDownloadUrl {
            get {
                if (string.IsNullOrWhiteSpace(_thumbnailDownloadUrl))
                    _thumbnailDownloadUrl = AAPEnvironmentHelper.GetSetting("ThumbnailDownloadUrl");

                return _thumbnailDownloadUrl;
            }
        }

        public string WieckMultimediaUrl {
            get {
                if (string.IsNullOrWhiteSpace(_wieckMultimediaUrl))
                    _wieckMultimediaUrl = AAPEnvironmentHelper.GetSetting("WieckMultimediaURL");

                return _wieckMultimediaUrl;
            }
        }

        public string WieckVideoUrl {
            get {
                if (string.IsNullOrWhiteSpace(_wieckVideoUrl))
                    _wieckVideoUrl = AAPEnvironmentHelper.GetSetting("WieckVideoURL");

                return _wieckVideoUrl;
            }
        }

        public Size GetThumbnailSize(int height, int width) {
            if (height == 0 || width == 0)
                return new Size(0, 0);
            else {
                int maxThumbnailWidth = 100;
                int maxThumbnailHeight = 100;
                var result = new Size(height, width);

                // Scale down the width to meet width restrictions.
                if (width > maxThumbnailWidth) {
                    double scale = ((double)maxThumbnailWidth) / ((double)width);

                    result.Width = maxThumbnailWidth;
                    result.Height = (int)(((double)height) * scale);
                }
                else if (width == 0) {
                    result.Width = maxThumbnailWidth;
                    result.Height = maxThumbnailHeight;
                }

                return result;
            }
        }
    }

    public class Size
    {
        public Size(int height, int width) {
            Height = height;
            Width = width;
        }

        public int Height { get; set; }
        public int Width { get; set; }
    }
}
