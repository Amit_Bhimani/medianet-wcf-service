﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class ContactRoleBL
    {
        private UnitOfWork _uow;

        public ContactRoleBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public ContactRole Get(int id)
        {
            var repo = new ContactRoleRepository(_uow);
            Model.Entities.ContactRole dbEntities;

            dbEntities = repo.Get(id);

            return AutoMapper.Mapper.Map<Model.Entities.ContactRole, ContactRole>(dbEntities);
        }

        public List<ContactRole> GetAll()
        {
            var repo = new ContactRoleRepository(_uow);
            List<Model.Entities.ContactRole> dbEntities;

            dbEntities = repo.GetAll();

            return AutoMapper.Mapper.Map<List<Model.Entities.ContactRole>, List<ContactRole>>(dbEntities);
        }
    }
}
