﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class ContinentBL
    {
        private UnitOfWork _uow;

        public ContinentBL(UnitOfWork uow) {
            _uow = uow;
        }

        public Continent Get(int id) {
            var repo = new ContinentRepository(_uow);
            Model.Entities.Continent dbEntity;

            dbEntity = repo.GetById(id);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.Continent, Continent>(dbEntity);
        }

        public List<Continent> GetAll() {
            var repo = new ContinentRepository(_uow);
            List<Model.Entities.Continent> dbEntities;

            dbEntities = repo.GetAll();

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.Continent>, List<Continent>>(dbEntities);
        }
    }
}
