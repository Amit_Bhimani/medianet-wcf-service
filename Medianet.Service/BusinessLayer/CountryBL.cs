﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class CountryBL
    {
        private UnitOfWork _uow;

        public CountryBL(UnitOfWork uow) {
            _uow = uow;
        }

        public Country Get(int id) {
            var repo = new CountryRepository(_uow);
            Model.Entities.Country dbEntity;

            dbEntity = repo.GetById(id);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.Country, Country>(dbEntity);
        }

        public List<Country> GetAll() {
            var repo = new CountryRepository(_uow);
            List<Model.Entities.Country> dbEntities;

            dbEntities = repo.GetAll();

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.Country>, List<Country>>(dbEntities);
        }
    }
}
