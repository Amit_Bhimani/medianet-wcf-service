﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class CustomerBL
    {
        private UnitOfWork _uow;

        public CustomerBL(UnitOfWork uow) {
            _uow = uow;
        }

        public Customer Get(string debtorNumber) {
            var repo = new CustomerRepository(_uow);
            Model.Entities.Customer dbEntity;

            dbEntity = repo.GetByDebtorNumber(debtorNumber);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.Customer, Customer>(dbEntity);
        }

        public Customer GetByName(string name) {
            var repo = new CustomerRepository(_uow);
            Model.Entities.Customer dbEntity;

            dbEntity = repo.GetByName(name);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.Customer, Customer>(dbEntity);
        }

        public CustomerStatistic GetStatistics(string debtorNumber)
        {
            var repo = new CustomerStatisticRepository(_uow);
            Model.Entities.CustomerStatistic dbEntity;

            dbEntity = repo.GetByDebtorNumber(debtorNumber);

            // If for some reason we don't have a record create a fake one.
            if (dbEntity == null)
            {
                dbEntity = new Model.Entities.CustomerStatistic();
                dbEntity.DebtorNumber = debtorNumber;
                dbEntity.CreatedDate = DateTime.Now;
                dbEntity.BusiestDayOfWeek = "Sunday";
            }

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.CustomerStatistic, CustomerStatistic>(dbEntity);
        }

        public void Add(Customer entity) {
            var repo = new CustomerRepository(_uow);
            Model.Entities.Customer dbEntity;

            dbEntity = AutoMapper.Mapper.Map<Customer, Model.Entities.Customer>(entity);

            repo.Add(dbEntity);
        }

        public void Update(Customer entity, int userId) {
            var dbEntity = AutoMapper.Mapper.Map<Customer, Model.Entities.Customer>(entity);
            var repo = new CustomerRepository(_uow);

            entity.LastModifiedDate = DateTime.Now;
            entity.LastModifiedByUserId = userId;

            repo.Update(dbEntity);
        }

        public void Delete(string debtorNumber, int userId) {
            var custRepo = new CustomerRepository(_uow);
            var userRepo = new UserRepository(_uow);
            string statusDeleted = ((char)RowStatusType.Deleted).ToString();
            var cust = custRepo.GetByDebtorNumber(debtorNumber);

            if (cust != null) {
                // Set the status of all the Users for this Customer to Deleted first.
                var users = userRepo.GetAllByDebtorNumber(debtorNumber);

                foreach (var user in users) {
                    user.RowStatus = statusDeleted;
                    user.LastModifiedDate = DateTime.Now;
                    user.LastModifiedByUserId = userId;
                    userRepo.Update(user);
                }

                // Now set the status of the Customer to deleted.
                cust.RowStatus = statusDeleted;
                cust.LastModifiedDate = DateTime.Now;
                cust.LastModifiedByUserId = userId;
                custRepo.Update(cust);
            }
        }

        public void AcknowledgeTermsRead(int userId, string primaryName, string primaryEmailAddress, SystemType system)
        {
            var userRepo = new UserRepository(_uow);
            var u = userRepo.GetById(userId);

            new CustomerRepository(_uow).AcknowledgeTermsRead(u.DebtorNumber, userId, primaryEmailAddress, ((char)system).ToString());

            if (!string.IsNullOrWhiteSpace(primaryName) && !string.IsNullOrWhiteSpace(primaryEmailAddress))
                EmailHelper.TermsReadEmail(u.FirstName.Trim(), DateTime.Now, primaryName, primaryEmailAddress, system);
        }

        /// <summary>
        /// Get New DebtorNumber from mn_Environment Table from Type AfterChargeBeeDebtorNumber Default Value: 20000
        /// </summary>
        /// <returns></returns>
        public string GetNewDebtorNumber()
        {
            var envRepo = new AAPEnvironmentRepository(_uow);
            int newNumber = envRepo.GetValue("AfterChargeBeeDebtorNumber", "OPR").ConvertToInt();
            string result;
            if (newNumber.ToString().Length < 6)
                result = "E0" + newNumber;
            else
                result = "E" + newNumber;
            
            return result;
        }

        /// <summary>
        /// Update by 1 number in mn_Environment table for customer added to Database
        /// </summary>
        public void UpdateEnvironmentVariable()
        {
           var envRepo = new AAPEnvironmentRepository(_uow);
           var dbEntity = envRepo.GetByType("AfterChargeBeeDebtorNumber");
           dbEntity.Value = (dbEntity.Value.ConvertToInt() + 1).ToString();
           envRepo.Update(dbEntity);
        }
    }
}
