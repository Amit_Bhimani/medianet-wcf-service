﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class CustomerBillingCodeBL
    {
        private UnitOfWork _uow;

        public CustomerBillingCodeBL(UnitOfWork uow) {
            _uow = uow;
        }

        public List<CustomerBillingCode> GetAllByDebtorNumber(string debtorNumber) {
            var repo = new CustomerBillingCodeRepository(_uow);
            List<Model.Entities.CustomerBillingCode> dbEntities;

            dbEntities = repo.GetAllByDebtorNumber(debtorNumber);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.CustomerBillingCode>, List<CustomerBillingCode>>(dbEntities);
        }

        public int Add(CustomerBillingCode entity)
        {
            var dbEntity = AutoMapper.Mapper.Map<CustomerBillingCode, Model.Entities.CustomerBillingCode>(entity);
            var repo = new CustomerBillingCodeRepository(_uow);

            repo.Add(TruncateCustomerBillingCode(dbEntity));

            return dbEntity.Id;
        }

        public void Update(CustomerBillingCode entity)
        {
            var dbEntity = AutoMapper.Mapper.Map<CustomerBillingCode, Model.Entities.CustomerBillingCode>(entity);
            var repo = new CustomerBillingCodeRepository(_uow);

            repo.Update(TruncateCustomerBillingCode(dbEntity));
        }

        public void Delete(int id)
        {
            var repo = new CustomerBillingCodeRepository(_uow);

            repo.Delete(id);
        }

        #region Private methods

        private Model.Entities.CustomerBillingCode TruncateCustomerBillingCode(Model.Entities.CustomerBillingCode dbEntity)
        {
            dbEntity.Name = dbEntity.Name.Truncate(20);

            return dbEntity;
        }

        #endregion
    }
}
