﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class CustomerCustomPricingBL
    {
        private UnitOfWork _uow;

        public CustomerCustomPricingBL(UnitOfWork uow) {
            _uow = uow;
        }
        public CustomerCustomPricing GetCustomPrice(int id)
        {
            var repo = new CustomerCustomPricingRepository(_uow);
            Model.Entities.CustomerCustomPricing dbEntity;

            dbEntity = repo.GetById(id);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.CustomerCustomPricing, CustomerCustomPricing>(dbEntity);
        }
        public List<CustomerCustomPricing> GetAllCustomPricing(string debtorNumber) 
        {
            var repo = new CustomerCustomPricingRepository(_uow);
            List<Model.Entities.CustomerCustomPricing> dbEntities;

            dbEntities = repo.GetCustomerCustomPricingByDebtorNumber(debtorNumber);
            
            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.CustomerCustomPricing>, List<CustomerCustomPricing>>(dbEntities);
        }
    }
}
