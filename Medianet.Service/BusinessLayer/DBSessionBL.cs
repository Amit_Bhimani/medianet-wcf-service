﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;
using Medianet.Service.BusinessLayer.Exceptions;
using Medianet.Service.BusinessLayer.Common;

namespace Medianet.Service.BusinessLayer
{
    public class DBSessionBL
    {
        private UnitOfWork _uow;

        public DBSessionBL(UnitOfWork uow) {
            _uow = uow;
        }

        public DBSession Get(string key, bool allowPasswordChangingSessions = false) {
            Model.Entities.DBSession dbEntity;

            dbEntity = GetSession(key, allowPasswordChangingSessions);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.DBSession, DBSession>(dbEntity);
        }

        public DBSession Authorise(string key, string password)
        {
            var repo = new DBSessionRepository(_uow);
            Model.Entities.DBSession dbEntity;

            dbEntity = GetSession(key, true);

            // Session does not exist
            if (dbEntity == null)
                throw new SessionInvalidException("Session key supplied is invalid or expired. Please login again.");

            // check the password matches whats supplied
            bool passwordsEqual = PasswordHelper.ComparePasswords(PasswordHelper.HashPassword(password, Convert.FromBase64String(dbEntity.User.Salt)), dbEntity.User.HashedPassword);

            if (!passwordsEqual)
                throw new AuthenticationFailureException("Unable to Authenticate session. User credentials do not match password.");

            // Set the LastAccessedDate to now on the database, and Authorise the session.
            dbEntity.LastAccessedDate = DateTime.Now;
            dbEntity.IsAuthorised = true;
            repo.Update(dbEntity);

            return AutoMapper.Mapper.Map<Model.Entities.DBSession, DBSession>(dbEntity);
        }

        public DBSession Validate(string key) {
            var repo = new DBSessionRepository(_uow);
            Model.Entities.DBSession dbEntity;

            dbEntity = GetSession(key, true);

            if (dbEntity == null)
                return null;

            // Set the LastAccessedDate to now on the database.
            dbEntity.LastAccessedDate = DateTime.Now;
            repo.Update(dbEntity);

            return AutoMapper.Mapper.Map<Model.Entities.DBSession, DBSession>(dbEntity);
        }

        public void Logout(string key) {
            var repo = new DBSessionRepository(_uow);
            int id = CheckSessionKeyIsNumeric(key);

            repo.Delete(id);
        }

        public DBSession CreateSession(User user, SystemType system, bool isForAdmin = false, bool isImpersonating = false, bool forcePasswordChange = false, bool isAuthorised = true) {
            var repo = new DBSessionRepository(_uow);
            List<Model.Entities.DBSession> dbSessions = null;
            Model.Entities.DBSession dbEntity;
            string dbSystem = ((char)system).ToString();
            var errorCode = LogonErrorType.None;
            var nowDate = DateTime.Now;

            try {
                if (!isImpersonating) {
                    // If it's the Medianet Contacts website, make sure they haven't exceeded their maximum licenses.
                    // Don't check if changing their forgotten password. We will give them a special password changing session for now.
                    // If being called from admin then let contacts users re-use an old session.
                    if (system == SystemType.Contacts && !isForAdmin) {
                        if (!forcePasswordChange)
                            CheckContactsLicensesAvailable(repo, user.DebtorNumber, user.Id, dbSystem);
                    }
                    else {
                        // For other websites, if they already have a session then use it again, just to cut down on active sessions.
                        // Make sure the DebtorNumber is the same. When called from UpdateAccountUnverified we are
                        // migrating to a new DebtorNumber. We don't want to return a session for the old one.
                        dbSessions = repo.GetAllByUserSystem(user.Id, dbSystem).Where(s => s.IsImpersonating == false && s.DebtorNumber == user.DebtorNumber).ToList();
                    }
                }

                if (dbSessions == null || dbSessions.Count == 0) {
                    // create a new DBSession.
                    dbEntity = new Model.Entities.DBSession() {
                        DebtorNumber = user.DebtorNumber,
                        UserId = user.Id,
                        System = dbSystem,
                        RowStatus = ((char)(forcePasswordChange ? RowStatusType.ChangingPassword : RowStatusType.Active)).ToString(),
                        LoginDate = nowDate,
                        LastAccessedDate = nowDate,
                        IsImpersonating = isImpersonating,
                        IsAuthorised = isAuthorised
                    };

                    dbEntity.Id = CreateSessionId(repo);
                    dbEntity = repo.Add(dbEntity);
                }
                else {
                    dbEntity = dbSessions[0];

                    // Use the existing DBSession.
                    dbEntity.DebtorNumber = user.DebtorNumber;
                    dbEntity.LoginDate = nowDate;
                    dbEntity.LastAccessedDate = nowDate;
                    dbEntity.RowStatus = ((char)RowStatusType.Active).ToString();
                    dbEntity.IsAuthorised = isAuthorised;

                    repo.Update(dbEntity);
                }

                // Map the database object to a friendly representation of the data.
                return AutoMapper.Mapper.Map<Model.Entities.DBSession, DBSession>(dbEntity);
            }
            catch (Exception ex) {
                if (ex is LogonLicensesExceededException)
                    errorCode = LogonErrorType.LicensesExceeded;
                else if (ex is SessionAlreadyExistsException)
                    errorCode = LogonErrorType.SessionAlreadyExists;
                else
                    errorCode = LogonErrorType.Unknown;
                throw;
            }
            finally {
                // Log the logon, unless they are impersonating or changing a forgotten password.
                if (!isImpersonating) // && !forcePasswordChange)
                    AddLogonLog(user.Id, nowDate, system, errorCode);
            }
        }

        public void AddLogonLog(int userId, DateTime logonDate, SystemType system, LogonErrorType errorCode)
        {
            var repo = new LogonLogRepository(_uow);
            var dbEntity = new Model.Entities.LogonLog();

            dbEntity.LoginDate = logonDate;
            dbEntity.UserId = userId;
            dbEntity.System = ((char)system).ToString();
            dbEntity.ErrorCode = (int)errorCode;

            repo.Add(dbEntity);
        }

        #region Private methods

        private void CheckContactsLicensesAvailable(DBSessionRepository repo, string debtorNumber, int userId, string system) {
            List<Model.Entities.DBSession> dbSessions;
            Model.Entities.DBSession userDBSession;

            int maxLicenses;

            dbSessions = repo.GetAllActiveByCustomerSystem(debtorNumber, system)
                            .Where(s => s.IsImpersonating == false).ToList();
           
            // If we got any sessions then get the MediaContactsLicenseCount from it, otherwise fetch from the Customer.
            if (dbSessions.Count > 0)
                maxLicenses = dbSessions[0].Customer.MediaContactsLicenseCount;
            else
            {
                var customerBL = new CustomerBL(_uow);
                var cust = customerBL.Get(debtorNumber);
                maxLicenses = cust.MediaContactsLicenseCount;
            }

            userDBSession = dbSessions.FirstOrDefault(s => s.UserId == userId);

            // If this user is already logged in then throw an exception.
            if (userDBSession != null) {
                DBSession session = AutoMapper.Mapper.Map<Model.Entities.DBSession, DBSession>(userDBSession);
                throw new SessionAlreadyExistsException(session);
            }
            
            // If the Customer has reached their max licenses then throw an exception.
            if (dbSessions.Count >= maxLicenses)
            {
                List<DBSession> sessions = AutoMapper.Mapper.Map<List<Model.Entities.DBSession>, List<DBSession>>(dbSessions);
                throw new LogonLicensesExceededException(debtorNumber, sessions, maxLicenses);
            }
        }

        private Model.Entities.DBSession GetSession(string key, bool allowPasswordChangingSessions = false) {
            var repo = new DBSessionRepository(_uow);
            Model.Entities.DBSession dbEntity;
            int id = CheckSessionKeyIsNumeric(key);

            if (allowPasswordChangingSessions) {
                dbEntity = repo.GetById(id);

                // Only return an active session or one for changing a forgotten password.
                if (dbEntity != null) {
                    var rowStatus = Medianet.Service.Common.ExtensionMethods.ToEnum<Medianet.Service.Common.RowStatusType>(dbEntity.RowStatus);
                    if (rowStatus != RowStatusType.Active && rowStatus != RowStatusType.ChangingPassword)
                        dbEntity = null;
                }
            }
            else
                dbEntity = repo.GetActiveById(id);

            return dbEntity;
        }

        private int CreateSessionId(DBSessionRepository repo) {
            Model.Entities.DBSession dbEntity;
            int sessionId = 0;
            Random r = new Random();
            bool found = false;
            int tries = 0;

            // Make sure we create a unique SessionKey.
            while (!found) {
                sessionId = r.Next();

                dbEntity = repo.GetById(sessionId);

                if (dbEntity == null)
                    found = true;
                else {
                    tries++;
                    if (tries > 20)
                        throw new Exception("Failed to get unique Session Key.");
                }
            }

            return sessionId;
        }

        private static int CheckSessionKeyIsNumeric(string sessionKey) {
            int sessionId;

            if (!string.IsNullOrWhiteSpace(sessionKey))
                if (int.TryParse(sessionKey, out sessionId))
                    return sessionId;

            throw new ArgumentException(string.Format("SessionKey {0} is not valid.", sessionKey));
        }

        #endregion
    }
}
