﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class DayBookBL
    {
        private UnitOfWork _uow;

        public DayBookBL(UnitOfWork uow) {
            _uow = uow;
        }

        public Career GetCareer() {
            var career = new Career();
            var repo = new DayBookRepository(_uow);
            var dbEntity = repo.GetLatestByType(5);

           if (dbEntity != null)
            {
                career.Description = dbEntity.Text;
            }

            return career;
        }
    }
}
