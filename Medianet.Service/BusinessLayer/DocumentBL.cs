﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class DocumentBL
    {
        private UnitOfWork _uow;

        public DocumentBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public Document Get(int id)
        {
            var repo = new DocumentRepository(_uow);
            Model.Entities.Document dbEntity;

            dbEntity = repo.GetById(id);

            return AutoMapper.Mapper.Map<Model.Entities.Document, Document>(dbEntity);
        }

        public List<Document> GetAllByReleaseId(int releaseId)
        {
            var repo = new DocumentRepository(_uow);
            List<Model.Entities.Document> dbEntities;

            dbEntities = repo.GetAllByReleaseId(releaseId);

            return AutoMapper.Mapper.Map<List<Model.Entities.Document>, List<Document>>(dbEntities);
        }

        public Document GetPrimaryByReleaseId(int releaseId)
        {
            var repo = new DocumentRepository(_uow);
            Model.Entities.Document dbEntity;

            dbEntity = repo.GetPrimaryByReleaseId(releaseId);

            return AutoMapper.Mapper.Map<Model.Entities.Document, Document>(dbEntity);
        }

        public Document GetByReleaseIdSequenceNumber(int releaseId, int sequenceNumber)
        {
            var repo = new DocumentRepository(_uow);
            Model.Entities.Document dbEntity;

            dbEntity = repo.GetByReleaseIdSequenceNumber(releaseId, sequenceNumber);

            return AutoMapper.Mapper.Map<Model.Entities.Document, Document>(dbEntity);
        }

        public int Add(Document entity, int userId)
        {
            var dbEntity = AutoMapper.Mapper.Map<Document, Model.Entities.Document>(entity);
            var repo = new DocumentRepository(_uow);

            dbEntity.CreatedDate = DateTime.Now;
            dbEntity.CreatedByUserId = userId;
            dbEntity.LastModifiedDate = dbEntity.CreatedDate;
            dbEntity.LastModifiedByUserId = userId;

            repo.Add(dbEntity);

            return dbEntity.Id;
        }

        public void DeleteByReleaseId(int releaseId)
        {
            var repo = new DocumentRepository(_uow);

            repo.DeleteAllByReleaseId(releaseId);
        }
    }
}
