﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer.Exceptions
{
    public class AuthenticationFailureException : Exception
    {
        public AuthenticationFailureException(string message)
            : base(message)
        {
        }
    }
}
