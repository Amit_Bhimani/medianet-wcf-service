﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer.Exceptions
{
    public class DuplicateNameException : Exception
    {
        public List<string> ReasonList;

        public DuplicateNameException(string message, List<string> list = null)
            : base(message)
        {
            ReasonList = list;
        }
    }
}
