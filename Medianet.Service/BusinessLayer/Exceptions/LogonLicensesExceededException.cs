﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer.Exceptions
{
    public class LogonLicensesExceededException : Exception
    {
        public int LicenseCount { get; set; }

        public List<DBSession> ActiveSessions { get; set; }

        public LogonLicensesExceededException(string debtorNumber, List<DBSession> sessions, int licenses)
            : base(string.Format("The maximum number of exceptions for Customer with DebtorNumber='{0}' has been reached.", debtorNumber))
        {
            LicenseCount = licenses;
            ActiveSessions = sessions;
        }
    }
}
