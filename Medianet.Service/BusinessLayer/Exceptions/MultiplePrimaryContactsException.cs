﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer.Exceptions
{
    public class MultiplePrimaryContactsException : Exception
    {
        public string OutletId { get; set; }

        public MultiplePrimaryContactsException(string outletId, string message)
            : base(message)
        {
            OutletId = outletId;
        }
    }
}
