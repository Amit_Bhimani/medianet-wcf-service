﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer.Exceptions
{
    public class SessionAlreadyExistsException : Exception
    {
        public DBSession ActiveSession { get; set; }

        public SessionAlreadyExistsException(DBSession session)
            : base("A sesson already exists for this User.")
        {
            ActiveSession = session;
        }
    }
}
