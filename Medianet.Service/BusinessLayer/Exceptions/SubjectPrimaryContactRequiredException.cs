﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer.Exceptions
{
    public class SubjectPrimaryContactRequiredException : Exception
    {
        public string OutletId { get; set; }

        public int SubjectId { get; set; }

        public SubjectPrimaryContactRequiredException(string outletId, int subjectId, string message)
            : base(message)
        {
            OutletId = outletId;
            SubjectId = subjectId;
        }
    }
}
