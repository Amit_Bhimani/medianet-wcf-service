﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer.Exceptions
{
    public class UpdateException : Exception
    {
        public List<string> ReasonList;

        public UpdateException(string message, List<string> list = null)
            : base(message) {
            ReasonList = list;
        }
    }
}
