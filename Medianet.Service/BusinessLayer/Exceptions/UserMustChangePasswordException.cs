﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer.Exceptions
{
    public class UserMustChangePasswordException : Exception
    {
        public UserMustChangePasswordException()
            : base("User must change password.")
        {
        }
    }
}
