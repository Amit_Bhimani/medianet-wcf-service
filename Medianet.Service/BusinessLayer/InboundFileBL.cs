﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class InboundFileBL
    {
        private UnitOfWork _uow;

        public InboundFileBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public InboundFile Get(int id) {
            var repo = new InboundFileRepository(_uow);
            Model.Entities.InboundFile dbEntity;

            dbEntity = repo.GetById(id);

            return AutoMapper.Mapper.Map<Model.Entities.InboundFile, InboundFile>(dbEntity);
        }

        public List<InboundFile> GetAllByDebtorNumberType(string debtorNumber, MessageType type, InboundFileSortOrderType sortOrder, int startPos, int pageSize) {
            var repo = new InboundFileRepository(_uow);
            List<Model.Entities.InboundFile> dbEntities;

            dbEntities = repo.GetAllByDebtorNumberType(debtorNumber, ((char)type).ToString(), 
                ((char)InboundFileStatusType.Ready).ToString(), (int)sortOrder, startPos, pageSize);

            return AutoMapper.Mapper.Map<List<Model.Entities.InboundFile>, List<InboundFile>>(dbEntities);
        }

        public int GetCountByDebtorNumberType(string debtorNo, MessageType system) {
            var repo = new InboundFileRepository(_uow);

            return repo.GetCountByDebtorNumberType(debtorNo, ((char)system).ToString(), ((char)InboundFileStatusType.Ready).ToString());
        }

        public void Update(InboundFile entity, int userId) {
            var dbEntity = AutoMapper.Mapper.Map<InboundFile, Model.Entities.InboundFile>(entity);
            var repo = new InboundFileRepository(_uow);

            entity.LastModifiedDate = DateTime.Now;
            entity.LastModifiedByUserId = userId;

            repo.Update(dbEntity);
        }

        public void Delete(int id) {
            var repo = new InboundFileRepository(_uow);
            repo.Delete(id);
        }

        public void AddUsageLog(int inboundFileId, int releaseId, int userId) {
            var repo = new InboundFileRepository(_uow);
            repo.AddUsageLog(inboundFileId, releaseId, userId);
        }
    }
}
