﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class ListCategoryBL
    {
        private UnitOfWork _uow;

        public ListCategoryBL(UnitOfWork uow) {
            _uow = uow;
        }

        public ListCategory Get(int id) {
            var repo = new ListCategoryRepository(_uow);
            Model.Entities.ListCategory dbEntity;

            dbEntity = repo.GetById(id);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.ListCategory, ListCategory>(dbEntity);
        }

        public List<ListCategory> GetAll() {
            var repo = new ListCategoryRepository(_uow);
            List<Model.Entities.ListCategory> dbEntities;

            dbEntities = repo.GetAll();

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.ListCategory>, List<ListCategory>>(dbEntities);
        }
    }
}
