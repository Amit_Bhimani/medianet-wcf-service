﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class MediaBL
    {
        private UnitOfWork _uow;

        public MediaBL(UnitOfWork uow) {
            _uow = uow;
        }

        public MediaStatistics GetStatistics() {
            var repo = new MediaStatisticsRepository(_uow);
            Model.Entities.MediaStatistics dbEntity;

            dbEntity = repo.Get();

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.MediaStatistics, MediaStatistics>(dbEntity);
        }
    }
}
