﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;
using Medianet.Service.BusinessLayer.Common;
using Medianet.Service.BusinessLayer.Exceptions;

namespace Medianet.Service.BusinessLayer
{
    public class MediaContactBL
    {
        private UnitOfWork _uow;

        public MediaContactBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public MediaContact Get(string id)
        {
            var repo = new MediaContactRepository(_uow);
            MediaContact retval;
            Model.Entities.MediaContact dbEntity;
            MediaContactProfileBL profileBL = new MediaContactProfileBL(_uow);
            dbEntity = repo.GetById(id);

            // Map the database object to a friendly representation of the data.
            retval = AutoMapper.Mapper.Map<Model.Entities.MediaContact, MediaContact>(dbEntity);
            /*if (retval != null)
            {
                retval.MediaContactProfile = profileBL.Get(retval.Id);
            }*/

            return retval;
        }

        public List<MediaEmployee> GetAllEmployees(string id)
        {
            var repo = new MediaEmployeeRepository(_uow);
            List<Model.Entities.MediaEmployee> dbEntities;

            dbEntities = repo.GetAllByContactId(id);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.MediaEmployee>, List<MediaEmployee>>(dbEntities);
        }

        public string Add(MediaContact entity, int userId)
        {
            var dbEntity = AutoMapper.Mapper.Map<MediaContact, Model.Entities.MediaContact>(entity);
            var repo = new MediaContactRepository(_uow);

            // If the Media Database is locked for exporting then don't allow them to update.
            if (MediaDatabaseHelper.IsMediaDatabaseLocked())
                throw new UpdateException("The Media Database is currently locked for exporting. Please try again later.");

            // Validate everything.
            ValidateContact(dbEntity, null);

            dbEntity.Id = MediaDatabaseHelper.GetNewContactId(); ;
            dbEntity.CreatedDate = DateTime.Now;
            dbEntity.LastModifiedDate = dbEntity.CreatedDate;
            dbEntity.LastModifiedByUserId = userId;

            // A new Contact will not have any Employee records so the status should be Unassigned.
            dbEntity.RowStatus = ((char)ContactRowStatusType.Unassigned).ToString();
            dbEntity.DeletedDate = null;
            if (entity.RowStatus == ContactRowStatusType.Deleted)
                dbEntity.DeletedDate = DateTime.Now;

            if (!string.IsNullOrWhiteSpace(entity.Profile))
                dbEntity.Profile = new Model.Entities.MediaContactProfile { Content = entity.Profile, Updated = dbEntity.LastModifiedDate };

            dbEntity.IsDirty = true;
            repo.Add(dbEntity);
            _uow.Save();

            return dbEntity.Id;
        }

        public void Update(MediaContact entity, int userId)
        {
            var dbEntity = AutoMapper.Mapper.Map<MediaContact, Model.Entities.MediaContact>(entity);
            var contactRepo = new MediaContactRepository(_uow);

            if (MediaDatabaseHelper.IsMediaDatabaseLocked())
                throw new UpdateException("The Media Database is currently locked for exporting. Please try again later.");

            // Pad the ContactId with spaces so comparisons work with values from the database.
            dbEntity.Id = MediaDatabaseHelper.PadOutletId(dbEntity.Id);

            // Set the ContactId of all child nodes to the correct values.
            SetIdsOfChildNodes(dbEntity);

            List<Model.Entities.MediaEmployee> employees =
                new MediaEmployeeRepository(_uow).GetAllByContactId(dbEntity.Id).Where(e => e.RowStatus == Constants.ROWSTATUS_ACTIVE).ToList();

            // Validate everything.
            ValidateContact(dbEntity, employees);

            Model.Entities.MediaContact originalContact = contactRepo.GetById(dbEntity.Id);

            if (entity.RowStatus == ContactRowStatusType.Deleted)
            {
                if (employees.Count > 0)
                    throw new UpdateException("Please remove all Employee records before deleting.");

                /* Validation commented out for now. Just ask them to remove Employees manually.
                 * ValidateEmployeesDoNotBelongToAnyServices(dbEntity.Id, employees);

                // Attempt to delete all MediaEmployees for this MediaContact.
                var employeeBL = new MediaEmployeeBL(_uow);

                foreach (var emp in employees) {
                    emp.RowStatus = ((char)EmployeeRowStatusType.Deleted).ToString();
                    employeeBL.UpdateDBEntity(emp, null, userId);
                }*/
            }
            else
            {
                // If they have MediaEmployee records make sure the status isn't unassigned.
                if (employees.Count > 0)
                    dbEntity.RowStatus = ((char)ContactRowStatusType.Active).ToString();
                else
                    dbEntity.RowStatus = ((char)ContactRowStatusType.Unassigned).ToString();
            }

            dbEntity.CreatedDate = originalContact.CreatedDate;
            dbEntity.LastModifiedByUserId = userId;
            dbEntity.DeletedDate = null;
            if (entity.RowStatus == ContactRowStatusType.Deleted)
                dbEntity.DeletedDate = DateTime.Now;

            if (originalContact.Profile != null)
            {
                dbEntity.Profile = originalContact.Profile;
                dbEntity.Profile.Content = entity.Profile;
                dbEntity.Profile.Updated = dbEntity.LastModifiedDate;
            }
            else
            {
                dbEntity.Profile = new Model.Entities.MediaContactProfile { Content = entity.Profile, Updated = dbEntity.LastModifiedDate };
            }

            dbEntity.IsDirty = true;
            contactRepo.Update(dbEntity);

            _uow.Save();
        }

        #region Private Methods

        //private void ValidateEmployeesDoNotBelongToAnyServices(string id, List<Model.Entities.MediaEmployee> employees)
        //{
        //    // Check to see if this Contact  is used in any Services.
        //    var repo = new ServiceListRepository(_uow);

        //    foreach (var emp in employees)
        //    {
        //        var serviceList = repo.GetAllByDebtorNumberOutletIdContactId(Constants.DEBTOR_NUMBER_AAP, emp.OutletId, id, ((char)SystemType.Medianet).ToString());

        //        if (serviceList.Count > 0)
        //        {
        //            throw new UpdateException(string.Format("This Contact cannot be deleted from outlet '{0}' as it is used in the following Services:",
        //                emp.MediaOutlet.Name.Trim()), serviceList.Select(s => s.SelectionDescription.Trim()).ToList());
        //        }
        //    }
        //}

        private void ValidateContact(Model.Entities.MediaContact dbEntity, List<Model.Entities.MediaEmployee> employees)
        {
            if (employees != null && employees.Count > 0)
            {
                ValidatePreferredDelivery(dbEntity.Facebook, PreferredDeliveryType.Facebook, employees);
                ValidatePreferredDelivery(dbEntity.LinkedIn, PreferredDeliveryType.LinkedIn, employees);
                ValidatePreferredDelivery(dbEntity.Twitter, PreferredDeliveryType.Twitter, employees);
                ValidatePreferredDelivery(dbEntity.Instagram, PreferredDeliveryType.Instagram, employees);
                ValidatePreferredDelivery(dbEntity.Skype, PreferredDeliveryType.Skype, employees);
            }

            // Validate that all the outlet Ids exist
            if (dbEntity.JobHistory != null && dbEntity.JobHistory.Count > 0)
            {
                var outletIds = dbEntity.JobHistory.Where(j => !string.IsNullOrWhiteSpace(j.OutletId)).Select(j => j.OutletId).ToList();

                if (outletIds.Count > 0)
                {
                    var outletRepo = new MediaOutletRepository(_uow);
                    var dbOutletIds = outletRepo.GetOutletIdsByIds(outletIds.ToArray());

                    foreach (var id in outletIds)
                    {
                        if (!dbOutletIds.Contains(id))
                            throw new UpdateException("Outlet Id '" + id.Trim() + "' is not found in the database.");
                    }
                }
            }
        }

        private void ValidatePreferredDelivery(string contactAddress, PreferredDeliveryType preferredMethod, List<Model.Entities.MediaEmployee> employees)
        {
            if (!string.IsNullOrWhiteSpace(contactAddress) && !contactAddress.Equals("NA", StringComparison.CurrentCultureIgnoreCase))
                return;

            if (employees.Exists(e => e.PreferredDeliveryMethod == ((char)preferredMethod).ToString() && e.RowStatus == Constants.ROWSTATUS_ACTIVE))
                throw new UpdateException("One or more Employee records require " + preferredMethod.ToString() + " which was not supplied.");
        }

        private void SetIdsOfChildNodes(Model.Entities.MediaContact dbEntity)
        {
            // Add ContactId to any job histories
            if (dbEntity.JobHistory != null)
            {
                foreach (var item in dbEntity.JobHistory)
                {
                    item.ContactId = dbEntity.Id;
                    item.OutletId = string.IsNullOrWhiteSpace(item.OutletId) ? null : MediaDatabaseHelper.PadOutletId(item.OutletId);
                }
            }
        }

        #endregion
    }
}
