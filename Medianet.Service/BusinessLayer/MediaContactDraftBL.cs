﻿using System;
using System.Collections.Generic;
using System.Linq;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Service.Dto;
using System.Transactions;
using Medianet.Service.Common;
using Medianet.DataLayer.Common;

namespace Medianet.Service.BusinessLayer
{
    public class MediaContactDraftBL
    {
        private UnitOfWork _uow;

        public MediaContactDraftBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public MediaContactDraft GetByContactId(string contactId)
        {
            Model.Entities.MediaContactDraft dbEntity = new MediaContactDraftRepository(_uow).GetByContactId(contactId);

            // Map the database object to a friendly representation of the data.
            MediaContactDraft retval = AutoMapper.Mapper.Map<Model.Entities.MediaContactDraft, MediaContactDraft>(dbEntity);

            return retval;
        }

        public MediaContactDraft GetByQueueId(int queueId)
        {
            Model.Entities.MediaContactDraft dbEntity = new MediaContactDraftRepository(_uow).GetByQueueId(queueId);

            // Map the database object to a friendly representation of the data.
            MediaContactDraft retval = AutoMapper.Mapper.Map<Model.Entities.MediaContactDraft, MediaContactDraft>(dbEntity);

            return retval;
        }

        public void ApproveOrDecline(int draftId, MediaDraftQueue draftQueue, DraftQueueStatus status, MediaContact contact, int userId)
        {
            using (TransactionScope transaction = new TransactionScope())
            {
                if (status == DraftQueueStatus.Approved)
                    new MediaContactBL(_uow).Update(contact, userId);
                
                var dbEntity = AutoMapper.Mapper.Map<MediaDraftQueue, Model.Entities.MediaDraftQueue>(draftQueue);
                dbEntity.LastModifiedBy = userId;

                new MediaDraftQueueRepository(_uow).Update(dbEntity, status, true);

                _uow.Save();

                if (!draftQueue.SkipSendingEmailToJournalist)
                {
                    User user = new UserBL(_uow).GetUserByContactId(draftQueue.ContactId);

                    if (!string.IsNullOrEmpty(user.EmailAddress))
                        EmailHelper.JournalistUpdatedInfoRequestResult(user.FirstName, draftQueue.Description, user.EmailAddress);
                }

                transaction.Complete();
            }
        }

        public void Save(MediaContactDraft entity, int userId, bool isAdminUser = false)
        {
            var dbEntity = AutoMapper.Mapper.Map<MediaContactDraft, Model.Entities.MediaContactDraft>(entity);

            if (entity.Id <= 0)
                new MediaContactDraftRepository(_uow).Add(dbEntity);
            else
            {
                dbEntity.DraftQueue.LastModifiedBy = userId;

                new MediaDraftQueueRepository(_uow).Update(
                    dbEntity.DraftQueue,
                    entity.IsDeleted ? DraftQueueStatus.Deleted : DraftQueueStatus.Unchanged,
                    isAdminUser);

                new MediaContactDraftRepository(_uow).Update(dbEntity, entity.IsDeleted);
            }

            _uow.Save();
        }
    }
}
