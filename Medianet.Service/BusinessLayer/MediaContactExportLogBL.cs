﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class MediaContactExportLogBL
    {
        private UnitOfWork _uow;

        public MediaContactExportLogBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public int Add(MediaContactExportLog entity)
        {
            var dbEntity = AutoMapper.Mapper.Map<MediaContactExportLog, Model.Entities.MediaContactExportLog>(entity);
            var repo = new MediaContactExportLogRepository(_uow);

            repo.Add(dbEntity);
            _uow.Save();

            return dbEntity.Id;
        }
    }
}
