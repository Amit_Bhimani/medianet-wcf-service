﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class MediaContactGroupBL
    {
        private UnitOfWork _uow;

        public MediaContactGroupBL(UnitOfWork uow) {
            _uow = uow;
        }

        public MediaContactGroup Get(int id) {
            var repo = new MediaContactGroupRepository(_uow);
            Model.Entities.MediaContactGroup dbEntity;

            dbEntity = repo.GetById(id);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.MediaContactGroup, MediaContactGroup>(dbEntity);
        }

        public List<MediaContactGroup> GetAll(int userId)
        {
            var repo = new MediaContactGroupRepository(_uow);
            List<Model.Entities.MediaContactGroup> dbEntities;

            dbEntities = repo.GetAll(userId);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.MediaContactGroup>, List<MediaContactGroup>>(dbEntities);
        }

        public List<MediaContactGroup> GetAllByType(MediaContactGroupType type, int userId) {
            var repo = new MediaContactGroupRepository(_uow);
            List<Model.Entities.MediaContactGroup> dbEntities;

            dbEntities = repo.GetAllByType((byte)type, userId);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.MediaContactGroup>, List<MediaContactGroup>>(dbEntities);
        }

        public int Add(MediaContactGroup entity, int userId) {
            var dbEntity = AutoMapper.Mapper.Map<MediaContactGroup, Model.Entities.MediaContactGroup>(entity);
            var repo = new MediaContactGroupRepository(_uow);

            dbEntity.CreatedDate = DateTime.Now;
            //dbEntity.CreatedByUserId = userId;
            dbEntity.LastModifiedDate = dbEntity.CreatedDate;
            //dbEntity.LastModifiedByUserId = userId;

            repo.Add(dbEntity);

            return dbEntity.Id;
        }

        public void Update(MediaContactGroup entity, int userId) {
            var dbEntity = AutoMapper.Mapper.Map<MediaContactGroup, Model.Entities.MediaContactGroup>(entity);
            var repo = new MediaContactGroupRepository(_uow);

            dbEntity.LastModifiedDate = DateTime.Now;
            //dbEntity.LastModifiedByUserId = userId;

            repo.Update(dbEntity);
        }

        public void Delete(int id, int userId) {
            throw new NotImplementedException();
        }
    }
}
