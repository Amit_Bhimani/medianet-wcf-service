﻿using System;
using System.Collections.Generic;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Service.Dto;
using Medianet.Service.Common;
using Medianet.Service.BusinessLayer.Common;
using Medianet.Service.BusinessLayer.Exceptions;

namespace Medianet.Service.BusinessLayer
{
    public class MediaContactJobHistoryBL
    {
        private UnitOfWork _uow;

        public MediaContactJobHistoryBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public List<MediaContactJobHistory> GetAll(string contactId)
        {
            MediaContactJobHistoryRepository repo = new MediaContactJobHistoryRepository(_uow);

            List<Model.Entities.MediaContactJobHistory> dbEntities = repo.GetByContactId(contactId);

            // Map the database object to a friendly representation of the data.
            List<MediaContactJobHistory> retval = AutoMapper.Mapper.Map<List<Model.Entities.MediaContactJobHistory>, List<MediaContactJobHistory>>(dbEntities);
            
            return retval;
        }

        public MediaContactJobHistory GetById(int jobHistoryId)
        {
            MediaContactJobHistoryRepository repo = new MediaContactJobHistoryRepository(_uow);

            Model.Entities.MediaContactJobHistory dbEntity = repo.GetById(jobHistoryId);

            // Map the database object to a friendly representation of the data.
            MediaContactJobHistory retval = AutoMapper.Mapper.Map<Model.Entities.MediaContactJobHistory, MediaContactJobHistory>(dbEntity);

            return retval;
        }

        public int Add(MediaContactJobHistory entity, int userId)
        {
            var dbEntity = AutoMapper.Mapper.Map<MediaContactJobHistory, Model.Entities.MediaContactJobHistory>(entity);
            var repo = new MediaContactJobHistoryRepository(_uow);

            // If the Media Database is locked for exporting then don't allow them to update.
            if (MediaDatabaseHelper.IsMediaDatabaseLocked())
                throw new UpdateException("The Media Database is currently locked for exporting. Please try again later.");
            
            dbEntity.CreatedDate = DateTime.Now;
            dbEntity.CreatedBy = userId;
            dbEntity.LastModifiedDate = dbEntity.CreatedDate;
            dbEntity.LastModifiedBy = userId;
         
            repo.Add(dbEntity);
            _uow.Save();
            
            return dbEntity.Id;
        }

        public void Update(MediaContactJobHistory entity, int userId)
        {
            var dbEntity = AutoMapper.Mapper.Map<MediaContactJobHistory, Model.Entities.MediaContactJobHistory>(entity);
            var repo = new MediaContactJobHistoryRepository(_uow);

            if (MediaDatabaseHelper.IsMediaDatabaseLocked())
                throw new UpdateException("The Media Database is currently locked for exporting. Please try again later.");
            
            Model.Entities.MediaContactJobHistory originalEntity = repo.GetById(dbEntity.Id);
           
            dbEntity.CreatedDate = originalEntity.CreatedDate;
            dbEntity.CreatedBy = originalEntity.CreatedBy;
            dbEntity.LastModifiedBy = userId;
            dbEntity.LastModifiedDate = DateTime.Now;
           
            _uow.Save();
        }

        public void Delete(int jobHistoryId, int userId)
        {
            new MediaContactJobHistoryRepository(_uow).Delete(jobHistoryId);

            _uow.Save();
        }

        public void UpdateBulk(List<MediaContactJobHistory> entities, string contactId, int userId)
        {
            var dbEntities = AutoMapper.Mapper.Map<List<MediaContactJobHistory>, List<Model.Entities.MediaContactJobHistory>>(entities);
            var repo = new MediaContactJobHistoryRepository(_uow);

            contactId = MediaDatabaseHelper.PadContactId(contactId);
            foreach (var dbEntity in dbEntities)
            {
                if (string.IsNullOrWhiteSpace(dbEntity.OutletId))
                    dbEntity.OutletId = null;
                else
                    dbEntity.OutletId = MediaDatabaseHelper.PadOutletId(dbEntity.OutletId);
            }

            repo.UpdateBulk(dbEntities, contactId, userId);

            _uow.Save();
        }
    }
}
