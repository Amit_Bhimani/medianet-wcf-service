﻿using System;
using System.Collections.Generic;
using System.Linq;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class MediaContactListBL
    {
        private UnitOfWork _uow;

        public MediaContactListBL(UnitOfWork uow) {
            _uow = uow;
        }

        public MediaContactList Get(int id) {
            var repo = new MediaContactListRepository(_uow);
            Model.Entities.MediaContactList dbEntity;

            dbEntity = repo.GetById(id);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.MediaContactList, MediaContactList>(dbEntity);
        }

        public List<MediaContactList> GetAll(int userId)
        {
            var repo = new MediaContactListRepository(_uow);
            List<Model.Entities.MediaContactList> dbEntities;

            dbEntities = repo.GetAll(userId);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.MediaContactList>, List<MediaContactList>>(dbEntities);
        }

        public List<MediaContactList> GetRecent(int userId, int recentCount)
        {
            var repo = new MediaContactListRepository(_uow);
            List<Model.Entities.MediaContactList> dbEntities;

            dbEntities = repo.GetRecent(userId, recentCount);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.MediaContactList>, List<MediaContactList>>(dbEntities);
        }

        public int Add(MediaContactList entity, int userId)
        {
            var dbEntity = AutoMapper.Mapper.Map<MediaContactList, Model.Entities.MediaContactList>(entity);
            var repo = new MediaContactListRepository(_uow);

            dbEntity.CreatedDate = DateTime.Now;
            //dbEntity.CreatedByUserId = userId;
            dbEntity.LastModifiedDate = dbEntity.CreatedDate;
            //dbEntity.LastModifiedByUserId = userId;

            repo.Add(dbEntity);

            return dbEntity.Id;
        }

        public void Update(MediaContactList entity, int userId)
        {
            var dbEntity = AutoMapper.Mapper.Map<MediaContactList, Model.Entities.MediaContactList>(entity);
            var repo = new MediaContactListRepository(_uow);

            dbEntity.LastModifiedDate = DateTime.Now;
            //dbEntity.LastModifiedByUserId = userId;

            repo.Update(dbEntity);
        }

        public void Delete(int id, int userId) {
            throw new NotImplementedException();
        }
    }
}
