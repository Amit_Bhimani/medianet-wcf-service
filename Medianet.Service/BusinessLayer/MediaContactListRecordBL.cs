﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.DataLayer;
using Medianet.Service.Common;
using Medianet.Service.Dto;
using Medianet.DataLayer.Repositories;

namespace Medianet.Service.BusinessLayer
{
    public class MediaContactListRecordBL
    {
        private UnitOfWork _uow;

        private const int DEFAULT_MAX_ROWS = 5000;

        public MediaContactListRecordBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public MediaContactPaginatedListRecordDetails GetPaginatedRecordsByListsetAndListId(int listId, int listSet, int pageNumber,
                                                                                          int recordsPerPage,
                                                                                          SortColumn sortColumn,
                                                                                          SortDirection pSortDirection,
                                                                                          int userId)
        {
            var repo = new MediaContactListRecordRepository(_uow);
            var recordlist = repo.GetByListIdAndListSet(listId, listSet,userId, DEFAULT_MAX_ROWS);
            List<MediaContactListRecordDetails> mappedlist = AutoMapper.Mapper.Map<List<Model.Entities.MediaContactListRecordDetails>, List<MediaContactListRecordDetails>>(recordlist);
            var myUniqueListEntries = new List<string>();
            var uniquelist = new List<MediaContactListRecordDetails>();

            foreach (var aRecord in mappedlist)
            {
                var uniqueentry = aRecord.OutletId + "#" + aRecord.ContactId + "#" + aRecord.RecordType;

                // If there's a contact that was deleted and 'automatically' replaced by PrimaryNewsContact then we only display PrimaryNewsContact once,
                // so [e.g.] when you export list there's no duplicates.
                if (myUniqueListEntries.Contains(uniqueentry))
                {
                    var myRecord = uniquelist.FirstOrDefault(r => ((r.OutletId == aRecord.OutletId) && (r.ContactId == aRecord.ContactId)));
                    bool atLeastOneIsNotDeleted = ((myRecord.IsEntityDeleted == false) || (aRecord.IsEntityDeleted == false));

                    // If one of them isn't deleted then make the one we show not deleted.
                    myRecord.IsEntityDeleted = !atLeastOneIsNotDeleted;

                    // If the primary contact was in the list already and we have a contact that is supposed to be replaced by this, 
                    // in the list we need to show only one contact and it should be the primary contact for the outlet
                    if (!(aRecord.IsEntityDeleted))
                        myRecord.RecordId = aRecord.RecordId;
                }
                else
                {
                    myUniqueListEntries.Add(uniqueentry);
                    uniquelist.Add(aRecord);
                }
            }

            var lst = new MediaContactPaginatedListRecordDetails
                {
                    ListRecord = uniquelist,
                    TotalRecordsCount = uniquelist.Count
                };
            Func<MediaContactListRecordDetails, Object> orderByFunc;

            switch (sortColumn)
            {
                case SortColumn.OutletName:
                    orderByFunc = item => item.OutletName;
                    break;
                case SortColumn.ContactName:
                    orderByFunc = item => item.ContactDisplay;
                    break;
                case SortColumn.JobTitle:
                    orderByFunc = item => item.JobTitle;
                    break;
                case SortColumn.Subject:
                    orderByFunc = item => item.SubjectDisplay;
                    break;
                case SortColumn.MediaType:
                    orderByFunc = item => item.ProductTypeName;
                    break;
                case SortColumn.ContactInfluencerScore:
                    orderByFunc = item => item.MediaInfluencerScore;
                    pSortDirection = pSortDirection == SortDirection.Asc ? SortDirection.Desc : SortDirection.Asc;
                    break;
                case SortColumn.Location:
                    orderByFunc = item => item.CityName;
                    break;
                default:
                    orderByFunc = item => item.RecordId;
                    break;
            }

            if (pSortDirection == SortDirection.Asc)
                lst.ListRecord =
                    lst.ListRecord.OrderBy(orderByFunc)
                       .Skip((pageNumber - 1) * recordsPerPage)
                       .Take(recordsPerPage)
                       .ToList();
            else
                lst.ListRecord =
                    lst.ListRecord.OrderByDescending(orderByFunc)
                       .Skip((pageNumber - 1) * recordsPerPage)
                       .Take(recordsPerPage)
                       .ToList();

            return lst;
        }

        public void DeleteFromList(int listId, List<int> recordIds, int userId)
        {
            var batches = new List<string>();
            var swapSpc = new StringBuilder("");

            recordIds.ForEach(delegate(int record)
            {
                if (swapSpc.Length + record.ToString().Length + ", ".Length < 8000)
                {
                    swapSpc.Append(record.ToString() + ", ");
                }
                else
                {
                    batches.Add(swapSpc.ToString());
                    swapSpc = new StringBuilder(record.ToString() + ", ");
                }
            });

            if (!batches.Contains(swapSpc.ToString())) batches.Add(swapSpc.ToString());

            var repo = new MediaContactListRecordRepository(_uow);
            int listsetId = 0;

            //_uow.BeginTransaction();

            foreach (string batch in batches)
            {
                listsetId = repo.AddAndDelete(listId, listsetId, string.Empty, batch, true, userId);
            }

            //_uow.CommitTransaction();
        }

        public void AddToList(int listId, int userId, List<string> recordIds, bool createNewVersion)
        {
            List<string> batches = new List<string>();
            StringBuilder swapSpc = new StringBuilder("");

            recordIds.ForEach(delegate(string record)
            {
                string s = record;
                if (swapSpc.Length + s.Length + ",".Length < 8000)
                {
                    swapSpc.Append(s + ",");
                }
                else
                {
                    batches.Add(swapSpc.ToString());
                    swapSpc = new StringBuilder(s + ",");
                }
            });

            if (!batches.Contains(swapSpc.ToString())) batches.Add(swapSpc.ToString());

            var repo = new MediaContactListRecordRepository(_uow);
            int listsetId = 0;

            //_uow.BeginTransaction();

            foreach (string batch in batches)
            {
                var tmpListsetId = repo.AddAndDelete(listId, listsetId, batch, string.Empty, createNewVersion, userId);

                // The call above can return 0 if all items inserted are already in the list
                if (tmpListsetId > 0) listsetId = tmpListsetId;
            }

            //_uow.CommitTransaction();
        }
    }
}
