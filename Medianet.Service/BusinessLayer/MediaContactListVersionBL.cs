﻿using System;
using System.Collections.Generic;
using System.Linq;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class MediaContactListVersionBL
    {
        private UnitOfWork _uow;

        public MediaContactListVersionBL(UnitOfWork uow) {
            _uow = uow;
        }

        public List<MediaContactListVersion> GetAllByListId(int listId)
        {
            var repo = new MediaContactListVersionRepository(_uow);
            List<Model.Entities.MediaContactListVersion> dbEntity;

            dbEntity = repo.GetAllByListId(listId);

            // Insert the current version in the list
            var listRepo = new MediaContactListRepository(_uow);
            var list = listRepo.GetById(listId);
            if (list != null)
                dbEntity.Insert(0, new Model.Entities.MediaContactListVersion { Id = 0, ListId = listId, Version = 0, Name = list.Name, CreatedDate = list.LastModifiedDate});

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.MediaContactListVersion>, List<MediaContactListVersion>>(dbEntity);
        }

        public void Restore(int listId, int listSetId, int userId)
        {
            var repo = new MediaContactListVersionRepository(_uow);
            repo.Restore(listId, listSetId, userId);
        }
    }
}
