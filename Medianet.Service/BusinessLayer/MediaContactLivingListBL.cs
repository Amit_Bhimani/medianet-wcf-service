﻿using System;
using System.Collections.Generic;
using System.Linq;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Service.Dto;
using Medianet.BusinessLayer.Common;

namespace Medianet.Service.BusinessLayer
{
    public class MediaContactLivingListBL
    {
        private UnitOfWork _uow;

        public MediaContactLivingListBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public List<MediaContactLivingListContactReplacement> GetContactReplacements(int userId = 0, int listId = 0, int maxRows = 6)
        {
            var repo = new MediaContactLivingListRepository(_uow);
            List<Model.Entities.MediaContactLivingListContactReplacement> dbEntities = repo.GetReplacements(userId, listId, maxRows);

            var dtoEntities = new List<MediaContactLivingListContactReplacement>();

            foreach (var item in dbEntities)
            {
                MediaContactLivingListContactReplacement entity = new MediaContactLivingListContactReplacement
                {
                    RecordId = item.RId,
                    ListId = item.ListId,
                    ListName = item.ListName,
                    MediaContactId = item.ContactIdAap.Trim(),
                    PrnContactId = item.ContactIdPrn.Trim(),
                    OmaContactId = item.ContactIdOma.Trim(),
                    ContactReplacementId = item.ContactReplacementId
                };

                if (!string.IsNullOrEmpty(item.ContactIdAap.Trim()))
                {
                    //@ We have AAP Contact deleted
                    entity.ContactDeletedName = item.ContactNameAap.ToString().Trim();
                    entity.OutletDeletedName = item.OutletNameAap.ToString().Trim();
                    entity.ContactReplacementName = item.ContactReplacementName.ToString().Trim();
                    entity.OutletDeletedId = item.OutletIdAap.ToString().Trim();
                    entity.RecordType = (int)RecordType.MediaContact;
                }
                else if (!string.IsNullOrEmpty(item.ContactIdPrn.Trim()))
                {
                    //@ We have PRN Contact or Outlet deleted
                    entity.ContactDeletedName = string.IsNullOrEmpty(item.ContactNamePrn.Trim()) ? "Unknown" : item.ContactNamePrn.ToString().Trim();
                    entity.OutletDeletedName = item.OutletNamePrn.ToString().Trim();
                    entity.ContactReplacementName = item.ContactReplacementName.ToString().Trim();
                    entity.OutletDeletedId = item.OutletIdPrn.ToString().Trim();
                    entity.RecordType = (int)RecordType.PrnContact;

                }
                else if (!string.IsNullOrEmpty(item.ContactIdOma.Trim()))
                {
                    //@ We have OMA Contact deleted, it may have OMA, AAP or PRN outlet so we need to be careful here
                    entity.ContactDeletedName = item.ContactNameOma.ToString().Trim();

                    if (!string.IsNullOrEmpty(item.OutletIdAap.Trim()))
                    {
                        entity.OutletDeletedName = item.OutletNameAap.ToString().Trim();
                        entity.ContactReplacementName = item.ContactReplacementName.ToString().Trim();
                        entity.OutletDeletedId = item.OutletIdAap.ToString().Trim();
                        entity.RecordType = (int)RecordType.OmaContactAtMediaOutlet;
                    }
                    else if (!string.IsNullOrEmpty(item.OutletIdPrn.Trim()))
                    {
                        entity.OutletDeletedName = item.OutletNamePrn.ToString().Trim();
                        entity.ContactReplacementName = item.ContactReplacementName.ToString().Trim();
                        entity.OutletDeletedId = item.OutletIdPrn.ToString().Trim();
                        entity.RecordType = (int)RecordType.OmaContactAtPrnOutlet;
                    }
                    else /*if (!string.IsNullOrWhiteSpace(item.OutletIdOma)*/
                    {
                        entity.OutletDeletedName = item.OutletNameOma.ToString().Trim();
                        entity.ContactReplacementName = string.Empty;
                        entity.OutletDeletedId = item.OutletIdOma.ToString().Trim();

                        if (!string.IsNullOrWhiteSpace(item.OutletIdOma))
                            entity.RecordType = (int)RecordType.OmaContactAtOmaOutlet;
                        else
                            entity.RecordType = (int)RecordType.OmaContactNoOutlet;
                    }
                }
                else if (!string.IsNullOrEmpty(item.OutletIdAap.Trim()))
                {
                    //@ We have AAP Outlet deleted
                    entity.ContactDeletedName = string.Empty;
                    entity.OutletDeletedName = item.OutletNameAap.ToString().Trim();
                    entity.ContactReplacementName = string.Empty;
                    entity.OutletDeletedId = item.OutletIdAap.ToString().Trim();

                    entity.RecordType = (int)RecordType.MediaOutlet;
                }
                else if (!string.IsNullOrEmpty(item.OutletIdOma.Trim()))
                {
                    //@ We have OMA Outlet deleted
                    entity.ContactDeletedName = string.Empty;
                    entity.OutletDeletedName = item.OutletNameOma.ToString().Trim();
                    entity.ContactReplacementName = string.Empty;
                    entity.OutletDeletedId = item.OutletIdOma.ToString().Trim();
                    entity.RecordType = (int)RecordType.OmaOutlet;
                }
                else if (!string.IsNullOrEmpty(item.OutletIdPrn.Trim()))
                {
                    //@ We have PRN Outlet deleted
                    entity.ContactDeletedName = string.Empty;
                    entity.OutletDeletedName = item.OutletNamePrn.ToString().Trim();
                    entity.ContactReplacementName = string.Empty;
                    entity.OutletDeletedId = item.OutletIdPrn.ToString().Trim();
                    entity.RecordType = (int)RecordType.PrnOutlet;
                }

                dtoEntities.Add(entity);
            }

            return dtoEntities;
        }

        public void AcceptReplacement(int recordId, int userId)
        {
            var repo = new MediaContactLivingListRepository(_uow);
            repo.AcceptReplacement(recordId, userId);
        }
        
        public void RejectReplacement(int recordId, int userId)
        {
            var repo = new MediaContactLivingListRepository(_uow);
            repo.RejectReplacement(recordId, userId);
        }
        public void AcceptRejectAll(int listId, int userId)
        {
            var repo = new MediaContactLivingListRepository(_uow);

            //last param MaxRow = 0 means get all records.
            var list = GetContactReplacements(userId, listId, 0);
            var listSetId = 0;
           
            //We don't want to create new list version for every
            //accept/reject, because it would take a while to roll back if we decide we don't want it.
            //The first time we call the sp we pass 0, and return the Id of the new mn_list_version we create.
            //Then for all the other items we accept we pass the same Id so it appends the changes to the same list version

            for (int i = 0; i < list.Count; i++)
            {
                if (i == 0)
                {
                    listSetId = string.IsNullOrWhiteSpace(list[i].ContactReplacementName) ?
                        repo.RejectReplacement(list[i].RecordId, userId, 0) : repo.AcceptReplacement(list[i].RecordId, userId, 0);
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(list[i].ContactReplacementName))
                        repo.RejectReplacement(list[i].RecordId, userId, listSetId);
                    else
                        repo.AcceptReplacement(list[i].RecordId, userId, listSetId);
                }
            }
        }

        public Dictionary<string, string> GetOutletContactsByRecordId(int recordId, int maxRow)
        {
            List<Model.Entities.MediaContactLivingListContacts> dbEntities = null;
            var repo = new MediaContactLivingListRepository(_uow);
            dbEntities = repo.GetOutletContactsByRecordId(recordId, maxRow);
            var d = new Dictionary<string, string>();

            if (dbEntities != null)
            {
                foreach (var item in dbEntities)
                {
                    d.Add(item.ContactId.ToString(), item.ContactName.ToString() + (string.IsNullOrEmpty(item.JobTitle) ? "" : " - " + item.JobTitle.Trim()));
                }
            }

            return d;
        }

        public void ReplaceContact(int recordId, string newContactId, int userId)
        {
            var repo = new MediaContactLivingListRepository(_uow);
            repo.ReplaceContact(recordId, newContactId, userId);
        }
    }
}
