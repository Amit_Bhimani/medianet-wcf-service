﻿using System;
using System.Collections.Generic;
using System.Linq;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class MediaContactProfileBL
    {
        private UnitOfWork _uow;

        public MediaContactProfileBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public MediaContactProfile GetByContactId(string contactId)
        {
            var repo = new MediaContactProfileRepository(_uow);

            Model.Entities.MediaContactProfile dbEntity = repo.GetByContactId(contactId);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.MediaContactProfile, MediaContactProfile>(dbEntity);
        }
    }
}
