﻿using Medianet.Service.Dto;
using System.Collections.Generic;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;

namespace Medianet.Service.BusinessLayer
{
    class MediaContactRecentChangesBL
    {

        private UnitOfWork _uow;

        public MediaContactRecentChangesBL(UnitOfWork uow)
        {
            _uow = uow;
        }
        public List<MediaContactRecentChange> GetMediaContactRecentlyAdded(int total)
        {
            var repo = new MediaContactRecentChangeRepository(_uow);

            var dbEntities = repo.GetRecentlyAdded(total);

            return AutoMapper.Mapper.Map<List<Model.Entities.MediaContactRecentAdd>, List<MediaContactRecentChange>>(dbEntities);
        }
        public List<MediaContactRecentChange> GetMediaContactRecentlyUpdated(int total)
        {
            var repo = new MediaContactRecentChangeRepository(_uow);

            var dbEntities = repo.GetRecentlyUpdated(total);

            return AutoMapper.Mapper.Map<List<Model.Entities.MediaContactRecentUpdate>, List<MediaContactRecentChange>>(dbEntities);
        }
    }
}
