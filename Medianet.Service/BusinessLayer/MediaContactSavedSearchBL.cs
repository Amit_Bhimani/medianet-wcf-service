﻿using System;
using System.Collections.Generic;
using System.Linq;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class MediaContactSavedSearchBL
    {
        private UnitOfWork _uow;

        public MediaContactSavedSearchBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public MediaContactSavedSearch Get(int id) {
            var repo = new MediaContactSavedSearchRepository(_uow);
            Model.Entities.MediaContactSavedSearch dbEntity;

            dbEntity = repo.GetById(id);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.MediaContactSavedSearch, MediaContactSavedSearch>(dbEntity);
        }

        public List<MediaContactSavedSearch> GetAll(int userId)
        {
            var repo = new MediaContactSavedSearchRepository(_uow);
            List<Model.Entities.MediaContactSavedSearch> dbEntities;

            dbEntities = repo.GetAll(userId);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.MediaContactSavedSearch>, List<MediaContactSavedSearch>>(dbEntities);
        }

        public List<MediaContactSavedSearch> GetRecent(int userId, int takeCount)
        {
            var repo = new MediaContactSavedSearchRepository(_uow);
            List<Model.Entities.MediaContactSavedSearch> dbEntities;

            dbEntities = repo.GetRecent(userId, takeCount);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.MediaContactSavedSearch>, List<MediaContactSavedSearch>>(dbEntities);
        }

        public int Add(MediaContactSavedSearch entity, int userId)
        {
            var dbEntity = AutoMapper.Mapper.Map<MediaContactSavedSearch, Model.Entities.MediaContactSavedSearch>(entity);
            var repo = new MediaContactSavedSearchRepository(_uow);

            dbEntity.CreatedDate = DateTime.Now;
            //dbEntity.CreatedByUserId = userId;
            dbEntity.LastModifiedDate = dbEntity.CreatedDate;
            //dbEntity.LastModifiedByUserId = userId;

            repo.Add(dbEntity);

            return dbEntity.Id;
        }

        public void Update(MediaContactSavedSearch entity, int userId)
        {
            var dbEntity = AutoMapper.Mapper.Map<MediaContactSavedSearch, Model.Entities.MediaContactSavedSearch>(entity);
            var repo = new MediaContactSavedSearchRepository(_uow);

            dbEntity.LastModifiedDate = DateTime.Now;
            //dbEntity.LastModifiedByUserId = userId;

            repo.Update(dbEntity);
        }

        public void Delete(int id, int userId) {
            throw new NotImplementedException();
        }
    }
}
