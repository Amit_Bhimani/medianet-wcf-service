﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class MediaContactSearchLogBL
    {
        private UnitOfWork _uow;

        public MediaContactSearchLogBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public int Add(MediaContactSearchLog entity)
        {
            var dbEntity = AutoMapper.Mapper.Map<MediaContactSearchLog, Model.Entities.MediaContactSearchLog>(entity);
            var repo = new MediaContactSearchLogRepository(_uow);

            repo.Add(dbEntity);

            return dbEntity.Id;
        }

        public List<MediaContactSearchLog> GetRecentSearchList(int userId, int maxRecord)
        {
            var repo = new MediaContactSearchLogRepository(_uow);

            List<Model.Entities.MediaContactSearchLog> list = repo.GetRecentSearchList(userId, maxRecord);

            var result = AutoMapper.Mapper.Map<List<Model.Entities.MediaContactSearchLog>, List<MediaContactSearchLog>>(list);

            return result;
        }
        public MediaContactSearchLog GetRecentSearchById(int searchLogId, int userId)
        {
            var repo = new MediaContactSearchLogRepository(_uow);

            Model.Entities.MediaContactSearchLog entity = repo.GetById(searchLogId, userId);

            var result = AutoMapper.Mapper.Map<Model.Entities.MediaContactSearchLog, MediaContactSearchLog>(entity);

            return result;
        }
    }
}
