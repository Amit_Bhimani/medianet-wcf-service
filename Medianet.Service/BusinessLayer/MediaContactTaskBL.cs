﻿using System;
using System.Collections.Generic;
using System.Linq;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class MediaContactTaskBL
    {
        private UnitOfWork _uow;

        public MediaContactTaskBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public MediaContactTask Get(int id) {
            var repo = new MediaContactTaskRepository(_uow);
            Model.Entities.MediaContactTask dbEntity;

            dbEntity = repo.GetById(id);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.MediaContactTask, MediaContactTask>(dbEntity);
        }

        public List<MediaContactTask> GetAll(int userId)
        {
            var repo = new MediaContactTaskRepository(_uow);
            List<Model.Entities.MediaContactTask> dbEntities;

            dbEntities = repo.GetAll(userId);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.MediaContactTask>, List<MediaContactTask>>(dbEntities);
        }

        public int Add(MediaContactTask entity, int userId)
        {
            var dbEntity = AutoMapper.Mapper.Map<MediaContactTask, Model.Entities.MediaContactTask>(entity);
            var repo = new MediaContactTaskRepository(_uow);

            dbEntity.CreatedDate = DateTime.Now;
            dbEntity.CreatedByUserId = userId;
            dbEntity.LastModifiedDate = dbEntity.CreatedDate;
            //dbEntity.LastModifiedByUserId = userId;

            repo.Add(dbEntity);

            return dbEntity.Id;
        }

        public void Update(MediaContactTask entity, int userId)
        {
            var dbEntity = AutoMapper.Mapper.Map<MediaContactTask, Model.Entities.MediaContactTask>(entity);
            var repo = new MediaContactTaskRepository(_uow);

            dbEntity.LastModifiedDate = DateTime.Now;
            //dbEntity.LastModifiedByUserId = userId;

            repo.Update(dbEntity);
        }

        public void Delete(int id, int userId) {
            throw new NotImplementedException();
        }
    }
}
