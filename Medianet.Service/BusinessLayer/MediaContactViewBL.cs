﻿using System.Collections.Generic;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model.Entities;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class MediaContactViewBL
    {
        private readonly UnitOfWork _uow;

        public MediaContactViewBL(UnitOfWork uow)
        {
            _uow = uow;
        }
        public long Add (MediaContactViewDto entity)
        {
            var dbEntity = AutoMapper.Mapper.Map<Dto.MediaContactViewDto, Model.Entities.MediaContactView>(entity);
            var repo = new MediaContactViewRepository(_uow);

            repo.Add(dbEntity);
            _uow.Save();
            return dbEntity.Id;
        }
        public List<MediaContactAlsoViewedDto> GetMediaContactViewSummary(string contactId, string outletId ,List<int> subjectCodeIds, int maxCount)
        {
            var repo = new MediaContactViewRepository(_uow);
            var dbEntities = repo.GetMediaContactViewSummary(contactId, outletId, subjectCodeIds, maxCount);

            var contacts = AutoMapper.Mapper.Map<List<Model.Entities.MediaContactAlsoViewed>, List<MediaContactAlsoViewedDto>>(dbEntities);
            return contacts;
        }
    }
}
