﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class MediaContactsExportBL
    {
        private UnitOfWork _uow;

        public MediaContactsExportBL(UnitOfWork uow) {
            _uow = uow;
        }

        public MediaContactsExport Get(int id) {
            var repo = new MediaContactsExportRepository(_uow);
            Model.Entities.MediaContactsExport dbEntity;

            dbEntity = repo.GetById(id);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.MediaContactsExport, MediaContactsExport>(dbEntity);
        }

        public List<MediaContactsExport> GetAllByDebtorNumberType(string debtorNumber, MediaContactsExportType type) {
            var repo = new MediaContactsExportRepository(_uow);
            List<Model.Entities.MediaContactsExport> dbEntities;

            dbEntities = repo.GetAllByDebtorNumberType(debtorNumber, (int)type);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.MediaContactsExport>, List<MediaContactsExport>>(dbEntities);
        }

        public List<MediaContactsExport> GetAllByDebtorNumberUserIdType(string debtorNumber, int userId, MediaContactsExportType type) {
            return GetAllByDebtorNumberType(debtorNumber, type).Where(e => e.IsPrivate == false || e.UserId == userId).ToList();
        }

        public int Add(MediaContactsExport entity, int userId) {
            var dbEntity = AutoMapper.Mapper.Map<MediaContactsExport, Model.Entities.MediaContactsExport>(entity);
            var repo = new MediaContactsExportRepository(_uow);

            dbEntity.CreatedDate = DateTime.Now;
            dbEntity.CreatedByUserId = userId;
            dbEntity.LastModifiedDate = dbEntity.CreatedDate;
            dbEntity.LastModifiedByUserId = userId;

            repo.Add(dbEntity);

            return dbEntity.Id;
        }

        public void Update(MediaContactsExport entity, int userId) {
            var dbEntity = AutoMapper.Mapper.Map<MediaContactsExport, Model.Entities.MediaContactsExport>(entity);
            var repo = new MediaContactsExportRepository(_uow);

            dbEntity.LastModifiedDate = DateTime.Now;
            dbEntity.LastModifiedByUserId = userId;

            repo.Update(dbEntity);
        }

        public void Delete(int id, int userId) {
            var repo = new MediaContactsExportRepository(_uow);
            var dbEntity = repo.GetById(id);

            if (dbEntity != null) {
                dbEntity.IsDeleted = true;
                dbEntity.LastModifiedDate = DateTime.Now;
                dbEntity.LastModifiedByUserId = userId;

                repo.Update(dbEntity);
            }
        }
    }
}
