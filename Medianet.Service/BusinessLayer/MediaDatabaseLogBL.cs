﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.Service.Common;
using Medianet.Service.BusinessLayer.Common;
using Medianet.Service.BusinessLayer.Exceptions;
using Medianet.Service.Dto;
using MediaDatabaseLog = Medianet.Service.Dto.MediaDatabaseLog;

namespace Medianet.Service.BusinessLayer
{
    public class MediaDatabaseLogBL
    {
        private UnitOfWork _uow;

        public MediaDatabaseLogBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public int Add(MediaDatabaseLog mdbl)
        {
            var dbEntity = AutoMapper.Mapper.Map<MediaDatabaseLog, Model.Entities.MediaDatabaseLog>(mdbl);
            var repo = new MediaDatabaseLogRepository(_uow);

            repo.Add(dbEntity);
            _uow.Save();

            return dbEntity.ItemID;
        }

        public void Update(MediaDatabaseLog mdbl)
        {
            var dbEntity = AutoMapper.Mapper.Map<MediaDatabaseLog, Model.Entities.MediaDatabaseLog>(mdbl);
            var repo = new MediaDatabaseLogRepository(_uow);

            repo.Update(dbEntity);
            _uow.Save();
        }

        public List<MediaDatabaseLog> GetMediaDatabaseLog(string outletId, string contactId)
        {
            var rep = new MediaDatabaseLogRepository(_uow);
            List<Model.Entities.MediaDatabaseLog> dbEntities;

            dbEntities = rep.GetMediaDatabaseLog(outletId,contactId);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.MediaDatabaseLog>,List<MediaDatabaseLog>>(dbEntities);
        }

        public MediaDatabaseLog GetMediaDatabaseLogById(int id)
        {
            var rep = new MediaDatabaseLogRepository(_uow);
            Model.Entities.MediaDatabaseLog dbEntity;

            dbEntity = rep.GetById(id);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.MediaDatabaseLog, MediaDatabaseLog>(dbEntity);
        }

        public List<MediaRecentlyViewedDto> GetMediaRecentlyViewed(int userId)
        {
            var rep = new MediaDatabaseLogRepository(_uow);

            List<MediaRecentlyViewed> dbEntities = rep.GetMediaRecentlyViewed(userId);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<MediaRecentlyViewed>, List<MediaRecentlyViewedDto>>(dbEntities);
        }
    }
}
