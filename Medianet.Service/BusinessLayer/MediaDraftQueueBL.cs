﻿using System;
using System.Collections.Generic;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Service.Dto;
using Medianet.Service.Common;

namespace Medianet.Service.BusinessLayer
{
    public class MediaDraftQueueBL
    {
        private UnitOfWork _uow;

        public MediaDraftQueueBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public List<MediaDraftQueueView> GetAllInQueue(DateTime? startDate, DateTime? endDate, string searchText, 
            int pageNumber, int recordsPerPage, SortColumn sortColumn, SortDirection pSortDirection, out int totalCount)
        {
            MediaDraftQueueRepository repo = new MediaDraftQueueRepository(_uow);

            List<Model.Entities.MediaDraftQueueView> dbEntities = repo.GetItemsInQueue(startDate, endDate, searchText, 
                pageNumber, recordsPerPage, sortColumn.ToString(), pSortDirection == SortDirection.Asc);

            // Map the database object to a friendly representation of the data.
            List<MediaDraftQueueView> retval = AutoMapper.Mapper.Map<List<Model.Entities.MediaDraftQueueView>, List<MediaDraftQueueView>>(dbEntities);

            totalCount = repo.GetItemsInQueueCount(startDate, endDate, searchText);

            return retval;
        }

        public int GetPendingDraftsInQueueCount()
        {
            MediaDraftQueueRepository repo = new MediaDraftQueueRepository(_uow);

            var totalCount = repo.GetPendingDraftsInQueueCount();

            return totalCount;
        }
    }
}
