﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Service.Common;
using Medianet.Service.Dto;
using Medianet.Service.BusinessLayer.Common;
using Medianet.Service.BusinessLayer.Exceptions;
using NLog;

namespace Medianet.Service.BusinessLayer
{
    public class MediaEmployeeBL
    {
        private UnitOfWork _uow;
        private List<Model.Entities.MediaEmployee> _employees = null;
        private Model.Entities.MediaOutlet _outlet = null;

        public MediaEmployeeBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public MediaEmployee Get(string contactId, string outletId)
        {
            var repo = new MediaEmployeeRepository(_uow);
            Model.Entities.MediaEmployee dbEntities;

            dbEntities = repo.GetByKey(outletId, contactId);

            // Map the database object to a friendly representation of the data.
            var result = AutoMapper.Mapper.Map<Model.Entities.MediaEmployee, MediaEmployee>(dbEntities);

            if (result != null)
                result.CurrentlyOutOfOffice = IsOutOfOffice(result.OutOfOfficeStartDate, result.OutOfOfficeReturnDate);

            return result;
        }

        public MediaEmployeeView GetDetails(string contactId, string outletId, int userId)
        {
           Model.Entities.MediaEmployeeView dbEntity = new MediaEmployeeRepository(_uow).GetDetails(outletId, contactId, userId);

            // Map the database object to a friendly representation of the data.
            var result = AutoMapper.Mapper.Map<Model.Entities.MediaEmployeeView, MediaEmployeeView>(dbEntity);

            result.CurrentlyOutOfOffice = IsOutOfOffice(result.OutOfOfficeStartDate, result.OutOfOfficeReturnDate);

            return result;
        }

        public List<MediaOutlet> GetEmployeesOutletsInfo(string contactId)
        {
            var result = new MediaEmployeeRepository(_uow).GetEmployeesOutletsInfo(contactId);

            return AutoMapper.Mapper.Map<List<Model.Entities.MediaOutlet>, List<MediaOutlet>>(result);
        }

        public List<MediaEmployee> GetAllByOutletId(string outletId) {
            var repo = new MediaEmployeeRepository(_uow);
            List<Model.Entities.MediaEmployee> dbEntities;

            dbEntities = repo.GetAllByOutletId(outletId);

            // Map the database object to a friendly representation of the data.
            var results = AutoMapper.Mapper.Map<List<Model.Entities.MediaEmployee>, List<MediaEmployee>>(dbEntities);

            foreach (var employee in results)
            {
                employee.CurrentlyOutOfOffice = IsOutOfOffice(employee.OutOfOfficeStartDate, employee.OutOfOfficeReturnDate);
            }

            return results;
        }

        public void Add(MediaEmployee entity, MediaEmployeeValidation validation, int userId)
        {
            var dbEntity = AutoMapper.Mapper.Map<MediaEmployee, Model.Entities.MediaEmployee>(entity);
            var repo = new MediaEmployeeRepository(_uow);

            // If the Media Database is locked for exporting then don't allow them to update.
            if (MediaDatabaseHelper.IsMediaDatabaseLocked())
                throw new UpdateException("The Media Database is currently locked for exporting. Please try again later.");

            // Pad the Ids with spaces so comparisons work with values from the database.
            dbEntity.OutletId = MediaDatabaseHelper.PadOutletId(dbEntity.OutletId);
            dbEntity.ContactId = MediaDatabaseHelper.PadContactId(dbEntity.ContactId);

            //The City ID in the entity object is a Media Atlas City ID. We need this to be a Medianet City Id.
            SetGeographicDetails(dbEntity);

            // Set the OutletId and ContactId of all child nodes to the correct values.
            SetIdsOfChildNodes(dbEntity);

            // Validate everything not already validated.
            ValidateEmployee(dbEntity, validation, !string.IsNullOrEmpty(entity.EmailServiceIdsCsv));

            dbEntity.SubjectNames = MediaDatabaseHelper.GetSubjectNames(dbEntity.Subjects.Select(s => s.SubjectId).ToList());
            dbEntity.CreatedDate = DateTime.Now;
            dbEntity.LastModifiedDate = dbEntity.CreatedDate;
            dbEntity.LastModifiedByUserId = userId;
            dbEntity.DeletedDate = null;
            if (entity.RowStatus == EmployeeRowStatusType.Deleted)
                dbEntity.DeletedDate = DateTime.Now;

            repo.Add(dbEntity);

            // Now update the Email Services List updated for the employee.
            if (entity.EmailServiceIdsCsv != null)
                UpdateServiceDistributions(entity.OutletId, entity.ContactId, Constants.DEBTOR_NUMBER_AAP, SystemType.Medianet,
                                          DistributionType.Email, entity.EmailServiceIdsCsv, userId);


            repo.SetIsDirtyFlagOnContact(dbEntity.ContactId);

            // Now update all the Employees required by validation.
            UpdateValidationEmployees(dbEntity, validation, userId);

            FixContactStatus(dbEntity);

            _uow.Save();

            try
            {
                // Call the denormalize_aap_contacts stored proc.

                //repo.UpdateContactSnapshot(dbEntity.ContactId);
                repo.UpdateIsEntityDeletedFlag(dbEntity.OutletId, dbEntity.ContactId);
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().LogException(LogLevel.Error, "Exception thrown in UpdateIsEntityDeletedFlag().", ex);
                throw new UpdateException("The Media Database was unable to update snapshot data or check deletion status. Please try again.");
            }
        }

        public void Update(MediaEmployee entity, MediaEmployeeValidation validation, int userId)
        {
            var dbEntity = AutoMapper.Mapper.Map<MediaEmployee, Model.Entities.MediaEmployee>(entity);

            // If the Media Database is locked for exporting then don't allow them to update.
            if (MediaDatabaseHelper.IsMediaDatabaseLocked())
                throw new UpdateException("The Media Database is currently locked for exporting. Please try again later.");

            // Pad the Ids with spaces so comparisons work with values from the database.
            dbEntity.OutletId = MediaDatabaseHelper.PadOutletId(dbEntity.OutletId);
            dbEntity.ContactId = MediaDatabaseHelper.PadContactId(dbEntity.ContactId);

            //The City ID in the entity object is a Media Atlas City ID. We need this to be a Medianet City Id.
            SetGeographicDetails(dbEntity);
            dbEntity.DeletedDate = null;
            if (entity.RowStatus == EmployeeRowStatusType.Deleted)
                dbEntity.DeletedDate = DateTime.Now;
            var employeeRepo = new MediaEmployeeRepository(_uow);
            var originalEmployee = employeeRepo.GetByKey(dbEntity.OutletId, dbEntity.ContactId);
            var employeeStatusHasChanged = originalEmployee.RowStatus != dbEntity.RowStatus;
            

            UpdateDBEntity(dbEntity, validation, !string.IsNullOrEmpty(entity.EmailServiceIdsCsv), userId);
            FixContactStatus(dbEntity);

            // Now update the Email Services List updated for the employee.
            UpdateServiceDistributions(entity.OutletId, entity.ContactId, Constants.DEBTOR_NUMBER_AAP, SystemType.Medianet,
                                          DistributionType.Email, entity.EmailServiceIdsCsv, userId);

            // Update the contact Dirty flag
            employeeRepo.SetIsDirtyFlagOnContact(dbEntity.ContactId);

            _uow.Save();

            try
            {
                // Call the denormalize_aap_contacts stored proc.
                //employeeRepo.UpdateContactSnapshot(dbEntity.ContactId);
                
                if (employeeStatusHasChanged)
                {
                    employeeRepo.UpdateIsEntityDeletedFlag(dbEntity.OutletId, dbEntity.ContactId);
                }
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().LogException(LogLevel.Error, "Exception thrown in UpdateIsEntityDeletedFlag().", ex);
                throw new UpdateException("The Media Database was unable to update snapshot data or check deletion status. Please try again.");
            }
        }

        public void UpdateDBEntity(Model.Entities.MediaEmployee dbEntity, MediaEmployeeValidation validation, bool isInEmailLists, int userId) {
            var employeeRepo = new MediaEmployeeRepository(_uow);

            // Set the OutletId and ContactId of all child nodes to the correct values.
            SetIdsOfChildNodes(dbEntity);

            // Validate everything not already validated.
            ValidateEmployee(dbEntity, validation, isInEmailLists);

            dbEntity.SubjectNames = MediaDatabaseHelper.GetSubjectNames(dbEntity.Subjects.Select(s => s.SubjectId).ToList());
            dbEntity.LastModifiedByUserId = userId;

            employeeRepo.Update(dbEntity);

            // Now update all the Employees required by validation.
            UpdateValidationEmployees(dbEntity, validation, userId);
        }

        #region Private Methods

        private void UpdateValidationEmployees(Model.Entities.MediaEmployee dbEntity, MediaEmployeeValidation validation, int userId)
        {
            if (!NeedsValidating(dbEntity.OutletId))
                return;

            var repo = new MediaEmployeeRepository(_uow);
            var employeesToUpdate = new List<Model.Entities.MediaEmployee>();

            if (validation != null) {
                // Get all employees except the current one. This will be updated later.
                var employees = GetAllEntitiesByOutletId(dbEntity.OutletId).Where(e => e.RowStatus == Constants.ROWSTATUS_ACTIVE && e.ContactId != dbEntity.ContactId).ToList();

                // If told to update the PrimaryNewsContact then do it now.
                if (validation.IsValidated(dbEntity.OutletId, dbEntity.ContactId, dbEntity.IsPrimaryNewsContact, dbEntity.RowStatus)) {
                    employeesToUpdate.AddRange(SetOutletPrimaryContact(employees, dbEntity.OutletId, validation.PrimaryContactId, dbEntity.ContactId, userId));
                }

                if (validation.Subjects != null) {
                    // If told to update the PrimaryContact for any subjects then do it now.
                    foreach (var subjectValidation in validation.Subjects) {
                        var currentSub = dbEntity.Subjects.Where(s => s.SubjectId == subjectValidation.SubjectId).FirstOrDefault();
                        bool isPrimary = false;

                        if (currentSub != null)
                            isPrimary = currentSub.IsPrimaryContact;

                        if (subjectValidation.IsValidated(dbEntity.OutletId, dbEntity.ContactId, isPrimary, dbEntity.RowStatus)) {
                            MediaDatabaseHelper.MergeEmployees(employeesToUpdate, MediaDatabaseHelper.SetSubjectPrimaryContact(employees, dbEntity.OutletId, subjectValidation.PrimaryContactId, subjectValidation.SubjectId, dbEntity.ContactId, userId));
                        }
                    }
                }
            }

            // Now update all records changed.
            foreach (var emp in employeesToUpdate) {
                repo.Update(emp);
            }
        }

        private void ValidateEmployee(Model.Entities.MediaEmployee dbEntity, MediaEmployeeValidation validation, bool isInEmailLists) {
            if (validation == null)
                validation = new MediaEmployeeValidation();

            if (validation.Subjects == null)
                validation.Subjects = new List<MediaEmployeeSubjectValidation>();

            if (dbEntity.RowStatus == Constants.ROWSTATUS_ACTIVE) {
                // If this is the Primary Contact for the Outlet make sure it's the only one. If not then make sure someone else is.
                ValidatePrimaryContact(dbEntity, validation);

                // If the EmailAddress is empty then make sure the empty field is not used in a service.
                if (string.IsNullOrEmpty(dbEntity.EmailAddress) && isInEmailLists)
                    throw new UpdateException("The Email Address for this Employee cannot be blank as it is used in Email Services");
            }
            else {
                ValidateDeletedEmployeeDoesNotBelongToAnyServices(dbEntity.OutletId, dbEntity.ContactId);
                ValidatePrimaryContact(dbEntity, validation);
            }

            ValidateSubjectsHavePrimaryContact(dbEntity, validation);
        }

        private void ValidatePrimaryContact(Model.Entities.MediaEmployee dbEntity, MediaEmployeeValidation validation) {
            if (!NeedsValidating(dbEntity.OutletId))
                return;

            // If we have validated the primary contact already then we don't need to validate again.
            if (validation.IsValidated(dbEntity.OutletId, dbEntity.ContactId, dbEntity.IsPrimaryNewsContact, dbEntity.RowStatus)) {
                return;
            }

            // Get the current PrimaryNewsContact, unless the current one is this MediaEmployee record.
            var dbEntityList = GetAllEntitiesByOutletId(dbEntity.OutletId).Where(e =>
                e.ContactId != dbEntity.ContactId
                && e.IsPrimaryNewsContact
                && e.RowStatus == Constants.ROWSTATUS_ACTIVE).ToList();

            if (dbEntity.IsPrimaryNewsContact && dbEntity.RowStatus == Constants.ROWSTATUS_ACTIVE) {
                if (dbEntityList.Count > 0)
                    throw new MultiplePrimaryContactsException(dbEntity.OutletId, string.Format("There are multiple Primary Contacts for Outlet '{0}'. Only one Contact can be marked as Primary for a given Outlet.",
                        dbEntityList[0].MediaOutlet.Name.Trim()));
            }
            else {
                if (dbEntityList.Count == 0) {
                    var outlet = GetOutletEntityByOutletId(dbEntity.OutletId);

                    throw new OutletPrimaryContactRequiredException(dbEntity.OutletId, string.Format("There is no Primary Contact for Outlet '{0}'. A Contact must be marked as Primary for all Outlets.",
                        outlet.Name));
                }
            }
        }

        private void ValidateDeletedEmployeeDoesNotBelongToAnyServices(string outletId, string contactId) {
            // Check to see if this Employee  is used in any Services.
            var repo = new ServiceListRepository(_uow);
            var serviceList = repo.GetAllByDebtorNumberOutletIdContactId(Constants.DEBTOR_NUMBER_AAP, outletId, contactId, ((char)SystemType.Medianet).ToString());

            if (serviceList.Count > 0) {
                throw new UpdateException("This Employee cannot be deleted as it is used in the following Services:",
                    serviceList.Select(s => s.SelectionDescription.Trim()).ToList());
            }
        }

        private void ValidateSubjectsHavePrimaryContact(Model.Entities.MediaEmployee dbEntity, MediaEmployeeValidation validation) {
            if (!NeedsValidating(dbEntity.OutletId))
                return;

            var outletSubjects = GetOutletEntityByOutletId(dbEntity.OutletId).Subjects.Select(s => s.SubjectId).ToList();
            var employees = GetAllEntitiesByOutletId(dbEntity.OutletId).Where(e => e.ContactId != dbEntity.ContactId && e.RowStatus == Constants.ROWSTATUS_ACTIVE).ToList();
            var otherSubjectsList = (from e in employees select e.Subjects).ToList();
            var otherPrimarySubjects = new List<int>();

            // Generate a list of all subjects that all Employees for this Outlet have (excluding the Employee being updated).
            foreach (var sl in otherSubjectsList) {
                if (sl != null)
                    otherPrimarySubjects.AddRange((from s in sl where s.IsPrimaryContact select s.SubjectId).ToList());
            }

            // Now add the Employee being updated (unless it's being deleted).
            if (dbEntity.RowStatus == Constants.ROWSTATUS_ACTIVE)
                otherPrimarySubjects.AddRange((from s in dbEntity.Subjects where s.IsPrimaryContact select s.SubjectId).ToList());

            // Finally make sure all Subjects in the Outlet have a Primary Contact.
            foreach (var sub in outletSubjects) {
                var subjectValidation = validation.Subjects.Where(s => s.SubjectId == sub).FirstOrDefault();

                if (subjectValidation != null) {
                    // If we have validated the subject primary contact already then we don't need to validate again.
                    var currentSub = dbEntity.Subjects.Where(s => s.SubjectId == subjectValidation.SubjectId).FirstOrDefault();
                    bool isPrimary = false;

                    if (currentSub != null)
                        isPrimary = currentSub.IsPrimaryContact;

                    if (subjectValidation.IsValidated(dbEntity.OutletId, dbEntity.ContactId, isPrimary, dbEntity.RowStatus)) {
                        continue;
                    }
                }

                var primaryList = otherPrimarySubjects.Where(s => s == sub).ToList();

                if (primaryList.Count != 1) {
                    var subjectRepo = new SubjectRepository(_uow);
                    var subjectName = subjectRepo.Get(sub).Name.Trim();

                    if (primaryList.Count == 0)
                        throw new SubjectPrimaryContactRequiredException(dbEntity.OutletId, sub, string.Format("The subject '{0}' does not have a primary Contact. Please choose one.", subjectName));
                    else
                        throw new SubjectPrimaryContactRequiredException(dbEntity.OutletId, sub, string.Format("The subject '{0}' has more than one primary Contact. Please choose the correct one.", subjectName));
                }
            }
        }

        private void SetGeographicDetails(Model.Entities.MediaEmployee dbEntity) {
            var city = MediaDatabaseHelper.GetAAPCity(dbEntity.CityId, _uow);

            dbEntity.CityId = city.Id;
            dbEntity.CountryId = city.MediaAtlasCountryId;
        }

        private void SetIdsOfChildNodes(Model.Entities.MediaEmployee dbEntity) {
            // add outletid to any station formats
            if (dbEntity.Roles != null) {
                foreach (var item in dbEntity.Roles) {
                    item.OutletId = dbEntity.OutletId;
                    item.ContactId = dbEntity.ContactId;
                }
            }

            // add outletid to any subjects
            if (dbEntity.Subjects != null) {
                foreach (var item in dbEntity.Subjects) {
                    item.OutletId = dbEntity.OutletId;
                    item.ContactId = dbEntity.ContactId;
                }
            }
        }

        private List<Model.Entities.MediaEmployee> SetOutletPrimaryContact(List<Model.Entities.MediaEmployee> employees, string outletId, string primaryContactId, string currentContactId, int userId) {
            var employeesToUpdate = new List<Model.Entities.MediaEmployee>();
            bool isPrimary;
            var nowDate = DateTime.Now;

            // Pad the primaryContactId with spaces so comparisons work with values from the database.
            primaryContactId = MediaDatabaseHelper.PadContactId(primaryContactId);

            if (primaryContactId == currentContactId || employees.Exists(e => e.ContactId == primaryContactId)) {
                foreach (var emp in employees) {
                    isPrimary = (emp.ContactId == primaryContactId);

                    if (emp.IsPrimaryNewsContact != isPrimary) {
                        emp.IsPrimaryNewsContact = isPrimary;
                        emp.LastModifiedByUserId = userId;

                        employeesToUpdate.Add(emp);
                    }
                }
            }
            else
                throw new UpdateException(string.Format("Contact with Id '{0}' was not found in the Outlet.", primaryContactId.Trim()));

            return employeesToUpdate;
        }

        private void FixContactStatus(Model.Entities.MediaEmployee employee) {
            var contactRepo = new MediaContactRepository(_uow);
            var employeeRepo = new MediaEmployeeRepository(_uow);
            var contact = contactRepo.GetById(employee.ContactId);

            if (employee.RowStatus == Constants.ROWSTATUS_ACTIVE) {
                // Since the Employee record is Active make sure the Contact is also Active.
                if (contact.RowStatus != Constants.ROWSTATUS_ACTIVE) {
                    contact.RowStatus = Constants.ROWSTATUS_ACTIVE;
                    contact.LastModifiedByUserId = employee.LastModifiedByUserId;

                    contactRepo.Update(contact);
                }
            }
            else {
                // The Employee record is deleted. If the Contact record is deleted then don't do anything.
                // But if the Contact Record is Active then if it has no more Employee records set the status to Unassigned.
                if (contact.RowStatus != ((char)ContactRowStatusType.Deleted).ToString()) {
                    var employees = employeeRepo.GetAllByContactId(employee.ContactId).Where(e => e.RowStatus == Constants.ROWSTATUS_ACTIVE && e.OutletId != employee.OutletId).ToList();

                    if (employees.Count == 0) {
                        if (contact.RowStatus == Constants.ROWSTATUS_ACTIVE) {
                            contact.RowStatus = ((char)ContactRowStatusType.Unassigned).ToString();
                            contact.LastModifiedByUserId = employee.LastModifiedByUserId;

                            contactRepo.Update(contact);
                        }
                    }
                }
            }
        }

        private void UpdateServiceDistributions(string outletId, string contactId, string debtorNumber, SystemType system, 
            DistributionType serviceDistributionType, string serviceIdsCsv, int userId)
        {
            var serviceRepo = new ServiceListRepository(_uow);

            List<int> lstServiceIds = string.IsNullOrEmpty(serviceIdsCsv) ? new List<int>() : serviceIdsCsv.Split(',').Select(int.Parse).ToList();

            List<Model.Entities.ServiceListDistribution> dbEntities = serviceRepo.GetOutletOrEmployeeDistributionByType(outletId, contactId, ((char)serviceDistributionType).ToString(), debtorNumber, ((char)system).ToString());
            var removedEntities = dbEntities.Where(s => !lstServiceIds.Contains(s.ServiceId.Value));

            var logRepo = new ServiceDeletedUserLogRepository(_uow);

            foreach (var servdist in removedEntities)
            {
                serviceRepo.DeleteServiceDistribution(servdist);

                logRepo.Add(servdist.ServiceId.Value, servdist.OutletId, servdist.ContactId, servdist.ChildServiceId, userId);

                if (serviceDistributionType != DistributionType.Wire)
                {
                    serviceRepo.RecalculateServiceAddresses(servdist.ServiceId.GetValueOrDefault(), false);
                }
            }

            foreach (var srvs in lstServiceIds)
            {
                if (dbEntities.Any(s => s.ServiceId == srvs)) continue;
                var dist = new Model.Entities.ServiceListDistribution
                {
                    ServiceId = srvs,
                    OutletId = MediaDatabaseHelper.PadOutletId(outletId),
                    SequenceNumber = serviceRepo.GetNextSequenceNumber(srvs),
                    ChildServiceId = null,
                    ContactId = MediaDatabaseHelper.PadContactId(contactId),
                    ContactName = null,
                    RecipientId = null
                };

                serviceRepo.AddServiceDistribution(dist);

                if (serviceDistributionType != DistributionType.Wire)
                {
                    serviceRepo.RecalculateServiceAddresses(srvs, false);
                }
            }           
        }

        private bool NeedsValidating(string outletId )
        {
            var outlet = GetOutletEntityByOutletId(outletId);

            if (outlet == null) return true;

            // Outlet with SendToOma false or generic outlets do not need validating
            return (outlet.SendToOMA && !(outlet.IsGeneric ?? false));
        }

        private List<Model.Entities.MediaEmployee> GetAllEntitiesByOutletId(string outletId)
        {
            if (_employees == null || (_employees.Count > 0 && !_employees[0].OutletId.Trim().Equals(outletId.Trim())))
            {
                var employeeRepo = new MediaEmployeeRepository(_uow);
                _employees = employeeRepo.GetAllByOutletId(outletId);
            }

            return _employees;
        }

        private Model.Entities.MediaOutlet GetOutletEntityByOutletId(string outletId)
        {
            if (_outlet == null || !_outlet.Id.Trim().Equals(outletId.Trim()))
            {
                var outletRepo = new MediaOutletRepository(_uow);
                _outlet = outletRepo.GetById(outletId);
            }

            return _outlet;
        }

        private bool IsOutOfOffice(DateTime? startDate, DateTime? returnDate)
        {
            // We must have a start date. If not then they aren't out of office
            if (!startDate.HasValue)
                return false;

            DateTime nowDate = DateTime.Now;
            nowDate = new DateTime(nowDate.Year, nowDate.Month, nowDate.Day);

            // If the start date is in the future then they aren't on leave yet
            if (startDate.Value > nowDate)
                return false;

            // If we have a start date but no return date then assume they are still out of office
            if (!returnDate.HasValue)
                return true;

            if (returnDate.Value > nowDate)
                return true;

            return false;
        }

        #endregion
    }
}
