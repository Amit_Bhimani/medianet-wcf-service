﻿using System;
using System.Collections.Generic;
using System.Linq;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Service.Common;
using Medianet.Service.Dto;
using System.Transactions;
using Medianet.DataLayer.Common;

namespace Medianet.Service.BusinessLayer
{
    public class MediaEmployeeDraftBL
    {
        private UnitOfWork _uow;

        public MediaEmployeeDraftBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public MediaEmployeeDraft GetByQueueId(int queueId)
        {
            Model.Entities.MediaEmployeeDraft dbEntity = new MediaEmployeeDraftRepository(_uow).GetByQueueId(queueId);

            // Map the database object to a friendly representation of the data.
            MediaEmployeeDraft retval = AutoMapper.Mapper.Map<Model.Entities.MediaEmployeeDraft, MediaEmployeeDraft>(dbEntity);

            return retval;
        }

        public MediaEmployeeDraft Get(string contactId, string outletId)
        {
            Model.Entities.MediaEmployeeDraft dbEntity = new MediaEmployeeDraftRepository(_uow).Get(contactId, outletId);

            // Map the database object to a friendly representation of the data.
            MediaEmployeeDraft retval = AutoMapper.Mapper.Map<Model.Entities.MediaEmployeeDraft, MediaEmployeeDraft>(dbEntity);

            return retval;
        }

        public void ApproveOrDecline(int draftId, MediaDraftQueue draftQueue, DraftQueueStatus status, MediaEmployee employee, MediaEmployeeValidation validation, int userId)
        {
            using (TransactionScope transaction = new TransactionScope())
            {
                if (status == DraftQueueStatus.Approved)
                    new MediaEmployeeBL(_uow).Update(employee, validation, userId);

                var dbEntity = AutoMapper.Mapper.Map<MediaDraftQueue, Model.Entities.MediaDraftQueue>(draftQueue);
                dbEntity.LastModifiedBy = userId;
                new MediaDraftQueueRepository(_uow).Update(dbEntity, status, true);

                _uow.Save();

                if (!draftQueue.SkipSendingEmailToJournalist)
                {
                    User user = new UserBL(_uow).GetUserByContactId(draftQueue.ContactId);

                    if (!string.IsNullOrEmpty(user.EmailAddress))
                        EmailHelper.JournalistUpdatedInfoRequestResult(user.FirstName, draftQueue.Description, user.EmailAddress);
                }

                transaction.Complete();
            }
        }

        public void Save(MediaEmployeeDraft entity, int userId, bool isAdminUser = false)
        {
            var dbEntity = AutoMapper.Mapper.Map<MediaEmployeeDraft, Model.Entities.MediaEmployeeDraft>(entity);

            if (entity.Id > 0)
            {
                dbEntity.DraftQueue.LastModifiedBy = userId;

                new MediaDraftQueueRepository(_uow).Update(
                    dbEntity.DraftQueue,
                    entity.IsDeleted ? DraftQueueStatus.Deleted : DraftQueueStatus.Unchanged,
                    isAdminUser);

                new MediaEmployeeDraftRepository(_uow).Update(dbEntity, entity.IsDeleted);

            }
            else
                new MediaEmployeeDraftRepository(_uow).Add(dbEntity);

            _uow.Save();
        }
    }
}
