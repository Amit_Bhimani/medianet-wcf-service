﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.DataLayer;
using Medianet.Service.Dto;
using Medianet.DataLayer.Repositories;
using Medianet.Model.Entities;
using Medianet.Service.BusinessLayer.Common;
using Medianet.Service.BusinessLayer.Exceptions;

namespace Medianet.Service.BusinessLayer
{
    public class MediaMovementBL
    {
        private UnitOfWork _uow;

        public MediaMovementBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public MediaMovement Get(int id)
        {
            var repo = new MediaMovementRepository(_uow);
            Model.Entities.MediaContactMovement dbEntity;

            dbEntity = repo.GetById(id);

            return AutoMapper.Mapper.Map<Model.Entities.MediaContactMovement, MediaMovement>(dbEntity);
        }

        public List<MediaMovement> GetAll(bool includeHidden, int pageNumber, int recordsPerPage, out int totalCount, out int totalPinned)
        {
            var repo = new MediaMovementRepository(_uow);
            List<Model.Entities.MediaContactMovement> dbEntities;

            dbEntities = repo.GetAll(includeHidden, pageNumber, recordsPerPage);
            totalCount = repo.GetAllCount(includeHidden);
            totalPinned = repo.GetAllPinned();

            return AutoMapper.Mapper.Map<List<Model.Entities.MediaContactMovement>, List<MediaMovement>>(dbEntities);
        }
        public List<MediaMovement> GetByOutletId(string outletId, int pageNumber, int recordsPerPage)
        {
            var repo = new MediaMovementRepository(_uow);

            var dbEntities = repo.GetByOutletId(outletId, pageNumber, recordsPerPage);

            return AutoMapper.Mapper.Map<List<Model.Entities.MediaContactMovement>, List<MediaMovement>>(dbEntities);
        }
        public List<MediaMovement> GetByContactId(string contactId, int pageNumber, int recordsPerPage)
        {
            var repo = new MediaMovementRepository(_uow);

            var dbEntities = repo.GetByContactId(contactId, pageNumber, recordsPerPage);

            return AutoMapper.Mapper.Map<List<Model.Entities.MediaContactMovement>, List<MediaMovement>>(dbEntities);
        }
        public bool Add(MediaMovement entity, int userId)
        {
            var repo = new MediaMovementRepository(_uow);

            var dbEntity = AutoMapper.Mapper.Map<MediaMovement, Model.Entities.MediaContactMovement>(entity);

            //// Pad the Ids with spaces so comparisons work with values from the database.
            dbEntity.OutletId = MediaDatabaseHelper.PadOutletId(dbEntity.OutletId);
            dbEntity.ContactId = (dbEntity.ContactId == null ? null : MediaDatabaseHelper.PadContactId(dbEntity.ContactId));
            dbEntity.CreatedByUserId = userId;
            dbEntity.LastModifiedByUserId = userId;
            dbEntity.CreatedDate = DateTime.Now;
            dbEntity.LastModifiedDate = DateTime.Now;
            dbEntity.Pinned = entity.Pinned;
            
            repo.Add(dbEntity);

            _uow.Save();

            return true;
        }

        public bool Update(MediaMovement entity, int userId)
        {
            var repo = new MediaMovementRepository(_uow);

            var dbEntity = AutoMapper.Mapper.Map<MediaMovement, Model.Entities.MediaContactMovement>(entity);

            var originalEntity = repo.GetById(entity.Id);
            if (originalEntity != null)
            {

                originalEntity.LastModifiedByUserId = userId;
                originalEntity.LastModifiedDate = DateTime.Now;
                originalEntity.JobTitle = dbEntity.JobTitle;
                originalEntity.Description = dbEntity.Description;
                originalEntity.Visible = dbEntity.Visible;
                originalEntity.Pinned = dbEntity.Pinned;
                repo.Update(originalEntity);
                _uow.Save();

            }
            else
            {
                throw new UpdateException("The Media Database was unable to update the movement. Entry not found in the database.");
            }


            return true;
        }

        public bool Delete(int entityId)
        {
            var repo = new MediaMovementRepository(_uow);
            repo.Delete(entityId);
            _uow.Save();
            return true;
        }
        public void PinUnpin(int id, bool pinned)
        {
            var repo = new MediaMovementRepository(_uow);
            repo.PinUnpin(id, pinned);
        }

    }

}
