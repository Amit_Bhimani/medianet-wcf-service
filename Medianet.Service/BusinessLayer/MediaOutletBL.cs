﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;
using Medianet.Service.BusinessLayer.Exceptions;
using Medianet.Service.BusinessLayer.Common;
using Medianet.BusinessLayer.Common;
using NLog;

namespace Medianet.Service.BusinessLayer
{
    public class MediaOutletBL
    {
        private UnitOfWork _uow;

        public MediaOutletBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public MediaOutlet Get(string id)
        {
            var repo = new MediaOutletRepository(_uow);
            Model.Entities.MediaOutlet dbEntity;
            MediaOutlet retval;

            dbEntity = repo.GetById(id);

            // Map the database object to a friendly representation of the data.
            retval = AutoMapper.Mapper.Map<Model.Entities.MediaOutlet, MediaOutlet>(dbEntity);


            return retval;
        }

        public List<MediaOutletPartial> GetOutletsByIds(string[] ids)
        {
            var repo = new MediaOutletRepository(_uow);
            List<Model.Entities.MediaOutlet> dbEntity;
            List<MediaOutletPartial> retval;

            dbEntity = repo.GetOutletsByIds(ids);

            // Map the database object to a friendly representation of the data.
            retval = AutoMapper.Mapper.Map<List<Model.Entities.MediaOutlet>, List<MediaOutletPartial>>(dbEntity);

            return retval;
        }

        public List<MediaOutletPartial> GetRelatedOutlets(string outletId)
        {
            var repo = new MediaOutletRepository(_uow);
            List<Model.Entities.MediaOutletPartial> dbEntity = repo.GetRelatedOutlets(outletId);

            // Map the database object to a friendly representation of the data.
            List<MediaOutletPartial> retval = AutoMapper.Mapper.Map<List<Model.Entities.MediaOutletPartial>, List<MediaOutletPartial>>(dbEntity);

            return retval;
        }

        public List<MediaOutletPartial> GetSyndicatingToOutlets(string outletId)
        {
            var repo = new MediaOutletRepository(_uow);
            List<Model.Entities.MediaOutletPartial> dbEntity = repo.GetSyndicatingToOutlets(outletId);

            // Map the database object to a friendly representation of the data.
            List<MediaOutletPartial> retval = AutoMapper.Mapper.Map<List<Model.Entities.MediaOutletPartial>, List<MediaOutletPartial>>(dbEntity);

            return retval;
        }

        public List<MediaOutletPartial> GetSyndicatedFromOutlets(string outletId)
        {
            var repo = new MediaOutletRepository(_uow);
            List<Model.Entities.MediaOutletPartial> dbEntity = repo.GetSyndicatedFromOutlets(outletId);

            // Map the database object to a friendly representation of the data.
            List<MediaOutletPartial> retval = AutoMapper.Mapper.Map<List<Model.Entities.MediaOutletPartial>, List<MediaOutletPartial>>(dbEntity);

            return retval;
        }

        public List<MediaOutletPartial> GetMediaOwnerBusinesses(string outletId)
        {
            var repo = new MediaOutletRepository(_uow);
            List<Model.Entities.MediaOutletPartial> dbEntity = repo.GetMediaOwnerBusinesses(outletId);

            // Map the database object to a friendly representation of the data.
            List<MediaOutletPartial> retval = AutoMapper.Mapper.Map<List<Model.Entities.MediaOutletPartial>, List<MediaOutletPartial>>(dbEntity);

            return retval;
        }

        public List<MediaOutlet> GetByName(string namePart)
        {
            var repo = new MediaOutletRepository(_uow);
            List<Model.Entities.MediaOutlet> dbEntity;
            MediaOutletProfileBL profileBL = new MediaOutletProfileBL(_uow);
            List<MediaOutlet> retval;

            dbEntity = repo.GetByNamePartial(namePart);

            // Map the database object to a friendly representation of the data.
            retval = AutoMapper.Mapper.Map<List<Model.Entities.MediaOutlet>, List<MediaOutlet>>(dbEntity);
            return retval;
        }

        public string Add(MediaOutlet entity, int userId)
        {
            var repo = new MediaOutletRepository(_uow);
            var dbEntity = AutoMapper.Mapper.Map<MediaOutlet, Model.Entities.MediaOutlet>(entity);

            // If the Media Database is locked for exporting then don't allow them to update.
            if (MediaDatabaseHelper.IsMediaDatabaseLocked())
                throw new UpdateException("The Media Database is currently locked for exporting. Please try again later.");

            // Generate a new OutletId.
            dbEntity.Id = MediaDatabaseHelper.GetNewOutletId();

            //The CityId in the entity object is a Media Atlas CityId. We need this to be a Medianet CityId.
            SetGeographicDetails(dbEntity);

            // Set the OutletId of all child nodes to the new OutletId.
            SetOutletIdOfChildNodes(dbEntity);

            if (entity.RowStatus == OutletRowStatusType.Active)
            {
                // If another Outlet has the same name in the same City then don't let them save it.
                ValidateCityIsUnique(dbEntity);

                if (dbEntity.Subjects != null && dbEntity.Subjects.Count > 0)
                    throw new UpdateException("Please create an Employee to be the primary Contact for all Subjects before adding to the Outlet.");
            }

            if (string.IsNullOrEmpty(dbEntity.ParentId) || dbEntity.ParentId.Trim() == dbEntity.Id.Trim())
            {
                dbEntity.ParentId = dbEntity.Id;
                dbEntity.HasParent = false;
                dbEntity.OwnershipCompany = dbEntity.Name;
            }
            else
                dbEntity.OwnershipCompany = repo.GetOwnershipById(dbEntity.ParentId, dbEntity.Name);

            //dbEntity.RoleNames = MediaDatabaseHelper.GetRoleNames(dbEntity.Roles.Select(r => r.RoleId).ToList());
            dbEntity.CreatedDate = DateTime.Now;
            dbEntity.LastModifiedDate = dbEntity.CreatedDate;
            dbEntity.LastModifiedByUserId = userId;
            dbEntity.DeletedDate = null;
            if (entity.RowStatus == OutletRowStatusType.Deleted)
                dbEntity.DeletedDate = DateTime.Now;

            if (!string.IsNullOrWhiteSpace(entity.Profile))
                dbEntity.Profile = new Model.Entities.MediaOutletProfile { Content = entity.Profile };

            dbEntity.IsDirty = true;
            repo.Add(dbEntity);

            // Now update the Email Services List updated for the outlet.
            if (entity.EmailServiceIdsCsv != null)
                UpdateServiceDistributions(dbEntity.Id, Constants.DEBTOR_NUMBER_AAP, SystemType.Medianet, DistributionType.Email, entity.EmailServiceIdsCsv, userId);

            _uow.Save();

            //try
            //{
            //    // Call the denormalize_aap_outlet stored proc.
            //   //repo.UpdateSnapshot(dbEntity.Id);
            //}
            //catch (Exception ex)
            //{
            //    throw new UpdateException("The Media Database was unable to update snapshot data. Please try again.");
            //}

            return dbEntity.Id;
        }

        public void Update(MediaOutlet entity, List<MediaEmployeeSubjectValidation> validation, int userId)
        {
            var repo = new MediaOutletRepository(_uow);
            var dbEntity = AutoMapper.Mapper.Map<MediaOutlet, Model.Entities.MediaOutlet>(entity);

            // If the Media Database is locked for exporting then don't allow them to update.
            if (MediaDatabaseHelper.IsMediaDatabaseLocked())
                throw new UpdateException("The Media Database is currently locked for exporting. Please try again later.");

            // Pad the OutletId with spaces so comparisons work with values from the database.
            dbEntity.Id = MediaDatabaseHelper.PadOutletId(dbEntity.Id);

            //The CityId in the entity object is a Media Atlas CityId. We need this to be a Medianet CityId.
            SetGeographicDetails(dbEntity);

            // Set the OutletId of any new child nodes to the current OutletId (just in case).
            SetOutletIdOfChildNodes(dbEntity);

            if (validation == null)
                validation = new List<MediaEmployeeSubjectValidation>();

            if (entity.RowStatus == OutletRowStatusType.Active)
            {
                // If another Outlet has the same name in the same City then don't let them save it.
                ValidateCityIsUnique(dbEntity);
                ValidatePostalAddress(dbEntity);
                // If the EmailAddress is empty then make sure the empty field is not used in a service.
                if (string.IsNullOrEmpty(dbEntity.EmailAddress) && !string.IsNullOrEmpty(entity.EmailServiceIdsCsv))
                    throw new UpdateException("The Email Address for this Outlet cannot be blank as it is used in Email Services");

                // Make sure there is a contact that is a primary contact for each subject here.
                if (dbEntity.Subjects != null && dbEntity.Subjects.Count > 0)
                {
                    ValidateSubjectsHavePrimaryContact(dbEntity, validation);
                }
            }
            else
            {
                ValidateDeletedOutletDoesNotBelongToAnyServices(dbEntity.Id);
                ValidateNoActiveContactsExist(dbEntity.Id);
            }

            Model.Entities.MediaOutlet originalOutlet = repo.GetById(entity.Id);

            if (!string.IsNullOrEmpty(originalOutlet.LogoFileName))
            {
                if (originalOutlet.LogoFileName != entity.LogoFileName)
                {
                    RemoveLogoFile(originalOutlet.LogoFileName);
                }
            }
            if (string.IsNullOrEmpty(dbEntity.ParentId) || dbEntity.ParentId.Trim() == dbEntity.Id.Trim())
            {
                dbEntity.ParentId = dbEntity.Id;
                dbEntity.HasParent = false;
                dbEntity.OwnershipCompany = dbEntity.Name;
            }
            else
                dbEntity.OwnershipCompany = repo.GetOwnershipById(dbEntity.ParentId, dbEntity.Name);

            dbEntity.SubjectNames = MediaDatabaseHelper.GetSubjectNames(dbEntity.Subjects.Select(s => s.SubjectId).ToList());
            //dbEntity.RoleNames = MediaDatabaseHelper.GetRoleNames(dbEntity.Roles.Select(r => r.RoleId).ToList());
            dbEntity.CreatedDate = originalOutlet.CreatedDate;
            dbEntity.LastModifiedByUserId = userId;
            dbEntity.DeletedDate = null;
            if (entity.RowStatus == OutletRowStatusType.Deleted)
                dbEntity.DeletedDate = DateTime.Now;

            if (originalOutlet.Profile != null)
            {
                dbEntity.Profile = originalOutlet.Profile;
                dbEntity.Profile.Content = entity.Profile;
            }
            else
            {
                dbEntity.Profile = new Model.Entities.MediaOutletProfile { OutletId = entity.Id, Content = entity.Profile };
            }

            var RowStatusHasChanged = dbEntity.RowStatus != originalOutlet.RowStatus;
            // Set the IsDirtyFlag to true. ( for denormalizing the outlet in the backend)
            dbEntity.IsDirty = true;
            repo.Update(dbEntity);

            // Now update all the Employees required by validation.
            UpdateValidationEmployees(dbEntity, validation, userId);

            // Now update the Email Services List updated for the outlet.
            //if (entity.ServiceLists.Any(s => s.DistributionType == DistributionType.Email))
            UpdateServiceDistributions(entity.Id, Constants.DEBTOR_NUMBER_AAP, SystemType.Medianet, DistributionType.Email, entity.EmailServiceIdsCsv, userId);

            _uow.Save();

            try
            {
                // Call the denormalize_aap_outlet stored proc.
                //repo.UpdateSnapshot(dbEntity.Id);
                if (RowStatusHasChanged)
                {
                    repo.UpdateIsEntityDeletedFlag(dbEntity.Id);
                }
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().LogException(LogLevel.Error, "Exception thrown in UpdateIsEntityDeletedFlag().", ex);
                throw new UpdateException("The Media Database was unable to check deletion status. Please try again.");
            }
        }

        public void SetOutletLogo(string outletId, string logoFilename, int userId)
        {
            var repo = new MediaOutletRepository(_uow);
            repo.SetOutletLogo(outletId, logoFilename, userId);
        }

        public List<OmaDocument> GetDocuments(string outletId, string contactId, RecordType recordType, int userId, string debtorNumber)
        {
            var repo = new OmaDocumentRepository(_uow);
            List<Model.Entities.OmaDocumentView> dbEntity = repo.GetList(outletId, contactId, recordType, userId, debtorNumber);

            // Map the database object to a friendly representation of the data.
            List<OmaDocument> retval = AutoMapper.Mapper.Map<List<Model.Entities.OmaDocumentView>, List<OmaDocument>>(dbEntity);

            return retval;
        }

        #region Private Methods

        private void UpdateValidationEmployees(Model.Entities.MediaOutlet dbEntity, List<MediaEmployeeSubjectValidation> validation, int userId)
        {
            var repo = new MediaEmployeeRepository(_uow);
            List<Model.Entities.MediaEmployee> employees;
            var employeesToUpdate = new List<Model.Entities.MediaEmployee>();

            // Get all employees for the Outlet.
            employees = repo.GetAllByOutletId(dbEntity.Id).Where(e => e.RowStatus == Constants.ROWSTATUS_ACTIVE).ToList();

            if (validation != null)
            {
                // If told to update the PrimaryContact for any subjects then do it now.
                foreach (var subjectValidation in validation)
                {
                    if (subjectValidation.IsValidated(dbEntity.Id))
                    {
                        MediaDatabaseHelper.MergeEmployees(employeesToUpdate, MediaDatabaseHelper.SetSubjectPrimaryContact(employees, dbEntity.Id, subjectValidation.PrimaryContactId, subjectValidation.SubjectId, string.Empty, userId));
                    }
                }
            }

            // Now update all records changed.
            foreach (var emp in employeesToUpdate)
            {
                repo.Update(emp);
            }
        }

        private void ValidateCityIsUnique(Model.Entities.MediaOutlet dbEntity)
        {
            var repo = new MediaOutletRepository(_uow);
            var dbEntityList = repo.GetByName(dbEntity.Name).Where(o =>
                o.Id != dbEntity.Id
                && o.CityId == dbEntity.CityId
                && o.Name.Equals(dbEntity.Name, StringComparison.CurrentCultureIgnoreCase)
                && o.RowStatus == Constants.ROWSTATUS_ACTIVE).ToList();

            if (dbEntityList.Count > 0)
                throw new UpdateException(string.Format("Outlet with Id '{0}' in the City '{1}' already has the name '{2}'. Please change the name of this Outlet.",
                    dbEntityList[0].Id.Trim(), dbEntityList[0].AAPCity.Name.Trim(), dbEntityList[0].Name.Trim()));
        }
        private void ValidatePostalAddress(Model.Entities.MediaOutlet dbEntity)
        {
            var postalAddress = new Dictionary<string, string>();
            postalAddress.Add("Postal Address Line 1", dbEntity.PostalAddressLine1);
            postalAddress.Add("Postal Country", dbEntity.PostalCountryId.ToString());
            postalAddress.Add("Postal City", dbEntity.PostalCityId.ToString());

            if (postalAddress.Any(p => p.Value != null) || postalAddress.Any(p => p.Value != ""))
            {
                var emptyFields = postalAddress.Where(p => (p.Value == null || p.Value == "")).ToList();
                if (emptyFields.Any() && emptyFields.Count < postalAddress.Count)
                {
                    throw new UpdateException(string.Format("Please provide a value for {0}", string.Join(", ", emptyFields.Select(z => z.Key))));
                }
            }
        }
        private void ValidateSubjectsHavePrimaryContact(Model.Entities.MediaOutlet dbEntity, List<MediaEmployeeSubjectValidation> validation)
        {
            var repo = new MediaEmployeeRepository(_uow);
            var employees = repo.GetAllByOutletId(dbEntity.Id).Where(e => e.RowStatus == Constants.ROWSTATUS_ACTIVE).ToList();
            var subjectsList = (from e in employees select e.Subjects).ToList();
            var employeeSubjects = new List<int>();

            // Generate a list of all subjects that all MediaEmployees have.
            foreach (var sl in subjectsList)
            {
                if (sl != null)
                    employeeSubjects.AddRange((from s in sl where s.IsPrimaryContact select s.SubjectId).ToList());
            }

            // Now make sure each Subject has a primary Contact.
            foreach (var sub in dbEntity.Subjects)
            {
                var subjectValidation = validation.Where(s => s.SubjectId == sub.SubjectId).FirstOrDefault();

                if (subjectValidation != null)
                {
                    // If we have validated the subject primary contact already then we don't need to validate again.
                    if (subjectValidation.IsValidated(dbEntity.Id))
                    {
                        continue;
                    }
                }

                if (!employeeSubjects.Contains(sub.SubjectId))
                {
                    var subjectRepo = new SubjectRepository(_uow);
                    var subjectName = subjectRepo.Get(sub.SubjectId).Name.Trim();

                    throw new SubjectPrimaryContactRequiredException(dbEntity.Id, sub.SubjectId, string.Format("Please choose an Employee to be the primary Contact for Subject '{0}'.", subjectName));
                }
            }
        }

        private void ValidateNoActiveContactsExist(string outletId)
        {
            var repo = new MediaEmployeeRepository(_uow);
            var employees = repo.GetAllByOutletId(outletId).Where(e => e.RowStatus == Constants.ROWSTATUS_ACTIVE).ToList();

            if (employees.Count > 0)
            {
                throw new UpdateException("This Outlet has Contacts asigned to it. Please remove the Contacts before deleting.");
            }
        }

        private void ValidateDeletedOutletDoesNotBelongToAnyServices(string outletId)
        {
            // Check to see if this Outlet  is used in any Services.
            var repo = new ServiceListRepository(_uow);
            var serviceList = repo.GetAllByDebtorNumberOutletId(Constants.DEBTOR_NUMBER_AAP, outletId, ((char)SystemType.Medianet).ToString());

            if (serviceList.Count > 0)
            {
                //var ex = new ExistsInServiceException();
                throw new UpdateException("This Outlet cannot be deleted as it is used in the following Services:",
                    serviceList.Select(s => s.SelectionDescription.Trim()).ToList());
            }
        }

        private void SetOutletIdOfChildNodes(Model.Entities.MediaOutlet dbEntity)
        {
            // add outletid to any new profile
            if (dbEntity.Profile != null)
                dbEntity.Profile.OutletId = dbEntity.Id;

            // add outletid to any station formats
            if (dbEntity.StationFormats != null)
            {
                foreach (var item in dbEntity.StationFormats)
                {
                    item.OutletId = dbEntity.Id;
                }
            }

            // add outletid to any working languages
            if (dbEntity.WorkingLanguages != null)
            {
                foreach (var item in dbEntity.WorkingLanguages)
                {
                    item.OutletId = dbEntity.Id;
                }
            }

            // add outletid to any subjects
            if (dbEntity.Subjects != null)
            {
                foreach (var item in dbEntity.Subjects)
                {
                    item.OutletId = dbEntity.Id;
                }
            }

            // add outletid to related outlets
            if (dbEntity.RelatedOutlets != null)
            {
                foreach (var item in dbEntity.RelatedOutlets)
                {
                    item.OutletId = dbEntity.Id;
                }
            }

            // add outletid to Syndicated outlets
            if (dbEntity.SyndicatedOutlets != null)
            {
                foreach (var item in dbEntity.SyndicatedOutlets)
                {
                    item.OutletId = dbEntity.Id;
                }
            }

        }

        private void SetGeographicDetails(Model.Entities.MediaOutlet dbEntity)
        {
            var city = MediaDatabaseHelper.GetAAPCity(dbEntity.CityId, _uow);
            var cRepo = new CountryRepository(_uow);
            var country = cRepo.GetById(city.MediaAtlasCountryId);

            dbEntity.CityId = city.Id;
            dbEntity.CountryId = city.MediaAtlasCountryId;
            dbEntity.ContinentId = country.MediaAtlasContinentId;

            if (dbEntity.PostalCityId.HasValue)
            {
                var postalCity = MediaDatabaseHelper.GetAAPCity(dbEntity.PostalCityId.Value, _uow);
                var postalCountry = cRepo.GetById(city.MediaAtlasCountryId);

                dbEntity.PostalCityId = postalCity.Id;
                dbEntity.PostalCountryId = postalCity.MediaAtlasCountryId;
                dbEntity.PostalContinentId = postalCountry.MediaAtlasContinentId;
            }
        }

        private void UpdateServiceDistributions(string outletId, string debtorNumber, SystemType system, DistributionType serviceDistributionType, 
            string ServiceIdsCsv, int userId)
        {
            var serviceRepo = new ServiceListRepository(_uow);
            List<int> lstServiceIds = string.IsNullOrEmpty(ServiceIdsCsv) ? new List<int>() : ServiceIdsCsv.Split(',').Select(int.Parse).ToList();

            List<Model.Entities.ServiceListDistribution> dbEntities = serviceRepo.GetOutletOrEmployeeDistributionByType(outletId, null, ((char)serviceDistributionType).ToString(), debtorNumber, ((char)system).ToString());
            var removedEntities = dbEntities.Where(s => !lstServiceIds.Contains(s.ServiceId.Value));

            var logRepo = new ServiceDeletedUserLogRepository(_uow);

            foreach (var servdist in removedEntities)
            {
                serviceRepo.DeleteServiceDistribution(servdist);

                logRepo.Add(servdist.ServiceId.Value, servdist.OutletId, servdist.ContactId, servdist.ChildServiceId, userId);

                if (serviceDistributionType != DistributionType.Wire)
                {
                    serviceRepo.RecalculateServiceAddresses(servdist.ServiceId.GetValueOrDefault(), false);
                }
            }

            foreach (var srvs in lstServiceIds)
            {
                if (dbEntities.Any(s => s.ServiceId == srvs)) continue;
                var dist = new Model.Entities.ServiceListDistribution
                {
                    ServiceId = srvs,
                    OutletId = MediaDatabaseHelper.PadOutletId(outletId),
                    SequenceNumber = serviceRepo.GetNextSequenceNumber(srvs),
                    ChildServiceId = null,
                    ContactId = null,
                    ContactName = null,
                    RecipientId = null
                };

                serviceRepo.AddServiceDistribution(dist);

                if (serviceDistributionType != DistributionType.Wire)
                {
                    serviceRepo.RecalculateServiceAddresses(srvs, false);
                }
            }
        }

        private void RemoveLogoFile(string logoFileName)
        {
            var imagePath = PathHelper.GetContactsLogoEnvironmentPath();
            var firstIndex = logoFileName.IndexOf("\\", StringComparison.Ordinal);
            string subdir = string.Empty;
            if (firstIndex > 0)
            {
                subdir = logoFileName.Substring(0, firstIndex);
            }
            var fullFileName = $"{imagePath}{subdir}\\{logoFileName.Substring(firstIndex + 1)}";

            if (System.IO.File.Exists(fullFileName))
            {
                System.IO.File.Delete(fullFileName);
            }
        }


        #endregion
    }
}
