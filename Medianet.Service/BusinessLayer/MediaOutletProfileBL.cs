﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class MediaOutletProfileBL
    {
        private UnitOfWork _uow;

        public MediaOutletProfileBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public MediaOutletProfile Get(string id)
        {
            var repo = new MediaOutletProfileRepository(_uow);
            Model.Entities.MediaOutletProfile dbEntity;

            dbEntity = repo.GetById(id);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.MediaOutletProfile, MediaOutletProfile>(dbEntity);
        }
    }
}
