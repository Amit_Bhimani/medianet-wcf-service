﻿using System.Collections.Generic;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model.Entities;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class MediaOutletViewBL
    {
        private readonly UnitOfWork _uow;

        public MediaOutletViewBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public long Add(MediaOutletViewDto entity)
        {
            var dbEntity = AutoMapper.Mapper.Map<Dto.MediaOutletViewDto, Model.Entities.MediaOutletView>(entity);
            var repo = new MediaOutletViewRepository(_uow);

            repo.Add(dbEntity);
            _uow.Save();
            return dbEntity.Id;
        }
    }
}
