﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.DataLayer;
using Medianet.Service.Dto;
using Medianet.DataLayer.Repositories;
using Medianet.Service.BusinessLayer.Common;
using Medianet.Service.BusinessLayer.Exceptions;

namespace Medianet.Service.BusinessLayer
{
    public class MediaPriorityNoticeBL
    {
        private UnitOfWork _uow;

        public MediaPriorityNoticeBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public MediaPriorityNotice Get(int id)
        {
            var repo = new MediaPriorityNoticeRepository(_uow);
            Model.Entities.MediaPriorityNotice dbEntity;

            dbEntity = repo.GetById(id);

            return AutoMapper.Mapper.Map<Model.Entities.MediaPriorityNotice, MediaPriorityNotice>(dbEntity);
        }

        public MediaPriorityNotice GetByContactId(string contactId)
        {
            var repo = new MediaPriorityNoticeRepository(_uow);
            Model.Entities.MediaPriorityNotice dbEntity;

            dbEntity = repo.GetByContactId(contactId);

            return AutoMapper.Mapper.Map<Model.Entities.MediaPriorityNotice, MediaPriorityNotice>(dbEntity);
        }

        public MediaPriorityNotice GetByOutletId(string outletId)
        {
            var repo = new MediaPriorityNoticeRepository(_uow);
            Model.Entities.MediaPriorityNotice dbEntity;

            dbEntity = repo.GetByOutletId(outletId);

            return AutoMapper.Mapper.Map<Model.Entities.MediaPriorityNotice, MediaPriorityNotice>(dbEntity);
        }

        public List<MediaPriorityNotice> GetAll(int pageNumber, int recordsPerPage, out int totalCount)
        {
            var repo = new MediaPriorityNoticeRepository(_uow);
            List<Model.Entities.MediaPriorityNotice> dbEntities;

            dbEntities = repo.GetAll(pageNumber, recordsPerPage);
            totalCount = repo.GetAllCount();

            return AutoMapper.Mapper.Map<List<Model.Entities.MediaPriorityNotice>, List<MediaPriorityNotice>>(dbEntities);
        }

        public bool Add(MediaPriorityNotice entity, int userId)
        {
            var repo = new MediaPriorityNoticeRepository(_uow);

            var dbEntity = AutoMapper.Mapper.Map<MediaPriorityNotice, Model.Entities.MediaPriorityNotice>(entity);

            //// Pad the Ids with spaces so comparisons work with values from the database.
            dbEntity.OutletId = (string.IsNullOrWhiteSpace(dbEntity.OutletId) ? null : MediaDatabaseHelper.PadOutletId(dbEntity.OutletId));
            dbEntity.ContactId = (string.IsNullOrWhiteSpace(dbEntity.ContactId) ? null : MediaDatabaseHelper.PadContactId(dbEntity.ContactId));
            dbEntity.CreatedByUserId = userId;
            dbEntity.LastModifiedByUserId = userId;
            dbEntity.CreatedDate = DateTime.Now;
            dbEntity.LastModifiedDate = DateTime.Now;

            repo.Add(dbEntity);

            // Update the IsDirty flag so we can push this to Solr
            if (string.IsNullOrWhiteSpace(dbEntity.OutletId))
                (new MediaContactRepository(_uow)).UpdateIsDirtyFlag(dbEntity.ContactId);
            else
                (new MediaOutletRepository(_uow)).UpdateIsDirtyFlag(dbEntity.OutletId);

            _uow.Save();

            return true;
        }

        public bool Update(MediaPriorityNotice entity, int userId)
        {
            var repo = new MediaPriorityNoticeRepository(_uow);

            var dbEntity = AutoMapper.Mapper.Map<MediaPriorityNotice, Model.Entities.MediaPriorityNotice>(entity);

            var originalEntity = repo.GetById(entity.Id);

            if (originalEntity == null)
                throw new UpdateException("Unable to update the priority notice. Entry not found in the database.");

            //// Pad the Ids with spaces so comparisons work with values from the database.
            originalEntity.OutletId = (string.IsNullOrWhiteSpace(dbEntity.OutletId) ? null : MediaDatabaseHelper.PadOutletId(dbEntity.OutletId));
            originalEntity.ContactId = (string.IsNullOrWhiteSpace(dbEntity.ContactId) ? null : MediaDatabaseHelper.PadContactId(dbEntity.ContactId));
            originalEntity.LastModifiedByUserId = userId;
            originalEntity.LastModifiedDate = DateTime.Now;
            originalEntity.Description = dbEntity.Description;
            repo.Update(originalEntity);

            // Update the IsDirty flag so we can push this to Solr
            if (string.IsNullOrWhiteSpace(dbEntity.OutletId))
                (new MediaContactRepository(_uow)).UpdateIsDirtyFlag(dbEntity.ContactId);
            else
                (new MediaOutletRepository(_uow)).UpdateIsDirtyFlag(dbEntity.OutletId);

            _uow.Save();

            return true;
        }

        public bool Delete(int entityId)
        {
            var repo = new MediaPriorityNoticeRepository(_uow);

            repo.Delete(entityId);
            _uow.Save();

            return true;
        }
    }
}
