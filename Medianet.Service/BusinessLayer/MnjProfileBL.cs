﻿using System;
using System.Collections.Generic;
using System.Linq;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class MnjProfileBL
    {
        private UnitOfWork _uow;

        public MnjProfileBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public List<MnjProfile> GetByUserId(int userId)
        {
            var repo = new MnjProfileRepository(_uow);

            var list = repo.GetByUserId(userId);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.MnjProfile>, List<MnjProfile>>(list);
        }

        public MnjProfile GetById(int profileId)
        {
            var entity = new MnjProfileRepository(_uow).GetById(profileId);

            // Map the database object to a friendly representation of the data.
            var result = AutoMapper.Mapper.Map<Model.Entities.MnjProfile, MnjProfile>(entity);
            
            return result;
        }

        public void DeleteProfile(int profileId, int userId)
        {
            new MnjProfileRepository(_uow).DeleteProfile(profileId, userId);
        }

        public int AddProfile(MnjProfile entity)
        {
            var dbEntity = AutoMapper.Mapper.Map<MnjProfile, Model.Entities.MnjProfile>(entity);

            return new MnjProfileRepository(_uow).AddProfile(dbEntity);
        }

        public void EditProfile(MnjProfile entity)
        {
            var dbEntity = AutoMapper.Mapper.Map<MnjProfile, Model.Entities.MnjProfile>(entity);

            new MnjProfileRepository(_uow).EditProfile(dbEntity);
        }
		
		public void UpdateSubscription(List<MnjProfile> profiles, int userId)
        {
            var repo = new MnjProfileRepository(_uow);

            foreach (var item in profiles)
            {
                var entity = repo.GetById(item.ProfileID, userId);

                if (entity != null)
                    entity.EmailAlert = item.EmailAlert;
            }

            _uow.Save();
        }
    }
}
