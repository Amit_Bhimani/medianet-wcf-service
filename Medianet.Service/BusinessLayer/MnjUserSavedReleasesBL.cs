﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class MnjUserSavedReleasesBL
    {
        private UnitOfWork _uow;

        public MnjUserSavedReleasesBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public ReleasePagePublic GetSavedReleases(int userId, int itemsPerPage, int currentPage)
        {
            var list = new MnjUserSavedReleasesRepository(_uow).GetSavedReleases(userId, itemsPerPage, currentPage);

            ReleasePagePublic result = new ReleasePagePublic
            {
                Releases = new List<ReleaseSummaryPublic>(),
                TotalCount = list == null || !list.Any() ? 0 : list.First().TotalCount
            };

            var urlHelper = new Common.UrlHelper();
            var thumbnailBL = new ThumbnailBL(_uow);

            foreach (var entity in list)
            {
                var releaseSummary = AutoMapper.Mapper.Map<Model.Entities.MnjProfileReleases, ReleaseSummaryPublic>(entity);
                if (entity.PhotoDocId > 0)
                {
                    Thumbnail thumbnail = thumbnailBL.GetAllByDocumentId(entity.PhotoDocId).FirstOrDefault();

                    if (thumbnail != null)
                    {
                        releaseSummary.Thumbnail = new ThumbnailPublic();
                        releaseSummary.Thumbnail.Url = urlHelper.GetImageThumbnailUrl(thumbnail.Id);
                        releaseSummary.Thumbnail.Width = thumbnail.Width;
                        releaseSummary.Thumbnail.Height = thumbnail.Height;
                    }
                }

                result.Releases.Add(releaseSummary);
            }

            return result;
        }

        public void SaveReleases(int userId, string releaseIds)
        {
            new MnjUserSavedReleasesRepository(_uow).Save(userId, releaseIds);
        }

        public void Delete(int userId, List<int> releaseIds)
        {
            new MnjUserSavedReleasesRepository(_uow).Delete(userId, releaseIds);
        }
    }
}
