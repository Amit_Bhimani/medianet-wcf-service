﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;
using Medianet.Service.BusinessLayer.Exceptions;

namespace Medianet.Service.BusinessLayer
{
    public class MonitoringBL
    {
        private UnitOfWork _uow;

        public MonitoringBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public MonitoringSearch GetSearch(int id)
        {
            var repo = new MonitoringSearchRepository(_uow);
            Model.Entities.MonitoringSearch search = repo.GetById(id);
            return AutoMapper.Mapper.Map<Model.Entities.MonitoringSearch, MonitoringSearch>(search);
        }

        public int AddSearch(MonitoringSearch search)
        {
            var repo = new MonitoringSearchRepository(_uow);
            var dbEntity = AutoMapper.Mapper.Map<MonitoringSearch, Model.Entities.MonitoringSearch>(search);
            dbEntity.CreationDate = DateTime.Now;
            repo.Add(dbEntity);
            return _uow.Save();
        }

        public MonitoringCampaign GetCampaign(int id)
        {
            var repo = new MonitoringCampaignRepository(_uow);
            Model.Entities.MonitoringCampaign campaign = repo.GetById(id);
            return AutoMapper.Mapper.Map<Model.Entities.MonitoringCampaign, MonitoringCampaign>(campaign);
        }

        public List<MonitoringCampaign> GetCampaigns(int userId, string debtorNumber)
        {
            List<int> userIds = GetUserIds(userId, debtorNumber);

            var repo = new MonitoringCampaignRepository(_uow);
            List<Model.Entities.MonitoringCampaign> campaigns = repo.GetAll(userId, userIds);
            return AutoMapper.Mapper.Map<List<Model.Entities.MonitoringCampaign>, List<MonitoringCampaign>>(campaigns);
        }

        private List<int> GetUserIds(int userId, string debtorNumber)
        {
            var userRepo = new UserRepository(_uow);
            List<Model.Entities.User> dbEntities = userRepo.GetAllByDebtorNumber(debtorNumber);
            List<User> users = AutoMapper.Mapper.Map<List<Model.Entities.User>, List<User>>(dbEntities);
            List<int> userIds = users.Where(u => u.Id != userId).Select(u => u.Id).ToList();
            return userIds;
        }

        public bool IsCampaignNameUnique(MonitoringCampaign campaign)
        {
            var repo = new MonitoringCampaignRepository(_uow);
            
            if(campaign.IsPrivate)
            {
                List<Model.Entities.MonitoringCampaign> campaigns = repo.GetByNameAndUser(campaign.CampaignName, campaign.UserId);
                if (campaigns.Count > 0 && campaigns.First().Id != campaign.Id)
                {
                    return false;
                }
            }
            else
            {
                List<Model.Entities.MonitoringCampaign> campaigns = repo.GetByNameAndCustomer(campaign.CampaignName, campaign.DebtorNumber);
                if (campaigns.Count > 0 && campaigns.First().Id != campaign.Id)
                {
                    return false;
                }
            }

            return true;
        }

        public int AddCampaign(MonitoringCampaign campaign)
        {
            if (!IsCampaignNameUnique(campaign))
            {
                throw new DuplicateNameException(string.Format("Campaign with name '{0}' has already been created as a public campaign. Please change the name of this campaign.", campaign.CampaignName));
            }

            var repo = new MonitoringCampaignRepository(_uow);
            var dbEntity = AutoMapper.Mapper.Map<MonitoringCampaign, Model.Entities.MonitoringCampaign>(campaign);
            dbEntity.SearchCriteria.Id = 0;
            dbEntity.SearchCriteria.CreationDate = DateTime.Now;
            repo.Add(dbEntity);
            _uow.Save();
            return dbEntity.Id;
        }

        public void DeleteCampaign(int campaignId)
        {
            var repo = new MonitoringCampaignRepository(_uow);
            repo.Remove(campaignId);
            _uow.Save();
        }

        public void UpdateCampaign(MonitoringCampaign campaign)
        {
            if (!IsCampaignNameUnique(campaign))
            {
                throw new DuplicateNameException(string.Format("Campaign with name '{0}' has already been created as a public campaign. Please change the name of this campaign.", campaign.CampaignName));
            }

            var repo = new MonitoringCampaignRepository(_uow);
            var dbEntity = AutoMapper.Mapper.Map<MonitoringCampaign, Model.Entities.MonitoringCampaign>(campaign);
            repo.Update(dbEntity);
            _uow.Save();
        }

        public int AddEmail(MonitoringEmail email)
        {
            var repo = new MonitoringEmailRepository(_uow);
            var dbEntity = AutoMapper.Mapper.Map<MonitoringEmail, Model.Entities.MonitoringEmail>(email);
            dbEntity.SearchCriteria.Id = 0;
            dbEntity.SearchCriteria.CreationDate = DateTime.Now;
            dbEntity.EmailDate = DateTime.Now;
            repo.Add(dbEntity);
            return _uow.Save();
        }

        public List<MonitoringEmail> GetEmailByUser(int userId)
        {
            var repo = new MonitoringEmailRepository(_uow);
            List<Model.Entities.MonitoringEmail> emails = repo.GetAllByUser(userId);
            return AutoMapper.Mapper.Map<List<Model.Entities.MonitoringEmail>, List<MonitoringEmail>>(emails);
        }

        public List<MonitoringEmail> GetEmailByCustomer(string debtorNumber)
        {
            List<int> userIds = GetUserIds(0, debtorNumber); 
            
            var repo = new MonitoringEmailRepository(_uow);
            List<Model.Entities.MonitoringEmail> emails = repo.GetAllByCustomer(userIds);
            return AutoMapper.Mapper.Map<List<Model.Entities.MonitoringEmail>,  List<MonitoringEmail>>(emails);
        }

        public int AddAlert(MonitoringAlert alert)
        {
            var repo = new MonitoringAlertRepository(_uow);
            var dbEntity = AutoMapper.Mapper.Map<MonitoringAlert, Model.Entities.MonitoringAlert>(alert);
            repo.Add(dbEntity);
            _uow.Save();
            return dbEntity.Id;
        }

        public void DeleteAlert(int alertId)
        {
            var repo = new MonitoringAlertRepository(_uow);
            repo.Remove(alertId);
            _uow.Save();
        }

        public void UpdateAlert(MonitoringAlert alert)
        {
            var repo = new MonitoringAlertRepository(_uow);
            var dbEntity = AutoMapper.Mapper.Map<MonitoringAlert, Model.Entities.MonitoringAlert>(alert);
            repo.Update(dbEntity);
            _uow.Save();
        }

        public List<MonitoringCampaign> GetCampaignsByAlertFrequency(int frequencyId)
        {
            var repo = new MonitoringCampaignRepository(_uow);
            List<Model.Entities.MonitoringCampaign> campaigns = repo.GetAllByAlertFrequency(frequencyId);
            return AutoMapper.Mapper.Map<List<Model.Entities.MonitoringCampaign>, List<MonitoringCampaign>>(campaigns);
        }

        public List<MonitoringCampaign> GetAllCampaigns()
        {
            var repo = new MonitoringCampaignRepository(_uow);
            List<Model.Entities.MonitoringCampaign> campaigns = repo.GetAll();
            return AutoMapper.Mapper.Map<List<Model.Entities.MonitoringCampaign>, List<MonitoringCampaign>>(campaigns);
        }
    }
}
