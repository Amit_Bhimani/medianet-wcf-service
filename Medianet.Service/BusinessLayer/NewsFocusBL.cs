﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class NewsFocusBL
    {
        private UnitOfWork _uow;

        public NewsFocusBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public NewsFocus Get(int id)
        {
            var repo = new NewsFocusRepository(_uow);
            Model.Entities.NewsFocus dbEntities;

            dbEntities = repo.Get(id);

            return AutoMapper.Mapper.Map<Model.Entities.NewsFocus, NewsFocus>(dbEntities);
        }

        public List<NewsFocus> GetAll()
        {
            var repo = new NewsFocusRepository(_uow);
            List<Model.Entities.NewsFocus> dbEntities;

            dbEntities = repo.GetAll();

            return AutoMapper.Mapper.Map<List<Model.Entities.NewsFocus>, List<NewsFocus>>(dbEntities);
        }
    }
}
