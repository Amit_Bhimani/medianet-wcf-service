﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;
using Medianet.BusinessLayer.Common;

namespace Medianet.Service.BusinessLayer
{
    public class NoteBL
    {
        private UnitOfWork _uow;

        public NoteBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public Note Get(int id)
        {
            var repo = new NoteRepository(_uow);
            Model.Entities.Note dbEntities;

            dbEntities = repo.Get(id);

            return AutoMapper.Mapper.Map<Model.Entities.Note, Note>(dbEntities);
        }

        public void Delete(int id)
        {
            var repo = new NoteRepository(_uow);
            repo.Delete(id);
        }

        public void PinUnpin(int id, bool pinned)
        {
            var repo = new NoteRepository(_uow);
            repo.PinUnpin(id,pinned);
        }

        public int Add(Note note)
        {
            var repo = new NoteRepository(_uow);
            var dbEntity = AutoMapper.Mapper.Map<Note, Model.Entities.Note>(note);
            return repo.Add(dbEntity);
        }

        public void Update(Note note)
        {
            var repo = new NoteRepository(_uow);
            var dbEntity = AutoMapper.Mapper.Map<Note, Model.Entities.Note>(note);
            repo.Update(dbEntity);
        }

        public void AddBulk(NotesBulk notes)
        {
            var repo = new NoteRepository(_uow);
            var dbEntities = new List<Model.Entities.Note>();

            foreach (var record in notes.Records)
            {
                var dbEntity = new Model.Entities.Note
                {
                    Notes = notes.Note,
                    CreatedDate = DateTime.Now,
                    Status = 1,
                    IsPrivate = notes.IsPrivate,
                    UserId = notes.UserId,
                    MediaOutletId = record.MediaOutletId,
                    MediaContactId = record.MediaContactId,
                    OmaOutletId = record.OmaOutletId,
                    OmaContactId = record.OmaContactId,
                    PrnOutletId = record.PrnOutletId,
                    PrnContactId = record.PrnContactId,
                    Pinned = notes.IsPinned,
                    AAPNote = notes.IsAapNote,
                    DebtorNumber = notes.DebtorNumber
                };

                dbEntities.Add(dbEntity);
            }

            repo.AddBulk(dbEntities);
        }

        public List<Note> GetAll(string outletId, string contactId, string recordType,string debtorNumber, int userId)
        {
            var repo = new NoteRepository(_uow,debtorNumber,userId);
            var dbEntities = new List<Model.Entities.Note>();
            var rt = (RecordType)Enum.Parse(typeof(RecordType), recordType.ToString());

            //Get outlet notes
            if (!string.IsNullOrEmpty(outletId) && string.IsNullOrEmpty(contactId))
            {
                switch (rt)
                {
                    case RecordType.MediaOutlet:
                        dbEntities = repo.GetMediaOutletNotes(outletId);
                        break;
                    case RecordType.OmaOutlet:
                        int omaOId;

                        if (int.TryParse(outletId, out omaOId))
                        {
                            dbEntities = repo.GetOmaOutletNotes(omaOId);
                        }
                        break;
                    case RecordType.PrnOutlet:
                        long prnOId;

                        if (long.TryParse(outletId, out prnOId))
                        {
                            dbEntities = repo.GetPrnOutletNotes(prnOId);
                        }
                        break;
                }

            }
            //Get contact notes
            else if (!string.IsNullOrEmpty(outletId) && !string.IsNullOrEmpty(contactId))
            {
                switch (rt)
                {
                    case RecordType.MediaContact:
                        dbEntities = repo.GetMediaContactNotes(outletId, contactId);
                        break;
                    case RecordType.PrnContact:
                        long prnOId;
                        long prnCId;

                        if (long.TryParse(outletId, out prnOId) && long.TryParse(contactId, out prnCId))
                        {
                            dbEntities = repo.GetPrnContactNotes(prnOId, prnCId);
                        }
                        break;
                }

                if (RecordTypeExtensions.IsOmaContact(rt))
                {
                    int omaCId;
                    if (int.TryParse(contactId, out omaCId))
                    {
                        dbEntities = repo.GetOmaContactNotes(omaCId);
                    }
                }
            }

            return AutoMapper.Mapper.Map<List<Model.Entities.Note>, List<Note>>(dbEntities);
        }
    }
}
