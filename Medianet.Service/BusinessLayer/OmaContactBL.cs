﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;
using Medianet.BusinessLayer.Model;
using Medianet.BusinessLayer.Common;

namespace Medianet.Service.BusinessLayer
{
    public class OmaContactBL
    {
        private UnitOfWork _uow;

        public OmaContactBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public List<int> GetContactIds(int outletId)
        {
            return new MediaOutletRepository(_uow).GetOmaContactIds(outletId);
        }
    }
}
