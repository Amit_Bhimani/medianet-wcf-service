﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;
using Medianet.BusinessLayer.Common;

namespace Medianet.Service.BusinessLayer
{
    public class OmaDocumentBL
    {
        private UnitOfWork _uow;

        public OmaDocumentBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public OmaDocument GetById(int documentId, int userID, string debtorNumber)
        {
            var repo = new OmaDocumentRepository(_uow);

            Model.Entities.OmaDocumentView entity = repo.GetById(documentId);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.OmaDocumentView, OmaDocument>(entity);
        }

        public List<OmaDocument> GetList(string outletId, string contactId, RecordType recordType, int userID, string debtorNumber)
        {
            var repo = new OmaDocumentRepository(_uow);

            List<Model.Entities.OmaDocumentView> list = repo.GetList(outletId, contactId, recordType, userID, debtorNumber);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.OmaDocumentView>, List<OmaDocument>>(list);
        }

        public OmaDocument GetDocumentFile(int documentId, int userID, string debtorNumber)
        {
            var repo = new OmaDocumentRepository(_uow);

            Model.Entities.OmaDocument entity = repo.GetFile(documentId);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.OmaDocument, OmaDocument>(entity);
        }

        public void Save(OmaDocument entity, int userID, string debtorNumber)
        {
            var repo = new OmaDocumentRepository(_uow);

            var model = AutoMapper.Mapper.Map<OmaDocument, Model.Entities.OmaDocumentView>(entity);

            if (entity.DocumentId <= 0)
                switch ((RecordType)entity.RecordType)
                {
                    case RecordType.MediaOutlet:
                        model.MediaOutletId = entity.OutletId;
                        break;
                    case RecordType.MediaContact:
                        model.MediaContactId = entity.ContactId;
                        model.MediaOutletId = entity.OutletId;
                        break;
                    case RecordType.PrnOutlet:
                        model.PrnOutletId = decimal.Parse(entity.OutletId);
                        break;

                    case RecordType.PrnContact:
                        model.PrnContactId = decimal.Parse(entity.ContactId);
                        model.PrnOutletId = decimal.Parse(entity.OutletId);
                        break;

                    case RecordType.OmaOutlet:
                        model.OmaOutletId = int.Parse(entity.OutletId);
                        break;

                    case RecordType.OmaContactAtPrnOutlet:
                        model.OmaContactId = int.Parse(entity.ContactId);
                        model.PrnOutletId = decimal.Parse(entity.OutletId);
                        break;
                    case RecordType.OmaContactAtMediaOutlet:
                        model.OmaContactId = int.Parse(entity.ContactId);
                        model.MediaOutletId = entity.OutletId;
                        break;
                    case RecordType.OmaContactNoOutlet:
                        model.OmaContactId = int.Parse(entity.ContactId);
                        break;
                    case RecordType.OmaContactAtOmaOutlet:
                        model.OmaContactId = int.Parse(entity.ContactId);
                        model.OmaOutletId = int.Parse(entity.OutletId);
                        break;
                }

            repo.Save(model);
        }

        public void Delete(int documentId, int userId, string debtorNumber)
        {
            var repo = new OmaDocumentRepository(_uow);

            repo.Delete(documentId, userId, debtorNumber);
        }
    }
}
