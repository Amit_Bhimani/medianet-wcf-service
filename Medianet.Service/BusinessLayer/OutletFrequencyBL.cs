﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class OutletFrequencyBL
    {
        private UnitOfWork _uow;

        public OutletFrequencyBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public OutletFrequency Get(int id)
        {
            var repo = new OutletFrequencyRepository(_uow);
            Model.Entities.OutletFrequency dbEntities;

            dbEntities = repo.Get(id);

            return AutoMapper.Mapper.Map<Model.Entities.OutletFrequency, OutletFrequency>(dbEntities);
        }

        public List<OutletFrequency> GetAll()
        {
            var repo = new OutletFrequencyRepository(_uow);
            List<Model.Entities.OutletFrequency> dbEntities;

            dbEntities = repo.GetAll();

            return AutoMapper.Mapper.Map<List<Model.Entities.OutletFrequency>, List<OutletFrequency>>(dbEntities);
        }
    }
}
