﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class OutletTypeBL
    {
        private UnitOfWork _uow;

        public OutletTypeBL(UnitOfWork uow) {
            _uow = uow;
        }

        public OutletType Get(int id)
        {
            var repo = new OutletTypeRepository(_uow);
            Model.Entities.OutletType dbEntities;

            dbEntities = repo.Get(id);

            return AutoMapper.Mapper.Map<Model.Entities.OutletType, OutletType>(dbEntities);
        }

        public List<OutletType> GetAll()
        {
            var repo = new OutletTypeRepository(_uow);
            List<Model.Entities.OutletType> dbEntities;

            dbEntities = repo.GetAll();

            return AutoMapper.Mapper.Map<List<Model.Entities.OutletType>, List<OutletType>>(dbEntities);
        }
    }
}
