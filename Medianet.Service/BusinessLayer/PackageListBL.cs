﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.BusinessLayer.Common;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class PackageListBL
    {
        private UnitOfWork _uow;

        public PackageListBL(UnitOfWork uow) {
            _uow = uow;
        }

        public PackageList Get(int id) {
            var repo = new PackageListRepository(_uow);
            PackageList package;
            List<Model.Entities.ServiceList> dbServices;
            Model.Entities.PackageList dbEntity;

            dbEntity = repo.GetById(id);

            if (dbEntity == null)
                return null;

            dbServices = repo.GetServicesById(id);

            // Map the database object to a friendly representation of the data.
            package = AutoMapper.Mapper.Map<Model.Entities.PackageList, PackageList>(dbEntity);

            // Add the address counts for Email, SMS, Voice and Wire.
            package.EmailAddressCount = dbServices.Where(d => d.DistributionType == ((char)(DistributionType.Email)).ToString()).Sum(c => c.AddressCount);
            package.SMSAddressCount = dbServices.Where(d => d.DistributionType == ((char)(DistributionType.SMS)).ToString()).Sum(c => c.AddressCount);
            package.VoiceAddressCount = dbServices.Where(d => d.DistributionType == ((char)(DistributionType.Voice)).ToString()).Sum(c => c.AddressCount);
            package.WireAddressCount = dbServices.Where(d => d.DistributionType == ((char)(DistributionType.Wire)).ToString()).Sum(c => c.AddressCount);

            return package;
        }

        public List<PackageList> GetTopValue() {
            var repo = new PackageListRepository(_uow);
            List<Model.Entities.PackageList> dbEntities;

            dbEntities = repo.GetTopValue(Common.Constants.DEBTOR_NUMBER_AAP);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.PackageList>, List<PackageList>>(dbEntities);
        }

        public List<PackageList> GetAllByDebtorNumberSystem(string debtorNumber, SystemType system)
        {
            var repo = new PackageListRepository(_uow);
            List<Model.Entities.PackageList> dbEntities;

            dbEntities = repo.GetAllByDebtorNumberSystem(debtorNumber, ((char)system).ToString());

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.PackageList>, List<PackageList>>(dbEntities);
        }

        public List<PackageList> GetAllByDebtorNumberAccountCode(string debtorNumber, string accountCode)
        {
            var repo = new PackageListRepository(_uow);
            List<Model.Entities.PackageList> dbEntities = null;

            // Temporary hack. If the account code is P1234 then 1234 is the Id of the package, not the accountcode.
            // Eventually the accouncodes will be changed to P1234 for existing, and new ones will be allocated from SalesForce.
            if (accountCode.StartsWith("P"))
            {
                int id;

                if (int.TryParse(accountCode.Substring(1), out id))
                {
                    var s = repo.GetById(id);
                    if (s != null)
                    {
                        dbEntities = new List<Model.Entities.PackageList>();
                        dbEntities.Add(s);
                    }
                }
            }

            if (dbEntities == null)
                dbEntities = repo.GetAllByDebtorNumberAccountCode(debtorNumber, accountCode);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.PackageList>, List<PackageList>>(dbEntities);

        }

        public List<ListTicker> GetForTicker()
        {
            var repo = new PackageListRepository(_uow);
            List<Model.Entities.ListTicker> dbEntities;

            dbEntities = repo.GetForTicker(Common.Constants.DEBTOR_NUMBER_AAP, ((char)SystemType.Medianet).ToString());

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.ListTicker>, List<ListTicker>>(dbEntities);
        }

        public ListComment GetCommentById(int id)
        {
            var repo = new PackageListRepository(_uow);
            Model.Entities.ListComment dbEntity;

            dbEntity = repo.GetCommentById(id);

            if (dbEntity == null)
                return null;

            if (dbEntity.Comment != null)
                dbEntity.Comment = dbEntity.Comment.Replace("\r\n", "<br />");

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.ListComment, ListComment>(dbEntity);
        }

        public List<string> GetRecipientOutletsById(int id)
        {
            var repo = new PackageListRepository(_uow);
            List<int> serviceIds;
            List<string> dbList = new List<string>();

            serviceIds = repo.GetServiceIdsById(id);

            var listBL = new ServiceListBL(_uow);

            foreach (int s in serviceIds)
            {
                dbList.AddRange(listBL.GetRecipientOutletNames(s));
            }

            return dbList.Distinct().OrderBy(ld => ld).ToList();
        }

        public List<ServiceList> GetServicesById(int id)
        {
            var repo = new PackageListRepository(_uow);
            List<Model.Entities.ServiceList> dbEntities;

            dbEntities = repo.GetServicesById(id);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.ServiceList>, List<ServiceList>>(dbEntities);
        }

        public int Add(PackageList entity, int userId)
        {
            var dbEntity = AutoMapper.Mapper.Map<PackageList, Model.Entities.PackageList>(entity);
            var repo = new PackageListRepository(_uow);

            dbEntity.CreatedDate = DateTime.Now;
            dbEntity.CreatedByUserId = userId;
            dbEntity.LastModifiedDate = dbEntity.CreatedDate;
            dbEntity.LastModifiedByUserId = userId;
            dbEntity = TruncatePackageList(dbEntity);

            repo.Add(dbEntity);
            repo.UpdateInNopCommerce(dbEntity);

            return dbEntity.Id;
        }

        public void Update(PackageList entity, int userId)
        {
            var dbEntity = AutoMapper.Mapper.Map<PackageList, Model.Entities.PackageList>(entity);
            var repo = new PackageListRepository(_uow);

            dbEntity.LastModifiedDate = DateTime.Now;
            dbEntity.LastModifiedByUserId = userId;
            dbEntity = TruncatePackageList(dbEntity);

            repo.Update(dbEntity);
            _uow.Save();
            repo.UpdateInNopCommerce(dbEntity);
        }

        public void Delete(int id, int userId)
        {
            Model.Entities.PackageList dbEntity;
            var repo = new PackageListRepository(_uow);

            dbEntity = repo.GetById(id);
            dbEntity.LastModifiedDate = DateTime.Now;
            dbEntity.LastModifiedByUserId = userId;
            dbEntity.RowStatus = ((char)RowStatusType.Deleted).ToString();
            repo.Update(dbEntity);
        }
        
        public string GetAttachmentFileName(int packageId)
        {
            var entiy = new PackageAttachmentRepository(_uow).GetByPackageId(packageId);

            return entiy == null ? null : entiy.FileName;
        }

        #region Private methods

        private Model.Entities.PackageList TruncatePackageList(Model.Entities.PackageList dbEntity)
        {
            dbEntity.SelectionDescription = dbEntity.SelectionDescription.Truncate(50);
            dbEntity.Introduction = dbEntity.Introduction.Truncate(255);

            return dbEntity;
        }

        #endregion
    }
}
