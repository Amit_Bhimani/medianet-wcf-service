﻿using System.Collections.Generic;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model.Entities;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class PrnContactViewBL
    {
        private readonly UnitOfWork _uow;
        public PrnContactViewBL(UnitOfWork uow)
        {
            _uow = uow;
        }
        public long Add (PrnContactViewDto entity)
        {
            var dbEntity = AutoMapper.Mapper.Map<Dto.PrnContactViewDto, Model.Entities.PrnContactView>(entity);
            var repo = new PrnContactViewRepository(_uow);

            repo.Add(dbEntity);
            _uow.Save();
            return dbEntity.Id;
        }
        public List<MediaContactAlsoViewedDto> GetPrnContactViewSummary(string contactId, string outletId,List<int> subjectCodeIds, List<int> continentIds, int maxCount)
        {
            var repo = new PrnContactViewRepository(_uow);
            var dbEntities = repo.GetPrnContactViewSummary(contactId, outletId, subjectCodeIds, continentIds, maxCount);

            var contacts = AutoMapper.Mapper.Map<List<Model.Entities.MediaContactAlsoViewed>, List<MediaContactAlsoViewedDto>>(dbEntities);
            return contacts;
        }
    }
}
