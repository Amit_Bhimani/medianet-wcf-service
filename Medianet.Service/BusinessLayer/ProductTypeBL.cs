﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class ProductTypeBL
    {
        private UnitOfWork _uow;

        public ProductTypeBL(UnitOfWork uow) {
            _uow = uow;
        }

        public ProductType Get(int id)
        {
            var repo = new ProductTypeRepository(_uow);
            Model.Entities.ProductType dbEntities;

            dbEntities = repo.Get(id);

            return AutoMapper.Mapper.Map<Model.Entities.ProductType, ProductType>(dbEntities);
        }

        public List<ProductType> GetAll()
        {
            var repo = new ProductTypeRepository(_uow);
            List<Model.Entities.ProductType> dbEntities;

            dbEntities = repo.GetAll();

            return AutoMapper.Mapper.Map<List<Model.Entities.ProductType>, List<ProductType>>(dbEntities);
        }

        public List<ProductType> GetByOutletType(int outletTypeId)
        {
            var repo = new ProductTypeRepository(_uow);
            List<Model.Entities.ProductType> dbEntities;

            dbEntities = repo.GetAllByOutletType(outletTypeId);

            return AutoMapper.Mapper.Map<List<Model.Entities.ProductType>, List<ProductType>>(dbEntities);
        }

    }
}
