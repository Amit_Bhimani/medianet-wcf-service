﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class RegistrationValidationBL
    {
        private UnitOfWork _uow;

        public RegistrationValidationBL(UnitOfWork uow) {
            _uow = uow;
        }

        public RegistrationValidation Get(Guid token) {
            var repo = new RegistrationValidationRepository(_uow);
            Model.Entities.RegistrationValidation dbEntity;

            dbEntity = repo.Get(token);

            return AutoMapper.Mapper.Map<Model.Entities.RegistrationValidation, RegistrationValidation>(dbEntity);
        }

        public RegistrationValidation GetByEmail(string email)
        {
            var repo = new RegistrationValidationRepository(_uow);
            Model.Entities.RegistrationValidation dbEntity;

            dbEntity = repo.GetByEmail(email);

            return AutoMapper.Mapper.Map<Model.Entities.RegistrationValidation, RegistrationValidation>(dbEntity);
        }

        public Guid Add(RegistrationValidation entity) {
            var dbEntity = AutoMapper.Mapper.Map<RegistrationValidation, Model.Entities.RegistrationValidation>(entity);
            var repo = new RegistrationValidationRepository(_uow);

            dbEntity.CreatedDate = DateTime.Now;
            dbEntity.Token = Guid.NewGuid();

            repo.Add(dbEntity);

            return dbEntity.Token;
        }
    }
}
