﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Transactions;
using System.IO;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;
using Medianet.Service.BusinessLayer.Common;
using System.Configuration;

namespace Medianet.Service.BusinessLayer
{
    public class ReleaseBL
    {
        private UnitOfWork _uow;
        private List<string> _statusListReleased = null;
        private List<string> _statusListAvailable = null;

        public ReleaseBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public Release Get(int id)
        {
            var repo = new ReleaseRepository(_uow);
            Model.Entities.Release dbEntity;

            dbEntity = repo.GetById(id);

            return AutoMapper.Mapper.Map<Model.Entities.Release, Release>(dbEntity);
        }

        public List<Release> GetAllByDebtorNumber(string debtorNo, SystemType system, ReleaseSortOrderType sortOrder, int startPos, int pageSize)
        {
            var repo = new ReleaseRepository(_uow);
            List<Model.Entities.Release> dbEntities;

            dbEntities = repo.GetAllByDebtorNumber(debtorNo, ((char)system).ToString(), StatusListAvailable, (int)sortOrder, startPos, pageSize);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.Release>, List<Release>>(dbEntities);
        }

        public int GetCountByDebtorNumber(string debtorNo, SystemType system)
        {
            var repo = new ReleaseRepository(_uow);

            return repo.GetCountByDebtorNumber(debtorNo, ((char)system).ToString(), StatusListAvailable);
        }

        void OpenRelease(int id, ReleaseStatusType status, int userId)
        {
            var repo = new ReleaseRepository(_uow);

            repo.OpenRelease(id, ((char)status).ToString(), userId);
        }

        public void CloseRelease(int id)
        {
            var repo = new ReleaseRepository(_uow);

            repo.CloseRelease(id);
        }

        public ReleasePublic GetPublic(int id, WebsiteType website, string securityKey = null)
        {
            var relRepo = new ReleaseRepository(_uow);
            Model.Entities.Release dbRelease;
            ReleasePublic release = null;
            string content = string.Empty;

            dbRelease = relRepo.GetByIdWithDocuments(id);

            // If they have the correct security key then let them see it. Otherwise make sure it's publicly available.
            if (dbRelease != null && dbRelease.WebDetails != null)
            {
                bool isVisible = dbRelease.WebDetails.ShouldShowOnPublic.HasValue &&
                                 dbRelease.WebDetails.ShouldShowOnPublic.Value &&
                                 dbRelease.WebDetails.PublicDistributedDate.HasValue;

                if ((!string.IsNullOrWhiteSpace(securityKey) && securityKey.Equals(dbRelease.SecurityKey.ToString())) || isVisible)
                {
                    // Find the release content. Start by looking for the email/wire html.
                    var contentDoc = dbRelease.Documents.FirstOrDefault(d => !d.IsDerived && d.CanDistributeViaEmailBody);

                    if (contentDoc == null) // original html.
                        contentDoc =
                            dbRelease.Documents.FirstOrDefault(
                                d =>
                                !d.IsDerived && d.CanDistributeViaWeb &&
                                d.FileExtension.IndexOf("htm", StringComparison.CurrentCultureIgnoreCase) >= 0);

                    if (contentDoc == null) // derived html from a word conversion.
                        contentDoc =
                            dbRelease.Documents.FirstOrDefault(
                                d =>
                                d.IsDerived && d.CanDistributeViaWeb &&
                                d.FileExtension.IndexOf("htm", StringComparison.CurrentCultureIgnoreCase) >= 0);

                    if (contentDoc == null) // wire text.
                        contentDoc =
                            dbRelease.Documents.FirstOrDefault(
                                d =>
                                d.IsDerived && d.CanDistributeViaWire &&
                                d.FileExtension.IndexOf("txt", StringComparison.CurrentCultureIgnoreCase) >= 0);

                    if (contentDoc != null)
                        content = GetHtmlContent(contentDoc, dbRelease.SecurityKey.ToString());

                    release = GetPublicReleaseWithDocuments(dbRelease, contentDoc == null ? 0 : contentDoc.Id);
                    release.HtmlContent = content;
                    release.IsVerified = (dbRelease.WebDetails.IsVerified.HasValue ? dbRelease.WebDetails.IsVerified.Value : false);

                    // If this release doesn't have a short url yet and it's distributed then create one now.
                    if (string.IsNullOrWhiteSpace(release.ShortUrl) && isVisible)
                        release.ShortUrl = CreateShortUrl(dbRelease.WebDetails);

                    if (website == WebsiteType.Journalists)
                    {
                        release.IsVisible = dbRelease.WebDetails.ShouldShowOnJournalists.HasValue &&
                                 dbRelease.WebDetails.ShouldShowOnJournalists.Value &&
                                 dbRelease.WebDetails.JournalistsDistributedDate.HasValue; ;
                        release.DistributedDate = dbRelease.WebDetails.JournalistsDistributedDate;
                    }
                    else
                    {
                        release.IsVisible = isVisible;
                        release.DistributedDate = dbRelease.WebDetails.PublicDistributedDate;
                    }
                }
            }

            return release;
        }

        public List<ReleaseSummaryPublic> GetAllPublicFeatureReleases(int? webCategoryId)
        {
            var repo = new ReleaseRepository(_uow);
            var docRepo = new DocumentRepository(_uow);
            List<Model.Entities.FeatureRelease> dbEntities = null;
            var results = new List<ReleaseSummaryPublic>();
            var urlHelper = new UrlHelper();

            if (webCategoryId.HasValue)
                dbEntities = repo.GetAllFeatureReleases(webCategoryId)
                    .Where(f => f.Release.WebDetails.ShouldShowOnPublic.HasValue && f.Release.WebDetails.ShouldShowOnPublic.Value && f.Release.WebDetails.PublicDistributedDate.HasValue).ToList();

            // If we weren't passed a WebCategoryId or didn't find any above then get the default feature releases.
            if (dbEntities == null || dbEntities.Count == 0)
                dbEntities = repo.GetAllFeatureReleases(null)
                    .Where(f => f.Release.WebDetails.ShouldShowOnPublic.HasValue && f.Release.WebDetails.ShouldShowOnPublic.Value && f.Release.WebDetails.PublicDistributedDate.HasValue).ToList();

            // Get the attachment details for each release.
            foreach (var feature in dbEntities)
            {
                var relPublic = AutoMapper.Mapper.Map<Model.Entities.FeatureRelease, ReleaseSummaryPublic>(feature);

                if (relPublic.HasPhoto)
                {
                    // Look for a thumbnail for the first photo to set the ThumbnailUrl.
                    var dbDocuments = docRepo.GetAllByReleaseIdWithThumbnails(relPublic.Id);

                    if (dbDocuments != null)
                    {
                        var images = dbDocuments.Where(d => !d.IsDerived && d.ContentType.IndexOf("image", StringComparison.CurrentCultureIgnoreCase) >= 0)
                            .OrderBy(d => d.SequenceNumber).ToList();

                        if (images != null && images.Count > 0)
                        {
                            if (relPublic.MultimediaType != MultimediaType.Multimedia && relPublic.MultimediaType != MultimediaType.Video)
                                relPublic.Thumbnail = GetThumbnailsPublic(images[0].Thumbnails, urlHelper).FirstOrDefault();
                            else
                                relPublic.Thumbnail = GetWieckReleaseThumbnailPublic(feature.Release.WieckReference, feature.Release.WieckThumbnailHeight, feature.Release.WieckThumbnailWidth, urlHelper);
                        }
                        else
                            relPublic.HasPhoto = false;
                    }
                    else
                        relPublic.HasPhoto = false;
                }

                results.Add(relPublic);
            }

            return results;
        }

        public List<ReleaseSummaryPublic> GetAllPublicByCriteria(WebsiteType website, DateTime fromDate, DateTime toDate, int? categoryId, string searchText, MultimediaType type,
            bool showVideos, bool showAudios, bool showPhotos, bool showAttachments, bool showMultimedia, int pageNumber, int pageSize, bool showMedianetOnly,
            out int totalCount)
        {

            var repo = new ReleaseRepository(_uow);
            List<Model.Entities.ReleaseSummaryPublic> dbEntities;
            var results = new List<ReleaseSummaryPublic>();
            var urlHelper = new UrlHelper();

            dbEntities = repo.GetAllPublicByCriteria(fromDate, toDate, categoryId, searchText, ((char)type).ToString(),
                showVideos, showAudios, showPhotos, showAttachments, showMultimedia, pageNumber, pageSize, !showMedianetOnly, website == WebsiteType.Journalists);

            if (dbEntities != null && dbEntities.Count > 0)
            {
                foreach (var rs in dbEntities)
                {
                    var relPublic = AutoMapper.Mapper.Map<Model.Entities.ReleaseSummaryPublic, ReleaseSummaryPublic>(rs);

                    relPublic.DistributedDateTime = rs.WhenDistributed.ToString("dd MMM yyy HH:mm");

                    if (relPublic.MultimediaType == MultimediaType.Multimedia || relPublic.MultimediaType == MultimediaType.Video)
                    {
                        // If this is a Wieck job we need to provide a Url for when they want to view the release.
                        relPublic.ReleaseUrl = urlHelper.GetWieckReleaseUrl(relPublic.MultimediaType, rs.WieckReferenceId);
                    }

                    if (rs.PhotoThumbnailId.HasValue && rs.PhotoThumbnailId.Value > 0)
                    {
                        relPublic.Thumbnail = new ThumbnailPublic();
                        relPublic.Thumbnail.Url = urlHelper.GetImageThumbnailUrl(rs.PhotoThumbnailId.Value);
                        relPublic.Thumbnail.Width = rs.PhotoThumbnailWidth ?? 0;
                        relPublic.Thumbnail.Height = rs.PhotoThumbnailHeight ?? 0;
                    }

                    // Only Journalists get the SecurityKey so they can see releases not made public
                    if (website == WebsiteType.Journalists)
                        relPublic.SecurityKey = rs.SecurityKey.ToString();

                    results.Add(relPublic);
                }

                totalCount = dbEntities[0].TotalCount;
            }
            else
                totalCount = 0;

            return results;
        }

        public int Add(Release release, int userId)
        {
            Random r = new Random();

            // First make sure all our files exist.
            foreach (var doc in release.Documents)
            {
                if (!File.Exists(doc.SavedFile))
                    throw new Exception(string.Format("File {0} not found.", doc.SavedFile));
            }

            // Create a SecurityKey required to display the Release.
            release.SecurityKey = r.Next(0, 10000000).ToString();

            // Remove the Unknown transactions. They are only there for quoting. The database doesn't support them yet.
            release.Transactions = release.Transactions.Where(t => t.DistributionType != DistributionType.Unknown).ToList();

            try
            {
                // Begin Transaction.
                var releaseRepo = new ReleaseRepository(_uow);
                var historyRepo = new ReleaseHistoryRepository(_uow);
                var dbEntity = AutoMapper.Mapper.Map<Release, Model.Entities.Release>(release);

                release.Id = releaseRepo.Add(dbEntity);

                historyRepo.Add(new Model.Entities.ReleaseHistory()
                {
                    ReleaseId = release.Id,
                    Status = ((char)release.Status).ToString(),
                    CreatedByUserId = userId,
                    CreatedDate = DateTime.Now
                });

                if (release.WebDetails != null)
                {
                    CreateWebRelease(release.WebDetails, release.Id);
                }

                CreateTransactions(release.Transactions, release.DebtorNumber, release.Id, userId);
                CreateDocuments(release.Documents, release.Id, userId);
                CreateWireReleaseCategories(release.WebDetails, release.Id);

                // no need to update HasTransactions column because now we have CreditCard and Invoice
                //var customerBL = new CustomerBL(_uow);
                //var cust = customerBL.Get(release.DebtorNumber);
                //var cust = cbservice.GetChargeBeeCustomer(release.DebtorNumber);
                //if (!cust.HasTransactions) // not having Column HasTransactions
                //{
                //    cust.HasTransactions = true;
                //    customerBL.Update(cust, userId);
                //}

                releaseRepo.CloseRelease(release.Id);

                //End Transaction
            }
            catch (Exception)
            {
                DeleteRelease(release.Id);
                throw;
            }

            return release.Id;
        }

        public void UpdateDebtorNumberByUserId(int userId, string newDebtorNumber)
        {
            var repo = new ReleaseRepository(_uow);

            repo.UpdateDebtorNumberByUserId(userId, newDebtorNumber);
        }

        public void RecordView(int id, string clientIp, string userAgent, WebsiteType website)
        {
            var repo = new ReleaseRepository(_uow);

            repo.RecordView(id, clientIp, userAgent, website.GetStringValue());
        }

        public void CancelRelease(int id, int userId)
        {
            var repo = new ReleaseRepository(_uow);

            repo.CancelRelease(id, userId);
        }

        #region Private methods

        private string CreateShortUrl(Model.Entities.WebRelease webRel)
        {
            try
            {
                var urlHelper = new UrlHelper();
                var urlShortener = new SocialMediaServices.Url.ShortUrlUtil();
                string fullUrl = urlHelper.GetReleaseUrl(webRel.ReleaseId);

                SocialMediaServices.Url.ShortUrlUtil.AccessToken = AAPEnvironmentHelper.GetSetting("BitlyAccessToken");
                SocialMediaServices.Url.ShortUrlUtil.AccessTokenForBackupUser = AAPEnvironmentHelper.GetSetting("BitlyAccessTokenBackup");
                SocialMediaServices.Url.ShortUrlUtil.ApiShortenUrl = AAPEnvironmentHelper.GetSetting("BitlyApiShortenUrl");

                webRel.ShortUrl = SocialMediaServices.Url.ShortUrlUtil.ShortenUrl(fullUrl);

                using (var localUow = new UnitOfWork())
                {
                    var webRepo = new WebReleaseRepository(localUow);

                    webRepo.Update(webRel);
                    localUow.Save();
                }

                return webRel.ShortUrl;
            }
            catch
            {
                return string.Empty;
            }
        }

        private ReleasePublic GetPublicReleaseWithDocuments(Model.Entities.Release dbRelease, int contentDocId = 0)
        {
            var release = AutoMapper.Mapper.Map<Model.Entities.Release, ReleasePublic>(dbRelease);
            bool isHostedAtAap = (release.MultimediaType != MultimediaType.Multimedia && release.MultimediaType != MultimediaType.Video);
            var urlHelper = new UrlHelper();

            release.Videos = new List<DocumentPublic>();
            release.Audios = new List<DocumentPublic>();
            release.Images = new List<DocumentPublic>();
            release.Attachments = new List<DocumentPublic>();

            foreach (var doc in dbRelease.Documents)
            {
                if (!doc.IsDerived && doc.Id != contentDocId)
                {
                    var docPublic = AutoMapper.Mapper.Map<Model.Entities.Document, DocumentPublic>(doc);

                    // Only show the download URL for releases hosted by us.
                    if (release.MultimediaType != MultimediaType.Multimedia)
                    {
                        if (string.IsNullOrEmpty(doc.MediaReferenceId))
                            docPublic.DownloadUrl = urlHelper.GetDocumentDownloadUrl(dbRelease.Id, doc.SequenceNumber, dbRelease.SecurityKey.ToString());
                        else
                            docPublic.DownloadUrl = urlHelper.GetWieckReleaseUrl(release.MultimediaType, doc.MediaReferenceId);
                    }

                    if (docPublic.ContentType.IndexOf("video", StringComparison.CurrentCultureIgnoreCase) >= 0)
                    {
                        release.Videos.Add(docPublic);
                    }
                    else if (docPublic.ContentType.IndexOf("audio", StringComparison.CurrentCultureIgnoreCase) >= 0)
                    {
                        release.Audios.Add(docPublic);
                    }
                    else if (docPublic.ContentType.IndexOf("image", StringComparison.CurrentCultureIgnoreCase) >= 0 &&
                             !docPublic.FileExtension.StartsWith("tif", StringComparison.CurrentCultureIgnoreCase))
                    {
                        if (isHostedAtAap)
                            docPublic.Thumbnails = GetThumbnailsPublic(doc.Thumbnails, urlHelper);
                        else
                            docPublic.Thumbnails = GetWieckThumbnailsPublic(dbRelease.WieckReference, doc.MediaReferenceId, doc.Height, doc.Width, doc.ContentType, urlHelper);

                        release.Images.Add(docPublic);
                    }
                    else
                    {
                        release.Attachments.Add(docPublic);
                    }
                }
            }

            return release;
        }

        private void CreateWebRelease(WebRelease webDetails, int releaseId)
        {
            var repo = new WebReleaseRepository(_uow);
            var dbEntity = AutoMapper.Mapper.Map<WebRelease, Model.Entities.WebRelease>(webDetails);

            dbEntity.ReleaseId = releaseId;
            repo.Add(dbEntity);
        }

        private void CreateTransactions(List<Transaction> transactions, string debtorNumber, int releaseId, int userId)
        {
            var transBL = new TransactionBL(_uow);
            List<string> data = new List<string>();

            foreach (var trans in transactions)
            {
                trans.ReleaseId = releaseId;
                trans.Id = transBL.Add(trans, userId);

                if (trans.EmailAlert == true)
                    data.Add(trans.PackageId.HasValue ? trans.PackageDescription : trans.SelectionDescription);
            }

            if (data.Any())
                EmailHelper.ServiceUsageEmail(releaseId, debtorNumber, string.Join(", ", data.Distinct()));
        }

        private void CreateDocuments(List<Document> documents, int releaseId, int userId)
        {
            string docFilePath = PathHelper.GetDocumentFileBasePath();
            string docFilePathAndFolder;
            var docBL = new DocumentBL(_uow);

            foreach (var doc in documents)
            {
                doc.ReleaseId = releaseId;
                doc.Id = docBL.Add(doc, userId);
                docFilePathAndFolder = docFilePath + PathHelper.GetDocumentFileFolder(doc.Id) + "\\";

                if (!Directory.Exists(docFilePathAndFolder))
                    Directory.CreateDirectory(docFilePathAndFolder);

                File.Copy(doc.SavedFile, docFilePathAndFolder + PathHelper.GetDocumentFileName(doc.Id, doc.FileExtension));

                // If this Document was created from an InboundFile then log it.
                if (doc.InboundFileId.HasValue && doc.InboundFileId.Value > 0)
                {
                    var inboundBL = new InboundFileBL(_uow);
                    inboundBL.AddUsageLog(doc.InboundFileId.Value, releaseId, doc.CreatedByUserId);
                }
            }
        }

        private void CreateWireReleaseCategories(WebRelease webDetails, int releaseId)
        {
            var repo = new Medianet.DataLayer.Repositories.ReleaseWireCategoryRepository(_uow);
            var rwc = new Medianet.Model.Entities.ReleaseWireCategory();
            rwc.CategoryId = Constants.DEFAULT_WIRE_CATEGORY;
            rwc.ReleaseId = releaseId;

            // Add all to the general category
            repo.Add(rwc);

            // also add to corresponding web category if its available.
            if (webDetails != null && webDetails.PrimaryWebCategoryId.HasValue)
            {
                var catsrepo = new Medianet.DataLayer.Repositories.WireReleaseCategoryRepository(_uow);
                var catlist = catsrepo.GetAll();

                if (catlist != null && catlist.Count > 0)
                {
                    // get the web category by ID
                    var webcatBL = new WebCategoryBL(_uow);
                    var webcat = webcatBL.Get((int)webDetails.PrimaryWebCategoryId);

                    if (webcat != null)
                    {
                        // try and find the matching web category in our wire categories.
                        var cat = catlist.SingleOrDefault(c => c.Name.ToUpper() == webcat.Name.ToUpper());
                        if (cat != null)
                        {
                            rwc = new Medianet.Model.Entities.ReleaseWireCategory();
                            rwc.CategoryId = cat.Id;
                            rwc.ReleaseId = releaseId;
                            repo.Add(rwc);
                        }
                    }
                }
            }
        }

        private void DeleteRelease(int releaseId)
        {

            try
            {
                var repo = new TransactionRepository(_uow);
                repo.DeleteByReleaseId(releaseId);
            }
            catch { }

            try
            {
                var repo = new DocumentRepository(_uow);
                repo.DeleteAllByReleaseId(releaseId);
            }
            catch { }

            try
            {
                var repo = new WebReleaseRepository(_uow);
                repo.Delete(releaseId);
            }
            catch { }

            try
            {
                var releaseRepo = new ReleaseRepository(_uow);
                var historyRepo = new ReleaseRepository(_uow);
                historyRepo.Delete(releaseId);
                releaseRepo.Delete(releaseId);
            }
            catch { }
        }

        private string GetHtmlContent(Model.Entities.Document doc, string securityKey)
        {
            string file = PathHelper.GetDocumentFilePathAndName(doc.Id, doc.FileExtension);
            var urlHelper = new UrlHelper();

            return HtmlHelper.GetHtmlContent(file, doc.ReleaseId.Value, doc.SequenceNumber, securityKey, urlHelper.DocumentDownloadUrl);
        }

        private List<string> StatusListReleased
        {
            get
            {
                if (_statusListReleased == null)
                    _statusListReleased = new List<string> {((char)ReleaseStatusType.Invoiced).ToString(),
                                             ((char)ReleaseStatusType.Accounts).ToString(),
                                             ((char)ReleaseStatusType.Resulted).ToString(),
                                             ((char)ReleaseStatusType.Transmitted).ToString()};
                return _statusListReleased;
            }
        }

        private List<string> StatusListAvailable
        {
            get
            {
                if (_statusListAvailable == null)
                    _statusListAvailable = new List<string> {((char)ReleaseStatusType.Invoiced).ToString(),
                                             ((char)ReleaseStatusType.Accounts).ToString(),
                                             ((char)ReleaseStatusType.Resulted).ToString(),
                                             ((char)ReleaseStatusType.Transmitted).ToString(),
                                             ((char)ReleaseStatusType.Released).ToString(),
                                             ((char)ReleaseStatusType.Embargoed).ToString(),
                                             ((char)ReleaseStatusType.Cancelled).ToString(),
                                             ((char)ReleaseStatusType.OnHold).ToString()};
                return _statusListAvailable;
            }
        }

        private List<ThumbnailPublic> GetThumbnailsPublic(List<Model.Entities.Thumbnail> thumbnails, UrlHelper urlHelper)
        {
            var thumbList = new List<ThumbnailPublic>();

            if (thumbnails != null)
            {
                foreach (var t in thumbnails)
                {
                    var thumb = new ThumbnailPublic();
                    thumb.Url = urlHelper.GetImageThumbnailUrl(t.Id);
                    thumb.Width = t.Width;
                    thumb.Height = t.Height;

                    thumbList.Add(thumb);
                }
            }

            return thumbList;
        }

        private ThumbnailPublic GetWieckReleaseThumbnailPublic(string wieckReference, int? height, int? width, UrlHelper urlHelper)
        {
            ThumbnailPublic thumbnail = null;

            if (!string.IsNullOrWhiteSpace(wieckReference) && height.HasValue && width.HasValue && height > 0 && width > 0)
            {
                thumbnail = new ThumbnailPublic();
                var size = urlHelper.GetThumbnailSize(height.Value, width.Value);

                thumbnail.Url = urlHelper.GetWieckReleaseThumbnailUrl(wieckReference, size.Height, size.Width);
                thumbnail.Width = size.Width;
                thumbnail.Height = size.Height;
            }

            return thumbnail;
        }

        private List<ThumbnailPublic> GetWieckThumbnailsPublic(string wieckReference, string mediaReference, int? height, int? width, string contentType, UrlHelper urlHelper)
        {
            var thumbList = new List<ThumbnailPublic>();

            if (!string.IsNullOrWhiteSpace(wieckReference) && height.HasValue && width.HasValue && height > 0 && width > 0)
            {
                var thumb = new ThumbnailPublic();
                var size = urlHelper.GetThumbnailSize(height.Value, width.Value);

                thumb.Url = urlHelper.GetWieckImageThumbnailUrl(wieckReference, mediaReference, size.Height, size.Width, contentType);
                thumb.Width = size.Width;
                thumb.Height = size.Height;

                thumbList.Add(thumb);
            }

            return thumbList;
        }

        #endregion

        #region Get MNJ Profile releases

        public ReleasePagePublic GetWebReleaseByProfileId(int profileId, int itemsPerPage, int currentPage)
        {
            var repository = new WebReleaseRepository(_uow);

            var list = repository.GetWebReleaseByProfileId(profileId, itemsPerPage, currentPage);

            // Map the database object to a friendly representation of the data.
            ReleasePagePublic result = new ReleasePagePublic
            {
                Releases = AutoMapper.Mapper.Map<List<Model.Entities.MnjProfileReleases>, List<ReleaseSummaryPublic>>(list),
                TotalCount = list == null || !list.Any() ? 0 : list.First().TotalCount
            };

            var urlHelper = new UrlHelper();
            var thumbnailBL = new ThumbnailBL(_uow);

            foreach (var entity in list.Where(m => m.PhotoDocId > 0))
            {
                var thumbnail = thumbnailBL.GetAllByDocumentId(entity.PhotoDocId).FirstOrDefault();

                if (thumbnail != null)
                    result.Releases.Find(m => m.Id == entity.JobId).Thumbnail = new ThumbnailPublic
                    {
                        Url = urlHelper.GetImageThumbnailUrl(thumbnail.Id)
                    };
            }

            return result;
        }

        #endregion
    }
}
