﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class ReleaseUnverifiedPreviewBL
    {
        private UnitOfWork _uow;

        public ReleaseUnverifiedPreviewBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public ReleaseUnverifiedPreview Get(Guid token)
        {
            var repo = new ReleaseUnverifiedPreviewRepository(_uow);
            Model.Entities.ReleaseUnverifiedPreview dbEntity;

            dbEntity = repo.Get(token);
            return AutoMapper.Mapper.Map<Model.Entities.ReleaseUnverifiedPreview, ReleaseUnverifiedPreview>(dbEntity);
        }

        public void Add(ReleaseUnverifiedPreview entity)
        {
            var dbEntity = AutoMapper.Mapper.Map<ReleaseUnverifiedPreview, Model.Entities.ReleaseUnverifiedPreview>(entity);
            var repo = new ReleaseUnverifiedPreviewRepository(_uow);
            repo.Add(dbEntity);
        }
    }
}
