﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class ResultBL
    {
        private UnitOfWork _uow;

        public ResultBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public List<Result> GetAllByReleaseIdTransactionId(int releaseId, int transactionId)
        {
            var repo = new ResultRepository(_uow);
            List<Model.Entities.Result> dbEntities;

            dbEntities = repo.GetAllByReleaseIdTransactionId(releaseId, transactionId);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.Result>, List<Result>>(dbEntities);
        }

        public void UpdateOpenCount(int releaseId, int transactionId)
        {
            var repo = new ResultRepository(_uow);
            repo.UpdateOpenCount(releaseId, transactionId);
        }

        public void UpdateOpenCountByDistributionId(int releaseId, int transactionId, int distributionId)
        {
            var repo = new ResultRepository(_uow);
            repo.UpdateOpenCountByDistributionId(releaseId, transactionId, distributionId);
        }
        
        public bool UpdateStatus(Result entity)
        {
            Model.Entities.Result model = AutoMapper.Mapper.Map<Result, Model.Entities.Result>(entity);

            return new ResultRepository(_uow).UpdateStatus(model);
        }
    }
}
