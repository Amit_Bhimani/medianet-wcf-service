﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Model.Entities;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class RssLogBL
    {
        private UnitOfWork _uow;

        public RssLogBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public int Add(Medianet.Service.Dto.RssLog entity)
        {
            var dbEntity = AutoMapper.Mapper.Map<Medianet.Service.Dto.RssLog,Medianet.Model.Entities.RssLog>(entity);
            var repo = new RssLogRepository(_uow);

            repo.Add(dbEntity);

            return dbEntity.Id;
        }
    }
}
