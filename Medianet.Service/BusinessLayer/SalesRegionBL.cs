﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class SalesRegionBL
    {
        private UnitOfWork _uow;

        public SalesRegionBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public SalesRegion Get(int id)
        {
            var repo = new SalesRegionRepository(_uow);
            Model.Entities.SalesRegion dbEntity;

            dbEntity = repo.GetById(id);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.SalesRegion, SalesRegion>(dbEntity);
        }
        public SalesRegion GetByName(string name)
        {
            var repo = new SalesRegionRepository(_uow);
            Model.Entities.SalesRegion dbEntity;

            dbEntity = repo.GetByName(name);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.SalesRegion, SalesRegion>(dbEntity);
        }

        public List<SalesRegion> GetAll()
        {
            var repo = new SalesRegionRepository(_uow);
            List<Model.Entities.SalesRegion> dbEntities;

            dbEntities = repo.GetAll();

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.SalesRegion>, List<SalesRegion>>(dbEntities);
        }

        public int Add(SalesRegion entity)
        {
            var dbEntity = AutoMapper.Mapper.Map<SalesRegion, Model.Entities.SalesRegion>(entity);
            var repo = new SalesRegionRepository(_uow);

            repo.Add(TruncateSalesRegion(dbEntity));

            return dbEntity.Id;
        }

        public void Update(SalesRegion entity)
        {
            var dbEntity = AutoMapper.Mapper.Map<SalesRegion, Model.Entities.SalesRegion>(entity);
            var repo = new SalesRegionRepository(_uow);

            repo.Update(TruncateSalesRegion(dbEntity));
        }

        public void Delete(int id)
        {
            var repo = new SalesRegionRepository(_uow);

            repo.Delete(id);
        }

        #region Private methods

        private Model.Entities.SalesRegion TruncateSalesRegion(Model.Entities.SalesRegion dbEntity)
        {
            dbEntity.Name = dbEntity.Name.Truncate(50);
            dbEntity.Name = dbEntity.SalesEmail.Truncate(200);

            return dbEntity;
        }

        #endregion
    }
}
