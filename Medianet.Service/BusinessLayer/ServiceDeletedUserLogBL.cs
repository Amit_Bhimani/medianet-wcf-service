﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class ServiceDeletedUserLogBL
    {
        private UnitOfWork _uow;

        public ServiceDeletedUserLogBL(UnitOfWork uow)
        {
            _uow = uow;
        }
        
        public List<ServiceDeletedUserLog> GetList(string outletId, string contactId)
        {
            List<Model.Entities.ServiceDeletedUserLogView> list = new ServiceDeletedUserLogRepository(_uow).GetList(outletId, contactId);

            var result = AutoMapper.Mapper.Map<List<Model.Entities.ServiceDeletedUserLogView>, List<ServiceDeletedUserLog>>(list);

            return result;
        }
    }
}
