﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.BusinessLayer.Common;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class ServiceListBL
    {
        private UnitOfWork _uow;

        public ServiceListBL(UnitOfWork uow) {
            _uow = uow;
        }

        public ServiceList Get(int serviceId) {
            var repo = new ServiceListRepository(_uow);
            Model.Entities.ServiceList dbEntity;

            dbEntity = repo.GetById(serviceId);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.ServiceList, ServiceList>(dbEntity);
        }

        public List<ServiceList> GetAll(string debtorNumber, Medianet.Service.Common.SystemType system) {
            var repo = new ServiceListRepository(_uow);
            List<Model.Entities.ServiceList> dbEntities;

            dbEntities = repo.GetAllByDebtorNumber(debtorNumber, ((char)system).ToString());

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.ServiceList>, List<ServiceList>>(dbEntities);
        }

        public List<ServiceList> GetAllByDebtorNumberOutletIdContactId(string debtorNumber, string outletId, string contactId, Medianet.Service.Common.SystemType system)
        {
            var repo = new ServiceListRepository(_uow);
            List<Model.Entities.ServiceList> dbEntities;

            dbEntities = repo.GetAllByDebtorNumberOutletIdContactId(debtorNumber, outletId, contactId, ((char)system).ToString());

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.ServiceList>, List<ServiceList>>(dbEntities);
            
        }

        public List<ServiceListSummary> GetAll(string debtorNumber, DistributionType distType, Medianet.Service.Common.SystemType system, int startPos, int pageSize)
        {
            var repo = new ServiceListRepository(_uow);
            List<Model.Entities.ServiceListSummary> dbEntities;

            dbEntities = repo.GetAllByDebtorNumberDistributionType(debtorNumber, ((char)distType).ToString(), ((char)system).ToString(), startPos, pageSize);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.ServiceListSummary>, List<ServiceListSummary>>(dbEntities);
        }

        public List<ServiceList> GetAllByDebtorNumberAccountCode(string debtorNumber, string accountCode)
        {
            var repo = new ServiceListRepository(_uow);
            List<Model.Entities.ServiceList> dbEntities = null;

            // Temporary hack. If the account code is S1234 then 1234 is the Id of the service, not the accountcode.
            // Eventually the accouncodes will be changed to S1234 for existing, and new ones will be allocated from SalesForce.
            if (accountCode.StartsWith("S"))
            {
                int id;

                if (int.TryParse(accountCode.Substring(1), out id))
                {
                    var s = repo.GetById(id);
                    if (s != null)
                    {
                        dbEntities = new List<Model.Entities.ServiceList>();
                        dbEntities.Add(s);
                    }
                }
            }

            if (dbEntities == null)
                dbEntities = repo.GetAllByDebtorNumberAccountCode(debtorNumber, accountCode);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.ServiceList>, List<ServiceList>>(dbEntities);
        }

        public int GetCountByDebtorNumberDistributionType(string debtorNumber, DistributionType distType, Medianet.Service.Common.SystemType system)
        {
            var repo = new ServiceListRepository(_uow);

            return repo.GetCountByDebtorNumberDistributionType(debtorNumber, ((char)distType).ToString(), ((char)system).ToString());
        }

        public List<ServiceList> GetAllByServiceIds(string serviceIds)
        {
            var repo = new ServiceListRepository(_uow);
            List<Model.Entities.ServiceList> dbEntities;

            dbEntities = repo.GetAllByServiceIds(serviceIds);

            return AutoMapper.Mapper.Map<List<Model.Entities.ServiceList>, List<ServiceList>>(dbEntities);
        }

        public List<ListTicker> GetForTicker() {
            var repo = new ServiceListRepository(_uow);
            List<Model.Entities.ListTicker> dbEntities;

            dbEntities = repo.GetTickerByDebtorNumber(Common.Constants.DEBTOR_NUMBER_AAP, ((char)SystemType.Medianet).ToString());

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.ListTicker>, List<ListTicker>>(dbEntities);
        }

        public List<Recipient> GetRecipients(int serviceId) {
            var repo = new ServiceListRepository(_uow);
            List<Model.Entities.Recipient> dbEntities;

            dbEntities = repo.GetRecipientsById(serviceId);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.Recipient>, List<Recipient>>(dbEntities);
        }

        public List<String> GetRecipientOutletNames(int serviceId) {
            var repo = new ServiceListRepository(_uow);

            return GetOutletsRecursive(serviceId, 0, repo);
        }

        public ListComment GetComment(int serviceId) {
            var repo = new ServiceListRepository(_uow);
            Model.Entities.ListComment dbEntity;

            dbEntity = repo.GetCommentById(serviceId);

            if (dbEntity != null && dbEntity.Comment != null)
                dbEntity.Comment = dbEntity.Comment.Replace("\r\n", "<br />");

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.ListComment, ListComment>(dbEntity);
        }

        public List<TopPerformingList> GetTopPerformingLists(PeriodType period, int maxRows)
        {
            var repo = new TopPerformingListRepository(_uow);
            List<Model.Entities.TopPerformingList> dbEntities;

            dbEntities = repo.GetAllByPeriod(((char)period).ToString(), maxRows);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.TopPerformingList>, List<TopPerformingList>>(dbEntities);
        }

        public List<ServiceList> SearchAapListsByOutletId(string outletId)
        {
            var repo = new ServiceListRepository(_uow);
            List<Model.Entities.ServiceList> dbEntities;

            dbEntities = repo.SearchAapListsByOutletId(outletId, Constants.DEBTOR_NUMBER_AAP);

            return AutoMapper.Mapper.Map<List<Model.Entities.ServiceList>, List<ServiceList>>(dbEntities);
        }

        public int Add(ServiceList entity, int userId) {
            var dbEntity = AutoMapper.Mapper.Map<ServiceList, Model.Entities.ServiceList>(entity);
            var repo = new ServiceListRepository(_uow);

            dbEntity.CreatedDate = DateTime.Now;
            dbEntity.CreatedByUserId = userId;
            dbEntity.LastModifiedDate = dbEntity.CreatedDate;
            dbEntity.LastModifiedByUserId = userId;
            dbEntity = TruncateServiceList(dbEntity);

            repo.Add(dbEntity);
            repo.UpdateInNopCommerce(entity.Id);

            return dbEntity.Id;
        }

        public int AddWithRecipients(ServiceList entity, List<Recipient> recipients, int userId) {
            var dbService = AutoMapper.Mapper.Map<ServiceList, Model.Entities.ServiceList>(entity);
            var dbRecipients = AutoMapper.Mapper.Map<List<Recipient>, List<Model.Entities.Recipient>>(recipients);
            int sequenceNumber = 1;

            var repo = new ServiceListRepository(_uow);

            dbService.ListNumber = repo.GetNextListNumber(dbService.DebtorNumber, dbService.DistributionType, dbService.System);
            dbService.SequenceNumber = repo.GetNextSequenceNumber(dbService.DebtorNumber, dbService.DistributionType, dbService.System);
            dbService.CreatedDate = DateTime.Now;
            dbService.CreatedByUserId = userId;
            dbService.LastModifiedDate = dbService.CreatedDate;
            dbService.LastModifiedByUserId = userId;


            dbService.Id = repo.Add(TruncateServiceList(dbService));

            foreach (var recip in dbRecipients) {
                AddRecipientAndServiceListDistribution(dbService.Id, recip, sequenceNumber, repo);
                sequenceNumber += 1;
            }

            return dbService.Id;
        }

        public int AddRecipient(int serviceId, Recipient recipient, int userId) {
            Model.Entities.ServiceList dbService;
            var dbRecipient = AutoMapper.Mapper.Map<Recipient, Model.Entities.Recipient>(recipient);
            int sequenceNumber;

            var repo = new ServiceListRepository(_uow);
            dbService = repo.GetById(serviceId);

            sequenceNumber = repo.GetNextSequenceNumber(serviceId);
            dbRecipient.Id = AddRecipientAndServiceListDistribution(serviceId, dbRecipient, sequenceNumber, repo);

            dbService.LastModifiedDate = DateTime.Now;
            dbService.LastModifiedByUserId = userId;

            if (dbService.ShouldUpdateAddresses)
                dbService.AddressCount += 1; // = repo.GetAddressCount(serviceId);

            repo.Update(dbService);

            return dbRecipient.Id;
        }

        public void Update(ServiceList entity, int userId) {
            var dbEntity = AutoMapper.Mapper.Map<ServiceList, Model.Entities.ServiceList>(entity);
            var repo = new ServiceListRepository(_uow);

            dbEntity.LastModifiedDate = DateTime.Now;
            dbEntity.LastModifiedByUserId = userId;
            dbEntity = TruncateServiceList(dbEntity);

            repo.Update(dbEntity);
            _uow.Save();
            repo.UpdateInNopCommerce(entity.Id);
        }

        public void UpdateRecipient(int serviceId, Recipient recipient, int userId) {
            //Model.Entities.ServiceList dbService;
            var dbRecipient = AutoMapper.Mapper.Map<Recipient, Model.Entities.Recipient>(recipient);

            var repo = new ServiceListRepository(_uow);

            repo.UpdateRecipient(TruncateRecipient(dbRecipient));

            //dbService = repo.GetById(serviceId);
            //dbService.LastModifiedDate = DateTime.Now;
            //dbService.LastModifiedByUserId = userId;
            //repo.Update(dbService);
        }

        public void UpdateAllRecipients(int serviceId, List<Recipient> recipients, int userId) {
            Model.Entities.ServiceList dbService;
            var dbRecipients = AutoMapper.Mapper.Map<List<Recipient>, List<Model.Entities.Recipient>>(recipients);
            var repo = new ServiceListRepository(_uow);
            int sequenceNumber = 1;

            repo.DeleteRecipients(serviceId);

            foreach (var recip in dbRecipients) {
                AddRecipientAndServiceListDistribution(serviceId, recip, sequenceNumber, repo);
                sequenceNumber += 1;
            }

            dbService = repo.GetById(serviceId);
            dbService.LastModifiedDate = DateTime.Now;
            dbService.LastModifiedByUserId = userId;

            if (dbService.ShouldUpdateAddresses)
                dbService.AddressCount = (short)dbRecipients.Count();

            repo.Update(dbService);
        }

        public void Delete(int serviceId, int userId) {
            var repo = new ServiceListRepository(_uow);

            repo.DeleteRecipients(serviceId);
            repo.Delete(serviceId);
        }

        public void DeleteRecipient(int serviceId, int recipientId, int userId) {
            var repo = new ServiceListRepository(_uow);
            Model.Entities.ServiceList dbService;

            repo.DeleteRecipient(serviceId, recipientId);

            dbService = repo.GetById(serviceId);
            dbService.LastModifiedDate = DateTime.Now;
            dbService.LastModifiedByUserId = userId;

            if (dbService.ShouldUpdateAddresses)
                dbService.AddressCount -= 1;

            repo.Update(dbService);
        }

        public int GetNextListNumber(string debtorNumber, DistributionType distType, SystemType system) {
            var repo = new ServiceListRepository(_uow);
            return repo.GetNextListNumber(debtorNumber, ((char)distType).ToString(), ((char)system).ToString());
        }

        public bool IsListNumberUnique(string debtorNumber, DistributionType distType, int listNumber, SystemType system) {
            var repo = new ServiceListRepository(_uow);
            Model.Entities.ServiceList duplicate = repo.GetByDebtorNumberDistributionTypeListNumber(debtorNumber, ((char)distType).ToString(), listNumber, ((char)system).ToString());
            return (duplicate == null);
        }

        public bool IsListInUse(int serviceId, string debtorNumber) {
            List<string> statusList = new List<string> {((char)ReleaseStatusType.New).ToString(),
                                                        ((char)ReleaseStatusType.Converting).ToString(),
                                                        ((char)ReleaseStatusType.Processing).ToString(),
                                                        ((char)ReleaseStatusType.OnHold).ToString(),
                                                        ((char)ReleaseStatusType.MailMerging).ToString(),
                                                        ((char)ReleaseStatusType.Released).ToString()};
            var repo = new ServiceListRepository(_uow);
            return repo.IsInUseByStatus(serviceId, debtorNumber, statusList);
        }

        public int GetAddressCount(int serviceId) {
            var repo = new ServiceListRepository(_uow);
            return repo.GetAddressCount(serviceId);

        }

        public static string GetAccountCode(string debtorNumber, DistributionType distType, SystemType system) {
            if (debtorNumber == Constants.DEBTOR_NUMBER_AAP) {
                switch (distType) {
                    case DistributionType.Email:
                        return Constants.ACCOUNTCODE_MNET_AAPEMAIL;
                    case DistributionType.SMS:
                        return Constants.ACCOUNTCODE_MNET_AAPSMS;
                    case DistributionType.Voice:
                        return Constants.ACCOUNTCODE_MNET_AAPVOICE;
                }
            }
            else {
                if (system == SystemType.MessageConnect) {
                    switch (distType) {
                        case DistributionType.Email:
                            return Constants.ACCOUNTCODE_MSGCON_CUSTEMAIL;
                        case DistributionType.SMS:
                            return Constants.ACCOUNTCODE_MSGCON_CUSTSMS;
                        case DistributionType.Voice:
                            return Constants.ACCOUNTCODE_MSGCON_CUSTVOICE;
                    }
                }
                else {
                    switch (distType) {
                        case DistributionType.Email:
                            return Constants.ACCOUNTCODE_MNET_CUSTEMAIL;
                        case DistributionType.SMS:
                            return Constants.ACCOUNTCODE_MNET_CUSTSMS;
                        case DistributionType.Voice:
                            return Constants.ACCOUNTCODE_MNET_CUSTVOICE;
                    }
                }
            }

            throw new Exception("Unable to work out the AccountCode.");
        }

        #region Private methods

        private int AddRecipientAndServiceListDistribution(int serviceId, Model.Entities.Recipient dbRecipient, int sequenceNumber, ServiceListRepository repo) {
            var dbDistribution = new Model.Entities.ServiceListDistribution();

            dbRecipient.Id = repo.AddRecipient(TruncateRecipient(dbRecipient));

            dbDistribution.ServiceId = serviceId;
            dbDistribution.RecipientId = dbRecipient.Id;
            dbDistribution.SequenceNumber = sequenceNumber;

            repo.AddServiceListDistribution(dbDistribution);

            return dbRecipient.Id;
        }

        private List<string> GetOutletsRecursive(int serviceId, int depth, ServiceListRepository repo) {
            List<string> outlets = new List<string>();
            List<int> serviceList = null;
            Model.Entities.ServiceList sList;
            DistributionType distType;

            if (depth > 10) throw new Exception("Depth of Service is greater than 10 and can not be resolved.");

            sList = repo.GetById(serviceId);

            if (sList == null)
                throw new KeyNotFoundException(string.Format("Service with Id {0} not found.", serviceId));

            distType = ExtensionMethods.ToEnum<DistributionType>(sList.DistributionType);

            if (sList.DebtorNumber == Constants.DEBTOR_NUMBER_AAP && distType == DistributionType.Email)
            {
                // Get all the Outlet names for this service.
                outlets.AddRange(repo.GetAllOutletNamesById(serviceId));
            }
            else if (distType == DistributionType.Wire) {
                // Get all the ListDistributions for this service.
                outlets.AddRange(repo.GetAllListDistributionsById(serviceId));
            }
            else {
                // Get all the Recipients for this service.
                outlets.AddRange(repo.GetAllRecipientCompaniesById(serviceId));
            }

            // Now fetch all the Services that are a child of this Service and fetch their Outlets.
            serviceList = repo.GetAllChildServiceListsById(serviceId);

            foreach (int s in serviceList) {
                outlets.AddRange(GetOutletsRecursive(s, depth + 1, repo));
            }

            return outlets;
        }

        private Model.Entities.ServiceList TruncateServiceList(Model.Entities.ServiceList dbEntity) {
            dbEntity.SelectionDescription = dbEntity.SelectionDescription.Truncate(50);
            dbEntity.Introduction = dbEntity.Introduction.Truncate(255);
            dbEntity.Destinations = dbEntity.Destinations.Truncate(80);
            dbEntity.EditorialCategory = dbEntity.EditorialCategory.Truncate(10);
            
            return dbEntity;
        }

        private Model.Entities.Recipient TruncateRecipient(Model.Entities.Recipient dbEntity) {
            dbEntity.ReferenceId = dbEntity.ReferenceId.Truncate(24);
            dbEntity.Company = dbEntity.Company.Truncate(75);
            dbEntity.JobTitle = dbEntity.JobTitle.Truncate(50);
            dbEntity.Title = dbEntity.Title.Truncate(50);
            dbEntity.FirstName = dbEntity.FirstName.Truncate(50);
            dbEntity.MiddleName = dbEntity.MiddleName.Truncate(50);
            dbEntity.LastName = dbEntity.LastName.Truncate(50);
            dbEntity.PhoneNumber = dbEntity.PhoneNumber.Truncate(24);
            dbEntity.AddressLine1 = dbEntity.AddressLine1.Truncate(50);
            dbEntity.AddressLine2 = dbEntity.AddressLine2.Truncate(50);
            dbEntity.Address = dbEntity.Address.Replace("\n", "").Replace("\r", "").Trim().Truncate(120);

            return dbEntity;
        }

        #endregion

    }
}
