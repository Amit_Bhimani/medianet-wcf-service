﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class StateBL
    {
        private UnitOfWork _uow;

        public StateBL(UnitOfWork uow) {
            _uow = uow;
        }

        public State Get(int id) {
            var repo = new StateRepository(_uow);
            Model.Entities.State dbEntity;

            dbEntity = repo.GetById(id);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.State, State>(dbEntity);
        }

        public List<State> GetAll() {
            var repo = new StateRepository(_uow);
            List<Model.Entities.State> dbEntities;

            dbEntities = repo.GetAll();

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.State>, List<State>>(dbEntities);
        }
    }
}
