﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class StationFormatBL
    {
        private UnitOfWork _uow;

        public StationFormatBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public StationFormat Get(int id)
        {
            var repo = new StationFormatRepository(_uow);
            Model.Entities.StationFormat dbEntities;

            dbEntities = repo.Get(id);

            return AutoMapper.Mapper.Map<Model.Entities.StationFormat, StationFormat>(dbEntities);
        }

        public List<StationFormat> GetAll()
        {
            var repo = new StationFormatRepository(_uow);
            List<Model.Entities.StationFormat> dbEntities;

            dbEntities = repo.GetAll();

            return AutoMapper.Mapper.Map<List<Model.Entities.StationFormat>, List<StationFormat>>(dbEntities);
        }
    }
}
