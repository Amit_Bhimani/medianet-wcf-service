﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class SubjectBL
    {
        private UnitOfWork _uow;

        public SubjectBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public Subject Get(int id)
        {
            var repo = new SubjectRepository(_uow);
            Model.Entities.Subject dbEntities;

            dbEntities = repo.Get(id);

            return AutoMapper.Mapper.Map<Model.Entities.Subject, Subject>(dbEntities);
        }

        public List<Subject> GetAll()
        {
            var repo = new SubjectRepository(_uow);
            List<Model.Entities.Subject> dbEntities;

            dbEntities = repo.GetAll();

            return AutoMapper.Mapper.Map<List<Model.Entities.Subject>, List<Subject>>(dbEntities);
        }
    }
}
