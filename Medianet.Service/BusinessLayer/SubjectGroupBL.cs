﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class SubjectGroupBL
    {
        private UnitOfWork _uow;

        public SubjectGroupBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public SubjectGroup Get(int id)
        {
            var repo = new SubjectGroupRepository(_uow);
            Model.Entities.SubjectGroup dbEntities;

            dbEntities = repo.Get(id);

            return AutoMapper.Mapper.Map<Model.Entities.SubjectGroup, SubjectGroup>(dbEntities);
        }

        public List<SubjectGroup> GetAll()
        {
            var repo = new SubjectGroupRepository(_uow);
            List<Model.Entities.SubjectGroup> dbEntities;

            dbEntities = repo.GetAll();

            return AutoMapper.Mapper.Map<List<Model.Entities.SubjectGroup>, List<SubjectGroup>>(dbEntities);
        }
    }
}
