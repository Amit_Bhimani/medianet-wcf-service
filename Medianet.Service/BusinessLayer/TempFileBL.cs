﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class TempFileBL
    {
        private UnitOfWork _uow;

        public TempFileBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public TempFile Get(int id) {
            var repo = new TempFileRepository(_uow);
            Model.Entities.TempFile dbEntity;

            dbEntity = repo.GetById(id);

            return AutoMapper.Mapper.Map<Model.Entities.TempFile, TempFile>(dbEntity);
        }

        public List<TempFile> GetAllByParentId(int parentId) {
            var repo = new TempFileRepository(_uow);
            List<Model.Entities.TempFile> dbEntities;

            dbEntities = repo.GetAllByParentId(parentId);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.TempFile>, List<TempFile>>(dbEntities);
        }

        public int Add(TempFile entity) {
            var dbEntity = AutoMapper.Mapper.Map<TempFile, Model.Entities.TempFile>(entity);
            var repo = new TempFileRepository(_uow);

            return repo.Add(dbEntity);
        }

        public void Update(TempFile entity) {
            var dbEntity = AutoMapper.Mapper.Map<TempFile, Model.Entities.TempFile>(entity);
            var repo = new TempFileRepository(_uow);

            repo.Update(dbEntity);
        }

        public void Delete(int id) {
            var repo = new TempFileRepository(_uow);
            repo.Delete(id);
        }

    }
}
