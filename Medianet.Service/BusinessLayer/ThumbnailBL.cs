﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class ThumbnailBL
    {
        private UnitOfWork _uow;

        public ThumbnailBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public Thumbnail Get(int id) {
            var repo = new ThumbnailRepository(_uow);
            Model.Entities.Thumbnail dbEntity;

            dbEntity = repo.GetById(id);

            return AutoMapper.Mapper.Map<Model.Entities.Thumbnail, Thumbnail>(dbEntity);
        }

        public List<Thumbnail> GetAllByDocumentId(int documentId) {
            var repo = new ThumbnailRepository(_uow);
            List<Model.Entities.Thumbnail> dbEntities;

            dbEntities = repo.GetAllByDocumentId(documentId);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.Thumbnail>, List<Thumbnail>>(dbEntities);
        }

        public int Add(Thumbnail entity) {
            var dbEntity = AutoMapper.Mapper.Map<Thumbnail, Model.Entities.Thumbnail>(entity);
            var repo = new ThumbnailRepository(_uow);

            return repo.Add(dbEntity);
        }

        public void Update(Thumbnail entity) {
            var dbEntity = AutoMapper.Mapper.Map<Thumbnail, Model.Entities.Thumbnail>(entity);
            var repo = new ThumbnailRepository(_uow);

            repo.Update(dbEntity);
        }

        public void Delete(int id) {
            var repo = new ThumbnailRepository(_uow);
            repo.Delete(id);
        }

    }
}
