﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class TimezoneBL
    {
        private UnitOfWork _uow;

        public TimezoneBL(UnitOfWork uow) {
            _uow = uow;
        }

        public Timezone Get(string code) {
            var repo = new TimezoneRepository(_uow);
            Model.Entities.Timezone dbEntity;

            dbEntity = repo.GetByCode(code);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.Timezone, Timezone>(dbEntity);
        }

        public List<Timezone> GetAll() {
            var repo = new TimezoneRepository(_uow);
            List<Model.Entities.Timezone> dbEntities;

            dbEntities = repo.GetAll();

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.Timezone>, List<Timezone>>(dbEntities);
        }

        public TimeZoneInfo GetTimezoneInfo(string code) {
            var repo = new TimezoneRepository(_uow);
            Model.Entities.Timezone dbEntity;

            dbEntity = repo.GetByCode(code);

            return TimeZoneInfo.FindSystemTimeZoneById(dbEntity.Description);
        }
    }
}
