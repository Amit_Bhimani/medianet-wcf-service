﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
//using Medianet.Model.Entities;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class TrainingCourseBL
    {
        private UnitOfWork _uow;

        public TrainingCourseBL(UnitOfWork uow) {
            _uow = uow;
        }

        public TrainingCourse Get(string id)
        {
            var repo = new TrainingCourseRepository(_uow);
            Model.Entities.TrainingCourse dbEntity;

            dbEntity = repo.GetById(id);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.TrainingCourse, TrainingCourse>(dbEntity);
        }

        public List<TrainingCourse> GetAll()
        {
            var repo = new TrainingCourseRepository(_uow);
            List<Model.Entities.TrainingCourse> dbEntities;

            dbEntities = repo.GetAll();

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.TrainingCourse>, List<TrainingCourse>>(dbEntities);
        }

        public TrainingCourseSchedule GetSchedule(int scheduleId)
        {
            var repo = new TrainingCourseScheduleRepository(_uow);
            Model.Entities.TrainingCourseSchedule dbEntity;

            dbEntity = repo.GetById(scheduleId);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.TrainingCourseSchedule, TrainingCourseSchedule>(dbEntity);
        }

        public void RegisterForSchedule(TrainingCourseEnrolment enrolment)
        {
            var repo = new TrainingCourseEnrolmentRepository(_uow);
            Model.Entities.TrainingCourseEnrolment dbEntity;

            dbEntity = AutoMapper.Mapper.Map<TrainingCourseEnrolment, Model.Entities.TrainingCourseEnrolment>(enrolment);

            repo.Add(dbEntity);
        }
    }
}
