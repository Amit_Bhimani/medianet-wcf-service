﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class TransactionBL
    {
        private UnitOfWork _uow;

        public TransactionBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public Transaction Get(int id)
        {
            var repo = new TransactionRepository(_uow);
            Model.Entities.Transaction dbEntity;

            dbEntity = repo.GetById(id);

            return AutoMapper.Mapper.Map<Model.Entities.Transaction, Transaction>(dbEntity);
        }

        public List<Transaction> GetAllByReleaseId(int releaseId)
        {
            var repo = new TransactionRepository(_uow);
            List<Model.Entities.Transaction> dbEntities;

            dbEntities = repo.GetAllByReleaseId(releaseId);

            return AutoMapper.Mapper.Map<List<Model.Entities.Transaction>, List<Transaction>>(dbEntities);
        }


        public List<TransactionStatistics> GetAllStatisticsByReleaseId(int releaseId)
        {
            var repo = new TransactionRepository(_uow);
            List<Model.Entities.TransactionStatistics> dbEntities;

            dbEntities = repo.GetAllStatisticsByReleaseId(releaseId);

            return AutoMapper.Mapper.Map<List<Model.Entities.TransactionStatistics>, List<TransactionStatistics>>(dbEntities);
        }

        public int Add(Transaction entity, int userId)
        {
            var dbEntity = AutoMapper.Mapper.Map<Transaction, Model.Entities.Transaction>(entity);
            var repo = new TransactionRepository(_uow);

            dbEntity.CreatedDate = DateTime.Now;
            dbEntity.CreatedByUserId = userId;
            dbEntity.LastModifiedDate = dbEntity.CreatedDate;
            dbEntity.LastModifiedByUserId = userId;

            repo.Add(dbEntity);

            return dbEntity.Id;
        }

        public void DeleteByReleaseId(int releaseId)
        {
            var repo = new TransactionRepository(_uow);

            repo.DeleteByReleaseId(releaseId);
        }
    }
}
