﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class TwitterCategoryBL
    {
        private UnitOfWork _uow;

        public TwitterCategoryBL(UnitOfWork uow) {
            _uow = uow;
        }

        public TwitterCategory Get(int id) {
            var repo = new TwitterCategoryRepository(_uow);
            Model.Entities.TwitterCategory dbEntity;

            dbEntity = repo.GetById(id);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.TwitterCategory, TwitterCategory>(dbEntity);
        }

        public List<TwitterCategory> GetAll() {
            var repo = new TwitterCategoryRepository(_uow);
            List<Model.Entities.TwitterCategory> dbEntities;

            dbEntities = repo.GetAll().Where(c => !c.IsDeleted).ToList();

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.TwitterCategory>, List<TwitterCategory>>(dbEntities);
        }
    }
}
