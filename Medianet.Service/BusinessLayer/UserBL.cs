﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class UserBL
    {
        private UnitOfWork _uow;

        public UserBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public User Get(int id)
        {
            var repo = new UserRepository(_uow);
            Model.Entities.User dbEntity;

            dbEntity = repo.GetById(id);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.User, User>(dbEntity);
        }

        public List<User> GetAllByDebtorNumber(string debtorNumber)
        {
            var repo = new UserRepository(_uow);
            List<Model.Entities.User> dbEntities;

            dbEntities = repo.GetAllByDebtorNumber(debtorNumber);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.User>, List<User>>(dbEntities);
        }

        public int? GetUserIdByUnsubscribeKey(Guid unsubscribeKey)
        {
            var userId = new UserRepository(_uow).GetUserIdByUnsubscribeKey(unsubscribeKey);

            return userId;
        }

        public List<User> GetAllByLogonDetails(string userName, string companyName, SystemType system)
        {
            var repo = new UserRepository(_uow);
            List<Model.Entities.User> dbEntities;

            // Journalists, admin website and MNUser don't need a company logon
            if (system == SystemType.Admin)
                dbEntities = repo.GetAllByLogonName(userName).Where(u => u.HasAdminWebAccess).ToList();
            else if (system == SystemType.MNUser)
                dbEntities = repo.GetAllByLogonName(userName).Where(u => u.MNUserAccessRights.Trim().Length > 0).ToList();
            else if (system == SystemType.Journalists)
                dbEntities = repo.GetAllByLogonName(userName).Where(u => u.HasJournalistsWebAccess).ToList();
            else
                dbEntities = repo.GetAllByLogonDetails(userName, companyName);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.User>, List<User>>(dbEntities);
        }

        public bool IsMnjLogonNameUnique(string logonName)
        {
            var isUnique = new UserRepository(_uow).IsMnjLogonNameUnique(logonName);

            return isUnique;
        }

        public List<User> GetAllByEmailAddress(string emailAddress)
        {
            var repo = new UserRepository(_uow);
            List<Model.Entities.User> dbEntities;

            dbEntities = repo.GetAllByEmailAddress(emailAddress);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.User>, List<User>>(dbEntities);
        }

        public int? GetUserIdByContactId(string contactId)
        {
            var userId = new UserRepository(_uow).GetUserByContactId(contactId)?.Id;

            return userId;
        }

        public User GetUserByContactId(string contactId)
        {
            var user = new UserRepository(_uow).GetUserByContactId(contactId);

            return AutoMapper.Mapper.Map<Model.Entities.User, User>(user);
        }

        public int Add(User entity)
        {
            Model.Entities.User dbEntity;
            var repo = new UserRepository(_uow);

            // Truncate everything to the correct length.
            entity.Truncate();

            dbEntity = AutoMapper.Mapper.Map<User, Model.Entities.User>(entity);
            dbEntity.CreatedDate = DateTime.Now;
            dbEntity.LastModifiedDate = dbEntity.CreatedDate;

            repo.Add(dbEntity);

            // Now set the created and modified UserId to the new Id.
            dbEntity.CreatedByUserId = dbEntity.Id;
            dbEntity.LastModifiedByUserId = dbEntity.Id;
            repo.Update(dbEntity);

            return dbEntity.Id;
        }

        public int Add(User entity, int userId)
        {
            Model.Entities.User dbEntity;
            var repo = new UserRepository(_uow);

            // Truncate everything to the correct length.
            entity.Truncate();

            dbEntity = AutoMapper.Mapper.Map<User, Model.Entities.User>(entity);
            dbEntity.CreatedDate = DateTime.Now;
            dbEntity.CreatedByUserId = userId;
            dbEntity.LastModifiedDate = dbEntity.CreatedDate;
            dbEntity.LastModifiedByUserId = userId;

            repo.Add(dbEntity);

            return dbEntity.Id;
        }

        public void Update(User entity, int userId)
        {
            Model.Entities.User dbEntity;
            var repo = new UserRepository(_uow);

            // Truncate everything to the correct length.
            entity.Truncate();

            dbEntity = AutoMapper.Mapper.Map<User, Model.Entities.User>(entity);
            dbEntity.LastModifiedDate = DateTime.Now;
            dbEntity.LastModifiedByUserId = userId;

            repo.Update(dbEntity);
        }

        public void UpdateLogonAttempt(int id, DateTime? logonDate, SystemType system, bool logonSucceeded)
        {
            var repo = new UserRepository(_uow);
            var dbEntity = repo.GetById(id);

            if (dbEntity != null)
            {
                if (logonSucceeded)
                {
                    dbEntity.LogonFailureCount = 0;
                    dbEntity.LastLogonDate = logonDate.HasValue ? logonDate.Value : DateTime.Now;
                    dbEntity.RecentLogonCount += 1;
                }
                else
                {
                    var envRepo = new AAPEnvironmentRepository(_uow);
                    Model.Entities.AAPEnvironment envValue;
                    int warnLogonFailureCount = 0, inactivateLogonFailureCount = 0;

                    // Get the WarnLogonFailures number.
                    envValue = envRepo.GetByType("WarnLogonFailures");
                    if (envValue != null)
                        int.TryParse(envValue.Value, out warnLogonFailureCount);

                    // Get the InactivateLogonFailures number.
                    envValue = envRepo.GetByType("InactivateLogonFailures");
                    if (envValue != null)
                        int.TryParse(envValue.Value, out inactivateLogonFailureCount);

                    dbEntity.LogonFailureCount += 1;

                    if (inactivateLogonFailureCount > 0 && dbEntity.LogonFailureCount >= inactivateLogonFailureCount)
                    {
                        EmailHelper.LogonFailureDisableEmail(string.Format("{0} {1}", dbEntity.FirstName, dbEntity.LastName).Trim(), "", dbEntity.DebtorNumber, system.ToString(), dbEntity.LogonFailureCount);
                        dbEntity.RowStatus = ((char)RowStatusType.Inactive).ToString();
                    }
                    else if (warnLogonFailureCount > 0 && dbEntity.LogonFailureCount >= warnLogonFailureCount)
                    {
                        EmailHelper.LogonFailureWarningEmail(string.Format("{0} {1}", dbEntity.FirstName, dbEntity.LastName).Trim(), "", dbEntity.DebtorNumber, system.ToString(), dbEntity.LogonFailureCount);
                    }
                }

                repo.Update(dbEntity);
            }
        }

        public void Delete(int id, int userId)
        {
            var repo = new UserRepository(_uow);
            var dbEntity = repo.GetById(id);

            if (dbEntity != null)
            {
                dbEntity.RowStatus = ((char)RowStatusType.Deleted).ToString();
                dbEntity.LastModifiedDate = DateTime.Now;
                dbEntity.LastModifiedByUserId = userId;
                repo.Update(dbEntity);
            }
        }

        public void AcknowledgeMessageRead(int id)
        {
            var repo = new UserRepository(_uow);

            repo.AcknowledgeMessageRead(id);
        }

        private const int GenerationMNJLogonName_MaxEfford = 3;
        private int GenerationMNJLogonName_Counter = 1;

        public string GenerateUniqueMNJLogonName(string fullName)
        {
            int value = new Random().Next(100, 999);
            string logonName = fullName.Truncate(13) + value;

            if (IsMnjLogonNameUnique(logonName))
                return logonName;

            else if (GenerationMNJLogonName_Counter == GenerationMNJLogonName_MaxEfford)
                return null;
            else
            {
                GenerationMNJLogonName_Counter++;

                return GenerateUniqueMNJLogonName(fullName);
            }
        }
    }
}
