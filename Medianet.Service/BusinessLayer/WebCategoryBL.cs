﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class WebCategoryBL
    {
        private UnitOfWork _uow;

        public WebCategoryBL(UnitOfWork uow) {
            _uow = uow;
        }

        public WebCategory Get(int id) {
            var repo = new WebCategoryRepository(_uow);
            Model.Entities.WebCategory dbEntity;

            dbEntity = repo.GetById(id);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.WebCategory, WebCategory>(dbEntity);
        }

        public List<WebCategory> GetAll() {
            var repo = new WebCategoryRepository(_uow);
            List<Model.Entities.WebCategory> dbEntities;

            dbEntities = repo.GetAll().Where(c => !c.IsDeleted).ToList();

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<List<Model.Entities.WebCategory>, List<WebCategory>>(dbEntities);
        }
    }
}
