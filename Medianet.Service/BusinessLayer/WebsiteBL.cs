﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class WebsiteBL
    {
        private UnitOfWork _uow;

        public WebsiteBL(UnitOfWork uow) {
            _uow = uow;
        }

        public Website GetWebsite(int id) {
            var repo = new WebsiteRepository(_uow);
            Model.Entities.Website dbEntity;

            dbEntity = repo.GetById(id);

            // Map the database object to a friendly representation of the data.
            return AutoMapper.Mapper.Map<Model.Entities.Website, Website>(dbEntity);
        }
    }
}
