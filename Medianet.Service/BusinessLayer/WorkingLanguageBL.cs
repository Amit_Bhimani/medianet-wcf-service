﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Medianet.DataLayer;
using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.Common;
using Medianet.Service.Dto;

namespace Medianet.Service.BusinessLayer
{
    public class WorkingLanguageBL
    {
        private UnitOfWork _uow;

        public WorkingLanguageBL(UnitOfWork uow)
        {
            _uow = uow;
        }

        public WorkingLanguage Get(int id)
        {
            var repo = new WorkingLanguageRepository(_uow);
            Model.Entities.WorkingLanguage dbEntities;

            dbEntities = repo.Get(id);

            return AutoMapper.Mapper.Map<Model.Entities.WorkingLanguage, WorkingLanguage>(dbEntities);
        }

        public List<WorkingLanguage> GetAll()
        {
            var repo = new WorkingLanguageRepository(_uow);
            List<Model.Entities.WorkingLanguage> dbEntities;

            dbEntities = repo.GetAll();

            return AutoMapper.Mapper.Map<List<Model.Entities.WorkingLanguage>, List<WorkingLanguage>>(dbEntities);
        }
    }
}
