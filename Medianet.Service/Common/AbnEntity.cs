﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medianet.Service.Common
{
    
    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://abr.business.gov.au/ABRXMLSearch/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://abr.business.gov.au/ABRXMLSearch/", IsNullable = false)]
    public partial class ABRPayloadSearchResults
    {

        private ABRPayloadSearchResultsRequest requestField;

        private ABRPayloadSearchResultsResponse responseField;

        /// <remarks/>
        public ABRPayloadSearchResultsRequest request
        {
            get
            {
                return this.requestField;
            }
            set
            {
                this.requestField = value;
            }
        }

        /// <remarks/>
        public ABRPayloadSearchResultsResponse response
        {
            get
            {
                return this.responseField;
            }
            set
            {
                this.responseField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://abr.business.gov.au/ABRXMLSearch/")]
    public partial class ABRPayloadSearchResultsRequest
    {

        private ABRPayloadSearchResultsRequestIdentifierSearchRequest identifierSearchRequestField;

        /// <remarks/>
        public ABRPayloadSearchResultsRequestIdentifierSearchRequest identifierSearchRequest
        {
            get
            {
                return this.identifierSearchRequestField;
            }
            set
            {
                this.identifierSearchRequestField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://abr.business.gov.au/ABRXMLSearch/")]
    public partial class ABRPayloadSearchResultsRequestIdentifierSearchRequest
    {

        private string authenticationGUIDField;

        private string identifierTypeField;

        private ulong identifierValueField;

        private string historyField;

        /// <remarks/>
        public string authenticationGUID
        {
            get
            {
                return this.authenticationGUIDField;
            }
            set
            {
                this.authenticationGUIDField = value;
            }
        }

        /// <remarks/>
        public string identifierType
        {
            get
            {
                return this.identifierTypeField;
            }
            set
            {
                this.identifierTypeField = value;
            }
        }

        /// <remarks/>
        public ulong identifierValue
        {
            get
            {
                return this.identifierValueField;
            }
            set
            {
                this.identifierValueField = value;
            }
        }

        /// <remarks/>
        public string history
        {
            get
            {
                return this.historyField;
            }
            set
            {
                this.historyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://abr.business.gov.au/ABRXMLSearch/")]
    public partial class ABRPayloadSearchResultsResponse
    {

        private string usageStatementField;

        private System.DateTime dateRegisterLastUpdatedField;

        private System.DateTime dateTimeRetrievedField;

        private ABRPayloadSearchResultsResponseBusinessEntity202001 businessEntity202001Field;

        /// <remarks/>
        public string usageStatement
        {
            get
            {
                return this.usageStatementField;
            }
            set
            {
                this.usageStatementField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime dateRegisterLastUpdated
        {
            get
            {
                return this.dateRegisterLastUpdatedField;
            }
            set
            {
                this.dateRegisterLastUpdatedField = value;
            }
        }

        /// <remarks/>
        public System.DateTime dateTimeRetrieved
        {
            get
            {
                return this.dateTimeRetrievedField;
            }
            set
            {
                this.dateTimeRetrievedField = value;
            }
        }

        /// <remarks/>
        public ABRPayloadSearchResultsResponseBusinessEntity202001 businessEntity202001
        {
            get
            {
                return this.businessEntity202001Field;
            }
            set
            {
                this.businessEntity202001Field = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://abr.business.gov.au/ABRXMLSearch/")]
    public partial class ABRPayloadSearchResultsResponseBusinessEntity202001
    {

        private System.DateTime recordLastUpdatedDateField;

        private ABRPayloadSearchResultsResponseBusinessEntity202001ABN aBNField;

        private ABRPayloadSearchResultsResponseBusinessEntity202001EntityStatus entityStatusField;

        private uint aSICNumberField;

        private ABRPayloadSearchResultsResponseBusinessEntity202001EntityType entityTypeField;

        private ABRPayloadSearchResultsResponseBusinessEntity202001GoodsAndServicesTax goodsAndServicesTaxField;

        private ABRPayloadSearchResultsResponseBusinessEntity202001MainName[] mainNameField;

        private ABRPayloadSearchResultsResponseBusinessEntity202001MainTradingName[] mainTradingNameField;

        private ABRPayloadSearchResultsResponseBusinessEntity202001MainBusinessPhysicalAddress[] mainBusinessPhysicalAddressField;

        private ABRPayloadSearchResultsResponseBusinessEntity202001BusinessName[] businessNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime recordLastUpdatedDate
        {
            get
            {
                return this.recordLastUpdatedDateField;
            }
            set
            {
                this.recordLastUpdatedDateField = value;
            }
        }

        /// <remarks/>
        public ABRPayloadSearchResultsResponseBusinessEntity202001ABN ABN
        {
            get
            {
                return this.aBNField;
            }
            set
            {
                this.aBNField = value;
            }
        }

        /// <remarks/>
        public ABRPayloadSearchResultsResponseBusinessEntity202001EntityStatus entityStatus
        {
            get
            {
                return this.entityStatusField;
            }
            set
            {
                this.entityStatusField = value;
            }
        }

        /// <remarks/>
        public uint ASICNumber
        {
            get
            {
                return this.aSICNumberField;
            }
            set
            {
                this.aSICNumberField = value;
            }
        }

        /// <remarks/>
        public ABRPayloadSearchResultsResponseBusinessEntity202001EntityType entityType
        {
            get
            {
                return this.entityTypeField;
            }
            set
            {
                this.entityTypeField = value;
            }
        }

        /// <remarks/>
        public ABRPayloadSearchResultsResponseBusinessEntity202001GoodsAndServicesTax goodsAndServicesTax
        {
            get
            {
                return this.goodsAndServicesTaxField;
            }
            set
            {
                this.goodsAndServicesTaxField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("mainName")]
        public ABRPayloadSearchResultsResponseBusinessEntity202001MainName[] mainName
        {
            get
            {
                return this.mainNameField;
            }
            set
            {
                this.mainNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("mainTradingName")]
        public ABRPayloadSearchResultsResponseBusinessEntity202001MainTradingName[] mainTradingName
        {
            get
            {
                return this.mainTradingNameField;
            }
            set
            {
                this.mainTradingNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("mainBusinessPhysicalAddress")]
        public ABRPayloadSearchResultsResponseBusinessEntity202001MainBusinessPhysicalAddress[] mainBusinessPhysicalAddress
        {
            get
            {
                return this.mainBusinessPhysicalAddressField;
            }
            set
            {
                this.mainBusinessPhysicalAddressField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("businessName")]
        public ABRPayloadSearchResultsResponseBusinessEntity202001BusinessName[] businessName
        {
            get
            {
                return this.businessNameField;
            }
            set
            {
                this.businessNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://abr.business.gov.au/ABRXMLSearch/")]
    public partial class ABRPayloadSearchResultsResponseBusinessEntity202001ABN
    {

        private ulong identifierValueField;

        private string isCurrentIndicatorField;

        private System.DateTime replacedFromField;

        /// <remarks/>
        public ulong identifierValue
        {
            get
            {
                return this.identifierValueField;
            }
            set
            {
                this.identifierValueField = value;
            }
        }

        /// <remarks/>
        public string isCurrentIndicator
        {
            get
            {
                return this.isCurrentIndicatorField;
            }
            set
            {
                this.isCurrentIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime replacedFrom
        {
            get
            {
                return this.replacedFromField;
            }
            set
            {
                this.replacedFromField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://abr.business.gov.au/ABRXMLSearch/")]
    public partial class ABRPayloadSearchResultsResponseBusinessEntity202001EntityStatus
    {

        private string entityStatusCodeField;

        private System.DateTime effectiveFromField;

        private System.DateTime effectiveToField;

        /// <remarks/>
        public string entityStatusCode
        {
            get
            {
                return this.entityStatusCodeField;
            }
            set
            {
                this.entityStatusCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime effectiveFrom
        {
            get
            {
                return this.effectiveFromField;
            }
            set
            {
                this.effectiveFromField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime effectiveTo
        {
            get
            {
                return this.effectiveToField;
            }
            set
            {
                this.effectiveToField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://abr.business.gov.au/ABRXMLSearch/")]
    public partial class ABRPayloadSearchResultsResponseBusinessEntity202001EntityType
    {

        private string entityTypeCodeField;

        private string entityDescriptionField;

        /// <remarks/>
        public string entityTypeCode
        {
            get
            {
                return this.entityTypeCodeField;
            }
            set
            {
                this.entityTypeCodeField = value;
            }
        }

        /// <remarks/>
        public string entityDescription
        {
            get
            {
                return this.entityDescriptionField;
            }
            set
            {
                this.entityDescriptionField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://abr.business.gov.au/ABRXMLSearch/")]
    public partial class ABRPayloadSearchResultsResponseBusinessEntity202001GoodsAndServicesTax
    {

        private System.DateTime effectiveFromField;

        private System.DateTime effectiveToField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime effectiveFrom
        {
            get
            {
                return this.effectiveFromField;
            }
            set
            {
                this.effectiveFromField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime effectiveTo
        {
            get
            {
                return this.effectiveToField;
            }
            set
            {
                this.effectiveToField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://abr.business.gov.au/ABRXMLSearch/")]
    public partial class ABRPayloadSearchResultsResponseBusinessEntity202001MainName
    {

        private string organisationNameField;

        private System.DateTime effectiveFromField;

        private System.DateTime effectiveToField;

        private bool effectiveToFieldSpecified;

        /// <remarks/>
        public string organisationName
        {
            get
            {
                return this.organisationNameField;
            }
            set
            {
                this.organisationNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime effectiveFrom
        {
            get
            {
                return this.effectiveFromField;
            }
            set
            {
                this.effectiveFromField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime effectiveTo
        {
            get
            {
                return this.effectiveToField;
            }
            set
            {
                this.effectiveToField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool effectiveToSpecified
        {
            get
            {
                return this.effectiveToFieldSpecified;
            }
            set
            {
                this.effectiveToFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://abr.business.gov.au/ABRXMLSearch/")]
    public partial class ABRPayloadSearchResultsResponseBusinessEntity202001MainTradingName
    {

        private string organisationNameField;

        private System.DateTime effectiveFromField;

        private System.DateTime effectiveToField;

        private bool effectiveToFieldSpecified;

        /// <remarks/>
        public string organisationName
        {
            get
            {
                return this.organisationNameField;
            }
            set
            {
                this.organisationNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime effectiveFrom
        {
            get
            {
                return this.effectiveFromField;
            }
            set
            {
                this.effectiveFromField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime effectiveTo
        {
            get
            {
                return this.effectiveToField;
            }
            set
            {
                this.effectiveToField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool effectiveToSpecified
        {
            get
            {
                return this.effectiveToFieldSpecified;
            }
            set
            {
                this.effectiveToFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://abr.business.gov.au/ABRXMLSearch/")]
    public partial class ABRPayloadSearchResultsResponseBusinessEntity202001MainBusinessPhysicalAddress
    {

        private string stateCodeField;

        private ushort postcodeField;

        private System.DateTime effectiveFromField;

        private System.DateTime effectiveToField;

        /// <remarks/>
        public string stateCode
        {
            get
            {
                return this.stateCodeField;
            }
            set
            {
                this.stateCodeField = value;
            }
        }

        /// <remarks/>
        public ushort postcode
        {
            get
            {
                return this.postcodeField;
            }
            set
            {
                this.postcodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime effectiveFrom
        {
            get
            {
                return this.effectiveFromField;
            }
            set
            {
                this.effectiveFromField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime effectiveTo
        {
            get
            {
                return this.effectiveToField;
            }
            set
            {
                this.effectiveToField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://abr.business.gov.au/ABRXMLSearch/")]
    public partial class ABRPayloadSearchResultsResponseBusinessEntity202001BusinessName
    {

        private string organisationNameField;

        private System.DateTime effectiveFromField;

        private System.DateTime effectiveToField;

        private bool effectiveToFieldSpecified;

        /// <remarks/>
        public string organisationName
        {
            get
            {
                return this.organisationNameField;
            }
            set
            {
                this.organisationNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime effectiveFrom
        {
            get
            {
                return this.effectiveFromField;
            }
            set
            {
                this.effectiveFromField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime effectiveTo
        {
            get
            {
                return this.effectiveToField;
            }
            set
            {
                this.effectiveToField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool effectiveToSpecified
        {
            get
            {
                return this.effectiveToFieldSpecified;
            }
            set
            {
                this.effectiveToFieldSpecified = value;
            }
        }
    }
}
