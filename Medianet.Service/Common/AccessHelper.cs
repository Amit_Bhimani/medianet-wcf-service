﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Medianet.Service.Dto;
using Medianet.Service.FaultExceptions;
using Medianet.Service.BusinessLayer;
using Medianet.DataLayer;
using Medianet.Service.BusinessLayer.Common;
using System.Configuration;

namespace Medianet.Service.Common
{
    public class AccessHelper
    {
        private const int security_sleep_time = 1000;

        public static DBSession GetSessionFromKey(string sessionKey, bool allowPasswordChangingSessions = false) {
            using (var uow = new UnitOfWork()) {
                var sessionBL = new DBSessionBL(uow);
                DBSession session;

                try {
                    session = sessionBL.Get(sessionKey, allowPasswordChangingSessions);
                    //For Fetching Customer Details from ChargeBee
                    Medianet.Service.ChargeBeeService cb = new ChargeBeeService();
                    session.Customer = cb.GetChargeBeeCustomer(session.DebtorNumber, session.Customer);
                }
                catch (ArgumentException) {
                    session = null;
                }

                if (session == null) {
                    BruteForcePreventionDelay();
                    throw new FaultException<InvalidSessionFault>(new InvalidSessionFault(), string.Format("SessionKey {0} is not valid.", sessionKey));
                }

                return session;
            }
        }

        public static bool IsAccessAllowedToWebsite(User user, SystemType system) {
            return (system == SystemType.Medianet && user.HasDistributeWebAccess) ||
                   (system == SystemType.MessageConnect && user.HasMessageConnectWebAccess) ||
                   (system == SystemType.Journalists && user.HasJournalistsWebAccess) ||
                   (system == SystemType.MNUser && user.MNUserAccessRights != AdminAccessType.None) ||
                   (system == SystemType.Admin && user.HasAdminWebAccess) ||
                   ((system == SystemType.Contacts) && user.HasContactsWebAccess);
        }

        public static bool IsAccessAllowedToJournalistsSearch(string sessionKey) {
            DBSession session = GetSessionFromKey(sessionKey);

            return IsAccessAllowedToJournalistsSearch(session);
        }

        public static bool IsAccessAllowedToJournalistsSearch(DBSession session) {
            return (session.System == SystemType.Journalists || IsAdmin(session));
        }

        public static bool IsAccessAllowedToImpersonate(SystemType fromSystem, SystemType toSystem) {
            return (fromSystem == SystemType.Admin && toSystem != SystemType.Admin);
        }

        public static bool IsFullAccessAllowedToRelease(string releaseDebtorNumber, SystemType releaseSystem, string sessionKey) {
            DBSession session = GetSessionFromKey(sessionKey);

            return IsFullAccessAllowedToRelease(releaseDebtorNumber, releaseSystem, session);
        }

        public static bool IsVerifiedCustomer(string debtorNumber)
        {
            return (ConfigurationManager.AppSettings["UnverifiedCustomer"] != debtorNumber);
        }

        public static bool IsFullAccessAllowedToRelease(string releaseDebtorNumber, SystemType releaseSystem, DBSession session) {
            // If they are logged in to the admin system give them access.
            if (session.System != SystemType.Admin) {
                // If their customer record doesn't own the Release or it's for a different system don't give them access.
                if (releaseSystem != session.System || releaseDebtorNumber != session.DebtorNumber)
                    return false;
            }

            return true;
        }

        public static bool IsPublicAccessAllowedToRelease(Release release, string securityKey) {
            // If it's visible to the public let them see it. Otherwise validate they have the correct security key.
            if (release.WebDetails == null || !release.WebDetails.ShouldShowOnPublic) {
                if (securityKey != release.SecurityKey)
                    return false;
            }

            return true;
        }

        public static bool IsAccessAllowedToTransactionResults(Transaction trans, DBSession session) {
            // If they are logged in to the admin system give them access.
            if (session.System != SystemType.Admin) {
                // If the transaction specifies not to send results to them or it's not an AAP list or one of theirs then don't give them access.
                if (session.DebtorNumber != trans.DebtorNumber && trans.DebtorNumber != Constants.DEBTOR_NUMBER_AAP)
                    return false;
            }

            return true;
        }

        public static void CheckAccessAllowedToServiceList(int serviceId, string sessionKey) {
            using (var uow = new UnitOfWork()) {
                var listBL = new ServiceListBL(uow);
                var service = listBL.Get(serviceId);

                if (service == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Service with Id {0} not found.", serviceId));

                // Make sure they have access to it.
                if (!AccessHelper.IsAccessAllowedToList(service.DebtorNumber, service.System, sessionKey))
                    throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to Service with Id {0} denied.", serviceId));
            }
        }

        public static bool IsAccessAllowedToList(string listDebtorNumber, SystemType listSystem, string sessionKey) {
            DBSession session = GetSessionFromKey(sessionKey);

            return IsAccessAllowedToList(listDebtorNumber, listSystem, session);
        }

        public static bool IsAccessAllowedToList(string listDebtorNumber, SystemType listSystem, DBSession session) {
            // If they are logged in to the admin system give them access.
            if (session.System != SystemType.Admin) {
                // If the System is not the same they arem't allowed to see it.
                if (listSystem != session.System)
                    return false;

                // If it's not an AAP list and it's not one of theirs then they aren't allowed to see it.
                if (!listDebtorNumber.Equals(BusinessLayer.Common.Constants.DEBTOR_NUMBER_AAP) && listDebtorNumber != session.DebtorNumber)
                        return false;
            }

            return true;
        }

        public static bool IsAccessAllowedToContactsData(string debtorNumber, int? userId, bool isPrivate, DBSession session) {
            // If they are logged in to the admin system give them access.
            if (session.System != SystemType.Admin) {
                // If they aren't logged in to the Medianet Contacts website they aren't allowed access.
                if (session.System != SystemType.Contacts)
                    return false;

                // If it's not for the users own company then they aren't allowed access.
                if (debtorNumber != session.DebtorNumber)
                    return false;

                // If they are asking to access all Users data (i.e. UserId is null) or another users data then make sure they have contacts admin rights.
                if (!userId.HasValue && session.User.ContactsAccessRights != ContactsAccessType.Administrator)
                    return false;

                // If they are asking to access all Users data (i.e. UserId is null) or another users data that's private then make sure they have contacts admin rights.
                if ((!userId.HasValue || (userId.Value != session.UserId && isPrivate)) && session.User.ContactsAccessRights != ContactsAccessType.Administrator)
                    return false;
            }

            return true;
        }

        public static bool IsAccessAllowedToContactsData(string sessionKey) {
            DBSession session = GetSessionFromKey(sessionKey);

            return IsAccessAllowedToContactsData(session);
        }

        public static bool IsAccessAllowedToContactsData(DBSession session) {
            // If they are logged in to the admin system give them access.
            if (session.System != SystemType.Admin) {
                // If they aren't logged in to the Medianet Contacts website they aren't allowed access.
                if (session.System != SystemType.Contacts && session.System != SystemType.Journalists)
                    return false;
            }

            return true;
        }

        public static bool IsAdminOrSameCustomer(string debtorNumber, string sessionKey) {
            DBSession session = GetSessionFromKey(sessionKey);

            return IsAdminOrSameCustomer(debtorNumber, session);
        }

        public static bool IsAdminOrSameCustomer(string debtorNumber, DBSession session) {
            // If they are logged in to the admin system give them access.
            if (session.System != SystemType.Admin) {
                // Only allow it if they are the owner of it.
                if (debtorNumber != session.DebtorNumber)
                    return false;
            }

            return true;
        }

        public static bool IsAdminOrSameUser(int userId, string sessionKey)
        {
            DBSession session = GetSessionFromKey(sessionKey);

            return IsAdminOrSameUser(userId, session);
        }

        public static bool IsAdminOrSameUser(int userId, DBSession session)
        {
            // If they are logged in to the admin system give them access.
            if (session.System != SystemType.Admin)
            {
                // Only allow it if they are the owner of it.
                if (userId != session.UserId)
                    return false;
            }

            return true;
        }

        public static bool IsAdmin(string sessionKey) {
            DBSession session = GetSessionFromKey(sessionKey);

            return IsAdmin(session);
        }

        public static bool IsAdmin(DBSession session) {
            return (session.System == SystemType.Admin);
        }

        public static void BruteForcePreventionDelay() {
            System.Threading.Thread.Sleep(security_sleep_time);
        }

        public static void ProtectResults(List<Result>results , Transaction trans)
        {
            foreach (Result r in results)
            {
                if (!trans.ShouldSendResultsToOwner)
                {
                    r.Address = string.Empty;
                }
            }
        }
    }
}
