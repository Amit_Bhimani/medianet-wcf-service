﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Medianet.Service;
using Medianet.Model;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using Medianet.BusinessLayer.Common;
using Medianet.Model.Entities;

namespace Medianet.Service.Common
{
    public class AutomapperProfile : Profile
    {
        public const string VIEW_MODEL = "AutomapperProfile";
        private const char SEPARATOR = '#';
        private const char COMMA = ',';
        private const string COMMA_SPACE = ", ";

        public override string ProfileName
        {
            get { return VIEW_MODEL; }
        }

        protected override void Configure()
        {

            #region ------ AAPEnvironment ------

            Mapper.CreateMap<Model.Entities.AAPEnvironment, Service.Dto.AAPEnvironment>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.AAPEnvironment, Model.Entities.AAPEnvironment>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ AccountAddress to  FinanceCustomerService.MedianetCustomerAddress ------

            Mapper.CreateMap<FinanceCustomerService.MedianetCustomerAddress, Service.Dto.AccountAddress>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.AccountAddress, FinanceCustomerService.MedianetCustomerAddress>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ City ------

            Mapper.CreateMap<Model.Entities.City, Service.Dto.City>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.CityId))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.CityName))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.City, Model.Entities.City>()
                .ForMember(dest => dest.CityId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.CityName, opt => opt.MapFrom(src => src.Name))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Contact Role ------

            Mapper.CreateMap<Model.Entities.ContactRole, Service.Dto.ContactRole>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.RoleId))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.ContactRole, Model.Entities.ContactRole>()
                .ForMember(dest => dest.RoleId, opt => opt.MapFrom(src => src.Id))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Continent ------

            Mapper.CreateMap<Model.Entities.Continent, Service.Dto.Continent>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.Continent, Model.Entities.Continent>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Country ------

            Mapper.CreateMap<Model.Entities.Country, Service.Dto.Country>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.MediaAtlasCountryId))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.CountryName))
                .ForMember(dest => dest.ContinentId, opt => opt.MapFrom(src => src.MediaAtlasContinentId))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.Country, Model.Entities.Country>()
                .ForMember(dest => dest.MediaAtlasCountryId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.CountryName, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.MediaAtlasContinentId, opt => opt.MapFrom(src => src.ContinentId))
                .IgnoreAllNonExisting();

            #endregion

            //#region ------ CourseRegistrationRequest to Account ------

            //Mapper.CreateMap<Service.Dto.CourseRegistrationRequest, Service.Dto.Account>()
            //    .IgnoreAllNonExisting();

            //#endregion

            //#region ------ CourseRegistrationRequest to NewAccountRequest ------

            //Mapper.CreateMap<Service.Dto.CourseRegistrationRequest, Service.Dto.NewAccountRequest>()
            //    .IgnoreAllNonExisting();

            //#endregion

            #region ------ CourseRegistrationRequest to TrainingCourseEnrolment ------

            Mapper.CreateMap<Service.Dto.CourseRegistrationRequest, Service.Dto.TrainingCourseEnrolment>()
                .ForMember(dest => dest.Title, opt => opt.UseValue(string.Empty))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName.TrimEnd().Truncate(50)))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName.TrimEnd().Truncate(50)))
                .ForMember(dest => dest.Company, opt => opt.MapFrom(src => src.CompanyName.TrimEnd().Truncate(50)))
                .ForMember(dest => dest.Address, opt => opt.MapFrom(src => string.Format("{0}, {1}, {2}, {3} {4}", src.Address.TrimEnd(), src.Suburb.TrimEnd(), src.State.TrimEnd(), src.PostCode.TrimEnd(), src.Country.TrimEnd()).TrimEnd().Truncate(500)))
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.TelephoneNumber.TrimEnd().Truncate(20)))
                .ForMember(dest => dest.Fax, opt => opt.UseValue(string.Empty))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.EmailAddress.TrimEnd().Truncate(50)))
                .ForMember(dest => dest.ABN, opt => opt.UseValue(string.Empty))
                .ForMember(dest => dest.ReceiptId, opt => opt.MapFrom(src => src.ReceiptId.TrimEnd().Truncate(50)))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Customer ------

            Mapper.CreateMap<Model.Entities.Customer, Service.Dto.Customer>()
                .ForMember(dest => dest.DebtorNumber, opt => opt.MapFrom(src => src.DebtorNumber.TrimEnd()))
                .ForMember(dest => dest.ReportSendMethod, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.ReportType>(src.ReportSendMethod, Common.ReportType.None)))
                .ForMember(dest => dest.ReportResultsToSend, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.ResultsType>(src.ReportResultsToSend)))
                .ForMember(dest => dest.OptOutMethod, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.OptoutType>(src.OptOutMethod)))
                .ForMember(dest => dest.MediaDirectorySystem, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.MediaDirectoryType>(src.OptOutMethod, Common.MediaDirectoryType.None)))
                .ForMember(dest => dest.RowStatus, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.RowStatusType>(src.RowStatus)))
                .ForMember(dest => dest.AccountType, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.CustomerBillingType>(src.AccountType, CustomerBillingType.Invoiced)))
                .ForMember(dest => dest.Timezone, opt => opt.MapFrom(src => Mapper.Map<Model.Entities.Timezone, Service.Dto.Timezone>(src.Timezone)))
                .ForMember(dest => dest.CanExportContacts, opt => opt.MapFrom(src => (src.CanExportContacts.HasValue ? src.CanExportContacts : false)))
                .ForMember(dest => dest.IsVerified, opt => opt.MapFrom(src => (AccessHelper.IsVerifiedCustomer(src.DebtorNumber))))
                .ForMember(dest => dest.SalesRegions, opt => opt.MapFrom(src => Mapper.Map<List<Model.Entities.CustomerRegion>, List<Service.Dto.CustomerRegion>>(src.SalesRegions != null ? src.SalesRegions.ToList() : null)))
                .ForMember(dest => dest.HasViewedTermsAndConditionsP, opt => opt.MapFrom(src => (src.HasViewedTermsAndConditionsP.HasValue ? src.HasViewedTermsAndConditionsP : false)))
                .ForMember(dest => dest.HasViewedTermsAndConditionsC, opt => opt.MapFrom(src => (src.HasViewedTermsAndConditionsC.HasValue ? src.HasViewedTermsAndConditionsC : false)))
                .ForMember(dest => dest.HasViewedTermsAndConditionsM, opt => opt.MapFrom(src => (src.HasViewedTermsAndConditionsM.HasValue ? src.HasViewedTermsAndConditionsM : false)))
                .ForMember(dest => dest.DefaultSearchCountry, opt => opt.MapFrom(src => (src.DefaultSearchCountry == null ?  0 : src.DefaultSearchCountry)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.Customer, Model.Entities.Customer>()
                .ForMember(dest => dest.ReportSendMethod, opt => opt.MapFrom(src => ((char)src.ReportSendMethod).ToString()))
                .ForMember(dest => dest.ReportResultsToSend, opt => opt.MapFrom(src => ((char)src.ReportResultsToSend).ToString()))
                .ForMember(dest => dest.OptOutMethod, opt => opt.MapFrom(src => ((char)src.OptOutMethod).ToString()))
                .ForMember(dest => dest.MediaDirectorySystem, opt => opt.MapFrom(src => ((char)src.OptOutMethod).ToString()))
                .ForMember(dest => dest.MediaContactsMasterUserId, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.MediaContactsMasterPassword, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.RowStatus, opt => opt.MapFrom(src => ((char)src.RowStatus).ToString()))
                .ForMember(dest => dest.AccountType, opt => opt.MapFrom(src => ((char)src.AccountType).ToString()))
                .ForMember(dest => dest.Timezone, opt => opt.Ignore())
                .ForMember(dest => dest.SalesRegions, opt => opt.MapFrom(src => Mapper.Map<List<Service.Dto.CustomerRegion>, List<Model.Entities.CustomerRegion>>(src.SalesRegions != null ? src.SalesRegions.ToList() : null)))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Customer Billing Code ------

            Mapper.CreateMap<Model.Entities.CustomerBillingCode, Service.Dto.CustomerBillingCode>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.CustomerBillingCode, Model.Entities.CustomerBillingCode>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Customer Region ------

            Mapper.CreateMap<Model.Entities.CustomerRegion, Service.Dto.CustomerRegion>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.CustomerRegion, Model.Entities.CustomerRegion>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Customer Statistic ------

            Mapper.CreateMap<Model.Entities.CustomerStatistic, Service.Dto.CustomerStatistic>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.CustomerStatistic, Model.Entities.CustomerStatistic>()
                .IgnoreAllNonExisting();

            #endregion
            #region ------ Customer Custom Pricing ------

            Mapper.CreateMap<Model.Entities.CustomerCustomPricing, Service.Dto.CustomerCustomPricing>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.CustomerCustomPricing, Model.Entities.CustomerCustomPricing>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ DBSession ------

            Mapper.CreateMap<Model.Entities.DBSession, Service.Dto.DBSession>()
                .ForMember(dest => dest.Key, opt => opt.MapFrom(src => src.Id.ToString()))
                .ForMember(dest => dest.DebtorNumber, opt => opt.MapFrom(src => src.DebtorNumber.TrimEnd()))
                .ForMember(dest => dest.System, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.SystemType>(src.System)))
                .ForMember(dest => dest.RowStatus, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.RowStatusType>(src.RowStatus)))
                .ForMember(dest => dest.Customer, opt => opt.MapFrom(src => Mapper.Map<Model.Entities.Customer, Service.Dto.Customer>(src.Customer)))
                .ForMember(dest => dest.User, opt => opt.MapFrom(src => Mapper.Map<Model.Entities.User, Service.Dto.User>(src.User)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.DBSession, Model.Entities.DBSession>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => int.Parse(src.Key)))
                .ForMember(dest => dest.System, opt => opt.MapFrom(src => ((char)src.System).ToString()))
                .ForMember(dest => dest.RowStatus, opt => opt.MapFrom(src => ((char)src.RowStatus).ToString()))
                .ForMember(dest => dest.CompanyName, opt => opt.MapFrom(src => "")) // We don't need this as it's in the Customer entity.
                .ForMember(dest => dest.Customer, opt => opt.MapFrom(src => Mapper.Map<Service.Dto.Customer, Model.Entities.Customer>(src.Customer)))
                .ForMember(dest => dest.User, opt => opt.MapFrom(src => Mapper.Map<Service.Dto.User, Model.Entities.User>(src.User)))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Document ------

            Mapper.CreateMap<Model.Entities.Document, Service.Dto.Document>()
                .ForMember(dest => dest.OriginalFilename, opt => opt.MapFrom(src => src.OriginalFilename.TrimEnd()))
                .ForMember(dest => dest.FileExtension, opt => opt.MapFrom(src => src.FileExtension.TrimEnd()))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.Document, Model.Entities.Document>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Document to DocumentPublic ------

            Mapper.CreateMap<Model.Entities.Document, Service.Dto.DocumentPublic>()
                .ForMember(dest => dest.OriginalFilename, opt => opt.MapFrom(src => src.OriginalFilename.TrimEnd()))
                .ForMember(dest => dest.FileExtension, opt => opt.MapFrom(src => src.FileExtension.TrimEnd()))
                .ForMember(dest => dest.Thumbnails, opt => opt.Ignore())
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Document to FileContentResponse ------

            Mapper.CreateMap<Service.Dto.Document, Service.Dto.FileContentResponse>()
                .ForMember(dest => dest.FileId, opt => opt.MapFrom(src => src.Id))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ InboundFile ------

            Mapper.CreateMap<Model.Entities.InboundFile, Service.Dto.InboundFile>()
                .ForMember(dest => dest.DistributionType, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.DistributionType>(src.DistributionType)))
                .ForMember(dest => dest.FileSize, opt => opt.MapFrom(src => src.FileSize.HasValue ? src.FileSize : 0))
                .ForMember(dest => dest.ForwardedDate, opt => opt.MapFrom(src => src.LastForwardedDate.HasValue ? src.LastForwardedDate.Value : DateTime.MinValue))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.InboundFileStatusType>(src.Status, InboundFileStatusType.Ready)))
                .ForMember(dest => dest.Type, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.MessageType>(src.Type, MessageType.FaxRelease)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.InboundFile, Model.Entities.InboundFile>()
                .ForMember(dest => dest.DistributionType, opt => opt.MapFrom(src => ((char)src.DistributionType).ToString()))
                .ForMember(dest => dest.LastForwardedDate, opt => opt.MapFrom(src => (src.ForwardedDate == DateTime.MinValue ? (DateTime?)null : src.ForwardedDate)))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => ((char)src.Status).ToString()))
                .ForMember(dest => dest.Type, opt => opt.MapFrom(src => ((char)src.Type).ToString()))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ InboundFile to FileContentResponse ------

            Mapper.CreateMap<Service.Dto.InboundFile, Service.Dto.FileContentResponse>()
                .ForMember(dest => dest.FileId, opt => opt.MapFrom(src => src.Id))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ FeatureRelease to ReleaseSummaryPublic ------

            Mapper.CreateMap<FeatureRelease, Dto.ReleaseSummaryPublic>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.ReleaseId))
                .ForMember(dest => dest.Headline, opt => opt.MapFrom(src => src.Release.WebDetails.Headline))
                .ForMember(dest => dest.Organisation, opt => opt.MapFrom(src => src.Release.WebDetails.Organisation))
                .ForMember(dest => dest.Summary, opt => opt.MapFrom(src => src.Release.WebDetails.Summary))
                .ForMember(dest => dest.WebCategoryId, opt => opt.MapFrom(src => src.Release.WebDetails.PrimaryWebCategoryId))
                .ForMember(dest => dest.MultimediaType, opt => opt.MapFrom(src => ExtensionMethods.ToEnum(src.Release.MultimediaType, MultimediaType.None)))
                .ForMember(dest => dest.DistributedDate, opt => opt.MapFrom(src => src.Release.WebDetails.PublicDistributedDate))
                .ForMember(dest => dest.HasVideo, opt => opt.MapFrom(src => src.Release.WebDetails.HasVideo))
                .ForMember(dest => dest.HasAudio, opt => opt.MapFrom(src => src.Release.WebDetails.HasAudio))
                .ForMember(dest => dest.HasPhoto, opt => opt.MapFrom(src => src.Release.WebDetails.HasPhoto))
                .ForMember(dest => dest.HasOther, opt => opt.MapFrom(src => src.Release.WebDetails.HasOther))
                //.ForMember(dest => dest.ShortUrl, opt => opt.MapFrom(src => src.Release.WebDetails.ShortUrl))
                .ForMember(dest => dest.IsVerified, opt => opt.MapFrom(src => src.Release.WebDetails.IsVerified.HasValue ? src.Release.WebDetails.IsVerified : false))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<MnjProfileReleases, Dto.ReleaseSummaryPublic>()
              .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.JobId))
              .ForMember(dest => dest.Headline, opt => opt.MapFrom(src => src.Headline))
              .ForMember(dest => dest.DistributedDate, opt => opt.MapFrom(src => src.WhenDistributed))
              .ForMember(dest => dest.DistributedDateTime, opt => opt.MapFrom(src => src.Date))
              .ForMember(dest => dest.Organisation, opt => opt.MapFrom(src => src.Organisation))
              .ForMember(dest => dest.MultimediaType, opt => opt.MapFrom(src => ExtensionMethods.ToEnum(src.MultimediaType, MultimediaType.None)))
              .ForMember(dest => dest.HasVideo, opt => opt.MapFrom(src => src.VideoDocId > 0))
              .ForMember(dest => dest.HasAudio, opt => opt.MapFrom(src => src.AudioDocId > 0))
              .ForMember(dest => dest.HasPhoto, opt => opt.MapFrom(src => src.PhotoDocId > 0))
              .ForMember(dest => dest.HasOther, opt => opt.MapFrom(src => src.OtherDocId > 0))
              .IgnoreAllNonExisting();

            #endregion

            #region ------ FinanceCustomerService.IndustryClass to IndustryCode ------

            Mapper.CreateMap<FinanceCustomerService.IndustryClass, Service.Dto.IndustryCode>()
                .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.IndustryClassCode))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.IndustryClassDesc))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ ListCategory ------

            Mapper.CreateMap<Model.Entities.ListCategory, Service.Dto.ListCategory>()
                .ForMember(dest => dest.IsVisible, opt => opt.MapFrom(src => (src.IsHidden.StartsWith("T") ? false : true)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.ListCategory, Model.Entities.ListCategory>()
                .ForMember(dest => dest.IsHidden, opt => opt.MapFrom(src => (src.IsVisible ? "F" : "T")))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ ListComment ------

            Mapper.CreateMap<Model.Entities.ListComment, Service.Dto.ListComment>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.ListComment, Model.Entities.ListComment>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ ListDistribution ------

            Mapper.CreateMap<Model.Entities.ListDistribution, Service.Dto.ListDistribution>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.ListDistribution, Model.Entities.ListDistribution>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ ListTicker ------

            Mapper.CreateMap<Model.Entities.ListTicker, Service.Dto.ListTicker>()
                .ForMember(dest => dest.ListDistributionType, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.DistributionType>(src.ListDistributionType)))
                .ForMember(dest => dest.ListSequenceNumber, opt => opt.MapFrom(src => (int)src.ListSequenceNumber))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.ListTicker, Model.Entities.ListTicker>()
                .ForMember(dest => dest.ListDistributionType, opt => opt.MapFrom(src => ((char)src.ListDistributionType).ToString()))
                .ForMember(dest => dest.ListSequenceNumber, opt => opt.MapFrom(src => (Int16)src.ListSequenceNumber))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ MediaContact ------

            Mapper.CreateMap<Model.Entities.MediaContact, Service.Dto.MediaContact>()
                .ForMember(dest => dest.RowStatus, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.ContactRowStatusType>(src.RowStatus)))
                .ForMember(dest => dest.User, opt => opt.MapFrom(src => Mapper.Map<Model.Entities.User, Service.Dto.User>(src.User)))
                .ForMember(dest => dest.Profile, opt => opt.MapFrom(src => (string)(src.Profile != null ? src.Profile.Content : "")))
                .ForMember(dest => dest.JobHistory, opt => opt.MapFrom(src => Mapper.Map<List<Model.Entities.MediaContactJobHistory>, List<Service.Dto.MediaContactJobHistory>>(src.JobHistory != null ? src.JobHistory.ToList() : null)))
                .ForMember(dest => dest.LastModifiedDate, opt => opt.MapFrom(src => src.LastModifiedDate))
                .ForMember(dest => dest.AlsoKnownAs, opt => opt.MapFrom(src => src.AlsoKnownAs))
                .ForMember(dest => dest.AppearsIn, opt => opt.MapFrom(src => src.AppearsIn))
                .ForMember(dest => dest.Awards, opt => opt.MapFrom(src => src.Awards))
                .ForMember(dest => dest.BugBears, opt => opt.MapFrom(src => src.BugBears))
                .ForMember(dest => dest.NamePronunciation, opt => opt.MapFrom(src => src.NamePronunciation))
                .ForMember(dest => dest.PersonalInterests, opt => opt.MapFrom(src => src.PersonalInterests))
                .ForMember(dest => dest.PoliticalParty, opt => opt.MapFrom(src => src.PoliticalParty))
                .ForMember(dest => dest.PressReleaseInterests, opt => opt.MapFrom(src => src.PressReleaseInterests))
                .ForMember(dest => dest.BestContactTime, opt => opt.MapFrom(src => src.BestContactTime))
                .ForMember(dest => dest.GiftsAccepted, opt => opt.MapFrom(src => src.GiftsAccepted))
                .ForMember(dest => dest.BusyTimes, opt => opt.MapFrom(src => src.BusyTimes))
                .ForMember(dest => dest.PitchingDos, opt => opt.MapFrom(src => src.PitchingDos))
                .ForMember(dest => dest.PitchingDonts, opt => opt.MapFrom(src => src.PitchingDonts))
                .ForMember(dest => dest.AuthorOf, opt => opt.MapFrom(src => src.AuthorOf))
                .ForMember(dest => dest.Facebook, opt => opt.MapFrom(src => src.Facebook))
                .ForMember(dest => dest.Instagram, opt => opt.MapFrom(src => src.Instagram))
                .ForMember(dest => dest.LinkedIn, opt => opt.MapFrom(src => src.LinkedIn))
                .ForMember(dest => dest.Snapchat, opt => opt.MapFrom(src => src.Snapchat))
                .ForMember(dest => dest.Twitter, opt => opt.MapFrom(src => src.Twitter))
                .ForMember(dest => dest.YouTube, opt => opt.MapFrom(src => src.YouTube))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MediaContact, Model.Entities.MediaContact>()
                .ForMember(dest => dest.RowStatus, opt => opt.MapFrom(src => ((char)src.RowStatus).ToString()))
                .ForMember(dest => dest.User, opt => opt.Ignore())
                .ForMember(dest => dest.Profile, opt => opt.Ignore())
                .ForMember(dest => dest.JobHistory, opt => opt.MapFrom(src => Mapper.Map<List<Service.Dto.MediaContactJobHistory>, List<Model.Entities.MediaContactJobHistory>>(src.JobHistory != null ? src.JobHistory.ToList() : null)))
                .ForMember(dest => dest.LastModifiedDate, opt => opt.MapFrom(src => src.LastModifiedDate))
                .ForMember(dest => dest.Facebook, opt => opt.MapFrom(src => src.Facebook))
                .ForMember(dest => dest.LinkedIn, opt => opt.MapFrom(src => src.LinkedIn))
                .ForMember(dest => dest.Instagram, opt => opt.MapFrom(src => src.Instagram))
                .ForMember(dest => dest.YouTube, opt => opt.MapFrom(src => src.YouTube))
                .ForMember(dest => dest.Snapchat, opt => opt.MapFrom(src => src.Snapchat))
                .ForMember(dest => dest.Skype, opt => opt.MapFrom(src => src.Skype))
                .ForMember(dest => dest.Twitter, opt => opt.MapFrom(src => src.Twitter))
                 // Pitching Profile
                .ForMember(dest => dest.BugBears, opt => opt.MapFrom(src => src.BugBears))
                .ForMember(dest => dest.AlsoKnownAs, opt => opt.MapFrom(src => src.AlsoKnownAs))
                .ForMember(dest => dest.PressReleaseInterests, opt => opt.MapFrom(src => src.PressReleaseInterests))
                .ForMember(dest => dest.Awards, opt => opt.MapFrom(src => src.Awards))
                .ForMember(dest => dest.PersonalInterests, opt => opt.MapFrom(src => src.PersonalInterests))
                .ForMember(dest => dest.PoliticalParty, opt => opt.MapFrom(src => src.PoliticalParty))
                .ForMember(dest => dest.NamePronunciation, opt => opt.MapFrom(src => src.NamePronunciation))
                .ForMember(dest => dest.AppearsIn, opt => opt.MapFrom(src => src.AppearsIn))
                .ForMember(dest => dest.BestContactTime, opt => opt.MapFrom(src => src.BestContactTime))
                .ForMember(dest => dest.AuthorOf, opt => opt.MapFrom(src => src.AuthorOf))
                .ForMember(dest => dest.CoffeeOrder, opt => opt.MapFrom(src => src.CoffeeOrder))
                .ForMember(dest => dest.BusyTimes, opt => opt.MapFrom(src => src.BusyTimes))
                .ForMember(dest => dest.GiftsAccepted, opt => opt.MapFrom(src => src.GiftsAccepted))
                .ForMember(dest => dest.PitchingDos, opt => opt.MapFrom(src => src.PitchingDos))
                .ForMember(dest => dest.PitchingDonts, opt => opt.MapFrom(src => src.PitchingDonts))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ MediaContactGroup ------

            Mapper.CreateMap<Model.Entities.MediaContactGroup, Service.Dto.MediaContactGroup>()
                .ForMember(dest => dest.IsActive, opt => opt.MapFrom(src => src.IsActive == 1 ? true : false))
                .ForMember(dest => dest.GroupType, opt => opt.MapFrom(src => src.GroupType.HasValue ? (MediaContactGroupType)src.GroupType : MediaContactGroupType.List))
                .ForMember(dest => dest.CreatedByUser, opt => opt.MapFrom(src => Mapper.Map<Model.Entities.User, Service.Dto.UserSummary>(src.CreatedByUser)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MediaContactGroup, Model.Entities.MediaContactGroup>()
                .ForMember(dest => dest.IsActive, opt => opt.MapFrom(src => src.IsActive ? 1 : 0))
                .ForMember(dest => dest.GroupType, opt => opt.MapFrom(src => (byte)src.GroupType))
                .ForMember(dest => dest.CreatedByUser, opt => opt.Ignore())
                .IgnoreAllNonExisting();

            #endregion

            #region ------ MediaContactList ------

            Mapper.CreateMap<Model.Entities.MediaContactList, Service.Dto.MediaContactList>()
                .ForMember(dest => dest.OwnerUser, opt => opt.MapFrom(src => Mapper.Map<Model.Entities.User, Service.Dto.UserSummary>(src.OwnerUser)))
                .ForMember(dest => dest.Group, opt => opt.MapFrom(src => Mapper.Map<Model.Entities.MediaContactGroup, Service.Dto.MediaContactGroup>(src.Group)))
                //.ForMember(dest => dest.Versions, opt => opt.MapFrom(src => Mapper.Map<List<Model.Entities.MediaContactListVersion>, List<Service.Dto.MediaContactListVersion>>(src.Versions)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MediaContactList, Model.Entities.MediaContactList>()
                .ForMember(dest => dest.OwnerUser, opt => opt.Ignore())
                .ForMember(dest => dest.Group, opt => opt.Ignore())
                //.ForMember(dest => dest.Versions, opt => opt.Ignore())
                .IgnoreAllNonExisting();

            #endregion

            #region ------ MediaContactListRecordDetails ------

            Mapper.CreateMap<Model.Entities.MediaContactListRecordDetails, Service.Dto.MediaContactListRecordDetails>()
                .ForMember(dest => dest.IsEntityDeleted, opt => opt.MapFrom(src => src.IsEntityDeleted == 1 ? true : false))
                .ForMember(dest => dest.ContactDisplay, opt => opt.MapFrom(src => src.FullName.TrimAndEmptyIfNull().Split(SEPARATOR).Length > 1 ? string.Format("{0:d} {1}", src.FullName.TrimAndEmptyIfNull().Split(SEPARATOR).Length, "Contacts") : src.FullName.TrimAndEmptyIfNull()))
                .ForMember(dest => dest.SubjectDisplay, opt => opt.MapFrom(src => src.SubjectName.TrimAndEmptyIfNull().Split(SEPARATOR).Length > 1 ? string.Format("{0:d} {1}", src.SubjectName.TrimAndEmptyIfNull().Split(SEPARATOR).Length, "Subjects") : src.SubjectName.TrimAndEmptyIfNull()))
                .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => Regex.Replace(src.FullName.TrimAndEmptyIfNull(),SEPARATOR.ToString(), COMMA_SPACE)))
                .ForMember(dest => dest.SubjectName, opt => opt.MapFrom(src => Regex.Replace(src.SubjectName.TrimAndEmptyIfNull(), SEPARATOR.ToString(), COMMA_SPACE)))
                .ForMember(dest => dest.ContactId, opt => opt.MapFrom(src => src.ContactId.TrimAndEmptyIfNull()))
                .ForMember(dest => dest.OutletId, opt => opt.MapFrom(src => src.OutletId.TrimAndEmptyIfNull()))
                .ForMember(dest => dest.CityName, opt => opt.MapFrom(src => src.CityName.TrimAndEmptyIfNull()))
                .ForMember(dest => dest.OutletName, opt => opt.MapFrom(src => src.OutletName.TrimAndEmptyIfNull()))
                .ForMember(dest => dest.JobTitle, opt => opt.MapFrom(src => src.JobTitle.TrimAndEmptyIfNull()))
                .ForMember(dest => dest.ProductTypeName, opt => opt.MapFrom(src => src.ProductTypeName.TrimAndEmptyIfNull()))
                .ForMember(dest => dest.IsGeneric, opt => opt.MapFrom(src => src.IsGeneric == 1))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ MediaContactListRecord ------

            Mapper.CreateMap<Model.Entities.MediaContactListRecord, Service.Dto.MediaContactListRecord>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MediaContactListRecord, Model.Entities.MediaContactListRecord>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ MediaContactListVersion ------

            Mapper.CreateMap<Model.Entities.MediaContactListVersion, Service.Dto.MediaContactListVersion>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MediaContactListVersion, Model.Entities.MediaContactListVersion>()
                .ForMember(dest => dest.LastModifiedDate, opt => opt.MapFrom(src => src.CreatedDate))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ MediaContactListVersionRecord ------

            Mapper.CreateMap<Model.Entities.MediaContactListVersionRecord, Service.Dto.MediaContactListVersionRecord>()
                .ForMember(dest => dest.ChangeType, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.MediaContactListChangeType>(src.ChangeType)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MediaContactListVersionRecord, Model.Entities.MediaContactListVersionRecord>()
                .ForMember(dest => dest.ChangeType, opt => opt.MapFrom(src => ((char)src.ChangeType).ToString()))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ MediaContactProfile ------

            Mapper.CreateMap<Model.Entities.MediaContactProfile, Service.Dto.MediaContactProfile>()
                .ForMember(dest => dest.ContactId, opt => opt.MapFrom(src => src.MediaContact.Id))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MediaContactProfile, Model.Entities.MediaContactProfile>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ MediaContactReminderType ------

            Mapper.CreateMap<Model.Entities.MediaContactReminderType, Service.Dto.MediaContactReminderType>()
                .ForMember(dest => dest.IsActive, opt => opt.MapFrom(src => src.IsActive == 1 ? true : false))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MediaContactReminderType, Model.Entities.MediaContactReminderType>()
                .ForMember(dest => dest.IsActive, opt => opt.MapFrom(src => src.IsActive ? 1 : 0))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ MediaContactSavedSearch ------

            Mapper.CreateMap<Model.Entities.MediaContactSavedSearch, Service.Dto.MediaContactSavedSearch>()
                .ForMember(dest => dest.ResultsCount, opt => opt.MapFrom(src => src.ResultsCount.HasValue ? src.ResultsCount.Value : 0))
                .ForMember(dest => dest.IsActive, opt => opt.MapFrom(src => src.IsActive == 1 ? true : false))
                .ForMember(dest => dest.Context, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.MediaContactSearchContext>(src.Context)))
                .ForMember(dest => dest.OwnerUser, opt => opt.MapFrom(src => Mapper.Map<Model.Entities.User, Service.Dto.UserSummary>(src.OwnerUser)))
                .ForMember(dest => dest.Group, opt => opt.MapFrom(src => Mapper.Map<Model.Entities.MediaContactGroup, Service.Dto.MediaContactGroup>(src.Group)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MediaContactSavedSearch, Model.Entities.MediaContactSavedSearch>()
                .ForMember(dest => dest.ResultsCount, opt => opt.MapFrom(src => src.ResultsCount))
                .ForMember(dest => dest.IsActive, opt => opt.MapFrom(src => src.IsActive ? 1 : 0))
                .ForMember(dest => dest.Context, opt => opt.MapFrom(src => ((char)src.Context).ToString()))
                .ForMember(dest => dest.OwnerUser, opt => opt.Ignore())
                .ForMember(dest => dest.Group, opt => opt.Ignore())
                .IgnoreAllNonExisting();

            #endregion

            #region ------ MediaContactsExport ------

            Mapper.CreateMap<Model.Entities.MediaContactsExport, Service.Dto.MediaContactsExport>()
                .ForMember(dest => dest.ExportType, opt => opt.MapFrom(src => (MediaContactsExportType)src.ExportType))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MediaContactsExport, Model.Entities.MediaContactsExport>()
                .ForMember(dest => dest.ExportType, opt => opt.MapFrom(src => (int)src.ExportType))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ MediaContactTask ------

            Mapper.CreateMap<Model.Entities.MediaContactTask, Service.Dto.MediaContactTask>()
                .ForMember(dest => dest.NotificationType, opt => opt.MapFrom(src => (MediaContactNotificationType)src.NotificationType))
                .ForMember(dest => dest.StatusType, opt => opt.MapFrom(src => (MediaContactTaskStatusType)src.StatusType))
                .ForMember(dest => dest.OwnerUser, opt => opt.MapFrom(src => Mapper.Map<Model.Entities.User, Service.Dto.UserSummary>(src.OwnerUser)))
                .ForMember(dest => dest.CreatedByUser, opt => opt.MapFrom(src => Mapper.Map<Model.Entities.User, Service.Dto.UserSummary>(src.CreatedByUser)))
                .ForMember(dest => dest.Group, opt => opt.MapFrom(src => Mapper.Map<Model.Entities.MediaContactGroup, Service.Dto.MediaContactGroup>(src.Group)))
                .ForMember(dest => dest.ReminderType, opt => opt.MapFrom(src => Mapper.Map<Model.Entities.MediaContactReminderType, Service.Dto.MediaContactReminderType>(src.ReminderType)))
                .ForMember(dest => dest.Category, opt => opt.MapFrom(src => Mapper.Map<Model.Entities.MediaContactTaskCategory, Service.Dto.MediaContactTaskCategory>(src.Category)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MediaContactTask, Model.Entities.MediaContactTask>()
                .ForMember(dest => dest.NotificationType, opt => opt.MapFrom(src => (int)src.NotificationType))
                .ForMember(dest => dest.StatusType, opt => opt.MapFrom(src => (int)src.StatusType))
                .ForMember(dest => dest.OwnerUser, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedByUser, opt => opt.Ignore())
                .ForMember(dest => dest.Group, opt => opt.Ignore())
                .ForMember(dest => dest.ReminderType, opt => opt.Ignore())
                .ForMember(dest => dest.Category, opt => opt.Ignore())
                .IgnoreAllNonExisting();

            #endregion

            #region ------ MediaContactTaskCategory ------

            Mapper.CreateMap<Model.Entities.MediaContactTaskCategory, Service.Dto.MediaContactTaskCategory>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MediaContactTaskCategory, Model.Entities.MediaContactTaskCategory>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ MediaContactRecentChange ------

            Mapper.CreateMap<Model.Entities.MediaContactRecentAdd, Service.Dto.MediaContactRecentChange>()
               .ForMember(dest => dest.ContactId, opt => opt.MapFrom(src => src.ContactId))
                .ForMember(dest => dest.OutletId, opt => opt.MapFrom(src => src.OutletId))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                .ForMember(dest => dest.CompanyName, opt => opt.MapFrom(src => src.CompanyName))
                .ForMember(dest => dest.JobTitle, opt => opt.MapFrom(src => src.JobTitle))
                .ForMember(dest => dest.LogoFileName, opt => opt.MapFrom(src => src.LogoFileName))
                .ForMember(dest => dest.Date, opt => opt.MapFrom(src => src.Added))
                .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.RecordType))
                .ForMember(dest => dest.SocialDetails, opt => opt.MapFrom(src => new Dto.SocialInfo
                {
                    Twitter = src.Twitter,
                }))
                .ForMember(dest => dest.MediaType, opt => opt.MapFrom(src => src.MediaType));

            Mapper.CreateMap<Model.Entities.MediaContactRecentUpdate, Service.Dto.MediaContactRecentChange>()
                .ForMember(dest => dest.ContactId, opt => opt.MapFrom(src => src.ContactId))
                .ForMember(dest => dest.OutletId, opt => opt.MapFrom(src => src.OutletId))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                .ForMember(dest => dest.CompanyName, opt => opt.MapFrom(src => src.CompanyName))
                .ForMember(dest => dest.JobTitle, opt => opt.MapFrom(src => src.JobTitle))
                .ForMember(dest => dest.LogoFileName, opt => opt.MapFrom(src => src.LogoFileName))
                .ForMember(dest => dest.Date, opt => opt.MapFrom(src => src.Updated))
                .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.RecordType))
                .ForMember(dest => dest.SocialDetails, opt => opt.MapFrom(src => new Dto.SocialInfo
                  {  
                      Twitter = src.Twitter,
                  }))
                .ForMember(dest => dest.MediaType, opt => opt.MapFrom(src => src.MediaType));

            #endregion

            #region ------ Media Contact View Log ------
            Mapper.CreateMap<Service.Dto.MediaContactViewDto, Model.Entities.MediaContactView>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Model.Entities.MediaContactView, Service.Dto.MediaContactViewDto>()
               .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.PrnContactViewDto, Model.Entities.PrnContactView>()
               .IgnoreAllNonExisting();

            Mapper.CreateMap<Model.Entities.PrnContactView, Service.Dto.PrnContactViewDto>()
               .IgnoreAllNonExisting();

            Mapper.CreateMap<Model.Entities.MediaContactAlsoViewed, Service.Dto.MediaContactAlsoViewedDto>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ MediaDatabaseLog ------
            Mapper.CreateMap<Model.Entities.MediaDatabaseLog, Service.Dto.MediaDatabaseLog>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.ItemID))
                .ForMember(dest => dest.ProfileViewed, opt => opt.MapFrom(src => src.ProfileViewed.HasValue ? src.ProfileViewed.Value : false))
                .ForMember(dest => dest.User, opt => opt.MapFrom(src => Mapper.Map<Model.Entities.User, Service.Dto.User>(src.User)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MediaDatabaseLog, Model.Entities.MediaDatabaseLog>()
                .ForMember(dest => dest.ItemID, opt => opt.MapFrom(src => src.Id))
                .IgnoreAllNonExisting();
            #endregion

            #region ------ MediaRecentlyViewed ------
            Mapper.CreateMap<Model.Entities.MediaRecentlyViewed, Service.Dto.MediaRecentlyViewedDto>();
            #endregion

            #region ------ MediaEmployee ------

            Mapper.CreateMap<Model.Entities.MediaEmployee, Service.Dto.MediaEmployee>()
                .ForMember(dest => dest.CurrentlyOutOfOffice, opt => opt.MapFrom(src => src.CurrentlyOutOfOffice ?? false))
                .ForMember(dest => dest.PreferredDeliveryMethod, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.PreferredDeliveryType>(src.PreferredDeliveryMethod)))
                .ForMember(dest => dest.CityId, opt => opt.MapFrom(src => src.AAPCity.MediaAtlasCityId))
                .ForMember(dest => dest.PostCode, opt => opt.MapFrom(src => src.AAPCity.PostCode))
                .ForMember(dest => dest.RowStatus, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.EmployeeRowStatusType>(src.RowStatus)))
                .ForMember(dest => dest.User, opt => opt.MapFrom(src => Mapper.Map<Model.Entities.User, Service.Dto.User>(src.User)))
                .ForMember(dest => dest.MediaContact, opt => opt.MapFrom(src => Mapper.Map<Model.Entities.MediaContact, Service.Dto.MediaContact>(src.MediaContact)))
                .ForMember(dest => dest.MediaOutlet, opt => opt.MapFrom(src => Mapper.Map<Model.Entities.MediaOutlet, Service.Dto.MediaOutlet>(src.MediaOutlet)))
                .ForMember(dest => dest.Roles, opt => opt.MapFrom(src => Mapper.Map<List<Model.Entities.MediaEmployeeRole>, List<Service.Dto.MediaEmployeeRole>>(src.Roles != null ? src.Roles.ToList() : null)))
                .ForMember(dest => dest.Subjects, opt => opt.MapFrom(src => Mapper.Map<List<Model.Entities.MediaEmployeeSubject>, List<Service.Dto.MediaEmployeeSubject>>(src.Subjects != null ? src.Subjects.ToList() : null)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MediaEmployee, Model.Entities.MediaEmployee>()
                .ForMember(dest => dest.PreferredDeliveryMethod, opt => opt.MapFrom(src => ((char)src.PreferredDeliveryMethod).ToString()))
                .ForMember(dest => dest.RowStatus, opt => opt.MapFrom(src => ((char)src.RowStatus).ToString()))
                .ForMember(dest => dest.User, opt => opt.Ignore())
                .ForMember(dest => dest.MediaContact, opt => opt.MapFrom(src => Mapper.Map<Service.Dto.MediaContact, Model.Entities.MediaContact>(src.MediaContact)))
                .ForMember(dest => dest.MediaOutlet, opt => opt.MapFrom(src => Mapper.Map<Service.Dto.MediaOutlet, Model.Entities.MediaOutlet>(src.MediaOutlet)))
                .ForMember(dest => dest.Roles, opt => opt.MapFrom(src => Mapper.Map<List<Service.Dto.MediaEmployeeRole>, List<Model.Entities.MediaEmployeeRole>>(src.Roles != null ? src.Roles : null)))
                .ForMember(dest => dest.Subjects, opt => opt.MapFrom(src => Mapper.Map<List<Service.Dto.MediaEmployeeSubject>, List<Model.Entities.MediaEmployeeSubject>>(src.Subjects != null ? src.Subjects : null)))
                .IgnoreAllNonExisting();


            Mapper.CreateMap<MediaEmployeeView, Dto.MediaEmployeeView>()
                .ForMember(dest => dest.Mobile, opt => opt.MapFrom(src => src.MobileNumber))
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.PhoneNumber))
                .ForMember(dest => dest.Fax, opt => opt.MapFrom(src => src.FaxNumber))
                .ForMember(dest => dest.BlogUrl, opt => opt.MapFrom(src => src.Blog))
                .ForMember(dest => dest.WebUrl, opt => opt.MapFrom(src => src.Web))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.EmailAddress))
                .ForMember(dest => dest.Profile, opt => opt.MapFrom(src => src.Notes))
                .ForMember(dest => dest.AdditionalMobileNumber, opt => opt.MapFrom(src => src.AdditionalMobile))
                .ForMember(dest => dest.CurrentlyOutOfOffice, opt => opt.MapFrom(src => src.CurrentlyOutOfOffice ?? false))
                .ForMember(dest => dest.PreferredDeliveryMethod, opt => opt.MapFrom(src => string.IsNullOrEmpty(src.DeliveryMethod) ? (object)null : ExtensionMethods.ToEnum<PreferredDeliveryType>(src.DeliveryMethod)))
                .ForMember(dest => dest.BelongsToLists, opt => opt.MapFrom(src => Mapper.Map<List<MediaEmployeeListView>, List<Dto.MediaEmployeeListView>>(src.BelongsToLists)))
                .ForMember(dest => dest.Subjects, opt => opt.MapFrom(src => Mapper.Map<List<MediaEmployeeSubjectView>, List<Dto.MediaEmployeeSubjectView>>(src.Subjects != null ? src.Subjects.ToList() : null)))
                .ForMember(dest => dest.AtOutlet, opt => opt.MapFrom(src => Mapper.Map<MediaEmployeeOutletView, Dto.MediaEmployeeOutletView>(src.AtOutlet)))
                .ForMember(dest => dest.Documents, opt => opt.MapFrom(src => Mapper.Map<List<MediaEmployeeDocumentView>, List<Dto.MediaEmployeeDocumentView>>(src.Documents)))
                .ForMember(dest => dest.SocialDetails, opt => opt.MapFrom(src => new Dto.SocialInfo
                {
                    Facebook = src.Facebook,
                    Instagram = src.Instagram,
                    LinkedIn = src.LinkedIn,
                    SocialInfoName = src.Name,
                    Skype = src.Skype,
                    SnapChat = src.SnapChat,
                    Twitter = src.Twitter,
                    Youtube = src.Youtube,
                    Name = src.FirstName + " " + src.LastName
                }))
                .ForMember(dest => dest.Address, opt => opt.MapFrom(src => new Dto.Address
                {
                    AddressLine1 = src.AddressLine1,
                    AddressLine2 = src.AddressLine2,
                    City = src.City,
                    Country = src.Country,
                    PostCode = src.PostCode,
                    State = src.State
                }))
               .ForMember(dest => dest.MediaContactPitchingProfileDto, opt => opt.MapFrom(src => new Dto.MediaContactPitchingProfileDto
               {
                   AlsoKnownAs = src.AlsoKnownAs,
                   AppearsIn = src.AppearsIn,
                   Awards = src.Awards,
                   BasedInLocation = src.BasedInLocation,
                   BugBears = src.BugBears,
                   CurrentStatus = src.CurrentStatus,
                   Deadlines = src.Deadlines,
                   NamePronunciation = src.NamePronunciation,
                   PersonalInterests = src.PersonalInterests,
                   PoliticalParty = src.PoliticalParty,
                   PressReleaseInterests = src.PressReleaseInterests,
                   BestContactTime = src.BestContactTime,
                   BusyTimes = src.BusyTimes,
                   GiftsAccepted = src.GiftsAccepted,
                   PitchingDos = src.PitchingDos,
                   PitchingDonts = src.PitchingDonts,
                   AuthorOf = src.AuthorOf,
                   CoffeeOrder = src.CoffeeOrder
               }))
              .IgnoreAllNonExisting();

            Mapper.CreateMap<MediaEmployeeDocumentView, Dto.MediaEmployeeDocumentView>()
               .ForMember(dest => dest.UploadedByUserId, opt => opt.MapFrom(src => src.UserId))
               .ForMember(dest => dest.DocumentId, opt => opt.MapFrom(src => src.Id))
               .IgnoreAllNonExisting();

            Mapper.CreateMap<MediaEmployeeOutletView, Dto.MediaEmployeeOutletView>().IgnoreAllNonExisting();
            Mapper.CreateMap<MediaEmployeeSubjectView, Dto.MediaEmployeeSubjectView>().IgnoreAllNonExisting();
            Mapper.CreateMap<MediaEmployeeListView, Dto.MediaEmployeeListView>().IgnoreAllNonExisting();

            #endregion

            #region ------ MediaEmployeeRole ------

            Mapper.CreateMap<Model.Entities.MediaEmployeeRole, Service.Dto.MediaEmployeeRole>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MediaEmployeeRole, Model.Entities.MediaEmployeeRole>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ MediaEmployeeSubject ------

            Mapper.CreateMap<Model.Entities.MediaEmployeeSubject, Service.Dto.MediaEmployeeSubject>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MediaEmployeeSubject, Model.Entities.MediaEmployeeSubject>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ MediaMovement ------

            Mapper.CreateMap<Model.Entities.MediaContactMovement, Service.Dto.MediaMovement>()
                .ForMember(d => d.FirstName, opt => opt.MapFrom(s => s.MediaEmployee.MediaContact != null ? s.MediaEmployee.MediaContact.FirstName : null))
                .ForMember(d => d.LastName, opt => opt.MapFrom(s => s.MediaEmployee.MediaContact != null ? s.MediaEmployee.MediaContact.LastName : null))
                .ForMember(d => d.MiddleName, opt => opt.MapFrom(s => s.MediaEmployee.MediaContact != null ? s.MediaEmployee.MediaContact.MiddleName : null))
                .ForMember(d => d.Twitter, opt => opt.MapFrom(s => s.MediaEmployee.MediaContact != null ? s.MediaEmployee.MediaContact.Twitter : null))
                .ForMember(d => d.CompanyName, opt => opt.MapFrom(s => s.MediaOutlet != null ? s.MediaOutlet.Name : null))
                .ForMember(d => d.LogoFileName, opt => opt.MapFrom(s => s.MediaOutlet != null ? s.MediaOutlet.LogoFileName : null))
                .ForMember(d => d.CreatedByUserName, opt => opt.MapFrom(s => s.User != null ? (s.User.FirstName + " " + s.User.LastName).Trim() : string.Empty))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MediaMovement, Model.Entities.MediaContactMovement>()
                .IgnoreAllNonExisting();


            #endregion

            #region ------ MediaOutletSubject ------

            Mapper.CreateMap<Model.Entities.MediaOutletSubject, Service.Dto.MediaOutletSubject>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MediaOutletSubject, Model.Entities.MediaOutletSubject>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ MediaPriorityNotice ------

            Mapper.CreateMap<Model.Entities.MediaPriorityNotice, Service.Dto.MediaPriorityNotice>()
                .ForMember(d => d.FirstName, opt => opt.MapFrom(s => s.MediaContact != null ? s.MediaContact.FirstName : null))
                .ForMember(d => d.LastName, opt => opt.MapFrom(s => s.MediaContact != null ? s.MediaContact.LastName : null))
                .ForMember(d => d.MiddleName, opt => opt.MapFrom(s => s.MediaContact != null ? s.MediaContact.MiddleName : null))
                .ForMember(d => d.CompanyName, opt => opt.MapFrom(s => s.MediaOutlet != null ? s.MediaOutlet.Name : null))
                .ForMember(d => d.CreatedByUserName, opt => opt.MapFrom(s => s.User != null ? (s.User.FirstName + " " + s.User.LastName).Trim() : string.Empty))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MediaPriorityNotice, Model.Entities.MediaPriorityNotice>()
                .IgnoreAllNonExisting();


            #endregion

            #region ------ MediaRelatedOutlet ------

            Mapper.CreateMap<Model.Entities.MediaRelatedOutlet, Service.Dto.MediaRelatedOutlet>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MediaRelatedOutlet, Model.Entities.MediaRelatedOutlet>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ MediaSyndicatedOutlet ------

            Mapper.CreateMap<Model.Entities.MediaSyndicatedOutlet, Service.Dto.MediaSyndicatedOutlet>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MediaSyndicatedOutlet, Model.Entities.MediaSyndicatedOutlet>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ MediaOutletView ------
            Mapper.CreateMap<Service.Dto.MediaOutletViewDto, Model.Entities.MediaOutletView>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Model.Entities.MediaOutletView, Service.Dto.MediaOutletViewDto>()
               .IgnoreAllNonExisting();
            #endregion

            #region ------ MediaOutletWorkingLanguage ------

            Mapper.CreateMap<Model.Entities.MediaOutletWorkingLanguage, Service.Dto.MediaOutletWorkingLanguage>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MediaOutletWorkingLanguage, Model.Entities.MediaOutletWorkingLanguage>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ MediaOutletStationFormat ------

            Mapper.CreateMap<Model.Entities.MediaOutletStationFormat, Service.Dto.MediaOutletStationFormat>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MediaOutletStationFormat, Model.Entities.MediaOutletStationFormat>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ MediaOutlet ------

            Mapper.CreateMap<Model.Entities.MediaOutlet, Service.Dto.MediaOutlet>()
                .ForMember(dest => dest.PreferredDeliveryMethod, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.PreferredDeliveryType>(src.PreferredDeliveryMethod)))
                .ForMember(dest => dest.CityId, opt => opt.MapFrom(src => src.AAPCity.MediaAtlasCityId))
                .ForMember(dest => dest.PostCode, opt => opt.MapFrom(src => src.AAPCity.PostCode))
                .ForMember(dest => dest.PostalCityId, opt => opt.MapFrom(src => src.AAPPostalCity.MediaAtlasCityId))
                .ForMember(dest => dest.PostalPostCode, opt => opt.MapFrom(src => src.AAPPostalCity.PostCode))
                .ForMember(dest => dest.RowStatus, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.OutletRowStatusType>(src.RowStatus)))
                .ForMember(dest => dest.User, opt => opt.MapFrom(src => Mapper.Map<Model.Entities.User, Service.Dto.User>(src.User)))
                .ForMember(dest => dest.Profile, opt => opt.MapFrom(src => (string)(src.Profile != null ? src.Profile.Content : "")))
                .ForMember(dest => dest.StationFormats, opt => opt.MapFrom(src => Mapper.Map<List<Model.Entities.MediaOutletStationFormat>, List<Service.Dto.MediaOutletStationFormat>>(src.StationFormats != null ? src.StationFormats.ToList() : null)))
                .ForMember(dest => dest.Subjects, opt => opt.MapFrom(src => Mapper.Map<List<Model.Entities.MediaOutletSubject>, List<Service.Dto.MediaOutletSubject>>(src.Subjects != null ? src.Subjects.ToList() : null)))
                .ForMember(dest => dest.WorkingLanguages, opt => opt.MapFrom(src => Mapper.Map<List<Model.Entities.MediaOutletWorkingLanguage>, List<Service.Dto.MediaOutletWorkingLanguage>>(src.WorkingLanguages != null ? src.WorkingLanguages.ToList() : null)))
                .ForMember(dest => dest.RelatedOutlets, opt => opt.MapFrom(src => Mapper.Map<List<Model.Entities.MediaRelatedOutlet>, List<Service.Dto.MediaRelatedOutlet>>(src.RelatedOutlets != null ? src.RelatedOutlets.ToList() : null)))
                .ForMember(dest => dest.SyndicatedOutlets, opt => opt.MapFrom(src => Mapper.Map<List<Model.Entities.MediaSyndicatedOutlet>, List<Service.Dto.MediaSyndicatedOutlet>>(src.SyndicatedOutlets != null ? src.SyndicatedOutlets.ToList() : null)))
                .ForMember(dest => dest.HasReceivedLegals, opt => opt.MapFrom(src => (bool)(src.HasReceivedLegals != null ? src.HasReceivedLegals : false)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MediaOutlet, Model.Entities.MediaOutlet>()
                .ForMember(dest => dest.PreferredDeliveryMethod, opt => opt.MapFrom(src => ((char)src.PreferredDeliveryMethod).ToString()))
                .ForMember(dest => dest.RowStatus, opt => opt.MapFrom(src => ((char)src.RowStatus).ToString()))
                .ForMember(dest => dest.User, opt => opt.Ignore())
                .ForMember(dest => dest.Profile, opt => opt.Ignore())
                .ForMember(dest => dest.StationFormats, opt => opt.MapFrom(src => Mapper.Map<List<Service.Dto.MediaOutletStationFormat>, List<Model.Entities.MediaOutletStationFormat>>(src.StationFormats != null ? src.StationFormats.ToList() : null)))
                .ForMember(dest => dest.Subjects, opt => opt.MapFrom(src => Mapper.Map<List<Service.Dto.MediaOutletSubject>, List<Model.Entities.MediaOutletSubject>>(src.Subjects != null ? src.Subjects.ToList() : null)))
                .ForMember(dest => dest.WorkingLanguages, opt => opt.MapFrom(src => Mapper.Map<List<Service.Dto.MediaOutletWorkingLanguage>, List<Model.Entities.MediaOutletWorkingLanguage>>(src.WorkingLanguages != null ? src.WorkingLanguages.ToList() : null)))
                 .ForMember(dest => dest.RelatedOutlets, opt => opt.MapFrom(src => Mapper.Map<List<Service.Dto.MediaRelatedOutlet>, List<Model.Entities.MediaRelatedOutlet>>(src.RelatedOutlets != null ? src.RelatedOutlets.ToList() : null)))
                 .ForMember(dest => dest.SyndicatedOutlets, opt => opt.MapFrom(src => Mapper.Map<List<Service.Dto.MediaSyndicatedOutlet>, List<Model.Entities.MediaSyndicatedOutlet>>(src.SyndicatedOutlets != null ? src.SyndicatedOutlets.ToList() : null)))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ MediaOutletPartial ------

            Mapper.CreateMap<Model.Entities.MediaOutlet, Service.Dto.MediaOutletPartial>()
                 .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                 .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                  .ForMember(dest => dest.ProductTypeId, opt => opt.MapFrom(src => src.ProductTypeId))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MediaOutletPartial, Model.Entities.MediaOutlet>()
                 .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                 .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                  .ForMember(dest => dest.ProductTypeId, opt => opt.MapFrom(src => src.ProductTypeId))
               .IgnoreAllNonExisting();

            #endregion

            #region ------ MediaOutletProfile ------

            Mapper.CreateMap<Model.Entities.MediaOutletProfile, Service.Dto.MediaOutletProfile>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MediaOutletProfile, Model.Entities.MediaOutletProfile>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ MediaStatistics ------

            Mapper.CreateMap<Model.Entities.MediaStatistics, Service.Dto.MediaStatistics>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MediaStatistics, Model.Entities.MediaStatistics>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Monitoring ------

            Mapper.CreateMap<Model.Entities.MonitoringMediaType, Service.Dto.MonitoringMediaType>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MonitoringMediaType, Model.Entities.MonitoringMediaType>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Model.Entities.MonitoringSearch, Service.Dto.MonitoringSearch>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MonitoringSearch, Model.Entities.MonitoringSearch>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Model.Entities.MonitoringCampaign, Service.Dto.MonitoringCampaign>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MonitoringCampaign, Model.Entities.MonitoringCampaign>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Model.Entities.MonitoringEmail, Service.Dto.MonitoringEmail>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MonitoringEmail, Model.Entities.MonitoringEmail>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Model.Entities.MonitoringAlert, Service.Dto.MonitoringAlert>()
                 .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MonitoringAlert, Model.Entities.MonitoringAlert>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Model.Entities.MonitoringFrequency, Service.Dto.MonitoringFrequency>()
                 .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MonitoringFrequency, Model.Entities.MonitoringFrequency>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ NewAccountRequest to Account ------

            Mapper.CreateMap<Service.Dto.NewAccountRequest, Service.Dto.Account>()
                .ForMember(dest => dest.CompanyAddress, opt => opt.MapFrom(src => Mapper.Map<Service.Dto.NewAccountRequest, Service.Dto.AccountAddress>(src)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.NewAccountRequest, Service.Dto.AccountAddress>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ News Focus ------

            Mapper.CreateMap<Model.Entities.NewsFocus, Service.Dto.NewsFocus>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.NewsFocus, Model.Entities.NewsFocus>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Outlet Frequency ------

            Mapper.CreateMap<Model.Entities.OutletFrequency, Service.Dto.OutletFrequency>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.OutletFrequency, Model.Entities.OutletFrequency>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ OutletType ------

            Mapper.CreateMap<Model.Entities.OutletType, Service.Dto.OutletType>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.OutletType, Model.Entities.OutletType>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ PackageList ------

            Mapper.CreateMap<Model.Entities.PackageList, Service.Dto.PackageList>()
                .ForMember(dest => dest.System, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.SystemType>(src.System, Common.SystemType.Medianet)))
                .ForMember(dest => dest.TransactionType, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.DistributionType>(src.TransactionType, Common.DistributionType.Unknown)))
                .ForMember(dest => dest.RowStatus, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.RowStatusType>(src.RowStatus)))
                .ForMember(dest => dest.IsVisible, opt => opt.MapFrom(src => (src.IsHidden.StartsWith("T") ? false : true)))
                .ForMember(dest => dest.IsTopValue, opt => opt.MapFrom(src => (src.IsTopValue.HasValue ? src.IsTopValue.Value : false)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.PackageList, Model.Entities.PackageList>()
                .ForMember(dest => dest.System, opt => opt.MapFrom(src => ((char)src.System).ToString()))
                .ForMember(dest => dest.TransactionType, opt => opt.MapFrom(src => ((char)src.TransactionType).ToString()))
                .ForMember(dest => dest.RowStatus, opt => opt.MapFrom(src => ((char)src.RowStatus).ToString()))
                .ForMember(dest => dest.IsHidden, opt => opt.MapFrom(src => (src.IsVisible ? "F" : "T")))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ ProductType ------

            Mapper.CreateMap<Model.Entities.ProductType, Service.Dto.ProductType>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.ProductType, Model.Entities.ProductType>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Recipient ------

            Mapper.CreateMap<Model.Entities.Recipient, Service.Dto.Recipient>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.Recipient, Model.Entities.Recipient>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ RegistrationValidation ------

            Mapper.CreateMap<Model.Entities.RegistrationValidation, Service.Dto.RegistrationValidation>()
                .ForMember(dest => dest.System, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.SystemType>(src.System)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.RegistrationValidation, Model.Entities.RegistrationValidation>()
                .ForMember(dest => dest.System, opt => opt.MapFrom(src => ((char)src.System).ToString()))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Release ------

            Mapper.CreateMap<Model.Entities.Release, Service.Dto.Release>()
                .ForMember(dest => dest.DebtorNumber, opt => opt.MapFrom(src => src.DebtorNumber.TrimEnd()))
                .ForMember(dest => dest.System, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.SystemType>(src.System, Common.SystemType.Medianet)))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.ReleaseStatusType>(src.Status)))
                .ForMember(dest => dest.IsOpen, opt => opt.MapFrom(src => (src.IsOpen == 1 ? true : false)))
                .ForMember(dest => dest.IsInQueue, opt => opt.MapFrom(src => (src.IsInQueue == 1 ? true : false)))
                .ForMember(dest => dest.Priority, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.ReleasePriorityType>(src.Priority, Common.ReleasePriorityType.Standard)))
                .ForMember(dest => dest.OCRStatus, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.OCRConversionStatusType>(src.OCRStatus, Common.OCRConversionStatusType.None)))
                .ForMember(dest => dest.ReceptionMethod, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.ReceptionType>(src.ReceptionMethod)))
                .ForMember(dest => dest.ReportResultsToSend, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.ResultsType>(src.ReportResultsToSend, ResultsType.All)))
                .ForMember(dest => dest.SecurityKey, opt => opt.MapFrom(src => src.SecurityKey.ToString()))
                .ForMember(dest => dest.IsResend, opt => opt.MapFrom(src => src.IsResend.HasValue ? src.IsResend.Value : false))
                .ForMember(dest => dest.MultimediaType, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.MultimediaType>(src.MultimediaType, MultimediaType.None)))
                .ForMember(dest => dest.NextStatus, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.ReleaseStatusType>(src.NextStatus, ReleaseStatusType.None)))
                .ForMember(dest => dest.WebDetails, opt => opt.MapFrom(src => Mapper.Map<Model.Entities.WebRelease, Service.Dto.WebRelease>(src.WebDetails)))
                //.ForMember(dest => dest.Documents, opt => opt.MapFrom(src => Mapper.Map<List<Model.Entities.Document>, List<Service.Dto.Document>>(src.Documents)))
                //.ForMember(dest => dest.Transactions, opt => opt.MapFrom(src => Mapper.Map<List<Model.Entities.Transaction>, List<Service.Dto.Transaction>>(src.Transactions)))
                .ForMember(dest => dest.Documents, opt => opt.Ignore())
                .ForMember(dest => dest.Transactions, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedByUser, opt => opt.MapFrom(src => Mapper.Map<Model.Entities.User, Service.Dto.UserSummary>(src.CreatedByUser)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.Release, Model.Entities.Release>()
                .ForMember(dest => dest.System, opt => opt.MapFrom(src => ((char)src.System).ToString()))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => ((char)src.Status).ToString()))
                .ForMember(dest => dest.IsOpen, opt => opt.MapFrom(src => (src.IsOpen ? 1 : 0)))
                .ForMember(dest => dest.IsInQueue, opt => opt.MapFrom(src => (src.IsInQueue ? 1 : 0)))
                .ForMember(dest => dest.HasHoldDate, opt => opt.MapFrom(src => (src.HoldUntilDate.HasValue ? true : false)))
                .ForMember(dest => dest.HasEmbargoDate, opt => opt.MapFrom(src => (src.EmbargoUntilDate.HasValue ? true : false)))
                .ForMember(dest => dest.Priority, opt => opt.MapFrom(src => ((char)src.Priority).ToString()))
                .ForMember(dest => dest.OCRStatus, opt => opt.MapFrom(src => ((char)src.OCRStatus).ToString()))
                .ForMember(dest => dest.ReceptionMethod, opt => opt.MapFrom(src => ((char)src.ReceptionMethod).ToString()))
                .ForMember(dest => dest.ReportResultsToSend, opt => opt.MapFrom(src => ((char)src.ReportResultsToSend).ToString()))
                .ForMember(dest => dest.SecurityKey, opt => opt.MapFrom(src => int.Parse(src.SecurityKey)))
                .ForMember(dest => dest.MultimediaType, opt => opt.MapFrom(src => ((char)src.MultimediaType).ToString()))
                .ForMember(dest => dest.NextStatus, opt => opt.MapFrom(src => ((char)src.NextStatus).ToString()))
                //.ForMember(dest => dest.WebDetails, opt => opt.MapFrom(src => Mapper.Map<Service.Dto.WebRelease, Model.Entities.WebRelease>(src.WebDetails)))
                //.ForMember(dest => dest.Documents, opt => opt.MapFrom(src => Mapper.Map<List<Service.Dto.Document>, List<Model.Entities.Document>>(src.Documents)))
                //.ForMember(dest => dest.Transactions, opt => opt.MapFrom(src => Mapper.Map<List<Service.Dto.Transaction>, List<Model.Entities.Transaction>>(src.Transactions)))
                .ForMember(dest => dest.WebDetails, opt => opt.Ignore())
                .ForMember(dest => dest.Documents, opt => opt.Ignore())
                .ForMember(dest => dest.Transactions, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedByUser, opt => opt.Ignore())
                .IgnoreAllNonExisting();

            #endregion

            #region ------ ReleaseSummary to Release ------

            Mapper.CreateMap<Service.Dto.ReleaseSummary, Service.Dto.Release>()
                .ForMember(dest => dest.Transactions, opt => opt.Ignore())
                .ForMember(dest => dest.Documents, opt => opt.Ignore())
                .ForMember(dest => dest.WebDetails, opt => opt.MapFrom(src => Mapper.Map<Service.Dto.ReleaseSummary, Service.Dto.WebRelease>(src)))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ ReleaseSummary to WebRelease ------

            Mapper.CreateMap<Service.Dto.ReleaseSummary, Service.Dto.WebRelease>()
                .ForMember(dest => dest.Headline, opt => opt.MapFrom(src => src.ReleaseDescription))
                .ForMember(dest => dest.PrimaryWebCategoryId, opt => opt.MapFrom(src => src.WebCategoryId))
                .ForMember(dest => dest.SecondaryWebCategoryId, opt => opt.MapFrom(src => src.SecondaryWebCategoryId))
                .ForMember(dest => dest.IsVerified, opt => opt.MapFrom(src => src.IsVerified))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ ReleaseCalculatorRequest to Release ------

            Mapper.CreateMap<Service.Dto.ReleaseCalculatorRequest, Service.Dto.Release>()
                .ForMember(dest => dest.Transactions, opt => opt.Ignore())
                .ForMember(dest => dest.Documents, opt => opt.Ignore())
                .ForMember(dest => dest.WebDetails, opt => opt.MapFrom(src => Mapper.Map<Service.Dto.ReleaseCalculatorRequest, Service.Dto.WebRelease>(src)))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ ReleaseCalculatorRequest to WebRelease ------

            Mapper.CreateMap<Service.Dto.ReleaseCalculatorRequest, Service.Dto.WebRelease>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ ReleaseQuoteRequest to Release ------

            Mapper.CreateMap<Service.Dto.ReleaseQuoteRequest, Service.Dto.Release>()
                .ForMember(dest => dest.Transactions, opt => opt.Ignore())
                .ForMember(dest => dest.Documents, opt => opt.Ignore())
                .ForMember(dest => dest.WebDetails, opt => opt.MapFrom(src => Mapper.Map<Service.Dto.ReleaseQuoteRequest, Service.Dto.WebRelease>(src)))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ ReleaseQuoteRequest to WebRelease ------

            Mapper.CreateMap<Service.Dto.ReleaseQuoteRequest, Service.Dto.WebRelease>()
                .ForMember(dest => dest.Headline, opt => opt.MapFrom(src => src.ReleaseDescription))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Release to ReleasePublic ------

            Mapper.CreateMap<Model.Entities.Release, Service.Dto.ReleasePublic>()
                .ForMember(dest => dest.Headline, opt => opt.MapFrom(src => src.WebDetails.Headline))
                .ForMember(dest => dest.Organisation, opt => opt.MapFrom(src => src.WebDetails.Organisation))
                .ForMember(dest => dest.WebCategoryId, opt => opt.MapFrom(src => src.WebDetails.PrimaryWebCategoryId))
                .ForMember(dest => dest.EmbargoUntilDate, opt => opt.MapFrom(src => src.EmbargoUntilDate))
                .ForMember(dest => dest.MultimediaType, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.MultimediaType>(src.MultimediaType, MultimediaType.None)))
                .ForMember(dest => dest.Summary, opt => opt.MapFrom(src => src.WebDetails.Summary))
                .ForMember(dest => dest.ShortUrl, opt => opt.MapFrom(src => src.WebDetails.ShortUrl))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ ReleaseSummaryPublic ------

            Mapper.CreateMap<ReleaseSummaryPublic, Dto.ReleaseSummaryPublic>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.JobId))
                .ForMember(dest => dest.WebCategoryId, opt => opt.MapFrom(src => src.PrimaryWebCategoryId))
                .ForMember(dest => dest.SecondaryWebCategoryId, opt => opt.MapFrom(src => src.SecondaryWebCategoryId))
                .ForMember(dest => dest.MultimediaType, opt => opt.MapFrom(src => ExtensionMethods.ToEnum(src.MultimediaType, MultimediaType.None)))
                .ForMember(dest => dest.DistributedDate, opt => opt.MapFrom(src => src.WhenDistributed))
                .ForMember(dest => dest.IsVerified, opt => opt.MapFrom(src => src.IsVerified.HasValue ? src.IsVerified : false))
                .ForMember(dest => dest.SecurityKey, opt => opt.Ignore()) // Don't auto copy the SecurityKey. It's only copied for Journalists
                .IgnoreAllNonExisting();

            #endregion

            #region ------ ReleaseUnverifiedPreview ------
            Mapper.CreateMap<Model.Entities.ReleaseUnverifiedPreview, Service.Dto.ReleaseUnverifiedPreview>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.ReleaseUnverifiedPreview, Model.Entities.ReleaseUnverifiedPreview>()
                .IgnoreAllNonExisting();
            #endregion

            #region ------ Result ------

            Mapper.CreateMap<Model.Entities.Result, Service.Dto.Result>()
                .ForMember(dest => dest.GroupNumber, opt => opt.MapFrom(src => (int)src.GroupNumber))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.TransactionStatusType>(src.Status)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.Result, Model.Entities.Result>()
                .ForMember(dest => dest.GroupNumber, opt => opt.MapFrom(src => (Int16)src.GroupNumber))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => ((char)src.Status).ToString()))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Sales Region ------

            Mapper.CreateMap<Model.Entities.SalesRegion, Service.Dto.SalesRegion>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.SalesRegion, Model.Entities.SalesRegion>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ ServiceList ------

            Mapper.CreateMap<Model.Entities.ServiceList, Service.Dto.ServiceList>()
                .ForMember(dest => dest.DebtorNumber, opt => opt.MapFrom(src => src.DebtorNumber.TrimEnd()))
                .ForMember(dest => dest.System, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.SystemType>(src.System, Common.SystemType.Medianet)))
                .ForMember(dest => dest.DistributionType, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.DistributionType>(src.DistributionType)))
                .ForMember(dest => dest.SequenceNumber, opt => opt.MapFrom(src => (int)src.SequenceNumber))
                .ForMember(dest => dest.TransactionType, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.DistributionType>(src.TransactionType)))
                .ForMember(dest => dest.AddressCount, opt => opt.MapFrom(src => (int)src.AddressCount))
                .ForMember(dest => dest.PageAdjustment, opt => opt.MapFrom(src => (int)src.PageAdjustment))
                .ForMember(dest => dest.IsVisible, opt => opt.MapFrom(src => (src.IsHidden.StartsWith("T") ? false : true)))
                .ForMember(dest => dest.IsOnlineDB, opt => opt.MapFrom(src => (src.IsOnlineDB.StartsWith("Y") ? true : false)))
                .ForMember(dest => dest.IsMediaList, opt => opt.MapFrom(src => (src.IsMediaList.HasValue ? src.IsMediaList : false)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.ServiceList, Model.Entities.ServiceList>()
                .ForMember(dest => dest.System, opt => opt.MapFrom(src => ((char)src.System).ToString()))
                .ForMember(dest => dest.DistributionType, opt => opt.MapFrom(src => ((char)src.DistributionType).ToString()))
                .ForMember(dest => dest.SequenceNumber, opt => opt.MapFrom(src => (Int16)src.SequenceNumber))
                .ForMember(dest => dest.TransactionType, opt => opt.MapFrom(src => ((char)src.TransactionType).ToString()))
                .ForMember(dest => dest.AddressCount, opt => opt.MapFrom(src => (Int16)src.AddressCount))
                .ForMember(dest => dest.PageAdjustment, opt => opt.MapFrom(src => (Int16)src.PageAdjustment))
                .ForMember(dest => dest.IsHidden, opt => opt.MapFrom(src => (src.IsVisible ? "F" : "T")))
                .ForMember(dest => dest.IsOnlineDB, opt => opt.MapFrom(src => (src.IsOnlineDB ? "Y" : "N")))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ ServiceListSummary ------

            Mapper.CreateMap<Model.Entities.ServiceListSummary, Service.Dto.ServiceListSummary>()
                .ForMember(dest => dest.DistributionType, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.DistributionType>(src.DistributionType)))
                .ForMember(dest => dest.SequenceNumber, opt => opt.MapFrom(src => (int)src.SequenceNumber))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ ServiceListUpload to ServiceList ------

            Mapper.CreateMap<Service.Dto.ServiceListUpload, Service.Dto.ServiceList>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ State ------

            Mapper.CreateMap<Model.Entities.State, Service.Dto.State>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.StateId))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.StateName))
                .ForMember(dest => dest.CountryId, opt => opt.MapFrom(src => src.MediaAtlasCountryId))
                .ForMember(dest => dest.CountryName, opt => opt.MapFrom(src => src.CountryName))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.State, Model.Entities.State>()
                .ForMember(dest => dest.StateId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.StateName, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.MediaAtlasCountryId, opt => opt.MapFrom(src => src.CountryId))
                .ForMember(dest => dest.CountryName, opt => opt.MapFrom(src => src.CountryName))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Station Format ------

            Mapper.CreateMap<Model.Entities.StationFormat, Service.Dto.StationFormat>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.StationFormat, Model.Entities.StationFormat>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Subject ------

            Mapper.CreateMap<Model.Entities.Subject, Service.Dto.Subject>()
                .ForMember(dest => dest.subjectGroupId, opt => opt.MapFrom(src => src.SubjectGroups.Count > 0 ? (int?)(src.SubjectGroups.First().Id) : null))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.Subject, Model.Entities.Subject>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Subject Group ------

            Mapper.CreateMap<Model.Entities.SubjectGroup, Service.Dto.SubjectGroup>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.SubjectGroup, Model.Entities.SubjectGroup>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ TempFile ------

            Mapper.CreateMap<Model.Entities.TempFile, Service.Dto.TempFile>()
                .ForMember(dest => dest.SessionKey, opt => opt.MapFrom(src => src.SessionId.ToString()))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.TempFile, Model.Entities.TempFile>()
                .ForMember(dest => dest.SessionId, opt => opt.MapFrom(src => int.Parse(src.SessionKey)))
                .ForMember(dest => dest.SavedFile, opt => opt.MapFrom(src => string.Empty))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ TempFile to FileContentResponse ------

            Mapper.CreateMap<Service.Dto.TempFile, Service.Dto.FileContentResponse>()
                .ForMember(dest => dest.FileId, opt => opt.MapFrom(src => src.Id))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ TempFileStreamResponse ------

            Mapper.CreateMap<Service.Dto.TempFile, Service.Dto.FileContentResponse>()
                .ForMember(dest => dest.FileId, opt => opt.MapFrom(src => src.Id))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Thumbnail ------

            Mapper.CreateMap<Model.Entities.Thumbnail, Service.Dto.Thumbnail>()
                .ForMember(dest => dest.FileExtension, opt => opt.MapFrom(src => src.FileExtension.TrimEnd()))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.Thumbnail, Model.Entities.Thumbnail>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Timezone ------

            Mapper.CreateMap<Model.Entities.Timezone, Service.Dto.Timezone>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.Timezone, Model.Entities.Timezone>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ TopPerformingList ------

            Mapper.CreateMap<Model.Entities.TopPerformingList, Service.Dto.TopPerformingList>()
                .ForMember(dest => dest.Period, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.PeriodType>(src.Period, Common.PeriodType.Month)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.TopPerformingList, Model.Entities.TopPerformingList>()
                .ForMember(dest => dest.Period, opt => opt.MapFrom(src => ((char)src.Period).ToString()))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Transaction ------

            Mapper.CreateMap<Model.Entities.Transaction, Service.Dto.Transaction>()
                .ForMember(dest => dest.DebtorNumber, opt => opt.MapFrom(src => src.DebtorNumber.TrimEnd()))
                .ForMember(dest => dest.SequenceNumber, opt => opt.MapFrom(src => (int)src.SequenceNumber))
                .ForMember(dest => dest.GroupNumber, opt => opt.MapFrom(src => (int)src.GroupNumber))
                .ForMember(dest => dest.System, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.SystemType>(src.System, Common.SystemType.Medianet)))
                .ForMember(dest => dest.DistributionType, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.DistributionType>(src.DistributionType)))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.TransactionStatusType>(src.Status)))
                .ForMember(dest => dest.TransactionType, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.DistributionType>(src.TransactionType)))
                .ForMember(dest => dest.DistributionSystem, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.DistributionSystemType>(src.DistributionSystem, DistributionSystemType.Undefined)))
                .ForMember(dest => dest.PageAdjustment, opt => opt.MapFrom(src => (int)src.PageAdjustment))
                .ForMember(dest => dest.IsMediaList, opt => opt.MapFrom(src => (src.IsMediaList.HasValue ? src.IsMediaList : false)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.Transaction, Model.Entities.Transaction>()
                .ForMember(dest => dest.SequenceNumber, opt => opt.MapFrom(src => (Int16)src.SequenceNumber))
                .ForMember(dest => dest.GroupNumber, opt => opt.MapFrom(src => (Int16)src.GroupNumber))
                .ForMember(dest => dest.System, opt => opt.MapFrom(src => ((char)src.System).ToString()))
                .ForMember(dest => dest.DistributionType, opt => opt.MapFrom(src => ((char)src.DistributionType).ToString()))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => ((char)src.Status).ToString()))
                .ForMember(dest => dest.TransactionType, opt => opt.MapFrom(src => ((char)src.TransactionType).ToString()))
                .ForMember(dest => dest.DistributionSystem, opt => opt.MapFrom(src => ((char)src.DistributionSystem).ToString()))
                .ForMember(dest => dest.PageAdjustment, opt => opt.MapFrom(src => (Int16)src.PageAdjustment))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ FinancePaymentService.TransactionPriceDetail to TransactionQuoteResponse ------

            Mapper.CreateMap<FinancePaymentService.TransactionPriceDetail, Service.Dto.TransactionQuoteResponse>()
                .ForMember(dest => dest.SequenceNumber, opt => opt.MapFrom(src => src.MNTransactionID))
                .ForMember(dest => dest.AccountCode, opt => opt.MapFrom(src => src.ListCode))
                .ForMember(dest => dest.Quantity, opt => opt.MapFrom(src => src.Qty))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ TransactionStatistics ------

            Mapper.CreateMap<Model.Entities.TransactionStatistics, Service.Dto.TransactionStatistics>()
                .ForMember(dest => dest.SequenceNumber, opt => opt.MapFrom(src => (int)src.SequenceNumber))
                .ForMember(dest => dest.DistributionType, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.DistributionType>(src.DistributionType)))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.TransactionStatusType>(src.Status)))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ TransactionSummary to TransactionQuoteResponse ------

            Mapper.CreateMap<Service.Dto.TransactionSummary, Service.Dto.TransactionQuoteResponse>()
                .ForMember(dest => dest.SequenceNumber, opt => opt.MapFrom(src => src.SequenceNumber))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Transaction to TransactionSummary ------

            Mapper.CreateMap<Service.Dto.Transaction, Service.Dto.TransactionSummary>()
                .IgnoreAllNonExisting();

            #endregion


            #region ------ TrainingCourse ------

            Mapper.CreateMap<Model.Entities.TrainingCourse, Service.Dto.TrainingCourse>()
                .ForMember(dest => dest.EventType, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.EventType>(src.EventType)))
                .ForMember(dest => dest.Contact, opt => opt.MapFrom(src => src.Contact != null ?
                    AutoMapper.Mapper.Map<Model.Entities.TrainingCourseContact, Service.Dto.TrainingCourseContact>(src.Contact.FirstOrDefault()) : null))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.TrainingCourse, Model.Entities.TrainingCourse>()
                .ForMember(dest => dest.EventType, opt => opt.MapFrom(src => ((char)src.EventType).ToString()))
                .ForMember(dest => dest.Contact, opt => opt.Ignore())
                .IgnoreAllNonExisting();

            #endregion

            #region ------ TrainingCourseLocation ------

            Mapper.CreateMap<Model.Entities.TrainingCourseLocation, Service.Dto.TrainingCourseLocation>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.TrainingCourseLocation, Model.Entities.TrainingCourseLocation>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ TrainingCourseSchedule ------

            Mapper.CreateMap<Model.Entities.TrainingCourseSchedule, Service.Dto.TrainingCourseSchedule>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.TrainingCourseSchedule, Model.Entities.TrainingCourseSchedule>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ TrainingCourseContact ------

            Mapper.CreateMap<Model.Entities.TrainingCourseContact, Service.Dto.TrainingCourseContact>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.TrainingCourseContact, Model.Entities.TrainingCourseContact>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ TrainingCourseEnrolment ------

            Mapper.CreateMap<Model.Entities.TrainingCourseEnrolment, Service.Dto.TrainingCourseEnrolment>()
                .ForMember(dest => dest.RowStatus, opt => opt.MapFrom(src => src.Status == 0 ? Common.RowStatusType.Active : Common.RowStatusType.Deleted))
                .ForMember(dest => dest.TotalCost, opt => opt.MapFrom(src => Convert.ToDecimal(src.TotalCost)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.TrainingCourseEnrolment, Model.Entities.TrainingCourseEnrolment>()
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.RowStatus == Common.RowStatusType.Active ? 0 : 1))
                .ForMember(dest => dest.TotalCost, opt => opt.MapFrom(src => Convert.ToDouble(src.TotalCost)))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ TrainingCoursePromoCode ------

            Mapper.CreateMap<Model.Entities.TrainingCoursePromoCode, Service.Dto.TrainingCoursePromoCode>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.TrainingCoursePromoCode, Model.Entities.TrainingCoursePromoCode>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ TrainingCourseTopic ------

            Mapper.CreateMap<Model.Entities.TrainingCourseTopic, Service.Dto.TrainingCourseTopic>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.TrainingCourseTopic, Model.Entities.TrainingCourseTopic>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ TwitterCategory ------

            Mapper.CreateMap<Model.Entities.TwitterCategory, Service.Dto.TwitterCategory>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.TwitterCategory, Model.Entities.TwitterCategory>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ User ------

            Mapper.CreateMap<Model.Entities.User, Service.Dto.User>()
                .ForMember(dest => dest.DebtorNumber, opt => opt.MapFrom(src => src.DebtorNumber.TrimEnd()))
                .ForMember(dest => dest.UnsubscribeKey, opt => opt.MapFrom(src => src.UnsubscribeKey))
                .ForMember(dest => dest.DefaultJobPriority, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.ReleasePriorityType>(src.DefaultJobPriority)))
                .ForMember(dest => dest.MustChangePassword, opt => opt.MapFrom(src => src.MustChangePassword.HasValue ? src.MustChangePassword : false))
                .ForMember(dest => dest.ReportSendMethod, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.ReportType>(src.ReportSendMethod)))
                .ForMember(dest => dest.ReportResultsToSend, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.ResultsType>(src.ReportResultsToSend)))
                .ForMember(dest => dest.MNUserAccessRights, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.AdminAccessType>(src.MNUserAccessRights)))
                .ForMember(dest => dest.MessageConnectAccessRights, opt => opt.MapFrom(src => (src.HasMessageConnectAdminAccess ? MessageConnectAccessType.Administrator : (src.HasMessageConnectWebAccess ? MessageConnectAccessType.Operator : MessageConnectAccessType.None))))
                .ForMember(dest => dest.ContactsAccessRights, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.ContactsAccessType>(src.ContactsAccessRights, ContactsAccessType.None)))
                .ForMember(dest => dest.LogonFailureCount, opt => opt.MapFrom(src => (int)src.LogonFailureCount))
                .ForMember(dest => dest.RowStatus, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.RowStatusType>(src.RowStatus)))
                .ForMember(dest => dest.MonitorInternet, opt => opt.MapFrom(src => (src.MonitorInternet == null ? false : src.MonitorInternet.Value)))
                .ForMember(dest => dest.MonitorText, opt => opt.MapFrom(src => (src.MonitorText == null ? false : src.MonitorText.Value)))
                .ForMember(dest => dest.MonitorWire, opt => opt.MapFrom(src => (src.MonitorWire == null ? false : src.MonitorWire.Value)))
                .ForMember(dest => dest.RecentLogonCount, opt => opt.MapFrom(src => (src.RecentLogonCount.HasValue ? src.RecentLogonCount : 0)))
                .ForMember(dest => dest.HasViewedDistributionMessage, opt => opt.MapFrom(src => (src.HasViewedDistributionMessage.HasValue ? src.HasViewedDistributionMessage : false)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.User, Model.Entities.User>()
                .ForMember(dest => dest.DefaultJobPriority, opt => opt.MapFrom(src => ((char)src.DefaultJobPriority).ToString()))
                .ForMember(dest => dest.ReportSendMethod, opt => opt.MapFrom(src => ((char)src.ReportSendMethod).ToString()))
                .ForMember(dest => dest.ReportResultsToSend, opt => opt.MapFrom(src => ((char)src.ReportResultsToSend).ToString()))
                .ForMember(dest => dest.MNUserAccessRights, opt => opt.MapFrom(src => ((char)src.MNUserAccessRights).ToString()))
                .ForMember(dest => dest.HasMessageConnectAdminAccess, opt => opt.MapFrom(src => (src.MessageConnectAccessRights == MessageConnectAccessType.Administrator ? true : false)))
                .ForMember(dest => dest.ContactsAccessRights, opt => opt.MapFrom(src => ((char)src.ContactsAccessRights).ToString()))
                .ForMember(dest => dest.LogonFailureCount, opt => opt.MapFrom(src => (Int16)src.LogonFailureCount))
                .ForMember(dest => dest.RowStatus, opt => opt.MapFrom(src => ((char)src.RowStatus).ToString()))
                .ForMember(dest => dest.FaxNumber, opt => opt.NullSubstitute(""))
                .ForMember(dest => dest.MonitorInternet, opt => opt.MapFrom(src => src.MonitorInternet))
                .ForMember(dest => dest.MonitorText, opt => opt.MapFrom(src => src.MonitorText))
                .ForMember(dest => dest.MonitorWire, opt => opt.MapFrom(src => src.MonitorWire))
                .ForMember(dest => dest.UnsubscribeKey, opt => opt.MapFrom(src => src.UnsubscribeKey))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ User to UserSummary ------

            Mapper.CreateMap<Model.Entities.User, Service.Dto.UserSummary>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ WebCategory ------

            Mapper.CreateMap<Model.Entities.WebCategory, Service.Dto.WebCategory>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.WebCategory, Model.Entities.WebCategory>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ WebRelease ------

            Mapper.CreateMap<Model.Entities.WebRelease, Service.Dto.WebRelease>()
                .ForMember(dest => dest.ShouldShowOnPublic, opt => opt.MapFrom(src => (src.ShouldShowOnPublic.HasValue ? src.ShouldShowOnPublic.Value : false)))
                .ForMember(dest => dest.ShouldShowOnJournalists, opt => opt.MapFrom(src => (src.ShouldShowOnJournalists.HasValue ? src.ShouldShowOnJournalists.Value : false)))
                .ForMember(dest => dest.WasSentToJournalists, opt => opt.MapFrom(src => (src.WasSentToJournalists.StartsWith("T") ? true : false)))
                .ForMember(dest => dest.IsWidgetUpdated, opt => opt.MapFrom(src => (src.IsWidgetUpdated.HasValue ? src.IsWidgetUpdated.Value : false)))
                .ForMember(dest => dest.HasVideo, opt => opt.MapFrom(src => (src.HasVideo.HasValue ? src.HasVideo.Value : false)))
                .ForMember(dest => dest.HasAudio, opt => opt.MapFrom(src => (src.HasAudio.HasValue ? src.HasAudio.Value : false)))
                .ForMember(dest => dest.HasPhoto, opt => opt.MapFrom(src => (src.HasPhoto.HasValue ? src.HasPhoto.Value : false)))
                .ForMember(dest => dest.HasOther, opt => opt.MapFrom(src => (src.HasOther.HasValue ? src.HasOther.Value : false)))
                .ForMember(dest => dest.MultimediaType, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.MultimediaType>(src.MultimediaType, MultimediaType.None)))
                .ForMember(dest => dest.IsVerified, opt => opt.MapFrom(src => src.IsVerified.HasValue ? src.IsVerified : false))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.WebRelease, Model.Entities.WebRelease>()
                .ForMember(dest => dest.WasSentToJournalists, opt => opt.MapFrom(src => (src.WasSentToJournalists ? "T" : "F")))
                .ForMember(dest => dest.MultimediaType, opt => opt.MapFrom(src => ((char)src.MultimediaType).ToString()))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Website ------

            Mapper.CreateMap<Model.Entities.Website, Service.Dto.Website>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.Website, Model.Entities.Website>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Working Language ------

            Mapper.CreateMap<Model.Entities.WorkingLanguage, Service.Dto.WorkingLanguage>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.WorkingLanguage, Model.Entities.WorkingLanguage>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Rss Log ------

            Mapper.CreateMap<Model.Entities.RssLog, Service.Dto.RssLog>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.RssLog, Model.Entities.RssLog>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId == 0 ? null : src.UserId))
                .ForMember(dest => dest.WebCategoryId, opt => opt.MapFrom(src => src.WebCategoryId == 0 ? null : src.WebCategoryId))
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate))
                .ForMember(dest => dest.UserAgent, opt => opt.MapFrom(src => String.IsNullOrEmpty(src.UserAgent) ? String.Empty : src.UserAgent))
                .ForMember(dest => dest.UrlParams, opt => opt.MapFrom(src => String.IsNullOrEmpty(src.UrlParams) ? String.Empty : src.UrlParams))
                .ForMember(dest => dest.IPAddress, opt => opt.MapFrom(src => String.IsNullOrEmpty(src.IPAddress) ? String.Empty : src.IPAddress))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Search Log ------

            Mapper.CreateMap<Model.Entities.MediaContactSearchLog, Service.Dto.MediaContactSearchLog>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.DebtorNumber, opt => opt.MapFrom(src => src.DebtorNumber))
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate))
                .ForMember(dest => dest.SearchType, opt => opt.MapFrom(src => src.SearchType))
                .ForMember(dest => dest.SearchCriteria, opt => opt.MapFrom(src => src.SearchCriteria))
                .ForMember(dest => dest.SavedSearchId, opt => opt.MapFrom(src => src.SavedSearchId == 0 ? null : src.SavedSearchId))
                .ForMember(dest => dest.System, opt => opt.MapFrom(src => Medianet.Service.Common.ExtensionMethods.ToEnum<Common.SystemType>(src.System)))
                .ForMember(dest => dest.SearchContext, opt => opt.MapFrom(src => src.SearchContext))
                .ForMember(dest => dest.ResultsCount, opt => opt.MapFrom(src => src.ResultsCount))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MediaContactSearchLog, Model.Entities.MediaContactSearchLog>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.DebtorNumber, opt => opt.MapFrom(src => src.DebtorNumber))
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate))
                .ForMember(dest => dest.SearchType, opt => opt.MapFrom(src => src.SearchType))
                .ForMember(dest => dest.SearchCriteria, opt => opt.MapFrom(src => src.SearchCriteria))
                .ForMember(dest => dest.SavedSearchId, opt => opt.MapFrom(src => src.SavedSearchId == 0 ? null : src.SavedSearchId))
                .ForMember(dest => dest.System, opt => opt.MapFrom(src => ((char)src.System).ToString()))
                .ForMember(dest => dest.SearchContext, opt => opt.MapFrom(src => src.SearchContext))
                .ForMember(dest => dest.ResultsCount, opt => opt.MapFrom(src => src.ResultsCount))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Export Log ------

            Mapper.CreateMap<Model.Entities.MediaContactExportLog, Service.Dto.MediaContactExportLog>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MediaContactExportLog, Model.Entities.MediaContactExportLog>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.DebtorNumber, opt => opt.MapFrom(src => src.DebtorNumber))
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate))
                .ForMember(dest => dest.ListId, opt => opt.MapFrom(src => src.ListId == 0 ? null : src.ListId))
                .ForMember(dest => dest.TaskId, opt => opt.MapFrom(src => src.TaskId == 0 ? null : src.TaskId))
                .ForMember(dest => dest.NoOfExportedRows, opt => opt.MapFrom(src => src.NoOfExportedRows))
                .ForMember(dest => dest.ExportedFields, opt => opt.MapFrom(src => src.ExportedFields))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Note ------
            Mapper.CreateMap<Service.Dto.Note, Model.Entities.Note>()
                .ForMember(dest => dest.User, opt => opt.Ignore());

            Mapper.CreateMap<Model.Entities.Note, Service.Dto.Note>()
                .ForMember(dest => dest.User, opt => opt.MapFrom(src => Mapper.Map<Model.Entities.User, Service.Dto.UserSummary>(src.User)));
            #endregion

            #region ------ MediaOutletPartial ------

            Mapper.CreateMap<Model.Entities.MediaOutletPartial, Service.Dto.MediaOutletPartial>()
                 .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                 .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                  .ForMember(dest => dest.ProductTypeId, opt => opt.MapFrom(src => src.ProductTypeId))
                  .ForMember(dest => dest.ProductTypeName, opt => opt.MapFrom(src => src.ProductTypeName))
                  .ForMember(dest => dest.ProductTypeCategory, opt => opt.MapFrom(src => src.ProductTypeCategory))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Service.Dto.MediaOutletPartial, Model.Entities.MediaOutletPartial>()
                 .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                 .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                  .ForMember(dest => dest.ProductTypeId, opt => opt.MapFrom(src => src.ProductTypeId))
                  .ForMember(dest => dest.ProductTypeName, opt => opt.MapFrom(src => src.ProductTypeName))
                  .ForMember(dest => dest.ProductTypeCategory, opt => opt.MapFrom(src => src.ProductTypeCategory))
               .IgnoreAllNonExisting();

            #endregion

            #region ------ OmaDocument ------

            Mapper.CreateMap<Service.Dto.OmaDocument, Model.Entities.OmaDocumentView>()
              .IgnoreAllNonExisting();

            Mapper.CreateMap<Model.Entities.OmaDocumentView, Service.Dto.OmaDocument>()
               .IgnoreAllNonExisting();

            Mapper.CreateMap<Model.Entities.OmaDocument, Service.Dto.OmaDocument>()
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Private records ------

            Mapper.CreateMap<Service.Dto.MediaOmaContact, Model.Entities.MediaOmaContact>()
              .IgnoreAllNonExisting();

            Mapper.CreateMap<Model.Entities.MediaOmaContact, Service.Dto.MediaOmaContact>()
                .ForMember(dest => dest.RecordType, opt => opt.MapFrom(src =>
                            !string.IsNullOrEmpty(src.MediaOutletId) ? (int)RecordType.OmaContactAtMediaOutlet :
                            src.PrnOutletId.HasValue ? (int)RecordType.OmaContactAtPrnOutlet :
                            src.OmaOutletId.HasValue ? (int)RecordType.OmaContactAtOmaOutlet : (int)RecordType.OmaContactNoOutlet))
                .IgnoreAllNonExisting();


            Mapper.CreateMap<Service.Dto.PrivateOutletPartial, Model.Entities.PrivateOutletPartial>()
              .IgnoreAllNonExisting();

            Mapper.CreateMap<Model.Entities.PrivateOutletPartial, Service.Dto.PrivateOutletPartial>()
              .IgnoreAllNonExisting();

            #endregion


            #region ----- SalesForce AAPConract to AAPContract

            Mapper.CreateMap<Salesforce.Models.AAPContract, Service.Dto.AAPContract>()
                .ForMember(dest => dest.ContractStartDate, opt => opt.MapFrom(src => src.Contract_Start_Date__c))
                .ForMember(dest => dest.ContractEndDate, opt => opt.MapFrom(src => src.Contract_End_Date__c))
                .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.Monthly_Revenue__c))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description__c))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status__c))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Product, opt => opt.MapFrom(src => src.Contract_Product__c))
                .ForMember(dest => dest.ContractTerm, opt => opt.MapFrom(src => (int?)src.Contract_Term__c))
                .IgnoreAllNonExisting();
            #endregion

            #region ----- SalesForce Contact to DTO.SalesForceContact

            Mapper.CreateMap<Salesforce.Models.SalesForceContact, Service.Dto.SalesForceContact>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.AccountName, opt => opt.MapFrom(src => src.Account_Name__c))
                .ForMember(dest => dest.DebtorNumber, opt => opt.MapFrom(src => src.Customer_No__c))
                .ForMember(dest => dest.Position, opt => opt.MapFrom(src => src.Positions__c))
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
                .ForMember(dest => dest.RecordType, opt => opt.MapFrom(src => src.RecordTypeId))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.MainContact, opt => opt.MapFrom(src => src.Main_Contact__c))
                .ForMember(dest => dest.BusinessType, opt => opt.MapFrom(src => src.Account.Goverment__c))
                .ForMember(dest => dest.RelevantSubjects, opt => opt.MapFrom(src => src.Subject_Category_of_Interest__c.Split(Convert.ToChar(";")).ToList()))
                .ForMember(dest => dest.AccountOwnerName, opt => opt.MapFrom(src => src.Account.Owner.Name))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ Media Drafts ------

            Mapper.CreateMap<Dto.MediaDraftQueue, MediaDraftQueue>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<MediaDraftQueue, Dto.MediaDraftQueue>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<MediaDraftQueueView, Dto.MediaDraftQueueView>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<MediaContactDraft, Dto.MediaContactDraft>()
                .ForMember(dest => dest.DraftQueue, opt => opt.MapFrom(src => Mapper.Map<MediaDraftQueue, Dto.MediaDraftQueue>(src.DraftQueue)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Dto.MediaContactDraft, MediaContactDraft>()
                .ForMember(dest => dest.DraftQueue, opt => opt.MapFrom(src => Mapper.Map<Dto.MediaDraftQueue, MediaDraftQueue>(src.DraftQueue)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<MediaEmployeeDraft, Dto.MediaEmployeeDraft>()
                .ForMember(dest => dest.PreferredDeliveryMethod, opt => opt.MapFrom(src => string.IsNullOrEmpty(src.PreferredDeliveryMethod) ? null :
                                        (object)ExtensionMethods.ToEnum<PreferredDeliveryType>(src.PreferredDeliveryMethod)))
                .ForMember(dest => dest.DraftQueue, opt => opt.MapFrom(src => Mapper.Map<MediaDraftQueue, Dto.MediaDraftQueue>(src.DraftQueue)))
                .IgnoreAllNonExisting();

            Mapper.CreateMap<Dto.MediaEmployeeDraft, MediaEmployeeDraft>()
                .ForMember(dest => dest.PreferredDeliveryMethod, opt => opt.MapFrom(src => src.PreferredDeliveryMethod.HasValue ? ((char)src.PreferredDeliveryMethod).ToString() : null))
                .ForMember(dest => dest.DraftQueue, opt => opt.MapFrom(src => Mapper.Map<Dto.MediaDraftQueue, MediaDraftQueue>(src.DraftQueue)))
                .IgnoreAllNonExisting();

            #endregion

            #region ------ ServiceDeletedUserLog ------

            Mapper.CreateMap<Service.Dto.ServiceDeletedUserLog, Model.Entities.ServiceDeletedUserLogView>()
                  .IgnoreAllNonExisting();

            Mapper.CreateMap<Model.Entities.ServiceDeletedUserLogView, Service.Dto.ServiceDeletedUserLog>()
                .IgnoreAllNonExisting();

            #endregion

            #region ----- MNJ ------

            Mapper.CreateMap<MnjProfile, Dto.MnjProfile>()
               .ForMember(dest => dest.ProfileID, opt => opt.MapFrom(src => src.ProfileID))
               .ForMember(dest => dest.ProfileName, opt => opt.MapFrom(src => src.ProfileName))
               .ForMember(dest => dest.UserID, opt => opt.MapFrom(src => src.UserID))
               .ForMember(dest => dest.IsDefault, opt => opt.MapFrom(src => src.IsDefault))
               .ForMember(dest => dest.Keywords, opt => opt.MapFrom(src => Mapper.Map<List<MnjProfileKeyword>, List<Dto.MnjProfileKeyword>>(src.Keywords)))
               .ForMember(dest => dest.Categories, opt => opt.MapFrom(src => Mapper.Map<List<MnjProfileCategory>, List<Dto.MnjProfileCategory>>(src.Categories)))
               .IgnoreAllNonExisting();

            Mapper.CreateMap<Dto.MnjProfile, MnjProfile>()
              .ForMember(dest => dest.ProfileID, opt => opt.MapFrom(src => src.ProfileID))
              .ForMember(dest => dest.ProfileName, opt => opt.MapFrom(src => src.ProfileName))
              .ForMember(dest => dest.UserID, opt => opt.MapFrom(src => src.UserID))
              .ForMember(dest => dest.IsDefault, opt => opt.MapFrom(src => src.IsDefault))
               .ForMember(dest => dest.EmailAddress, opt => opt.MapFrom(src => src.EmailAddress.TrimAndEmptyIfNull()))
              .ForMember(dest => dest.Keywords, opt => opt.MapFrom(src => Mapper.Map<List<Dto.MnjProfileKeyword>, List<MnjProfileKeyword>>(src.Keywords)))
              .ForMember(dest => dest.Categories, opt => opt.MapFrom(src => Mapper.Map<List<Dto.MnjProfileCategory>, List<MnjProfileCategory>>(src.Categories)))
              .IgnoreAllNonExisting();

            Mapper.CreateMap<MnjProfileCategory, Dto.MnjProfileCategory>()
              .IgnoreAllNonExisting();

            Mapper.CreateMap<MnjProfileKeyword, Dto.MnjProfileKeyword>()
              .IgnoreAllNonExisting();

            Mapper.CreateMap<Dto.MnjProfileCategory, MnjProfileCategory>()
           .IgnoreAllNonExisting();

            Mapper.CreateMap<Dto.MnjProfileKeyword, MnjProfileKeyword>()
              .IgnoreAllNonExisting();
            #endregion

            #region ----- MNJ ------
            
            Mapper.CreateMap<Dto.MediaContactJobHistory, MediaContactJobHistory>()
           .IgnoreAllNonExisting();

            Mapper.CreateMap<MediaContactJobHistory, Dto.MediaContactJobHistory>()
              .IgnoreAllNonExisting();

            #endregion
        }
    }
}
