﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Medianet.Service.Dto;
using System.Configuration;

namespace Medianet.Service.Common
{
    public class CasHelper
    {
        public static int CountMailmergeRecipients(int tempFileId, DistributionType distType) {
            ConverterService.CSVTempDocumentData t = new ConverterService.CSVTempDocumentData();

            t.DistributionType = ((char)distType).ToString();
            t.TempFileID = tempFileId;

            // Call the converter service to get the number of recipients.
            using (var svc = new ServiceProxyHelper<ConverterService.ConvertRequestClient, ConverterService.IConvertRequest>()) {
                return svc.Proxy.GetTempDocumentRecipientCount(t);
            }
        }

        public static List<ConverterService.RecipientData> GetRecipientsForCSV(int tempFileId, DistributionType distType) {
            List<ConverterService.RecipientData> recipList = null;
            ConverterService.CSVTempDocumentData t = new ConverterService.CSVTempDocumentData();

            t.DistributionType = ((char)distType).ToString();
            t.TempFileID = tempFileId;

            // Get CAS to give us the addresses in the data file.
            using (var svc = new ServiceProxyHelper<ConverterService.ConvertRequestClient, ConverterService.IConvertRequest>()) {
                recipList = svc.Proxy.GetTempDocumentRecipients(t).ToList();
            }

            return recipList;
        }

        public static ConverterService.DocumentSizeData ConvertDocuments(List<int> tempFileIdList, bool hasWire, string sessionKey) {
            ConverterService.DocumentListData files = new ConverterService.DocumentListData();

            files.SessionKey = sessionKey;
            files.HasFax = false;
            files.HasWire = hasWire;
            files.TempFileIDs = tempFileIdList.ToArray();

            if(ConfigurationManager.AppSettings["EnvironmentType"].ToString() == Constants2.EnvironmentLocal)
            {
                return new ConverterService.DocumentSizeData();
            }
            else
            {
                // Get CAS to convert it.
                using (var svc = new ServiceProxyHelper<ConverterService.ConvertRequestClient, ConverterService.IConvertRequest>())
                {
                    return svc.Proxy.ConvertDocuments(files);
                }
            }
        }
    }
}
