﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Medianet.Service.Common
{
    public class Constants2
    {
        public static readonly string GenericServiceErrorMessage =
            "An error has occured. If this continues, please contact Client Services on 1300 616 813 or at " +
            ConfigurationManager.AppSettings["MailDefaultAddress"] + ".";

        // Should be a URL or the course of action that the client should take.
        public const string GenericServiceErrorFaultAction = "information unavailable.";
        public const string EnvironmentLocal = "Local";
        public const string EnvironmentLive = "Live";
        public const string Invoiced = "invoiced";
        public const string Invoice = "invoice";
    }
}
