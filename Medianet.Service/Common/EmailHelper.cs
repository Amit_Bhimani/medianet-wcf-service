﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Globalization;
using System.Net.Mail;
using NLog;
using System.IO;
using Medianet.Service.Dto;
using Medianet.DataLayer.Common;
using System.Xml.Serialization;

namespace Medianet.Service.Common
{
    public class EmailHelper
    {
        private const string LogonFailureMessage = "Customer {0} of company {1} ({2}) has unsuccessfully tried to log on to the {3} website {4} times.";
        private const string AccountDisabledMessage = "The account has been set to inactive.";
        private const string NewAccountMessage = "A new account has been created with the following details:\n\nUser name: {0}\nCompany name: {1}\nDebtor number: {2}";
        private const string ServiceUsageMessage = "The Job with Id {0}, submitted by Customer {1} has \"{2}\" selected.";
        //private const string ForgotPasswordMessage = "Hello {0}.\nPlease click on the link below and enter a new password when prompted.\n\n{1}\n\nRegards,\nThe Medianet team";
        private const string UnverifiedReleaseSubmissionMessage = "An Unverified press release has been submitted.\n Job ID: {0}\n Headline: {1} \n Organisation: {2} \n Total Releases: {3} \n\n\nPlease consider release content for moderation or review.\n\n";

        public static void LogonFailureDisableEmail(string username, string customerName, string debtorNumber, string website, int failureCount) {
            try {
                String content = string.Format(LogonFailureMessage,
                    username, customerName, debtorNumber, website, failureCount);

                content += " " + AccountDisabledMessage;

                SendEmail(ConfigurationManager.AppSettings["LogonFailureEmailToAddress"],
                    "Website logon failure notice",
                    content);
            }
            catch (Exception ex) {
                LogManager.GetCurrentClassLogger().LogException(LogLevel.Error, "Exception thrown in LogonFailureDisableEmail().", ex);
            }
        }

        public static void LogonFailureWarningEmail(string username, string customerName, string debtorNumber, string website, int failureCount) {
            try {
                String content = string.Format(LogonFailureMessage,
                    username, customerName, debtorNumber, website, failureCount);

                SendEmail(ConfigurationManager.AppSettings["LogonFailureEmailToAddress"],
                    "Website logon failure notice",
                    content);
            }
            catch (Exception ex) {
                LogManager.GetCurrentClassLogger().LogException(LogLevel.Error, "Exception thrown in LogonFailureWarningEmail().", ex);
            }
        }

        public static void NewAccountEmail(string fullName, string customerName, string debtorNumber) {
            try {
                String content = string.Format(NewAccountMessage,
                    fullName, customerName, debtorNumber);

                SendEmail(ConfigurationManager.AppSettings["NewAccountEmailToAddresses"],
                    "New account",
                    content);
            }
            catch (Exception ex) {
                GeneralErrorEmail("Error sending New Account email for Customer " + customerName + ".", ex);
                LogManager.GetCurrentClassLogger().LogException(LogLevel.Error, "Exception thrown in NewAccountEmail().", ex);
            }
        }

        public static void ServiceUsageEmail(int releaseId, string debtorNumber, string serviceName)
        {
            try
            {
                string content = string.Format(ServiceUsageMessage, releaseId, debtorNumber, serviceName);

                SendEmail(ConfigurationManager.AppSettings["LogonFailureEmailToAddress"],
                    "Distribution Selection Alert",
                    content);
            }
            catch (Exception ex) {
                GeneralErrorEmail("Error sending Service Usage email.", ex);
                LogManager.GetCurrentClassLogger().LogException(LogLevel.Error, "Exception thrown in ServiceUsageEmail().", ex);
            }
        }

        public static void ForgotPasswordEmail(string name, string product, string emailAddress, string resetPasswordUrl) {
            try {
                string content;
                string templatePath = BusinessLayer.Common.AAPEnvironmentHelper.GetPath("TemplatePath") + 
                    "Medianet_Forgot_Password.html";

                using (StreamReader sr = new StreamReader(templatePath)) {
                    content = sr.ReadToEnd();
                }

                content = content.Replace("[%WEBSITE_URL%]", BusinessLayer.Common.AAPEnvironmentHelper.GetSetting("WebsiteURL"));
                content = content.Replace("[%FIRST_NAME%]", name);
                content = content.Replace("[%PRODUCT_NAME%]", product);
                content = content.Replace("[%RESET_PASSWORD_URL%]", resetPasswordUrl);
                content = content.Replace("[%YEAR%]", DateTime.Now.Year.ToString());
                content = content.Replace("[%CONTACT_US_URL%]", BusinessLayer.Common.AAPEnvironmentHelper.GetSetting("ContactUsURL"));
                content = content.Replace("[%DISCLAIMER%]", BusinessLayer.Common.AAPEnvironmentHelper.GetBlock("EmailDisclaimer"));

                SendEmail(emailAddress, "Reset your Medianet password", content, true);
            }
            catch (Exception ex) {
                GeneralErrorEmail("Error sending Forgot Password email to <" + emailAddress + ">.", ex);
                LogManager.GetCurrentClassLogger().LogException(LogLevel.Error, "Exception thrown in ForgotPasswordEmail().", ex);
            }
        }

        public static void NewMnjAccountEmail(string name, string emailAddress, string setPasswordUrl)
        {
            try
            {
                string content;
                string templatePath = BusinessLayer.Common.AAPEnvironmentHelper.GetPath("TemplatePath") +
                                      "Medianet_Add_MNJ_User.html";

                using (StreamReader sr = new StreamReader(templatePath))
                {
                    content = sr.ReadToEnd();
                }

                content = content.Replace("[%FIRST_NAME%]", name);
                content = content.Replace("[%SET_PASSWORD_URL%]", setPasswordUrl);

                SendEmail(emailAddress, "Welcome to Medianet for Journalists!", content, true);
            }
            catch (Exception ex)
            {
                GeneralErrorEmail("Error sending New MNJ Account email to <" + emailAddress + ">.", ex);
                LogManager.GetCurrentClassLogger().LogException(LogLevel.Error, "Exception thrown in NewMnjAccountEmail().", ex);
            }
        }

        public static void RegistrationEmailValidationEmail(string firstName, string product, string emailAddress, string url)
        {
            try
            {
                string content;
                string templatePath = BusinessLayer.Common.AAPEnvironmentHelper.GetPath("TemplatePath") + "Medianet_Registration_Email_Validation.html";
                
                using (StreamReader sr = new StreamReader(templatePath))
                {
                    content = sr.ReadToEnd();
                }

                content = content.Replace("[%PRODUCT_NAME%]", product);
                content = content.Replace("[%DISCLAIMER%]", BusinessLayer.Common.AAPEnvironmentHelper.GetBlock("EmailDisclaimer"));
                
                content = content.Replace("[%FIRST_NAME%]", firstName);
                content = content.Replace("[%REGISTRATION_VALIDATION_URL%]", url);
                content = content.Replace("[%YEAR%]", DateTime.Now.Year.ToString());

                SendEmail(emailAddress, "Medianet Sign Up - Verify Your Email", content, true);
            }
            catch (Exception ex)
            {
                GeneralErrorEmail("Error sending Registration Email Validation email to <" + emailAddress + ">.", ex);
                LogManager.GetCurrentClassLogger().LogException(LogLevel.Error, "Exception thrown in RegistrationEmailValidationEmail().", ex);
            }
        }

        public static void PublicReleaseEmail(ReleasePublic release, string emailAddress) {
            try {
                string content;
                string templatePath = BusinessLayer.Common.AAPEnvironmentHelper.GetPath("TemplatePath") + "Medianet_Public_Release.html";
                string attachments = string.Empty;

                using (StreamReader sr = new StreamReader(templatePath)) {
                    content = sr.ReadToEnd();
                }

                content = content.Replace("[%DISTRIBUTION_DATE%]", release.DistributedDate.HasValue ? release.DistributedDate.Value.ToString("dd MMMM yyyy hh:mm") : string.Empty);
                content = content.Replace("[%HEADLINE%]", release.Headline);
                content = content.Replace("[%ORGANISATION%]", release.Organisation);
                content = content.Replace("[%CONTENT%]", release.HtmlContent);

                attachments = attachments + GetAttachments("Videos", release.Videos, "Medianet_Public_Release_Attachment.html");
                attachments = attachments + GetAttachments("Audios", release.Audios, "Medianet_Public_Release_Attachment.html");
                attachments = attachments + GetAttachments("Images", release.Images, "Medianet_Public_Release_Image.html");
                attachments = attachments + GetAttachments("Attachments", release.Attachments, "Medianet_Public_Release_Attachment.html");

                content = content.Replace("[%ATTACHMENTS%]", attachments);

                content = content.Replace("[%DISCLAIMER%]", BusinessLayer.Common.AAPEnvironmentHelper.GetBlock("EmailDisclaimer"));

                SendEmail(emailAddress, "Medianet - " + release.Headline, content, true);
            }
            catch (Exception ex) {
                GeneralErrorEmail("Error sending Public Release email to <" + emailAddress + ">.", ex);
                LogManager.GetCurrentClassLogger().LogException(LogLevel.Error, "Exception thrown in PublicReleaseEmail().", ex);
            }
        }

        public static void TermsReadEmail(string readByName, DateTime readByDate, string primaryName, string emailAddress, SystemType system)
        {
            try
            {
                string content;
                string templatePath = BusinessLayer.Common.AAPEnvironmentHelper.GetPath("TemplatePath") + "Medianet_Terms_Read.html";

                using (StreamReader sr = new StreamReader(templatePath))
                {
                    content = sr.ReadToEnd();
                }

                content = content.Replace("[%CONTACT_NAME%]", primaryName);
                content = content.Replace("[%READ_BY%]", readByName);
                content = content.Replace("[%READ_DATE%]", readByDate.ToString("dd/MM/yyyy hh:mm tt", CultureInfo.InvariantCulture));

                if (system == SystemType.Medianet)
                {
                    content = content.Replace("[%TERMS_URL%]", ConfigurationManager.AppSettings["DistributionTerms"]);
                }
                else
                {
                    content = content.Replace("[%TERMS_URL%]", ConfigurationManager.AppSettings["ContactsTerms"]);
                }

                SendEmail(emailAddress, "Thank you for agreeing to the new Medianet Terms and Conditions", content, true);
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().LogException(LogLevel.Error, "Exception thrown in TermsReadEmail().", ex);
            }
        }

        public static void GeneralErrorEmail(string error, Exception exception = null) {
            try {
                String content = error;

                if (exception != null) content += "\n\n" + exception.ToString();

                SendEmail(ConfigurationManager.AppSettings["LogonFailureEmailToAddress"],
                    "Medianet WCF general error",
                    content);
            }
            catch (Exception ex) {
                LogManager.GetCurrentClassLogger().LogException(LogLevel.Error, "Exception thrown in GeneralErrorEmail().", ex);
            }
        }

        public static void UnverifiedReleaseSubmissionEmail(int JobId, ReleaseSummary releaseSummary, User user)
        {
            try
            {
                String content = string.Format(UnverifiedReleaseSubmissionMessage, 
                                                                    JobId.ToString(),
                                                                    releaseSummary.ReleaseDescription,
                                                                    releaseSummary.Organisation,
                                                                    user.ReleaseCount);

                SendEmail(ConfigurationManager.AppSettings["UnverifiedReleaseEmailToAddress"],
                    "Medianet Unverified Release Submitted (" + JobId.ToString() + ")",
                    content);
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().LogException(LogLevel.Error, "Exception thrown in UnverifiedReleaseSubmissionEmail().", ex);
            }
        }

        public static void CourseRegistrationEmail(CourseRegistrationRequest regoRequest, string courseName, DateTime courseDate, string location, bool isOverBooked, bool isContentMissing, string emailAddress)
        {
            try
            {
                string content;
                string emailSubject = "Registration for Training Course";
                string templatePath = BusinessLayer.Common.AAPEnvironmentHelper.GetPath("TemplatePath") + "Course_Registration.html";

                using (StreamReader sr = new StreamReader(templatePath))
                {
                    content = sr.ReadToEnd();
                }

                content = content.Replace("[%COURSE_NAME%]", courseName);
                content = content.Replace("[%COURSE_DATE%]", courseDate.ToString("dd MMMM yyyy"));
                content = content.Replace("[%COURSE_LOCATION%]", location);
                content = content.Replace("[%FIRST_NAME%]", regoRequest.FirstName);
                content = content.Replace("[%LAST_NAME%]", regoRequest.LastName);
                content = content.Replace("[%COMPANY_NAME%]", regoRequest.CompanyName);
                content = content.Replace("[%COMPANY_ABN%]", regoRequest.ABN);
                content = content.Replace("[%JOB_TITLE%]", regoRequest.ContactPosition);
                content = content.Replace("[%ADDRESS%]", regoRequest.Address);
                content = content.Replace("[%SUBURB%]", regoRequest.Suburb);
                content = content.Replace("[%POSTCODE%]", regoRequest.PostCode);
                content = content.Replace("[%STATE%]", regoRequest.State);
                content = content.Replace("[%COUNTRY%]", regoRequest.Country);
                content = content.Replace("[%EMAIL_ADDRESS%]", regoRequest.EmailAddress);
                content = content.Replace("[%CONTACT_NUMBER%]", regoRequest.TelephoneNumber);
                content = content.Replace("[%EXPERIENCE_LEVEL%]", regoRequest.ExperienceLevel);
                content = content.Replace("[%DESIRED_OUTCOME%]", regoRequest.DesiredOutcome);
                content = content.Replace("[%TOPICS_PLANNED%]", regoRequest.TopicsPlanned);

                if (isOverBooked || isContentMissing || regoRequest.IsInvoice)
                {
                    string note = string.Empty;

                    if (isOverBooked)
                    {
                        note = note + "This course is over booked. ";
                        emailSubject = emailSubject + " - Over Booked";
                    }

                    if (isContentMissing)
                    {
                        note = note + "Full details of this registration are missing. ";
                    }

                    if (regoRequest.IsInvoice)
                    {
                        note = note + "This is an invoice registration. Please create an invoice for them. ";
                    }

                    content = content.Replace("[%INTRODUCTION%]", "<p style='color:red;'>Note: " + note + "</p>");
                }
                else
                    content = content.Replace("[%INTRODUCTION%]", string.Empty);

                SendEmail(emailAddress, emailSubject, content, true);
            }
            catch (Exception ex)
            {
                GeneralErrorEmail("Error sending Course Registration email.", ex);
                LogManager.GetCurrentClassLogger().LogException(LogLevel.Error, "Exception thrown in CourseRegistrationEmail().", ex);
            }
        }

        public static void JournalistUpdatedInfoRequestResult(string firstName, string description, string emailAddress)
        {
            try
            {
                string content;
                string templatePath = BusinessLayer.Common.AAPEnvironmentHelper.GetPath("TemplatePath") + "Medianet_Journalists_UpdatingInfo_Result.html";

                using (StreamReader sr = new StreamReader(templatePath))
                {
                    content = sr.ReadToEnd();
                }
                
                content = content.Replace("[%FIRST_NAME%]", firstName);
                content = content.Replace("[%Description%]", description);
                content = content.Replace("[%YEAR%]", DateTime.Now.Year.ToString());
                content = content.Replace("[%CONTACT_US_URL%]", BusinessLayer.Common.AAPEnvironmentHelper.GetSetting("ContactUsURL"));
                content = content.Replace("[%DISCLAIMER%]", BusinessLayer.Common.AAPEnvironmentHelper.GetBlock("EmailDisclaimer"));

                SendEmail(emailAddress, "Journalist Request Result", content, true, ConfigurationManager.AppSettings["MNJUpdateResult_MailFromAddress"]);
            }
            catch (Exception ex)
            {
                GeneralErrorEmail($"Error Journalist UpdatedInfo Request Result to <{emailAddress}>.", ex);

                LogManager.GetCurrentClassLogger().LogException(LogLevel.Error, "Exception thrown in JournalistUpdatedInfoRequestResult().", ex);
            }
        }
        public static void UpgradeAccountEmailSend(Account account,string accountManager = "")
        {
            try
            {
                string content;
                string templatePath = BusinessLayer.Common.AAPEnvironmentHelper.GetPath("TemplatePath") + "Medianet_Upgrade_Account.html";

                using (StreamReader sr = new StreamReader(templatePath))
                {
                    content = sr.ReadToEnd();
                }
                //ABN Lookup Info
                var abnInfo = new ABRPayloadSearchResults(); //This class name is provided by ABNLookup info, Changing name can stop feature
                if (!string.IsNullOrWhiteSpace(account.ABN) && account.ABN.Length > 10)
                {
                    abnInfo = GetABNInfo(account.ABN.Replace(" ",""), "N");
                }
                var abnDetails = abnInfo?.response?.businessEntity202001;
                content = content.Replace("[%ABN_ASIC_NUMBER%]", abnDetails?.ASICNumber.ToString() ?? string.Empty);
                content = content.Replace("[%ABN_ENTITY_TYPE%]", abnDetails?.entityType.entityDescription ?? string.Empty);
                content = content.Replace("[%ABN_ENTITY_STATUS%]", abnDetails?.entityStatus.entityStatusCode ?? string.Empty);
                content = content.Replace("[%ABN_DATE_LAST_MODIFIED%]", abnDetails?.recordLastUpdatedDate.ToString("dd/MM/yyyy hh:mm tt") ?? string.Empty);
                content = content.Replace("[%ABN_TRADING_NAME%]", abnDetails?.businessName.FirstOrDefault()?.organisationName ?? string.Empty);
                content = content.Replace("[%ABN_COMPANY_NAME%]", abnDetails?.mainName.FirstOrDefault()?.organisationName ?? string.Empty);
                var abnAddress = abnDetails?.mainBusinessPhysicalAddress.FirstOrDefault();
                content = content.Replace("[%ABN_STATE_POST_CODE%]", $"{ abnAddress?.stateCode }, { abnAddress?.postcode }");
                content = content.Replace("[%ABN%]", account.ABN);


                content = content.Replace("[%FIRST_NAME%]", account.FirstName);
                content = content.Replace("[%LAST_NAME%]", account.LastName);
                content = content.Replace("[%DEBTORNUMBER%]", account.DebtorNumber);
                
                content = content.Replace("[%EMAIL_TO%]", account.EmailAddress);
                content = content.Replace("[%ADDRESS_LINE_1%]", account.CompanyAddress.AddressLine1);
                content = content.Replace("[%ADDRESS_LINE_2%]", account.CompanyAddress.AddressLine2);
                content = content.Replace("[%ADDRESS_LINE_3%]", account.CompanyAddress.AddressLine3);
                content = content.Replace("[%ADDRESS_CITY%]", account.CompanyAddress.City);
                content = content.Replace("[%ADDRESS_STATE%]", account.CompanyAddress.State);
                content = content.Replace("[%ADDRESS_POST_CODE%]", account.CompanyAddress.Postcode);
                content = content.Replace("[%ADDRESS_COUNTRY%]", account.CompanyAddress.Country);
                content = content.Replace("[%CONTACT_PHONE%]", account.TelephoneNumber);
                content = content.Replace("[%CONTACT_SALES_PERSON%]", accountManager);
                content = content.Replace("[%CONTACT_SALES_TERRITORY%]", account.SalesRegionName);


                content = content.Replace("[%YEAR%]", DateTime.Now.Year.ToString());
                content = content.Replace("[%CONTACT_US_URL%]", BusinessLayer.Common.AAPEnvironmentHelper.GetSetting("ContactUsURL"));
                content = content.Replace("[%DISCLAIMER%]", BusinessLayer.Common.AAPEnvironmentHelper.GetBlock("EmailDisclaimer"));
                SendEmail(ConfigurationManager.AppSettings["AccountsEmailToAddress"], "New Medianet Customer created:"+ account.DebtorNumber + " - " + $"{account.FirstName} {account.LastName}", content, true);
            }
            catch (Exception ex)
            {
                GeneralErrorEmail($"Error Upgrade Account Email Send Result to <{ConfigurationManager.AppSettings["AccountsEmailToAddress"]}>.", ex);

                LogManager.GetCurrentClassLogger().LogException(LogLevel.Error, "Exception thrown in UpgradeAccountEmailSend().", ex);
            }
        }
        #region Private Methods

        private static void SendEmail(string toAddress, string subject, string content, bool isHtml = false, string from = null) {
            SmtpClient smtpClient = new SmtpClient();
            MailMessage mailMessage = new MailMessage();

            smtpClient.Host = ConfigurationManager.AppSettings["MailServer"];
            if (smtpClient.Port == 0)
                smtpClient.Port = 26;

            if (string.IsNullOrEmpty(from))
                from = ConfigurationManager.AppSettings["MailFromAddress"];

            mailMessage.From = new MailAddress(from, ConfigurationManager.AppSettings["MailFromName"]);
            string[] addresses = toAddress.Split(new Char[] { ';' });
            foreach (var a in addresses)
            {
                mailMessage.To.Add(new MailAddress(a));
            }
            mailMessage.Subject = subject;
            mailMessage.IsBodyHtml = isHtml;
            mailMessage.Body = content;

            smtpClient.Send(mailMessage);
        }

        private static string GetAttachments(string title, List<DocumentPublic> attachments, string templateFile) {
            string content = string.Empty;
            string attachContent = string.Empty;
            string attachTemplate;
            string templatePath = BusinessLayer.Common.AAPEnvironmentHelper.GetPath("TemplatePath");

            if (attachments != null && attachments.Count > 0) {
                using (StreamReader sr = new StreamReader(templatePath + "Medianet_Public_Release_Attachments.html")) {
                    content = sr.ReadToEnd();
                }

                using (StreamReader sr = new StreamReader(templatePath + templateFile)) {
                    attachTemplate = sr.ReadToEnd();
                }

                foreach (var att in attachments) {
                    string attContent = attachTemplate.Replace("[%DOWNLOAD_URL%]", att.DownloadUrl);

                    attContent = attContent.Replace("[%FILENAME%]", att.OriginalFilename);

                    if (att.Thumbnails != null) {
                        var thumb = att.Thumbnails.Where(t => t.Height <= 100 && t.Width <= 100).FirstOrDefault();

                        if (thumb != null && !string.IsNullOrEmpty(thumb.Url)) {
                            attContent = attContent.Replace("[%THUMBNAIL%]", thumb.Url);
                        }
                    }

                    attachContent += attContent;
                }

                content = content.Replace("[%TITLE%]", title);
                content = content.Replace("[%ATTACHMENTS%]", attachContent);
            }

            return content;
        }

        private static ABRPayloadSearchResults GetABNInfo(string abnNumber, string includeHistoricalDetails)
        {
            var abnDetails = new ABRPayloadSearchResults();
            try
            {
                var builder = new UriBuilder("https://abr.business.gov.au/abrxmlsearch/AbrXmlSearch.asmx/SearchByABNv202001");
                builder.Query = $"searchString={abnNumber}&includeHistoricalDetails={includeHistoricalDetails}&authenticationGuid={ConfigurationManager.AppSettings["abnAuthenticationGuid"]}";

                //Create a query
                var client = new System.Net.Http.HttpClient();
                var response = client.GetAsync(builder.Uri).Result;
                var responseXmlString = string.Empty;
                if (response.IsSuccessStatusCode)
                {
                    using (var stringReader = new StreamReader(response.Content.ReadAsStreamAsync().Result))
                    {
                        responseXmlString = stringReader.ReadToEnd();
                    }
                }
                else
                {
                    LogManager.GetCurrentClassLogger().Info("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                }
                if (!string.IsNullOrEmpty(responseXmlString))
                {
                    var xmlSerializer = new XmlSerializer(typeof(ABRPayloadSearchResults));
                    using (var stringReader = new System.IO.StringReader(responseXmlString))
                    {
                        abnDetails = (ABRPayloadSearchResults)xmlSerializer.Deserialize(stringReader);
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().LogException(LogLevel.Error, "Exception thrown in GetABNInfo().", ex);
            }
            return abnDetails;
        }
        #endregion
    }
}
