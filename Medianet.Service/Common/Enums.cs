﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using AutoMapper;
using System.Globalization;

namespace Medianet.Service.Common
{
    /// <summary>
    /// This attribute is used to represent a string value
    /// for a value in an enum.
    /// </summary>
    public class StringValueAttribute : Attribute
    {
        /// <summary>
        /// Holds the stringvalue for a value in an enum.
        /// </summary>
        public string StringValue { get; protected set; }

        /// <summary>
        /// Constructor used to init a StringValue Attribute
        /// </summary>
        /// <param name="value"></param>
        public StringValueAttribute(string value) {
            this.StringValue = value;
        }
    }

    public static class ExtensionMethods
    {
        /// <summary>
        /// Truncate a string to a specified length without worrying if the string is less than that length.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        public static string Truncate(this string value, int maxLength) {
            if (value == null)
                return null;
            else
                return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }
        /// <summary>
        /// Convert string  into integer 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int ConvertToInt(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return 0;
            int num = 0;
            if (int.TryParse(value.ToString(), out num))
            {
                return num;
            }
            else
            {
                return -1;
            }
        }
        /// <summary>
        /// Converts the string value to camel/propercase.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        public static string ProperCase(this string value)
        {
            CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;

            if (value == null)
                return null;
            else
                return textInfo.ToTitleCase(value);
        }
        public static T[] Append<T>(this T[] array, T item)
        {
            if (array == null)
            {
                return new T[] { item };
            }
            return array.Concat(new T[] { item }).ToArray();
        }
        /// <summary>
        /// Convert known Word unicode characters to an ascii equivalent.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ConvertToAscii(this string value) {
            if (value == null)
                return null;
            else {
                List<int> codeNumbers = new List<int>() { 8218, 402, 8222, 8230, 8224, 8225, 710, 8240, 352, 8249, 338, 8216, 8217, 8220, 8221, 8226, 8211, 8212, 732, 8482, 353, 8250, 339, 376, 160 };
                List<string> characters = new List<string>() { ",", "f", "\"", "...", "+", "++", "^", "o/oo", "Sh", "<", "Oe", "'", "'", "\"", "\"", "*", "-", "--", "~", "(TM)", "sh", ">", "oe", "Y", " " }; 
                string result = value.Replace(Convert.ToChar(8218), ',');

                for (int i = 0; i < codeNumbers.Count; i++) {
                    result = result.Replace(Convert.ToChar(codeNumbers[i]).ToString(), characters[i]);
                }

                return result;
            }
        }

        /// <summary>
        /// Will get the string value for a given enums value, this will
        /// only work if you assign the StringValue attribute to
        /// the items in your enum.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetStringValue(this Enum value) {
            // Get the type
            Type type = value.GetType();

            // Get fieldinfo for this type
            FieldInfo fieldInfo = type.GetField(value.ToString());

            // Get the stringvalue attributes
            StringValueAttribute[] attribs = fieldInfo.GetCustomAttributes(
                typeof(StringValueAttribute), false) as StringValueAttribute[];

            // Return the first if there was a match.
            return attribs.Length > 0 ? attribs[0].StringValue : null;
        }

        public static IMappingExpression<TSource, TDestination>
                      IgnoreAllNonExisting<TSource, TDestination>(this IMappingExpression<TSource, TDestination> expression) {
            var sourceType = typeof(TSource);
            var destinationType = typeof(TDestination);
            var existingMaps = Mapper.GetAllTypeMaps().First(x => x.SourceType.Equals(sourceType) && x.DestinationType.Equals(destinationType));

            foreach (var property in existingMaps.GetUnmappedPropertyNames()) {
                expression.ForMember(property, opt => opt.Ignore());
            }
            return expression;
        }

        // This method isn't used because it doesn't work.
        public static string ToString<T>(T value) {
            //return ((char)value).ToString();
            return ((char)Enum.ToObject(typeof(T), value)).ToString();
        }

        public static T ToEnum<T>(string value) {
            if (string.IsNullOrEmpty(value))
                throw new Exception("Value cannot be empty for type " + typeof(T).ToString() + ".");

            int intValue = (int)value[0];

            if (!Enum.IsDefined(typeof(T), intValue))
                throw new Exception("Unknown value " + value + " for type " + typeof(T).ToString() + ".");

            return (T)Enum.ToObject(typeof(T), intValue);
        }

        public static T ToEnum<T>(string value, T defaultValue) {
            T ret = defaultValue;

            if (!string.IsNullOrEmpty(value)) {
                int intValue =  (int)value[0];

                if (Enum.IsDefined(typeof(T), intValue)) {
                    ret = (T)Enum.ToObject(typeof(T), intValue);
                }
            }

            return ret;
        }

        public static T ToEnum<T>(char value) {
            int intValue = (int)value;

            if (!Enum.IsDefined(typeof(T), intValue))
                throw new Exception("Unknown value " + value + " for type " + typeof(T).ToString() + ".");

            return (T)Enum.ToObject(typeof(T), intValue);
        }

        /// <summary>
        /// Trims a string. If the value is null then returns a empty string.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string TrimAndEmptyIfNull(this string value)
        {

            if (String.IsNullOrEmpty(value))
                return string.Empty;
            return value.Trim();
        }

        //public static T ToEnumFromValue<T>(string value) where T : struct {
        //    Type enumType = typeof(T);
        //    Array enumValArray = Enum.GetValues(enumType);

        //    foreach (FieldInfo fi in enumType.GetFields()) {
        //        // Get the stringvalue attributes
        //        StringValueAttribute[] attribs = fi.GetCustomAttributes(
        //            typeof(StringValueAttribute), false) as StringValueAttribute[];

        //        // Return the first if there was a match.
        //        return (T)Enum.Parse(enumType, fi.Name);
        //    }

        //    //foreach (T val in enumValArray) {
        //    //    if (((Enum)val).GetStringValue().Equals(value, StringComparison.CurrentCultureIgnoreCase))
        //    //        return val;
        //    //}

        //    throw new Exception("Unknown value " + value + " for type " + typeof(T).ToString() + ".");
        //}
    }

    [DataContract]
    public enum ReleaseStatusType
    {
        [EnumMember]
        None = ' ',

        [EnumMember]
        New = 'N',

        [EnumMember]
        Converting = 'W',

        [EnumMember]
        Processing = 'P',

        [EnumMember]
        OnHold = 'H',

        [EnumMember]
        Embargoed = 'E',

        [EnumMember]
        MailMerging = 'M',

        [EnumMember]
        Released = 'R',

        [EnumMember]
        Transmitted = 'T',

        [EnumMember]
        Resulted = 'G',

        [EnumMember]
        Accounts = 'A',

        [EnumMember]
        Invoiced = 'I',

        [EnumMember]
        Failed = 'F',

        [EnumMember]
        Cancelled = 'C',

        [EnumMember]
        Abandoned = 'X',

        [EnumMember]
        Serviced = 'S'
    }

    [DataContract]
    public enum InboundFileStatusType
    {
        [EnumMember]
        Ready = 'R',

        [EnumMember]
        Failed = 'F'
    }

    [DataContract]
    public enum TransactionStatusType
    {
        [EnumMember]
        New = ReleaseStatusType.New,

        [EnumMember]
        Released = ReleaseStatusType.Released,

        [EnumMember]
        Transmitted = ReleaseStatusType.Transmitted,

        [EnumMember]
        Resulted = ReleaseStatusType.Resulted,

        [EnumMember]
        Cancelled = ReleaseStatusType.Cancelled,

        [EnumMember]
        Failed = ReleaseStatusType.Failed
    }

    [DataContract]
    public enum RowStatusType
    {
        [EnumMember]
        Deleted = 'D',

        [EnumMember]
        Active = 'A',

        [EnumMember]
        Inactive = 'I',

        [EnumMember]
        Cancelled = 'C',

        [EnumMember]
        ChangingPassword = 'P'
    }

    [DataContract]
    public enum EventType
    {
        [EnumMember]
        Course = 'C',

        [EnumMember]
        Event = 'E',
    }

    [DataContract]
    public enum EmployeeRowStatusType
    {
        [EnumMember]
        Deleted = 'D',

        [EnumMember]
        Active = 'A'
    }

    [DataContract]
    public enum OutletRowStatusType
    {
        [EnumMember]
        Deleted = 'D',

        [EnumMember]
        Active = 'A'
    }

    [DataContract]
    public enum ContactRowStatusType
    {
        [EnumMember]
        Deleted = 'D',

        [EnumMember]
        Active = 'A',

        [EnumMember]
        Unassigned = 'U'
    }

    [DataContract]
    public enum ReleasePriorityType
    {
        [EnumMember]
        Standard = 'N',

        [EnumMember]
        High = 'U'
    }

    [DataContract]
    public enum DistributionType
    {
        [EnumMember]
        Unknown = ' ',

        [EnumMember]
        Fax = 'F',

        [EnumMember]
        Email = 'E',

        [EnumMember]
        SMS = 'S',

        [EnumMember]
        Voice = 'V',

        [EnumMember]
        Wire = 'W',

        [EnumMember]
        Internet = 'I',

        [EnumMember]
        Twitter = 'T',

        [EnumMember]
        Image = 'M',

        [EnumMember]
        Package = 'P',

        [EnumMember]
        ChargeOnly = 'C'
    }

    [DataContract]
    public enum ReportType
    {
        [EnumMember]
        None = ' ',

        [EnumMember]
        Fax = DistributionType.Fax,

        [EnumMember]
        Email = DistributionType.Email,

        [EnumMember]
        Both = 'B',
    }

    [DataContract]
    public enum PreferredDeliveryType
    {
        [EnumMember]
        Phone = 'P',

        [EnumMember]
        Mobile = 'O',

        [EnumMember]
        Email = DistributionType.Email,

        [EnumMember]
        Facebook = 'B',

        [EnumMember]
        Twitter = DistributionType.Twitter,

        [EnumMember]
        Skype = 'S',

        [EnumMember]
        Instagram = 'I',

        [EnumMember]
        LinkedIn = 'L',

        [EnumMember]
        Fax = DistributionType.Fax,

        [EnumMember]
        Mail = 'M'
    }

    [DataContract]
    public enum DistributionSystemType
    {
        [EnumMember]
        Undefined = ' ',

        [EnumMember]
        Topcall = 'T',

        [EnumMember]
        MMS = 'M',

        [EnumMember]
        DistributionEngine = 'D',

        [EnumMember]
        Editorial = 'E',
    }

    [DataContract]
    public enum AdminAccessType
    {
        [EnumMember]
        None = ' ',

        [EnumMember]
        Administrator = 'A',

        [EnumMember]
        Executive = 'X',

        [EnumMember]
        Operator = 'O',

        [EnumMember]
        Supervisor = 'S',

        [EnumMember]
        Query = 'Q',

        [EnumMember]
        Accounts = 'Z',

        [EnumMember]
        Trainee = 'T',

        [EnumMember]
        Trial = 'R'
    }

    [DataContract]
    public enum MessageConnectAccessType
    {
        [EnumMember]
        None = AdminAccessType.None,

        [EnumMember]
        Administrator = AdminAccessType.Administrator,

        [EnumMember]
        Operator = AdminAccessType.Operator,
    }

    [DataContract]
    public enum ContactsAccessType
    {
        [EnumMember]
        None = AdminAccessType.None,

        [EnumMember]
        Administrator = AdminAccessType.Administrator,

        [EnumMember]
        Operator = AdminAccessType.Operator,

        [EnumMember]
        Supervisor = AdminAccessType.Supervisor,

        [EnumMember]
        Trial = AdminAccessType.Trial
    }

    [DataContract]
    public enum ResultsType
    {
        [EnumMember]
        None = ' ',

        [EnumMember]
        All = 'A',

        [EnumMember]
        Negative = 'N'
    }

    [DataContract]
    public enum SystemType
    {
        [EnumMember]
        Medianet = 'M',

        [EnumMember]
        MessageConnect = 'C',

        [EnumMember]
        Journalists = 'J',

        [EnumMember]
        Contacts = 'P',
        
        [EnumMember]
        Admin = 'A',

        [EnumMember]
        Monitoring = 'N',

        [EnumMember]
        MNUser = 'U',
    }

    [DataContract]
    public enum OCRConversionStatusType
    {
        [EnumMember]
        None = ' ',

        [EnumMember]
        Blocked = 'B',

        [EnumMember]
        Queued = 'Q',

        [EnumMember]
        Ready = 'R',

        [EnumMember]
        Failed = 'F'
    }

    [DataContract]
    public enum ReceptionType
    {
        [EnumMember]
        Fax = 'F',

        [EnumMember]
        OldWeb = 'W',

        [EnumMember]
        MNUser = 'M',

        [EnumMember]
        IVR = 'V',

        [EnumMember]
        DistributeWebsite = 'B',

        [EnumMember]
        NewsCentreWebsite = 'N',

        [EnumMember]
        ContactsWebsite = 'P',

        [EnumMember]
        WebService = 'S'
    }

    [DataContract]
    public enum MultimediaType
    {
        [EnumMember]
        None = ' ',

        [EnumMember]
        Audio = 'A',

        [EnumMember]
        Multimedia = 'M',

        [EnumMember]
        Video = 'V',

        [EnumMember]
        WhitePaper = 'P'
    }

    [DataContract]
    public enum OptoutType
    {
        [EnumMember]
        None = ' ',

        [EnumMember]
        Manual = 'M',

        [EnumMember]
        Automatic = 'A'
    }

    [DataContract]
    public enum MediaDirectoryType
    {
        [EnumMember]
        None = ' ',

        [EnumMember]
        Contacts = 'P',

        [EnumMember]
        MediAtlas = 'A'
    }

    [DataContract]
    public enum CustomerBillingType
    {
        [EnumMember]
        Creditcard = 'C',

        [EnumMember]
        Invoiced = 'I',

        [EnumMember]
        InvoicedNotApproved = 'P',
    }

    [DataContract]
    public enum TimezoneType
    {
        [EnumMember] [StringValue("SYD")]
        Sydney = 0,

        [EnumMember] [StringValue("ADL")]
        Adelaide = 1,

        [EnumMember] [StringValue("PER")]
        Perth = 2,

        [EnumMember] [StringValue("BRI")]
        Brisbane = 3
    }

    [DataContract]
    public enum MessageType
    {
        [EnumMember]
        FaxRelease = 'F',

        [EnumMember]
        RadioRelease = 'R'
    }

    [DataContract]
    public enum ReleaseSortOrderType
    {
        [EnumMember]
        CreatedDate = 0,

        [EnumMember]
        DistributedDate = 1,

        [EnumMember]
        ReleaseDescription = 2,

        [EnumMember]
        CustomerReference = 3,

        [EnumMember]
        Priority = 4
    }

    [DataContract]
    public enum InboundFileSortOrderType
    {
        [EnumMember]
        Id = 0,

        [EnumMember]
        LastModifiedDate = 1,

        [EnumMember]
        Caller = 2,

        [EnumMember]
        DistributionType = 3,

        [EnumMember]
        InboundNumber = 4
    }

    [DataContract]
    public enum MediaContactsExportType
    {
        [EnumMember]
        Lists = 0,

        [EnumMember]
        Tasks
    }

    [DataContract]
    public enum LogonErrorType
    {
        [EnumMember]
        None = 0,

        [EnumMember]
        SessionAlreadyExists = 1,

        [EnumMember]
        LicensesExceeded = 2,

        [EnumMember]
        Unknown = 3,
    }

    [DataContract]
    public enum UpdateErrorType
    {
        [EnumMember]
        Generic = 0,

        [EnumMember]
        OutletPrimaryContactRequired = 1,

        [EnumMember]
        OutletMultiplePrimaryContactsExist = 2
    }

    [DataContract]
    public enum MediaContactGroupType
    {
        [EnumMember]
        SavedSearch = 0,

        [EnumMember]
        List = 1,

        [EnumMember]
        Task = 2
    }

    [DataContract]
    public enum MediaContactSearchContext
    {
        [EnumMember]
        Contact = 'p',

        [EnumMember]
        Outlet = 'o',

        [EnumMember]
        Both = 'b'
    }

    [DataContract]
    public enum MediaContactNotificationType
    {
        [EnumMember][StringValue("None")]
        None = -1,

        [EnumMember][StringValue("Email")]
        Email = 0,

        [EnumMember][StringValue("SMS")]
        SMS = 1,

        [EnumMember][StringValue("Email and SMS")]
        Both = 2
    }

    [DataContract]
    public enum MediaContactTaskStatusType
    {
        [EnumMember]
        [StringValue("Not Started")]
        NotStarted = 0,

        [EnumMember]
        [StringValue("In Progress")]
        InProgress = 1,

        [EnumMember]
        [StringValue("On Hold")]
        OnHold = 2,

        [EnumMember]
        [StringValue("Completed")]
        Completed = 3
    }

    [DataContract]
    public enum MediaContactListChangeType
    {
        [EnumMember]
        Added = 'A',

        [EnumMember]
        Deleted = 'D'
    }


    [DataContract]
    public enum SortColumn
    {
        [EnumMember]
        [StringValue("")]
        Default = 0,
        [EnumMember]
        [StringValue("outletname")]
        OutletName = 1,
        [EnumMember]
        [StringValue("fullname")]
        ContactName = 2,
        [EnumMember]
        [StringValue("jobtitle")]
        JobTitle = 3,
        [EnumMember]
        [StringValue("subjectname")]
        Subject = 4,
        [EnumMember]
        [StringValue("producttypename")]
        MediaType = 5,
        [EnumMember]
        [StringValue("cityname")]
        Location = 6,
        [EnumMember]
        [StringValue("servicename")]
        ServiceName = 7,
        [EnumMember]
        [StringValue("updateddate")]
        UpdatedDate = 8,
        [EnumMember]
        [StringValue("updateduser")]
        UpdatedUser = 9,
        [EnumMember]
        [StringValue("disttype")]
        DistributionType = 10,
        [EnumMember]
        [StringValue("ContactInfluencerScore")]
        ContactInfluencerScore = 11
    }

    [DataContract]
    public enum SortDirection
    {
        [EnumMember]
        [StringValue("asc")]
        Asc = 1,
        [EnumMember]
        [StringValue("desc")]
        Desc = 2
    }

    [DataContract]
    public enum WebsiteType
    {
        [EnumMember]
        [StringValue("P")]
        Public = 0,

        [EnumMember]
        [StringValue("J")]
        Journalists = 1,

        [EnumMember]
        [StringValue("")]
        None = 2
    }

    [DataContract]
    public enum PeriodType
    {
        [EnumMember]
        Week = 'W',

        [EnumMember]
        Month = 'M'
    }

    [DataContract]
    public enum ContractType
    {
        [EnumMember]
        [StringValue("P")]
        Database = 1,

        [EnumMember]
        [StringValue("D")]
        Distribution = 2
    }
    [DataContract]
    public enum IndustryCodeType
    {
        [EnumMember]
        [StringValue("Agriculture")]
        X,

        [EnumMember]
        [StringValue("Automotive")]
        A,

        [EnumMember]
        [StringValue("Aviation")]
        R,

        [EnumMember]
        [StringValue("Banking & Finance")]
        C,

        [EnumMember]
        [StringValue("Business Services")]
        D,

        [EnumMember]
        [StringValue("Education & Training")]
        H,

        [EnumMember]
        [StringValue("Energy & Utilities")]
        I,

        [EnumMember]
        [StringValue("Environment")]
        Y,

        [EnumMember]
        [StringValue("Food & Beverages")]
        K,

        [EnumMember]
        [StringValue("Health Services and Products")]
        L,

        [EnumMember]
        [StringValue("Hospitality & Tourism")]
        J,

        [EnumMember]
        [StringValue("Industrial & Construction")]
        M,

        [EnumMember]
        [StringValue("Insurance")]
        O,

        [EnumMember]
        [StringValue("IT and Telecommunications")]
        N,

        [EnumMember]
        [StringValue("Mining & Resources")]
        P,

        [EnumMember]
        [StringValue("PR, Advertising & Marketing")]
        Q,

        [EnumMember]
        [StringValue("Property & Real Estate")]
        T,

        [EnumMember]
        [StringValue("Retail")]
        F,

        [EnumMember]
        [StringValue("Transport & Logistics")]
        V
    }

    [DataContract]
    public enum StateType
    {
        [EnumMember]
        [StringValue("New South Wales")]
        NSW,

        [EnumMember]
        [StringValue("Northern Territory")]
        NT,

        [EnumMember]
        [StringValue("Australian Capital Territory")]
        ACT,

        [EnumMember]
        [StringValue("Victoria")]
        VIC,

        [EnumMember]
        [StringValue("Queensland")]
        QLD,

        [EnumMember]
        [StringValue("Western Australia")]
        WA,

        [EnumMember]
        [StringValue("South  Australia")]
        SA,

        [EnumMember]
        [StringValue("Tasmania")]
        TAS
    }
}
