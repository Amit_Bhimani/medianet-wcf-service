﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.ServiceModel;
using Medianet.Service.BusinessLayer.Common;
using Medianet.Service.Dto;
using Medianet.Service.BusinessLayer;
using Medianet.DataLayer;
using System.Configuration;

namespace Medianet.Service.Common
{
    public class FinanceHelper
    {
        private const int WireDividor = 400;
        private const int WireTolerance = 15;
        /// <summary>
        /// Add a Customer to the Finance system.
        /// </summary>
        /// <param name="customer">The Customer to add.</param>
        /// <returns>The DebtorNumber of the new Customer.</returns>
        public static string AddCustomer(Customer customer, NewAccountRequest account, DBSession session = null)
        {
            using (var svc = new ServiceProxyHelper<FinanceCustomerService.MNCustomerServiceClient, FinanceCustomerService.IMNCustomerService>())
            {
                var finCustomer = new FinanceCustomerService.MedianetCustomer
                {
                    CustomerName = account.CompanyName,
                    CustomerBillingType = customer.AccountType.ToString(),
                    IndustryCode = account.IndustryCode,
                    ABN = account.ABN,
                    ToEmail = (new List<string>() { account.EmailAddress }).ToArray(),

                    PrimaryAddress = new FinanceCustomerService.MedianetCustomerAddress
                    {
                        ContactPerson = $"{account.FirstName} {account.LastName}".Trim().Truncate(50),
                        Fax = account.FaxNumber,
                        Phone1 = account.TelephoneNumber,
                        AddressLine1 = account.AddressLine1,
                        AddressLine2 = account.AddressLine2,
                        City = account.City,
                        State = account.State.Truncate(3),
                        Postcode = account.Postcode,
                        Country = account.Country,
                    }
                };
                if (session != null)
                {
                    if (!AccessHelper.IsVerifiedCustomer(session.DebtorNumber))
                    {
                        finCustomer.MedianetCustomerCode = "F" + session.UserId.ToString();
                    }
                }

                if (string.IsNullOrWhiteSpace(finCustomer.PrimaryAddress.Country))
                    finCustomer.PrimaryAddress.Country = "Australia";

                var finCustomerResponse = svc.Proxy.CreateCustomer(finCustomer);

                return finCustomerResponse.CustomerId;
            }
        }

        public static void UpdateCustomer(Customer customer, Account account, bool isCourseAccount)
        {
            using (var svc = new ServiceProxyHelper<FinanceCustomerService.MNCustomerServiceClient, FinanceCustomerService.IMNCustomerService>())
            {
                FinanceCustomerService.MedianetCustomer finCustomer = svc.Proxy.GetCustomer(customer.DebtorNumber);

                finCustomer.CustomerId = customer.DebtorNumber;
                finCustomer.CustomerName = customer.Name;
                finCustomer.CustomerBillingType = customer.AccountType.ToString();
                finCustomer.IndustryCode = account.IndustryCode;
                finCustomer.ToEmail = (new List<string>() { account.EmailAddress }).ToArray();

                if (!string.IsNullOrWhiteSpace(account.ABN))
                {
                    finCustomer.ABN = account.ABN;
                }

                if (finCustomer.PrimaryAddress == null)
                    finCustomer.PrimaryAddress = new FinanceCustomerService.MedianetCustomerAddress();

                if (isCourseAccount || string.IsNullOrWhiteSpace(finCustomer.PrimaryAddress.ContactPerson))
                {
                    finCustomer.PrimaryAddress.ContactPerson = $"{account.FirstName} {account.LastName}".Trim().Truncate(50);
                    finCustomer.PrimaryAddress.Fax = account.FaxNumber.Truncate(20);
                    finCustomer.PrimaryAddress.Phone1 = account.TelephoneNumber.Truncate(20);
                }

                finCustomer.PrimaryAddress.AddressLine1 = account.CompanyAddress.AddressLine1;
                finCustomer.PrimaryAddress.AddressLine2 = account.CompanyAddress.AddressLine2;
                finCustomer.PrimaryAddress.AddressLine3 = account.CompanyAddress.AddressLine3;
                finCustomer.PrimaryAddress.City = account.CompanyAddress.City;
                finCustomer.PrimaryAddress.State = account.CompanyAddress.State;
                finCustomer.PrimaryAddress.Postcode = account.CompanyAddress.Postcode;
                finCustomer.PrimaryAddress.Country = account.CompanyAddress.Country;

                if (account.BillingAddress != null)
                {
                    if (finCustomer.BillingAddress == null)
                        finCustomer.BillingAddress = new FinanceCustomerService.MedianetCustomerAddress();

                    if (isCourseAccount || string.IsNullOrWhiteSpace(finCustomer.BillingAddress.ContactPerson))
                    {
                        finCustomer.BillingAddress.ContactPerson = $"{account.FirstName} {account.LastName}".Trim().Truncate(50);
                        finCustomer.BillingAddress.Fax = account.FaxNumber.Truncate(20);
                        finCustomer.BillingAddress.Phone1 = account.TelephoneNumber.Truncate(20);
                    }

                    finCustomer.BillingAddress.AddressLine1 = account.BillingAddress.AddressLine1;
                    finCustomer.BillingAddress.AddressLine2 = account.BillingAddress.AddressLine2;
                    finCustomer.BillingAddress.AddressLine3 = account.BillingAddress.AddressLine3;
                    finCustomer.BillingAddress.City = account.BillingAddress.City;
                    finCustomer.BillingAddress.State = account.BillingAddress.State;
                    finCustomer.BillingAddress.Postcode = account.BillingAddress.Postcode;
                    finCustomer.BillingAddress.Country = account.BillingAddress.Country;
                }
                else
                    finCustomer.BillingAddress = null;

                svc.Proxy.UpdateCustomer(finCustomer);
            }
        }

        public static void UpdateCustomerABN(string debtorNumber, string ABN)
        {
            using (var svc = new ServiceProxyHelper<FinanceCustomerService.MNCustomerServiceClient, FinanceCustomerService.IMNCustomerService>())
            {
                FinanceCustomerService.MedianetCustomer finCustomer = svc.Proxy.GetCustomer(debtorNumber);

                if (finCustomer.ABN == ABN)
                    return;

                finCustomer.ABN = ABN;
                svc.Proxy.UpdateCustomer(finCustomer);
            }
        }

        public static FinanceCustomerService.MedianetCustomer GetCustomer(string debtorNumber)
        {
            using (var svc = new ServiceProxyHelper<FinanceCustomerService.MNCustomerServiceClient, FinanceCustomerService.IMNCustomerService>())
            {
                var customer = svc.Proxy.GetCustomer(debtorNumber);

                string email = ConfigurationManager.AppSettings["ReplacingEmailAddress"];
                customer.ToEmail = string.IsNullOrEmpty(email) ? customer.ToEmail : new string[] { email };

                return customer;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<IndustryCode> GetIndustryCodes()
        {
            // Request the industry codes from finance.
            using (var svc = new ServiceProxyHelper<FinanceCustomerService.MNCustomerServiceClient, FinanceCustomerService.IMNCustomerService>())
            {
                return AutoMapper.Mapper.Map<List<FinanceCustomerService.IndustryClass>, List<IndustryCode>>(svc.Proxy.GetIndustryCodes(null).OrderBy(c => c.IndustryClassDesc).ToList());
            }
        }

        /// <summary>
        /// Build the object needed by finance for quoting from a ReleaseQuoteRequest object. This requires fetching extra info.
        /// </summary>
        /// <param name="release">The release to base the finance object off.</param>
        /// <param name="releaseItems">The items to use for each distribution type.</param>
        /// <param name="user">The user submitting the release.</param>
        /// <returns>An MNPricingService.MNTransactionHeader object neded by finance for quoting.</returns>
        public static FinancePaymentService.MNTransactionHeader GenerateFinanceQuoteRequest(Release release, ReleaseItemCounts releaseItems, User user, bool isCourse = false)
        {
            FinancePaymentService.MNTransactionHeader finQuoteRequest = null;
            FinancePaymentService.MNTransaction finTransQuoteRequest = null;
            List<FinancePaymentService.MNTransaction> finTransQuoteRequestList = null;
            Transaction transaction;
            List<int> packageList = new List<int>();
            int i = 0;

            finQuoteRequest = new FinancePaymentService.MNTransactionHeader();

            // If this User has a fake DebtorNumber for billing use it, otherwise use the real one.
            if (user == null || string.IsNullOrWhiteSpace(user.BillerDebtorNumber))
                finQuoteRequest.CustNum = release.DebtorNumber;
            else
                finQuoteRequest.CustNum = user.BillerDebtorNumber;

            finQuoteRequest.TransactionDate = System.DateTime.Now;
            finQuoteRequest.Headline = release.ReleaseDescription;
            finQuoteRequest.Reference = release.CustomerReference;

            finTransQuoteRequestList = new List<FinancePaymentService.MNTransaction>();

            if (release.MultimediaType != MultimediaType.Video && release.MultimediaType != MultimediaType.Multimedia)
            {
                for (i = 0; i <= release.Transactions.Count - 1; i++)
                {
                    transaction = release.Transactions[i];

                    if (transaction.ShouldCharge)
                    {
                        finTransQuoteRequest = new FinancePaymentService.MNTransaction();
                        finTransQuoteRequest.MNTransactionId = transaction.SequenceNumber;
                        finTransQuoteRequest.Description = transaction.SelectionDescription;
                        finTransQuoteRequest.ListCode = transaction.AccountCode;
                        finTransQuoteRequest.ProductCode = GetProductCode(transaction);
                        finTransQuoteRequest.Priority = ((char)ReleasePriorityType.Standard).ToString();
                        finTransQuoteRequest.NumAddresses = transaction.AddressCount;
                        finTransQuoteRequest.NumItems = 1; // Default value

                        if (isCourse)
                        {
                            finTransQuoteRequest.NumAddresses = releaseItems.SeatCount;
                            finTransQuoteRequest.NumItems = 1;
                        }
                        else
                        {
                            switch (transaction.TransactionType)
                            {
                                case DistributionType.Email:
                                    if (release.MultimediaType == MultimediaType.Audio)
                                    {
                                        // Emails for Audio count are a flat rate.
                                        finTransQuoteRequest.NumItems = 1;
                                    }
                                    else if (release.System == SystemType.MessageConnect)
                                    {
                                        // Messageconnect get the first attachment for free.
                                        finTransQuoteRequest.NumItems = releaseItems.AttachmentCount;
                                    }
                                    else
                                    {
                                        // Charge them once for the release and again for each attachment.
                                        finTransQuoteRequest.NumItems = 1 + releaseItems.AttachmentCount;
                                    }
                                    break;

                                case DistributionType.Wire:
                                    // change here
                                    int itemCount = (int)Math.Floor((decimal)(releaseItems.WireWordCount / WireDividor));

                                    if ((releaseItems.WireWordCount % WireDividor) > WireTolerance)
                                        itemCount++;
                                    finTransQuoteRequest.NumItems = itemCount;
                                    break;

                                case DistributionType.Internet:
                                    // Don't charge for website postings on white papers
                                    if (release.MultimediaType == MultimediaType.WhitePaper)
                                        continue;

                                    break;

                                case DistributionType.Image:
                                    finTransQuoteRequest.NumItems = releaseItems.ImageCount;
                                    break;

                                case DistributionType.ChargeOnly:
                                    if (transaction.AccountCode == Constants.ACCOUNTCODE_ATTACHMENT_FEE)
                                    {
                                        if (releaseItems.AttachmentCount <= 0)
                                            continue;

                                        finTransQuoteRequest.NumItems = releaseItems.AttachmentCount;
                                    }

                                    break;

                                case DistributionType.Voice:
                                    //tqRequest.NumItems = release.VoiceSecondCount;
                                    // Code to calculate video length not implemented.
                                    throw new NotImplementedException("Voice length calculation not implemented yet.");
                                    
                                    //Getting error for SMS 
                                //default:
                                //    throw new NotImplementedException("Unknown transaction type '" + transaction.TransactionType.ToString() + "'.");
                            }
                        }

                        // In case something went wrong charge them for at least 1 ItemCount.
                        if (finTransQuoteRequest.NumItems < 1)
                            finTransQuoteRequest.NumItems = 1;

                        if (transaction.DebtorNumber == BusinessLayer.Common.Constants.DEBTOR_NUMBER_AAP && transaction.PackageId.HasValue && transaction.PackageId.Value > 0)
                        {
                            // Needed for some stupid lambda expression limitation.
                            int packageId = transaction.PackageId.Value;

                            // Only add the first transaction of a package. It's a flat rate.
                            if (!packageList.Exists(pId => pId == packageId))
                            {
                                finTransQuoteRequest.Description = transaction.PackageDescription;

                                // Set the MNTransactionId to the lowest SequenceNumber for this package. This is the only
                                // one that existed in the original request. The rest were generated.
                                finTransQuoteRequest.MNTransactionId = release.Transactions.Where(t => t.PackageId.HasValue && t.PackageId.Value == packageId).Min(t => t.SequenceNumber);

                                finTransQuoteRequestList.Add(finTransQuoteRequest);

                                // Add the PackageID to this temporary list to remember which ones we have already added.
                                packageList.Add(packageId);
                            }
                        }
                        else
                        {
                            if (transaction.DebtorNumber != Constants.DEBTOR_NUMBER_AAP && (transaction.DistributionType == DistributionType.SMS || transaction.DistributionType == DistributionType.Voice))
                            {
                                // We need to charge different rates based on the country it's going to
                                // so fetch all the addresses to send to finance.
                                finTransQuoteRequest.MNTransDetails = GetRecipients(release, transaction);
                                finTransQuoteRequest.NumAddresses = finTransQuoteRequest.MNTransDetails.Length;
                                transaction.AddressCount = finTransQuoteRequest.NumAddresses;
                            }

                            finTransQuoteRequestList.Add(finTransQuoteRequest);
                        }
                    }
                }
            }
            else
            {
                throw new NotImplementedException("MNR and VNR billing not implemented yet.");
            }

            finQuoteRequest.MNTransactions = finTransQuoteRequestList.ToArray();
            return finQuoteRequest;
        }

        /// <summary>
        /// Fetch a quote (Estimate Invoice API) from ChargeBee
        /// </summary>
        /// <param name="request">The details of the release.</param>
        /// <returns>An MNPricingService.TransactionPriceResponse object containing the quote.</returns>
        public static FinancePaymentService.TransactionPriceResponse QuoteRelease(FinancePaymentService.MNTransactionHeader request)
        {
            var fTransactionPriceResponce = new FinancePaymentService.TransactionPriceResponse();
            fTransactionPriceResponce.ContractExpired = false;
            fTransactionPriceResponce.CustNum = request.CustNum;
            fTransactionPriceResponce.TransactionHeaderId = request.TransactionHeaderId;
            fTransactionPriceResponce.ExtensionData = request.ExtensionData;
            //check for Custom price for that customer.
            List<CustomerCustomPricing> pricelist = GetCustomerCustomPricings(request.CustNum);
            Medianet.Service.ChargeBeeService cb = new ChargeBeeService();
            for (int i = 0; i < request.MNTransactions.Length; i++)
            {
                CustomerCustomPricing customPrice = null;
                string addOnId = "mn-" + request.MNTransactions[i].ProductCode.ToLower();
                Medianet.Service.FinancePaymentService.TransactionPriceDetail transDetails = new FinancePaymentService.TransactionPriceDetail();
                transDetails.Description = request.MNTransactions[i].Description;
                transDetails.Qty = 1;
                if (pricelist.Count > 0)
                {
                    customPrice = pricelist.Where(x => x.ProductCode == request.MNTransactions[i].ListCode).FirstOrDefault();
                }
                bool isRecordFound = false;
                if (customPrice != null)
                {
                    transDetails.UnitPrice = (double)customPrice.CostPerUnit;
                    transDetails.SubTotalEx = (decimal)customPrice.CostPerUnit;
                    isRecordFound = true;
                }
                var quantity = request.MNTransactions[i].NumAddresses * request.MNTransactions[i].NumItems;
                var getAdd = cb.CreateEstimateInvoice(request.CustNum, addOnId, transDetails.SubTotalEx, quantity, isRecordFound);
                if (getAdd == null)
                {
                    throw new Exception("Package not Found in ChargeBee for Price.");
                }
                transDetails.UnitPrice = (double)getAdd.Price / 100;
                transDetails.SubTotalEx = (decimal)getAdd.Price / 100;
                transDetails.GST = (decimal)getAdd.GST / 100;
                transDetails.ListCode = request.MNTransactions[i].ListCode;
                transDetails.NumItems = request.MNTransactions[i].NumItems;
                transDetails.ExtensionData = request.MNTransactions[i].ExtensionData;
                transDetails.MNTransactionID = request.MNTransactions[i].MNTransactionId;
                transDetails.NumAddresses = request.MNTransactions[i].NumAddresses;
                transDetails.Reference = addOnId;//request.MNTransactions[i].Reference

                fTransactionPriceResponce.TransactionPriceDetails = ExtensionMethods.Append(fTransactionPriceResponce.TransactionPriceDetails, transDetails);
            }
            if (fTransactionPriceResponce.TransactionPriceDetails != null && fTransactionPriceResponce.TransactionPriceDetails.Length > 0)
            {
                fTransactionPriceResponce.Total = fTransactionPriceResponce.TransactionPriceDetails.Sum(x => x.SubTotalEx) + fTransactionPriceResponce.TransactionPriceDetails.Sum(x => x.GST);
                fTransactionPriceResponce.GST = fTransactionPriceResponce.TransactionPriceDetails.Sum(x => x.GST);
                fTransactionPriceResponce.ExGST = fTransactionPriceResponce.TransactionPriceDetails.Sum(x => x.SubTotalEx);
            }
            return fTransactionPriceResponce;
        }

        /// <summary>
        /// Request a Creditcard access code from Finance.
        /// </summary>
        /// <param name="request">The details of the release.</param>
        /// <returns>An MNPricingService.MedianetPaymentPage object containing the quote.</returns>
        public static FinancePaymentService.MedianetPaymentPage RequestCreditcardAccessCode(FinancePaymentService.PaymentPageRequest request)
        {
            //Request the EWay Creditcard access code from finance.
            using (var svc = new ServiceProxyHelper<FinancePaymentService.MNFinanceServiceClient, FinancePaymentService.IMNFinanceService>())
            {
                return svc.Proxy.GetPaymentPage(request);
            }
        }

        public static FinancePaymentService.MedianetPaymentResult RequestCreditcardResult(string paymentAccessCode)
        {
            // Request the EWay Creditcard access code from finance.
            using (var svc = new ServiceProxyHelper<FinancePaymentService.MNFinanceServiceClient, FinancePaymentService.IMNFinanceService>())
            {
                return svc.Proxy.GetPaymentResult(paymentAccessCode);
            }
        }

        public static string FormatNumberForFinance(string strNumber)
        {
            string result = null;
            string strTemp = null;

            // Remove any unwanted characters. The @ avoids the need to escape the \ characters.
            strTemp = Regex.Replace(strNumber, @"[^0-9]", "");

            if (strTemp[0] == '0' && strTemp.Length > 1)
            {
                if (strTemp.StartsWith("0011") || strTemp.StartsWith("0015") || strTemp.StartsWith("0019"))
                {
                    // Get rid of the international dialing code.
                    result = strTemp.Substring(4);
                }
                else
                {
                    // Must start with an area code. Put the australian country code on it and remove the 0.
                    result = "61" + strTemp.Substring(1);
                }
            }
            else
            {
                // Leave the number alone.
                result = strTemp;
            }

            return (result.Length <= 20 ? result : result.Substring(0, 20));
        }

        #region Private Methods

        private static FinancePaymentService.MNTransDetail[] GetRecipients(Release release, Transaction transaction)
        {
            List<FinancePaymentService.MNTransDetail> tDetailsList = new List<FinancePaymentService.MNTransDetail>();

            if (transaction.IsSingleRecipient)
            {
                tDetailsList.Add(new FinancePaymentService.MNTransDetail
                {
                    PhoneNum = FormatNumberForFinance(transaction.SingleRecipientAddress),
                    SeqNum = 1
                });
            }
            else if (transaction.IsMailMerge)
            {
                List<ConverterService.RecipientData> recipList;
                Document mmDoc = ReleaseHelper.GetMailMergeDataSourceDocument(release.Documents);

                if (mmDoc == null || !mmDoc.TempFileId.HasValue)
                    throw new Exception("Failed to find Mailmerge data source document.");

                recipList = CasHelper.GetRecipientsForCSV(mmDoc.TempFileId.Value, transaction.DistributionType);

                // Convert them to RecipientQuoteRequestData objects to get a quote.
                foreach (ConverterService.RecipientData r in recipList)
                {
                    FinancePaymentService.MNTransDetail rq = new FinancePaymentService.MNTransDetail();
                    rq.PhoneNum = FormatNumberForFinance(r.Address);
                    rq.SeqNum = tDetailsList.Count + 1;
                    tDetailsList.Add(rq);
                }
            }
            else
            {
                List<Recipient> recipList;

                using (var uow = new UnitOfWork())
                {
                    var listBL = new ServiceListBL(uow);
                    recipList = listBL.GetRecipients(transaction.ServiceId.Value);
                }

                // Convert them to RecipientQuoteRequestData objects to get a quote.
                foreach (Recipient r in recipList)
                {
                    FinancePaymentService.MNTransDetail rq = new FinancePaymentService.MNTransDetail();
                    rq.PhoneNum = FormatNumberForFinance(r.Address);
                    rq.SeqNum = tDetailsList.Count + 1;
                    tDetailsList.Add(rq);
                }
            }

            return tDetailsList.ToArray();
        }

        private static string GetProductCode(Transaction transaction)
        {
            if (transaction.DebtorNumber == BusinessLayer.Common.Constants.DEBTOR_NUMBER_AAP)
            {
                if (transaction.PackageId.HasValue)
                    return string.Format("P{0}", transaction.PackageId.Value);
                else if (transaction.ServiceId.HasValue)
                    return string.Format("S{0}", transaction.ServiceId.Value);
            }

            return transaction.AccountCode;
        }
        
        private static List<CustomerCustomPricing> GetCustomerCustomPricings(string debtorNumber)
        {
            try
            {
                List<CustomerCustomPricing> priceList = new List<CustomerCustomPricing>();
                using (var uow = new UnitOfWork())
                {
                    var listBL = new CustomerCustomPricingBL(uow);
                    priceList = listBL.GetAllCustomPricing(debtorNumber);
                }
                return priceList;
            }
            catch (Exception ex)
            {   
                NLog.LogManager.GetCurrentClassLogger()
                                          .LogException(NLog.LogLevel.Error,
                                              "Error while fetching Customer Custom Pricings  with DebtorNumber " + debtorNumber + " from Medianet Database.", ex);
                throw;
            }
        }
        #endregion
    }
}
