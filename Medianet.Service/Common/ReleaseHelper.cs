﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Configuration;
using Medianet.Service.Dto;
using Medianet.Service.BusinessLayer;
using Medianet.DataLayer;
using Medianet.Service.BusinessLayer.Common;

namespace Medianet.Service.Common
{
    public class ReleaseHelper
    {
        public const int PACKAGE_SEQUENCE_NUMBER_OFFSET = 100;
        public const int MAX_WEB_RELEASE_SUMMARY_LENGTH = 200;

        public static Release GenerateReleaseFromSummary(ReleaseSummary relSummary, DBSession session) {
            Release release;

            // Create the release.
            release = AutoMapper.Mapper.Map<ReleaseSummary, Release>(relSummary);

            return GenerateRelease(release, relSummary.Transactions, relSummary.Documents, relSummary.TimezoneCode, session);
        }

        public static Release GenerateReleaseFromQuote(ReleaseQuoteRequest relQuote, DBSession session) {
            Release release;

            // Create the release.
            release = AutoMapper.Mapper.Map<ReleaseQuoteRequest, Release>(relQuote);
            release.ReleaseDescription = relQuote.ReleaseDescription;

            return GenerateRelease(release, relQuote.Transactions, relQuote.Documents, null, session);
        }

        public static Release GenerateReleaseFromCalculator(ReleaseCalculatorRequest relCalc, DBSession session) {
            Release release;

            // Create the release.
            release = AutoMapper.Mapper.Map<ReleaseCalculatorRequest, Release>(relCalc);

            return GenerateRelease(release, relCalc.Transactions, null, null, session);
        }

        public static Release GenerateReleaseForCourse(int scheduleId, string debtorNumber, string promoCode)
        {
            Release release = new Release();
            int packageId;
            // Create the release.

            release.System = SystemType.Medianet;
            release.ReleaseDescription = "Registration for course";
            release.DebtorNumber = debtorNumber;

            // Get the PackageId for this schedule
            using (var uow = new UnitOfWork())
            {
                var courseBL = new TrainingCourseBL(uow);
                var sched = courseBL.GetSchedule(scheduleId);
                if (sched == null)
                    throw new Exception("Course schedule not found");

                var course = courseBL.Get(sched.TrainingCourseId);
                if (course == null)
                    throw new Exception("Course not found.");

                if (!course.PackageId.HasValue)
                    throw new Exception("Course has no package assigned to it.");

                packageId = course.PackageId.Value;

                if (!string.IsNullOrWhiteSpace(promoCode))
                {
                    var code = course.PromoCodes.FirstOrDefault(
                        c => c.PromoCode.Equals(promoCode, StringComparison.CurrentCultureIgnoreCase));

                    if (code != null)
                        packageId = code.PackageId;
                }

                if (course.EventType != EventType.Event)
                {
                    // For a course make sure it doesn't get over booked
                    var seatsTaken = sched.Enrolments.Count(e => e.RowStatus == RowStatusType.Active);
                    if (sched.Seats <= seatsTaken)
                        throw new Exception("This course is booked out.");
                }
            }

            release.Transactions = new List<Transaction>();
            release.Transactions.AddRange(GetTransactionFromPackage(packageId, 1, null, null, 0));

            return release;
        }

        private static Release GenerateRelease(Release release, List<TransactionSummary> transactions, List<DocumentSummary> documents, string timezoneCode, DBSession session) {
            release.ReleaseDescription = release.ReleaseDescription.ConvertToAscii().Truncate(256);
            release.CustomerReference = EmptyStringIfNull(release.CustomerReference).ConvertToAscii().Truncate(30);
            release.BillingCode = EmptyStringIfNull(release.BillingCode).ConvertToAscii().Truncate(16);
            release.SMSText = EmptyStringIfNull(release.SMSText).ConvertToAscii().Truncate(160);
            release.EmailFromName = EmptyStringIfNull(release.EmailFromName).ConvertToAscii().Truncate(100);
            release.EmailFromAddress = EmptyStringIfNull(release.EmailFromAddress).ConvertToAscii().Truncate(100);
            release.ReportEmailAddress = EmptyStringIfNull(release.ReportEmailAddress).ConvertToAscii().Truncate(200);

            release.Status = ReleaseStatusType.Converting;
            release.WireDescription = EmptyStringIfNull(release.WireDescription).ConvertToAscii().Truncate(80);
            release.IsOpen = true;
            release.OpenedDate = DateTime.Now;
            release.IsInQueue = (release.WebDetails.IsVerified);
            release.SpecialAddressText = string.Empty;
            release.Comment = string.Empty;
            release.BadTifPages = string.Empty;
            release.ReceptionMethod = ReceptionType.DistributeWebsite;
            release.OCRStatus = OCRConversionStatusType.None;
            release.NextStatus = ReleaseStatusType.None;
            release.CreatedDate = release.OpenedDate.Value;
            release.CreatedByUserId = session.UserId;

            // If the timezone isn't sydney then adjust hold and embargo.
            if (release.HoldUntilDate.HasValue || release.EmbargoUntilDate.HasValue)
            {
                if (!string.IsNullOrWhiteSpace(timezoneCode) && timezoneCode != Constants.TIMEZONE_SYD) {
                    try {
                        TimezoneHelper tzHelper = new TimezoneHelper(timezoneCode, Constants.TIMEZONE_SYD);
                        if (release.HoldUntilDate.HasValue) release.HoldUntilDate = tzHelper.AdjustDateTime(release.HoldUntilDate.Value);
                        if (release.EmbargoUntilDate.HasValue) release.EmbargoUntilDate = tzHelper.AdjustDateTime(release.EmbargoUntilDate.Value);
                    }
                    catch (TimeZoneNotFoundException ex) {
                        string error = string.Format("Error adjusting timezone of hold date {0} and embargo date {1} from {2} to {3} for release with Headline {3}.", 
                            release.HoldUntilDate.HasValue ? release.HoldUntilDate.Value.ToString("g") : "none",
                            release.EmbargoUntilDate.HasValue ? release.EmbargoUntilDate.Value.ToString("g") : "none",
                            timezoneCode, Constants.TIMEZONE_SYD, release.ReleaseDescription);
                        release.Status = ReleaseStatusType.New;
                        release.Comment = error;
                        NLog.LogManager.GetCurrentClassLogger().LogException(NLog.LogLevel.Error, error, ex);
                        EmailHelper.GeneralErrorEmail(error, ex);
                    }
                }
            }

            // Create all the documents.
            // NOTE: Do this before Transactions as mailerge transactions need info from the Documents.
            release.Documents = GenerateDocumentsFromSummary(documents, session);

            // Create all the transactions.
            release.Transactions = GenerateTransactionsFromSummary(release.System, release.DebtorNumber, transactions, release.Documents, session);

            // Create the WebDetails if being published to the MNJ website.
            release.WebDetails = GenerateWebReleaseFromRelease(release);

            // If we have a web release get the summary from the text conversion of the email/wire html.
            if (release.WebDetails != null)
                release.WebDetails.Summary = GetSummaryText(release.Documents);

            // If sending to any AAP email addresses ignore any sender address provided.
            if (release.Transactions.Exists(t => t.DebtorNumber == Constants.DEBTOR_NUMBER_AAP && t.DistributionType == DistributionType.Email)) {
                release.UseCustomerEmailAddress = false;
                release.EmailFromAddress = string.Empty;
                release.EmailFromName = string.Empty;
            }
            else if (!string.IsNullOrWhiteSpace(release.EmailFromAddress))
                release.UseCustomerEmailAddress = true;
            else
                release.UseCustomerEmailAddress = false;

            using (var uow = new UnitOfWork()) {
                var userBL = new UserBL(uow);
                User user = userBL.Get(release.CreatedByUserId.Value);
                if (user != null) {
                    if (user.ReportSendMethod == ReportType.Email || user.ReportSendMethod == ReportType.Both)
                        release.ReportEmailAddress = user.EmailAddress;

                    release.ReportResultsToSend = user.ReportResultsToSend;
                }
            }

            return release;
        }

        public static List<Transaction> GenerateTransactionsFromSummary(SystemType system, string debtorNumber, List<TransactionSummary> transSummaryList, List<Document> documents, DBSession session) {
            List<Transaction> transactions = new List<Transaction>();

            // Create all the transactions.
            if (transSummaryList != null && transSummaryList.Count > 0) {
                foreach (var transSummary in transSummaryList) {
                    if (transSummary.IsSingleRecipient)
                        transactions.Add(Transaction.CreateSingleAddressTransaction(system, transSummary.DistributionType, transSummary.SingleRecipientAddress, transSummary.SequenceNumber, debtorNumber, session.UserId));
                    else if (transSummary.IsMailMerge)
                        transactions.Add(GetTransactionForMailmerge(transSummary, documents, session));
                    else if (transSummary.IsAttachmentFee)
                        transactions.Add(Transaction.CreateAttachmentFeeTransaction(system, transSummary.SequenceNumber, session.UserId));
                    else if (transSummary.IsAnrFee)
                        transactions.Add(Transaction.CreateAnrFeeTransaction(system, transSummary.SequenceNumber, session.UserId));
                    else if (transSummary.IsSecondaryWebCategory)
                        transactions.Add(Transaction.CreateSecondaryWebCategoryTransaction(system, transSummary.SequenceNumber, session.UserId));
                    else if (transSummary.IsHighPriorityFee)
                        transactions.Add(Transaction.CreateHighPriorityFeeTransaction(system, transSummary.SequenceNumber, session.UserId));
                    else if (transSummary.PackageId.HasValue && transSummary.PackageId.Value > 0)
                        transactions.AddRange(GetTransactionFromPackage(transSummary.PackageId.Value, transSummary.SequenceNumber, transSummary.TwitterText, transSummary.TwitterCategoryId, session.UserId));
                    else if (transSummary.ServiceId.HasValue && transSummary.ServiceId.Value > 0)
                        transactions.Add(GetTransactionFromService(transSummary.ServiceId.Value, transSummary.SequenceNumber, transSummary.TwitterText, transSummary.TwitterCategoryId, session.UserId));
                    else
                        throw new Exception("Unknown Transaction type.");
                }
            }

            return transactions;
        }

        public static List<Document> GenerateDocumentsFromSummary(List<DocumentSummary> docSummaryList, DBSession session) {
            using (var uow = new UnitOfWork()) {
                var tempBL = new TempFileBL(uow);
                List<Document> documents = new List<Document>();

                if (docSummaryList != null && docSummaryList.Count > 0) {
                    int sequenceNo = (from d in docSummaryList select d.SequenceNumber).Max();

                    foreach (var docSummary in docSummaryList) {
                        if (docSummary.TempFileId.HasValue && docSummary.TempFileId.Value > 0) {
                            TempFile tFile = tempBL.Get(docSummary.TempFileId.Value);
                            Document doc = Document.CreateFromTempFileAndSummary(tFile, docSummary, session.UserId);

                            // Hack to fix the website incorrectly setting CanDistributeViaEmailAttachment to true for the Release document.
                            if (session.System == SystemType.Medianet && doc.CanDistributeViaWeb && doc.CanDistributeViaEmailAttachment)
                                doc.CanDistributeViaEmailAttachment = false;
                            // End hack.
                            // Hack to fix the website incorrectly setting CanDistributeViaWeb to true for the Attachment documents.
                            if (session.System == SystemType.Medianet && !doc.CanDistributeViaWire && doc.CanDistributeViaWeb && doc.CanDistributeViaEmailAttachment)
                                doc.CanDistributeViaWeb = false;
                            // End hack.

                            documents.Add(doc);

                            // Get all derived documents created for this TempFile and add them too.
                            if (doc.CanDistributeViaWire || doc.CanDistributeViaWeb) {
                                List<TempFile> children = tempBL.GetAllByParentId(docSummary.TempFileId.Value);

                                foreach (var tChild in children) {
                                    sequenceNo += 1;
                                    documents.Add(Document.CreateDerivedFromTempFile(tChild, sequenceNo, doc.CreatedByUserId));
                                }
                            }
                        }
                        else if (docSummary.InboundFileId.HasValue && docSummary.InboundFileId.Value > 0) {
                            var inboundBL = new InboundFileBL(uow);
                            var iFile = inboundBL.Get(docSummary.InboundFileId.Value);
                            Document doc = Document.CreateFromInboundFileAndSummary(iFile, docSummary, session.UserId);
                            documents.Add(doc);
                        }
                    }
                }

                return documents;
            }
        }

        public static WebRelease GenerateWebReleaseFromRelease(Release release) {
            WebRelease webRelease = null;

            // Create the WebDetails even it not published on the internet.

            webRelease = release.WebDetails;

            webRelease.Headline = EmptyStringIfNull(webRelease.Headline).ConvertToAscii().Truncate(256);
            webRelease.Organisation = EmptyStringIfNull(webRelease.Organisation).ConvertToAscii().Truncate(100);

            // Default to the default web category if we don't get one.
            if (!webRelease.PrimaryWebCategoryId.HasValue || webRelease.PrimaryWebCategoryId.Value == 0)
                webRelease.PrimaryWebCategoryId = BusinessLayer.Common.Constants.DEFAULT_WEB_CATEGORY_ID;

            webRelease.ShouldShowOnPublic = release.Transactions.Exists(t => t.DistributionType == DistributionType.Internet && t.ShouldDistribute && t.ServiceId == Constants.PUBLIC_WEBSITE_SERVICE_ID);
            webRelease.ShouldShowOnJournalists = release.Transactions.Exists(t => t.DistributionType == DistributionType.Internet && t.ShouldDistribute && t.ServiceId == Constants.MNJ_WEBSITE_SERVICE_ID);
            webRelease.WasSentToJournalists = false;
            webRelease.IsWidgetUpdated = true;
            webRelease.IsVerified = webRelease.IsVerified;

            webRelease.HasVideo = release.Documents.Exists(d => !d.IsDerived && 
                d.ContentType.IndexOf("video", StringComparison.CurrentCultureIgnoreCase) >= 0);
            webRelease.HasAudio = release.Documents.Exists(d => !d.IsDerived && 
                d.ContentType.IndexOf("audio", StringComparison.CurrentCultureIgnoreCase) >= 0);
            webRelease.HasPhoto = release.Documents.Exists(d => !d.IsDerived &&
                !d.FileExtension.StartsWith("tif", StringComparison.CurrentCultureIgnoreCase) &&
                d.ContentType.IndexOf("image", StringComparison.CurrentCultureIgnoreCase) >= 0);
            webRelease.HasOther = release.Documents.Exists(d => !d.IsDerived &&
                (d.FileExtension.StartsWith("tif", StringComparison.CurrentCultureIgnoreCase) ||
                    (d.ContentType.IndexOf("video", StringComparison.CurrentCultureIgnoreCase) < 0 &&
                    d.ContentType.IndexOf("audio", StringComparison.CurrentCultureIgnoreCase) < 0 &&
                    d.ContentType.IndexOf("image", StringComparison.CurrentCultureIgnoreCase) < 0)));

            return webRelease;

        }

        public static void LogXML<T>(object xmlObject) {
            try {
                StringBuilder xml = new StringBuilder();
                var serializer = new System.Runtime.Serialization.DataContractSerializer(typeof(T));
                using (var xw = System.Xml.XmlWriter.Create(xml)) {
                    serializer.WriteObject(xw, xmlObject);
                    xw.Flush();
                    NLog.LogManager.GetCurrentClassLogger().Log(NLog.LogLevel.Info, xml.ToString());
                }
            }
            catch (Exception ex) {
                NLog.LogManager.GetCurrentClassLogger().LogException(NLog.LogLevel.Error, "Error serializing ReleaseQuoteRequest for logging.", ex);
            }
        }

        private static List<Transaction> GetTransactionFromPackage(int packageId, int sequenceNo, string twitterText, int? twitterCategoryId, int userId) {
            PackageList package;
            List<ServiceList> services;
            var transactions = new List<Transaction>();
            int offset = 0;

            using (var uow = new UnitOfWork())
            {
                var listBL = new PackageListBL(uow);
                package = listBL.Get(packageId);
                services = listBL.GetServicesById(packageId);

            }

            if (package == null)
                throw new Exception(string.Format("Package with Id {0} not found.", packageId));

            foreach (var s in services) {
                // Add all the transactions for the package. Give them all a unique sequence number, but make sure the first one stays the same.
                var t = Transaction.CreatePackageTransaction(package, s, sequenceNo + (PACKAGE_SEQUENCE_NUMBER_OFFSET * offset), userId);

                if (t.DistributionType == DistributionType.Twitter) {
                    t.TweetText = twitterText;
                    t.TwitterCategoryId = twitterCategoryId;
                }

                transactions.Add(t);
                offset += 1;
            }

            return transactions;
        }

        private static Transaction GetTransactionFromService(int serviceId, int sequenceNo, string twitterText, int? twitterCategoryId, int userId) {
            ServiceList service;

            using (var uow = new UnitOfWork()) {
                var listBL = new ServiceListBL(uow);
                service = listBL.Get(serviceId);
            }

            if (service == null)
                throw new Exception(string.Format("Service with Id {0} not found.", serviceId));

            var t = Transaction.CreateServiceTransaction(service, sequenceNo, userId);

            if (t.DistributionType == DistributionType.Twitter) {
                t.TweetText = twitterText;
                t.TwitterCategoryId = twitterCategoryId;
            }

            return t;
        }

        private static Transaction GetTransactionForMailmerge(TransactionSummary transSummary, List<Document> documents, DBSession session) {
            List<Document> csvDocs = documents.Where(d => d.IsMailMergeDataSource && d.IsDerived).ToList();

            if (csvDocs.Count == 0)
                throw new Exception("Failed to find derived Mailmerge Datasource document.");

            return Transaction.CreateMailmergeTransaction(session.System, transSummary.DistributionType, transSummary.SequenceNumber, session.DebtorNumber, csvDocs[0].ItemCount, session.UserId);
        }

        public static ReleaseItemCounts GenerateReleaseItemCounts(Release release) {
            var releaseItems = new ReleaseItemCounts();

            // If we have Wire transactions then get the Wire word count.
            if (release.HasWireTransactions())
                releaseItems.WireWordCount = ReleaseHelper.GetWireWordCount(release.Documents, release.WireWords);

            releaseItems.AttachmentCount = ReleaseHelper.GetAttachmentCount(release.Documents);
            releaseItems.ImageCount = ReleaseHelper.GetImageCount(release.Documents);

            return releaseItems;
        }

        public static Document GetMailMergeDataSourceDocument(List<Document> documents) {
            List<Document> mmDoc = documents.Where(d => d.IsMailmergeDataFile()).ToList();

            if (mmDoc.Count == 0)
                return null;
            else
                return mmDoc[0];
        }

        public static bool IsMaxUnverifiedReleases(User user)
        {
            int MaxReleases;

            if (!int.TryParse(ConfigurationManager.AppSettings["MaxUnverifiedReleases"], out MaxReleases))
            {
                MaxReleases = 2;
            }

            return (user.ReleaseCount >= MaxReleases);
        }

        ///// <summary>
        ///// Get the number of wire words for this Release.
        ///// </summary>
        ///// <param name="documents">The list of Documents belonging to the Release.</param>
        ///// <returns>The number of wire words.</returns>
        //private static int GetWireWordCount(List<Document> documents) {
        //    return GetItemCount(documents, (d => d.CanDistributeViaWire && d.FileExtension.Equals(BusinessLayer.Common.Constants.FILETYPE_TEXT, StringComparison.CurrentCultureIgnoreCase)), BusinessLayer.Common.Constants.FILETYPE_TIFF);
        //}
        // no need
        //private static int GetItemCount(List<Document> documents, Func<Document, bool> documentFunc, string fileExt) {
        //    // First look for a derived document.
        //    List<Document> docs = documents.Where(documentFunc).Where(d => d.IsDerived).ToList();

        //    if (docs.Count > 0)
        //        return docs.Sum(d => d.ItemCount);

        //    //// We didn't find a derived document. Look for any originals.
        //    //docs = documents.Where(documentFunc).ToList();
        //    //if (docs.Count > 0)
        //    //    return docs.Sum(d => d.ItemCount);

        //    return 0;
        //}

        private static int GetAttachmentCount(List<Document> documents) {
            return documents.Count(d => d.IsEmailAttachmentFile());
        }

        private static int GetImageCount(List<Document> documents)
        {
            return documents.Count(d => d.IsImageFile());
        }

        private static string EmptyStringIfNull(string value) {
            if (string.IsNullOrEmpty(value))
                return string.Empty;

            return value;
        }

        private static string GetSummaryText(List<Document> documents) {
            string summary = string.Empty;

            foreach (var d in documents) {
                if (d.CanDistributeViaWire && d.IsDerived) {
                    using (StreamReader sr = new StreamReader(d.SavedFile))
                    {
                        summary = sr.ReadToEnd();
                    }
                    break;
                }
            }
            // Truncate to the max length and append '...'
            summary = TruncateToLength(summary, MAX_WEB_RELEASE_SUMMARY_LENGTH);

            return summary;
        }

        public static string TruncateToLength(string input, int length) {
            string output;

            if (input.Length > length) {
                var pos = input.LastIndexOf(" ", length - 3);

                if (pos > 0)
                    output = input.Substring(0, pos) + "...";
                else
                    output = input.Truncate(length - 3) + "...";
            }
            else
                output = input;

            return output;
        }
        /// <summary>
        /// Get the number of wire words for this Release.
        /// </summary>
        /// <param name="documents">The list of Documents belonging to the Release.</param>
        /// <param name="releaseWordCount">Release Word Count belonging to the Release.</param>
        /// <returns>The number of wire words.</returns>
        private static int GetWireWordCount(List<Document> documents, int releaseWordCount)
        {
            var x = documents.Where(d => d.CanDistributeViaWire && d.FileExtension.Equals("txt", StringComparison.CurrentCultureIgnoreCase))
                .OrderByDescending(d => d.IsDerived).FirstOrDefault();

            if (x == null)
                return releaseWordCount;

            return x.ItemCount;
        }

    }
}
