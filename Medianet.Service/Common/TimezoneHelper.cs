﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.BusinessLayer;
using Medianet.DataLayer;

namespace Medianet.Service.Common
{
    public class TimezoneHelper
    {
        private TimeZoneInfo _sourceZone;
        private TimeZoneInfo _destZone;

        //public TimezoneHelper() {
        //}

        public TimezoneHelper(string sourceCode, string destCode) {
            using (var uow = new UnitOfWork()) {
                var timezoneBL = new TimezoneBL(uow);
                _sourceZone = timezoneBL.GetTimezoneInfo(sourceCode);
                _destZone = timezoneBL.GetTimezoneInfo(destCode);
            }
        }

        //public string SourceTimeZoneCode {
        //    set { _sourceZone = GetTimezone(value); }
        //}

        //public string DestinationTimeZoneCode {
        //    set { _destZone = GetTimezone(value); }
        //}

        //public TimeZoneInfo SourceTimeZone {
        //    set { _sourceZone = value; }
        //}

        //public TimeZoneInfo DestinationTimeZone {
        //    set { _destZone = value; }
        //}

        public DateTime AdjustDateTime(DateTime date) {
            // We need to create a new date with the Kind specified as Unspecified. Otherwise the ConvertTime method complains.
            DateTime sourceDate = new DateTime(date.Year,date.Month, date.Day, date.Hour, date.Minute, date.Second, date.Millisecond, DateTimeKind.Unspecified);

            return TimeZoneInfo.ConvertTime(sourceDate, _sourceZone, _destZone);
        }

        private TimeZoneInfo GetTimezone(string code) {
            using (var uow = new UnitOfWork()) {
                var timezoneBL = new TimezoneBL(uow);
                return timezoneBL.GetTimezoneInfo(code);
            }
        }
    }
}
