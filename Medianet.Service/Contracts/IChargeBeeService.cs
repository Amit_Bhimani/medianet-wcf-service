﻿using Medianet.Service.Common;
using Medianet.Service.Dto;
using Medianet.Service.FaultExceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Medianet.Service.Contracts
{
    [ServiceContract]
    public interface IChargeBeeService
    {
        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        bool IsUniqueEmail(string emailAddress);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        List<AAPContract> GetCustomerContracts(string customerId);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidParameterFault))]
        [FaultContract(typeof(NonUniqueFault))]
        [FaultContract(typeof(UpdateFault))]
        string AddChargeBeeAccount(NewAccountRequest account);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        Account GetAccountFromChargeBee(int userId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidLoginFault))]
        [FaultContract(typeof(SessionAlreadyExistsFault))]
        [FaultContract(typeof(LogonLicensesExceededFault))]
        [FaultContract(typeof(AgreeToTermsFault))]
        DBSession LoginUsingEmailAddress(string emailAddress, string password, SystemType system);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        List<IndustryCode> GetIndustryCodes();

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        SalesRegion GetSalesRegion(string regionName);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<CustomerBillingCode> GetCustomerBillingCodes(string debtorNumber, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidLoginFault))]
        [FaultContract(typeof(SessionAlreadyExistsFault))]
        [FaultContract(typeof(LogonLicensesExceededFault))]
        [FaultContract(typeof(AgreeToTermsFault))]
        DBSession Login(string username, string companyLogon, string password, SystemType system);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        RegistrationValidation GetRegistrationValidation(Guid token);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(UserMustChangePasswordFault))]
        DBSession ValidateSession(string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidLoginFault))]
        [FaultContract(typeof(SessionAlreadyExistsFault))]
        [FaultContract(typeof(LogonLicensesExceededFault))]
        [FaultContract(typeof(AgreeToTermsFault))]
        DBSession LoginPartialAuthorisation(string emailAddress, Guid expiryToken, SystemType system);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        void Logout(string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidParameterFault))]
        [FaultContract(typeof(NonUniqueFault))]
        Guid AddRegistrationValidation(string emailAddress, string firstName, SystemType system);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(SessionAlreadyExistsFault))]
        [FaultContract(typeof(LogonLicensesExceededFault))]
        [FaultContract(typeof(AgreeToTermsFault))]
        DBSession ChangePassword(string password, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidLoginFault))]
        void SendForgotPasswordEmail(string emailAddress, SystemType system);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(InvalidLoginFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(SessionAlreadyExistsFault))]
        [FaultContract(typeof(LogonLicensesExceededFault))]
        [FaultContract(typeof(AgreeToTermsFault))]
        DBSession LoginUsingSession(string sessionKey, SystemType system);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(InvalidParameterFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(NonUniqueFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(UpdateFault))]
        void UpdateChargeBeeAccount(Account account, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(SessionAlreadyExistsFault))]
        [FaultContract(typeof(LogonLicensesExceededFault))]
        [FaultContract(typeof(AgreeToTermsFault))]
        DBSession LoginReplacingSession(string sessionKey, SystemType system);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(InvalidLoginFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        DBSession LoginImpersonationUsingSession(string sessionKey, int userId, SystemType system);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidLoginFault))]
        [FaultContract(typeof(SessionAlreadyExistsFault))]
        [FaultContract(typeof(LogonLicensesExceededFault))]
        DBSession AcknowledgeTermsReadAndLogin(string emailAddress, string password, SystemType system);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        SalesForceContact GetUserProfile(int userId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        string HostedPageCheckoutOneTime(ReleaseQuoteResponse quoteResponse,string redirectURL);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        AddOn CreateEstimateInvoice(string debtorNumber, string addOnId, decimal price = 0, int quantity = 1, bool isRecordFound = false);


        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        string AcknowledgePayment(string paymentAccessCode);

    }
}
