﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Reflection;
using System.Security.Permissions;
using Medianet.Service.Dto;
using Medianet.Service.Common;
using Medianet.Service.FaultExceptions;

namespace Medianet.Service.Contracts
{
    [ServiceContract(Namespace = "http://medianet.com.au/services")]
    interface ICustomerService
    {
        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        bool IsUniqueEmail(string emailAddress);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidParameterFault))]
        [FaultContract(typeof(NonUniqueFault))]
        [FaultContract(typeof(UpdateFault))]
        string AddAccount(NewAccountRequest account);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidParameterFault))]
        [FaultContract(typeof(NonUniqueFault))]
        [FaultContract(typeof(UpdateFault))]
        string CreateCustomerForCourse(NewAccountRequest account);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        Account GetAccount(int userId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(InvalidParameterFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(NonUniqueFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(UpdateFault))]
        void UpdateAccount(Account account, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidParameterFault))]
        [FaultContract(typeof(NonUniqueFault))]
        Guid AddRegistrationValidation(string emailAddress, string firstName, SystemType system);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        RegistrationValidation GetRegistrationValidation(Guid token);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        FinanceCustomerService.MedianetCustomer GetCustomerByDebtorNumber(string debtorNumber);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidLoginFault))]
        [FaultContract(typeof(SessionAlreadyExistsFault))]
        [FaultContract(typeof(LogonLicensesExceededFault))]
        [FaultContract(typeof(AgreeToTermsFault))]
        DBSession Login(string username, string companyLogon, string password, SystemType system);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidLoginFault))]
        [FaultContract(typeof(SessionAlreadyExistsFault))]
        [FaultContract(typeof(LogonLicensesExceededFault))]
        [FaultContract(typeof(AgreeToTermsFault))]
        DBSession LoginPartialAuthorisation(string emailAddress, Guid expiryToken, SystemType system);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidLoginFault))]
        [FaultContract(typeof(SessionAlreadyExistsFault))]
        [FaultContract(typeof(LogonLicensesExceededFault))]
        [FaultContract(typeof(AgreeToTermsFault))]
        DBSession LoginUsingEmailAddress(string emailAddress, string password, SystemType system);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(InvalidLoginFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(SessionAlreadyExistsFault))]
        [FaultContract(typeof(LogonLicensesExceededFault))]
        [FaultContract(typeof(AgreeToTermsFault))]
        DBSession LoginUsingSession(string sessionKey, SystemType system);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(SessionAlreadyExistsFault))]
        [FaultContract(typeof(LogonLicensesExceededFault))]
        [FaultContract(typeof(AgreeToTermsFault))]
        DBSession LoginReplacingSession(string sessionKey, SystemType system);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(InvalidLoginFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        DBSession LoginImpersonationUsingSession(string sessionKey, int userId, SystemType system);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidLoginFault))]
        void SendForgotPasswordEmail(string emailAddress, SystemType system);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(SessionAlreadyExistsFault))]
        [FaultContract(typeof(LogonLicensesExceededFault))]
        [FaultContract(typeof(AgreeToTermsFault))]
        DBSession ChangePassword(string password, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(UserMustChangePasswordFault))]
        DBSession ValidateSession(string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        void Logout(string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<User> GetAllUsersByCustomer(string debtorNumber, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        User GetUserById(int userId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        void UpdateUser(User user, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        List<IndustryCode> GetIndustryCodes();

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<CustomerBillingCode> GetCustomerBillingCodes(string debtorNumber, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        SalesRegion GetSalesRegion(int regionId);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        void AcknowledgeUserMessageRead(int userId, SystemType system, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        void AcknowledgeTermsRead(int userId, SystemType system, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidLoginFault))]
        [FaultContract(typeof(SessionAlreadyExistsFault))]
        [FaultContract(typeof(LogonLicensesExceededFault))]
        DBSession AcknowledgeTermsReadAndLogin(string emailAddress, string password, SystemType system);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        CustomerStatistic GetCustomerStatistics(string debtorNumber, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        SecurePassword HashPassword(string password);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<AAPContract> GetCustomerContracts(string debtorNumber);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        SalesForceContact GetUserProfile(string emailAddress, string debtorNumber);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<string> GetSubjectsOfInterests();

        //[OperationContract]
        //[FaultContract(typeof(ServiceFault))]
        //[FaultContract(typeof(InvalidSessionFault))]
        //[FaultContract(typeof(AccessDeniedFault))]
        //bool UpdateduserSubjectsOfInterest(string emailAddress, string debtorNumber, List<string> subjectsOfInterest );
    }
}
