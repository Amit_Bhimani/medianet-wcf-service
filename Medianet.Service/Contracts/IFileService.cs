﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Reflection;
using System.Security.Permissions;
using System.IO;
using Medianet.Service.Dto;
using Medianet.Service.Common;
using Medianet.Service.FaultExceptions;

namespace Medianet.Service.Contracts
{
    [ServiceContract(Namespace = "http://medianet.com.au/services")]
    interface IFileService
    {
        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        TempFileUploadResponse UploadTempFile(TempFileUploadRequest fileStream);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        TempFileUploadResponse UploadTempFileUnverified(TempFileUploadRequest fileStream);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(InvalidParameterFault))]
        TempFileUploadResponse UploadTempFileForConversion(TempFileUploadRequest fileStream);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(FileNotFoundFault))]
        FileContentResponse GetTempFileContent(FileContentRequest fcRequest);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(FileNotFoundFault))]
        FileContentResponse GetTempFileContentUnverified(FileContentRequest fcRequest);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(FileNotFoundFault))]
        FileContentResponse GetTempFileDerivedContent(DerivedFileContentRequest fcRequest);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(FileNotFoundFault))]
        FileContentResponse GetInboundFileContent(FileContentRequest fcRequest);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(FileNotFoundFault))]
        FileContentResponse GetThumbnailContent(FileContentRequest fcRequest);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(InvalidParameterFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(FileNotFoundFault))]
        FileContentResponse GetDocumentContent(DocumentContentRequest fcRequest);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidParameterFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(FileNotFoundFault))]
        FileContentResponse GetWebsiteLogo(WebContentRequest fcRequest);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(InvalidParameterFault))]
        FileContentResponse UpdateTempFileDetails(TempFileUpdateRequest fileStream);
    }
}
