﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Reflection;
using System.Security.Permissions;
using Medianet.Service.Dto;
using Medianet.Service.Common;
using Medianet.Service.FaultExceptions;

namespace Medianet.Service.Contracts
{
    [ServiceContract(Namespace = "http://medianet.com.au/services")]
    interface IGeneralService
    {
        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        List<TwitterCategory> GetTwitterCategories();

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        List<WebCategory> GetWebCategories();

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        void DeleteTempFileById(int tempFileId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        void DeleteTempFileChildrenById(int tempFileId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(FileNotFoundFault))]
        [FaultContract(typeof(InvalidDataSourceFault))]
        int CountMailmergeRecipients(int tempFileId, DistributionType distType, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        InboundFile GetInboundFileById(int inboundFileId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        InboundFilePage GetInboundFilesByDebtorNumber(string debtorNumber, MessageType type, int startPos, int pageSize, InboundFileSortOrderType sortOrder, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        void DeleteInboundFileById(int inboundFileId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        void RenameInboundFileById(int inboundFileId, string name, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        MediaStatistics GetMediaStatistics();

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        List<Timezone> GetTimezones();

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        string ConvertTimeByTimeZone(string date, string timeZone);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidParameterFault))]
        void RecordEmailOpened(int releaseId, int transactionId, int? distributionId);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        Career GetCareers();

        [OperationContract]
        void AddRssLog(RssLog entity, string sessionKey);

        [OperationContract]
        [FaultContract(typeof (ServiceFault))]
        TrainingCourse GetTrainingCourse(string courseId);


        [OperationContract]
        [FaultContract(typeof(KeyNotFoundFault))]
        void UpdateMessageMediaAPIResult(Result entity);


        [OperationContract]
        [FaultContract(typeof(KeyNotFoundFault))]
        string GetEnvironmentValue(string type);
    }
}
