﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Reflection;
using System.Security.Permissions;
using Medianet.Service.Dto;
using Medianet.Service.Common;
using Medianet.Service.FaultExceptions;

namespace Medianet.Service.Contracts
{
    [ServiceContract(Namespace = "http://medianet.com.au/services")]
    interface IGeographicService
    {
        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        Continent GetContinentById(int continentId);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        List<Continent> GetContinents();

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        Country GetCountryById(int countryId);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        List<Country> GetCountries();

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        State GetStateById(int stateId);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        List<State> GetStates();

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        City GetCityById(int cityId);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        List<City> GetCities();

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        List<City> GetCitiesByAny(int? continentId, int? countryId, int? stateId, string name);
    }
}
