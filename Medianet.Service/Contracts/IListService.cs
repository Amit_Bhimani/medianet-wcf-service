﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Reflection;
using System.Security.Permissions;
using Medianet.Service.Dto;
using Medianet.Service.Common;
using Medianet.Service.FaultExceptions;

namespace Medianet.Service.Contracts
{
    [ServiceContract(Namespace = "http://medianet.com.au/services")]
    interface IListService
    {
        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        ServiceList GetServiceListById(int serviceListId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<ServiceList> GetServiceListsByDebtorNumber(string debtorNumber, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<ServiceList> GetServiceListsByDebtorNumberSystem(string debtorNumber, SystemType system, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        ServiceListSummaryPage GetServiceListsByDebtorNumberDistributionType(string debtorNumber, DistributionType distType, int startPos, int pageSize, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        ServiceListSummaryPage GetServiceListsByDebtorNumberSystemDistributionType(string debtorNumber, SystemType system, DistributionType distType, int startPos, int pageSize, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<ServiceList> GetServiceListByDebtorNumberOutletIdContactId(string debtorNumber, string outletId, string contactId, string sessionKey);
        
        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        PackageList GetPackageListById(int packageListId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        List<PackageList> GetTopValuePackageLists(string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<PackageList> GetPackageListsByDebtorNumber(string debtorNumber, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<PackageList> GetPackageListsByDebtorNumberSystem(string debtorNumber, SystemType system, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<ListCategory> GetListCategories(string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        ListComment GetServiceCommentById(int id, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        ListComment GetPackageCommentById(int id, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        List<string> GetServiceRecipientOutletNamesById(int id, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        List<ListInformation> GetServiceListInfoByIds(List<int> serviceIds, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        List<string> GetPackageRecipientOutletsById(int id, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        List<ListInformation> GetPackageListInfoByIds(List<int> packageIds, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        int GetNextServiceListNumber(DistributionType distType, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<Recipient> GetServiceListRecipients(int serviceId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        List<ListTicker> GetTicker();

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        List<TopPerformingList> GetTopPerformingLists(PeriodType period, int maxRows);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidParameterFault))]
        void UpdateAapListByAccountCode(string accountCode, string description, string distributionType);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        List<ServiceList> SearchAapListsByOutletId(string outletId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        byte[] GetPackageAttachmentFile(int packageId, string fileName);
    }
}
