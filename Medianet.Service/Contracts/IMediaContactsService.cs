﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Reflection;
using System.Security.Permissions;
using Medianet.Service.Dto;
using Medianet.Service.Common;
using Medianet.Service.FaultExceptions;
using Medianet.BusinessLayer.Common;
using Medianet.BusinessLayer.Model;

namespace Medianet.Service.Contracts
{
    [ServiceContract(Namespace = "http://medianet.com.au/services")]
    interface IMediaContactsService
    {
        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        MediaContact GetMediaContact(string contactId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(SubjectPrimaryContactRequiredFault))]
        [FaultContract(typeof(MediaUpdateValidationFault))]
        string AddMediaContact(MediaContact entity, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(SubjectPrimaryContactRequiredFault))]
        [FaultContract(typeof(MediaUpdateValidationFault))]
        [FaultContract(typeof(DataConcurrencyFault))]
        void UpdateMediaContact(MediaContact entity, string sessionKey);


        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(SubjectPrimaryContactRequiredFault))]
        [FaultContract(typeof(MediaUpdateValidationFault))]
        int AddMediaDatabaseLog(MediaDatabaseLog entity, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(SubjectPrimaryContactRequiredFault))]
        [FaultContract(typeof(MediaUpdateValidationFault))]
        void UpdateMediaDatabaseLog(MediaDatabaseLog entity, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<MediaDatabaseLog> GetMediaDatabaseLogs(string outletId,string contactId,string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        MediaDatabaseLog GetMediaDatabaseLogById(int id, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<MediaRecentlyViewedDto> GetMediaRecentlyViewed(string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<MediaContactSearchLog> GetRecentSearchList(int maxRecords, string sessionKey);


        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        MediaContactSearchLog GetRecentSearchById(int searchLogId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        MediaOutlet GetMediaOutlet(string outletId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<MediaOutlet> GetMediaOutletsByName(string partialName, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(SubjectPrimaryContactRequiredFault))]
        [FaultContract(typeof(MediaUpdateValidationFault))]
        string AddMediaOutlet(MediaOutlet entity, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(SubjectPrimaryContactRequiredFault))]
        [FaultContract(typeof(MediaUpdateValidationFault))]
        [FaultContract(typeof(DataConcurrencyFault))]
        void UpdateMediaOutlet(MediaOutlet entity, List<MediaEmployeeSubjectValidation> validation, string sessionKey);





        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        MediaEmployee GetMediaEmployee(string contactId, string outletId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<MediaEmployee> GetMediaEmployeesByContactId(string contactId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<MediaEmployee> GetMediaEmployeesByOutletId(string outletId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(SubjectPrimaryContactRequiredFault))]
        [FaultContract(typeof(MediaUpdateValidationFault))]
        void AddMediaEmployee(MediaEmployee entity, MediaEmployeeValidation validation, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(SubjectPrimaryContactRequiredFault))]
        [FaultContract(typeof(MediaUpdateValidationFault))]
        [FaultContract(typeof(DataConcurrencyFault))]
        void UpdateMediaEmployee(MediaEmployee entity, MediaEmployeeValidation validation, string sessionKey);

        /*[OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(MediaUpdateValidationFault))]
        [FaultContract(typeof(DataConcurrencyFault))]
        void SetOutletPrimaryContact(string outletId, string contactId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(MediaUpdateValidationFault))]
        [FaultContract(typeof(DataConcurrencyFault))]
        void SetSubjectPrimaryContact(string outletId, string contactId, int subjectId, string sessionKey);
        */


        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        ContactRole GetContactRole(int roleId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<ContactRole> GetContactRoles(string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        NewsFocus GetNewsFocus(int newsFocusId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<NewsFocus> GetNewsFocuses(string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        OutletFrequency GetOutletFrequency(int frequencyId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<OutletFrequency> GetOutletFrequencies(string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        OutletType GetOutletType(int outletTypeId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<OutletType> GetOutletTypes(string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        ProductType GetProductType(int productTypeId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<ProductType> GetProductTypes(string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<ProductType> GetProductTypesByOutletType(int outletTypeId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        StationFormat GetStationFormat(int stationFormatId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<StationFormat> GetStationFormats(string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        Subject GetSubject(int subjectId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<Subject> GetSubjects(string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        SubjectGroup GetSubjectGroup(int subjectGroupId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<SubjectGroup> GetSubjectGroups(string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        WorkingLanguage GetWorkingLanguage(int languageId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<WorkingLanguage> GetWorkingLanguages(string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        MediaContactsExport GetExport(int exportId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<MediaContactsExport> GetExports(string debtorNumber, int? userId, MediaContactsExportType type, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        int AddExports(MediaContactsExport export, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        void UpdateExports(MediaContactsExport export, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        void DeleteExports(int exportId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        int AddMediaContactGroup(MediaContactGroup entity, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<MediaContactGroup> GetMediaContactGroups(int userId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof (ServiceFault))]
        [FaultContract(typeof (InvalidSessionFault))]
        [FaultContract(typeof (AccessDeniedFault))]
        List<MediaContactGroup> GetMediaContactGroupsByType(MediaContactGroupType type, int userId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<MediaContactSavedSearch> GetMediaContactSavedSearches(int userId, int recentCount, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        MediaContactSavedSearch GetMediaContactSavedSearch(int searchId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<MediaContactList> GetMediaContactLists(int userId, int recentCount, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        MediaContactList GetMediaContactList(int listId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<MediaContactTask> GetMediaContactTasks(int userId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        MediaContactTask GetMediaContactTask(int taskId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<MediaContactListVersion> GetMediaContactListVersions(int id, string sessionKey);

        [OperationContract]
        [FaultContract(typeof (ServiceFault))]
        [FaultContract(typeof (InvalidSessionFault))]
        [FaultContract(typeof (AccessDeniedFault))]
        void RestoreMediaContactListVersion(int listId, int listSetId, int userId,string sessionKey);

        [OperationContract]
        [FaultContract(typeof (ServiceFault))]
        [FaultContract(typeof (InvalidSessionFault))]
        [FaultContract(typeof (AccessDeniedFault))]
        List<MediaContactLivingListContactReplacement> GetMediaContactLivingListContactReplacement(string sessionKey, int userId = 0, int listSetId = 0,int maxRows = 6);

        [OperationContract]
        [FaultContract(typeof (ServiceFault))]
        [FaultContract(typeof (InvalidSessionFault))]
        [FaultContract(typeof (AccessDeniedFault))]
        void LivingListAccept(int recordId,string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        void LivingListReject(int recordId,string sessionKey);

        [OperationContract]
        [FaultContract(typeof (ServiceFault))]
        [FaultContract(typeof (InvalidSessionFault))]
        [FaultContract(typeof (AccessDeniedFault))]
        Dictionary<string, string> GetLivingListOutletContactsByRecordId(int recordId, int maxRow,string sessionKey);

        [OperationContract]
        [FaultContract(typeof (ServiceFault))]
        [FaultContract(typeof (InvalidSessionFault))]
        [FaultContract(typeof (AccessDeniedFault))]
        void LivingListReplaceContact(int recordId, string newContactId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        MediaContactPaginatedListRecordDetails GetPaginatedRecordsByListsetAndListId(int listId, int listSet, int pageNumber, int recordsPerPage, SortColumn pSortColumn, SortDirection pSortDirection, int userId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        void DeleteFromList(int listId, List<int> recordIds, string sessionKey);
        
        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        void AddToList(int listId, int userId, List<string> recordIds, bool createNewVersion, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        int MediaContactSearchLog(MediaContactSearchLog entity, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        int MediaContactExportLog(MediaContactExportLog entity, string sessionKey);
		
        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<MediaOutletPartial> GetMediaOutletsByIds(string[] outletIds, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<MediaOutletPartial> GetRelatedMediaOutlets(string outletId, string sessionKey);


        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<MediaOutletPartial> GetMediaOwnerBusinesses(string outletId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<MediaOutletPartial> GetSyndicatingToMediaOutlets(string outletId, string sessionKey);
        
        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<MediaOutletPartial> GetSyndicatedFromMediaOutlets(string outletId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<MediaMovement> GetMediaMovements(bool includeHidden, int pageNumber, int recordsPerPage, string sessionKey, out int totalCount, out int totalPinned);

        [OperationContract]
        [FaultContract(typeof (ServiceFault))]
        [FaultContract(typeof (InvalidSessionFault))]
        [FaultContract(typeof (AccessDeniedFault))]
        List<MediaMovement> GetOutletMediaMovements(string outletId, int pageNumber, int recordsPerPage,
            string sessionKey);

        [OperationContract]
        [FaultContract(typeof (ServiceFault))]
        [FaultContract(typeof (InvalidSessionFault))]
        [FaultContract(typeof (AccessDeniedFault))]
        List<MediaMovement> GetContactMediaMovements(string contactId, int pageNumber, int recordsPerPage,
            string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        MediaMovement GetMediaMovement(int id, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        bool AddMediaMovement(MediaMovement newMovement, string sessionKey);

        [OperationContract]
        [FaultContract(typeof (ServiceFault))]
        [FaultContract(typeof (InvalidSessionFault))]
        [FaultContract(typeof (AccessDeniedFault))]
        bool UpdateMediaMovement(MediaMovement updatedMediaMovement, string sessionKey);

        [OperationContract]
        [FaultContract(typeof (ServiceFault))]
        [FaultContract(typeof (InvalidSessionFault))]
        [FaultContract(typeof (AccessDeniedFault))]
        bool DeleteMediaMovement(int entityId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        void PinUnpinMediaMovement(int id, bool pinned, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<MediaPriorityNotice> GetMediaPriorityNotices(int pageNumber, int recordsPerPage, string sessionKey, out int totalCount);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        MediaPriorityNotice GetMediaPriorityNotice(int? id, string contactId, string outletId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        bool AddMediaPriorityNotice(MediaPriorityNotice entity, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        bool UpdateMediaPriorityNotice(MediaPriorityNotice entity, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        bool DeleteMediaPriorityNotice(int entityId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<Note> GetOutletNotes(string outletId, string recordType, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<Note> GetContactNotes(string outletId, string contactId, string recordType, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        void DeleteNote(int id,string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        Note GetNote(int id, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        void PinUnpinNote(int id, bool pinned, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        int AddNote(Note note, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        void UpdateNote(Note note, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        void AddNoteBulk(NotesBulk notes, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(FileNotFoundFault))]
        LogoUploadResponse UploadOutletLogo(LogoUploadRequest fileStream);

        [OperationContract]
        [FaultContract(typeof (ServiceFault))]
        [FaultContract(typeof (InvalidSessionFault))]
        [FaultContract(typeof (AccessDeniedFault))]
        LogoFileResponse GetOutletLogo(LogoFileRequest fileRequest);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        void LogMediaContactView(MediaContactViewDto mediaContactsView, string sessionKey);

        [OperationContract]
        [FaultContract(typeof (ServiceFault))]
        [FaultContract(typeof (InvalidSessionFault))]
        [FaultContract(typeof (AccessDeniedFault))]
        void LogPrnContactView(PrnContactViewDto entity, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<MediaContactAlsoViewedDto> GetContactViewSummary(string contactId, string outletId, List<int> subjectCodeIds, List<int> continentIds, int maxCount, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        void LogMediaOutletView(MediaOutletViewDto entity, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<OmaDocument> GetDocuments(string outletId, string contactId, int recordType, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        void SaveOmaDocument(OmaDocument entity, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        void DeleteOmaDocument(int documentId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        OmaDocument GetDocumentById(int documentId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        OmaDocument GetDocumentFile(int documentId, string sessionKey);
        

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<int> GetOmaContactIds(int outletId, string sessionKey);
		
 		[OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        List<ServiceDeletedUserLog> GetServiceDeletedUserLog(string outletId, string contactId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        MediaEmployeeView GetMediaEmployeeDetails(string contactId, string outletId, int userId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<MediaOutlet> GetEmployeesOutletsInfo(string contactId, string sessionKey);


        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(SubjectPrimaryContactRequiredFault))]
        [FaultContract(typeof(MediaUpdateValidationFault))]
        [FaultContract(typeof(DataConcurrencyFault))]
        void ApproveMediaContactDraft(int draftId, MediaDraftQueue draftQueue, MediaContact contact, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(SubjectPrimaryContactRequiredFault))]
        [FaultContract(typeof(MediaUpdateValidationFault))]
        [FaultContract(typeof(DataConcurrencyFault))]
        void ApproveMediaEmployeeDraft(int draftId, MediaDraftQueue draftQueue, MediaEmployee Employee, MediaEmployeeValidation validation, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        int? GetUserIdByContactId(string contactId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<MediaContactJobHistory> GetMediaContactJobHistories(string contactId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        MediaContactJobHistory GetMediaContactJobHistory(string contactId, int jobHistoryId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        int AddMediaContactJobHistory(MediaContactJobHistory entity, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        void UpdateMediaContactJobHistory(MediaContactJobHistory entity, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        void UpdateMediaContactJobHistories(List<MediaContactJobHistory> entities, string contactId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        void DeleteMediaContactJobHistory(int jobHistoryId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<MediaContactRecentChange> GetMediaContactRecentlyAdded(int total, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<MediaContactRecentChange> GetMediaContactRecentlyUpdated(int total, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        void LivingListAcceptRejectAll(int listId, string sessionKey);
    }
}
