﻿using Medianet.Service.Common;
using Medianet.Service.Dto;
using Medianet.Service.FaultExceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;

namespace Medianet.Service.Contracts
{
    [ServiceContract(Namespace = "http://medianet.com.au/services")]
    interface IMediaJournalistsService
    {
        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<MnjProfile> GetMNJUserProfiles(int userId);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        MnjProfile GetMNJUserProfile(int profileId, int userId);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        List<MnjProfile> GetProfilesByUnsubscribeKey(Guid unsubscribeKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(DuplicateNameFault))]
        int AddProfile(MnjProfile entity);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(DuplicateNameFault))]
        void EditProfile(MnjProfile entity);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        void DeleteProfile(int profileId, int userId);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        void SaveReleases(int userId, string releaseIds);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        void DeleteSavedReleases(int userId, List<int> releaseIds);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(DataConcurrencyFault))]
        void SaveMediaContactAndEmployeeDraft(MediaContactDraft contactDraft, MediaEmployeeDraft employeeDraft, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        JournalistEmployeeInfo GetJournalistEmployeeInfo(string contactId, string outletId, string sessionKey);
        
        [OperationContract(Name = "GetMediaContactDraft")]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        MediaContactDraft GetMediaContactDraft(int queueId, string sessionKey);
        
        [OperationContract(Name = "GetMediaContactDraftByContactId")]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        MediaContactDraft GetMediaContactDraft(string contactId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(DataConcurrencyFault))]
        void UpdateMediaContactDraft(MediaContactDraft entity, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(DataConcurrencyFault))]
        void DeclineMediaContactDraft(int id, MediaDraftQueue draftQueue, string sessionKey);

        [OperationContract(Name = "GetMediaEmployeeDraft")]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        MediaEmployeeDraft GetMediaEmployeeDraft(int queueId, string sessionKey);

        [OperationContract(Name = "GetMediaEmployeeDraftByOutletId")]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        MediaEmployeeDraft GetMediaEmployeeDraft(string contactId, string outletId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(DataConcurrencyFault))]
        void UpdateMediaEmployeeDraft(MediaEmployeeDraft entity, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(DataConcurrencyFault))]
        void DeclineMediaEmployeeDraft(int id, MediaDraftQueue draftQueue, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        List<MediaDraftQueueView> GetAllDraftsInQueue(DateTime? startDate, DateTime? endDate, string searchText, int pageNumber,
            int recordsPerPage, SortColumn pSortColumn, SortDirection pSortDirection, int userId, string sessionKey, out int totalCount);

		[OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        void UpdateSubscription(List<MnjProfile> profiles, Guid unsubscribeKey);
        
        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        int GetPendingDraftsInQueueCount(string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        void CreateMNJLogin(string contactId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        void ResendWelcomeMNJEmail(int userId, string contactId, string sessionKey);
    }
}
