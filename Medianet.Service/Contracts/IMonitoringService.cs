﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Reflection;
using System.Security.Permissions;
using Medianet.Service.Dto;
using Medianet.Service.Common;
using Medianet.Service.FaultExceptions;

namespace Medianet.Service.Contracts
{
    [ServiceContract(Namespace = "http://medianet.com.au/services")]
    interface IMonitoringService
    {
        [OperationContract]
        List<MonitoringCampaign> GetCampaigns(int userId, string debtorNumber);

        [OperationContract]
        [FaultContract(typeof(KeyNotFoundFault))]
        MonitoringCampaign GetCampaign(int campaignId);

        [OperationContract]
        [FaultContract(typeof(DuplicateNameFault))]
        int AddCampaign(MonitoringCampaign campaign, string sessionKey);

        [OperationContract]
        bool UpdateCampaign(MonitoringCampaign campaign);

        [OperationContract]
        [FaultContract(typeof(KeyNotFoundFault))]
        bool DeleteCampaign(int campaignId);

        [OperationContract]
        [FaultContract(typeof(UpdateFault))]
        int AddSearch(MonitoringSearch search, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(KeyNotFoundFault))]
        MonitoringSearch GetSearch(int searchId);

        [OperationContract]
        int AddEmail(MonitoringEmail email, string sessionKey);

        [OperationContract]
        List<MonitoringEmail> GetEmailsByUser(int userId);

        [OperationContract]
        List<MonitoringEmail> GetEmailsByCustomer(string debtorNumber);

        [OperationContract]
        bool IsCampaignNameUnique(MonitoringCampaign campaign);

        [OperationContract]
        int AddAlert(MonitoringAlert alert);

        [OperationContract]
        bool UpdateAlert(MonitoringAlert alert);

        [OperationContract]
        bool DeleteAlert(int alertId);

        [OperationContract]
        List<MonitoringCampaign> GetCampaignsByAlertFrequency(int frequencyId);

        [OperationContract]
        List<MonitoringCampaign> GetAllCampaigns();
    }
}
