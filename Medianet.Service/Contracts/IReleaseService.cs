﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Reflection;
using System.Security.Permissions;
using Medianet.Service.Dto;
using Medianet.Service.Common;
using Medianet.Service.FaultExceptions;

namespace Medianet.Service.Contracts
{
    [ServiceContract(Namespace = "http://medianet.com.au/services")]
    interface IReleaseService
    {
        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        int AddReleaseFromSummary(ReleaseSummary releaseSummary, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        Release GetReleaseById(int releaseId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        string GetReleaseURL(int id);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        ReleasePublic GetPublicReleaseById(int releaseId, WebsiteType website, string clientIp, string userAgent, string securityKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        ReleasePage GetReleasesWithResultsToShow(int startPos, int pageSize, ReleaseSortOrderType sortOrder, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        List<ReleaseSummaryPublic> GetPublicFeatureReleases(int? webCategoryId);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        ReleasePagePublic GetPublicReleases(ReleasePagePublicRequest request);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        List<Transaction> GetReleaseTransactions(int releaseId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        List<TransactionStatistics> GetReleaseTransactionStatistics(int releaseId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        List<Document> GetReleaseDocuments(int releaseId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        List<Result> GetReleaseResults(int releaseId, int transactionId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(KeyNotFoundFault))]
        void EmailPublicReleaseById(int releaseId, WebsiteType website, string emailAddress, string securityKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(FileNotFoundFault))]
        [FaultContract(typeof(InvalidParameterFault))]
        PaymentStartResponse InitialisePayment(PaymentStartRequest payment, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        PaymentEndResponse FinalisePayment(string paymentAccessCode, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(FileNotFoundFault))]
        ReleaseQuoteResponse QuoteRelease(ReleaseQuoteRequest releaseQuote, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        ReleaseQuoteResponse CalculateRelease(ReleaseCalculatorRequest releaseCalc, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(FileNotFoundFault))]
        ReleaseQuoteResponse CalculatePrice(Release release, string billerDebtorNumber);


        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidParameterFault))]
        PaymentStartResponse InitialiseCoursePayment(CoursePaymentStartRequest payment);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        PaymentEndResponse FinaliseCoursePayment(string paymentAccessCode, string debtorNumber);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidParameterFault))]
        void RegisterForCourse(CourseRegistrationRequest account);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        [FaultContract(typeof(FileNotFoundFault))]
        bool CancelRelease(int releaseId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        LinqToTwitter.Status PushToTwitter(CasTwitterCategory twitterCat, string tweet, string shortUrl);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        LinqToTwitter.Status Retweet(CasTwitterCategory twitterCat, LinqToTwitter.Status tweet);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        string GetShortUrlFromSocialMediaService(string fullUrl);
  		[OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        ReleasePagePublic GetWebReleaseByProfileId(int profileId, int itemsPerPage, int currentPage);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(AccessDeniedFault))]
        ReleasePagePublic GetSavedReleases(int userId, int itemsPerPage, int currentPage);

    }
}
