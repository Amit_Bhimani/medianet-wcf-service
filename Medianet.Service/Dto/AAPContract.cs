﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using ComLib.Extensions;
using Medianet.Service.FaultExceptions;


namespace Medianet.Service.Dto
{

    [DataContract]
    public class AAPContract
    {

        [DataMember]
        public DateTime? ContractStartDate { get; set; }
        [DataMember]
        public DateTime? ContractEndDate { get; set; }
        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public decimal? Price { get; set; }

        [DataMember]
        public int? ContractTerm { get; set; }
        [DataMember]
        public string Product { get; set; }

    }
}
