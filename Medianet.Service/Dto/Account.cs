﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class Account
    {
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public string DebtorNumber { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string CompanyName { get; set; }
        [DataMember]
        public string IndustryCode { get; set; }
        [DataMember]
        public string ContactPosition { get; set; }
        [DataMember]
        public AccountAddress CompanyAddress { get; set; }
        [DataMember]
        public AccountAddress BillingAddress { get; set; }
        [DataMember]
        public string TimezoneCode { get; set; }
        [DataMember]
        public Timezone Timezone { get; set; }
        [DataMember]
        public string EmailAddress { get; set; }
        [DataMember]
        public string TelephoneNumber { get; set; }
        [DataMember]
        public string FaxNumber { get; set; }
        [DataMember]
        public string ABN { get; set; }

        [DataMember]
        public string UserLogonName { get; set; }
        [DataMember]
        public string CompanyLogonName { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public CustomerBillingType AccountType { get; set; }
        [DataMember]
        public string SalesRegionName { get; set; }
        [DataMember]
        public string RelevantSubjects { get; set; }
        [DataMember]
        public bool IsForUpgrade { get; set; }
    }
}
