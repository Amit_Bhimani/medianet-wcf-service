﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class Address
    {
        [DataMember]
        public string AddressLine1 { get; set; }
        [DataMember]
        public string AddressLine2 { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string PostCode { get; set; }
        [DataMember]
        public string State { get; set; }
        [DataMember]
        public string Country { get; set; }
        
        public override string ToString()
        {
            return (AddressLine1 != null ? AddressLine1 : "") +
                (AddressLine2 != null ? ", " + AddressLine2 : "") +
                (City != null ? ", " + ", " + City : "") +
                (PostCode != null ? ", " + PostCode : "") +
                (State != null ? ", " + State : "") + (Country != null ? ", " + Country : "");
        }
    }
}
