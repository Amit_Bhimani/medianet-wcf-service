﻿namespace Medianet.Service.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class Career
    {
        public string Title { get; set; }
        public string Department { get; set; }
        public string Location { get; set; }

        public string Description { get; set; }
    }
}
