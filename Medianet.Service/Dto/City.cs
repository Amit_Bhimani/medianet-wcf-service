﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class City
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string PostCode { get; set; }
        [DataMember]
        public int? StateId { get; set; }
        [DataMember]
        public int CountryId { get; set; }
        [DataMember]
        public int ContinentId { get; set; }
        [DataMember]
        public string StateName { get; set; }
        [DataMember]
        public string CountryName { get; set; }
        [DataMember]
        public int? AapCityId { get; set; }
    }
}
