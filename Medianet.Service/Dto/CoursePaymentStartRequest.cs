﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class CoursePaymentStartRequest
    {
        [DataMember]
        public string DebtorNumber { get; set; }
        [DataMember]
        public int ScheduleId { get; set; }
        [DataMember]
        public string PromoCode { get; set; }
        [DataMember]
        public int Seats { get; set; }
        [DataMember]
        public string IPAddress { get; set; }
        [DataMember]
        public string RedirectURL { get; set; }
    }
}
