﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class CourseRegistrationRequest
    {
        [DataMember]
        public string DebtorNumber { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string CompanyName { get; set; }
        [DataMember]
        public string ABN { get; set; }
        [DataMember]
        public string ContactPosition { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string Suburb { get; set; }
        [DataMember]
        public string PostCode { get; set; }
        [DataMember]
        public string State { get; set; }
        [DataMember]
        public string Country { get; set; }
        [DataMember]
        public string EmailAddress { get; set; }
        [DataMember]
        public string TelephoneNumber { get; set; }
        [DataMember]
        public string ExperienceLevel { get; set; }
        [DataMember]
        public string DesiredOutcome { get; set; }
        [DataMember]
        public string TopicsPlanned { get; set; }
        [DataMember]
        public string ReceiptId { get; set; }
        [DataMember]
        public int ScheduleId { get; set; }
        [DataMember]
        public double TotalCost { get; set; }
        [DataMember]
        public bool IsInvoice { get; set; }
    }
}
