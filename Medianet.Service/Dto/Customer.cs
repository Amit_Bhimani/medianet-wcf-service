﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class Customer
    {
        public Customer()
        {
            DebtorNumber = string.Empty;
            Name = string.Empty;
            LogonName = string.Empty;
            FilerCode = string.Empty;
            PIN = string.Empty;
            SalesEmailAddress = string.Empty;
            DefaultOriginatorEmailAddress = string.Empty;
            ReportSendMethod = ReportType.Email;
            ReportResultsToSend = ResultsType.All;
            ShouldSendResultsData = false;
            OptOutMethod = OptoutType.Manual;
            ShouldSendOptOutForFax = false;
            ShouldSendOptOutForEmail = false;
            TimezoneCode = string.Empty;
            AccountType = CustomerBillingType.Creditcard;
            MustUseCreditcard = false;
            HasTransactions = false;
            MediaDirectorySystem = MediaDirectoryType.None;
            Comment = string.Empty;
            CanExportContacts = true;
            HasViewedTermsAndConditionsP = false;
            HasViewedTermsAndConditionsC = false;
            HasViewedTermsAndConditionsM = false;
            RowStatus = RowStatusType.Active;
            DefaultSearchCountry = 0;
        }

        [DataMember]
        public string DebtorNumber { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string LogonName { get; set; }
        [DataMember]
        public string FilerCode { get; set; }
        [DataMember]
        public string PIN { get; set; }
        [DataMember]
        public string SalesEmailAddress { get; set; }
        [DataMember]
        public int? DefaultCoverpageId { get; set; }
        [DataMember]
        public string DefaultOriginatorEmailAddress { get; set; }
        [DataMember]
        public ReportType ReportSendMethod { get; set; }
        [DataMember]
        public ResultsType ReportResultsToSend { get; set; }
        [DataMember]
        public bool ShouldSendResultsData { get; set; }
        [DataMember]
        public OptoutType OptOutMethod { get; set; }
        [DataMember]
        public bool ShouldSendOptOutForFax { get; set; }
        [DataMember]
        public bool ShouldSendOptOutForEmail { get; set; }
        [DataMember]
        public bool ShouldSendOptOutForSMS { get; set; }
        [DataMember]
        public string TimezoneCode { get; set; }
        [DataMember]
        public Timezone Timezone { get; set; }

        [DataMember]
        public CustomerBillingType AccountType { get; set; }
        [DataMember]
        public bool MustUseCreditcard { get; set; }
        [DataMember]
        public bool UseMedianetRewrite { get; set; }
        [DataMember]
        public bool HasTransactions { get; set; }
        [DataMember]
        public bool IsVerified { get; set; }
        [DataMember]
        public DateTime? TrendWatcherExpiryDate { get; set; }
        [DataMember]
        public bool CanExportContacts { get; set; }

        [DataMember]
        public MediaDirectoryType MediaDirectorySystem { get; set; }
        [DataMember]
        public int MediaContactsLicenseCount { get; set; }

        [DataMember]
        public string Comment { get; set; }
        [DataMember]
        public bool HasViewedTermsAndConditionsP { get; set; }
        [DataMember]
        public bool HasViewedTermsAndConditionsC { get; set; }
        [DataMember]
        public bool HasViewedTermsAndConditionsM { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int CreatedByUserId { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public int LastModifiedByUserId { get; set; }
        [DataMember]
        public RowStatusType RowStatus { get; set; }
        [DataMember]
        public List<CustomerRegion> SalesRegions { get; set; }
        [DataMember]
        public int DefaultSearchCountry { get; set; }
        [DataMember]
        public string SalesRegionName { get; set; }
    }
}
