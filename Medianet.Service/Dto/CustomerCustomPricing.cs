﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class CustomerCustomPricing
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember] 
        public string DebtorNumber { get; set; }
        [DataMember] 
        public int BrsContractId { get; set; }
        [DataMember] 
        public DateTime? StartDate { get; set; }
        [DataMember] 
        public DateTime? EndDate { get; set; }
        [DataMember] 
        public string Comment { get; set; }
        [DataMember]
        public int BrsServiceId { get; set; }
        [DataMember]
        public int BrsServiceAddressId { get; set; }
        [DataMember]
        public string ProductCode { get; set; }
        [DataMember]
        public string ProductName { get; set; }
        [DataMember]
        public string TransactionType { get; set; }
        [DataMember]
        public decimal CostPerUnit { get; set; }
        [DataMember]
        public int CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int LastModifiedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
    }
}
