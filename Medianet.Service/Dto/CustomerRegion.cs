﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class CustomerRegion
    {
        [DataMember]
        public string DebtorNumber { get; set; }
        [DataMember]
        public int RegionId { get; set; }
    }
}
