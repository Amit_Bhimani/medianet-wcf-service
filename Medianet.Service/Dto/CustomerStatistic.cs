﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class CustomerStatistic
    {
        [DataMember]
        public string DebtorNumber { get; set; }
        [DataMember]
        public int LastMonthTotal { get; set; }
        [DataMember]
        public int PreviousMonthTotal { get; set; }
        [DataMember]
        public decimal AveragePerMonth { get; set; }
        [DataMember]
        public decimal AveragePerMonthAll { get; set; }
        [DataMember]
        public decimal PercentageUpSinceLastMonth { get; set; }
        [DataMember]
        public decimal PercentageUpVsOthers { get; set; }
        [DataMember]
        public string BusiestDayOfWeek { get; set; }
        [DataMember]
        public int? DaysSinceLastSent { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
    }
}
