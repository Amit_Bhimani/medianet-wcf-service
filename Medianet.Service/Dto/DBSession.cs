﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class DBSession
    {
        [DataMember]
        public string Key { get; set; }
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public string DebtorNumber { get; set; }
        [DataMember]
        public SystemType System { get; set; }
        [DataMember]
        public RowStatusType RowStatus { get; set; }
        [DataMember]
        public DateTime LoginDate { get; set; }
        [DataMember]
        public DateTime LastAccessedDate { get; set; }
        [DataMember]
        public bool IsImpersonating { get; set; }
        [DataMember]
        public bool IsAuthorised { get; set; }

        [DataMember]
        public Customer Customer { get; set; }
        [DataMember]
        public User User { get; set; }
    }
}
