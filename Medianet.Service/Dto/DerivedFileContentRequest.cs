﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Medianet.Service.Dto
{
    [MessageContract]
    public class DerivedFileContentRequest
    {
        [MessageHeader(MustUnderstand = true)]
        public string SessionKey { get; set; }

        [MessageHeader(MustUnderstand = true)]
        public int FileId { get; set; }
        
        [MessageHeader(MustUnderstand = true)]
        public bool WireConversion { get; set; }
    }
}
