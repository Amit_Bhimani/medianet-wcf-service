﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;
using Medianet.Service.BusinessLayer.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class Document
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ReleaseId { get; set; }
        [DataMember]
        public int SequenceNumber { get; set; }
        [DataMember]
        public string FileExtension { get; set; }
        [DataMember]
        public bool IsDerived { get; set; }
        [DataMember]
        public string OriginalFilename { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public bool CanDistributeViaFax { get; set; }
        [DataMember]
        public bool CanDistributeViaEmailBody { get; set; }
        [DataMember]
        public bool CanDistributeViaEmailAttachment { get; set; }
        [DataMember]
        public bool CanDistributeViaWire { get; set; }
        [DataMember]
        public bool CanDistributeViaVoice { get; set; }
        [DataMember]
        public bool CanDistributeViaWeb { get; set; }
        [DataMember]
        public bool CanDistributeViaSMS { get; set; }
        [DataMember]
        public bool IsMailMergeDataSource { get; set; }
        [DataMember]
        public int ItemCount { get; set; }
        [DataMember]
        public int Width { get; set; }
        [DataMember]
        public int Height { get; set; }
        [DataMember]
        public bool IsTabulated { get; set; }
        [DataMember]
        public string MediaReferenceId { get; set; }
        [DataMember]
        public string ContentType { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int CreatedByUserId { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public int LastModifiedByUserId { get; set; }

        // Used when attaching an inbound wav or temp file to a job.
        public int? InboundFileId { get; set; }
        public int? TempFileId { get; set; }
        public string SavedFile { get; set; }

        public Document() {
            FileExtension = string.Empty;
            OriginalFilename = string.Empty;
            Description = string.Empty;
            ContentType = string.Empty;
        }

        #region Helper Methods

        public bool IsReleaseFile() {
            return !IsDerived && CanDistributeViaWeb;
        }

        public bool IsAudioWireFile() {
            return !IsDerived && !CanDistributeViaWeb;
        }

        public bool IsEmailAttachmentFile() {
            return !IsDerived && CanDistributeViaEmailAttachment && !IsReleaseFile();
        }

        public bool IsMailmergeDataFile() {
           return !IsDerived & (IsMailMergeDataSource);
        }

        public bool IsImageFile()
        {
            return !IsDerived && ContentType.IndexOf("image", StringComparison.CurrentCultureIgnoreCase) >= 0;
        }

        #endregion

        #region "Factory Methods"

        public static Document CreateDerivedFromTempFile(TempFile tFile, int sequenceNo, int userId) {
            Document doc = CreateFromTempFile(tFile, sequenceNo, userId);

            doc.IsDerived = true;
            doc.Description = string.Empty;
            doc.CanDistributeViaEmailBody = false;
            doc.CanDistributeViaEmailAttachment = false;
            doc.CanDistributeViaWire = tFile.IsWireConversion;
            doc.CanDistributeViaWeb = tFile.IsWebConversion;
            doc.IsMailMergeDataSource = tFile.IsMailMergeDataSource;
            return doc;
        }

        public static Document CreateFromTempFileAndSummary(TempFile tFile, DocumentSummary doc, int userId) {
            Document d = CreateFromTempFile(tFile, doc.SequenceNumber, userId);

            d.IsDerived = false;
            d.CanDistributeViaFax = doc.CanDistributeViaWeb;
            d.CanDistributeViaEmailBody = doc.CanDistributeViaEmailBody;
            d.CanDistributeViaEmailAttachment = doc.CanDistributeViaEmailAttachment;
            d.CanDistributeViaWire = doc.CanDistributeViaWire;
            d.CanDistributeViaVoice = false;
            d.CanDistributeViaWeb = doc.CanDistributeViaWeb;
            d.CanDistributeViaSMS = doc.CanDistributeViaSMS;
            d.IsMailMergeDataSource = doc.IsMailMergeDataSource;
            d.ItemCount = 1;

            return d;
        }

        public static Document CreateFromInboundFileAndSummary(InboundFile iFile, DocumentSummary doc, int userId) {
            Document d = CreateFromInboundFile(iFile, doc.SequenceNumber, userId);

            d.IsDerived = false;
            d.CanDistributeViaFax = false;
            d.CanDistributeViaEmailBody = false;
            d.CanDistributeViaEmailAttachment = true;
            d.CanDistributeViaWire = false;
            d.CanDistributeViaVoice = false;
            d.CanDistributeViaWeb = false;
            d.CanDistributeViaSMS = false;
            d.IsMailMergeDataSource = false;
            d.ItemCount = 1;

            return d;
        }

        private static Document CreateFromTempFile(TempFile tFile, int sequenceNo, int userId) {
            Document doc = new Document();

            doc.SequenceNumber = sequenceNo;
            doc.FileExtension = tFile.FileExtension.Truncate(12);
            doc.OriginalFilename = tFile.OriginalFilename.Truncate(256);
            doc.ItemCount = tFile.ItemCount;
            doc.ContentType = tFile.ContentType.Truncate(150);
            doc.CreatedDate = DateTime.Now;
            doc.CreatedByUserId = userId;
            doc.LastModifiedDate = doc.CreatedDate;
            doc.LastModifiedByUserId = doc.CreatedByUserId;
            doc.TempFileId = tFile.Id;
            doc.SavedFile = PathHelper.GetTempFilePathAndName(tFile.Id, tFile.FileExtension);

            return doc;
        }

        private static Document CreateFromInboundFile(InboundFile iFile, int sequenceNo, int userId) {
            Document doc = new Document();

            doc.SequenceNumber = sequenceNo;
            doc.FileExtension = iFile.FileExtension.Truncate(12);
            doc.OriginalFilename = iFile.OriginalFilename.Truncate(256);
            doc.ItemCount = iFile.ItemCount;
            doc.ContentType = MimeExtensionHelper.GetMimeType(iFile.OriginalFilename).Truncate(150);
            doc.CreatedDate = DateTime.Now;
            doc.CreatedByUserId = userId;
            doc.LastModifiedDate = doc.CreatedDate;
            doc.LastModifiedByUserId = doc.CreatedByUserId;
            doc.InboundFileId = iFile.Id;
            doc.SavedFile = PathHelper.GetInboundFilePathAndName(iFile.Id, iFile.FileExtension);

            return doc;
        }

        #endregion

/*#region "Shared methods"

        public static bool ValidReleaseFileExtension(string pExt) {
            string ext = pExt.ToLower();

            return ext.Equals(Constants.FILETYPE_DOC) ||
                   ext.Equals(Constants.FILETYPE_DOCX) |
                   ext.Equals(Constants.FILETYPE_PDF) ||
                   ext.Equals(Constants.FILETYPE_TEXT) |
                   ext.Equals(Constants.FILETYPE_HTML) ||
                   ext.Equals(Constants.FILETYPE_HTM);
        }

#endregion*/

    }
}
