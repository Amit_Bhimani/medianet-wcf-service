﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Medianet.Service.Dto
{
    [MessageContract]
    public class DocumentContentRequest
    {
        [MessageHeader(MustUnderstand = true)]
        public int? DocumentId { get; set; }

        [MessageHeader(MustUnderstand = true)]
        public int? ReleaseId { get; set; }
        
        [MessageHeader(MustUnderstand = true)]
        public int? SequenceNumber { get; set; }

        [MessageHeader(MustUnderstand = true)]
        public string SecurityKey { get; set; }

        [MessageHeader(MustUnderstand = true)]
        public string SessionKey { get; set; }
    }
}