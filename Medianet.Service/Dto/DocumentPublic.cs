﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class DocumentPublic
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int SequenceNumber { get; set; }
        [DataMember]
        public string FileExtension { get; set; }
        [DataMember]
        public string OriginalFilename { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public int ItemCount { get; set; }
        [DataMember]
        public string ContentType { get; set; }
        [DataMember]
        public List<ThumbnailPublic> Thumbnails { get; set; }
        [DataMember]
        public string DownloadUrl { get; set; }
    }
}
