﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class DocumentSummary
    {
        [DataMember]
        public int SequenceNumber { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public bool CanDistributeViaEmailBody { get; set; }
        [DataMember]
        public bool CanDistributeViaEmailAttachment { get; set; }
        [DataMember]
        public bool CanDistributeViaWire { get; set; }
        //[DataMember]
        //public bool CanDistributeViaVoice { get; set; }
        [DataMember]
        public bool CanDistributeViaWeb { get; set; }
        [DataMember]
        public bool CanDistributeViaSMS { get; set; }
        [DataMember]
        public bool IsMailMergeDataSource { get; set; }
        [DataMember]
        public int? TempFileId { get; set; }

        // Used when attaching an inbound wav file to a job.
        [DataMember]
        public int? InboundFileId { get; set; }
    }
}
