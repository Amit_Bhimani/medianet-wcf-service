﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Medianet.Service.Dto
{
    [MessageContract]
    public class FileContentResponse
    {
        [MessageHeader(MustUnderstand = true)]
        public int FileId { get; set; }

        [MessageHeader(MustUnderstand = true)]
        public string FileExtension { get; set; }

        [MessageHeader(MustUnderstand = true)]
        public string OriginalFilename { get; set; }

        [MessageHeader(MustUnderstand = true)]
        public int FileSize { get; set; }

        [MessageHeader(MustUnderstand = true)]
        public int ItemCount { get; set; }

        [MessageHeader(MustUnderstand = true)]
        public string ContentType { get; set; }

        [MessageBodyMember(Order = 1)]
        public Stream FileData { get; set; }
    }
}
