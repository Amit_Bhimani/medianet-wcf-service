﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class InboundFile
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Caller { get; set; }
        [DataMember]
        public DistributionType DistributionType { get; set; }
        [DataMember]
        public string FileExtension { get; set; }
        [DataMember]
        public string OriginalFilename { get; set; }
        [DataMember]
        public int? OwnerUserId { get; set; }
        [DataMember]
        public string InboundNumber { get; set; }
        [DataMember]
        public DateTime ForwardedDate { get; set; }
        [DataMember]
        public int ForwardedCount { get; set; }
        [DataMember]
        public int ItemCount { get; set; }
        [DataMember]
        public int? FinanceFileId { get; set; }
        [DataMember]
        public int FileSize { get; set; }
        [DataMember]
        public int Length { get; set; }
        [DataMember]
        public InboundFileStatusType Status { get; set; }
        [DataMember]
        public MessageType Type { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int CreatedByUserId { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public int LastModifiedByUserId { get; set; }
    }
}
