﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class InboundFilePage
    {
        [DataMember]
        public int TotalCount { get; set; }
        [DataMember]
        public MessageType Type { get; set; }
        [DataMember]
        public int StartPos { get; set; }
        [DataMember]
        public int PageSize { get; set; }
        [DataMember]
        public InboundFileSortOrderType SortOrder { get; set; }
        [DataMember]
        public List<InboundFile> InboundFiles;
    }
}
