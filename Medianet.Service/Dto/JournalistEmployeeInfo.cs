﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class JournalistEmployeeInfo
    {
        [DataMember]
        public MediaEmployee Employee { get; set; }

        [DataMember]
        public MediaContactDraft ContactDraft { get; set; }

        [DataMember]
        public MediaEmployeeDraft EmployeeDraft { get; set; }
    }
}
