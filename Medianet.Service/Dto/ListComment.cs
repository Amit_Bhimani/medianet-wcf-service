﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class ListComment
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int? ServiceId { get; set; }
        [DataMember]
        public int? PackageId { get; set; }
        [DataMember]
        public string Comment { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int CreatedByUserId { get; set; }
    }
}
