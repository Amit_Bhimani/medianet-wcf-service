﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class ListInformation
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Introduction { get; set; }
        [DataMember]
        public List<string> OutletNames { get; set; }
        [DataMember]
        public ListComment Comment { get; set; }

        [DataMember]
        public string AttachmentFileName { get; set; }
    }
}
