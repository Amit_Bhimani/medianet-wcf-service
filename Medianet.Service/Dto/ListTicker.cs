﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    public class ListTicker
    {
        [DataMember]
        public int ListCategoryId { get; set; }
        [DataMember]
        public int ListCategoryParentSequenceNumber { get; set; }
        [DataMember]
        public int ListCategorySequenceNumber { get; set; }
        [DataMember]
        public string ListCategoryName { get; set; }
        [DataMember]
        public int ListId { get; set; }
        [DataMember]
        public int ListSequenceNumber { get; set; }
        [DataMember]
        public DistributionType ListDistributionType { get; set; }
        [DataMember]
        public string ListDescription { get; set; }
    }
}
