﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace Medianet.Service.Dto
{
    [MessageContract]
    public class LogoFileRequest
    {
        [MessageHeader(MustUnderstand = true)]
        public string LogoFileName { get; set; }

        [MessageHeader(MustUnderstand = true)]
        public string sessionKey { get; set; }

        [MessageHeader(MustUnderstand = true)]
        public bool Draft { get; set; }
    }
}
