﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace Medianet.Service.Dto
{
    [MessageContract]
    public class LogoFileResponse
    {
        [MessageHeader(MustUnderstand = true)]
        public string LogoFileName { get; set; }

        [MessageBodyMember(Order = 1)]
        public Stream FileData { get; set; }

    }
}
