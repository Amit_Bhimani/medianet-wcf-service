﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class LogoUploadRequest
    {
        [DataMember]
        public string OutletId { get; set; }

        [DataMember]
        public int TempFileId { get; set; }

        [DataMember]
        public string SessionKey { get; set; }

        [DataMember]
        public bool UpdateOutlet { get; set; }
    }
}
