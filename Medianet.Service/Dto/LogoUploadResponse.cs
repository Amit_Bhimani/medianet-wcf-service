﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class LogoUploadResponse
    {
        [DataMember]
        public string LogoFileName { get; set; }

        [DataMember]
        public bool status { get; set; }

    }
}
