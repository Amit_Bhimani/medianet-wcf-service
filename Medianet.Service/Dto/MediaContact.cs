﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MediaContact
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Prefix { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string MiddleName { get; set; }
        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public int? LastModifiedByUserId { get; set; }
        [DataMember]
        public int? LockedByUserId { get; set; }
        [DataMember]
        public DateTime? DeletedDate { get; set; }
        [DataMember]
        public ContactRowStatusType RowStatus { get; set; }
        [DataMember]
        public string InternalNotes { get; set; }
        [DataMember]
        public string Bio { get; set; }
        
        [DataMember]
        public int? MediaInfluencerScore { get; set; }

        [DataMember]
        public bool? IsDirty { get; set; }

        [DataMember]
        public string Profile { get; set; }

        [DataMember]
        public string Facebook { get; set; }
        [DataMember]
        public string LinkedIn { get; set; }
        [DataMember]
        public string Twitter { get; set; }
        [DataMember]
        public string Instagram { get; set; }
        [DataMember]
        public string YouTube { get; set; }
        [DataMember]
        public string Snapchat { get; set; }
        [DataMember]
        public string Skype { get; set; }
        [DataMember]
        public string BugBears { get; set; }
        [DataMember]
        public string AlsoKnownAs { get; set; }
        [DataMember]
        public string PressReleaseInterests { get; set; }
        [DataMember]
        public string Awards { get; set; }
        [DataMember]
        public string PersonalInterests { get; set; }
        [DataMember]
        public string PoliticalParty { get; set; }
        [DataMember]
        public string NamePronunciation { get; set; }
        [DataMember]
        public string AppearsIn { get; set; }
        [DataMember]
        public string BestContactTime { get; set; }
        [DataMember]
        public string BusyTimes { get; set; }
        [DataMember]
        public bool? GiftsAccepted { get; set; }
        [DataMember]
        public string PitchingDos { get; set; }
        [DataMember]
        public string PitchingDonts { get; set; }
        [DataMember]
        public string AuthorOf { get; set; }
        [DataMember]
        public string CoffeeOrder { get; set; }
        [DataMember]
        public string InterviewLink { get; set; }
        [DataMember]
        public string InterviewPhotoLink { get; set; }
        [DataMember]
        public User User { get; set; }
        [DataMember]
        public List<MediaContactJobHistory> JobHistory { get; set; }
    }
}
