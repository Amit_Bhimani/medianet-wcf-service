﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Service.Dto
{
    public class MediaContactAlsoViewedDto
    {
        public string ContactId { get; set; }
        public string OutletId { get; set; }
        public int RecordType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public string OutletName { get; set; }
        public bool IsGeneric { get; set; }
        public string TwitterHandle { get; set; }
        public DateTime DateViewed { get; set; }
        public int ViewCount { get; set; }
    }
}
