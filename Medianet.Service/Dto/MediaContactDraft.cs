﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MediaContactDraft
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int QueueId { get; set; }
        [DataMember]
        public string ContactId { get; set; }
        [DataMember]
        public string Prefix { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string MiddleName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string Bio { get; set; }
        [DataMember]
        public string BugBears { get; set; }
        [DataMember]
        public string AlsoKnownAs { get; set; }
        [DataMember]
        public string PressReleaseInterests { get; set; }
        [DataMember]
        public string Awards { get; set; }
        [DataMember]
        public string PersonalInterests { get; set; }
        [DataMember]
        public string PoliticalParty { get; set; }
        [DataMember]
        public string NamePronunciation { get; set; }
        [DataMember]
        public string AppearsIn { get; set; }
        [DataMember]
        public string Profile { get; set; }
        [DataMember]
        public string BestContactTime { get; set; }
        [DataMember]
        public string BusyTimes { get; set; }
        [DataMember]
        public string AuthorOf { get; set; }
        [DataMember]
        public string GiftsAccepted { get; set; }
        [DataMember]
        public string CoffeeOrder { get; set; }
        [DataMember]
        public string PitchingDos { get; set; }
        [DataMember]
        public string PitchingDonts { get; set; }
        [DataMember]
        public string Facebook { get; set; }
        [DataMember]
        public string LinkedIn { get; set; }
        [DataMember]
        public string Twitter { get; set; }
        [DataMember]
        public string Instagram { get; set; }
        [DataMember]
        public string YouTube { get; set; }
        [DataMember]
        public string Snapchat { get; set; }
        [DataMember]
        public string Skype { get; set; }

        [DataMember]
        public bool IsDeleted { get; set; }

        [DataMember]
        public MediaDraftQueue DraftQueue { get; set; }
    }
}
