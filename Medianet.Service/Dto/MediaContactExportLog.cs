﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    public class MediaContactExportLog
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string DebtorNumber { get; set; }
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public int? ListId { get; set; }
        [DataMember]
        public int? TaskId { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int NoOfExportedRows { get; set; }
        [DataMember]
        public string ExportedFields { get; set; }
    }
}
