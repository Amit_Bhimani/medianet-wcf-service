﻿using System;
using System.Runtime.Serialization;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MediaContactJobHistory
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string ContactId { get; set; }
        [DataMember]
        public string OutletId { get; set; }
        [DataMember]
        public string OutletName { get; set; }
        [DataMember]
        public string JobTitle { get; set; }
        [DataMember]
        public int StartYear { get; set; }
        [DataMember]
        public int? StartMonth { get; set; }

        [DataMember]
        public int? EndYear { get; set; }
        [DataMember]
        public int? EndMonth { get; set; }
        
        [DataMember]
        public int CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public int? LastModifiedBy { get; set; }
    }
}
