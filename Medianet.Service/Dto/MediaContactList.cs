﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MediaContactList
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int GroupId { get; set; }
        [DataMember]
        public int OwnerUserId { get; set; }
        [DataMember]
        public bool IsPrivate { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }

        [DataMember]
        public UserSummary OwnerUser { get; set; }
        [DataMember]
        public MediaContactGroup Group { get; set; }
        //[DataMember]
        //public List<MediaContactListVersion> Versions { get; set; }
    }
}
