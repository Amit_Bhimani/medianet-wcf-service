﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MediaContactListRecordDetails
    {
        [DataMember]
        public int RecordId { get; set; }
        [DataMember]
        public int RecordType { get; set; }
        [DataMember]
        public string OutletId { get; set; }
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public string OutletName { get; set; }
        [DataMember]
        public string ContactId { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string JobTitle { get; set; }
        [DataMember]
        public string SubjectName { get; set; }
        [DataMember]
        public string ProductTypeName { get; set; }
        [DataMember]
        public string RoleName { get; set; }
        [DataMember]
        public string CityName { get; set; }
        [DataMember]
        public bool IsEntityDeleted { get; set; }
        [DataMember]
        public string ContactDisplay { get; set; }
        [DataMember]
        public string RoleDisplay { get; set; }
        [DataMember]
        public string SubjectDisplay { get; set; }
        [DataMember]
        public int? MediaInfluencerScore { get; set; }
        [DataMember]
        public bool IsGeneric { get; set; }
    }
}
