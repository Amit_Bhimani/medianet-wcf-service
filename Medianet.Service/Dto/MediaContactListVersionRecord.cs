﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MediaContactListVersionRecord
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int RecordId { get; set; }
        [DataMember]
        public int ListSetId { get; set; }
        [DataMember]
        public MediaContactListChangeType ChangeType { get; set; }
        [DataMember]
        public string MediaOutletId { get; set; }
        [DataMember]
        public string MediaContactId { get; set; }
        [DataMember]
        public int? OmaOutletId { get; set; }
        [DataMember]
        public int? OmaContactId { get; set; }
        [DataMember]
        public decimal? PrnOutletId { get; set; }
        [DataMember]
        public decimal? PrnContactId { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int CreatedByUserId { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
    }
}
