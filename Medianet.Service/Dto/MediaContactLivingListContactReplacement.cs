﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;
using Medianet.BusinessLayer.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MediaContactLivingListContactReplacement
    {
        [DataMember]
        public int RecordId { get; set; }
        [DataMember]
        public int ListSetId { get; set; }
        [DataMember]
        public int ListId { get; set; }
        [DataMember]
        public string ListName { get; set; }
        [DataMember]
        public string MediaContactId { get; set; }
        [DataMember]
        public string PrnContactId { get; set; }
        [DataMember]
        public string OmaContactId { get; set; }
        [DataMember]
        public string ContactDeletedName { get; set; }
        [DataMember]
        public string OutletDeletedName { get; set; }

        [DataMember]
        public string ContactReplacementId{ get; set; }

        [DataMember]
        public string ContactReplacementName { get; set; }

        [DataMember]
        public string OutletDeletedId { get; set; }

        [DataMember]
        public int RecordType { get; set; }
    }
}
