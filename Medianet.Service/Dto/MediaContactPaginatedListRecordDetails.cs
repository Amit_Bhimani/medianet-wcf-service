﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MediaContactPaginatedListRecordDetails
    {
        [DataMember]
        public List<MediaContactListRecordDetails> ListRecord { get; set; }
        [DataMember]
        public int TotalRecordsCount { get; set; }
    }
}
