using System.Runtime.Serialization;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MediaContactPitchingProfileDto
    {
        [DataMember]
        public string BugBears { get; set; }
        [DataMember]
        public string AlsoKnownAs { get; set; }
        [DataMember]
        public string PressReleaseInterests { get; set; }
        [DataMember]
        public string Awards { get; set; }
        [DataMember]
        public string PersonalInterests { get; set; }
        [DataMember]
        public string PoliticalParty { get; set; }
        [DataMember]
        public string NamePronunciation { get; set; }
        [DataMember]
        public string AppearsIn { get; set; }
        [DataMember]
        public string BestContactTime  { get; set; }
        [DataMember]
        public string BusyTimes { get; set; }
        [DataMember]
        public bool? GiftsAccepted { get; set; }
        [DataMember]
        public string PitchingDos { get; set; }
        [DataMember]
        public string PitchingDonts { get; set; }
        [DataMember]
        public string AuthorOf { get; set; }
        [DataMember]
        public string CoffeeOrder { get; set; }

        //For Employee view used in Journalist
        [DataMember]
        public string BasedInLocation { get; set; }
        [DataMember]
        public string Deadlines { get; set; }
        [DataMember]
        public string CurrentStatus { get; set; }
    }
}