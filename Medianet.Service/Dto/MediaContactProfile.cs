﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MediaContactProfile
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string ContactId { get; set; }
        [DataMember]
        public string Content { get; set; }
        [DataMember]
        public DateTime Updated { get; set; }
    }
}
