﻿using System;
using System.Runtime.Serialization;
using Medianet.BusinessLayer.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MediaContactRecentChange
    {
        [DataMember]
        public string OutletId { get; set; }
        [DataMember]
        public string ContactId { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string JobTitle { get; set; }
        [DataMember]
        public string CompanyName { get; set; }
        [DataMember]
        public string LogoFileName { get; set; }
        [DataMember]
        public SocialInfo SocialDetails { get; set; }
        [DataMember]
        public int MediaType { get; set; }
        [DataMember]
        public DateTime Date { get; set; }
    }
}
