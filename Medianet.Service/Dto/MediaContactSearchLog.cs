﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    public class MediaContactSearchLog
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string DebtorNumber { get; set; }
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public string SearchCriteria { get; set; }
        [DataMember]
        public int SearchType { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int? SavedSearchId { get; set; }
        [DataMember]
        public SystemType System { get; set; }
        [DataMember]
        public string SearchContext { get; set; }
        [DataMember]
        public int? ResultsCount { get; set; }
    }
}
