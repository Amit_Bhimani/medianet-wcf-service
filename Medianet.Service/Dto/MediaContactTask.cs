﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MediaContactTask
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public int GroupId { get; set; }
        [DataMember]
        public int ReminderTypeId { get; set; }
        [DataMember]
        public MediaContactNotificationType NotificationType { get; set; }
        [DataMember]
        public DateTime DueDate { get; set; }
        [DataMember]
        public MediaContactTaskStatusType StatusType { get; set; }
        [DataMember]
        public int? CategoryId { get; set; }
        [DataMember]
        public bool IsEmailSent { get; set; }
        [DataMember]
        public bool IsSmsSent { get; set; }
        [DataMember]
        public int OwnerUserId { get; set; }
        [DataMember]
        public bool IsPrivate { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int CreatedByUserId { get; set; } 
        [DataMember]
        public DateTime LastModifiedDate { get; set; }

        [DataMember]
        public UserSummary OwnerUser { get; set; }
        [DataMember]
        public UserSummary CreatedByUser { get; set; }
        [DataMember]
        public MediaContactGroup Group { get; set; }
        [DataMember]
        public MediaContactReminderType ReminderType { get; set; }
        [DataMember]
        public MediaContactTaskCategory Category { get; set; }
    }
}
