﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.BusinessLayer.Common;

namespace Medianet.Service.Dto
{
    public class MediaContactViewSummaryDto
    {
        public long Id { get; set; }
        public string ContactId { get; set; }
        public string OutletId { get; set; }
        public int SubjectCodeId { get; set; }
        public int ViewCount { get; set; }
    }
}
