﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MediaDatabaseLog
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int UserID { get; set; }
        [DataMember]
        public System.DateTime Accessed { get; set; }
        [DataMember]
        public string OutletID { get; set; }
        [DataMember]
        public string ContactID { get; set; }
        [DataMember]
        public string FieldsUpdated { get; set; }
        [DataMember]
        public int? DraftQueueId { get; set; }
        [DataMember]
        public bool ProfileViewed { get; set; }

        [DataMember]
        public User User { get; set; }
    }
}
