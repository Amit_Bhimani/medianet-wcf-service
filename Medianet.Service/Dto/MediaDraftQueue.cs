﻿using System;
using System.Runtime.Serialization;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MediaDraftQueue
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string ContactId { get; set; }
        [DataMember]
        public string OutletId { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime? LastModifiedDate { get; set; }
        [DataMember]
        public int? LastModifiedBy { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public byte[] RowVersion { get; set; }

        [DataMember]
        public string JournalistComment { get; set; }

        [DataMember]
        public bool SkipSendingEmailToJournalist { get; set; }
    }
}
