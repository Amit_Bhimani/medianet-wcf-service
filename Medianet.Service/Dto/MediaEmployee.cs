﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MediaEmployee
    {
        [DataMember]
        public string OutletId { get; set; }
        [DataMember]
        public string ContactId { get; set; }
        [DataMember]
        public string JobTitle { get; set; }
        [DataMember]
        public string AddressLine1 { get; set; }
        [DataMember]
        public string AddressLine2 { get; set; }
        [DataMember]
        public int? CityId { get; set; }
        [DataMember]
        public string PostCode { get; set; }
        [DataMember]
        public int CountryId { get; set; }
        [DataMember]
        public string PhoneNumber { get; set; }
        [DataMember]
        public string FaxNumber { get; set; }
        [DataMember]
        public string MobileNumber { get; set; }
        [DataMember]
        public string EmailAddress { get; set; }
        
        [DataMember]
        public string BlogURL { get; set; }
        [DataMember]
        public PreferredDeliveryType PreferredDeliveryMethod { get; set; }
        [DataMember]
        public string OutletName { get; set; }
        [DataMember]
        public bool IsPrimaryNewsContact { get; set; }
        [DataMember]
        public bool HasReceivedLegals { get; set; }
        [DataMember]
        public string TrackingReference { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public int? LastModifiedByUserId { get; set; }
        [DataMember]
        public DateTime? DeletedDate { get; set; }
        [DataMember]
        public EmployeeRowStatusType RowStatus { get; set; }

        [DataMember]
        public User User { get; set; }
        [DataMember]
        public MediaContact MediaContact { get; set; }
        [DataMember]
        public MediaOutlet MediaOutlet { get; set; }

        [DataMember]
        public List<MediaEmployeeRole> Roles { get; set; }

        [DataMember]
        public List<MediaEmployeeSubject> Subjects { get; set; }

        [DataMember]
        public List<ServiceList> ServiceLists { get; set; }

        [DataMember]
        public string EmailServiceIdsCsv { get; set; }

        [DataMember]
        public string CurrentStatus { get; set; }
        [DataMember]
        public string BasedInLocation { get; set; }
        [DataMember]
        public string Deadlines { get; set; }
        [DataMember]
        public string AlternativeEmailAddress { get; set; }
        [DataMember]
        public string AdditionalMobile { get; set; }
        [DataMember]
        public string AdditionalPhoneNumber { get; set; }
        [DataMember]
        public string Skype { get; set; }
        [DataMember]
        public string Website { get; set; }
        [DataMember]
        public string OfficeHours { get; set; }

        [DataMember]
        public bool CurrentlyOutOfOffice { get; set; }

        [DataMember]
        public DateTime? OutOfOfficeReturnDate { get; set; }

        [DataMember]
        public DateTime? OutOfOfficeStartDate { get; set; }
    }
}
