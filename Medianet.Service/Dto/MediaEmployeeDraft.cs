﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MediaEmployeeDraft
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string ContactId { get; set; }

        [DataMember]
        public string OutletId { get; set; }

        [DataMember]
        public int QueueId { get; set; }

        [DataMember]
        public string JobTitle { get; set; }

        [DataMember]
        public string AddressLine1 { get; set; }

        [DataMember]
        public string AddressLine2 { get; set; }

        [DataMember]
        public int? CityId { get; set; }

        [DataMember]
        public int? StateId { get; set; }

        [DataMember]
        public string PostCode { get; set; }

        [DataMember]
        public int? CountryId { get; set; }

        [DataMember]
        public string PhoneNumber { get; set; }

        [DataMember]
        public string FaxNumber { get; set; }

        [DataMember]
        public string MobileNumber { get; set; }

        [DataMember]
        public string EmailAddress { get; set; }

        [DataMember]
        public string BlogURL { get; set; }

        [DataMember]
        public PreferredDeliveryType? PreferredDeliveryMethod { get; set; }

        [DataMember]
        public bool? IsPrimaryNewsContact { get; set; }

        [DataMember]
        public string TrackingReference { get; set; }

        [DataMember]
        public string Roles { get; set; }

        [DataMember]
        public string Subjects { get; set; }

        [DataMember]
        public string CurrentStatus { get; set; }

        [DataMember]
        public string BasedInLocation { get; set; }

        [DataMember]
        public string Deadlines { get; set; }

        [DataMember]
        public string AlternativeEmailAddress { get; set; }

        [DataMember]
        public string AdditionalMobile { get; set; }

        [DataMember]
        public string AdditionalPhoneNumber { get; set; }

        [DataMember]
        public string Website { get; set; }

        [DataMember]
        public string OfficeHours { get; set; }

        [DataMember]
        public bool IsDeleted { get; set; }

        [DataMember]
        public string AdditionalSubjects { get; set; }

        [DataMember]
        public bool? CurrentlyOutOfOffice { get; set; }

        [DataMember]
        public DateTime? OutOfOfficeReturnDate { get; set; }

        [DataMember]
        public DateTime? OutOfOfficeStartDate { get; set; }

        [DataMember]
        public MediaDraftQueue DraftQueue { get; set; }
    }
}
