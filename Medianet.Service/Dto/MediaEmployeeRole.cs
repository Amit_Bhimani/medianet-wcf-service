﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MediaEmployeeRole
    {
        [DataMember]
        public string OutletId { get; set; }
        [DataMember]
        public string ContactId { get; set; }
        [DataMember]
        public int RoleId { get; set; }
    }
}
