﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;
using Medianet.Service.BusinessLayer.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MediaEmployeeSubjectValidation
    {
        [DataMember]
        public string OutletId { get; set; }
        [DataMember]
        public string PrimaryContactId { get; set; }
        [DataMember]
        public int SubjectId { get; set; }

        public bool IsValidated(string currentOutletId, string currentContactId, bool currentIsPrimary, string currentRowStatus) {
            currentOutletId = MediaDatabaseHelper.PadOutletId(currentOutletId);
            OutletId = MediaDatabaseHelper.PadOutletId(OutletId);
            currentContactId = MediaDatabaseHelper.PadContactId(currentContactId);
            PrimaryContactId = MediaDatabaseHelper.PadContactId(PrimaryContactId);

            if (!string.IsNullOrWhiteSpace(PrimaryContactId)) {
                if (currentOutletId == OutletId) {
                    if ((!currentIsPrimary && PrimaryContactId != currentContactId) ||
                        (currentIsPrimary && PrimaryContactId == currentContactId && currentRowStatus == Constants.ROWSTATUS_ACTIVE)) {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool IsValidated(string currentOutletId) {
            currentOutletId = MediaDatabaseHelper.PadOutletId(currentOutletId);
            OutletId = MediaDatabaseHelper.PadOutletId(OutletId);

            if (!string.IsNullOrWhiteSpace(PrimaryContactId)) {
                if (currentOutletId == OutletId) {
                    return true;
                }
            }

            return false;
        }
    }
}
