﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;
using Medianet.Service.BusinessLayer.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MediaEmployeeValidation
    {
        [DataMember]
        public string OutletId { get; set; }
        [DataMember]
        public string PrimaryContactId { get; set; }

        [DataMember]
        public List<MediaEmployeeSubjectValidation> Subjects { get; set; }

        public bool IsValidated(string currentOutletId, string currentContactId, bool currentIsPrimary, string currentRowStatus) {
            currentOutletId = MediaDatabaseHelper.PadOutletId(currentOutletId);
            OutletId = MediaDatabaseHelper.PadOutletId(OutletId);
            currentContactId = MediaDatabaseHelper.PadContactId(currentContactId);
            PrimaryContactId = MediaDatabaseHelper.PadContactId(PrimaryContactId);

            if (!string.IsNullOrWhiteSpace(PrimaryContactId)) {
                if (currentOutletId == OutletId) {
                    if ((!currentIsPrimary && PrimaryContactId != currentContactId) ||
                        (currentIsPrimary && PrimaryContactId == currentContactId && currentRowStatus == Constants.ROWSTATUS_ACTIVE)) {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
