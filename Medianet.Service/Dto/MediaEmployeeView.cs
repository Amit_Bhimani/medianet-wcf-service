﻿using Medianet.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Medianet.Service.Dto
{

    [DataContract]
    public class MediaEmployeeView
    {
        [DataMember]
        public string ContactId { get; set; }
        [DataMember]
        public string Prefix { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string MiddleName { get; set; }
        [DataMember]
        public string JobTitle { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string Mobile { get; set; }
        [DataMember]
        public string Fax { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]

        public string AlternativeEmailAddress { get; set; }
        [DataMember]
        public string AdditionalMobileNumber { get; set; }
        [DataMember]
        public string AdditionalPhoneNumber { get; set; }
        [DataMember]
        public string OfficeHours { get; set; }
        [DataMember]

        public string Bio { get; set; }
        [DataMember]
        public int? MediaInfluencerScore { get; set; }
        [DataMember]
        public string BlogUrl { get; set; }
        [DataMember]
        public string WebUrl { get; set; }
        [DataMember]
        public string Profile { get; set; }
        [DataMember]
        public bool CurrentlyOutOfOffice { get; set; }
        [DataMember]
        public DateTime? OutOfOfficeReturnDate { get; set; }

        [DataMember]
        public DateTime? OutOfOfficeStartDate { get; set; }

        [DataMember]
        public SocialInfo SocialDetails { get; set; }
        [DataMember]
        public Address Address { get; set; }
        [DataMember]
        public MediaContactPitchingProfileDto MediaContactPitchingProfileDto { get; set; }

        [DataMember]
        public PreferredDeliveryType? PreferredDeliveryMethod { get; set; }

        [DataMember]
        public List<KeyValuePair<int, string>> Roles { get; set; }
        [DataMember]
        public List<MediaEmployeeSubjectView> Subjects { get; set; }
        [DataMember]
        public List<MediaEmployeeListView> BelongsToLists { get; set; }
        [DataMember]
        public List<MediaEmployeeDocumentView> Documents { get; set; }
        [DataMember]
        public MediaEmployeeOutletView AtOutlet { get; set; }
    }

    [DataContract]
    public class MediaEmployeeSubjectView
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public bool IsPrimaryContact { get; set; }
    }

    [DataContract]
    public class MediaEmployeeListView
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public bool IsPrivate { get; set; }
        [DataMember]
        public DateTime? UpdatedDate { get; set; }
        [DataMember]
        public bool ListStatus { get; set; }
        [DataMember]
        public int GroupId { get; set; }
        [DataMember]
        public string GroupName { get; set; }
        [DataMember]
        public bool GroupStatus { get; set; }
        [DataMember]
        public short GroupType { get; set; }
    }

    [DataContract]
    public class MediaEmployeeOutletView
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int OutletRecordType { get; set; }
        [DataMember]
        public string OutletId { get; set; }
        [DataMember]
        public string ContactId { get; set; }
        [DataMember]
        public bool IsGeneric { get; set; }

        [DataMember]
        public string LogoFileName { get; set; }
    }

    [DataContract]
    public class MediaEmployeeDocumentView
    {
        [DataMember]
        public int DocumentId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public DateTime? CreatedDate { get; set; }

        [DataMember]
        public int UploadedByUserId { get; set; }

        [DataMember]
        public bool? Status { get; set; }

        [DataMember]
        public bool IsPrivate { get; set; }
    }
}
