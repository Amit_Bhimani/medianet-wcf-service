﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MediaOmaContact
    {
        [DataMember]
        public string ContactId { get; set; }

        [DataMember]
        public string OutletId { get; set; }

        [DataMember]
        public int RecordType { get; set; }

        [DataMember]
        public string Prefix { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string MiddleName { get; set; }

        [DataMember]
        public string LastName { get; set; }
        
        [DataMember]
        public string JobTitle { get; set; }

        [DataMember]
        public string EmailAddress { get; set; }

        [DataMember]
        public string PhoneNumber { get; set; }
    }
}
