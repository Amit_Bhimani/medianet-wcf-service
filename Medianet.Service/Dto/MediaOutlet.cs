﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MediaOutlet
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public bool HasParent { get; set; }
        [DataMember]
        public string ParentId { get; set; }
        [DataMember]
        public string OwnershipCompany { get; set; }
        [DataMember]
        public string AddressLine1 { get; set; }
        [DataMember]
        public string AddressLine2 { get; set; }
        [DataMember]
        public int? CityId { get; set; }
        [DataMember]
        public string PostCode { get; set; }
        [DataMember]
        public int CountryId { get; set; }
        [DataMember]
        public int ContinentId { get; set; }
        [DataMember]
        public string PostalAddressLine1 { get; set; }
        [DataMember]
        public string PostalAddressLine2 { get; set; }
        [DataMember]
        public int? PostalCityId { get; set; }
        [DataMember]
        public string PostalPostCode { get; set; }
        [DataMember]
        public int? PostalCountryId { get; set; }
        [DataMember]
        public int? PostalContinentId { get; set; }
        [DataMember]
        public string PhoneNumber { get; set; }
        [DataMember]
        public string FaxNumber { get; set; }
        [DataMember]
        public string EmailAddress { get; set; }
        [DataMember]
        public string WebSite { get; set; }
        [DataMember]
        public string Facebook { get; set; }
        [DataMember]
        public string LinkedIn { get; set; }
        [DataMember]
        public string Twitter { get; set; }
        [DataMember]
        public string Instagram { get; set; }
        [DataMember]
        public string YouTube { get; set; }
        [DataMember]
        public string Snapchat { get; set; }
        [DataMember]
        public string BlogURL { get; set; }
        [DataMember]
        public int? OutletTypeId { get; set; }
        [DataMember]
        public int? ProductTypeId { get; set; }
        [DataMember]
        public PreferredDeliveryType PreferredDeliveryMethod { get; set; }
        [DataMember]
        public decimal? Circulation { get; set; }
        [DataMember]
        public decimal? Audience { get; set; }
        [DataMember]
        public string CallLetters { get; set; }
        [DataMember]
        public string YearEstablished { get; set; }
        [DataMember]
        public string StationFrequency { get; set; }
        [DataMember]
        public string CopyPrice { get; set; }
        [DataMember]
        public int? FrequencyId { get; set; }
        [DataMember]
        public int? NewsFocusId { get; set; }
        [DataMember]
        public string TrackingReference { get; set; }
        [DataMember]
        public bool SendToOMA { get; set; }
        [DataMember]
        public string P16 { get; set; }
        [DataMember]
        public string M16 { get; set; }
        [DataMember]
        public string F16 { get; set; }
        [DataMember]
        public string GBs { get; set; }
        [DataMember]
        public string ABs { get; set; }
        [DataMember]
        public string InternalNotes { get; set; }
        [DataMember]
        public string Bio { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public int? LastModifiedByUserId { get; set; }
        [DataMember]
        public int? LockedByUserId { get; set; }
        [DataMember]
        public DateTime? DeletedDate { get; set; }
        [DataMember]
        public OutletRowStatusType RowStatus { get; set; }
        [DataMember]
        public bool HasReceivedLegals { get; set; }


        [DataMember]
        public string NamePronunciation { get; set; }
        [DataMember]
        public bool? SubscriptionOnlyPublication { get; set; }
        [DataMember]
        public string AlsoKnownAs { get; set; }
        [DataMember]
        public string Deadlines { get; set; }
        [DataMember]
        public int? Readership { get; set; }
        [DataMember]
        public string BroadcastTime { get; set; }
        [DataMember]
        public string AlternativeEmailAddress { get; set; }
        [DataMember]
        public string Mobile { get; set; }
        [DataMember]
        public string AdditionalPhoneNumber { get; set; }
        [DataMember]
        public string AdditionalWebsite { get; set; }
        [DataMember]
        public string Skype { get; set; }
        [DataMember]
        public string OfficeHours { get; set; }
        [DataMember]
        public string PublishedOn { get; set; }
        [DataMember]
        public int? MediaInfluencerScore { get; set; }
        [DataMember]
        public string LogoFileName { get; set; }
        [DataMember]
        public string PressReleaseInterests { get; set; }
        [DataMember]
        public string RegionsCovered { get; set; }

        [DataMember]
        public bool? IsGeneric { get; set; }

        [DataMember]
        public bool? IsDirty { get; set; }

        [DataMember]
        public User User { get; set; }
        [DataMember]
        public string Profile { get; set; }

        [DataMember]
        public List<MediaOutletSubject> Subjects { get; set; }
        [DataMember]
        public List<MediaOutletWorkingLanguage> WorkingLanguages { get; set; }
        [DataMember]
        public List<MediaOutletStationFormat> StationFormats { get; set; }
        [DataMember]
        public List<ServiceList> ServiceLists { get; set; }

        [DataMember]
        public string EmailServiceIdsCsv { get; set; }

        [DataMember]
        public List<MediaRelatedOutlet> RelatedOutlets { get; set; }

        [DataMember]
        public List<MediaSyndicatedOutlet> SyndicatedOutlets { get; set; }

    }
}
