﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MediaOutletPartial
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int? ProductTypeId { get; set; }

        [DataMember]
        public string ProductTypeName { get; set; }

        [DataMember]
        public string ProductTypeCategory { get; set; }
    }
}
