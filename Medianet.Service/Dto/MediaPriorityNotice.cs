﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Medianet.Service.Dto
{

    [DataContract]
    public class MediaPriorityNotice
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string ContactId { get; set; }

        [DataMember]
        public string OutletId { get; set; }

        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string MiddleName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string CompanyName { get; set; }
        [DataMember]
        public string Description { get; set; }


        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int? CreatedByUserId { get; set; }
        [DataMember]
        public string CreatedByUserName { get; set; }
        [DataMember]
        public DateTime? LastModifiedDate { get; set; }
        [DataMember]
        public int? LastModifiedByUserId { get; set; }
    }
}
