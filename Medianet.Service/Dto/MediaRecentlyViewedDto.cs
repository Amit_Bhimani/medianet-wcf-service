﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MediaRecentlyViewedDto
    {
        [DataMember]
        public string OutletId { get; set; }
        [DataMember]
        public string OutletName { get; set; }
        [DataMember]
        public string ContactId { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
    }
}
