﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MediaRecordIdentifier
    {
        [DataMember]
        public string MediaOutletId { get; set; }
        [DataMember]
        public string MediaContactId { get; set; }
        [DataMember]
        public decimal? PrnOutletId { get; set; }
        [DataMember]
        public decimal? PrnContactId { get; set; }
        [DataMember]
        public int? OmaOutletId { get; set; }
        [DataMember]
        public int? OmaContactId { get; set; }
    }
}
