﻿// -----------------------------------------------------------------------
// <copyright file="MediaRelatedOutlet.cs" company="Australian Associated Press">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Medianet.Service.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class MediaRelatedOutlet
    {
        public string OutletId { get; set; }
        public string RelatedOutletId { get; set; }
    }
}
