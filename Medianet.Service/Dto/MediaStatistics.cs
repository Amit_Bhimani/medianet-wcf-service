﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MediaStatistics
    {
        [DataMember]
        public int AustralasiaToday { get; set; }
        [DataMember]
        public int AustralasiaLastWeek { get; set; }
        [DataMember]
        public int AustralasiaLastMonth { get; set; }
        [DataMember]
        public int AustralasiaYearToDate { get; set; }
        [DataMember]
        public int InternationalToday { get; set; }
        [DataMember]
        public int InternationalLastWeek { get; set; }
        [DataMember]
        public int InternationalYearToDate { get; set; }
    }
}
