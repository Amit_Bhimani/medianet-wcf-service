﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Service.Dto
{
    public class MediaSyndicatedOutlet
    {
        public string OutletId { get; set; }
        public string SyndicatedOutletId { get; set; }

    }
}
