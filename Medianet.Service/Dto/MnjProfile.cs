﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MnjProfile
    {
        [DataMember]
        public int ProfileID { get; set; }

        [DataMember]
        [Display(Name = "Profile Name")]
        [Required]
        public string ProfileName { get; set; }

        [DataMember]
        public int UserID { get; set; }

        [DataMember]
        public bool IsDefault { get; set; }

        [DataMember]
        public bool EmailAlert { get; set; }

        [DataMember]
        [DataType(DataType.EmailAddress), Display(Name = "Email Address")]
        [EmailAddress]
        public string EmailAddress { get; set; }

        [DataMember]
        public List<MnjProfileCategory> Categories { get; set; }

        [DataMember]
        public List<MnjProfileKeyword> Keywords { get; set; }
    }
}
