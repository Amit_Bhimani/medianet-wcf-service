﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MnjProfileKeyword
    {
        [DataMember]
        public int ProfileID { get; set; }

        [DataMember]
        public string Keyword { get; set; }
    }
}
