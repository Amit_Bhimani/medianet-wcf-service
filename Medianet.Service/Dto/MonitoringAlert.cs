﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MonitoringAlert
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int CampaignId { get; set; }
        [DataMember]
        public int FrequencyId { get; set; }
        [DataMember]
        public string Subject{ get; set; }
        [DataMember]
        public string Blurb { get; set; }
        [DataMember]
        public string RecipientUsers { get; set; }
        [DataMember]
        public string CustomRecipients { get; set; }

        [DataMember]
        public virtual MonitoringFrequency Frequency { get; set; }

    }
}
