﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MonitoringCampaign
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string ProjectName { get; set; }
        [DataMember]
        public string CampaignName { get; set; }
        [DataMember]
        public int MonitoringCampaignId { get; set; }
        [DataMember]
        public int SearchId { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public bool IsPrivate { get; set; }
        [DataMember]
        public string DebtorNumber { get; set; }
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public virtual MonitoringSearch SearchCriteria { get; set; }
        [DataMember]
        public virtual List<MonitoringAlert> Alerts { get; set; }
    }
}
