﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MonitoringEmail
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public System.DateTime EmailDate { get; set; }
        [DataMember]
        public int SearchId { get; set; }
        [DataMember]
        public string Articles { get; set; }
        [DataMember]
        public string RecipientUsers { get; set; }
        [DataMember]
        public string CustomRecipients { get; set; }
        [DataMember]
        public int AlertId { get; set; }

        [DataMember]
        public virtual User User { get; set; }
        [DataMember]
        public virtual MonitoringSearch SearchCriteria { get; set; }
    }
}
