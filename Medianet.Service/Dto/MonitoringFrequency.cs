﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MonitoringFrequency
    {
        public const int HOURLY = 1;
        public const int DAILY = 2;
        public const int WEEKLY = 3;

        public const string HOURLY_TEXT = "Hourly";
        public const string DAILY_TEXT = "Daily";
        public const string WEEKLY_TEXT = "Weekly";

        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Frequency { get; set; }

        public static MonitoringFrequency GetFrequency(int frequencyId)
        {
            switch (frequencyId)
            {
                case HOURLY:
                    return new MonitoringFrequency { Id = HOURLY, Frequency = HOURLY_TEXT };
                case DAILY:
                    return new MonitoringFrequency { Id = DAILY, Frequency = DAILY_TEXT };
                case WEEKLY:
                    return new MonitoringFrequency { Id = WEEKLY, Frequency = WEEKLY_TEXT };
            }

            return null;
        }
    }
}
