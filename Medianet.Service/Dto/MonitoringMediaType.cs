﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MonitoringMediaType
    {
        public const int PRINT = 1;
        public const int BROADCAST = 2;
        public const int INTERNET = 3;
        public const int TEXT = 4;
        public const int BREAKINGNEWS = 5;

        public const string PRINT_PRODUCT = "PRINT";
        public const string BROADCAST_PRODUCT = "BROADCAST";
        public const string INTERNET_PRODUCT = "INTERNET";
        public const string TEXT_PRODUCT = "TEXT";
        public const string BREAKINGNEWS_PRODUCT = "BREAKINGNEWS";

        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string MediaTypeName { get; set; }

        public static MonitoringMediaType GetMediaType(int mediaTypeId)
        {
            switch (mediaTypeId)
            {
                case PRINT:
                    return new MonitoringMediaType { Id = PRINT, MediaTypeName = PRINT_PRODUCT };
                case BROADCAST:
                    return new MonitoringMediaType { Id = BROADCAST, MediaTypeName = BROADCAST_PRODUCT };
                case INTERNET:
                    return new MonitoringMediaType { Id = INTERNET, MediaTypeName = INTERNET_PRODUCT };
                case TEXT:
                    return new MonitoringMediaType { Id = TEXT, MediaTypeName = TEXT_PRODUCT };
                case BREAKINGNEWS:
                    return new MonitoringMediaType { Id = BREAKINGNEWS, MediaTypeName = BREAKINGNEWS_PRODUCT };
            }

            return null;
        }
    }
}
