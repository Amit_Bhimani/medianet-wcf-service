﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class MonitoringSearch
    {
        public MonitoringSearch()
        {
            CreationDate = DateTime.Now;
        }

        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public bool IsFullText { get; set; }
        [DataMember]
        public string Keywords { get; set; }
        [DataMember]
        public string Sources { get; set; }
        [DataMember]
        public string Owners { get; set; }
        [DataMember]
        public bool IsMostRecent { get; set; }
        [DataMember]
        public DateTime StartDate { get; set; }
        [DataMember]
        public DateTime EndDate { get; set; }
        [DataMember]
        public DateTime CreationDate { get; set; }

        [DataMember]
        public virtual User user { get; set; }
        [DataMember]
        public virtual List<MonitoringMediaType> MediaTypes { get; set; }
    }
}
