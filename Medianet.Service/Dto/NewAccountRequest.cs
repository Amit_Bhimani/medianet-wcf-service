﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class NewAccountRequest
    {
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string CompanyName { get; set; }
        [DataMember]
        public string IndustryCode { get; set; }
        [DataMember]
        public string ContactPosition { get; set; }
        [DataMember]
        public string AddressLine1 { get; set; }
        [DataMember]
        public string AddressLine2 { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string State { get; set; }
        [DataMember]
        public string Postcode { get; set; }
        [DataMember]
        public string Country { get; set; }
        [DataMember]
        public string EmailAddress { get; set; }
        [DataMember]
        public string TelephoneNumber { get; set; }
        [DataMember]
        public string FaxNumber { get; set; }
        [DataMember]
        public string ABN { get; set; }
        [DataMember]
        public string TimezoneCode { get; set; }
        [DataMember]
        public SystemType System { get; set; }

        [DataMember]
        public string UserLogonName { get; set; }
        [DataMember]
        public string CompanyLogonName { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public CustomerBillingType AccountType { get; set; }
    }
}
