﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class Note
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Notes { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public byte Status { get; set; }
        [DataMember]
        public Boolean IsPrivate { get; set; }
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public UserSummary User { get; set; }
        [DataMember]
        public string MediaOutletId { get; set; }
        [DataMember]
        public string MediaContactId { get; set; }
        [DataMember]
        public int? OmaOutletId { get; set; }
        [DataMember]
        public int? OmaContactId { get; set; }
        [DataMember]
        public decimal? PrnOutletId { get; set; }
        [DataMember]
        public decimal? PrnContactId { get; set; }
        [DataMember]
        public Boolean Pinned { get; set; }
        [DataMember]
        public Boolean AAPNote { get; set; }
        [DataMember]
        public string DebtorNumber { get; set; }
    }
}
