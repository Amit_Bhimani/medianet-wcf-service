﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class NotesBulk
    {
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public List<MediaRecordIdentifier> Records { get; set; }
        [DataMember]
        public Boolean IsPrivate { get; set; }
        [DataMember]
        public Boolean IsPinned { get; set; }
        [DataMember]
        public Boolean IsAapNote { get; set; }
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public string DebtorNumber { get; set; }
    }
}
