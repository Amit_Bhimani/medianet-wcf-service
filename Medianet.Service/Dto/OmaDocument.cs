﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class OmaDocument
    {
        [DataMember]
        public int DocumentId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public DateTime? CreatedDate { get; set; }

        [DataMember]
        public int UploadedByUserId { get; set; }

        [DataMember]
        public bool? Status { get; set; }

        [DataMember]
        public bool IsPrivate { get; set; }

        [DataMember]
        public byte[] DocumentContent { get; set; }

        [DataMember]
        public string DocumentType { get; set; }

        [DataMember]
        public string OutletId { get; set; }

        [DataMember]
        public string ContactId { get; set; }

        [DataMember]
        public int RecordType { get; set; }

    }
}
