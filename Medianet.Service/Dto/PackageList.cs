﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class PackageList
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string DebtorNumber { get; set; }
        [DataMember]
        public SystemType System { get; set; }
        [DataMember]
        public int SequenceNumber { get; set; }
        [DataMember]
        public DistributionType TransactionType { get; set; }
        [DataMember]
        public string AccountCode { get; set; }
        [DataMember]
        public string SelectionDescription { get; set; }
        [DataMember]
        public int? ListNumber { get; set; }
        [DataMember]
        public RowStatusType RowStatus { get; set; }
        [DataMember]
        public bool ShouldCharge { get; set; }
        [DataMember]
        public bool ShouldDistribute { get; set; }
        [DataMember]
        public bool ShouldSendResultsToOwner { get; set; }
        [DataMember]
        public bool ShouldUseSpecialAddressText { get; set; }
        [DataMember]
        public string Introduction { get; set; }
        [DataMember]
        public int? Category1Id { get; set; }
        [DataMember]
        public int? Category2Id { get; set; }
        [DataMember]
        public bool IsVisible { get; set; }
        [DataMember]
        public decimal Cost { get; set; }
        [DataMember]
        public int? CoverPageId { get; set; }
        [DataMember]
        public bool IsTopValue { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int CreatedByUserId { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public int LastModifiedByUserId { get; set; }

        // Generated columns.
        [DataMember]
        public int EmailAddressCount { get; set; }
        [DataMember]
        public int SMSAddressCount { get; set; }
        [DataMember]
        public int VoiceAddressCount { get; set; }
        [DataMember]
        public int WireAddressCount { get; set; }

        [DataMember]
        public bool? EmailAlert { get; set; }

        public PackageList()
        {
            System = SystemType.Medianet;
            SequenceNumber = 1;
            RowStatus = RowStatusType.Active;
            ShouldCharge = true;
            ShouldDistribute = true;
            ShouldSendResultsToOwner = false;
            ShouldUseSpecialAddressText = false;
            Introduction = string.Empty;
            IsVisible = false;
            Cost = 0;
            IsTopValue = false;
        }

        #region "Factory Methods"

        public static PackageList Create(string debtorNumber, string accountCode, string description, DistributionType transactionType)
        {
            var package = new PackageList();

            package.DebtorNumber = debtorNumber;
            package.TransactionType = transactionType;
            package.AccountCode = accountCode;
            package.SelectionDescription = description;

            return package;
        }

        #endregion
    }
}
