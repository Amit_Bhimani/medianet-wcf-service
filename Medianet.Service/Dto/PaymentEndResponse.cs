﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class PaymentEndResponse
    {
        [DataMember]
        public decimal Total { get; set; }
        [DataMember]
        public string PaymentAccessCode { get; set; }
        [DataMember]
        public int TransactionRecordId { get; set; }
        [DataMember]
        public string ResponseCode { get; set; }
        [DataMember]
        public string ResponseMessage { get; set; }
        [DataMember]
        public bool PaymentSucceeded { get; set; }
    }
}
