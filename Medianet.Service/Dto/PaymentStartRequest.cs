﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class PaymentStartRequest
    {
        [DataMember]
        public ReleaseQuoteRequest ReleaseQuote { get; set; }
        [DataMember]
        public string IPAddress { get; set; }
        [DataMember]
        public string RedirectURL { get; set; }
    }
}
