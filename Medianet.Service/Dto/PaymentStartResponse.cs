﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class PaymentStartResponse
    {
        //[DataMember]
        //public decimal ExGST { get; set; }
        //[DataMember]
        //public decimal GST { get; set; }
        [DataMember]
        public decimal Total { get; set; }
        [DataMember]
        public string PaymentAccessCode { get; set; }
        [DataMember]
        public string PaymentForm { get; set; }
        [DataMember]
        public int QuoteReferenceId { get; set; }
    }
}
