﻿using System;
using System.Runtime.Serialization;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class Plan
    {
        [DataMember]
        public string AccountingCode { get; set; }
        [DataMember]
        public DateTime? ArchivedAt { get; set; }
        [DataMember]
        public int? BillingCycles { get; set; }
        [DataMember]
        public string CurrencyCode { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int Period { get; set; }
        [DataMember]
        public string PeriodUnit { get; set; }
        [DataMember]
        public int? Price { get; set; }
        [DataMember]
        public decimal PriceInDecimal { get; set; }
        [DataMember]
        public bool? Taxable { get; set; }
        [DataMember]
        public int? TrialPeriod { get; set; }
        [DataMember]
        public DateTime? UpdatedAt { get; set; }    

    }
    [DataContract]
    public class AddOn
    {
        [DataMember]
        public string AccountingCode { get; set; }
        [DataMember]
        public DateTime? ArchivedAt { get; set; }
        [DataMember]
        public int? BillingCycles { get; set; }
        [DataMember]
        public string CurrencyCode { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int Period { get; set; }
        [DataMember]
        public int? Price { get; set; }
        [DataMember]
        public decimal PriceInDecimal { get; set; }
        [DataMember]
        public int? GST { get; set; }
        [DataMember]
        public bool? Taxable { get; set; }
        [DataMember]
        public DateTime? UpdatedAt { get; set; }

    }
}
