﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Service.Dto
{
    public class PrivateContactList
    {
        public List<MediaOmaContact> List { get; set; }

        public int ResultCount { get; set; }
    }
}
