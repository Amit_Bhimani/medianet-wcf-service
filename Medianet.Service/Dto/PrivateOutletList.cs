﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Service.Dto
{
    public class PrivateOutletList
    {
        public List<MediaOutletPartial> List { get; set; }

        public int ResultCount { get; set; }
    }
}
