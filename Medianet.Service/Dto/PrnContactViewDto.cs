﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.BusinessLayer.Common;

namespace Medianet.Service.Dto
{
    public class PrnContactViewDto
    {
        public long Id { get; set; }
        public decimal ContactId { get; set; }
        public decimal OutletId { get; set; }
        public int UserId { get; set; }
        public string IpAddress { get; set; }
        public DateTime DateViewed { get; set; }
    }
}
