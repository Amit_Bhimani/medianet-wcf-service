﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.BusinessLayer.Common;

namespace Medianet.Service.Dto
{
    public class PrnContactViewSummaryDto
    {
        public long Id { get; set; }
        public decimal ContactId { get; set; }
        public decimal OutletId { get; set; }
        public int SubjectCodeId { get; set; }
        public int ViewCount { get; set; }
    }
}
