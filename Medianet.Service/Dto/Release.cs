﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class Release
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int? ParentId { get; set; }
        [DataMember]
        public int? ChildId { get; set; }
        [DataMember]
        public string DebtorNumber { get; set; }
        [DataMember]
        public SystemType System { get; set; }
        [DataMember]
        public ReleaseStatusType Status { get; set; }
        [DataMember]
        public string ReleaseDescription { get; set; }
        [DataMember]
        public string WireDescription { get; set; }
        [DataMember]
        public string CustomerReference { get; set; }
        [DataMember]
        public string OMAReference { get; set; }
        [DataMember]
        public string BillingCode { get; set; }
        [DataMember]
        public string WieckReference { get; set; }
        [DataMember]
        public int? WieckThumbnailWidth { get; set; }
        [DataMember]
        public int? WieckThumbnailHeight { get; set; }

        [DataMember]
        public bool IsOpen { get; set; }
        [DataMember]
        public DateTime? OpenedDate { get; set; }
        [DataMember]
        public bool IsFrozen { get; set; }
        [DataMember]
        public bool IsInQueue { get; set; }
        [DataMember]
        public DateTime? HoldUntilDate { get; set; }
        [DataMember]
        public DateTime? EmbargoUntilDate { get; set; }
        [DataMember]
        public ReleasePriorityType Priority { get; set; }
        
        [DataMember]
        public int WireWords { get; set; }
        [DataMember]
        public string SpecialAddressText { get; set; }
        [DataMember]
        public string Comment { get; set; }
        [DataMember]
        public string SMSText { get; set; }

        [DataMember]
        public int? FinanceFileId { get; set; }

        [DataMember]
        public string BadTifPages { get; set; }

        [DataMember]
        public OCRConversionStatusType OCRStatus { get; set; }
        [DataMember]
        public ReceptionType ReceptionMethod { get; set; }

        // Email from details.
        [DataMember]
        public bool UseCustomerEmailAddress { get; set; }
        [DataMember]
        public string EmailFromName { get; set; }
        [DataMember]
        public string EmailFromAddress { get; set; }

        // Report details.
        [DataMember]
        public string ReportFaxAddress { get; set; }
        [DataMember]
        public string ReportEmailAddress { get; set; }
        [DataMember]
        public ResultsType ReportResultsToSend { get; set; }

        [DataMember]
        public string SecurityKey { get; set; }
        [DataMember]
        public bool ShouldDeduplicateRecipients { get; set; }
        [DataMember]
        public bool IsOnHoldIndefinitely { get; set; }
        [DataMember]
        public bool IsResend { get; set; }
        [DataMember]
        public MultimediaType MultimediaType { get; set; }
        [DataMember]
        public ReleaseStatusType NextStatus { get; set; }
        [DataMember]
        public int? QuoteReferenceId { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int? CreatedByUserId { get; set; }
        [DataMember]
        public DateTime? DistributedDate { get; set; }

        [DataMember]
        public WebRelease WebDetails { get; set; }
        [DataMember]
        public List<Document> Documents { get; set; }
        [DataMember]
        public List<Transaction> Transactions { get; set; }
        [DataMember]
        public UserSummary CreatedByUser { get; set; }
        [DataMember]
        public string TransactionId { get; set; } //chargeBeeTransactionId
        [DataMember]
        public string PaymentMethod { get; set; }
        public Release() {
            ReleaseDescription = string.Empty;
            WireDescription = string.Empty;
            CustomerReference = string.Empty;
            BillingCode = string.Empty;
            IsInQueue = true;
            SpecialAddressText = string.Empty;
            Comment = string.Empty;
            SMSText = string.Empty;
            BadTifPages = string.Empty;
            ReportFaxAddress = string.Empty;
            ReportEmailAddress = string.Empty;
            EmailFromName = string.Empty;
            EmailFromAddress = string.Empty;
            OCRStatus = OCRConversionStatusType.None;
            NextStatus = ReleaseStatusType.None;
        }

        #region Methods

        public bool HasWireTransactions() {
            return Transactions.Exists(t => t.DistributionType == DistributionType.Wire || t.TransactionType == DistributionType.Wire);
        }

        public bool IsMailMergeRelease() {
            int count = (from t in Transactions where t.IsMailMerge select t).Count();
            return (count > 0);
        }

        #endregion
    }
}
