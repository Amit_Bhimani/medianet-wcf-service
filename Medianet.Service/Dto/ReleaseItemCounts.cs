﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Service.Dto
{
    public class ReleaseItemCounts
    {
        public int WireWordCount;
        public int AttachmentCount;
        public int ImageCount;
        public int SeatCount;
    }
}
