﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class ReleasePagePublic
    {
        [DataMember]
        public int TotalCount { get; set; }
        [DataMember]
        public int Page { get; set; }
        [DataMember]
        public int PageSize { get; set; }
        [DataMember]
        public List<ReleaseSummaryPublic> Releases;
    }
}
