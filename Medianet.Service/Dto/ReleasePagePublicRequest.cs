﻿using System;
using System.Runtime.Serialization;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class ReleasePagePublicRequest
    {
        [DataMember]
        public WebsiteType Website { get; set; }
        [DataMember]
        public string SessionKey { get; set; }
        [DataMember]
        public DateTime FromDate { get; set; }
        [DataMember]
        public DateTime ToDate { get; set; }
        [DataMember]
        public int? WebCategoryId { get; set; }
        [DataMember]
        public string SearchText { get; set; }
        [DataMember]
        public MultimediaType MultimediaType { get; set; }
        [DataMember]
        public bool ShowVideos { get; set; }
        [DataMember]
        public bool ShowAudios { get; set; }
        [DataMember]
        public bool ShowPhotos { get; set; }
        [DataMember]
        public bool ShowOthers { get; set; }
        [DataMember]
        public bool ShowMultimedia { get; set; }
        [DataMember]
        public bool ShowMedianetOnly { get; set; }
        [DataMember]
        public int PageNumber { get; set; }
        [DataMember]
        public int PageSize { get; set; }
    }
}
