﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class ReleasePublic
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Headline { get; set; }
        [DataMember]
        public string Organisation { get; set; }
        [DataMember]
        public int? WebCategoryId { get; set; }
        [DataMember]
        public DateTime? EmbargoUntilDate { get; set; }
        [DataMember]
        public bool IsVisible { get; set; }
        [DataMember]
        public DateTime? DistributedDate { get; set; }
        [DataMember]
        public string ShortUrl { get; set; }
        [DataMember]
        public string HtmlContent { get; set; }
        [DataMember]
        public string Summary { get; set; }
        [DataMember]
        public MultimediaType MultimediaType { get; set; }
        [DataMember]
        public bool IsVerified { get; set; }

        [DataMember]
        public List<DocumentPublic> Videos { get; set; }
        [DataMember]
        public List<DocumentPublic> Audios { get; set; }
        [DataMember]
        public List<DocumentPublic> Images { get; set; }
        [DataMember]
        public List<DocumentPublic> Attachments { get; set; }
    }
}
