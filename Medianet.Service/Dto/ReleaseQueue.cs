﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class ReleaseQueue
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ParentId { get; set; }
        [DataMember]
        public int ChildId { get; set; }
        [DataMember]
        public string DebtorNumber { get; set; }
        [DataMember]
        public SystemType System { get; set; }
        [DataMember]
        public ReleaseStatusType Status { get; set; }

        [DataMember]
        public bool IsOpened { get; set; }
        [DataMember]
        public DateTime? OpenedDate { get; set; }
        [DataMember]
        public bool IsFrozen { get; set; }
        [DataMember]
        public bool IsInQueue { get; set; }
        [DataMember]
        public DateTime? HoldUntilDate { get; set; }
        [DataMember]
        public ReleasePriorityType Priority { get; set; }

        [DataMember]
        public OCRConversionStatusType OCRStatus { get; set; }
        [DataMember]
        public ReceptionType ReceptionMethod { get; set; }      // ReceptionType

        [DataMember]
        public DateTime CreatedDate { get; set; }               // WhenReceived
        [DataMember]
        public int CreatedByUserId { get; set; }                // SubmittedUserId
        [DataMember]
        public DateTime? DistributedDate { get; set; }          // WhenDistributed
    }
}
