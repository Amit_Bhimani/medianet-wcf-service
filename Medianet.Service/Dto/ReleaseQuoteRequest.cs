﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class ReleaseQuoteRequest
    {
        /// <summary>
        /// The DebtorNumber of the customer.
        /// </summary>
        [DataMember]
        public string DebtorNumber { get; set; }

        /// <summary>
        /// The System the Release came from.
        /// </summary>
        [DataMember]
        public SystemType System { get; set; }

        /// <summary>
        /// The description of the Release. Only needed for creditcard payments.
        /// </summary>
        [DataMember]
        public string ReleaseDescription { get; set; }

        /// <summary>
        /// The Customer reference to show on an invoice.
        /// </summary>
        [DataMember]
        public string CustomerReference { get; set; }

        /// <summary>
        /// The Billing Code used for grouping on an invoice.
        /// </summary>
        [DataMember]
        public string BillingCode { get; set; }

        /// <summary>
        /// The Priority of the Release. Either Normal or Urgent.
        /// </summary>
        [DataMember]
        public ReleasePriorityType Priority { get; set; }

        /// <summary>
        /// The MultimediaType of the Release. Either None, Audio, Multimedia or Video.
        /// </summary>
        [DataMember]
        public MultimediaType MultimediaType { get; set; }

        /// <summary>
        /// The list of transactions to be used in the quote.
        /// </summary>
        [DataMember]
        public List<TransactionSummary> Transactions { get; set; }

        [DataMember]
        public List<DocumentSummary> Documents { get; set; }
    }
}
