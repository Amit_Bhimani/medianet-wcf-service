﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class ReleaseQuoteResponse
    {
        [DataMember]
        public bool ContractExpired { get; set; }
        [DataMember]
        public string CustNum { get; set; }
        [DataMember]
        public decimal ExGST { get; set; }
        [DataMember]
        public decimal GST { get; set; }
        [DataMember]
        public decimal Total { get; set; }
        [DataMember]
        public int ReleasePages { get; set; }
        [DataMember]
        public int ReleaseWords { get; set; }
        [DataMember]
        public int QuoteReferenceId { get; set; }
        [DataMember]
        public List<TransactionQuoteResponse> Transactions { get; set; }
    }
}
