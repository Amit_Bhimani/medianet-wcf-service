﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class ReleaseSummary
    {
        [DataMember]
        public string DebtorNumber { get; set; }
        [DataMember]
        public SystemType System { get; set; }
        [DataMember]
        public string ReleaseDescription { get; set; }
        [DataMember]
        public string CustomerReference { get; set; }
        [DataMember]
        public string BillingCode { get; set; }
        [DataMember]
        public DateTime? HoldUntilDate { get; set; }
        [DataMember]
        public DateTime? EmbargoUntilDate { get; set; }
        [DataMember]
        public ReleasePriorityType Priority { get; set; }
        [DataMember]
        public string SMSText { get; set; }

        // Email from details.
        [DataMember]
        public bool UseCustomerEmailAddress { get; set; }
        [DataMember]
        public string EmailFromName { get; set; }
        [DataMember]
        public string EmailFromAddress { get; set; }

        // Report details.
        [DataMember]
        public string ReportEmailAddress { get; set; }
        [DataMember]
        public ResultsType ReportResultsToSend { get; set; }

        [DataMember]
        public MultimediaType MultimediaType { get; set; }
        [DataMember]
        public int? QuoteReferenceId { get; set; }
        [DataMember]
        public string TimezoneCode { get; set; }

        // WebRelease fields.
        [DataMember]
        public int? WebCategoryId { get; set; }
        [DataMember]
        public int? SecondaryWebCategoryId { get; set; }
        [DataMember]
        public string Organisation { get; set; }
        [DataMember]
        public bool IsVerified { get; set; }

        [DataMember]
        public List<TransactionSummary> Transactions { get; set; }
        [DataMember]
        public List<DocumentSummary> Documents { get; set; }
        [DataMember]
        public string TransactionId { get; set; } // ChargeBee
    }
}
