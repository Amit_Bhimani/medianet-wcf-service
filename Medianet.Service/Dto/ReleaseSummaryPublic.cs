﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class ReleaseSummaryPublic
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Headline { get; set; }
        [DataMember]
        public string Organisation { get; set; }
        [DataMember]
        public string Summary { get; set; }
        [DataMember]
        public int? WebCategoryId { get; set; }
        [DataMember]
        public int? SecondaryWebCategoryId { get; set; }
        [DataMember]
        public MultimediaType MultimediaType { get; set; }
        [DataMember]
        public DateTime DistributedDate { get; set; }
        [DataMember]
        public string ReleaseUrl { get; set; }
        [DataMember]
        public string SecurityKey { get; set; }
        [DataMember]
        public ThumbnailPublic Thumbnail { get; set; }
        [DataMember]
        public bool HasVideo { get; set; }
        [DataMember]
        public bool HasAudio { get; set; }
        [DataMember]
        public bool HasPhoto { get; set; }
        [DataMember]
        public bool HasOther { get; set; }
        [DataMember]
        public bool IsVerified { get; set; }
        [DataMember]
        public string DistributedDateTime { get; set; } //JournalistsDistributedDate
    }
}
