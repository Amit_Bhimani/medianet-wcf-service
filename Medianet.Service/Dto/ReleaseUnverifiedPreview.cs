﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class ReleaseUnverifiedPreview
    {
        [DataMember]
        public Guid Token { get; set; }
        [DataMember]
        public string Headline { get; set; }
        [DataMember]
        public string Organisation { get; set; }
        [DataMember]
        public int? ReleaseTempFileId { get; set; }
        [DataMember]
        public int? WhitePaperTempFileId { get; set; }
        [DataMember]
        public int? WebCategoryId { get; set; }
    }
}
