﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class Result
    {
        [DataMember]
        public int ReleaseId { get; set; }
        [DataMember]
        public int GroupNumber { get; set; }
        [DataMember]
        public int TransactionId { get; set; }
        [DataMember]
        public int? DistributionId { get; set; }
        [DataMember]
        public string Recipient { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public TransactionStatusType Status { get; set; }
        [DataMember]
        public string ErrorCode { get; set; }
        [DataMember]
        public string ErrorMessage { get; set; }
        [DataMember]
        public DateTime? TransmittedTime { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int? SendDuration { get; set; }
        [DataMember]
        public int OpenCount { get; set; }
    }
}
