﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class RssLog
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int? UserId { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int? WebCategoryId { get; set; }
        [DataMember]
        public string UserAgent { get; set; }
        [DataMember]
        public string UrlParams { get; set; }
        [DataMember]
        public string IPAddress { get; set; }   
    }
}
