﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComLib.Models;

namespace Medianet.Service.Dto
{
    public class SalesForceContact
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string AccountName { get; set; }
        public string DebtorNumber { get; set; }
        public string Position { get; set; }
        public string Phone { get; set; }
        public string RecordType { get; set; }
        public string Email { get; set; }

        public string BusinessType { get; set; }
        public bool MainContact { get; set; }
        public List<string> RelevantSubjects { get; set; }

        public string AccountOwnerName { get; set; }

        public string ABN { get; set; }

    }
}
