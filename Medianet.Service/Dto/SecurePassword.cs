﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class SecurePassword
    {
        public SecurePassword()
        {
            HashedPassword = string.Empty;
            Salt = string.Empty;           
        }

        [DataMember]
        public string HashedPassword { get; set; }
        [DataMember]
        public string Salt { get; set; }
    }
}
