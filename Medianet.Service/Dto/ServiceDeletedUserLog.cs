﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    public class ServiceDeletedUserLog
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string OutletId { get; set; }

        [DataMember]
        public string ContactId { get; set; }

        [DataMember]
        public int ServiceId { get; set; }

        [DataMember]
        public DateTime DeletedTime { get; set; }
                
        [DataMember]
        public string ServiceName { get; set; }

        [DataMember]
        public int DeletedByUserId { get; set; }
        
        [DataMember]
        public string DeletedByUser { get; set; }
    }
}
