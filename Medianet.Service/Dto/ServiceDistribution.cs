﻿using System.Runtime.Serialization;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class ServiceDistribution
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string ContactId { get; set; }

        [DataMember]
        public string Outletid { get; set; }
    }
}
