﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class ServiceList
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string DebtorNumber { get; set; }
        [DataMember]
        public SystemType System { get; set; }
        [DataMember]
        public DistributionType DistributionType { get; set; }
        [DataMember]
        public int SequenceNumber { get; set; }
        [DataMember]
        public DistributionType TransactionType { get; set; }
        [DataMember]
        public string AccountCode { get; set; }
        [DataMember]
        public string SelectionDescription { get; set; }
        [DataMember]
        public int? ListNumber { get; set; }
        [DataMember]
        public int AddressCount { get; set; }
        [DataMember]
        public bool ShouldUpdateAddresses { get; set; }
        [DataMember]
        public bool ShouldCharge { get; set; }
        [DataMember]
        public bool ShouldDistribute { get; set; }
        [DataMember]
        public bool ShouldSendResultsToOwner { get; set; }
        [DataMember]
        public bool ShouldUseSpecialAddressText { get; set; }
        [DataMember]
        public int PageAdjustment { get; set; }
        [DataMember]
        public string Introduction { get; set; }
        [DataMember]
        public string Destinations { get; set; }
        [DataMember]
        public int? CategoryId { get; set; }
        [DataMember]
        public bool IsVisible { get; set; }
        [DataMember]
        public string EditorialCategory { get; set; }
        [DataMember]
        public bool IsOnlineDB { get; set; }
        [DataMember]
        public int? CoverPageId { get; set; }
        [DataMember]
        public bool IsMediaList { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int CreatedByUserId { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public int LastModifiedByUserId { get; set; }

        [DataMember]
        public bool? EmailAlert { get; set; }

        public ServiceList()
        {
            System = SystemType.Medianet;
            SequenceNumber = 1;
            AddressCount = 1;
            ShouldUpdateAddresses = true;
            ShouldCharge = true;
            ShouldDistribute = true;
            ShouldSendResultsToOwner = false;
            ShouldUseSpecialAddressText = false;
            Introduction = string.Empty;
            Destinations = string.Empty;
            IsVisible = false;
            EditorialCategory = string.Empty;
            IsOnlineDB = false;
            IsMediaList = true;
        }

        #region "Factory Methods"

        public static ServiceList Create(string debtorNumber, string accountCode, string description, DistributionType distributionType)
        {
            var service = new ServiceList();

            service.DebtorNumber = debtorNumber;
            service.DistributionType = distributionType;
            service.TransactionType = distributionType;
            service.AccountCode = accountCode;
            service.SelectionDescription = description;

            return service;
        }

        #endregion
    }
}
