﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class ServiceListSummary
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public DistributionType DistributionType { get; set; }
        [DataMember]
        public int SequenceNumber { get; set; }
        [DataMember]
        public string SelectionDescription { get; set; }
        [DataMember]
        public int? ListNumber { get; set; }
    }
}
