﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class ServiceListSummaryPage
    {
        [DataMember]
        public int TotalCount { get; set; }
        [DataMember]
        public int StartPos { get; set; }
        [DataMember]
        public int PageSize { get; set; }
        [DataMember]
        public List<ServiceListSummary> Services;
    }
}

