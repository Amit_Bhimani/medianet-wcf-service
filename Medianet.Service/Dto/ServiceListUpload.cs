﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class ServiceListUpload
    {
        [DataMember]
        public DistributionType DistributionType { get; set; }
        [DataMember]
        public string SelectionDescription { get; set; }

        [DataMember]
        public List<Recipient> Recipients { get; set; }
    }
}
