﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class SocialInfo
    {
        [DataMember]
        public string Facebook { get; set; }
        [DataMember]
        public string LinkedIn { get; set; }
        [DataMember]
        public string Twitter { get; set; }
        [DataMember]
        public string Instagram { get; set; }
        [DataMember]
        public string Youtube { get; set; }
        [DataMember]
        public string SnapChat { get; set; }
        [DataMember]
        public string Skype { get; set; }
        [DataMember]
        public string SocialInfoName { get; set; }
        [DataMember]
        public string Name { get; set; }
    }
}
