﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class Subject
    {
        [DataMember]
        public int Id { get; set; }         // SubjectCodeId
        [DataMember]
        public string Name { get; set; }    // SubjectName
        [DataMember]
        public bool Hidden { get; set; }

        [DataMember]
        public int? subjectGroupId { get; set; }
    }
}
