﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class SubjectGroup
    {
        [DataMember]
        public int Id { get; set; }         // SubjectGroupId
        [DataMember]
        public string Name { get; set; }    // SubjectGroupName
    }
}