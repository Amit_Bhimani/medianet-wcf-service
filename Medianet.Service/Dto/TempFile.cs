﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class TempFile
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string SessionKey { get; set; }
        [DataMember]
        public int? ParentId { get; set; }
        [DataMember]
        public string FileExtension { get; set; }
        [DataMember]
        public string OriginalFilename { get; set; }
        //[DataMember]
        //public string SavedFile { get; set; }
        [DataMember]
        public bool IsWireConversion { get; set; }
        [DataMember]
        public bool IsWebConversion { get; set; }
        [DataMember]
        public bool IsMailMergeDataSource { get; set; }
        [DataMember]
        public int FileSize { get; set; }
        [DataMember]
        public int ItemCount { get; set; }
        [DataMember]
        public string ContentType { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
    }
}
