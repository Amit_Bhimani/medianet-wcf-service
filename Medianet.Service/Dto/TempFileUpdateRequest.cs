﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Medianet.Service.Dto
{
    [MessageContract]
    public class TempFileUpdateRequest
    {
        [MessageHeader(MustUnderstand = true)]
        public int Id { get; set; }
        [MessageHeader(MustUnderstand = true)]
        public string SessionKey { get; set; }
        [MessageBodyMember(Order = 1)]
        public Stream FileData { get; set; }
    }
}
