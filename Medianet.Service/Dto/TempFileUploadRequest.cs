﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Medianet.Service.Dto
{
    [MessageContract]
    public class TempFileUploadRequest
    {
        [MessageHeader(MustUnderstand = true)]
        public string SessionKey { get; set; }

        [MessageHeader(MustUnderstand = true)]
        public string FileName { get; set; }

        [MessageHeader(MustUnderstand = true)]
        public bool ConvertToWire { get; set; }

        //[MessageHeader(MustUnderstand = true)]
        //public bool ConvertToWeb { get; set; }

        [MessageHeader(MustUnderstand = true)]
        public string ContentType { get; set; }

        [MessageBodyMember(Order=1)]
        public Stream FileData { get; set; }
    }
}
