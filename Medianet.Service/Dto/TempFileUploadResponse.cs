﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Medianet.Service.Dto
{
    [MessageContract]
    public class TempFileUploadResponse
    {
        [MessageHeader(MustUnderstand = true)]
        public int TempFileId { get; set; }

        [MessageHeader(MustUnderstand = true)]
        public int PageCount { get; set; }

        [MessageHeader(MustUnderstand = true)]
        public int WordCount { get; set; }
    }
}
