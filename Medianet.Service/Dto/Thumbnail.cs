﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class Thumbnail
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int DocumentId { get; set; }
        [DataMember]
        public string FileExtension { get; set; }
        [DataMember]
        public int Width { get; set; }
        [DataMember]
        public int Height { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public Document Document { get; set; }
    }
}
