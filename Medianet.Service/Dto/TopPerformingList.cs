﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class TopPerformingList
    {
        [DataMember]
        public PeriodType Period { get; set; }
        [DataMember]
        public int ServiceId { get; set; }
        [DataMember]
        public string SelectionDescription { get; set; }
        [DataMember]
        public int LastPeriodTotal { get; set; }
        [DataMember]
        public int PreviousPeriodTotal { get; set; }
        [DataMember]
        public decimal PercentageImprovement { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
    }
}
