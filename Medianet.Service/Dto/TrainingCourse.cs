﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class TrainingCourse
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Summary { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Sector { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        [DataMember]
        public bool IsFeatured { get; set; }
        [DataMember]
        public int? PackageId { get; set; }
        [DataMember]
        public EventType EventType { get; set; }

        [DataMember]
        public List<TrainingCourseSchedule> Schedules { get; set; }
        [DataMember]
        public List<TrainingCourseTopic> Topics { get; set; }
        [DataMember]
        public List<TrainingCoursePromoCode> PromoCodes { get; set; }
        [DataMember]
        public TrainingCourseContact Contact { get; set; }
    }
}
