﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class TrainingCourseEnrolment
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string Company { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string Fax { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string ABN { get; set; }
        [DataMember]
        public string ReceiptId { get; set; }
        [DataMember]
        public int ScheduleId { get; set; }
        [DataMember]
        public int NumberOfAttendees { get; set; }
        [DataMember]
        public RowStatusType RowStatus { get; set; }
        [DataMember]
        public decimal TotalCost { get; set; }
    }
}
