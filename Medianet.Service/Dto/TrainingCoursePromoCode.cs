﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class TrainingCoursePromoCode
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string PromoCode { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        [DataMember]
        public int PackageId { get; set; }
        [DataMember]
        public string TrainingCourseId { get; set; }
    }
}
