﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class TrainingCourseSchedule
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public DateTime CourseDate { get; set; }
        [DataMember]
        public string CourseTime { get; set; }
        [DataMember]
        public string LocationId { get; set; }
        [DataMember]
        public int Seats { get; set; }
        [DataMember]
        public string Agenda { get; set; }
        [DataMember]
        public string TrainingCourseId { get; set; }
        [DataMember]
        public string FormHandler { get; set; }

        [DataMember]
        public List<TrainingCourseEnrolment> Enrolments { get; set; }
        [DataMember]
        public TrainingCourseLocation Location { get; set; }
    }
}
