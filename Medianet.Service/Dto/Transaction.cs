﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;
using Medianet.Service.BusinessLayer.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class Transaction
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ReleaseId { get; set; }
        [DataMember]
        public int? ParentTransactionId { get; set; }
        [DataMember]
        public int SequenceNumber { get; set; }
        [DataMember]
        public int GroupNumber { get; set; }
        [DataMember]
        public int? PackageId { get; set; }
        [DataMember]
        public int? ServiceId { get; set; }
        [DataMember]
        public string DebtorNumber { get; set; }
        [DataMember]
        public SystemType System { get; set; }
        [DataMember]
        public DistributionType DistributionType { get; set; }
        [DataMember]
        public bool IsSingleRecipient { get; set; }
        [DataMember]
        public bool IsMailMerge { get; set; }
        [DataMember]
        public TransactionStatusType Status { get; set; }
        [DataMember]
        public DistributionType TransactionType { get; set; }
        [DataMember]
        public string AccountCode { get; set; }
        [DataMember]
        public string SelectionDescription { get; set; }
        [DataMember]
        public string PackageDescription { get; set; }
        [DataMember]
        public int? ListNumber { get; set; }
        [DataMember]
        public int AddressCount { get; set; }
        [DataMember]
        public bool ShouldCharge { get; set; }
        [DataMember]
        public bool ShouldDistribute { get; set; }
        [DataMember]
        public DistributionSystemType DistributionSystem { get; set; }
        [DataMember]
        public bool ShouldSendResultsToOwner { get; set; }
        [DataMember]
        public bool ShouldUseSpecialAddressText { get; set; }
        [DataMember]
        public int PageAdjustment { get; set; }
        [DataMember]
        public string Introduction { get; set; }
        [DataMember]
        public string SingleRecipientAddress { get; set; }
        [DataMember]
        public string Destinations { get; set; }
        [DataMember]
        public string EditorialCategory { get; set; }
        [DataMember]
        public int? CoverPageId { get; set; }
        [DataMember]
        public string TweetText { get; set; }
        [DataMember]
        public int? TwitterCategoryId { get; set; }
        [DataMember]
        public string TwitterId { get; set; }
        [DataMember]
        public bool IsMediaList { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int CreatedByUserId { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public int LastModifiedByUserId { get; set; }

        [DataMember]
        public bool? EmailAlert { get; set; }

        public Transaction() {
            DebtorNumber = string.Empty;
            AccountCode = string.Empty;
            SelectionDescription = string.Empty;
            PackageDescription = string.Empty;
            Introduction = string.Empty;
            SingleRecipientAddress = string.Empty;
            Destinations = string.Empty;
            TweetText = string.Empty;
            TwitterId = string.Empty;
            IsMediaList = false;
        }

        #region "Factory Methods"

        public static Transaction CreateServiceTransaction(ServiceList list, int sequenceNo, int userId) {
            Transaction t = new Transaction();

            t.SequenceNumber = sequenceNo;
            t.GroupNumber = 0;
            t.PackageId = null;
            t.ServiceId = list.Id;
            t.DebtorNumber = list.DebtorNumber;
            t.System = list.System;
            t.DistributionType = list.DistributionType;
            t.IsSingleRecipient = false;
            t.IsMailMerge = false;
            t.Status = TransactionStatusType.New;
            t.TransactionType = list.TransactionType;
            t.AccountCode = list.AccountCode;
            t.SelectionDescription = list.SelectionDescription;
            t.PackageDescription = string.Empty;
            t.ListNumber = list.ListNumber;
            t.AddressCount = list.AddressCount;
            t.ShouldCharge = list.ShouldCharge;
            t.ShouldDistribute = list.ShouldDistribute;
            t.DistributionSystem = DistributionSystemType.Undefined;
            t.ShouldSendResultsToOwner = list.ShouldSendResultsToOwner;
            t.ShouldUseSpecialAddressText = list.ShouldUseSpecialAddressText;
            t.PageAdjustment = list.PageAdjustment;
            t.Introduction = list.Introduction;
            t.SingleRecipientAddress = string.Empty;
            t.Destinations = list.Destinations;
            t.EditorialCategory = list.EditorialCategory;
            t.CoverPageId = list.CoverPageId;
            t.IsMediaList = list.IsMediaList;
            t.CreatedDate = DateTime.Now;
            t.CreatedByUserId = userId;
            t.LastModifiedDate = t.CreatedDate;
            t.LastModifiedByUserId = userId;
            t.EmailAlert = list.EmailAlert;

            return t;
        }

        public static Transaction CreatePackageTransaction(PackageList list, ServiceList service, int sequenceNo, int userId) {
            Transaction t = new Transaction();

            t.SequenceNumber = sequenceNo;
            t.PackageId = list.Id;
            t.ServiceId = service.Id;
            t.DebtorNumber = list.DebtorNumber;
            t.System = list.System;
            t.DistributionType = service.DistributionType;
            t.IsSingleRecipient = false;
            t.IsMailMerge = false;
            t.Status = TransactionStatusType.New;
            t.TransactionType = list.TransactionType;
            t.AccountCode = list.AccountCode;
            t.SelectionDescription = service.SelectionDescription;
            t.PackageDescription = list.SelectionDescription;
            t.ListNumber = list.ListNumber;
            t.AddressCount = 1;
            t.ShouldCharge = list.ShouldCharge;
            t.ShouldDistribute = list.ShouldDistribute;
            t.DistributionSystem = DistributionSystemType.Undefined;
            t.ShouldSendResultsToOwner = list.ShouldSendResultsToOwner;
            t.ShouldUseSpecialAddressText = list.ShouldUseSpecialAddressText;
            t.PageAdjustment = 0;
            t.Introduction = list.Introduction;
            t.SingleRecipientAddress = string.Empty;
            t.Destinations = service.Destinations;
            t.EditorialCategory = service.EditorialCategory;
            t.CoverPageId = service.CoverPageId;
            t.IsMediaList = service.IsMediaList;
            t.CreatedDate = DateTime.Now;
            t.CreatedByUserId = userId;
            t.LastModifiedDate = t.CreatedDate;
            t.LastModifiedByUserId = userId;
            t.EmailAlert = list.EmailAlert;

            return t;
        }

        public static Transaction CreateSingleAddressTransaction(SystemType system, DistributionType pDistributionType, string pAddress, int sequenceNo, string debtorNumber, int userId) {
            Transaction t = new Transaction();

            t.SequenceNumber = sequenceNo;
            t.PackageId = null;
            t.ServiceId = null;
            t.DebtorNumber = debtorNumber;
            t.System = system;
            t.DistributionType = pDistributionType;
            t.IsSingleRecipient = true;
            t.IsMailMerge = false;
            t.Status = TransactionStatusType.New;
            t.TransactionType = pDistributionType;
            t.PackageDescription = string.Empty;
            t.ListNumber = null;
            t.AddressCount = 1;
            t.ShouldCharge = true;
            t.ShouldDistribute = true;
            t.DistributionSystem = DistributionSystemType.Undefined;
            t.ShouldSendResultsToOwner = true;
            t.ShouldUseSpecialAddressText = false;
            t.PageAdjustment = 0;
            t.Introduction = string.Empty;
            t.SingleRecipientAddress = pAddress.Truncate(100);
            t.Destinations = string.Empty;
            t.EditorialCategory = string.Empty;
            t.IsMediaList = true;
            t.CoverPageId = null;
            t.CreatedDate = DateTime.Now;
            t.CreatedByUserId = userId;
            t.LastModifiedDate = t.CreatedDate;
            t.LastModifiedByUserId = userId;

            if (pDistributionType == DistributionType.SMS) {
                t.AccountCode = Constants.ACCOUNTCODE_MNET_CUSTSMS;
                t.SelectionDescription = "SMS : " + pAddress;
            }
            else if (pDistributionType == DistributionType.Voice) {
                t.AccountCode = Constants.ACCOUNTCODE_MNET_CUSTVOICE;
                t.SelectionDescription = "VOICE : " + pAddress;
            }
            else {
                t.AccountCode = Constants.ACCOUNTCODE_MNET_CUSTEMAIL;
                t.SelectionDescription = "EMAIL : " + pAddress;
            }

            t.SelectionDescription = t.SelectionDescription.Truncate(50);

            return t;
        }

        public static Transaction CreateMailmergeTransaction(SystemType system, DistributionType pDistributionType, int sequenceNo, string debtorNumber, int addresses, int userId) {
            Transaction t = new Transaction();

            t.SequenceNumber = sequenceNo;
            t.PackageId = null;
            t.ServiceId = null;
            t.DebtorNumber = debtorNumber;
            t.System = system;
            t.DistributionType = pDistributionType;
            t.IsSingleRecipient = false;
            t.IsMailMerge = true;
            t.Status = TransactionStatusType.New;
            t.TransactionType = pDistributionType;
            t.PackageDescription = string.Empty;
            t.ListNumber = null;
            t.AddressCount = addresses;
            t.ShouldCharge = true;
            t.ShouldDistribute = true;
            t.DistributionSystem = DistributionSystemType.Undefined;
            t.ShouldSendResultsToOwner = true;
            t.ShouldUseSpecialAddressText = false;
            t.PageAdjustment = 0;
            t.Introduction = string.Empty;
            t.SingleRecipientAddress = string.Empty;
            t.Destinations = string.Empty;
            t.EditorialCategory = string.Empty;
            t.CoverPageId = null;
            t.CreatedDate = DateTime.Now;
            t.CreatedByUserId = userId;
            t.LastModifiedDate = t.CreatedDate;
            t.LastModifiedByUserId = userId;

            if (pDistributionType == DistributionType.SMS) {
                t.AccountCode = Constants.ACCOUNTCODE_MNET_CUSTSMS;
                t.SelectionDescription = "MailMerge SMS";
            }
            else {
                t.AccountCode = Constants.ACCOUNTCODE_MNET_CUSTEMAIL;
                t.SelectionDescription = "MailMerge Email";
            }

            return t;
        }

        public static Transaction CreateAttachmentFeeTransaction(SystemType system, int sequenceNo, int userId)
        {
            Transaction t = new Transaction();

            t.SequenceNumber = sequenceNo;
            t.GroupNumber = 0;
            t.PackageId = null;
            t.ServiceId = null;
            t.DebtorNumber = Constants.DEBTOR_NUMBER_AAP;
            t.System = system;
            t.DistributionType = DistributionType.ChargeOnly;
            t.IsSingleRecipient = false;
            t.IsMailMerge = false;
            t.Status = TransactionStatusType.New;
            t.TransactionType = t.DistributionType;
            t.AccountCode = Constants.ACCOUNTCODE_ATTACHMENT_FEE;
            t.SelectionDescription = "Attachment Charge";
            t.PackageDescription = string.Empty;
            t.ListNumber = null;
            t.AddressCount = 1;
            t.ShouldCharge = true;
            t.ShouldDistribute = true;
            t.DistributionSystem = DistributionSystemType.Undefined;
            t.ShouldSendResultsToOwner = false;
            t.ShouldUseSpecialAddressText = false;
            t.PageAdjustment = 0;
            t.Introduction = string.Empty;
            t.SingleRecipientAddress = string.Empty;
            t.Destinations = string.Empty;
            t.EditorialCategory = string.Empty;
            t.CoverPageId = null;
            t.CreatedDate = DateTime.Now;
            t.CreatedByUserId = userId;
            t.LastModifiedDate = t.CreatedDate;
            t.LastModifiedByUserId = userId;

            return t;
        }

        public static Transaction CreateAnrFeeTransaction(SystemType system, int sequenceNo, int userId)
        {
            Transaction t = new Transaction();

            t.SequenceNumber = sequenceNo;
            t.GroupNumber = 0;
            t.PackageId = null;
            t.ServiceId = null;
            t.DebtorNumber = Constants.DEBTOR_NUMBER_AAP;
            t.System = system;
            t.DistributionType = DistributionType.ChargeOnly;
            t.IsSingleRecipient = false;
            t.IsMailMerge = false;
            t.Status = TransactionStatusType.New;
            t.TransactionType = t.DistributionType;
            t.AccountCode = Constants.ACCOUNTCODE_ANR_FEE;
            t.SelectionDescription = "Audio News Release Charge";
            t.PackageDescription = string.Empty;
            t.ListNumber = null;
            t.AddressCount = 1;
            t.ShouldCharge = true;
            t.ShouldDistribute = true;
            t.DistributionSystem = DistributionSystemType.Undefined;
            t.ShouldSendResultsToOwner = false;
            t.ShouldUseSpecialAddressText = false;
            t.PageAdjustment = 0;
            t.Introduction = string.Empty;
            t.SingleRecipientAddress = string.Empty;
            t.Destinations = string.Empty;
            t.EditorialCategory = string.Empty;
            t.CoverPageId = null;
            t.CreatedDate = DateTime.Now;
            t.CreatedByUserId = userId;
            t.LastModifiedDate = t.CreatedDate;
            t.LastModifiedByUserId = userId;

            return t;
        }

        public static Transaction CreateSecondaryWebCategoryTransaction(SystemType system, int sequenceNo, int userId)
        {
            Transaction t = new Transaction();

            t.SequenceNumber = sequenceNo;
            t.GroupNumber = 0;
            t.PackageId = null;
            t.ServiceId = null;
            t.DebtorNumber = Constants.DEBTOR_NUMBER_AAP;
            t.System = system;
            t.DistributionType = DistributionType.ChargeOnly;
            t.IsSingleRecipient = false;
            t.IsMailMerge = false;
            t.Status = TransactionStatusType.New;
            t.TransactionType = t.DistributionType;
            t.AccountCode = Constants.ACCOUNTCODE_SECWEBCAT_FEE;
            t.SelectionDescription = "Additional Web Category Charge";
            t.PackageDescription = string.Empty;
            t.ListNumber = null;
            t.AddressCount = 1;
            t.ShouldCharge = true;
            t.ShouldDistribute = true;
            t.DistributionSystem = DistributionSystemType.Undefined;
            t.ShouldSendResultsToOwner = false;
            t.ShouldUseSpecialAddressText = false;
            t.PageAdjustment = 0;
            t.Introduction = string.Empty;
            t.SingleRecipientAddress = string.Empty;
            t.Destinations = string.Empty;
            t.EditorialCategory = string.Empty;
            t.CoverPageId = null;
            t.CreatedDate = DateTime.Now;
            t.CreatedByUserId = userId;
            t.LastModifiedDate = t.CreatedDate;
            t.LastModifiedByUserId = userId;

            return t;
        }

        public static Transaction CreateHighPriorityFeeTransaction(SystemType system, int sequenceNo, int userId)
        {
            Transaction t = new Transaction();

            t.SequenceNumber = sequenceNo;
            t.GroupNumber = 0;
            t.PackageId = null;
            t.ServiceId = null;
            t.DebtorNumber = Constants.DEBTOR_NUMBER_AAP;
            t.System = system;
            t.DistributionType = DistributionType.ChargeOnly;
            t.IsSingleRecipient = false;
            t.IsMailMerge = false;
            t.Status = TransactionStatusType.New;
            t.TransactionType = t.DistributionType;
            t.AccountCode = Constants.ACCOUNTCODE_PRIORITY_FEE;
            t.SelectionDescription = "High Priority Charge";
            t.PackageDescription = string.Empty;
            t.ListNumber = null;
            t.AddressCount = 1;
            t.ShouldCharge = true;
            t.ShouldDistribute = true;
            t.DistributionSystem = DistributionSystemType.Undefined;
            t.ShouldSendResultsToOwner = false;
            t.ShouldUseSpecialAddressText = false;
            t.PageAdjustment = 0;
            t.Introduction = string.Empty;
            t.SingleRecipientAddress = string.Empty;
            t.Destinations = string.Empty;
            t.EditorialCategory = string.Empty;
            t.CoverPageId = null;
            t.CreatedDate = DateTime.Now;
            t.CreatedByUserId = userId;
            t.LastModifiedDate = t.CreatedDate;
            t.LastModifiedByUserId = userId;

            return t;
        }

        #endregion
    }
}
