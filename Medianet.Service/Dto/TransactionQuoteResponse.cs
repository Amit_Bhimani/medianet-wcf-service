﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class TransactionQuoteResponse
    {
        [DataMember]
        public int SequenceNumber { get; set; }
        [DataMember]
        public int Discount { get; set; }
        [DataMember]
        public decimal GST { get; set; }
        [DataMember]
        public bool IsContractItem { get; set; }
        [DataMember]
        public bool IsSubscription { get; set; }
        [DataMember]
        public string AccountCode { get; set; }
        [DataMember]
        public double PrioritySurcharge { get; set; }
        [DataMember]
        public int Quantity { get; set; }
        [DataMember]
        public decimal SubTotalEx { get; set; }
        [DataMember]
        public double UnitPrice { get; set; }
        [DataMember]
        public int intPrice { get; set; }
        [DataMember]
        public int VolumeLimit { get; set; }
        [DataMember]
        public int VolumeUsed { get; set; }
        [DataMember]
        public string Reference { get; set; }
    }
}
