﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class TransactionStatistics
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int SequenceNumber { get; set; }
        [DataMember]
        public string DebtorNumber { get; set; }
        [DataMember]
        public DistributionType DistributionType { get; set; }
        [DataMember]
        public TransactionStatusType Status { get; set; }
        [DataMember]
        public bool IsSingleRecipient { get; set; }
        [DataMember]
        public string SelectionDescription { get; set; }
        [DataMember]
        public int? ListNumber { get; set; }
        [DataMember]
        public bool ShouldSendResultsToOwner { get; set; }

        [DataMember]
        public int TotalInProgress { get; set; }
        [DataMember]
        public int TotalSentOk { get; set; }
        [DataMember]
        public int TotalFailed { get; set; }
        [DataMember]
        public int TotalOpened { get; set; }
        [DataMember]
        public int TotalUnopened { get; set; }
    }
}
