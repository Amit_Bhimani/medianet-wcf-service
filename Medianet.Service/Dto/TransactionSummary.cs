﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class TransactionSummary
    {
        [DataMember]
        public int SequenceNumber { get; set; }
        [DataMember]
        public int? PackageId { get; set; }
        [DataMember]
        public int? ServiceId { get; set; }
        [DataMember]
        public DistributionType DistributionType { get; set; }
        [DataMember]
        public bool IsSingleRecipient { get; set; }
        [DataMember]
        public bool IsMailMerge { get; set; }
        [DataMember]
        public bool IsAttachmentFee { get; set; }
        [DataMember]
        public bool IsAnrFee { get; set; }
        [DataMember]
        public string SingleRecipientAddress { get; set; }
        [DataMember]
        public int TwitterCategoryId { get; set; }
        [DataMember]
        public string TwitterText { get; set; }
        [DataMember]
        public bool IsSecondaryWebCategory { get; set; }
        [DataMember]
        public bool IsHighPriorityFee { get; set; }
    }
}
