﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class TwitterCategory
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Handle { get; set; }
        [DataMember]
        public int SequenceNumber { get; set; }
        [DataMember]
        public bool IsVisible { get; set; }
        [DataMember]
        public bool IsDeleted { get; set; }
    }


    [DataContract]
    public class CasTwitterCategory
    {
        [DataMember]
        public string AccessToken { get; set; }

        [DataMember]
        public string AccessTokenSecret { get; set; }

        [DataMember]
        public string ConsumerKey { get; set; }

        [DataMember]
        public string ConsumerKeySecret { get; set; }
    }
}
