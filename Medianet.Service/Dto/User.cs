﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class User
    {
        public User()
        {
            LogonName = string.Empty;
            HashedPassword = string.Empty;
            Salt = string.Empty;
            DebtorNumber = string.Empty;
            ShortName = string.Empty;
            IVRPin = string.Empty;
            DefaultBillingRef = string.Empty;
            DefaultJobPriority = ReleasePriorityType.Standard;
            ShouldDeduplicateJobs = false;
            MustChangePassword = false;
            FirstName = string.Empty;
            LastName = string.Empty;
            Position = string.Empty;
            EmailAddress = string.Empty;
            TelephoneNumber = string.Empty;
            FaxNumber = string.Empty;
            ReportSendMethod = ReportType.Email;
            ReportResultsToSend = ResultsType.All;
            MNUserAccessRights = AdminAccessType.None;
            HasDistributeWebAccess = false;
            HasMessageConnectWebAccess = false;
            MessageConnectAccessRights = MessageConnectAccessType.None;
            HasJournalistsWebAccess = false;
            HasAdminWebAccess = false;
            HasContactsWebAccess = false;
            ContactsAccessRights = ContactsAccessType.None;
            HasContactsReportAccess = false;
            HasReleaseWatchAccess = false;
            CanSendFax = true;
            CanSendEmail = true;
            CanSendSMS = true;
            CanSendVoice = true;
            CanSendMailMerge = true;
            RowStatus = RowStatusType.Active;
            ExpiryToken = Guid.NewGuid();
            ReleaseCount = 0;
            RecentLogonCount = 0;
        }

        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string LogonName { get; set; }
        [DataMember]
        public string HashedPassword { get; set; }
        [DataMember]
        public string Salt { get; set; }
        [DataMember]
        public string DebtorNumber { get; set; }
        [DataMember]
        public string BillerDebtorNumber { get; set; }
        [DataMember]
        public string OutletID { get; set; }
        [DataMember]
        public string ContactID { get; set; }
        [DataMember]
        public string ShortName { get; set; }
        [DataMember]
        public string IVRPin { get; set; }
        [DataMember]
        public string DefaultBillingRef { get; set; }
        [DataMember]
        public ReleasePriorityType DefaultJobPriority { get; set; }
        [DataMember]
        public bool ShouldDeduplicateJobs { get; set; }
        [DataMember]
        public bool MustChangePassword { get; set; }

        // Contact details.
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string Position { get; set; }
        [DataMember]
        public string EmailAddress { get; set; }
        [DataMember]
        public string TelephoneNumber { get; set; }
        [DataMember]
        public string FaxNumber { get; set; }

        [DataMember]
        public string IndustryCode { get; set; }

        // Monitoring Search Access rights
        [DataMember]
        public bool MonitorInternet { get; set; }
        [DataMember]
        public bool MonitorText { get; set; }
        [DataMember]
        public bool MonitorWire { get; set; }

        // Report details.
        [DataMember]
        public ReportType ReportSendMethod { get; set; }
        [DataMember]
        public ResultsType ReportResultsToSend { get; set; }

        // Access rights.
        [DataMember]
        public AdminAccessType MNUserAccessRights { get; set; }
        [DataMember]
        public bool HasDistributeWebAccess { get; set; }
        [DataMember]
        public bool HasMessageConnectWebAccess { get; set; }
        [DataMember]
        public MessageConnectAccessType MessageConnectAccessRights { get; set; }
        [DataMember]
        public bool HasJournalistsWebAccess { get; set; }
        [DataMember]
        public bool HasAdminWebAccess { get; set; }
        [DataMember]
        public bool HasContactsWebAccess { get; set; }
        [DataMember]
        public ContactsAccessType ContactsAccessRights { get; set; }
        [DataMember]
        public bool HasContactsReportAccess { get; set; }
        [DataMember]
        public bool HasReleaseWatchAccess { get; set; }
        
        [DataMember]
        public Guid ExpiryToken { get; set; }
        [DataMember]
        public int ReleaseCount { get; set; }

        [DataMember]
        public bool CanSendFax { get; set; }
        [DataMember]
        public bool CanSendEmail { get; set; }
        [DataMember]
        public bool CanSendSMS { get; set; }
        [DataMember]
        public bool CanSendVoice { get; set; }
        [DataMember]
        public bool CanSendMailMerge { get; set; }
        [DataMember]
        public bool HasViewedDistributionMessage { get; set; }

        // Logon history.
        [DataMember]
        public DateTime? LastLogonDate { get; set; }
        [DataMember]
        public int LogonFailureCount { get; set; }
        [DataMember]
        public int RecentLogonCount { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int CreatedByUserId { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public int LastModifiedByUserId { get; set; }
        [DataMember]
        public RowStatusType RowStatus { get; set; }

        [DataMember]
        public Guid UnsubscribeKey { get; set; }
        [DataMember]
        public string RelevantSubjects { get; set; }
        
        public void Truncate() {
            LogonName = LogonName.Truncate(16);
            ShortName = ShortName.Truncate(4);
            IVRPin = IVRPin.Truncate(10);
            DefaultBillingRef = DefaultBillingRef.Truncate(16);
            FirstName = FirstName.Truncate(50);
            LastName = LastName.Truncate(50);
            Position = Position.Truncate(50);
            EmailAddress = EmailAddress.Truncate(200);
            TelephoneNumber = TelephoneNumber.Truncate(24);
            FaxNumber = FaxNumber.Truncate(24);
        }
    }
}
