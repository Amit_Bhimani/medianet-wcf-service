﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class WebCategory
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int? ParentId { get; set; }
        [DataMember]
        public int SequenceNumber { get; set; }
        [DataMember]
        public bool IsHighPriority { get; set; }
        [DataMember]
        public bool IsVisible { get; set; }
        [DataMember]
        public bool IsDeleted { get; set; }
    }
}
