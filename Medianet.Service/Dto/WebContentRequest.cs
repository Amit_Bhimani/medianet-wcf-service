﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Medianet.Service.Dto
{
    [MessageContract]
    public class WebContentRequest
    {
        [MessageHeader(MustUnderstand = true)]
        public int WebsiteId { get; set; }
    }
}
