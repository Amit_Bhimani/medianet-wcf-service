﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class WebRelease
    {
        public WebRelease() {
            Headline = string.Empty;
            Organisation = string.Empty;
            ShouldShowOnPublic = true;
            ShouldShowOnJournalists = true;
            WasSentToJournalists = false;
            IsWidgetUpdated = false;
            ShortUrl = string.Empty;
            Summary = string.Empty;
            HasVideo = false;
            HasAudio = false;
            HasPhoto = false;
            HasOther = false;
            MultimediaType = Common.MultimediaType.None;
            IsVerified = false;
        }

        [DataMember]
        public int ReleaseId { get; set; }
        [DataMember]
        public string Headline { get; set; }
        [DataMember]
        public string Organisation { get; set; }
        [DataMember]
        public int? PrimaryWebCategoryId { get; set; }
        [DataMember]
        public int? SecondaryWebCategoryId { get; set; }
        [DataMember]
        public bool ShouldShowOnPublic { get; set; }
        [DataMember]
        public bool ShouldShowOnJournalists { get; set; }
        [DataMember]
        public DateTime? PublicDistributedDate { get; set; }
        [DataMember]
        public DateTime? JournalistsDistributedDate { get; set; }
        [DataMember]
        public bool WasSentToJournalists { get; set; }
        [DataMember]
        public bool IsWidgetUpdated { get; set; }   
        [DataMember]
        public string ShortUrl { get; set; }
        [DataMember]
        public string Summary { get; set; }
        [DataMember]
        public bool HasVideo { get; set; }
        [DataMember]
        public bool HasAudio { get; set; }
        [DataMember]
        public bool HasPhoto { get; set; }
        [DataMember]
        public bool HasOther { get; set; }
        [DataMember]
        public MultimediaType MultimediaType { get; set; }
        [DataMember]
        public bool IsVerified { get; set; }
    }
}
