﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medianet.Service.Common;

namespace Medianet.Service.Dto
{
    [DataContract]
    public class Website
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string System { get; set; }
        [DataMember]
        public byte[] Logo { get; set; }
        [DataMember]
        public string LogoFilename { get; set; }
        [DataMember]
        public string CompanyName { get; set; }
        [DataMember]
        public string StartingURI { get; set; }
        [DataMember]
        public string Language { get; set; }
        [DataMember]
        public int MaxCrawlDepth { get; set; }
        [DataMember]
        public int MaxPageFetches { get; set; }
        [DataMember]
        public int VisitorsPerDay { get; set; }
        [DataMember]
        public bool ShouldIgnoreRobotsFile { get; set; }
        [DataMember]
        public bool IsMNJWebsite { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public string DestinationCode { get; set; }
        [DataMember]
        public string HomePage { get; set; }
        [DataMember]
        public string Schedule { get; set; }
        [DataMember]
        public int CrawlMethodId { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int CreatedByUserId { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public int LastModifiedByUserId { get; set; }
    }
}
