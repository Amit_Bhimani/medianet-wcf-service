﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Medianet.Service.FaultExceptions
{
    [DataContract]
    public class AccessDeniedFault
    {
        public const string FaultDescription = "Access denied.";

        [DataMember]
        public string Message { get; set; }

        public AccessDeniedFault() {
            Message = FaultDescription;
        }
    }
}
