﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Medianet.Service.Dto;

namespace Medianet.Service.FaultExceptions
{
    [DataContract]
    public class AgreeToTermsFault : DoNotLogFault
    {
        public const string FaultDescription = "You must agree to the terms and conditions to continue.";

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string EmailAddress { get; set; }

        public AgreeToTermsFault()
        {
            Message = FaultDescription;
        }
    }
}
