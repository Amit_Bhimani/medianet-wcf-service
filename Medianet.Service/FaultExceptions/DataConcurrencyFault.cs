﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Medianet.Service.FaultExceptions
{
    [DataContract]
    public class DataConcurrencyFault
    {
        public const string FaultDescription = "Data has been modified by another user. Please try again.";

        [DataMember]
        public string Message { get; set; }

        public DataConcurrencyFault()
        {
            Message = FaultDescription;
        }
    }
}
