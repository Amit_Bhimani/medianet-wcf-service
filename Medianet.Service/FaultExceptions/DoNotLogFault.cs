﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Medianet.Service.Dto;

namespace Medianet.Service.FaultExceptions
{
    // Classes inherited from this should not get logged.
    [DataContract]
    public abstract class DoNotLogFault
    {
    }
}
