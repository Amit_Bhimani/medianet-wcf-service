﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Medianet.Service.FaultExceptions
{
    [DataContract]
    public class DuplicateNameFault
    {
        public const string FaultDescription = "There is another record with the same name in the system";

        [DataMember]
        public string Message { get; set; }

        public DuplicateNameFault()
        {
            Message = FaultDescription;
        }
    }
}
