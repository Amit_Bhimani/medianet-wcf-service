﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Medianet.Service.FaultExceptions
{
    [DataContract]
    public class FileNotFoundFault
    {
        public const string FaultDescription = "File not found.";

        [DataMember]
        public string Message { get; set; }

        public FileNotFoundFault() {
            Message = FaultDescription;
        }
    }
}
