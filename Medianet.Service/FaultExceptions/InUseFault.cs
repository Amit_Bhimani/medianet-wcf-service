﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Medianet.Service.FaultExceptions
{
    [DataContract]
    public class InUseFault
    {
        public const string FaultDescription = "Item in use.";

        [DataMember]
        public string Message { get; set; }

        public InUseFault() {
            Message = FaultDescription;
        }
    }
}