﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Medianet.Service.FaultExceptions
{
    [DataContract]
    public class InvalidDataSourceFault
    {
        public const string FaultDescription = "Invalid data source.";

        [DataMember]
        public string Message { get; set; }

        public InvalidDataSourceFault() {
            Message = FaultDescription;
        }
    }
}
