﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Medianet.Service.FaultExceptions
{
    [DataContract]
    public class InvalidLoginFault : DoNotLogFault
    {
        public const string FaultDescription = "Invalid login details.";
        public const string FaultDescriptionDuplicateEmail = "More than one user with this email address.";

        [DataMember]
        public string Message { get; set; }

        public InvalidLoginFault() {
            Message = FaultDescription;
        }
    }
}
