﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Medianet.Service.FaultExceptions
{
    [DataContract]
    public class InvalidParameterFault
    {
        public const string FaultDescription = "Invalid parameter.";

        [DataMember]
        public string Message { get; set; }

        public InvalidParameterFault() {
            Message = FaultDescription;
        }
    }
}