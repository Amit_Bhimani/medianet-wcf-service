﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Medianet.Service.FaultExceptions
{
    [DataContract]
    public class InvalidSessionFault
    {
        public const string FaultDescription = "Invalid session or session expired.";

        [DataMember]
        public string Message { get; set; }

        public InvalidSessionFault() {
            Message = FaultDescription;
        }
    }
}
