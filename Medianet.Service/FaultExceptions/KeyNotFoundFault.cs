﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Medianet.Service.FaultExceptions
{
    [DataContract]
    public class KeyNotFoundFault
    {
        public const string FaultDescription = "Item not found.";

        [DataMember]
        public string Message { get; set; }

        public KeyNotFoundFault() {
            Message = FaultDescription;
        }
    }
}
