﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Medianet.Service.Dto;

namespace Medianet.Service.FaultExceptions
{
    [DataContract]
    public class LogonLicensesExceededFault : DoNotLogFault
    {
        public const string FaultDescription = "The maximum number of licences for this product has been exceeded.";

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public int LicenseCount { get; set; }

        [DataMember]
        public List<DBSession> ActiveSessions { get; set; }

        public LogonLicensesExceededFault(List<DBSession> sessions, int licenses) {
            Message = FaultDescription;
            LicenseCount = licenses;
            ActiveSessions = sessions;
        }
    }
}
