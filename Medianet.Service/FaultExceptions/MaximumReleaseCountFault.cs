﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Medianet.Service.FaultExceptions
{
    [DataContract]
    public class MaximumReleaseCountFault
    { 
        public const string FaultDescription = "Maximum allowable releases reached. ";

        [DataMember]
        public string Message { get; set; }

        public MaximumReleaseCountFault()
        {
            Message = FaultDescription;
        }
    }
}
