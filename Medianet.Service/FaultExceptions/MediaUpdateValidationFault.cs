﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Medianet.Service.Dto;
using Medianet.Service.Common;

namespace Medianet.Service.FaultExceptions
{
    [DataContract]
    public class MediaUpdateValidationFault : DoNotLogFault
    {

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public List<string> ReasonList { get; set; }

        [DataMember]
        public UpdateErrorType ErrorType { get; set; }

        [DataMember]
        public string OutletId { get; set; }

        public MediaUpdateValidationFault(string message, List<string> list = null) {
            Message = message;
            ReasonList = list;
            ErrorType = UpdateErrorType.Generic;

        }

        public MediaUpdateValidationFault(string outletId, UpdateErrorType type, string message, List<string> list = null) {
            Message = message;
            ReasonList = list;
            ErrorType = type;
            OutletId = outletId;
        }
    }
}
