﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Medianet.Service.FaultExceptions
{
    [DataContract]
    public class NonUniqueFault
    {
        public const string FaultDescription = "Item is not unique.";

        [DataMember]
        public string Message { get; set; }

        public NonUniqueFault() {
            Message = FaultDescription;
        }
    }
}
