﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Medianet.Service.FaultExceptions
{
    [DataContract]
    public class NotFullyAuthorisedFault
    {
        public const string FaultDescription = "Not fully authorised.";

        [DataMember]
        public string Message { get; set; }

        public NotFullyAuthorisedFault()
        {
            Message = FaultDescription;
        }
    }
}
