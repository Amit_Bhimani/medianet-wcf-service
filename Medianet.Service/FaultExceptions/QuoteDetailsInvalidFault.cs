﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Medianet.Service.FaultExceptions
{
    [DataContract]
    public class QuoteDetailsInvalidFault
    {
        public const string FaultDescription = "You quote details are invalid or dont exist.";

        [DataMember]
        public string Message { get; set; }

        public QuoteDetailsInvalidFault()
        {
            Message = FaultDescription;
        }
    }
}
