﻿namespace Medianet.Service.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Runtime.Serialization;
    using System.Collections;

    [DataContract(Name = "ServiceFault", Namespace = "http://medianet.com.au/services")]
    public class ServiceFault
    {
        private string message;
        private IDictionary<string, string> data;
        private Guid id;

        public ServiceFault() {
        }

        public ServiceFault(string message) {
            MessageText = message;
        }

        [DataMember]
        public string MessageText {
            get { return message; }
            set { message = value; }
        }

        [DataMember]
        public IDictionary<string, string> Data {
            get { return data; }
            set { data = value; }

        }
        [DataMember]
        public Guid Id {
            get { return id; }
            set { id = value; }
        }
    }
}
