﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Medianet.Service.Dto;

namespace Medianet.Service.FaultExceptions
{
    [DataContract]
    public class SessionAlreadyExistsFault : DoNotLogFault
    {
        public const string FaultDescription = "A sesson already exists for this User.";

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public DBSession ActiveSession { get; set; }

        public SessionAlreadyExistsFault(DBSession session) {
            Message = FaultDescription;
            ActiveSession = session;
        }
    }
}
