﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Medianet.Service.Dto;

namespace Medianet.Service.FaultExceptions
{
    [DataContract]
    public class SubjectPrimaryContactRequiredFault : DoNotLogFault
    {
        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string OutletId { get; set; }

        [DataMember]
        public int SubjectId { get; set; }

        public SubjectPrimaryContactRequiredFault(string outletId, int subjectId, string message) {
            Message = message;
            OutletId = outletId;
            SubjectId = subjectId;
        }
    }
}
