﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Medianet.Service.FaultExceptions
{
    [DataContract]
    public class UserMustChangePasswordFault : DoNotLogFault
    {
        public const string FaultDescription = "User must change password.";

        [DataMember]
        public string Message { get; set; }

        public UserMustChangePasswordFault() {
            Message = FaultDescription;
        }
    }
}
