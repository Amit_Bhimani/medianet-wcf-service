﻿using ChargeBee.Api;
using Medianet.DataLayer;
using Medianet.Service.BusinessLayer;
using Medianet.Service.BusinessLayer.Common;
using Medianet.Service.BusinessLayer.Exceptions;
using Medianet.Service.Common;
using Medianet.Service.Contracts;
using Medianet.Service.Dto;
using Medianet.Service.FaultExceptions;
using Medianet.Wcf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Threading;

namespace Medianet.Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [ErrorBehaviorAttribute(typeof(ServiceErrorHandler))]
    public class ChargeBeeService : IChargeBeeService
    {
        public ChargeBeeService()
        {
            // The windows service host normally does this but we do it here when running in debug mode.
            if (!AutoMapperConfigurator.IsInitialized)
                AutoMapperConfigurator.Configure();
            // This is for Configure API & SiteKey and ServicePointManager for Connection in Chargebee.
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            ApiConfig.Configure(ConfigurationManager.AppSettings["ChargeBeeSite"].ToString(), ConfigurationManager.AppSettings["ChargeBeeSiteAPIKey"].ToString());
        }
        
        /// <summary>
        /// Determines whether or not a User exists for a given email address 
        /// </summary>
        /// <param name="emailAddress">The email address to check.</param>
        /// <returns>True if the email address is unique. False if it already exists in ChargeBee or Medianet Databse.</returns>
        public bool IsUniqueEmail(string emailAddress)
        {
            try
            {
                using (var uow = new UnitOfWork())
                {
                    var userBL = new UserBL(uow);
                    List<User> existingUsers = userBL.GetAllByEmailAddress(emailAddress);
                    if (existingUsers.Count > 0)
                    {
                        return false;
                    }
                    ListResult result = ChargeBee.Models.Customer.List().Email().Is(emailAddress).Request();
                    if (result.List.Count > 0)
                    {
                        return false;
                    }
                    return true;
                }                
            }
            catch (Exception ex)
            {
                EmailHelper.GeneralErrorEmail("Error IsUniqueEmail from ChargeBee.", ex);
                throw new FaultException<ServiceFault>(new ServiceFault(), ex.Message);
            }
        }
       
        /// <summary>
        /// Get Customer Details from ChargeBee
        /// </summary>
        /// <param name="customerId">The Customer Id to Get Details of Customer.</param>
        /// <returns></returns>
        public Customer GetChargeBeeCustomer(string customerId, Customer customer)
        {
            try
            {
                EntityResult result = ChargeBee.Models.Customer.Retrieve(customerId).Request();
                if (result.Customer != null)
                {
                    customer = ConvertIntoDbCustomer(result.Customer, customer);
                }
                return customer;
            }
            catch (Exception ex)
            {
                EmailHelper.GeneralErrorEmail("Error GetChargeBeeCustomer for " + customerId + " from ChargeBee.", ex);
                throw new FaultException<ServiceFault>(new ServiceFault(), ex.Message);
            }
        }

        /// <summary>
        /// Log a user in to the specified System using a session created after logging into another system.
        /// </summary>
        /// <param name="sessionKey">The SessionKey to use to login to another System.</param>
        /// <param name="system">The system being logged in to.</param>
        /// <returns>A DBSession object containing the User and Customer details.</returns>
        public DBSession LoginUsingSession(string sessionKey, SystemType system)
        {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);
            User user;

            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);
                user = userBL.Get(session.UserId);

                if (user == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("User with Id {0} not found.", session.UserId));

                // Check if they need to agree to terms & conditions
                CheckTermsAndConditions(user, system);

                session = CreateSession(user, system, session.System == SystemType.Admin);
                session.Customer = GetChargeBeeCustomer(user.DebtorNumber, session.Customer);
                userBL.UpdateLogonAttempt(user.Id, session.LoginDate, system, true);

                return session;
            }
        }

        /// <summary>
        /// Add a new Customer To ChargeBee and User/Customer to the database based on basic Account details.
        /// </summary>
        /// <param name="account">A NewAccountRequest object containing the Customer and User details.</param>
        /// <returns>The DebtorNumber generated for this customer.</returns>
        public string AddChargeBeeAccount(NewAccountRequest account)
        {
            if (string.IsNullOrWhiteSpace(account.EmailAddress))
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), string.Format("EmailAddress must not be blank."));
            if (string.IsNullOrWhiteSpace(account.Password))
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), string.Format("Password must not be blank."));
            if (string.IsNullOrWhiteSpace(account.CompanyName))
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), string.Format("CompanyName must not be blank."));
            if (string.IsNullOrWhiteSpace(account.FirstName))
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), string.Format("FirstName must not be blank."));

            Customer cust = new Customer();
            string custId = "";
            int userId = 0;
            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);
                var customerBL = new CustomerBL(uow);
                List<User> existingUsers = userBL.GetAllByEmailAddress(account.EmailAddress);
                if (existingUsers.Count > 0)
                    throw new FaultException<NonUniqueFault>(new NonUniqueFault(), string.Format("User with EmailAddress {0} already exists.", account.EmailAddress));

                //Check For ChargeBee Customer Email Address
                if (IsUniqueEmail(account.EmailAddress) == false)
                    throw new FaultException<NonUniqueFault>(new NonUniqueFault(), string.Format("Customer with EmailAddress {0} already exists.", account.EmailAddress));

                cust = AddChargeBeeCustomer(account, customerBL);
                // Create the user.
                byte[] salt = PasswordHelper.GenerateSalt();
                try
                {
                    User user = new User
                    {
                        FirstName = account.FirstName,
                        LastName = account.LastName,
                        Position = account.ContactPosition,
                        DebtorNumber = cust.DebtorNumber,
                        EmailAddress = account.EmailAddress,
                        TelephoneNumber = account.TelephoneNumber,
                        FaxNumber = account.FaxNumber,
                        IndustryCode = account.IndustryCode,
                        HashedPassword = PasswordHelper.HashPassword(account.Password, salt),
                        Salt = Convert.ToBase64String(salt),
                        ExpiryToken = Guid.NewGuid(),
                        HasDistributeWebAccess = account.System == SystemType.Medianet,
                        LogonName = $"{account.FirstName}{account.LastName}".Replace(" ", "")
                    };
                    // Add the User to the database.
                    user.Id = userBL.Add(user);
                    custId = cust.DebtorNumber;
                    userId = user.Id;
                }
                catch (Exception ex)
                {
                    //Rollback
                    custId = "";
                    DeleteChargeBeeCustomer(cust.DebtorNumber);
                }
                // for Thanks for Agree Terms and Conditions
                customerBL.AcknowledgeTermsRead(userId, $"{account.FirstName} {account.LastName}".Trim(), account.EmailAddress, SystemType.Medianet);
 
                // Now set the create and modified UserId of the Customer record to the new User.
                cust.CreatedByUserId = userId;
                customerBL.Update(cust, userId);

                // update Last DebtorNumber in Environment table
                customerBL.UpdateEnvironmentVariable(); 

            }

            EmailHelper.NewAccountEmail($"{account.FirstName} {account.LastName}".Trim(), cust.Name, custId);

            return custId;
        }

        /// <summary>
        /// Update a User record and their Customer record based on information in an Account object.
        /// </summary>
        /// <param name="account">An Account object containing basic information to be updated.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>

        public void UpdateChargeBeeAccount(Account account, string sessionKey)
        {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);

            if (string.IsNullOrWhiteSpace(account.EmailAddress))
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), string.Format("EmailAddress must not be blank."));
            if (string.IsNullOrWhiteSpace(account.CompanyName))
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), string.Format("CompanyName must not be blank."));
            if (string.IsNullOrWhiteSpace(account.FirstName))
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), string.Format("FirstName must not be blank."));

            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);
                var customerBL = new CustomerBL(uow);
                List<User> existingUsers = userBL.GetAllByEmailAddress(account.EmailAddress);

                if (existingUsers.Exists(u => u.Id != account.UserId))
                    throw new FaultException<NonUniqueFault>(new NonUniqueFault(), string.Format("User with EmailAddress {0} already exists.", account.EmailAddress));

                User user = userBL.Get(account.UserId);
                if (user == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("User with Id {0} not found.", account.UserId));

                if (IsUniqueEmailWithCustomerId(user.EmailAddress,user.DebtorNumber) == false)
                    throw new FaultException<NonUniqueFault>(new NonUniqueFault(), string.Format("Customer with EmailAddress {0} already exists.", account.EmailAddress));

                Customer cust = customerBL.Get(user.DebtorNumber);
                if (cust == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Customer with DebtorNumber {0} not found.", user.DebtorNumber));

                cust = GetChargeBeeCustomer(user.DebtorNumber, cust);
                if (AccessHelper.IsVerifiedCustomer(user.DebtorNumber))
                {
                    UpdateChargeBeeCustomer(cust, account, user.Id,customerBL);
                }

                // Update the user.
                user.FirstName = account.FirstName.Truncate(50);
                user.LastName = account.LastName.Truncate(50);
                user.Position = account.ContactPosition.Truncate(50);
                user.DebtorNumber = session.DebtorNumber;
                user.EmailAddress = account.EmailAddress.Truncate(200);
                user.TelephoneNumber = account.TelephoneNumber.Truncate(24);
                user.FaxNumber = account.FaxNumber.Truncate(24);
                user.IndustryCode = account.IndustryCode;
                user.RelevantSubjects = account.RelevantSubjects;
                // If a new password was provided then update it too.
                if (!string.IsNullOrWhiteSpace(account.Password))
                {
                    if (!session.IsAuthorised)
                    {
                        throw new FaultException<NotFullyAuthorisedFault>(new NotFullyAuthorisedFault(), string.Format("Authorisation level does not permit a password to be changed."));
                    }
                    user.MustChangePassword = false;
                    byte[] salt = PasswordHelper.GenerateSalt();
                    user.HashedPassword = PasswordHelper.HashPassword(account.Password, salt);
                    user.Salt = Convert.ToBase64String(salt);
                    user.ExpiryToken = Guid.NewGuid();
                }

                // Update the User in the database.
                userBL.Update(user, session.UserId);
                if (account.IsForUpgrade == true)
                {
                    // EmailHelper.UpgradeAccountEmailSend(account, accountManagerName);
                    try
                    {
                        var accountManagerName = "";
                        if (!string.IsNullOrWhiteSpace(account.SalesRegionName))
                        {
                            accountManagerName = GetSalesRegion(account.SalesRegionName).AccountManagerName;
                        }
                        Thread thread = new Thread(() => EmailHelper.UpgradeAccountEmailSend(account, accountManagerName));
                        thread.Start();
                    }
                    catch (Exception ex)
                    {
                        EmailHelper.GeneralErrorEmail("Error UpgradeAccountEmailSend UpdateChargeBeeAccount.", ex);
                    }
                  
                }
            }
        }

        /// <summary>
        /// Get Account and Customer from Medianet also Customer Details From ChargeBee 
        /// </summary>
        /// <param name="userId">The userId of Login User to get Account Data With Customer Info</param>
        /// <param name="sessionKey">The when user login Created sessionKey</param>
        /// <returns></returns>
        public Account GetAccountFromChargeBee(int userId, string sessionKey)
        {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);
            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);
                User user = userBL.Get(userId);
                var customerBL = new CustomerBL(uow);
                if (user == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("User with Id {0} not found.", userId));

                if (string.IsNullOrWhiteSpace(user.DebtorNumber))
                    throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), string.Format("Customer With DebtorNumber  {0}  must not be blank."));

                var customer = customerBL.Get(user.DebtorNumber);
                if (customer == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Customer with DebtorNumber {0} not found.", user.DebtorNumber));


                ChargeBee.Models.Customer cust = GetChargeBeeCustomerDetails(user.DebtorNumber);
                if (cust == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Customer with DebtorNumber {0} not found..", user.DebtorNumber));


                // Populate the user data.
                Account account = new Account
                {
                    UserId = user.Id,
                    DebtorNumber = user.DebtorNumber,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    ContactPosition = user.Position,
                    EmailAddress = user.EmailAddress,
                    FaxNumber = user.FaxNumber,
                    UserLogonName = user.LogonName,
                    Password = string.Empty, // Make it blank for security reasons.
                    CompanyName = cust.Company,
                    TelephoneNumber = cust.Phone,
                    IndustryCode = user.IndustryCode,
                    RelevantSubjects = user.RelevantSubjects,
                    ABN = cust.VatNumber,
                    TimezoneCode = customer.TimezoneCode,
                    Timezone = customer.Timezone
                };
                account.CompanyLogonName = cust.Company.Replace(" ", "").Truncate(50);
                account.AccountType = GetAccountTypeFromAccountTypeName(cust.GetValue<String>("cf_account_type", false));
                account.SalesRegionName = cust.GetValue<String>("cf_sales_territory", false);
                if (!AccessHelper.IsVerifiedCustomer(user.DebtorNumber))
                {
                    // If the account is the unverified account, work some special magic to set the account/user details
                    // this is to allow the website to properly prefill fields when the user decides to upgrade to a true account
                    account.IndustryCode = user.ShortName;
                    account.ContactPosition = "";
                    account.AccountType = CustomerBillingType.Creditcard;
                }
                else
                {
                    try
                    {
                        account.CompanyAddress = new AccountAddress();
                        if (cust.BillingAddress != null)
                        {
                            account.CompanyAddress.AddressLine1 = cust.BillingAddress.Line1();
                            account.CompanyAddress.AddressLine2 = cust.BillingAddress.Line2();
                            account.CompanyAddress.AddressLine3 = cust.BillingAddress.Line3();
                            account.CompanyAddress.City = cust.BillingAddress.City();
                            if (!string.IsNullOrWhiteSpace(cust.BillingAddress.Country()))
                            {
                                RegionInfo regionInfo = new RegionInfo(cust.BillingAddress.Country());
                                account.CompanyAddress.Country = regionInfo.DisplayName;
                            }
                            account.CompanyAddress.Postcode = cust.BillingAddress.Zip();
                            account.CompanyAddress.State = cust.BillingAddress.State();
                        }
                        account.BillingAddress = account.CompanyAddress;// Same as Company Address
                    }
                    catch (Exception)
                    {
                        // This is an internal customer so Finance don't have details. Ignore the error.
                        account.CompanyAddress = new AccountAddress();
                        account.BillingAddress = new AccountAddress();
                    }
                }
                return account;
            }
        }

        /// <summary>
        /// Gets a list of industry codes for a customer to be categorised into.
        /// </summary>
        /// <returns>A list of industry codes.</returns>
        public List<IndustryCode> GetIndustryCodes()
        {
            var array = (IndustryCodeType[])(Enum.GetValues(typeof(IndustryCodeType)).Cast<IndustryCodeType>());
            return array
              .Select(a => new IndustryCode
              {
                  Code = a.ToString(),
                  Description = a.GetStringValue().ToString(),
              })
               .ToList();
        }

        /// <summary>
        /// Gets a list of billing codes for a customer.
        /// </summary>
        /// <param name="debtorNumber">The DebtorNumber of the Customer to get billing codes for.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A list of industry codes.</returns>
        public List<CustomerBillingCode> GetCustomerBillingCodes(string debtorNumber, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAdminOrSameCustomer(debtorNumber, sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to Billing Codes for DebtorNumber {0} denied.", debtorNumber));

            using (var uow = new UnitOfWork())
            {
                var costBL = new CustomerBillingCodeBL(uow);
                List<CustomerBillingCode> costList = costBL.GetAllByDebtorNumber(debtorNumber);

                return costList;
            }
        }

        /// <summary>
        /// Validate a sessionKey and sets the LastAccessedDate to the current time.
        /// </summary>
        /// <param name="sessionKey">The SessionKey to validate.</param>
        /// <returns>The DBSession object containing the User and Customer details.</returns>
        public DBSession ValidateSession(string sessionKey)
        {
            using (var uow = new UnitOfWork())
            {
                var sessionBL = new DBSessionBL(uow);
                DBSession session = null;

                session = sessionBL.Validate(sessionKey);
                session.Customer = GetChargeBeeCustomer(session.DebtorNumber, session.Customer);

                // If it's an invalid session then sleep a bit to prevent brute force guessing.
                if (session == null)
                    AccessHelper.BruteForcePreventionDelay();

                return session;
            }
        }

        /// <summary>
        /// Log a user in to the specified System using an email adress and password.
        /// </summary>
        /// <param name="emailAddress">The EmailAddress of the User.</param>
        /// <param name="password">The password of the User.</param>
        /// <param name="system">The system being logged in to.</param>
        /// <returns>A DBSession object containing the User and Customer details.</returns>
        public DBSession LoginUsingEmailAddress(string emailAddress, string password, SystemType system)
        {
            User user;
            DBSession session;

            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);

                user = GetUserByEmailAddressOrLogonName(emailAddress, string.Empty, system, userBL, uow);

                bool passwordsEqual = PasswordHelper.ComparePasswords(PasswordHelper.HashPassword(password, Convert.FromBase64String(user.Salt)), user.HashedPassword);

                if (!passwordsEqual)
                {
                    userBL.UpdateLogonAttempt(user.Id, null, system, false);

                    AccessHelper.BruteForcePreventionDelay();
                    throw new FaultException<InvalidLoginFault>(new InvalidLoginFault(), InvalidLoginFault.FaultDescription);
                }

                // Check if they need to agree to terms & conditions
                CheckTermsAndConditions(user, system);

                session = CreateSession(user, system);
                session.Customer = GetChargeBeeCustomer(user.DebtorNumber, session.Customer);
                userBL.UpdateLogonAttempt(user.Id, session.LoginDate, system, true);

                return session;
            }
        }

        /// <summary>
        /// Logs a user in to the specified System using a username, company logon and password.
        /// </summary>
        /// <param name="username">The username of the User.</param>
        /// <param name="companyLogon">The logon name of the Company the User belongs to.</param>
        /// <param name="password">The password of the User.</param>
        /// <param name="system">The system being logged in to.</param>
        /// <returns>A DBSession object containing the User and Customer details.</returns>
        public DBSession Login(string username, string companyLogon, string password, SystemType system)
        {
            DBSession session;

            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);
                var user = GetUserByEmailAddressOrLogonName(username, companyLogon, system, userBL, uow);

                bool passwordsEqual = PasswordHelper.ComparePasswords(PasswordHelper.HashPassword(password, Convert.FromBase64String(user.Salt)), user.HashedPassword);

                if (!passwordsEqual)
                {
                    userBL.UpdateLogonAttempt(user.Id, null, system, false);

                    AccessHelper.BruteForcePreventionDelay();
                    throw new FaultException<InvalidLoginFault>(new InvalidLoginFault(), InvalidLoginFault.FaultDescription);
                }

                // Check if they need to agree to terms & conditions
                CheckTermsAndConditions(user, system);

                session = CreateSession(user, system);
                session.Customer = GetChargeBeeCustomer(user.DebtorNumber, session.Customer);

                userBL.UpdateLogonAttempt(user.Id, session.LoginDate, system, true);

                return session;
            }
        }

        /// <summary>
        /// Get the details of a potential User waiting to be validated via email.
        /// </summary>
        /// <param name="token">The GUID to fetch registration details for.</param>
        /// <returns>The account details of the User.</returns>
        public RegistrationValidation GetRegistrationValidation(Guid token)
        {
            RegistrationValidation rego = null;

            using (var uow = new UnitOfWork())
            {
                var registrationBL = new RegistrationValidationBL(uow);
                var releasePreviewBL = new ReleaseUnverifiedPreviewBL(uow);

                rego = registrationBL.Get(token);
                rego.ReleaseUnverifiedPreview = releasePreviewBL.Get(token);

                if (rego == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Registration validation token {0} not found.", token));
            }

            return rego;
        }

        /// <summary>
        /// Sends an email to someone with a link allowing them to reset their password.
        /// </summary>
        /// <param name="emailAddress">The EmailAddress of the User. For the Journalists website this may be just a username.</param>
        /// <param name="system">The system being logged in to.</param>
        public void SendForgotPasswordEmail(string emailAddress, SystemType system)
        {
            string url;
            string firstName;
            string toAddress = emailAddress;
            string product;

            List<User> users;
            User user;
            DBSession session;

            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);
                users = userBL.GetAllByEmailAddress(emailAddress);

                if (users.Count == 0)
                {
                    // Maybe the email address is username@companylogon or just username for the journalists website. Check for this.
                    string[] logonParams = emailAddress.Split('@');
                    users = userBL.GetAllByLogonDetails(logonParams[0], logonParams.Length > 1 ? logonParams[1] : string.Empty, system);
                }

                if (users.Count == 0)
                {
                    AccessHelper.BruteForcePreventionDelay();
                    throw new FaultException<InvalidLoginFault>(new InvalidLoginFault(), InvalidLoginFault.FaultDescription);
                }

                if (users.Count > 1)
                    throw new FaultException<InvalidLoginFault>(new InvalidLoginFault(), InvalidLoginFault.FaultDescriptionDuplicateEmail);

                user = users[0];

                session = CreateSession(user, system, false, false, true);
                session.Customer = GetChargeBeeCustomer(user.DebtorNumber, session.Customer);//check for if Exist on ChargeBee Or not
                // Make sure the user must change their email on next logon.
                user.MustChangePassword = true;
                user.LastLogonDate = session.LoginDate;
                userBL.Update(user, user.Id);

                firstName = user.FirstName.ProperCase();
                toAddress = user.EmailAddress;
            }

            switch (system)
            {
                case SystemType.Contacts:
                    url = ConfigurationManager.AppSettings["ContactsForgotPasswordURL"] + session.Key;
                    product = "Medianet's Media Database";
                    break;
                case SystemType.Journalists:
                    url = ConfigurationManager.AppSettings["JournalistsForgotPasswordURL"] + session.Key;
                    product = "the Medianet for Journalist";
                    break;
                default:
                    url = ConfigurationManager.AppSettings["DistributionForgotPasswordURL"] + session.Key;
                    product = "Medianet";
                    break;
            }

            if (string.IsNullOrEmpty(firstName))
                firstName = "Medianet Customer";

            // Send an email to them with a link allowing them to change their password.
            EmailHelper.ForgotPasswordEmail(firstName, product, toAddress, url);
        }

        /// <summary>
        /// Change a Users password and create a new session if currently using a session designed for changing passwords only.
        /// </summary>
        /// <param name="password">The new password.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A DBSession object containing the User and Customer details.</returns>
        public DBSession ChangePassword(string password, string sessionKey)
        {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey, true);
            User user;

            if (!session.IsAuthorised)
                throw new FaultException<NotFullyAuthorisedFault>(new NotFullyAuthorisedFault(), string.Format("Please verify your credentials by logging in before password changes can be processed."));

            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);
                //bool IsVerified;

                user = userBL.Get(session.UserId);

                if (user == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("User with Id {0} not found.", session.UserId));

                byte[] salt = PasswordHelper.GenerateSalt();
                user.Salt = Convert.ToBase64String(salt);
                user.HashedPassword = PasswordHelper.HashPassword(password, salt);
                user.MustChangePassword = false;

                // Reset this so previously used links to logon for the first time can't be used forever
                user.ExpiryToken = Guid.NewGuid();

                userBL.Update(user, user.Id);

                //IsVerified = AccessHelper.IsVerifiedCustomer(user.DebtorNumber);

                // If they only have a temporary ChangingPassword session then create a proper one.
                if (session.RowStatus == RowStatusType.ChangingPassword)
                {
                    var sessionBL = new DBSessionBL(uow);
                    sessionBL.Logout(sessionKey);

                    // Check if they need to agree to terms & conditions
                    CheckTermsAndConditions(user, session.System);

                    session = CreateSession(user, session.System);
                    session.Customer = GetChargeBeeCustomer(user.DebtorNumber, session.Customer);
                    userBL.UpdateLogonAttempt(user.Id, session.LoginDate, session.System, true);
                }
                else
                {
                    // Get the session again to get changes to the User made above.
                    session = AccessHelper.GetSessionFromKey(session.Key);
                }

                return session;
            }
        }

        /// <summary>
        /// Add a new Registration validation to the database and send
        /// an email to the user asking them to verify thier email address.
        /// </summary>
        /// <param name="emailAddress">The EmailAddress of the user registering.</param>
        /// <param name="firstName">The first name of the user registering.</param>
        /// <param name="system"></param>
        /// <returns>The unique Guid generated for this registration.</returns>
        public Guid AddRegistrationValidation(string emailAddress, string firstName, SystemType system)
        {
            if (string.IsNullOrWhiteSpace(emailAddress))
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), string.Format("EmailAddress must not be blank."));
            if (string.IsNullOrWhiteSpace(firstName))
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), string.Format("FirstName must not be blank."));

            var rego = new RegistrationValidation();

            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);
                var registrationBL = new RegistrationValidationBL(uow);
                List<User> existingUsers = userBL.GetAllByEmailAddress(emailAddress);

                if (existingUsers.Count > 0)
                    throw new FaultException<NonUniqueFault>(new NonUniqueFault(), string.Format("User with EmailAddress {0} already exists.", emailAddress));

                // Create the user.
                rego.System = system;
                rego.FirstName = firstName.Truncate(50);
                rego.EmailAddress = emailAddress.Truncate(200);
                rego.CreatedDate = DateTime.Now;

                // Add the User to the database.
                rego.Token = registrationBL.Add(rego);
            }

            SendRegistrationValidationEmail(system, rego);
            return rego.Token;
        }
        
        /// <summary>
        /// Log a user in to the specified System impersonating someone else using an admin session.
        /// </summary>
        /// <param name="sessionKey">The SessionKey to use to login to another System.</param>
        /// <param name="userId">The Id of the User to log in as.</param>
        /// <param name="system">The system being logged in to.</param>
        /// <returns>A DBSession object containing the User and Customer details.</returns>
        public DBSession LoginImpersonationUsingSession(string sessionKey, int userId, SystemType system)
        {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);
            User user;

            // Make sure this user is allowed to impersonate people.
            if (!AccessHelper.IsAccessAllowedToImpersonate(session.System, system))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Impersonating Access denied.");

            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);

                user = userBL.Get(userId);

                if (user == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("User with Id {0} not found.", session.UserId));

                session = CreateSession(user, system);
                session.Customer = GetChargeBeeCustomer(user.DebtorNumber, session.Customer);

                return session;
            }
        }
        
        /// <summary>
        /// Creates an unauthorised session within the specified system.
        /// </summary>
        /// <param name="emailAddress">The email address of the user to create the session for.</param>
        /// <param name="expiryToken">A unique identifier that specified the user concurrency details.</param>
        /// <param name="system">The system to create a session for.</param>
        /// <returns>A DBSession object where the session is not fully authorised (limited access to key areas).</returns>
        public DBSession LoginPartialAuthorisation(string emailAddress, Guid expiryToken, SystemType system)
        {
            User user;
            DBSession session;

            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);

                user = GetUserByEmailAddressOrLogonName(emailAddress, string.Empty, system, userBL, uow);

                if (!user.ExpiryToken.Equals(expiryToken))
                {
                    userBL.UpdateLogonAttempt(user.Id, null, system, false);

                    AccessHelper.BruteForcePreventionDelay();
                    throw new FaultException<InvalidLoginFault>(new InvalidLoginFault(), InvalidLoginFault.FaultDescription);
                }

                // Check if they need to agree to terms & conditions
                CheckTermsAndConditions(user, system);

                session = CreateSession(user, system);
                session.Customer = GetChargeBeeCustomer(user.DebtorNumber, session.Customer);
                session.IsAuthorised = false;

                userBL.UpdateLogonAttempt(user.Id, session.LoginDate, system, true);
            }

            return session;
        }

        /// <summary>
        /// Log the user out by setting the DBSession to inactive.
        /// </summary>
        /// <param name="sessionKey">The SessionKey to set to inactive.</param>
        public void Logout(string sessionKey)
        {
            NLog.LogManager.GetCurrentClassLogger().Log(NLog.LogLevel.Info, "Logout:SessionKey=" + sessionKey);

            using (var uow = new UnitOfWork())
            {
                var sessionBL = new DBSessionBL(uow);
                sessionBL.Logout(sessionKey);
            }
        }

        /// <summary>
        /// Acknowledge that a User has read the terms and conditions on dehalf of a customer for a particular system.
        /// Once done they are logged on.
        /// </summary>
        /// <param name="emailAddress">The EmailAddress of the User.</param>
        /// <param name="password">The password of the User.</param>
        /// <param name="system">The system being logged in to.</param>
        /// <returns>A DBSession object containing the User and Customer details.</returns>
        public DBSession AcknowledgeTermsReadAndLogin(string emailAddress, string password, SystemType system)
        {
            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);

                User user = GetUserByEmailAddressOrLogonName(emailAddress, string.Empty, system, userBL, uow);

                bool passwordsEqual = PasswordHelper.ComparePasswords(PasswordHelper.HashPassword(password, Convert.FromBase64String(user.Salt)), user.HashedPassword);

                if (!passwordsEqual)
                {
                    userBL.UpdateLogonAttempt(user.Id, null, system, false);

                    AccessHelper.BruteForcePreventionDelay();
                    throw new FaultException<InvalidLoginFault>(new InvalidLoginFault(), InvalidLoginFault.FaultDescription);
                }

                string primaryName = string.Empty;
                string primaryEmailAddress = string.Empty;

                if (!BusinessLayer.Common.Constants.DEBTOR_NUMBERS_INTERNAL.Contains(user.DebtorNumber))
                {
                    try
                    {
                        var x = GetChargeBeeCustomer(user.DebtorNumber, new Customer());

                        if (x.DefaultOriginatorEmailAddress != null)
                            primaryName = x.Name;

                        if (x.DefaultOriginatorEmailAddress != null)
                            primaryEmailAddress = string.Join(";", x.DefaultOriginatorEmailAddress);
                    }
                    catch (Exception ex)
                    {
                        NLog.LogManager.GetCurrentClassLogger().Log(NLog.LogLevel.Error,
                                string.Format("AcknowledgeTermsRead - Failed to find customer with DebtorNumber {0} in finance. {1}",
                                user.DebtorNumber, ex.ToString()));
                    }
                }
              
                var customerBL = new CustomerBL(uow);
                customerBL.AcknowledgeTermsRead(user.Id, primaryName, primaryEmailAddress, system);

                DBSession session = CreateSession(user, system);
                session.Customer = GetChargeBeeCustomer(user.DebtorNumber, session.Customer);
                userBL.UpdateLogonAttempt(user.Id, session.LoginDate, system, true);
                return session;
            }
        }

        /// <summary>
        /// Acknowledge Payment For Getting Transaction id from ChargeBee
        /// </summary>
        /// <param name="paymentAccessCode">The paymentAccessCode to Get Details of Transaction.</param>
        /// <returns> Transaction Id of ChargeBee</returns>
        public string AcknowledgePayment(string paymentAccessCode)
        {
            try
            {
                var result = ChargeBee.Models.HostedPage.Acknowledge(paymentAccessCode).Request();
                var dd = result.HostedPage.Content.Invoice.LinkedPayments;
                if (dd.Count> 0)
                {
                    return dd[0].TxnId();
                }
                return "";
            }
            catch (Exception ex)
            {
                EmailHelper.GeneralErrorEmail("Error AcknowledgePayment from ChargeBee.", ex);
                return "";
                //throw new FaultException<ServiceFault>(new ServiceFault(), ex.Message);
            }
        }

        /// <summary>
        /// For fetching Estimate Price of addon with GST From ChargeBee
        /// </summary>
        /// <param name="debtorNumber"> Customer DebtorNumber to Get Data from ChargeBee </param>
        /// <param name="addOnId">AddonId to Get Price</param>
        /// <param name="price">Default Price of Addod if Have</param>
        /// <param name="quantity">Quantity of Addod</param>
        /// <param name="isRecordFound"> Custom Price Exist or not</param>s
        /// <returns>addon which GST Price</returns>
        public AddOn CreateEstimateInvoice(string debtorNumber, string addOnId, decimal price = 0, int quantity = 1, bool isRecordFound = false)
        {
            try
            {
                var estimateResponse = ChargeBee.Models.Estimate.CreateInvoice().InvoiceCustomerId(debtorNumber)
                                  .AddonId(0, addOnId).AddonQuantity(0, quantity);
                if (isRecordFound)
                {
                    estimateResponse = estimateResponse.AddonUnitPrice(0, (int)Math.Round(price * 100));
                }
                EntityResult result = estimateResponse.Request();
                var addon = new AddOn();
                addon.Price = result.Estimate.InvoiceEstimate.SubTotal;
                addon.GST = result.Estimate.InvoiceEstimate.Total - result.Estimate.InvoiceEstimate.SubTotal;
                return addon;
            }
            catch (Exception ex)
            {
                EmailHelper.GeneralErrorEmail("Error CreateEstimateInvoice from ChargeBee.", ex);
                throw new FaultException<ServiceFault>(new ServiceFault(), ex.Message);
            }
        }

        /// <summary>
        /// For Fetching Hosted Page URL of Payment Page From ChargeBee
        /// </summary>
        /// <param name="quoteResponse">The Release Quote Response for Getting Priceoverriding logic and Generate Payment Page</param>
        /// <returns>The Url of Payment page from Chargebee</returns>
        public string HostedPageCheckoutOneTime(ReleaseQuoteResponse quoteResponse,string redirectURL)
        {
            try
            {
                EntityResult result = null;
                //AUD is Default Australian Currency(Australian Dollar equals)
                var hostedPageResponse = ChargeBee.Models.HostedPage.CheckoutOneTime().Embed(true).CurrencyCode("AUD").CustomerId(quoteResponse.CustNum).RedirectUrl(redirectURL).CancelUrl(redirectURL.Replace("Thanks", "Payment"));
                for (int i = 0; i < quoteResponse.Transactions.Count; i++)
                {
                    hostedPageResponse = hostedPageResponse.AddonId(i, quoteResponse.Transactions[i].Reference.ToString())
                                   .AddonQuantity(i, quoteResponse.Transactions[i].Quantity)
                                   .AddonUnitPrice(i, quoteResponse.Transactions[i].intPrice);
                }
                result = hostedPageResponse.Request();
                var hostedPage = result.HostedPage;
                return hostedPage.Url;
            }
            catch (Exception ex)
            {
                EmailHelper.GeneralErrorEmail("Error HostedPageCheckoutOneTime from ChargeBee.", ex);
                throw new FaultException<ServiceFault>(new ServiceFault(), ex.Message);
            }
        }

        /// <summary>
        /// For Fetching Subscription For DebtorNumber from ChargeBee
        /// </summary>
        /// <param name="debtorNumber"> Customer DebtorNumber </param>
        /// <returns>List of AAPContract</returns>
        public List<AAPContract> GetCustomerContracts(string debtorNumber)
        {
            var contracts = GetContacts(debtorNumber);
            List<AAPContract> aapcontracts = new List<AAPContract>();
            if (contracts != null)
            {
                for (int i = 0; i < contracts.Count; i++)
                {
                    var plan = GetChargeBeePlanFromPlanId(contracts[i].Subscription.PlanId);
                    AAPContract aapcontract = new AAPContract();
                    aapcontract.ContractStartDate = contracts[i].Subscription.StartedAt;
                    aapcontract.ContractEndDate = contracts[i].Subscription.CurrentTermEnd;
                    aapcontract.Product = plan.Name;
                    aapcontract.ContractTerm = contracts[i].Subscription.BillingPeriod;
                    aapcontract.Status = contracts[i].Subscription.Status.ToString();
                    if (plan.PeriodUnit == "Year")      
                        aapcontract.Price = contracts[i].Subscription.PlanUnitPrice / 12 / 100;
                    else if (plan.PeriodUnit == "Week")
                        aapcontract.Price = contracts[i].Subscription.PlanUnitPrice * 4 / 100;
                    else
                        aapcontract.Price = contracts[i].Subscription.PlanUnitPrice / 100;
                    aapcontract.Description = contracts[i].Subscription.PlanId;
                    aapcontract.Name = contracts[i].Subscription.PlanId;
                    aapcontracts.Add(aapcontract);
                }
            }
            return aapcontracts;
        }
        
        /// <summary>
        /// Get User Profile for Contact
        /// </summary>
        /// <param name="userId">userId of Customer</param>
        /// <param name="sessionKey">Login user sessionKey</param>
        /// <returns></returns>
        public SalesForceContact GetUserProfile(int userId,string sessionKey)
        {
            var user = GetAccountFromChargeBee(userId, sessionKey);
            if (user != null)
            {
                SalesForceContact sfcontacts = new SalesForceContact();
                sfcontacts.Id = user.UserId.ToString();
                sfcontacts.Name = $"{user.FirstName} {user.LastName}".Trim();
                sfcontacts.AccountName = user.CompanyName;
                sfcontacts.DebtorNumber = user.DebtorNumber;
                sfcontacts.Position = user.ContactPosition;
                sfcontacts.Phone = user.TelephoneNumber;
                sfcontacts.RecordType = "";
                sfcontacts.Email = user.EmailAddress;
                if (!string.IsNullOrWhiteSpace(user.IndustryCode))
                {
                    var indcode = (IndustryCodeType)Enum.Parse(typeof(IndustryCodeType), user.IndustryCode);
                    sfcontacts.BusinessType = indcode.GetStringValue();
                }
                sfcontacts.MainContact = false;
                sfcontacts.RelevantSubjects = string.IsNullOrWhiteSpace(user.RelevantSubjects) == true ? new List<string>() : user.RelevantSubjects.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
                if (!string.IsNullOrEmpty(user.SalesRegionName))
                {
                    var salesRegion = GetSalesRegion(user.SalesRegionName);
                    sfcontacts.AccountOwnerName = salesRegion.AccountManagerName;
                }
                sfcontacts.ABN = user.ABN;
                return sfcontacts;
            }
            return null;
        }

        /// <summary>
        /// Log a user in to the specified System using an existing session for this System. The old session is killed.
        /// </summary>
        /// <param name="sessionKey">The SessionKey to use to login to the System.</param>
        /// <param name="system">The system being logged in to.</param>
        /// <returns>A DBSession object containing the User and Customer details.</returns>
        public DBSession LoginReplacingSession(string sessionKey, SystemType system)
        {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);
            User user;

            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);
                var sessionBL = new DBSessionBL(uow);

                user = userBL.Get(session.UserId);

                if (user == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("User with Id {0} not found.", session.UserId));

                sessionBL.Logout(sessionKey);

                // Check if they need to agree to terms & conditions
                CheckTermsAndConditions(user, system);

                session = CreateSession(user, system);
                session.Customer = GetChargeBeeCustomer(user.DebtorNumber, session.Customer);
                userBL.UpdateLogonAttempt(user.Id, session.LoginDate, system, true);

                return session;
            }
        }
     
        /// <summary>
        /// Get the details of a Sales Region
        /// </summary>
        /// <param name="regionName">The Name of the region to get details</param>
        /// <returns>The SalesRegion</returns>
        public SalesRegion GetSalesRegion(string regionName)
        {
            using (var uow = new UnitOfWork())
            {
                var regionBL = new SalesRegionBL(uow);
                var region = regionBL.GetByName(regionName);

                if (region == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("SalesRegion with Name {0} not found.", regionName));

                return region;
            }
        }
       
        #region Private methods

        /// <summary>
        /// Determines Product Or Plan From ChargeBee
        /// </summary>
        /// <param name="planId">The Plan Id to Get of Plan.</param>
        /// <returns>Plan Details</returns>
        private Plan GetChargeBeePlanFromPlanId(string planId)
        {
            try
            {
                EntityResult result = ChargeBee.Models.Plan.Retrieve(planId).Request();
                if (result.Plan != null)
                {
                    ChargeBee.Models.Plan cbPlan = result.Plan;
                    var plan = new Plan();
                    plan.AccountingCode = cbPlan.AccountingCode;
                    plan.BillingCycles = cbPlan.BillingCycles;
                    plan.CurrencyCode = cbPlan.CurrencyCode;
                    plan.Description = cbPlan.Description;
                    plan.Id = cbPlan.Id;
                    plan.Name = cbPlan.Name;
                    plan.Period = cbPlan.Period;
                    plan.PeriodUnit = cbPlan.PeriodUnit.ToString();
                    plan.Price = cbPlan.Price;
                    plan.PriceInDecimal = Convert.ToDecimal(cbPlan.Price) / 100;
                    plan.Taxable = cbPlan.Taxable;
                    plan.TrialPeriod = cbPlan.TrialPeriod;
                    plan.UpdatedAt = cbPlan.UpdatedAt;
                    return plan;
                }
                return null;
            }
            catch (Exception ex)
            {
                EmailHelper.GeneralErrorEmail("Error GetChargeBeePlanFromPlanId For Id " + planId + " from ChargeBee.", ex);
                throw new FaultException<ServiceFault>(new ServiceFault(), ex.Message);
            }
        }

        /// <summary>
        /// Get Customer Details
        /// </summary>
        /// <param name="customerId">The Customer Id to Get Details of Customer.</param>
        /// <returns></returns>
        private ChargeBee.Models.Customer GetChargeBeeCustomerDetails(string customerId)
        {
            ChargeBee.Models.Customer customer = null;
            try
            {
                EntityResult result = ChargeBee.Models.Customer.Retrieve(customerId).Request();
                customer = result.Customer;
            }
            catch (Exception ex)
            {
                EmailHelper.GeneralErrorEmail("Error GetChargeBeeCustomerDetails for "+ customerId +" from ChargeBee.", ex);
                throw new FaultException<ServiceFault>(new ServiceFault(), ex.Message);
            }
            return customer;
        }

        /// <summary>
        /// Delete  Customer Details -For Roll Back
        /// </summary>
        /// <param name="customerId">The Customer Id to Get Details of Customer.</param>
        /// <returns></returns>
        private void DeleteChargeBeeCustomer(string customerId)
        {
            try
            {
                EntityResult result = ChargeBee.Models.Customer.Delete(customerId).Request();
            }
            catch (Exception ex)
            {
                EmailHelper.GeneralErrorEmail("Error delete customer in DeleteChargeBeeCustomer for " + customerId + " in ChargeBee.", ex);
                throw new FaultException<ServiceFault>(new ServiceFault(), ex.Message);
            }
        }

        private void SendRegistrationValidationEmail(SystemType system, RegistrationValidation rego)
        {
            string url;
            string product;

            switch (system)
            {
                case SystemType.Contacts:
                    url = ConfigurationManager.AppSettings["ContactsValidateEmailURL"] + rego.Token.ToString();
                    product = "Medianet's Media Database";
                    break;
                case SystemType.Journalists:
                    url = ConfigurationManager.AppSettings["JournalistsValidateEmailURL"] + rego.Token.ToString();
                    product = "the Medianet for Journalist";
                    break;
                default:
                    url = ConfigurationManager.AppSettings["DistributionValidateEmailURL"] + rego.Token.ToString();
                    product = "Medianet's Press Release Distribution";
                    break;
            }

            EmailHelper.RegistrationEmailValidationEmail(rego.FirstName.ProperCase(), product, rego.EmailAddress, url);
        }

        private Customer ConvertIntoDbCustomer(ChargeBee.Models.Customer chargebeeCustomer, Customer customer)
        {
            customer.DebtorNumber = chargebeeCustomer.Id;
            customer.Name = chargebeeCustomer.Company.Truncate(50);
            customer.AccountType = GetAccountTypeFromAccountTypeName(chargebeeCustomer.GetValue<String>("cf_account_type", false));//default
            customer.LogonName = chargebeeCustomer.Company.Replace(" ", "").Truncate(50);
            //customer.DefaultOriginatorEmailAddress = chargebeeCustomer.Email;
            customer.SalesRegionName = chargebeeCustomer.GetValue<String>("cf_sales_territory", false);
            return customer;
        }

        private void CheckTermsAndConditions(User user, SystemType system)
        {
            // If logging on to the contacts website check if they need to agree to terms & conditions
            if (system == SystemType.Contacts)
            {
                using (var uow = new UnitOfWork())
                {
                    var customerBL = new CustomerBL(uow);
                    var cust = customerBL.Get(user.DebtorNumber);

                    if (cust == null)
                        throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(),
                            string.Format("Customer {0} not found.", user.DebtorNumber));

                    if (!cust.HasViewedTermsAndConditionsP)
                        throw new FaultException<AgreeToTermsFault>(new AgreeToTermsFault { EmailAddress = user.EmailAddress }, AgreeToTermsFault.FaultDescription);
                }
            }
        }

        private DBSession CreateSession(User user, SystemType system, bool isForAdmin = false, bool isImpersonating = false, bool forcePasswordChange = false, bool isAuthorised = true)
        {
            DBSession sess = null;

            // Make sure they have access to this website.
            if (!AccessHelper.IsAccessAllowedToWebsite(user, system))
                throw new FaultException<InvalidLoginFault>(new InvalidLoginFault(), InvalidLoginFault.FaultDescription);

            using (var uow = new UnitOfWork())
            {
                var sessionBL = new DBSessionBL(uow);
                try
                {
                    sess = sessionBL.CreateSession(user, system, isForAdmin, isImpersonating, forcePasswordChange, isAuthorised);
                }
                catch (SessionAlreadyExistsException ex)
                {
                    throw new FaultException<SessionAlreadyExistsFault>(new SessionAlreadyExistsFault(ex.ActiveSession), SessionAlreadyExistsFault.FaultDescription);
                }
                catch (LogonLicensesExceededException lex)
                {
                    throw new FaultException<LogonLicensesExceededFault>(new LogonLicensesExceededFault(lex.ActiveSessions, lex.LicenseCount), LogonLicensesExceededFault.FaultDescription);
                }
            }

            NLog.LogManager.GetCurrentClassLogger().Log(NLog.LogLevel.Info, string.Format("CreateSession:UserId={0},SessionKey={1}", user.Id, sess.Key));

            return sess;
        }

        private User GetUserByEmailAddressOrLogonName(string emailAddress, string companyLogon, SystemType system, UserBL userBL, UnitOfWork uow)
        {
            List<User> users = null;

            // If there's an @ symbol then assume they are logging on using an email address
            if (emailAddress.IndexOf("@", StringComparison.CurrentCultureIgnoreCase) > 0)
            {
                users = userBL.GetAllByEmailAddress(emailAddress);
            }

            // If that didn't work. Try loggin on using username and optionally company logon
            if (users == null || users.Count == 0)
            {
                string username, company;

                if (!string.IsNullOrWhiteSpace(companyLogon))
                {
                    username = emailAddress;
                    company = companyLogon;
                }
                else
                {
                    string[] logonParams = emailAddress.Split('@');

                    if (logonParams.Length > 1)
                    {
                        // Maybe the email address is username@companylogon.
                        username = logonParams[0];
                        company = logonParams[1];
                    }
                    else
                    {
                        // If we didn't have any luck then try logging on with the logon name instead
                        username = emailAddress;
                        company = string.Empty;
                    }
                }
                users = userBL.GetAllByLogonDetails(username, company, system);
            }

            if (users == null || users.Count == 0)
            {
                AccessHelper.BruteForcePreventionDelay();
                throw new FaultException<InvalidLoginFault>(new InvalidLoginFault(), InvalidLoginFault.FaultDescription);
            }

            if (users.Count > 1)
                throw new FaultException<InvalidLoginFault>(new InvalidLoginFault(), InvalidLoginFault.FaultDescriptionDuplicateEmail);

            if (!string.IsNullOrWhiteSpace(users[0].DebtorNumber))
            {
                var customer = new CustomerBL(uow).Get(users[0].DebtorNumber);

                // Make sure they have access to this website.
                if ((system == SystemType.Contacts && customer.MediaContactsLicenseCount == 0) || !AccessHelper.IsAccessAllowedToWebsite(users[0], system))
                    throw new FaultException<InvalidLoginFault>(new InvalidLoginFault(), InvalidLoginFault.FaultDescription);

                // Make sure user have account in chargebee have access to this website.
                if (IsCustomerIdInChargeBee(users[0].DebtorNumber) == false) 
                    throw new FaultException<InvalidLoginFault>(new InvalidLoginFault(), InvalidLoginFault.FaultDescription);
            }
            else
            {
                throw new FaultException<InvalidLoginFault>(new InvalidLoginFault(), InvalidLoginFault.FaultDescription);
            }
            return users[0];
        }

        /// <summary>
        /// Get  Customer Details From ChargeBee Used For Get Account Details
        /// </summary>
        /// <param name="account"></param>
        /// <param name="customerBL"></param>
        /// <returns></returns>
        private Customer AddChargeBeeCustomer(NewAccountRequest account, CustomerBL customerBL)
        {
            Customer cust = new Customer();
            Customer existingCust;
            
            string custTimezoneCode = account.TimezoneCode;
            CustomerBillingType custAccountType = account.AccountType;
          
            ////Default the timezone if none is supplied
            if (string.IsNullOrWhiteSpace(custTimezoneCode))
                custTimezoneCode = BusinessLayer.Common.Constants.TIMEZONE_SYD;

            //// They can't become Credit Card Default until they are approved.
            if (account.AccountType != CustomerBillingType.Creditcard)
                custAccountType = CustomerBillingType.Creditcard;

            cust.UseMedianetRewrite = true;
            cust.HasTransactions = false;
            cust.Name = account.CompanyName.Truncate(50);
            cust.TimezoneCode = custTimezoneCode;
            cust.AccountType = custAccountType;
            cust.UseMedianetRewrite = true;
            cust.HasTransactions = false;

            
            string custAccountTypeName = GetAccountTypeNameFromAccountType(custAccountType);
            string custStateName = ((StateType[])Enum.GetValues(typeof(StateType))).FirstOrDefault(c => c.ToString() == account.State).GetStringValue().ToString();
      
            string acccountCountryName = "AU";//Default For Australia
                     
            try
            {
                string newDebtorNumber = customerBL.GetNewDebtorNumber();
                cust.DebtorNumber = newDebtorNumber;
                existingCust = customerBL.Get(cust.DebtorNumber);
                // Make sure the DebtorNumber is unique.
                if (existingCust != null)
                    throw new FaultException<NonUniqueFault>(new NonUniqueFault(), string.Format("Customer with DebtorNumber {0} already exists.", cust.DebtorNumber));

                EntityResult result = ChargeBee.Models.Customer.Create().Id(cust.DebtorNumber)
                          .FirstName(account.FirstName)
                          .LastName(account.LastName)
                          .Email(account.EmailAddress)
                          .Phone(account.TelephoneNumber)
                          .Company(account.CompanyName.Truncate(50))
                          .BillingAddressFirstName(account.FirstName)
                          .BillingAddressLastName(account.LastName)
                          .BillingAddressLine1(account.AddressLine1)
                          .BillingAddressLine2(account.AddressLine2)
                          .BillingAddressZip(account.Postcode)
                          .BillingAddressCity(account.City)
                          .BillingAddressState(custStateName)
                          .BillingAddressCountry(acccountCountryName)
                          .BillingAddressEmail(account.EmailAddress)
                          .BillingAddressCompany(account.CompanyName)
                          .BillingAddressPhone(account.TelephoneNumber)
                          .VatNumber(account.ABN) //  VAT Number not applicable for this(Australia) country
                          .Param("cf_customer_status", "Active")//default added as Active
                          .Param("cf_account_type", custAccountTypeName)
                          .Request();
              
                //For adding in old Medinet Customer Details Table 
                cust.FilerCode = cust.DebtorNumber;

                // Just give the LogonName a value, even though we don't use it any more.
                cust.LogonName = account.CompanyName.Replace(" ", "").Truncate(50);

                cust.CreatedDate = DateTime.Now;
                cust.CreatedByUserId = 0;
                cust.LastModifiedDate = cust.CreatedDate;
                cust.LastModifiedByUserId = cust.CreatedByUserId;

                // Add the Customer to the database.
                customerBL.Add(cust);

                return cust;
            }
            catch (Exception ex)
            {
                EmailHelper.GeneralErrorEmail("Error in AddChargeBeeCustomer from Chargebee.", ex);
                throw new FaultException<ServiceFault>(new ServiceFault(), ex.Message);
            }
        }

        /// <summary>
        /// Get CustomerBillingType Enam From Account Type Name
        /// </summary>
        /// <param name="accountTypeName"> This is accountTypeName for getting CustomerBillingType Enam Data</param>
        /// <returns></returns>
        private CustomerBillingType GetAccountTypeFromAccountTypeName(string accountTypeName)
        {
            CustomerBillingType accountType = CustomerBillingType.Creditcard;
            if (!string.IsNullOrEmpty(accountTypeName) && accountTypeName.ToLower() == Constants2.Invoice || !string.IsNullOrEmpty(accountTypeName) && accountTypeName.ToLower() == Constants2.Invoiced)
            {
                accountType = CustomerBillingType.Invoiced;
            }
            return accountType;
        }

        /// <summary>
        ///  Get Account Type Name From CustomerBillingType Enam
        /// </summary>
        /// <param name="accountType"> This is accountType type of CustomerBillingType for getting Account Type Name in String</param>
        /// <returns></returns>
        private string GetAccountTypeNameFromAccountType(CustomerBillingType accountType)
        {
            string accountTypeName = "Creditcard";
            if (accountType == CustomerBillingType.Invoiced)
            {
                accountTypeName = "Invoiced";
            }
            return accountTypeName;
        }

        private void UpdateChargeBeeCustomer(Customer cust, Account account, int userId, CustomerBL customerBL)
        {
            if (account.IsForUpgrade == true)
            {   
                if (cust.MustUseCreditcard)
                    throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), string.Format("Billing type for Customer {0} must stay as Creditcard.", cust.DebtorNumber));
            }
            string acccountCountryName = "AU";//Default For Australia
           
            // Update the Customer in the ChargeBee system.
            try
            {
                string custStateName = ((StateType[])Enum.GetValues(typeof(StateType))).FirstOrDefault(c => c.ToString() == account.CompanyAddress.State).GetStringValue().ToString();
                EntityResult result = ChargeBee.Models.Customer.Update(cust.DebtorNumber)
                             .FirstName(account.FirstName)
                             .LastName(account.LastName)
                             .Email(account.EmailAddress)
                             .Phone(account.TelephoneNumber)
                             .Company(account.CompanyName.Truncate(50))
                             .Request();
                EntityResult customerResult = ChargeBee.Models.Customer.UpdateBillingInfo(cust.DebtorNumber)
                             .BillingAddressFirstName(account.FirstName)
                             .BillingAddressLastName(account.LastName)
                             .BillingAddressLine1(account.CompanyAddress.AddressLine1)
                             .BillingAddressLine2(account.CompanyAddress.AddressLine2)
                             .BillingAddressZip(account.CompanyAddress.Postcode)
                             .BillingAddressCity(account.CompanyAddress.City)
                             .BillingAddressState(custStateName)
                             .BillingAddressCountry(acccountCountryName)// no Country Change
                             .BillingAddressEmail(account.EmailAddress)
                             .BillingAddressCompany(account.CompanyName)
                             .BillingAddressPhone(account.TelephoneNumber)
                             .VatNumber(account.ABN)
                             .Request();
            }
            catch (Exception ex)
            {
                EmailHelper.GeneralErrorEmail("Error updating customer in UpdateChargeBeeCustomer from chargebee.", ex);
                throw new FaultException<ServiceFault>(new ServiceFault(), ex.Message);
            }
            // Update the Customer in the database.
            customerBL.Update(cust, userId);
        }

        /// <summary>
        /// Get List of Subscriptions/Contacts of Customer From CustomerId
        /// </summary>
        /// <param name="customerId">The Customer Id to Get List of Subscriptions</param>
        /// <returns></returns>
        private List<ListResult.Entry> GetContacts(string customerId)
        {
            try
            {
                ListResult result = ChargeBee.Models.Subscription.List().CustomerId().Is(customerId).Request();
                return result.List;
            }
            catch (Exception ex)
            {
                EmailHelper.GeneralErrorEmail("Error in GetContacts for "+ customerId + " from chargebee.", ex);
                throw new FaultException<ServiceFault>(new ServiceFault(), ex.Message);
            }
        }
        
        /// <summary>
        /// Determines whether or not a User exists for a given email address.
        /// </summary>
        /// <param name="emailAddress">The email address to check.</param>
        /// <param name="customerId">The customerId to check is uniqe or not.</param>
        /// <returns>True if the email address and Customer Id is unique. False if it already exists in ChargeBee.</returns>
        private bool IsUniqueEmailWithCustomerId(string emailAddress, string customerId)
        {
            try
            {
                ListResult result = ChargeBee.Models.Customer.List().Email().Is(emailAddress).Id().IsNot(customerId).Request();
                if (result.List.Count > 0)
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                EmailHelper.GeneralErrorEmail("Error IsUniqueEmail from ChargeBee.", ex);
                throw new FaultException<ServiceFault>(new ServiceFault(), ex.Message);
            }
            return true;
        }
   
        /// <summary>
        /// Determines whether or not a Customer exists for a given customerId in ChargeBee.
        /// </summary>
        /// <param name="customerId">The customerId to check is uniqe or not.</param>
        /// <returns>True if the email address and Customer Id is unique. False if it already exists in ChargeBee.</returns>
        private bool IsCustomerIdInChargeBee(string customerId)
        {
            try
            {
                ListResult result = ChargeBee.Models.Customer.List().Id().Is(customerId).Request();
                if (result.List.Count > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                EmailHelper.GeneralErrorEmail("Error IsUniqCustomerId from ChargeBee for customerId : " + customerId, ex);
            }
            return false;
        }
        
        #endregion
    }
}
