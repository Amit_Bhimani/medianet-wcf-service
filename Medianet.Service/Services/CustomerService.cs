﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using AutoMapper;
using Medianet.Wcf;
using Medianet.Service.Contracts;
using Medianet.Service.Dto;
using Medianet.Service.Common;
using Medianet.Service.FaultExceptions;
using Medianet.DataLayer;
using Medianet.Service.BusinessLayer;
using Medianet.Service.BusinessLayer.Exceptions;
using System.Configuration;
using Medianet.Salesforce.Services;
using Medianet.Service.FinanceCustomerService;
using Medianet.Service.BusinessLayer.Common;

namespace Medianet.Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [ErrorBehaviorAttribute(typeof(ServiceErrorHandler))]
    public class CustomerService : ICustomerService
    {
        private const string CONST_RIJN_PASS = "m3d1an3td15tr18ut10n";
        private const string CONST_RIJN_SALT = "3ncrypt10n5alt5";
        private const string CONST_RIJN_HASH_ALG = "SHA1";
        private const int CONST_RIJN_PASS_ITERATE = 2;
        private const string CONST_RIJN_VECT = "99#$MdeA20oo13ba";
        private const int CONST_RIJN_KSIZE = 256;

        public CustomerService()
        {
            // The windows service host normally does this but we do it here when running in debug mode.
            if (!AutoMapperConfigurator.IsInitialized)
                AutoMapperConfigurator.Configure();
        }

        /// <summary>
        /// Determines whether or not a User exists for a given email address.
        /// </summary>
        /// <param name="emailAddress">The email address to check.</param>
        /// <returns>True if the email address is unique. False if it already exists in our database.</returns>
        public bool IsUniqueEmail(string emailAddress)
        {
            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);
                List<User> existingUsers = userBL.GetAllByEmailAddress(emailAddress);

                if (existingUsers.Count > 0)
                {
                    return false;
                }

                return true;
            }
        }

        /// <summary>
        /// Add a new Customer and User to the database based on basic Account details.
        /// </summary>
        /// <param name="account">A NewAccountRequest object containing the Customer and User details.</param>
        /// <returns>The DebtorNumber generated for this customer.</returns>
        public string AddAccount(NewAccountRequest account)
        {
            if (string.IsNullOrWhiteSpace(account.EmailAddress))
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), string.Format("EmailAddress must not be blank."));
            if (string.IsNullOrWhiteSpace(account.Password))
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), string.Format("Password must not be blank."));
            if (string.IsNullOrWhiteSpace(account.CompanyName))
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), string.Format("CompanyName must not be blank."));
            if (string.IsNullOrWhiteSpace(account.FirstName))
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), string.Format("FirstName must not be blank."));

            Customer cust = new Customer();

            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);
                var customerBL = new CustomerBL(uow);
                List<User> existingUsers = userBL.GetAllByEmailAddress(account.EmailAddress);

                if (existingUsers.Count > 0)
                    throw new FaultException<NonUniqueFault>(new NonUniqueFault(), string.Format("User with EmailAddress {0} already exists.", account.EmailAddress));

                Customer existingCust = customerBL.GetByName(account.CompanyName);
                if (existingCust != null)
                    throw new FaultException<NonUniqueFault>(new NonUniqueFault(), string.Format("Customer with Name {0} already exists.", account.CompanyName));

                // Create the customer both in Finance and here
                cust = AddCustomer(account, customerBL);

                // Create the user.
                byte[] salt = PasswordHelper.GenerateSalt();
                User user = new User
                {
                    FirstName = account.FirstName,
                    LastName = account.LastName,
                    Position = account.ContactPosition,
                    DebtorNumber = cust.DebtorNumber,
                    EmailAddress = account.EmailAddress,
                    TelephoneNumber = account.TelephoneNumber,
                    FaxNumber = account.FaxNumber,
                    HashedPassword = PasswordHelper.HashPassword(account.Password, salt),
                    Salt = Convert.ToBase64String(salt),
                    ExpiryToken = Guid.NewGuid(),
                    HasDistributeWebAccess = account.System == SystemType.Medianet,
                    LogonName = $"{account.FirstName}{account.LastName}".Replace(" ", "")
                };

                // Add the User to the database.
                user.Id = userBL.Add(user);

                customerBL.AcknowledgeTermsRead(user.Id, $"{account.FirstName} {account.LastName}".Trim(), account.EmailAddress, SystemType.Medianet);

                // Now set the create and modified UserId of the Customer record to the new User.
                cust.CreatedByUserId = user.Id;
                customerBL.Update(cust, user.Id);
            }

            EmailHelper.NewAccountEmail($"{account.FirstName} {account.LastName}".Trim(), cust.Name, cust.DebtorNumber);

            return cust.DebtorNumber;
        }

        /// <summary>
        /// Add a new Customer to the database for the purpose of enroling in a training course.
        /// </summary>
        /// <param name="account">A NewAccountRequest object containing the Customer details.</param>
        /// <returns>The DebtorNumber generated for this customer.</returns>
        public string CreateCustomerForCourse(NewAccountRequest account)
        {
            if (string.IsNullOrWhiteSpace(account.EmailAddress))
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), string.Format("EmailAddress must not be blank."));
            if (string.IsNullOrWhiteSpace(account.CompanyName))
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), string.Format("CompanyName must not be blank."));
            if (string.IsNullOrWhiteSpace(account.FirstName))
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), string.Format("FirstName must not be blank."));

            Customer cust;

            using (var uow = new UnitOfWork())
            {
                var customerBL = new CustomerBL(uow);

                account.CompanyName = account.CompanyName.Truncate(50);
                cust = customerBL.GetByName(account.CompanyName);

                if (cust == null)
                {
                    cust = AddCustomer(account, customerBL);
                }
                else
                {
                    // We have a customer match. Make sure the customer also exists in finance.
                    MedianetCustomer financeCust = null;

                    try
                    {
                        financeCust = FinanceHelper.GetCustomer(cust.DebtorNumber);
                    }
                    catch (Exception ex)
                    {
                        NLog.LogManager.GetCurrentClassLogger()
                            .LogException(NLog.LogLevel.Error,
                                "Error fetching customer with DebtorNumber " + cust.DebtorNumber + " from finance.", ex);
                    }

                    if (financeCust == null)
                        throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(),
                            string.Format("Customer with name {0} exists but is invalid. Please select another name.",
                                account.CompanyName));

                    var existingAccount = AutoMapper.Mapper.Map<NewAccountRequest, Account>(account);

                    existingAccount.BillingAddress = existingAccount.CompanyAddress;
                    UpdateCustomer(cust, existingAccount, true, 0, customerBL);
                }
            }

            return cust.DebtorNumber;
        }

        /// <summary>
        /// Get the account details of a User.
        /// </summary>
        /// <param name="userId">The Id of the User to fetch account details for.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The account details of the User.</returns>
        public Account GetAccount(int userId, string sessionKey)
        {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);

            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);
                var customerBL = new CustomerBL(uow);

                User user = userBL.Get(userId);

                if (user == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("User with Id {0} not found.", userId));

                Customer cust = customerBL.Get(user.DebtorNumber);

                if (cust == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Customer with DebtorNumber {0} not found.", user.DebtorNumber));

                // Populate the user data.
                Account account = new Account
                {
                    UserId = user.Id,
                    DebtorNumber = user.DebtorNumber,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    ContactPosition = user.Position,
                    EmailAddress = user.EmailAddress,
                    FaxNumber = user.FaxNumber,
                    UserLogonName = user.LogonName,
                    Password = string.Empty, // Make it blank for security reasons.
                    CompanyName = cust.Name,
                    CompanyLogonName = cust.LogonName,
                    AccountType = cust.AccountType,
                    TimezoneCode = cust.TimezoneCode,
                    Timezone = cust.Timezone
                };

                if (!AccessHelper.IsVerifiedCustomer(user.DebtorNumber))
                {
                    // If the account is the unverified account, work some special magic to set the account/user details
                    // this is to allow the website to properly prefill fields when the user decides to upgrade to a true account
                    account.IndustryCode = user.ShortName;
                    account.ContactPosition = "";
                    account.AccountType = CustomerBillingType.Creditcard;
                }
                else
                {
                    // Get the Customer details from the Finance system.
                    try
                    {
                        FinanceCustomerService.MedianetCustomer financeCust = FinanceHelper.GetCustomer(user.DebtorNumber);
                        account.CompanyName = financeCust.CustomerName;
                        account.IndustryCode = financeCust.IndustryCode;
                        account.ABN = financeCust.ABN;

                        if (financeCust.PrimaryAddress != null)
                        {
                            account.TelephoneNumber = financeCust.PrimaryAddress.Phone1;

                            account.CompanyAddress = AutoMapper.Mapper.Map<FinanceCustomerService.MedianetCustomerAddress, AccountAddress>(financeCust.PrimaryAddress);
                        }

                        if (financeCust.BillingAddress != null)
                            account.BillingAddress = AutoMapper.Mapper.Map<FinanceCustomerService.MedianetCustomerAddress, AccountAddress>(financeCust.BillingAddress);
                        else
                            account.BillingAddress = new AccountAddress();
                    }
                    catch (FaultException<FinanceCustomerService.AAPInternalCustomer>)
                    {
                        // This is an internal customer so Finance don't have details. Ignore the error.
                        account.CompanyAddress = new AccountAddress();
                        account.BillingAddress = new AccountAddress();
                    }
                }
                return account;
            }
        }

        /// <summary>
        /// Update a User record and their Customer record based on information in an Account object.
        /// </summary>
        /// <param name="account">An Account object containing basic information to be updated.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        public void UpdateAccount(Account account, string sessionKey)
        {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);

            if (string.IsNullOrWhiteSpace(account.EmailAddress))
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), string.Format("EmailAddress must not be blank."));
            if (string.IsNullOrWhiteSpace(account.CompanyName))
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), string.Format("CompanyName must not be blank."));
            if (string.IsNullOrWhiteSpace(account.FirstName))
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), string.Format("FirstName must not be blank."));

            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);
                var customerBL = new CustomerBL(uow);
                List<User> existingUsers = userBL.GetAllByEmailAddress(account.EmailAddress);

                if (existingUsers.Exists(u => u.Id != account.UserId))
                    throw new FaultException<NonUniqueFault>(new NonUniqueFault(), string.Format("User with EmailAddress {0} already exists.", account.EmailAddress));

                User user = userBL.Get(account.UserId);

                if (user == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("User with Id {0} not found.", account.UserId));

                Customer cust = customerBL.Get(user.DebtorNumber);

                if (cust == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Customer with DebtorNumber {0} not found.", user.DebtorNumber));

                if (AccessHelper.IsVerifiedCustomer(user.DebtorNumber))
                {
                    UpdateCustomer(cust, account, false, session.UserId, customerBL);
                }

                // Update the user.
                user.FirstName = account.FirstName.Truncate(50);
                user.LastName = account.LastName.Truncate(50);
                user.Position = account.ContactPosition.Truncate(50);
                user.DebtorNumber = session.DebtorNumber;
                user.EmailAddress = account.EmailAddress.Truncate(200);
                user.TelephoneNumber = account.TelephoneNumber.Truncate(24);
                user.FaxNumber = account.FaxNumber.Truncate(24);

                // If a new password was provided then update it too.
                if (!string.IsNullOrWhiteSpace(account.Password))
                {
                    if (!session.IsAuthorised)
                    {
                        throw new FaultException<NotFullyAuthorisedFault>(new NotFullyAuthorisedFault(), string.Format("Authorisation level does not permit a password to be changed."));
                    }
                    user.MustChangePassword = false;
                    byte[] salt = PasswordHelper.GenerateSalt();
                    user.HashedPassword = PasswordHelper.HashPassword(account.Password, salt);
                    user.Salt = Convert.ToBase64String(salt);
                    user.ExpiryToken = Guid.NewGuid();
                }

                // Update the User in the database.
                userBL.Update(user, session.UserId);
            }
        }

        /// <summary>
        /// Add a new Registration validation to the database and send
        /// an email to the user asking them to verify thier email address.
        /// </summary>
        /// <param name="emailAddress">The EmailAddress of the user registering.</param>
        /// <param name="firstName">The first name of the user registering.</param>
        /// <param name="system"></param>
        /// <returns>The unique Guid generated for this registration.</returns>
        public Guid AddRegistrationValidation(string emailAddress, string firstName, SystemType system)
        {
            if (string.IsNullOrWhiteSpace(emailAddress))
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), string.Format("EmailAddress must not be blank."));
            if (string.IsNullOrWhiteSpace(firstName))
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), string.Format("FirstName must not be blank."));

            var rego = new RegistrationValidation();

            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);
                var registrationBL = new RegistrationValidationBL(uow);
                List<User> existingUsers = userBL.GetAllByEmailAddress(emailAddress);

                if (existingUsers.Count > 0)
                    throw new FaultException<NonUniqueFault>(new NonUniqueFault(), string.Format("User with EmailAddress {0} already exists.", emailAddress));

                // Create the user.
                rego.System = system;
                rego.FirstName = firstName.Truncate(50);
                rego.EmailAddress = emailAddress.Truncate(200);
                rego.CreatedDate = DateTime.Now;

                // Add the User to the database.
                rego.Token = registrationBL.Add(rego);
            }

            SendRegistrationValidationEmail(system, rego);
            return rego.Token;
        }

        /// <summary>
        /// Get the details of a potential User waiting to be validated via email.
        /// </summary>
        /// <param name="token">The GUID to fetch registration details for.</param>
        /// <returns>The account details of the User.</returns>
        public RegistrationValidation GetRegistrationValidation(Guid token)
        {
            RegistrationValidation rego = null;

            using (var uow = new UnitOfWork())
            {
                var registrationBL = new RegistrationValidationBL(uow);
                var releasePreviewBL = new ReleaseUnverifiedPreviewBL(uow);

                rego = registrationBL.Get(token);
                rego.ReleaseUnverifiedPreview = releasePreviewBL.Get(token);

                if (rego == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Registration validation token {0} not found.", token));
            }

            return rego;
        }

        /// <summary>
        /// Get the details of a Customer.
        /// </summary>
        /// <param name="debtorNumber">The DebtorNumber of the Customer to get.</param>
        /// <returns>The details of the Customer.</returns>
        public FinanceCustomerService.MedianetCustomer GetCustomerByDebtorNumber(string debtorNumber)
        {
            return FinanceHelper.GetCustomer(debtorNumber);
        }

        /// <summary>
        /// Logs a user in to the specified System using a username, company logon and password.
        /// </summary>
        /// <param name="username">The username of the User.</param>
        /// <param name="companyLogon">The logon name of the Company the User belongs to.</param>
        /// <param name="password">The password of the User.</param>
        /// <param name="system">The system being logged in to.</param>
        /// <returns>A DBSession object containing the User and Customer details.</returns>
        public DBSession Login(string username, string companyLogon, string password, SystemType system)
        {
            DBSession session;

            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);
                var user = GetUserByEmailAddressOrLogonName(username, companyLogon, system, userBL, uow);

                bool passwordsEqual = PasswordHelper.ComparePasswords(PasswordHelper.HashPassword(password, Convert.FromBase64String(user.Salt)), user.HashedPassword);

                if (!passwordsEqual)
                {
                    userBL.UpdateLogonAttempt(user.Id, null, system, false);

                    AccessHelper.BruteForcePreventionDelay();
                    throw new FaultException<InvalidLoginFault>(new InvalidLoginFault(), InvalidLoginFault.FaultDescription);
                }

                // Check if they need to agree to terms & conditions
                CheckTermsAndConditions(user, system);

                session = CreateSession(user, system);

                userBL.UpdateLogonAttempt(user.Id, session.LoginDate, system, true);

                return session;
            }
        }

        /// <summary>
        /// Log a user in to the specified System using an email adress and password.
        /// </summary>
        /// <param name="emailAddress">The EmailAddress of the User.</param>
        /// <param name="password">The password of the User.</param>
        /// <param name="system">The system being logged in to.</param>
        /// <returns>A DBSession object containing the User and Customer details.</returns>
        public DBSession LoginUsingEmailAddress(string emailAddress, string password, SystemType system)
        {
            User user;
            DBSession session;

            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);

                user = GetUserByEmailAddressOrLogonName(emailAddress, string.Empty, system, userBL, uow);

                bool passwordsEqual = PasswordHelper.ComparePasswords(PasswordHelper.HashPassword(password, Convert.FromBase64String(user.Salt)), user.HashedPassword);

                if (!passwordsEqual)
                {
                    userBL.UpdateLogonAttempt(user.Id, null, system, false);

                    AccessHelper.BruteForcePreventionDelay();
                    throw new FaultException<InvalidLoginFault>(new InvalidLoginFault(), InvalidLoginFault.FaultDescription);
                }

                // Check if they need to agree to terms & conditions
                CheckTermsAndConditions(user, system);

                session = CreateSession(user, system);

                userBL.UpdateLogonAttempt(user.Id, session.LoginDate, system, true);

                return session;
            }
        }

        /// <summary>
        /// Log a user in to the specified System using a session created after logging into another system.
        /// </summary>
        /// <param name="sessionKey">The SessionKey to use to login to another System.</param>
        /// <param name="system">The system being logged in to.</param>
        /// <returns>A DBSession object containing the User and Customer details.</returns>
        public DBSession LoginUsingSession(string sessionKey, SystemType system)
        {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);
            User user;

            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);
                user = userBL.Get(session.UserId);

                if (user == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("User with Id {0} not found.", session.UserId));

                // Check if they need to agree to terms & conditions
                CheckTermsAndConditions(user, system);

                session = CreateSession(user, system, session.System == SystemType.Admin);

                userBL.UpdateLogonAttempt(user.Id, session.LoginDate, system, true);

                return session;
            }
        }

        /// <summary>
        /// Log a user in to the specified System using an existing session for this System. The old session is killed.
        /// </summary>
        /// <param name="sessionKey">The SessionKey to use to login to the System.</param>
        /// <param name="system">The system being logged in to.</param>
        /// <returns>A DBSession object containing the User and Customer details.</returns>
        public DBSession LoginReplacingSession(string sessionKey, SystemType system)
        {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);
            User user;

            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);
                var sessionBL = new DBSessionBL(uow);

                user = userBL.Get(session.UserId);

                if (user == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("User with Id {0} not found.", session.UserId));

                sessionBL.Logout(sessionKey);

                // Check if they need to agree to terms & conditions
                CheckTermsAndConditions(user, system);

                session = CreateSession(user, system);

                userBL.UpdateLogonAttempt(user.Id, session.LoginDate, system, true);

                return session;
            }
        }

        /// <summary>
        /// Log a user in to the specified System impersonating someone else using an admin session.
        /// </summary>
        /// <param name="sessionKey">The SessionKey to use to login to another System.</param>
        /// <param name="userId">The Id of the User to log in as.</param>
        /// <param name="system">The system being logged in to.</param>
        /// <returns>A DBSession object containing the User and Customer details.</returns>
        public DBSession LoginImpersonationUsingSession(string sessionKey, int userId, SystemType system)
        {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);
            User user;

            // Make sure this user is allowed to impersonate people.
            if (!AccessHelper.IsAccessAllowedToImpersonate(session.System, system))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Impersonating Access denied.");

            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);

                user = userBL.Get(userId);

                if (user == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("User with Id {0} not found.", session.UserId));

                session = CreateSession(user, system);

                return session;
            }
        }

        /// <summary>
        /// Creates an unauthorised session within the specified system.
        /// </summary>
        /// <param name="emailAddress">The email address of the user to create the session for.</param>
        /// <param name="expiryToken">A unique identifier that specified the user concurrency details.</param>
        /// <param name="system">The system to create a session for.</param>
        /// <returns>A DBSession object where the session is not fully authorised (limited access to key areas).</returns>
        public DBSession LoginPartialAuthorisation(string emailAddress, Guid expiryToken, SystemType system)
        {
            User user;
            DBSession session;

            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);

                user = GetUserByEmailAddressOrLogonName(emailAddress, string.Empty, system, userBL, uow);

                if (!user.ExpiryToken.Equals(expiryToken))
                {
                    userBL.UpdateLogonAttempt(user.Id, null, system, false);

                    AccessHelper.BruteForcePreventionDelay();
                    throw new FaultException<InvalidLoginFault>(new InvalidLoginFault(), InvalidLoginFault.FaultDescription);
                }

                // Check if they need to agree to terms & conditions
                CheckTermsAndConditions(user, system);

                session = CreateSession(user, system);
                session.IsAuthorised = false;

                userBL.UpdateLogonAttempt(user.Id, session.LoginDate, system, true);
            }

            return session;
        }

        /// <summary>
        /// Sends an email to someone with a link allowing them to reset their password.
        /// </summary>
        /// <param name="emailAddress">The EmailAddress of the User. For the Journalists website this may be just a username.</param>
        /// <param name="system">The system being logged in to.</param>
        public void SendForgotPasswordEmail(string emailAddress, SystemType system)
        {
            string url;
            string firstName;
            string toAddress = emailAddress;
            string product;

            List<User> users;
            User user;
            DBSession session;

            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);
                users = userBL.GetAllByEmailAddress(emailAddress);

                if (users.Count == 0)
                {
                    // Maybe the email address is username@companylogon or just username for the journalists website. Check for this.
                    string[] logonParams = emailAddress.Split('@');
                    users = userBL.GetAllByLogonDetails(logonParams[0], logonParams.Length > 1 ? logonParams[1] : string.Empty, system);
                }

                if (users.Count == 0)
                {
                    AccessHelper.BruteForcePreventionDelay();
                    throw new FaultException<InvalidLoginFault>(new InvalidLoginFault(), InvalidLoginFault.FaultDescription);
                }

                if (users.Count > 1)
                    throw new FaultException<InvalidLoginFault>(new InvalidLoginFault(), InvalidLoginFault.FaultDescriptionDuplicateEmail);

                user = users[0];

                session = CreateSession(user, system, false, false, true);

                // Make sure the user must change their email on next logon.
                user.MustChangePassword = true;
                user.LastLogonDate = session.LoginDate;
                userBL.Update(user, user.Id);

                firstName = user.FirstName.ProperCase();
                toAddress = user.EmailAddress;
            }

            switch (system)
            {
                case SystemType.Contacts:
                    url = ConfigurationManager.AppSettings["ContactsForgotPasswordURL"] + session.Key;
                    product = "Medianet's Media Database";
                    break;
                case SystemType.Journalists:
                    url = ConfigurationManager.AppSettings["JournalistsForgotPasswordURL"] + session.Key;
                    product = "the Medianet for Journalist";
                    break;
                default:
                    url = ConfigurationManager.AppSettings["DistributionForgotPasswordURL"] + session.Key;
                    product = "Medianet";
                    break;
            }

            if (string.IsNullOrEmpty(firstName))
                firstName = "Medianet Customer";

            // Send an email to them with a link allowing them to change their password.
            EmailHelper.ForgotPasswordEmail(firstName, product, toAddress, url);
        }

        /// <summary>
        /// Change a Users password and create a new session if currently using a session designed for changing passwords only.
        /// </summary>
        /// <param name="password">The new password.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A DBSession object containing the User and Customer details.</returns>
        public DBSession ChangePassword(string password, string sessionKey)
        {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey, true);
            User user;

            if (!session.IsAuthorised)
                throw new FaultException<NotFullyAuthorisedFault>(new NotFullyAuthorisedFault(), string.Format("Please verify your credentials by logging in before password changes can be processed."));

            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);
                //bool IsVerified;

                user = userBL.Get(session.UserId);

                if (user == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("User with Id {0} not found.", session.UserId));

                byte[] salt = PasswordHelper.GenerateSalt();
                user.Salt = Convert.ToBase64String(salt);
                user.HashedPassword = PasswordHelper.HashPassword(password, salt);
                user.MustChangePassword = false;

                // Reset this so previously used links to logon for the first time can't be used forever
                user.ExpiryToken = Guid.NewGuid();

                userBL.Update(user, user.Id);

                //IsVerified = AccessHelper.IsVerifiedCustomer(user.DebtorNumber);

                // If they only have a temporary ChangingPassword session then create a proper one.
                if (session.RowStatus == RowStatusType.ChangingPassword)
                {
                    var sessionBL = new DBSessionBL(uow);
                    sessionBL.Logout(sessionKey);

                    // Check if they need to agree to terms & conditions
                    CheckTermsAndConditions(user, session.System);

                    session = CreateSession(user, session.System);
                    userBL.UpdateLogonAttempt(user.Id, session.LoginDate, session.System, true);
                }
                else
                {
                    // Get the session again to get changes to the User made above.
                    session = AccessHelper.GetSessionFromKey(session.Key);
                }

                return session;
            }
        }

        /// <summary>
        /// Authorise a session by providing the session key and current user password.
        /// </summary>
        /// <param name="sessionKey">The key of the current session.</param>
        /// <param name="password">The password to Authorise the user.</param>
        /// <returns>On success, returns an authorised session. Throws an exception when session or password is invalid.</returns>
        public DBSession AuthoriseSession(string sessionKey, string password)
        {
            DBSession session = null;

            try
            {
                using (var uow = new UnitOfWork())
                {
                    var sessionBL = new DBSessionBL(uow);

                    session = sessionBL.Authorise(sessionKey, password);

                    // If it's an invalid session then sleep a bit to prevent brute force guessing.
                    if (session == null)
                        AccessHelper.BruteForcePreventionDelay();
                }
            }
            catch (BusinessLayer.Exceptions.AuthenticationFailureException ex)
            {
                throw new FaultException<InvalidLoginFault>(new InvalidLoginFault(), ex.Message);
            }
            catch (BusinessLayer.Exceptions.SessionInvalidException ex)
            {
                throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), ex.Message);
            }

            return session;
        }

        /// <summary>
        /// Validate a sessionKey and sets the LastAccessedDate to the current time.
        /// </summary>
        /// <param name="sessionKey">The SessionKey to validate.</param>
        /// <returns>The DBSession object containing the User and Customer details.</returns>
        public DBSession ValidateSession(string sessionKey)
        {
            using (var uow = new UnitOfWork())
            {
                var sessionBL = new DBSessionBL(uow);
                DBSession session = null;

                session = sessionBL.Validate(sessionKey);

                // If it's an invalid session then sleep a bit to prevent brute force guessing.
                if (session == null)
                    AccessHelper.BruteForcePreventionDelay();

                return session;
            }
        }

        /// <summary>
        /// Log the user out by setting the DBSession to inactive.
        /// </summary>
        /// <param name="sessionKey">The SessionKey to set to inactive.</param>
        public void Logout(string sessionKey)
        {
            NLog.LogManager.GetCurrentClassLogger().Log(NLog.LogLevel.Info, "Logout:SessionKey=" + sessionKey);

            using (var uow = new UnitOfWork())
            {
                var sessionBL = new DBSessionBL(uow);
                sessionBL.Logout(sessionKey);
            }
        }

        /// <summary>
        /// Gets the details of all Users that belong to a Customer.
        /// </summary>
        /// <param name="debtorNumber">The DebtorNumber of the Customer to filter users by.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The details of all the Users found.</returns>
        public List<User> GetAllUsersByCustomer(string debtorNumber, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAdminOrSameCustomer(debtorNumber, sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to Users for DebtorNumber {0} denied.", debtorNumber));

            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);
                List<User> userList = userBL.GetAllByDebtorNumber(debtorNumber);

                return userList;
            }
        }

        /// <summary>
        /// Gets the details of a User.
        /// </summary>
        /// <param name="userId">The Id of the User to get.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The details of the User.</returns>
        public User GetUserById(int userId, string sessionKey)
        {
            // TODO: Blank out the password.
            throw new NotImplementedException();
        }

        /// <summary>
        /// Updates the details of a User.
        /// </summary>
        /// <param name="user">The details of the User.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        public void UpdateUser(User user, string sessionKey)
        {
            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);
                userBL.Update(user, user.Id);
            }
        }

        /// <summary>
        /// Gets a list of industry codes for a customer to be categorised into.
        /// </summary>
        /// <returns>A list of industry codes.</returns>
        public List<IndustryCode> GetIndustryCodes()
        {
            return FinanceHelper.GetIndustryCodes();
        }

        /// <summary>
        /// Gets a list of billing codes for a customer.
        /// </summary>
        /// <param name="debtorNumber">The DebtorNumber of the Customer to get billing codes for.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A list of industry codes.</returns>
        public List<CustomerBillingCode> GetCustomerBillingCodes(string debtorNumber, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAdminOrSameCustomer(debtorNumber, sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to Billing Codes for DebtorNumber {0} denied.", debtorNumber));

            using (var uow = new UnitOfWork())
            {
                var costBL = new CustomerBillingCodeBL(uow);
                List<CustomerBillingCode> costList = costBL.GetAllByDebtorNumber(debtorNumber);

                return costList;
            }
        }

        /// <summary>
        /// Get the details of a Sales Region
        /// </summary>
        /// <param name="regionId">The Id of the region to get</param>
        /// <returns>The SalesRegion</returns>
        public SalesRegion GetSalesRegion(int regionId)
        {
            using (var uow = new UnitOfWork())
            {
                var regionBL = new SalesRegionBL(uow);
                var region = regionBL.Get(regionId);

                if (region == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("SalesRegion with Id {0} not found.", regionId));

                return region;
            }
        }

        /// <summary>
        /// Acknowledge that a User has read a message for a particular system.
        /// </summary>
        /// <param name="userId">The Id of the User that read the message.</param>
        /// <param name="system">The system the message was read from.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        public void AcknowledgeUserMessageRead(int userId, SystemType system, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAdminOrSameUser(userId, sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to User with Id {0} denied.", userId));

            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);
                userBL.AcknowledgeMessageRead(userId);
            }
        }

        /// <summary>
        /// Acknowledge that a User has read the terms and conditions on dehalf of a customer for a particular system.
        /// </summary>
        /// <param name="userId">The Id of the User that read the terms and conditions.</param>
        /// <param name="system">The system the terms and conditions was read from.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        public void AcknowledgeTermsRead(int userId, SystemType system, string sessionKey)
        {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdminOrSameUser(userId, session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to User with Id {0} denied.", userId));

            string primaryName = string.Empty;
            string primaryEmailAddress = string.Empty;

            if (!BusinessLayer.Common.Constants.DEBTOR_NUMBERS_INTERNAL.Contains(session.Customer.DebtorNumber))
            {
                try
                {
                    var x = FinanceHelper.GetCustomer(session.DebtorNumber);

                    if (x.PrimaryAddress != null)
                        primaryName = x.PrimaryAddress.ContactPerson;

                    if (x.ToEmail != null)
                        primaryEmailAddress = string.Join(";", x.ToEmail);
                }
                catch (Exception ex)
                {
                    NLog.LogManager.GetCurrentClassLogger()
                        .Log(NLog.LogLevel.Error,
                            string.Format(
                                "AcknowledgeTermsRead - Failed to find customer with DebtorNumber {0} in finance. {1}",
                                session.DebtorNumber, ex.ToString()));
                }
            }

            using (var uow = new UnitOfWork())
            {
                var customerBL = new CustomerBL(uow);
                customerBL.AcknowledgeTermsRead(userId, primaryName, primaryEmailAddress, system);
            }
        }

        /// <summary>
        /// Acknowledge that a User has read the terms and conditions on dehalf of a customer for a particular system.
        /// Once done they are logged on.
        /// </summary>
        /// <param name="emailAddress">The EmailAddress of the User.</param>
        /// <param name="password">The password of the User.</param>
        /// <param name="system">The system being logged in to.</param>
        /// <returns>A DBSession object containing the User and Customer details.</returns>
        public DBSession AcknowledgeTermsReadAndLogin(string emailAddress, string password, SystemType system)
        {
            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);

                User user = GetUserByEmailAddressOrLogonName(emailAddress, string.Empty, system, userBL, uow);

                bool passwordsEqual = PasswordHelper.ComparePasswords(PasswordHelper.HashPassword(password, Convert.FromBase64String(user.Salt)), user.HashedPassword);

                if (!passwordsEqual)
                {
                    userBL.UpdateLogonAttempt(user.Id, null, system, false);

                    AccessHelper.BruteForcePreventionDelay();
                    throw new FaultException<InvalidLoginFault>(new InvalidLoginFault(), InvalidLoginFault.FaultDescription);
                }

                string primaryName = string.Empty;
                string primaryEmailAddress = string.Empty;

                if (!BusinessLayer.Common.Constants.DEBTOR_NUMBERS_INTERNAL.Contains(user.DebtorNumber))
                {
                    try
                    {
                        var x = FinanceHelper.GetCustomer(user.DebtorNumber);

                        if (x.PrimaryAddress != null)
                            primaryName = x.PrimaryAddress.ContactPerson;

                        if (x.ToEmail != null)
                            primaryEmailAddress = string.Join(";", x.ToEmail);
                    }
                    catch (Exception ex)
                    {
                        NLog.LogManager.GetCurrentClassLogger().Log(NLog.LogLevel.Error,
                                string.Format("AcknowledgeTermsRead - Failed to find customer with DebtorNumber {0} in finance. {1}",
                                user.DebtorNumber, ex.ToString()));
                    }
                }

                var customerBL = new CustomerBL(uow);
                customerBL.AcknowledgeTermsRead(user.Id, primaryName, primaryEmailAddress, system);
                DBSession session = CreateSession(user, system);

                userBL.UpdateLogonAttempt(user.Id, session.LoginDate, system, true);

                return session;
            }
        }

        /// <summary>
        /// Get statistics for a Customer.
        /// </summary>
        /// <param name="debtorNumber">The DebtorNumber of the Customer to get statistics for.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns></returns>
        public CustomerStatistic GetCustomerStatistics(string debtorNumber, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAdminOrSameCustomer(debtorNumber, sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to Customer with Id {0} denied.", debtorNumber));

            using (var uow = new UnitOfWork())
            {
                var custBL = new CustomerBL(uow);
                return custBL.GetStatistics(debtorNumber);
            }
        }

        public SecurePassword HashPassword(string password)
        {
            SecurePassword sPass = null;

            if (!string.IsNullOrEmpty(password))
            {
                byte[] salt = PasswordHelper.GenerateSalt();
                sPass = new SecurePassword();
                sPass.Salt = Convert.ToBase64String(salt);
                sPass.HashedPassword = PasswordHelper.HashPassword(password, salt);
            }

            return sPass;
        }

        public List<AAPContract> GetCustomerContracts(string debtorNumber)
        {
            var forceSettings = GetForceSettings();
            var customerNo = GetCustomerNo(debtorNumber);
            var contractService = new ForceContractService(forceSettings);
            var contracts = contractService.GetAAPContracts(customerNo).Result;
            var aapcontracts = Mapper.Map<List<Salesforce.Models.AAPContract>, List<Service.Dto.AAPContract>>(contracts);

            return aapcontracts;
        }

        public SalesForceContact GetUserProfile(string emailAddress, string debtorNumber)
        {
            var forceSettings = GetForceSettings();

            var customerNo = GetCustomerNo(debtorNumber);

            var contactService = new ForceContactService(forceSettings);
            var contacts = contactService.GetContacts(emailAddress, customerNo).Result;
            var sfcontacts = Mapper.Map<List<Salesforce.Models.SalesForceContact>, List<Service.Dto.SalesForceContact>>(contacts);

            if (sfcontacts.Count > 0)
            {
                return sfcontacts[0];
            }

            return null;
        }
        
        public List<string> GetSubjectsOfInterests()
        {
            var forceSettings = GetForceSettings();
            var generalService = new ForceGeneralService(forceSettings);
            return generalService.GetPickListValues(ForceGeneralService.ContactObject,
                ForceGeneralService.ContactSubjectOfInterestField).Result;
        }

        public bool UpdateduserSubjectsOfInterest(string emailAddress, string debtorNumber, List<string> subjectsOfInterest)
        {
            var forceSettings = GetForceSettings();
            var contactService = new ForceContactService(forceSettings);
            string customerNo = GetCustomerNo(debtorNumber);
            var contacts = contactService.GetContacts(emailAddress, customerNo).Result;
            return contactService.UpdateSubjectCategory(contacts[0].Id, subjectsOfInterest).Result;
        }

        private string GetCustomerNo(string debtorNumber)
        {
            int custNo;
            var isnumeric = int.TryParse(debtorNumber, out custNo);
            if (isnumeric)
            {
                return custNo.ToString();
            }
            return debtorNumber;
        }

        private static ForceSettings GetForceSettings()
        {
            var forceSettings = new ForceSettings()
            {
                ClientId = ConfigurationManager.AppSettings["SalesForceClientId"],
                ClientSecret = ConfigurationManager.AppSettings["SalesForceClientSecret"],
                UserName = ConfigurationManager.AppSettings["SalesForceUserName"],
                Password = ConfigurationManager.AppSettings["SalesForcePassword"],
                Url = ConfigurationManager.AppSettings["SalesForceUrl"]
            };
            return forceSettings;
        }

        #region Private methods

        private void CheckTermsAndConditions(User user, SystemType system)
        {
            // If logging on to the contacts website check if they need to agree to terms & conditions
            if (system == SystemType.Contacts)
            {
                using (var uow = new UnitOfWork())
                {
                    var customerBL = new CustomerBL(uow);
                    var cust = customerBL.Get(user.DebtorNumber);

                    if (cust == null)
                        throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(),
                            string.Format("Customer {0} not found.", user.DebtorNumber));

                    if (!cust.HasViewedTermsAndConditionsP)
                        throw new FaultException<AgreeToTermsFault>(new AgreeToTermsFault { EmailAddress = user.EmailAddress }, AgreeToTermsFault.FaultDescription);
                }
            }
        }

        private DBSession CreateSession(User user, SystemType system, bool isForAdmin = false, bool isImpersonating = false, bool forcePasswordChange = false, bool isAuthorised = true)
        {
            DBSession sess = null;

            // Make sure they have access to this website.
            if (!AccessHelper.IsAccessAllowedToWebsite(user, system))
                throw new FaultException<InvalidLoginFault>(new InvalidLoginFault(), InvalidLoginFault.FaultDescription);

            using (var uow = new UnitOfWork())
            {
                var sessionBL = new DBSessionBL(uow);
                try
                {
                    sess = sessionBL.CreateSession(user, system, isForAdmin, isImpersonating, forcePasswordChange, isAuthorised);
                }
                catch (SessionAlreadyExistsException ex)
                {
                    throw new FaultException<SessionAlreadyExistsFault>(new SessionAlreadyExistsFault(ex.ActiveSession), SessionAlreadyExistsFault.FaultDescription);
                }
                catch (LogonLicensesExceededException lex)
                {
                    throw new FaultException<LogonLicensesExceededFault>(new LogonLicensesExceededFault(lex.ActiveSessions, lex.LicenseCount), LogonLicensesExceededFault.FaultDescription);
                }
            }

            NLog.LogManager.GetCurrentClassLogger().Log(NLog.LogLevel.Info, string.Format("CreateSession:UserId={0},SessionKey={1}", user.Id, sess.Key));

            return sess;
        }

        private void SendRegistrationValidationEmail(SystemType system, RegistrationValidation rego)
        {
            string url;
            string product;

            switch (system)
            {
                case SystemType.Contacts:
                    url = ConfigurationManager.AppSettings["ContactsValidateEmailURL"] + rego.Token.ToString();
                    product = "Medianet's Media Database";
                    break;
                case SystemType.Journalists:
                    url = ConfigurationManager.AppSettings["JournalistsValidateEmailURL"] + rego.Token.ToString();
                    product = "the Medianet for Journalist";
                    break;
                default:
                    url = ConfigurationManager.AppSettings["DistributionValidateEmailURL"] + rego.Token.ToString();
                    product = "Medianet's Press Release Distribution";
                    break;
            }

            EmailHelper.RegistrationEmailValidationEmail(rego.FirstName.ProperCase(), product, rego.EmailAddress, url);
        }

        private List<CustomerRegion> GetCustomerDefaultRegion(string debtorNumber)
        {
            string regionName;
            List<CustomerRegion> custRegions = null;
            SalesRegion region = null;
            var finCust = FinanceHelper.GetCustomer(debtorNumber);

            if (finCust != null && finCust.PrimaryAddress != null)
            {
                // Get the name of the sales region set by Finance
                regionName = finCust.PrimaryAddress.SalesTerritoryId;

                if (!string.IsNullOrWhiteSpace(regionName))
                {
                    // Fetch the Id of this sales region
                    using (var uow = new UnitOfWork())
                    {
                        var regionBL = new SalesRegionBL(uow);
                        region = regionBL.GetAll().Where(s => s.Name.Equals(regionName, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                    }
                }
            }

            if (region == null)
            {
                int regionId;

                // We haven't got one from finance so try to assign the default sales region
                if (int.TryParse(BusinessLayer.Common.AAPEnvironmentHelper.GetSetting("DefaultSalesRegion"), out regionId))
                {
                    using (var uow = new UnitOfWork())
                    {
                        var regionBL = new SalesRegionBL(uow);
                        region = regionBL.Get(regionId);
                    }
                }
            }

            if (region != null)
            {
                custRegions = new List<CustomerRegion>();
                custRegions.Add(new CustomerRegion { DebtorNumber = debtorNumber, RegionId = region.Id });
            }

            return custRegions;
        }

        private Customer AddCustomer(NewAccountRequest account, CustomerBL customerBL)
        {
            Customer cust = new Customer();
            Customer existingCust;

            cust.Name = account.CompanyName.Truncate(50);
            cust.TimezoneCode = account.TimezoneCode;
            cust.AccountType = account.AccountType;
            cust.UseMedianetRewrite = true;
            cust.HasTransactions = false;

            //Default the timezone if none is supplied
            if (string.IsNullOrWhiteSpace(cust.TimezoneCode))
                cust.TimezoneCode = BusinessLayer.Common.Constants.TIMEZONE_SYD;

            // They can't become Invoiced until they are approved.
            if (account.AccountType == CustomerBillingType.Creditcard)
                cust.AccountType = account.AccountType;
            else
                cust.AccountType = CustomerBillingType.InvoicedNotApproved;

            // Ask Finance to create the customer so we can get the DebtorNumber and Sales Region/Territory.
            try
            {
                cust.DebtorNumber = FinanceHelper.AddCustomer(cust, account);

                // Add the default sales region
                cust.SalesRegions = GetCustomerDefaultRegion(cust.DebtorNumber);
            }
            catch (FaultException<FinanceCustomerService.CreateCustomerFault> createFault)
            {
                EmailHelper.GeneralErrorEmail("Error adding customer to Finance.", createFault);
                throw new FaultException<UpdateFault>(new UpdateFault(), createFault.Detail.ErrorMessage);
            }
            catch (FaultException<FinanceCustomerService.InvalidCustomerDetail> createFault)
            {
                EmailHelper.GeneralErrorEmail("Error adding customer to Finance.", createFault);
                throw new FaultException<UpdateFault>(new UpdateFault(), createFault.Detail.ErrorMessage);
            }

            cust.FilerCode = cust.DebtorNumber;

            // Just give the LogonName a value, even though we don't use it any more.
            cust.LogonName = account.CompanyName.Replace(" ", "").Truncate(50);

            // Make sure the DebtorNumber is unique.
            existingCust = customerBL.Get(cust.DebtorNumber);
            if (existingCust != null)
                throw new FaultException<NonUniqueFault>(new NonUniqueFault(), string.Format("Customer with DebtorNumber {0} already exists.", cust.DebtorNumber));

            cust.CreatedDate = DateTime.Now;
            cust.CreatedByUserId = 0;
            cust.LastModifiedDate = cust.CreatedDate;
            cust.LastModifiedByUserId = cust.CreatedByUserId;

            // Add the Customer to the database.
            customerBL.Add(cust);

            return cust;
        }

        private void UpdateCustomer(Customer cust, Account account, bool isCourseAccount, int userId, CustomerBL customerBL)
        {
            // Update the customer.
            cust.Name = account.CompanyName.Truncate(50);

            // If supplied a timezone then update it.
            if (!string.IsNullOrWhiteSpace(account.TimezoneCode))
                cust.TimezoneCode = account.TimezoneCode;

            // Update the Customer in the Finance system.
            try
            {
                if (cust.AccountType != account.AccountType)
                {
                    if (account.AccountType == CustomerBillingType.Creditcard)
                        cust.AccountType = account.AccountType;
                    else
                    {
                        if (cust.MustUseCreditcard)
                            throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), string.Format("Billing type for Customer {0} must stay as Creditcard.", cust.DebtorNumber));

                        // They can't become Invoiced until they are approved.
                        cust.AccountType = CustomerBillingType.InvoicedNotApproved;

                        FinanceHelper.UpdateCustomer(cust, account, isCourseAccount);
                    }
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(account.ABN))
                        FinanceHelper.UpdateCustomerABN(cust.DebtorNumber, account.ABN);
                }

            }
            catch (FaultException<FinanceCustomerService.AAPInternalCustomer>)
            {
                // This is an internal customer so Finance don't have details. Ignore the error.
            }
            catch (FaultException<FinanceCustomerService.UpdateCustomerFault> updateFault)
            {
                EmailHelper.GeneralErrorEmail("Error updating customer in Finance.", updateFault);
                throw new FaultException<UpdateFault>(new UpdateFault(), updateFault.Detail.ErrorMessage);
            }
            catch (FaultException<FinanceCustomerService.InvalidCustomerDetail> updateFault)
            {
                EmailHelper.GeneralErrorEmail("Error updating customer in Finance.", updateFault);
                throw new FaultException<UpdateFault>(new UpdateFault(), updateFault.Detail.ErrorMessage);
            }
            // Update the Customer in the database.
            customerBL.Update(cust, userId);
        }

        private User GetUserByEmailAddressOrLogonName(string emailAddress, string companyLogon, SystemType system, UserBL userBL, UnitOfWork uow)
        {
            List<User> users = null;

            // If there's an @ symbol then assume they are logging on using an email address
            if (emailAddress.IndexOf("@", StringComparison.CurrentCultureIgnoreCase) > 0)
            {
                users = userBL.GetAllByEmailAddress(emailAddress);
            }

            // If that didn't work. Try loggin on using username and optionally company logon
            if (users == null || users.Count == 0)
            {
                string username, company;

                if (!string.IsNullOrWhiteSpace(companyLogon))
                {
                    username = emailAddress;
                    company = companyLogon;
                }
                else
                {
                    string[] logonParams = emailAddress.Split('@');

                    if (logonParams.Length > 1)
                    {
                        // Maybe the email address is username@companylogon.
                        username = logonParams[0];
                        company = logonParams[1];
                    }
                    else
                    {
                        // If we didn't have any luck then try logging on with the logon name instead
                        username = emailAddress;
                        company = string.Empty;
                    }
                }

                users = userBL.GetAllByLogonDetails(username, company, system);
            }

            if (users == null || users.Count == 0)
            {
                AccessHelper.BruteForcePreventionDelay();
                throw new FaultException<InvalidLoginFault>(new InvalidLoginFault(), InvalidLoginFault.FaultDescription);
            }

            if (users.Count > 1)
                throw new FaultException<InvalidLoginFault>(new InvalidLoginFault(), InvalidLoginFault.FaultDescriptionDuplicateEmail);

            var customer = new CustomerBL(uow).Get(users[0].DebtorNumber);

            // Make sure they have access to this website.
            if ((system == SystemType.Contacts && customer.MediaContactsLicenseCount == 0) || !AccessHelper.IsAccessAllowedToWebsite(users[0], system))
                throw new FaultException<InvalidLoginFault>(new InvalidLoginFault(), InvalidLoginFault.FaultDescription);

            return users[0];
        }

        #endregion
    }
}
