﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Text.RegularExpressions;
using System.Reflection;
using System.IO;
using Medianet.Wcf;
using Medianet.Service.Contracts;
using Medianet.Service.Dto;
using Medianet.Service.Common;
using Medianet.Service.FaultExceptions;
using Medianet.Service.BusinessLayer;
using Medianet.DataLayer;

namespace Medianet.Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [ErrorBehaviorAttribute(typeof(ServiceErrorHandler))]
    public class FileService : IFileService
    {
        private const string UNVERIFIED_TEMP_SESSION = "UnverifiedTempFileImpersonationKey";

        public FileService() {
            // The windows service host normally does this but we do it here when running in debug mode.
            if (!AutoMapperConfigurator.IsInitialized)
                AutoMapperConfigurator.Configure();
        }

        /// <summary>
        /// Uploads a temporary file to the system. No conversions are performed.
        /// </summary>
        /// <param name="fileStream">A TempFileUploadRequest object containing the file name and the file stream.</param>
        /// <returns>A TempFileUploadResponse object containing the TempFileId.</returns>
        public TempFileUploadResponse UploadTempFile(TempFileUploadRequest fileStream) {
            TempFile tf;

            tf = UploadTempFileToServer(fileStream);

            return new TempFileUploadResponse() { TempFileId = tf.Id };
        }

        /// <summary>
        /// Uploads a temporary unverified file to the system. No conversions are performed.
        /// </summary>
        /// <param name="fileStream">A TempFileUploadRequest object containing the file name and the file stream.</param>
        /// <returns>A TempFileUploadResponse object containing the TempFileId.</returns>
        public TempFileUploadResponse UploadTempFileUnverified(TempFileUploadRequest fileStream)
        {
            TempFile tf;

            int UnverifiedKey = int.Parse(System.Configuration.ConfigurationManager.AppSettings[UNVERIFIED_TEMP_SESSION]);

            if (UnverifiedKey.ToString() != fileStream.SessionKey)
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access denied to Upload file."));

            tf = UploadTempFileToServer(fileStream,true);

            return new TempFileUploadResponse() { TempFileId = tf.Id };
        }

        /// <summary>
        /// Uploads a temporary file to the system and converts it to all formats specified.
        /// </summary>
        /// <param name="fileStream">A TempFileUploadRequest object containing the file name, the type of conversions to perform and the file stream.</param>
        /// <returns>A TempFileUploadResponse object containing the TempFileId, number of wire words.</returns>
        public TempFileUploadResponse UploadTempFileForConversion(TempFileUploadRequest fileStream) {
            string fileExt = System.IO.Path.GetExtension(fileStream.FileName);
            TempFile tf;
            ConverterService.DocumentSizeData releaseSizes;
            List<int> tempFileIds = new List<int>();

            if (!fileStream.ConvertToWire)
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), "No conversion has been specified.");

            // Upload the file.
            tf = UploadTempFileToServer(fileStream);

            // Convert it to get the tif and text.
            tempFileIds.Add(tf.Id);
            releaseSizes = CasHelper.ConvertDocuments(tempFileIds, fileStream.ConvertToWire, fileStream.SessionKey);

            return new TempFileUploadResponse() { TempFileId = tf.Id, PageCount = releaseSizes.Pages, WordCount = releaseSizes.Words };
        }

        /// <summary>
        /// Gets the details and contents of a TempFile for an Unverified user.
        /// Used only for the fetching of documents for Unverified Press Releases.
        /// </summary>
        /// <param name="fcRequest">A FileContentRequest object containing the Id of the TempFile.</param>
        /// <returns>A FileContentResponse object containing the file details and a stream containing the contents of the TempFile.</returns>
        public FileContentResponse GetTempFileContentUnverified(FileContentRequest fcRequest)
        {
            TempFile tFile;
            FileContentResponse fileResp;
            int UnverifiedKey = int.Parse(System.Configuration.ConfigurationManager.AppSettings[UNVERIFIED_TEMP_SESSION]);
            string filePath;

            if (UnverifiedKey.ToString() != fcRequest.SessionKey)
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access denied to TempFile with Id {0}.", fcRequest.FileId));

            using (var uow = new UnitOfWork())
            {
                var tempBL = new TempFileBL(uow);
                tFile = tempBL.Get(fcRequest.FileId);
            }

            if (tFile == null)
                throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("TempFile with Id {0} not found.", fcRequest.FileId));

            filePath = BusinessLayer.Common.PathHelper.GetTempFilePathAndName(tFile.Id, tFile.FileExtension);

            if (!System.IO.File.Exists(filePath))
                throw new FaultException<FileNotFoundFault>(new FileNotFoundFault(), string.Format("Content for TempFile with Id {0} not found.", fcRequest.FileId));

            fileResp = AutoMapper.Mapper.Map<TempFile, FileContentResponse>(tFile);
            fileResp.FileData = File.OpenRead(filePath);

            // Create a callback to close the file once the connection is closed
            OperationContext clientContext = OperationContext.Current;
            clientContext.OperationCompleted += new EventHandler(delegate (object sender, EventArgs args)
            {
                if (fileResp.FileData != null)
                    fileResp.FileData.Dispose();
            });

            return fileResp;
        }

        /// <summary>
        /// Gets the details and contents of a TempFile.
        /// </summary>
        /// <param name="fcRequest">A FileContentRequest object containing the Id of the TempFile.</param>
        /// <returns>A FileContentResponse object containing the file details and a stream containing the contents of the TempFile.</returns>
        public FileContentResponse GetTempFileContent(FileContentRequest fcRequest) {
            TempFile tFile;
            FileContentResponse fileResp;
            DBSession session = AccessHelper.GetSessionFromKey(fcRequest.SessionKey);
            string filePath;

            using (var uow = new UnitOfWork()) {
                var tempBL = new TempFileBL(uow);
                tFile = tempBL.Get(fcRequest.FileId);
            }

            if (tFile == null)
                throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("TempFile with Id {0} not found.", fcRequest.FileId));

            if (session.Key != tFile.SessionKey)
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access denied to TempFile with Id {0}.", fcRequest.FileId));

            filePath = BusinessLayer.Common.PathHelper.GetTempFilePathAndName(tFile.Id, tFile.FileExtension);

            if (!System.IO.File.Exists(filePath))
                throw new FaultException<FileNotFoundFault>(new FileNotFoundFault(), string.Format("Content for TempFile with Id {0} not found.", fcRequest.FileId));
            
            fileResp = AutoMapper.Mapper.Map<TempFile, FileContentResponse>(tFile);
            fileResp.FileData = File.OpenRead(filePath);

            // Create a callback to close the file once the connection is closed
            OperationContext clientContext = OperationContext.Current;
            clientContext.OperationCompleted += new EventHandler(delegate (object sender, EventArgs args)
            {
                if (fileResp.FileData != null)
                    fileResp.FileData.Dispose();
            });

            return fileResp;
        }

        /// <summary>
        /// Gets the details and contents of a derived file of TempFile.
        /// </summary>
        /// <param name="fcRequest">A DerivedFileContentRequest object containing the Id of the TempFile and the derived type wanted.</param>
        /// <returns>A FileContentResponse object containing the file details and a stream containing the contents of the TempFile.</returns>
        public FileContentResponse GetTempFileDerivedContent(DerivedFileContentRequest fcRequest) {
            TempFile tFile = null;
            FileContentResponse fileResp;
            DBSession session = AccessHelper.GetSessionFromKey(fcRequest.SessionKey);
            string filePath;

            if (!fcRequest.WireConversion)
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), "No derived file type has been specified.");

            using (var uow = new UnitOfWork()) {
                var tempBL = new TempFileBL(uow);
                // Get the derived TempFile's for the TempFileId provided.
                List<TempFile> children = tempBL.GetAllByParentId(fcRequest.FileId);

                foreach (var tChild in children) {
                   if (tChild.IsWireConversion && fcRequest.WireConversion) {
                        tFile = tChild;
                        break;
                    }
                }
            }

            if (tFile == null)
                throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Derived TempFile for TempFile with Id {0} not found.", fcRequest.FileId));

            if (session.Key != tFile.SessionKey)
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access denied to TempFile with Id {0}.", tFile.Id));

            filePath = BusinessLayer.Common.PathHelper.GetTempFilePathAndName(tFile.Id, tFile.FileExtension);

            if (!System.IO.File.Exists(filePath))
                throw new FaultException<FileNotFoundFault>(new FileNotFoundFault(), string.Format("Content for TempFile with Id {0} not found.", tFile.Id));

            fileResp = AutoMapper.Mapper.Map<TempFile, FileContentResponse>(tFile);
            fileResp.FileData = File.OpenRead(filePath);

            // Create a callback to close the file once the connection is closed
            OperationContext clientContext = OperationContext.Current;
            clientContext.OperationCompleted += new EventHandler(delegate (object sender, EventArgs args)
            {
                if (fileResp.FileData != null)
                    fileResp.FileData.Dispose();
            });

            return fileResp;
        }

        /// <summary>
        /// Gets the details and contents of a InboundFile.
        /// </summary>
        /// <param name="fcRequest">A FileContentRequest object containing the Id of the InboundFile.</param>
        /// <returns>A FileContentResponse object containing the file details and a stream containing the contents of the InboundFile.</returns>
        public FileContentResponse GetInboundFileContent(FileContentRequest fcRequest) {
            InboundFile iFile;
            FileContentResponse fileResp;
            DBSession session = AccessHelper.GetSessionFromKey(fcRequest.SessionKey);
            string filePath;

            using (var uow = new UnitOfWork()) {
                var inboundBL = new InboundFileBL(uow);
                iFile = inboundBL.Get(fcRequest.FileId);
            }

            if (iFile == null)
                throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("InboundFile with Id {0} not found.", fcRequest.FileId));

            // Not sure how to check security for this yet. The table structure should change to include a DebtorNumber.
            //if (session.Key != iFile.SessionKey)
            //    throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access denied to InboundFile with Id {0}.", tfRequest.FileId));

            filePath = BusinessLayer.Common.PathHelper.GetInboundFilePathAndName(iFile.Id, iFile.FileExtension);

            if (!System.IO.File.Exists(filePath))
                throw new FaultException<FileNotFoundFault>(new FileNotFoundFault(), string.Format("Content for InboundFile with Id {0} not found.", fcRequest.FileId));

            fileResp = AutoMapper.Mapper.Map<InboundFile, FileContentResponse>(iFile);
            fileResp.ContentType = MimeExtensionHelper.GetMimeType(iFile.OriginalFilename);
            fileResp.FileData = File.OpenRead(filePath);

            // Create a callback to close the file once the connection is closed
            OperationContext clientContext = OperationContext.Current;
            clientContext.OperationCompleted += new EventHandler(delegate (object sender, EventArgs args)
            {
                if (fileResp.FileData != null)
                    fileResp.FileData.Dispose();
            });

            return fileResp;
        }

        /// <summary>
        /// Gets the details and contents of a Thumbnail for a Document.
        /// </summary>
        /// <param name="thumbnailId">The Id of the Thumbnail.</param>
        /// <returns>A FileContentResponse object containing the file details and a stream containing the contents of the Thumbnail.</returns>
        public FileContentResponse GetThumbnailContent(FileContentRequest fcRequest) {
            Thumbnail thumb;
            var fileResp = new FileContentResponse();
            string filePath;

            // Fetch the Thumbnail.
            using (var uow = new UnitOfWork()) {
                var thumbBL = new ThumbnailBL(uow);
                thumb = thumbBL.Get(fcRequest.FileId);
            }

            if (thumb == null)
                throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Thumbnail with Id {0} not found.", fcRequest.FileId));

            filePath = BusinessLayer.Common.PathHelper.GetThumbnailFilePathAndName(thumb.Id, thumb.FileExtension);

            if (!System.IO.File.Exists(filePath))
                throw new FaultException<FileNotFoundFault>(new FileNotFoundFault(), string.Format("Content for Thumbnail with Id {0} not found.", thumb.Id));

            fileResp.FileId = thumb.Id;
            fileResp.FileExtension = thumb.FileExtension;
            fileResp.OriginalFilename = Path.GetFileName(filePath);
            fileResp.ContentType = MimeExtensionHelper.GetMimeType(fileResp.OriginalFilename).Truncate(150);
            fileResp.FileSize = (int)(new FileInfo(filePath)).Length;
            fileResp.FileData = File.OpenRead(filePath);

            // Create a callback to close the file once the connection is closed
            OperationContext clientContext = OperationContext.Current;
            clientContext.OperationCompleted += new EventHandler(delegate (object sender, EventArgs args)
            {
                if (fileResp.FileData != null)
                    fileResp.FileData.Dispose();
            });

            return fileResp;
        }

        /// <summary>
        /// Gets the details and contents of a Document.
        /// </summary>
        /// <param name="fcRequest">A FileContentRequest object containing either the Id of the Document or a ReleaseId and SequenceNumber.</param>
        /// <returns>A FileContentResponse object containing the file details and a stream containing the contents of the Document.</returns>
        public FileContentResponse GetDocumentContent(DocumentContentRequest fcRequest) {
            Document doc;
            Release release;
            FileContentResponse fileResp;
            string filePath;
            bool accessAllowed = false;

            // Fetch the Document.
            using (var uow = new UnitOfWork())
            {
                var docBL = new DocumentBL(uow);

                if (fcRequest.DocumentId.HasValue)
                    doc = docBL.Get(fcRequest.DocumentId.Value);
                else if (fcRequest.ReleaseId.HasValue) {
                    if (!fcRequest.SequenceNumber.HasValue || fcRequest.SequenceNumber.Value < 0)
                        doc = docBL.GetPrimaryByReleaseId(fcRequest.ReleaseId.Value);
                    else
                        doc = docBL.GetByReleaseIdSequenceNumber(fcRequest.ReleaseId.Value, fcRequest.SequenceNumber.Value);
                }
                else
                    throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), "DocumentId and ReleaseId can not both be null.");

                if (doc == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Document with Id {0} or ReleaseId {1} and SequenceNumber {2} not found.", fcRequest.DocumentId, fcRequest.ReleaseId, fcRequest.SequenceNumber));

                // Fetch the Release to check security access.
                var releaseBL = new ReleaseBL(uow);
                release = releaseBL.Get(doc.ReleaseId);

                if (release == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Release with Id {0} not found.", doc.ReleaseId));
            }

            if (!string.IsNullOrWhiteSpace(fcRequest.SessionKey)) {
                if (AccessHelper.IsFullAccessAllowedToRelease(release.DebtorNumber, release.System, fcRequest.SessionKey))
                    accessAllowed = true;
            }

            if (!accessAllowed) {
                // Check if the release is public or they have a valid SecurityKey.
                if (AccessHelper.IsPublicAccessAllowedToRelease(release, fcRequest.SecurityKey))
                    accessAllowed = true;
            }

            if (!accessAllowed)
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access denied to Document with Id {0}.", doc.Id));

            filePath = BusinessLayer.Common.PathHelper.GetDocumentFilePathAndName(doc.Id, doc.FileExtension);

            if (!System.IO.File.Exists(filePath))
                throw new FaultException<FileNotFoundFault>(new FileNotFoundFault(), string.Format("Content for Document with Id {0} not found.", doc.Id));

            fileResp = AutoMapper.Mapper.Map<Document, FileContentResponse>(doc);
            fileResp.FileSize = (int)(new FileInfo(filePath)).Length;
            fileResp.FileData = File.OpenRead(filePath);

            // Create a callback to close the file once the connection is closed
            OperationContext clientContext = OperationContext.Current;
            clientContext.OperationCompleted += new EventHandler(delegate (object sender, EventArgs args)
            {
                if (fileResp.FileData != null)
                    fileResp.FileData.Dispose();
            });

            return fileResp;
        }

        /// <summary>
        /// Gets the logo for a website that is crawled by the ReleaseWatch crawler.
        /// </summary>
        /// <param name="wcRequest">The Id of the website.</param>
        /// <returns></returns>
        public FileContentResponse GetWebsiteLogo(WebContentRequest wcRequest) {
            Website site;
            FileContentResponse fileResp = new FileContentResponse();

            using (var uow = new UnitOfWork()) {
                var webBL = new WebsiteBL(uow);
                site = webBL.GetWebsite(wcRequest.WebsiteId);
            }

            if (site == null)
                throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Website with Id {0} not found.", wcRequest.WebsiteId));

            if (site.Logo == null || site.Logo.Length == 0)
                throw new FaultException<FileNotFoundFault>(new FileNotFoundFault(), string.Format("Content for Website with Id {0} not found.", wcRequest.WebsiteId));

            fileResp.FileId = site.Id;
            fileResp.OriginalFilename = site.LogoFilename;
            fileResp.FileExtension = Path.GetExtension(site.LogoFilename).Replace(".", "");
            fileResp.ContentType = MimeExtensionHelper.GetMimeType(site.LogoFilename);
            fileResp.FileSize = site.Logo.Length;

            fileResp.FileData = new MemoryStream(site.Logo);

            // Create a callback to close the file once the connection is closed
            OperationContext clientContext = OperationContext.Current;
            clientContext.OperationCompleted += new EventHandler(delegate (object sender, EventArgs args)
            {
                if (fileResp.FileData != null)
                    fileResp.FileData.Dispose();
            });

            return fileResp;
        }

        /// <summary>
        /// Allows the update of the file size, item count and file content of an existing temp file.
        /// </summary>
        /// <param name="fileStream">The tempfile id, sessionid, and file content to update.</param>
        /// <returns>A FileContentResponse object containing the file details and a stream containing the contents of the Document.</returns>
        public FileContentResponse UpdateTempFileDetails(TempFileUpdateRequest fileStream)
        {
            TempFile tf;
            FileContentResponse fileResp;
            string filePath;
            DBSession session = AccessHelper.GetSessionFromKey(fileStream.SessionKey);

            if (session.Key != fileStream.SessionKey)
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access denied to TempFile with Id {0}.", fileStream.Id));

            using (var uow = new UnitOfWork())
            {
                var tempBL = new TempFileBL(uow);

                tf = tempBL.Get(fileStream.Id);
                filePath = BusinessLayer.Common.PathHelper.GetTempFilePathAndName(tf.Id, tf.FileExtension);

                using (var fs = File.Create(filePath))
                {
                    fileStream.FileData.CopyTo(fs);
                }

                tf.FileSize = (int)(new FileInfo(filePath)).Length;

                //If this is the derived wire text, make sure we update the item count.
                if (tf.IsWireConversion && tf.FileExtension.ToUpper() == "TXT" && tf.ParentId.HasValue)
                {
                    using (var fs = File.OpenText(filePath))
                    {
                        string text = fs.ReadToEnd();
                        tf.ItemCount = Medianet.Compatability.WordCount.CountWords(text);
                    }
                }

                tempBL.Update(tf);
            }

            fileResp = AutoMapper.Mapper.Map<TempFile, FileContentResponse>(tf);
            fileResp.FileData = fileStream.FileData;

            // Create a callback to close the file once the connection is closed
            OperationContext clientContext = OperationContext.Current;
            clientContext.OperationCompleted += new EventHandler(delegate (object sender, EventArgs args)
            {
                if (fileResp.FileData != null)
                    fileResp.FileData.Dispose();
            });

            return fileResp;
        }

        #region Private methods

        private TempFile UploadTempFileToServer(TempFileUploadRequest fileStream, bool IsUnverified = false) {
            TempFile tf = new TempFile();
            string filePath;

            // Trust the session key that is supplied if the upload is an unverified release.
            if (!IsUnverified)
            {
                // Fetch the DBSession to make sure it's valid.
                AccessHelper.GetSessionFromKey(fileStream.SessionKey);
            }

            tf.SessionKey = fileStream.SessionKey;
            tf.FileExtension = Path.GetExtension(fileStream.FileName).Replace(".", "").ToLower().Truncate(20);
            tf.OriginalFilename = Path.GetFileName(fileStream.FileName).Truncate(512);
            tf.IsWireConversion = fileStream.ConvertToWire;
            tf.ContentType = fileStream.ContentType.Truncate(100);
            tf.FileSize = 0;
            tf.ItemCount = 0;
            tf.CreatedDate = DateTime.Now;

            using (var uow = new UnitOfWork()) {
                var tempBL = new TempFileBL(uow);

                tf.Id = tempBL.Add(tf);

                filePath = BusinessLayer.Common.PathHelper.GetTempFilePathAndName(tf.Id, tf.FileExtension);

                using (var fs = File.Create(filePath)) {
                    fileStream.FileData.CopyTo(fs);
                }

                tempBL.Update(tf);
            }

            return tf;
        }

        #endregion
    }
}
