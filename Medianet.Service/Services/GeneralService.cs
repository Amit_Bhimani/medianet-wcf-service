﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.IO;
using AutoMapper;
using Medianet.Wcf;
using Medianet.Service.Contracts;
using Medianet.Service.Dto;
using Medianet.Service.Common;
using Medianet.Service.FaultExceptions;
using Medianet.Service.BusinessLayer;
using Medianet.DataLayer;
using System.Globalization;

namespace Medianet.Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [ErrorBehaviorAttribute(typeof(ServiceErrorHandler))]
    public class GeneralService : IGeneralService
    {
        public GeneralService() {
            // The windows service host normally does this but we do it here when running in debug mode.
            if (!AutoMapperConfigurator.IsInitialized)
                AutoMapperConfigurator.Configure();
        }

        /// <summary>
        /// Get all Twitter Categories.
        /// </summary>
        /// <returns>A list of all TwitterCategory items.</returns>
        public List<TwitterCategory> GetTwitterCategories() {
            using (var uow = new UnitOfWork()) {
                var listBL = new TwitterCategoryBL(uow);
                return listBL.GetAll();
            }
        }

        /// <summary>
        /// Get all Web Categories.
        /// </summary>
        /// <returns>A list of all WebCategory items.</returns>
        public List<WebCategory> GetWebCategories() {
            using (var uow = new UnitOfWork()) {
                var listBL = new WebCategoryBL(uow);
                return listBL.GetAll();
            }
        }

        /// <summary>
        /// Delete a TempFile.
        /// </summary>
        /// <param name="tempFileId">The Id of the temp file to delete.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        public void DeleteTempFileById(int tempFileId, string sessionKey)
        {
            TempFile file;
            string filePath;

            using (var uow = new UnitOfWork())
            {
                var tempBL = new TempFileBL(uow);
                file = tempBL.Get(tempFileId);

                if (file == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("TempFile with Id {0} not found.", tempFileId));

                // Make sure they have access to it.
                if (!file.SessionKey.Equals(sessionKey))
                    throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to TempFile with Id {0} denied.", tempFileId));

                tempBL.Delete(file.Id);
            }

            // Delete the TempFile.
            filePath = BusinessLayer.Common.PathHelper.GetTempFilePathAndName(file.Id, file.FileExtension);
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
        }

        public void DeleteTempFileChildrenById(int tempFileId, string sessionKey)
        {
            using (var uow = new UnitOfWork())
            {
                var tempBL = new TempFileBL(uow);
                var children = tempBL.GetAllByParentId(tempFileId);
                string filePath;

                if (children.Count > 0)
                {
                    // Make sure they have access to it.
                    if (!children[0].SessionKey.Equals(sessionKey))
                        throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to TempFile with Id {0} denied.", tempFileId));

                    foreach (var t in children)
                    {
                        tempBL.Delete(t.Id);

                        // Delete the TempFile.
                        filePath = BusinessLayer.Common.PathHelper.GetTempFilePathAndName(t.Id, t.FileExtension);
                        if (File.Exists(filePath))
                        {
                            File.Delete(filePath);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Get the number of recipients of a mailmerge file.
        /// </summary>
        /// <param name="tempFileId">The Id of the csv file already uploaded to the server.</param>
        /// <param name="distType">The distribution type.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The number of recipients in the csv file.</returns>
        public int CountMailmergeRecipients(int tempFileId, DistributionType distType, string sessionKey) {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);
            ValidateTempFile(tempFileId, session);

            try {
                return CasHelper.CountMailmergeRecipients(tempFileId, distType);
            }
            catch (FaultException<ConverterService.InvalidDatasourceFaultException> invalid) {
                throw new FaultException<InvalidDataSourceFault>(new InvalidDataSourceFault(), invalid.Message);
            }
        }

        /// <summary>
        /// Get the details of an inbound file.
        /// </summary>
        /// <param name="inboundFileId">The Id of the inbound file.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The details of the inbound file.</returns>
        public InboundFile GetInboundFileById(int inboundFileId, string sessionKey) {
            InboundFile file;

            using (var uow = new UnitOfWork()) {
                var inboundBL = new InboundFileBL(uow);
                file = inboundBL.Get(inboundFileId);
            }

            if (file == null)
                throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("InboundFile with Id {0} not found.", inboundFileId));

            //// Make sure they have access to it.
            //if (!AccessHelper.IsAccessAllowedToList(file.DebtorNumber, sessionKey))
            //    throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to InboundFile with Id {0} denied.", inboundFileId));

            return file;
        }

        /// <summary>
        /// Get a list of inbound files for a Customer.
        /// </summary>
        /// <param name="debtorNumber">The DebtorNumber of the Customer.</param>
        /// <param name="type">The MessageType, e.g. Radio.</param>
        /// <param name="startPos">The 0 based starting position, e.g. to return the 3rd page with a pageSize of 10 pass 30.</param>
        /// <param name="pageSize">The size of the page to return.</param>
        /// <param name="sortOrder">The order to sort the results by.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A list of inbound files.</returns>
        public InboundFilePage GetInboundFilesByDebtorNumber(string debtorNumber, MessageType type, int startPos, int pageSize, InboundFileSortOrderType sortOrder, string sessionKey) {
            InboundFilePage page = new InboundFilePage();

            // Make sure they have access to it.
            if (!AccessHelper.IsAdminOrSameCustomer(debtorNumber, sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to InboundFiles for DebtorNumber {0} denied.", debtorNumber));

            page.Type = type;
            page.StartPos = startPos;
            page.PageSize = pageSize;
            page.SortOrder = sortOrder;

            using (var uow = new UnitOfWork()) {
                var inboundBL = new InboundFileBL(uow);
                page.InboundFiles = inboundBL.GetAllByDebtorNumberType(debtorNumber, type, sortOrder, startPos, pageSize);
                page.TotalCount = inboundBL.GetCountByDebtorNumberType(debtorNumber, type);
            }

            return page;
        }

        /// <summary>
        /// Delete an inbound file.
        /// </summary>
        /// <param name="inboundFileId">The Id of the inbound file to delete.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        public void DeleteInboundFileById(int inboundFileId, string sessionKey) {
            InboundFile file;
            string filePath;

            using (var uow = new UnitOfWork()) {
                var inboundBL = new InboundFileBL(uow);
                file = inboundBL.Get(inboundFileId);

                if (file == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("InboundFile with Id {0} not found.", inboundFileId));

                //// Make sure they have access to it.
                //if (!AccessHelper.IsAccessAllowedToList(file.DebtorNumber, sessionKey))
                //    throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to InboundFile with Id {0} denied.", inboundFileId));

                inboundBL.Delete(file.Id);
            }

            // Delete the InboundFile.
            filePath = BusinessLayer.Common.PathHelper.GetInboundFilePathAndName(file.Id, file.FileExtension);
            if (File.Exists(filePath)) {
                File.Delete(filePath);
            }
        }

        /// <summary>
        /// Rename the original name of an Inbound file.
        /// </summary>
        /// <param name="inboundFileId">The Id of the inbound file to delete.</param>
        /// <param name="name">The new original filename.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        public void RenameInboundFileById(int inboundFileId, string name, string sessionKey) {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);
            InboundFile file;

            using (var uow = new UnitOfWork()) {
                var inboundBL = new InboundFileBL(uow);
                file = inboundBL.Get(inboundFileId);

                if (file == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("InboundFile with Id {0} not found.", inboundFileId));

                //// Make sure they have access to it.
                //if (!AccessHelper.IsAccessAllowedToList(file.DebtorNumber, sessionKey))
                //    throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to InboundFile with Id {0} denied.", inboundFileId));

                file.OriginalFilename = name;
                inboundBL.Update(file, session.UserId);
            }
        }

        public MediaStatistics GetMediaStatistics() {
            using (var uow = new UnitOfWork()) {
                var mediaBL = new MediaBL(uow);
                return mediaBL.GetStatistics();
            }
        }

        /// <summary>
        /// Get all Timezones.
        /// </summary>
        /// <returns>A list of all Timezone items.</returns>
        public List<Timezone> GetTimezones() {
            using (var uow = new UnitOfWork()) {
                var timezoneBL = new TimezoneBL(uow);
                return timezoneBL.GetAll();
            }
        }

        public string ConvertTimeByTimeZone(string date, string timeZone)
        {
            var dateTime = DateTime.Parse(date);

            return TimeZoneInfo.ConvertTime(dateTime, TimeZoneInfo.FindSystemTimeZoneById(timeZone)).ToString();
        }

        public void RecordEmailOpened(int releaseId, int transactionId, int? distributionId) {
            if (releaseId <= 0 || transactionId <= 0)
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), "ReleaseId and TransactionId must both be positive numbers greater than 0.");

            using (var uow = new UnitOfWork())
            {
                var resultBL = new ResultBL(uow);
                if (distributionId.HasValue && distributionId.Value > 0)
                    resultBL.UpdateOpenCountByDistributionId(releaseId, transactionId, distributionId.Value);
                else
                    resultBL.UpdateOpenCount(releaseId, transactionId);
            }
        }

        public Career GetCareers()
        {
            using (var uow = new UnitOfWork())
            {
                var careerBL = new DayBookBL(uow);
                return careerBL.GetCareer();
            }
        }

        public void AddRssLog(RssLog entity,string sessionKey)
        {
            using (var uow = new UnitOfWork())
            {
                var rssLogBL = new RssLogBL(uow);
                rssLogBL.Add(entity);
            }
        }

        /// <summary>
        /// Get the details of a Training Course.
        /// </summary>
        /// <param name="courseId">The Id of the Training Course.</param>
        /// <returns>The details of the Training Course.</returns>
        public TrainingCourse GetTrainingCourse(string courseId)
        {
            using (var uow = new UnitOfWork())
            {
                var listBL = new TrainingCourseBL(uow);
                return listBL.Get(courseId);
            }
        }

        public string GetEnvironmentValue(string type)
        {
            using (var uow = new UnitOfWork())
            {
                var entity = new AAPEnvironmentBL(uow).Get(type);

                return entity == null ? null : entity.Value;
            }
        }

        #region Private methods

        private void ValidateTempFile(int tempFileId, DBSession session) {
            TempFile tFile;
            string filePath;

            using (var uow = new UnitOfWork()) {
                var tempBL = new TempFileBL(uow);
                tFile = tempBL.Get(tempFileId);
            }

            if (tFile == null)
                throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("TempFile with Id {0} not found.", tempFileId));

            if (session.Key != tFile.SessionKey)
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access denied to TempFile with Id {0}.", tempFileId));

            filePath = BusinessLayer.Common.PathHelper.GetTempFilePathAndName(tFile.Id, tFile.FileExtension);

            if (!System.IO.File.Exists(filePath))
                throw new FaultException<FileNotFoundFault>(new FileNotFoundFault(), string.Format("Content for TempFile with Id {0} not found.", tempFileId));
        }

        #endregion

        #region Update mn_Results

        public void UpdateMessageMediaAPIResult(Result entity)
        {
            using (var uow = new UnitOfWork())
            {
               if (!new ResultBL(uow).UpdateStatus(entity))
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), 
                        string.Format("Result with jobId {0}, transactionId {1}, distributionId {2} not found.", 
                        entity.ReleaseId, entity.TransactionId, entity.DistributionId));
            }
        }
        
        #endregion
    }
}
