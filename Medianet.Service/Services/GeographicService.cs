﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.IO;
using AutoMapper;
using Medianet.Wcf;
using Medianet.Service.Contracts;
using Medianet.Service.Dto;
using Medianet.Service.Common;
using Medianet.Service.FaultExceptions;
using Medianet.Service.BusinessLayer;
using Medianet.DataLayer;

namespace Medianet.Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [ErrorBehaviorAttribute(typeof(ServiceErrorHandler))]
    public class GeographicService : IGeographicService
    {
        public GeographicService() {
            // The windows service host normally does this but we do it here when running in debug mode.
            AutoMapperConfigurator.Configure();
        }

        public Continent GetContinentById(int continentId) {
            using (var uow = new UnitOfWork()) {
                var continentBL = new ContinentBL(uow);
                return continentBL.Get(continentId);
            }
        }

        public List<Continent> GetContinents() {
            using (var uow = new UnitOfWork()) {
                var continentBL = new ContinentBL(uow);
                return continentBL.GetAll();
            }
        }

        public Country GetCountryById(int countryId) {
            using (var uow = new UnitOfWork()) {
                var countryBL = new CountryBL(uow);
                return countryBL.Get(countryId);
            }
        }

        public List<Country> GetCountries() {
            using (var uow = new UnitOfWork()) {
                var countryBL = new CountryBL(uow);
                return countryBL.GetAll();
            }
        }

        public State GetStateById(int stateId) {
            using (var uow = new UnitOfWork()) {
                var stateBL = new StateBL(uow);
                return stateBL.Get(stateId);
            }
        }

        public List<State> GetStates() {
            using (var uow = new UnitOfWork()) {
                var stateBL = new StateBL(uow);
                return stateBL.GetAll();
            }
        }

        public City GetCityById(int cityId) {
            using (var uow = new UnitOfWork()) {
                var cityBL = new CityBL(uow);
                return cityBL.Get(cityId);
            }
        }

        public List<City> GetCities() {
            using (var uow = new UnitOfWork()) {
                var cityBL = new CityBL(uow);
                return cityBL.GetAll();
            }
        }

        public List<City> GetCitiesByAny(int? continentId, int? countryId, int? stateId, string name) {
            using (var uow = new UnitOfWork()) {
                var cityBL = new CityBL(uow);
                return cityBL.GetAllBy(continentId, countryId, stateId, name);
            }
        }
    }
}
