﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using AutoMapper;
using Medianet.Wcf;
using Medianet.Service.Contracts;
using Medianet.Service.Dto;
using Medianet.Service.Common;
using Medianet.Service.FaultExceptions;
using Medianet.Service.BusinessLayer;
using Medianet.DataLayer;

namespace Medianet.Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [ErrorBehaviorAttribute(typeof(ServiceErrorHandler))]
    public class ListEditService : IListEditService
    {
        public ListEditService() {
            // The windows service host normally does this but we do it here when running in debug mode.
            if (!AutoMapperConfigurator.IsInitialized)
                AutoMapperConfigurator.Configure();
        }

        /// <summary>
        /// Add a new ServiceList and a list of Recipients.
        /// </summary>
        /// <param name="service">Basic details of the ServiceList which also contains a list of Recipients.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The Id of the new ServiceList created.</returns>
        public int AddServiceListFromSummary(ServiceListUpload serviceSummary, string sessionKey) {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);
            ServiceList service;

            if (string.IsNullOrWhiteSpace(serviceSummary.SelectionDescription))
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), "Service Name must not be blank.");

            foreach (var recipient in serviceSummary.Recipients) {
                if (string.IsNullOrWhiteSpace(recipient.Address))
                    throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), "Recipient Address must not be blank.");
            }

            using (var uow = new UnitOfWork()) {
                var listBL = new ServiceListBL(uow);

                service = GenerateServiceListFromSummary(serviceSummary, session);

                //uow.BeginTransaction();
                service.Id = listBL.AddWithRecipients(service, serviceSummary.Recipients, session.UserId);
                uow.Save();
            }

            return service.Id;
        }

        /// <summary>
        /// Rename a ServiceList.
        /// </summary>
        /// <param name="serviceId">The Id of the ServiceList.</param>
        /// <param name="name">The new ServiceList name.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        public void RenameServiceList(int serviceId, string name, string sessionKey) {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);

            if (string.IsNullOrWhiteSpace(name))
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), "Service Name must not be blank.");

            using (var uow = new UnitOfWork()) {
                var listBL = new ServiceListBL(uow);
                var service = listBL.Get(serviceId);

                if (service == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Service with Id {0} not found.", serviceId));

                // Make sure they have access to it.
                if (!AccessHelper.IsAccessAllowedToList(service.DebtorNumber, service.System, session))
                    throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to Service with Id {0} denied.", serviceId));

                service.SelectionDescription = name;

                //uow.BeginTransaction();
                listBL.Update(service, session.UserId);
                uow.Save();
            }
        }

        /// <summary>
        /// Delete a ServiceList and all Recipients that belong to it.
        /// </summary>
        /// <param name="serviceId">The Id of the ServiceList.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        public void DeleteServiceList(int serviceId, string sessionKey) {
            using (var uow = new UnitOfWork()) {
                var listBL = new ServiceListBL(uow);
                var session = AccessHelper.GetSessionFromKey(sessionKey);
                ServiceList service = listBL.Get(serviceId);

                if (service == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Service with Id {0} not found.", serviceId));

                // Make sure they have access to it.
                if (!AccessHelper.IsAccessAllowedToList(service.DebtorNumber, service.System, session))
                    throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to Service with Id {0} denied.", serviceId));

                if (listBL.IsListInUse(serviceId, service.DebtorNumber))
                    throw new FaultException<InUseFault>(new InUseFault(), string.Format("Service with Id {0} is currently in use by 1 or more releases and cannot be changed.", serviceId));

                //uow.BeginTransaction();
                listBL.Delete(serviceId, session.UserId);
                uow.Save();
            }
        }

        /// <summary>
        /// Add a new Recipient to a Service.
        /// </summary>
        /// <param name="serviceId">The Id of the Service to add the Recipient to.</param>
        /// <param name="recipient">The Recipient to add.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The Id of the new Recipient.</returns>
        public int AddServiceListRecipient(int serviceId, Recipient recipient, string sessionKey) {
            if (string.IsNullOrWhiteSpace(recipient.Address))
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), "Recipient Address must not be blank.");

            using (var uow = new UnitOfWork()) {
                var listBL = new ServiceListBL(uow);
                var session = AccessHelper.GetSessionFromKey(sessionKey);
                ServiceList service = listBL.Get(serviceId);
                int recipientId;

                if (service == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Service with Id {0} not found.", serviceId));

                // Make sure they have access to it.
                if (!AccessHelper.IsAccessAllowedToList(service.DebtorNumber, service.System, session))
                    throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to Service with Id {0} denied.", serviceId));

                if (listBL.IsListInUse(serviceId, service.DebtorNumber))
                    throw new FaultException<InUseFault>(new InUseFault(), string.Format("Service with Id {0} is currently in use by 1 or more releases and cannot be changed.", serviceId));

                //uow.BeginTransaction();
                recipientId = listBL.AddRecipient(serviceId, recipient, session.UserId);
                uow.Save();

                return recipientId;
            }
        }

        /// <summary>
        /// Replace all existing Recipients from a ServiceList with a new list of Recipients.
        /// </summary>
        /// <param name="serviceId">The Id of the ServiceList.</param>
        /// <param name="recipients">The list of Recipients.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        public void UpdateAllServiceListRecipients(int serviceId, List<Recipient> recipients, string sessionKey) {
            foreach (var recipient in recipients) {
                if (string.IsNullOrWhiteSpace(recipient.Address))
                    throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), "Recipient Address must not be blank.");
            }

            using (var uow = new UnitOfWork()) {
                var listBL = new ServiceListBL(uow);
                var session = AccessHelper.GetSessionFromKey(sessionKey);
                ServiceList service = listBL.Get(serviceId);

                if (service == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Service with Id {0} not found.", serviceId));

                // Make sure they have access to it.
                if (!AccessHelper.IsAccessAllowedToList(service.DebtorNumber, service.System, session))
                    throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to Service with Id {0} denied.", serviceId));

                if (listBL.IsListInUse(serviceId, service.DebtorNumber))
                    throw new FaultException<InUseFault>(new InUseFault(), string.Format("Service with Id {0} is currently in use by 1 or more releases and cannot be changed.", serviceId));

                //uow.BeginTransaction();
                listBL.UpdateAllRecipients(serviceId, recipients, session.UserId);
                uow.Save();
            }
        }

        /// <summary>
        /// Update a Recipient belonging to a ServiceList.
        /// </summary>
        /// <param name="serviceId">The Id of the ServiceList.</param>
        /// <param name="recipient">The Id of the Recipient.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        public void UpdateServiceListRecipient(int serviceId, Recipient recipient, string sessionKey) {
            using (var uow = new UnitOfWork()) {
                var listBL = new ServiceListBL(uow);
                var session = AccessHelper.GetSessionFromKey(sessionKey);
                ServiceList service = listBL.Get(serviceId);

                if (service == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Service with Id {0} not found.", serviceId));

                // Make sure they have access to it.
                if (!AccessHelper.IsAccessAllowedToList(service.DebtorNumber, service.System, session))
                    throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to Service with Id {0} denied.", serviceId));

                if (listBL.IsListInUse(serviceId, service.DebtorNumber))
                    throw new FaultException<InUseFault>(new InUseFault(), string.Format("Service with Id {0} is currently in use by 1 or more releases and cannot be changed.", serviceId));

                //uow.BeginTransaction();
                listBL.UpdateRecipient(serviceId, recipient, session.UserId);
                uow.Save();
            }
        }

        /// <summary>
        /// Delete a recipient from a ServiceList.
        /// </summary>
        /// <param name="serviceId">The Id of the ServiceList.</param>
        /// <param name="recipientId">The Id of the Recipient.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        public void DeleteServiceListRecipient(int serviceId, int recipientId, string sessionKey) {
            using (var uow = new UnitOfWork()) {
                var listBL = new ServiceListBL(uow);
                var session = AccessHelper.GetSessionFromKey(sessionKey);
                ServiceList service = listBL.Get(serviceId);

                // Make sure they have access to it.
                if (!AccessHelper.IsAccessAllowedToList(service.DebtorNumber, service.System, session))
                    throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to Service with Id {0} denied.", serviceId));

                if (listBL.IsListInUse(serviceId, service.DebtorNumber))
                    throw new FaultException<InUseFault>(new InUseFault(), string.Format("Service with Id {0} is currently in use by 1 or more releases and cannot be changed.", serviceId));

                //uow.BeginTransaction();
                listBL.DeleteRecipient(serviceId, recipientId, session.UserId);
                uow.Save();
            }
        }

        #region Private methods

        public ServiceList GenerateServiceListFromSummary(ServiceListUpload serviceSummary, DBSession session) {
            ServiceList service;

            // Create the release.
            service = AutoMapper.Mapper.Map<ServiceListUpload, ServiceList>(serviceSummary);

            service.DebtorNumber = session.DebtorNumber;
            service.System = session.System;
            service.TransactionType = service.DistributionType;
            service.AccountCode = ServiceListBL.GetAccountCode(service.DebtorNumber, service.DistributionType, service.System);
            service.AddressCount = serviceSummary.Recipients.Count();
            service.ShouldUpdateAddresses = true;
            service.ShouldCharge = true;
            service.ShouldDistribute = true;
            service.ShouldSendResultsToOwner = (service.DebtorNumber != BusinessLayer.Common.Constants.DEBTOR_NUMBER_AAP);
            service.Introduction = string.Empty;
            service.Destinations = string.Empty;
            service.IsVisible = true;
            service.IsMediaList = true;
            service.CreatedDate = DateTime.Now;
            service.CreatedByUserId = session.UserId;
            service.LastModifiedDate = service.CreatedDate;
            service.LastModifiedByUserId = service.CreatedByUserId;

            return service;
        }

        #endregion
    }
}
