﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using AutoMapper;
using Medianet.Wcf;
using Medianet.Service.Contracts;
using Medianet.Service.Dto;
using Medianet.Service.Common;
using Medianet.Service.FaultExceptions;
using Medianet.Service.BusinessLayer;
using Medianet.DataLayer;
using Medianet.Service.BusinessLayer.Common;
using System.IO;

namespace Medianet.Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [ErrorBehaviorAttribute(typeof(ServiceErrorHandler))]
    public class ListService : IListService
    {
        public ListService() {
            // The windows service host normally does this but we do it here when running in debug mode.
            if (!AutoMapperConfigurator.IsInitialized)
                AutoMapperConfigurator.Configure();
        }

        /// <summary>
        /// Gets a ServiceList by it's Id. Only ServiceLists owned by AAP or the Customer are returned.
        /// </summary>
        /// <param name="serviceId">The Id of the ServiceList to get.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The details of the ServiceList.</returns>
        public ServiceList GetServiceListById(int serviceId, string sessionKey) {
            ServiceList service;

            using (var uow = new UnitOfWork()) {
                var listBL = new ServiceListBL(uow);
                service = listBL.Get(serviceId);
            }

            if (service == null)
                throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Service with Id {0} not found.", serviceId));

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToList(service.DebtorNumber, service.System, sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to Service with Id {0} denied.", serviceId));

            return service;
        }

        /// <summary>
        /// Get a List of ServiceList items by the DebtorNumber and System.
        /// </summary>
        /// <param name="debtorNumber">The DebtorNumber of the ServiceLists to get.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The ServiceList items owned by the DebtorNumber.</returns>
        public List<ServiceList> GetServiceListsByDebtorNumber(string debtorNumber, string sessionKey) {
            List<ServiceList> services;
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToList(debtorNumber, session.System, session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to Services for DebtorNumber {0} denied.", debtorNumber));

            using (var uow = new UnitOfWork()) {
                var listBL = new ServiceListBL(uow);
                services = listBL.GetAll(debtorNumber, session.System);
            }

            return services;
        }

        /// <summary>
        /// Get a List of ServiceList items by the DebtorNumber and System.
        /// </summary>
        /// <param name="debtorNumber">The DebtorNumber of the ServiceLists to get.</param>
        /// <param name="system">The System that the ServiceLists belong to.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The ServiceList items owned by the DebtorNumber.</returns>
        public List<ServiceList> GetServiceListsByDebtorNumberSystem(string debtorNumber, SystemType system, string sessionKey) {
            List<ServiceList> services;
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToList(debtorNumber, system, session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to Services for DebtorNumber {0} denied.", debtorNumber));

            using (var uow = new UnitOfWork()) {
                var listBL = new ServiceListBL(uow);
                services = listBL.GetAll(debtorNumber, system);
            }

            return services;
        }

        /// <summary>
        /// Get a List of ServiceList items by the DebtorNumber, System and DistributionType. Paging is supported.
        /// </summary>
        /// <param name="debtorNumber">The DebtorNumber of the ServiceLists to get.</param>
        /// <param name="distType">The DebtorNumber of the ServiceLists to get.</param>
        /// <param name="startPos">The 0 based starting position, e.g. to return the 3rd page with a pageSize of 10 pass 30.</param>
        /// <param name="pageSize">The size of the page to return.</param>
        /// <param name="sortOrder">The order to sort the results by.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The ServiceList items owned by the DebtorNumber.</returns>
        public ServiceListSummaryPage GetServiceListsByDebtorNumberDistributionType(string debtorNumber, DistributionType distType, int startPos, int pageSize, string sessionKey) {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);
            ServiceListSummaryPage page = new ServiceListSummaryPage();

            page.StartPos = startPos;
            page.PageSize = pageSize;

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToList(debtorNumber, session.System, session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to Services for DebtorNumber {0} denied.", debtorNumber));

            using (var uow = new UnitOfWork()) {
                var listBL = new ServiceListBL(uow);
                page.Services = listBL.GetAll(debtorNumber, distType, session.System, startPos, pageSize);
                page.TotalCount = listBL.GetCountByDebtorNumberDistributionType(session.DebtorNumber, distType, session.System);
            }

            return page;
        }

        /// <summary>
        /// Get a List of ServiceList items by the DebtorNumber, System and DistributionType. Paging is supported.
        /// </summary>
        /// <param name="debtorNumber">The DebtorNumber of the ServiceLists to get.</param>
        /// <param name="system">The System that the ServiceLists belong to.</param>
        /// <param name="distType">The DebtorNumber of the ServiceLists to get.</param>
        /// <param name="startPos">The 0 based starting position, e.g. to return the 3rd page with a pageSize of 10 pass 30.</param>
        /// <param name="pageSize">The size of the page to return.</param>
        /// <param name="sortOrder">The order to sort the results by.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The ServiceList items owned by the DebtorNumber.</returns>
        public ServiceListSummaryPage GetServiceListsByDebtorNumberSystemDistributionType(string debtorNumber, SystemType system, DistributionType distType, int startPos, int pageSize, string sessionKey) {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);
            ServiceListSummaryPage page = new ServiceListSummaryPage();

            page.StartPos = startPos;
            page.PageSize = pageSize;

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToList(debtorNumber, system, session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to Services for DebtorNumber {0} denied.", debtorNumber));

            using (var uow = new UnitOfWork()) {
                var listBL = new ServiceListBL(uow);
                page.Services = listBL.GetAll(debtorNumber, distType, system, startPos, pageSize);
                page.TotalCount = listBL.GetCountByDebtorNumberDistributionType(session.DebtorNumber, distType, system);
            }

            return page;
        }

        /// <summary>
        /// Get All the services for an outlet or employee.
        /// </summary>
        /// <param name="debtorNumber">The Debtor number for the outlet to get. normally a fixed debtor no 1</param>
        /// <param name="outletId">The outlet for which services to get</param>
        /// <param name="contactId">ContactID if not null then returns services associated with employee</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>List of services tied to an outlet or employee</returns>
        public List<ServiceList> GetServiceListByDebtorNumberOutletIdContactId(string debtorNumber, string outletId,
                                                                               string contactId, string sessionKey)
        {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToList(debtorNumber, session.System, session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to Services for DebtorNumber {0} denied.", debtorNumber));
            using (var uow = new UnitOfWork()) {
                var listBL = new ServiceListBL(uow);
                return listBL.GetAllByDebtorNumberOutletIdContactId(debtorNumber, outletId, contactId, session.System);

            }
        }

        /// <summary>
        /// Get a PackageList by it's Id. Only PackageLists owned by AAP or the Customer are returned.
        /// </summary>
        /// <param name="packageId">The Id of the PackageList to get.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The details of the PackageList.</returns>
        public PackageList GetPackageListById(int packageId, string sessionKey) {
            PackageList package;

            using (var uow = new UnitOfWork())
            {
                var listBL = new PackageListBL(uow);
                package = listBL.Get(packageId);
            }

            if (package == null)
                throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Package with Id {0} not found.", packageId));

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToList(package.DebtorNumber, package.System, sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to Package with Id {0} denied.", packageId));

            return package;
        }

        /// <summary>
        /// Get a list of PackageLists flagged as top value.
        /// </summary>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A list of PackageList objects.</returns>
        public List<PackageList> GetTopValuePackageLists(string sessionKey)
        {
            List<PackageList> packages;

            AccessHelper.GetSessionFromKey(sessionKey);

            using (var uow = new UnitOfWork())
            {
                var listBL = new PackageListBL(uow);
                packages = listBL.GetTopValue();
            }

            return packages;
        }

        /// <summary>
        /// Get a List of PackageLists owned by a DebtorNumber.
        /// </summary>
        /// <param name="debtorNumber">The DebtorNumber of the PackageList items to get.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The PackageList items owned by the DebtorNumber.</returns>
        public List<PackageList> GetPackageListsByDebtorNumber(string debtorNumber, string sessionKey) {
            List<PackageList> packages;
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToList(debtorNumber, session.System, session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to Packages for DebtorNumber {0} denied.", debtorNumber));

            using (var uow = new UnitOfWork())
            {
                var listBL = new PackageListBL(uow);
                packages = listBL.GetAllByDebtorNumberSystem(debtorNumber, session.System);
            }

            return packages;
        }

        /// <summary>
        /// Get a List of PackageLists owned by a DebtorNumber.
        /// </summary>
        /// <param name="debtorNumber">The DebtorNumber of the PackageList items to get.</param>
        /// <param name="system">The System that the PackageLists belong to.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The PackageList items owned by the DebtorNumber.</returns>
        public List<PackageList> GetPackageListsByDebtorNumberSystem(string debtorNumber, SystemType system, string sessionKey) {
            List<PackageList> packages;
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToList(debtorNumber, system, session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to Packages for DebtorNumber {0} denied.", debtorNumber));

            using (var uow = new UnitOfWork())
            {
                var listBL = new PackageListBL(uow);
                packages = listBL.GetAllByDebtorNumberSystem(debtorNumber, system);
            }

            return packages;
        }

        /// <summary>
        /// Get all List Categories.
        /// </summary>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A list of all ListCategory items.</returns>
        public List<ListCategory> GetListCategories(string sessionKey) {
            // Get the DBSession object just to make sure it's valid.
            AccessHelper.GetSessionFromKey(sessionKey);

            using (var uow = new UnitOfWork()) {
                var listBL = new ListCategoryBL(uow);
                return listBL.GetAll().Where(lc => lc.IsVisible == true).ToList();
            }
        }

        /// <summary>
        /// Get the Comment for a ServiceList
        /// </summary>
        /// <param name="serviceId">The Id of the ServiceList.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The Comment.</returns>
        public ListComment GetServiceCommentById(int serviceId, string sessionKey) {
            // Get the DBSession object just to make sure it's valid.
            AccessHelper.GetSessionFromKey(sessionKey);

            using (var uow = new UnitOfWork()) {
                var listBL = new ServiceListBL(uow);
                // Only AAP lists have comments so don't bother checking access.
                return listBL.GetComment(serviceId);
            }
        }

        /// <summary>
        /// Get the Comment for a PackageList
        /// </summary>
        /// <param name="packageId">The Id of the PackageList.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The Comment.</returns>
        public ListComment GetPackageCommentById(int packageId, string sessionKey) {
            // Get the DBSession object just to make sure it's valid.
            AccessHelper.GetSessionFromKey(sessionKey);

            // Only AAP lists have comments so don't bother checking access.
            using (var uow = new UnitOfWork())
            {
                var listBL = new PackageListBL(uow);
                return listBL.GetCommentById(packageId);
            }
        }

        /// <summary>
        /// Get the Outlet names of all recipients for this ServiceList.
        /// </summary>
        /// <param name="serviceId">The Id of the ServiceList.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The list of Outlet names.</returns>
        public List<string> GetServiceRecipientOutletNamesById(int serviceId, string sessionKey) {
            using (var uow = new UnitOfWork()) {
                var listBL = new ServiceListBL(uow);
                ServiceList service = listBL.Get(serviceId);

                if (service == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Service with Id {0} not found.", serviceId));

                // Make sure they have access to it.
                if (!AccessHelper.IsAccessAllowedToList(service.DebtorNumber, service.System, sessionKey))
                    throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to Service with Id {0} denied.", serviceId));

                return listBL.GetRecipientOutletNames(serviceId).OrderBy(o => o).ToList();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceIds"></param>
        /// <param name="sessionKey"></param>
        /// <returns></returns>
        public List<ListInformation> GetServiceListInfoByIds(List<int> serviceIds, string sessionKey) {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);

            using (var uow = new UnitOfWork()) {
                var listBL = new ServiceListBL(uow);
                List<ListInformation> listInfo = new List<ListInformation>();

                foreach (int id in serviceIds) {
                    var info = new ListInformation();
                    ServiceList service = listBL.Get(id);

                    if (service == null)
                        throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Service with Id {0} not found.", id));

                    // Make sure they have access to it.
                    if (!AccessHelper.IsAccessAllowedToList(service.DebtorNumber, service.System, session))
                        throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to Service with Id {0} denied.", id));

                    info.Id = id;
                    info.Name = service.SelectionDescription;
                    info.Introduction = service.Introduction;
                    info.OutletNames = listBL.GetRecipientOutletNames(id).OrderBy(o => o).ToList();
                    info.Comment = listBL.GetComment(id);
                    listInfo.Add(info);
                }

                return listInfo;
            }
        }

        /// <summary>
        /// Get the Outlet names of all recipients for a PackageList.
        /// </summary>
        /// <param name="packageId">The Id of the PackageList.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The list of Outlet names.</returns>
        public List<string> GetPackageRecipientOutletsById(int packageId, string sessionKey) {
            PackageList package;

            using (var uow = new UnitOfWork())
            {
                var listBL = new PackageListBL(uow);
                package = listBL.Get(packageId);

                if (package == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Package with Id {0} not found.", packageId));

                // Make sure they have access to it.
                if (!AccessHelper.IsAccessAllowedToList(package.DebtorNumber, package.System, sessionKey))
                    throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to Package with Id {0} denied.", packageId));

                return listBL.GetRecipientOutletsById(packageId).OrderBy(o => o).ToList();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="packageIds"></param>
        /// <param name="sessionKey"></param>
        /// <returns></returns>
        public List<ListInformation> GetPackageListInfoByIds(List<int> packageIds, string sessionKey) {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);

            using (var uow = new UnitOfWork())
            {
                var listBL = new PackageListBL(uow);
                List<ListInformation> listInfo = new List<ListInformation>();

                foreach (int id in packageIds) {
                    var info = new ListInformation();
                    PackageList package = listBL.Get(id);

                    if (package == null)
                        throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Package with Id {0} not found.", id));

                    // Make sure they have access to it.
                    if (!AccessHelper.IsAccessAllowedToList(package.DebtorNumber, package.System, session))
                        throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to Package with Id {0} denied.", id));

                    info.Id = id;
                    info.Name = package.SelectionDescription;
                    info.Introduction = package.Introduction;
                    info.OutletNames = listBL.GetRecipientOutletsById(id).OrderBy(o => o).ToList();
                    info.Comment = listBL.GetCommentById(id);
                    info.AttachmentFileName = listBL.GetAttachmentFileName(id);
                    listInfo.Add(info);
                }

                return listInfo;
            }
        }

        /// <summary>
        /// Get the package attachment file
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public byte[] GetPackageAttachmentFile(int packageId, string fileName)
        {
            var imagePath = PathHelper.GetPackageAttachmentPath();
            
            Byte[] result = System.IO.File.ReadAllBytes(imagePath + packageId + Path.GetExtension(fileName));

            return result;
       }

        /// <summary>
        /// Get the next ServiceList list number likely to be used when creating a new ServiceList.
        /// This is only a guide as the list number will be determined when creating a new ServiceList.
        /// </summary>
        /// <param name="distType">The distribution type (e.g. Email) to filter by when determining the next list number.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The next list number likely to be used when creating the next ServiceList.</returns>
        public int GetNextServiceListNumber(DistributionType distType, string sessionKey) {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);

            using (var uow = new UnitOfWork()) {
                var listBL = new ServiceListBL(uow);
                return listBL.GetNextListNumber(session.DebtorNumber, distType, session.System);
            }
        }

        /// <summary>
        /// Get a list of recipients of a ServiceList.
        /// </summary>
        /// <param name="serviceId">The Id of the ServiceList.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A list of ServiceList recipients.</returns>
        public List<Recipient> GetServiceListRecipients(int serviceId, string sessionKey) {
            using (var uow = new UnitOfWork()) {
                var session = AccessHelper.GetSessionFromKey(sessionKey);
                var listBL = new ServiceListBL(uow);

                return listBL.GetRecipients(serviceId);
            }
        }

        /// <summary>
        /// Gets a ticker of AAP ServiceLists.
        /// </summary>
        /// <returns></returns>
        public List<ListTicker> GetTicker() {
            List<ListTicker> ticker;

            using (var uow = new UnitOfWork()) {
                var serviceBL = new ServiceListBL(uow);
                var packageBL = new PackageListBL(uow);

                ticker = serviceBL.GetForTicker();
                ticker.AddRange(packageBL.GetForTicker());
            }

            return ticker.OrderBy(t => t.ListCategoryParentSequenceNumber).ThenBy(t => t.ListCategorySequenceNumber).ThenBy(t => t.ListSequenceNumber).ToList();
        }

        /// <summary>
        /// Get's the top performing lists in a period.
        /// </summary>
        /// <param name="period">The period to use. Either Month or Week.</param>
        /// <param name="maxRows">The max number of rows to return.</param>
        /// <returns></returns>
        public List<TopPerformingList> GetTopPerformingLists(PeriodType period, int maxRows)
        {
            using (var uow = new UnitOfWork())
            {
                var listBL = new ServiceListBL(uow);
                return listBL.GetTopPerformingLists(period, maxRows);
            }
        }

        /// <summary>
        /// Update a Service or Package on the database by the AccountCode. If not found then a new one is created.
        /// </summary>
        /// <param name="accountCode">The unique AccountCode used to find the Service or Package.</param>
        /// <param name="description">The description for the Service or Package.</param>
        /// <param name="distributionType">The DistributionType, e.g. "Email", "Package".</param>
        public void UpdateAapListByAccountCode(string accountCode, string description, string distributionType)
        {
            DistributionType distType;

            // Convert the distributionType string to an enum. If it's invalid then ignore as
            // it's most likely "Other" which doesn't exist on the database and doesn't need updating.
            if (Enum.TryParse(distributionType, out distType))
            {
                if (distType == DistributionType.Package)
                    UpdateAapPackageByAccountCode(accountCode, description);
                else
                    UpdateAapServiceByAccountCode(accountCode, description, distType);
            }
        }

        /// <summary>
        /// Searches for ServiceLists that contain outlets matching a given name.
        /// </summary>
        /// <param name="outletName">The outlet name to search for.</param>
        /// <param name="sessionKey">he SessionKey of the user calling the method.</param>
        /// <returns>A list of ServiceList items.</returns>
        public List<ServiceList> SearchAapListsByOutletId(string outletId, string sessionKey)
        {
            using (var uow = new UnitOfWork()) {
                var session = AccessHelper.GetSessionFromKey(sessionKey);
                var listBL = new ServiceListBL(uow);

                return listBL.SearchAapListsByOutletId(outletId);
            }
        }

        #region Private Methods

        private void UpdateAapPackageByAccountCode(string accountCode, string description)
        {
            using (var uow = new UnitOfWork())
            {
                var listBL = new PackageListBL(uow);
                List<PackageList> packages = listBL.GetAllByDebtorNumberAccountCode(Medianet.Service.BusinessLayer.Common.Constants.DEBTOR_NUMBER_AAP, accountCode);

                if (packages.Count > 1)
                    return; // There's more than 1 service with this code. It doesn't require updating.

                if (packages.Count == 0)
                {
                    var package = PackageList.Create(Constants.DEBTOR_NUMBER_AAP, accountCode, description, DistributionType.Fax);
                    var id = listBL.Add(package, 0);
                }
                else
                {
                    packages[0].SelectionDescription = description;
                    listBL.Update(packages[0], 0);
                }

                uow.Save();
            }
        }

        private void UpdateAapServiceByAccountCode(string accountCode, string description, DistributionType distributionType)
        {
            using (var uow = new UnitOfWork())
            {
                var listBL = new ServiceListBL(uow);
                List<ServiceList> services = listBL.GetAllByDebtorNumberAccountCode(Medianet.Service.BusinessLayer.Common.Constants.DEBTOR_NUMBER_AAP, accountCode);

                if (services.Count > 1)
                    return; // There's more than 1 service with this code. It doesn't require updating.

                if (services.Count == 0) {
                    var service = ServiceList.Create(Medianet.Service.BusinessLayer.Common.Constants.DEBTOR_NUMBER_AAP, accountCode, description, distributionType);
                    var id = listBL.Add(service, 0);
                }
                else
                {
                    if (services[0].DistributionType != distributionType)
                        throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), string.Format("Cannot change DistributionType of Service from {0} to {1}.", services[0].DistributionType, distributionType));

                    services[0].SelectionDescription = description;
                    listBL.Update(services[0], 0);
                }

                uow.Save();
            }
        }

        #endregion
    }
}
