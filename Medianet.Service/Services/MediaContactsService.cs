﻿using System;
using System.Collections.Generic;
using System.Linq;
using Medianet.Wcf;
using Medianet.Service.Contracts;
using Medianet.Service.Dto;
using Medianet.Service.Common;
using Medianet.Service.FaultExceptions;
using Medianet.Service.BusinessLayer;
using Medianet.DataLayer;
using System.ServiceModel;
using Medianet.Service.BusinessLayer.Exceptions;
using System.Data.Entity.Infrastructure;
using System.IO;
using Medianet.BusinessLayer.Common;
using Medianet.Service.BusinessLayer.Common;

namespace Medianet.Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [ErrorBehaviorAttribute(typeof(ServiceErrorHandler))]
    public class MediaContactsService : IMediaContactsService
    {
        public MediaContactsService()
        {
            // The windows service host normally does this but we do it here when running in debug mode.
            if (!AutoMapperConfigurator.IsInitialized)
                AutoMapperConfigurator.Configure();
        }

        #region ----- Media Contact fetch/add/update methods -----

        /// <summary>
        /// Get a Media Contact.
        /// </summary>
        /// <param name="contactId">The Id of the Media Contact</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The Media Contact requested.</returns>
        public MediaContact GetMediaContact(string contactId, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to MediaContact with Id {0} denied.", contactId));

            using (var uow = new UnitOfWork())
            {
                var contactBL = new MediaContactBL(uow);
                var contact = contactBL.Get(contactId);

                if (contact == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("MediaContact with Id {0} not found.", contactId));

                return contact;
            }
        }

        /// <summary>
        /// Adds a new MediaContact to the database.
        /// </summary>
        /// <param name="entity">The MediaContact details which we want to apply.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The Id of the Media Contact created.</returns>
        public string AddMediaContact(MediaContact entity, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdmin(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Adding of MediaContact data denied.");

            using (var uow = new UnitOfWork())
            {
                var contactBL = new MediaContactBL(uow);

                try
                {
                    return contactBL.Add(entity, session.UserId);
                }
                catch (SubjectPrimaryContactRequiredException ex)
                {
                    throw new FaultException<SubjectPrimaryContactRequiredFault>(new SubjectPrimaryContactRequiredFault(ex.OutletId, ex.SubjectId, ex.Message), ex.Message);
                }
                catch (UpdateException updateEx)
                {
                    throw new FaultException<MediaUpdateValidationFault>(new MediaUpdateValidationFault(updateEx.Message, updateEx.ReasonList), updateEx.Message);
                }
            }
        }

        /// <summary>
        /// Applies the contact details specified in entity to the database.
        /// </summary>
        /// <param name="entity">The MediaContact details with updated content.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        public void UpdateMediaContact(MediaContact entity, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdmin(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Updating of MediaContact data denied.");

            using (var uow = new UnitOfWork())
            {
                try
                {
                    new MediaContactBL(uow).Update(entity, session.UserId);
                }
                catch (SubjectPrimaryContactRequiredException ex)
                {
                    throw new FaultException<SubjectPrimaryContactRequiredFault>(new SubjectPrimaryContactRequiredFault(ex.OutletId, ex.SubjectId, ex.Message), ex.Message);
                }
                catch (UpdateException updateEx)
                {
                    throw new FaultException<MediaUpdateValidationFault>(new MediaUpdateValidationFault(updateEx.Message, updateEx.ReasonList), updateEx.Message);
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw new FaultException<DataConcurrencyFault>(new DataConcurrencyFault(), DataConcurrencyFault.FaultDescription);
                }
            }
        }

        public int? GetUserIdByContactId(string contactId, string sessionKey)
        {
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), $"Access to MediaContact with Id {contactId} denied.");

            using (var uow = new UnitOfWork())
            {
                var userId = new UserBL(uow).GetUserIdByContactId(contactId);

                return userId;
            }
        }
        #endregion

        #region ----- Media Outlet fetch/add/update methods -----

        /// <summary>
        /// Get a MediaOutlet.
        /// </summary>
        /// <param name="outletId">The Id of the MediaOutlet</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The MediaOutlet requested.</returns>
        public MediaOutlet GetMediaOutlet(string outletId, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to MediaOutlet with Id {0} denied.", outletId));

            using (var uow = new UnitOfWork())
            {
                var outletBL = new MediaOutletBL(uow);
                var outlet = outletBL.Get(outletId);

                if (outlet == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("MediaOutlet with Id {0} not found.", outletId));
                var srvlist = new ServiceListBL(uow);
                outlet.ServiceLists = srvlist.GetAllByDebtorNumberOutletIdContactId(BusinessLayer.Common.Constants.DEBTOR_NUMBER_AAP, outletId, null, SystemType.Medianet);
                if (outlet.ServiceLists != null)
                {
                    outlet.EmailServiceIdsCsv = string.Join(",",
                                                            outlet.ServiceLists.Where(
                                                                s => s.DistributionType == DistributionType.Email)
                                                                  .Select(s => s.Id)
                                                                  .ToList());
                }

                return outlet;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="partialName"></param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns></returns>
        public List<MediaOutlet> GetMediaOutletsByName(string partialName, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Access to MediaOutlets denied.");

            using (var uow = new UnitOfWork())
            {
                var outletBL = new MediaOutletBL(uow);
                var outlet = outletBL.GetByName(partialName);

                return outlet;
            }
        }

        /// <summary>
        /// Adds a new MediaOutlet to the database.
        /// </summary>
        /// <param name="entity">The MediaOutlet details which we want to apply.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The Id of the MediaOutlet created.</returns>
        public string AddMediaOutlet(MediaOutlet entity, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdmin(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Adding of MediaOutlet data denied.");

            using (var uow = new UnitOfWork())
            {
                var outletBL = new MediaOutletBL(uow);

                try
                {
                    return outletBL.Add(entity, session.UserId);
                }
                catch (SubjectPrimaryContactRequiredException ex)
                {
                    throw new FaultException<SubjectPrimaryContactRequiredFault>(new SubjectPrimaryContactRequiredFault(ex.OutletId, ex.SubjectId, ex.Message), ex.Message);
                }
                catch (UpdateException updateEx)
                {
                    throw new FaultException<MediaUpdateValidationFault>(new MediaUpdateValidationFault(updateEx.Message, updateEx.ReasonList), updateEx.Message);
                }
            }
        }

        /// <summary>
        /// Applies the outlet details specified in entity to the database.
        /// </summary>
        /// <param name="entity">The MediaOutlet details with updated content.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        public void UpdateMediaOutlet(MediaOutlet entity, List<MediaEmployeeSubjectValidation> validation, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdmin(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Updating of MediaOutlet data denied.");

            using (var uow = new UnitOfWork())
            {
                try
                {
                    new MediaOutletBL(uow).Update(entity, validation, session.UserId);
                }
                catch (SubjectPrimaryContactRequiredException ex)
                {
                    throw new FaultException<SubjectPrimaryContactRequiredFault>(new SubjectPrimaryContactRequiredFault(ex.OutletId, ex.SubjectId, ex.Message), ex.Message);
                }
                catch (UpdateException updateEx)
                {
                    throw new FaultException<MediaUpdateValidationFault>(new MediaUpdateValidationFault(updateEx.Message, updateEx.ReasonList), updateEx.Message);
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw new FaultException<DataConcurrencyFault>(new DataConcurrencyFault(), DataConcurrencyFault.FaultDescription);
                }
            }
        }

        /// <summary>
        /// Get MediaOutlets Id and Name.
        /// </summary>
        /// <param name="outletIds">The Ids of the MediaOutlet</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The MediaOutlets requested.</returns>
        public List<MediaOutletPartial> GetMediaOutletsByIds(string[] outletIds, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to MediaOutlet with Id {0} denied.", outletIds));

            using (var uow = new UnitOfWork())
            {
                var outletBL = new MediaOutletBL(uow);
                var outlet = outletBL.GetOutletsByIds(outletIds);

                if (outlet == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("MediaOutlet with Ids provided not found."));

                return outlet;
            }
        }

        /// <summary>
        /// Get Related MediaOutlets
        /// </summary>
        /// <param name="outletIds">The Id of the MediaOutlet</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The related MediaOutlets requested.</returns>
        public List<MediaOutletPartial> GetRelatedMediaOutlets(string outletId, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to MediaOutlet with Id {0} denied.", outletId));

            using (var uow = new UnitOfWork())
            {
                var outletBL = new MediaOutletBL(uow);
                var outlet = outletBL.GetRelatedOutlets(outletId);

                if (outlet == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("MediaOutlet with Id provided not found."));

                return outlet;
            }
        }

        /// <summary>
        /// Get Syndicating To MediaOutlets
        /// </summary>
        /// <param name="outletIds">The Id of the MediaOutlet</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The syndicated MediaOutlets requested.</returns>
        public List<MediaOutletPartial> GetSyndicatingToMediaOutlets(string outletId, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to MediaOutlet with Id {0} denied.", outletId));

            var session = AccessHelper.GetSessionFromKey(sessionKey);

            using (var uow = new UnitOfWork())
            {
                var outletBL = new MediaOutletBL(uow);
                var outlet = outletBL.GetSyndicatingToOutlets(outletId);

                if (outlet == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("MediaOutlet with Id provided not found."));

                return outlet;
            }
        }

        /// <summary>
        /// Get Syndicated From MediaOutlets
        /// </summary>
        /// <param name="outletIds">The Id of the MediaOutlet</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The syndicated MediaOutlets requested.</returns>
        public List<MediaOutletPartial> GetSyndicatedFromMediaOutlets(string outletId, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to MediaOutlet with Id {0} denied.", outletId));

            var session = AccessHelper.GetSessionFromKey(sessionKey);

            using (var uow = new UnitOfWork())
            {
                var outletBL = new MediaOutletBL(uow);
                var outlet = outletBL.GetSyndicatedFromOutlets(outletId);

                if (outlet == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("MediaOutlet with Id provided not found."));

                return outlet;
            }
        }

        /// <summary>
        /// Get MediaOwner Businesses
        /// </summary>
        /// <param name="outletIds">The Id of the MediaOutlet</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The MediaOwner Outlets requested.</returns>
        public List<MediaOutletPartial> GetMediaOwnerBusinesses(string outletId, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to MediaOutlet with Id {0} denied.", outletId));

            using (var uow = new UnitOfWork())
            {
                var outletBL = new MediaOutletBL(uow);
                var outlet = outletBL.GetMediaOwnerBusinesses(outletId);

                if (outlet == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("MediaOutlet with Id provided not found."));

                return outlet;
            }
        }

        public LogoUploadResponse UploadOutletLogo(LogoUploadRequest request)
        {
            DBSession session = AccessHelper.GetSessionFromKey(request.SessionKey);
            TempFile tFile;

            if (!AccessHelper.IsAdmin(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Outlet logo upload access denied.");

            using (var uow = new UnitOfWork())
            {
                var tempBL = new TempFileBL(uow);
                tFile = tempBL.Get(request.TempFileId);

                if (tFile == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("TempFile with Id {0} not found.", request.TempFileId));

                if (session.Key != tFile.SessionKey)
                    throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access denied to TempFile with Id {0}.", request.TempFileId));

                var filePath = PathHelper.GetTempFilePathAndName(tFile.Id, tFile.FileExtension);

                if (!File.Exists(filePath))
                    throw new FaultException<FileNotFoundFault>(new FileNotFoundFault(), string.Format("Content for TempFile with Id {0} not found.", request.TempFileId));

                string subdir;
                var imageFileName = PathHelper.BuildLogoImageNameAndPath(request.OutletId, tFile.OriginalFilename, true, out subdir);
                var imagePath = PathHelper.GetContactsLogoEnvironmentPath();
                var partialPath = $"{subdir}\\{imageFileName}";

                File.Copy(filePath, $"{imagePath}{partialPath}");

                if (request.UpdateOutlet)
                {
                    var outletBL = new MediaOutletBL(uow);
                    outletBL.SetOutletLogo(request.OutletId, partialPath, session.UserId);
                }

                return new LogoUploadResponse() { LogoFileName = partialPath, status = true };
            }
        }

        public LogoFileResponse GetOutletLogo(LogoFileRequest fileRequest)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(fileRequest.sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to GetOutletLogo with logoFileName {0} denied.", fileRequest.LogoFileName));

            var imagePath = fileRequest.Draft ? PathHelper.GetContactsDraftLogoEnvironmentPath() : PathHelper.GetContactsLogoEnvironmentPath();
            var firstIndex = fileRequest.LogoFileName.IndexOf("\\", StringComparison.Ordinal);
            string subdir = string.Empty;
            if (firstIndex > 0)
            {
                subdir = fileRequest.LogoFileName.Substring(0, firstIndex);
            }
            var fullFileName = $"{imagePath}{subdir}\\{fileRequest.LogoFileName.Substring(firstIndex + 1)}";

            if (!File.Exists(fullFileName))
            {
                throw new FaultException<FileNotFoundFault>(new FileNotFoundFault(), $"Cannot find image file {fileRequest.LogoFileName}");
            }
            var response = new LogoFileResponse() { LogoFileName = fileRequest.LogoFileName };
            response.FileData = File.OpenRead(fullFileName);

            // Create a callback to close the file once the connection is closed
            OperationContext clientContext = OperationContext.Current;
            clientContext.OperationCompleted += new EventHandler(delegate (object sender, EventArgs args)
            {
                if (response.FileData != null)
                    response.FileData.Dispose();
            });

            return response;
        }

        #endregion

        #region ----- Media Employee fetch/add/update methods -----

        /// <summary>
        /// Get a MediaEmployee by ContactId and OutletId.
        /// </summary>
        /// <param name="contactId">The Id of the MediaContact</param>
        /// <param name="outletId">The Id of the MediaOutlet</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A single MediaEmployee.</returns>
        public MediaEmployee GetMediaEmployee(string contactId, string outletId, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to MediaEmployee with ContactId {0}, OutletId {1} denied.", contactId, outletId));

            using (var uow = new UnitOfWork())
            {
                var contactBL = new MediaEmployeeBL(uow);
                var employee = contactBL.Get(contactId.Trim(), outletId.Trim());

                if (employee == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("MediaEmployee with ContactId {0}, OutletId {1} not found.", contactId, outletId));

                var srvlist = new ServiceListBL(uow);
                employee.ServiceLists = srvlist.GetAllByDebtorNumberOutletIdContactId(BusinessLayer.Common.Constants.DEBTOR_NUMBER_AAP, outletId, contactId, SystemType.Medianet);

                if (employee.ServiceLists != null)
                {
                    employee.EmailServiceIdsCsv = string.Join(",",
                                                            employee.ServiceLists.Where(
                                                                s => s.DistributionType == DistributionType.Email)
                                                                  .Select(s => s.Id)
                                                                  .ToList());
                }
                return employee;
            }
        }

        /// <summary>
        /// Get a MediaEmployee details by ContactId and OutletId.
        /// </summary>
        /// <param name="contactId">The Id of the MediaContact</param>
        /// <param name="outletId">The Id of the MediaOutlet</param>
        /// <param name="userId"></param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A single MediaEmployee.</returns>
        public MediaEmployeeView GetMediaEmployeeDetails(string contactId, string outletId, int userId, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), $"Access to MediaEmployee with ContactId {contactId}, OutletId {outletId} denied.");

            using (var uow = new UnitOfWork())
            {
                var employee = new MediaEmployeeBL(uow).GetDetails(contactId.Trim(), outletId.Trim(), userId);

                if (employee == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), $"MediaEmployee with ContactId {contactId}, OutletId {outletId} not found.");

                return employee;
            }
        }

        /// <summary>
        /// Get all MediaEmployees by Contact Id.
        /// </summary>
        /// <param name="contactId">The Id of the MediaContact</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A list of MediaEmployees.</returns>
        public List<MediaEmployee> GetMediaEmployeesByContactId(string contactId, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to MediaEmployees for ContactId {0} denied.", contactId));

            using (var uow = new UnitOfWork())
            {
                var contactBL = new MediaContactBL(uow);
                var employees = contactBL.GetAllEmployees(contactId);

                return employees;
            }
        }

        /// <summary>
        /// Get Outlets' Id and names for a contact.
        /// </summary>
        /// <param name="contactId">The Id of the MediaContact</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A list of MediaEmployees.</returns>
        public List<MediaOutlet> GetEmployeesOutletsInfo(string contactId, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), $"Access to MediaEmployees for ContactId {contactId} denied.");

            using (var uow = new UnitOfWork())
            {
                var result = new MediaEmployeeBL(uow).GetEmployeesOutletsInfo(contactId);

                return result;
            }
        }

        /// <summary>
        /// Get all MediaEmployees by Outlet Id.
        /// </summary>
        /// <param name="outletId">The Id of the MediaOutlet</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A list of MediaEmployees.</returns>
        public List<MediaEmployee> GetMediaEmployeesByOutletId(string outletId, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to MediaEmployees for OutletId {0} denied.", outletId));

            using (var uow = new UnitOfWork())
            {
                var employeeBL = new MediaEmployeeBL(uow);
                var employees = employeeBL.GetAllByOutletId(outletId);

                return employees;
            }
        }

        /// <summary>
        /// Adds a new MediaEmployee to the database.
        /// </summary>
        /// <param name="entity">The MediaEmployee details which we want to apply.</param>
        /// <param name="validation">The MediaEmployeeValidation object used to resolve validation issues.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        public void AddMediaEmployee(MediaEmployee entity, MediaEmployeeValidation validation, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdmin(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Adding of MediaEmployee data denied.");

            using (var uow = new UnitOfWork())
            {
                var employeeBL = new MediaEmployeeBL(uow);

                try
                {
                    employeeBL.Add(entity, validation, session.UserId);
                }
                catch (MultiplePrimaryContactsException ex)
                {
                    throw new FaultException<MediaUpdateValidationFault>(new MediaUpdateValidationFault(ex.OutletId, UpdateErrorType.OutletMultiplePrimaryContactsExist, ex.Message), ex.Message);
                }
                catch (OutletPrimaryContactRequiredException ex)
                {
                    throw new FaultException<MediaUpdateValidationFault>(new MediaUpdateValidationFault(ex.OutletId, UpdateErrorType.OutletPrimaryContactRequired, ex.Message), ex.Message);
                }
                catch (SubjectPrimaryContactRequiredException ex)
                {
                    throw new FaultException<SubjectPrimaryContactRequiredFault>(new SubjectPrimaryContactRequiredFault(ex.OutletId, ex.SubjectId, ex.Message), ex.Message);
                }
                catch (UpdateException updateEx)
                {
                    throw new FaultException<MediaUpdateValidationFault>(new MediaUpdateValidationFault(updateEx.Message, updateEx.ReasonList), updateEx.Message);
                }
            }
        }

        /// <summary>
        /// Applies the MediaEmployee details specified in entity to the database.
        /// </summary>
        /// <param name="entity">The MediaEmployee details with updated content.</param>
        /// <param name="validation">The MediaEmployeeValidation object used to resolve validation issues.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        public void UpdateMediaEmployee(MediaEmployee entity, MediaEmployeeValidation validation, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdmin(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Updating of MediaEmployee data denied.");

            using (var uow = new UnitOfWork())
            {
                var employeeBL = new MediaEmployeeBL(uow);

                try
                {
                    employeeBL.Update(entity, validation, session.UserId);
                }
                catch (MultiplePrimaryContactsException ex)
                {
                    throw new FaultException<MediaUpdateValidationFault>(new MediaUpdateValidationFault(ex.OutletId, UpdateErrorType.OutletMultiplePrimaryContactsExist, ex.Message), ex.Message);
                }
                catch (OutletPrimaryContactRequiredException ex)
                {
                    throw new FaultException<MediaUpdateValidationFault>(new MediaUpdateValidationFault(ex.OutletId, UpdateErrorType.OutletPrimaryContactRequired, ex.Message), ex.Message);
                }
                catch (SubjectPrimaryContactRequiredException ex)
                {
                    throw new FaultException<SubjectPrimaryContactRequiredFault>(new SubjectPrimaryContactRequiredFault(ex.OutletId, ex.SubjectId, ex.Message), ex.Message);
                }
                catch (UpdateException updateEx)
                {
                    throw new FaultException<MediaUpdateValidationFault>(new MediaUpdateValidationFault(updateEx.Message, updateEx.ReasonList), updateEx.Message);
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw new FaultException<DataConcurrencyFault>(new DataConcurrencyFault(), DataConcurrencyFault.FaultDescription);
                }
            }
        }

        /*/// <summary>
        /// Set the Specified Contact as the Primary contact at an Outlet.
        /// </summary>
        /// <param name="outletId">The Id of the MediaOutlet</param>
        /// <param name="contactId">The Id of the MediaContact</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        public void SetOutletPrimaryContact(string outletId, string contactId, string sessionKey) {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdmin(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Updating of MediaEmployee data denied.");

            using (var uow = new UnitOfWork()) {
                var employeeBL = new MediaEmployeeBL(uow);

                try {
                    employeeBL.SetOutletPrimaryContact(outletId, contactId, session.UserId);
                }
                catch (UpdateException updateEx) {
                    throw new FaultException<MediaUpdateValidationFault>(new MediaUpdateValidationFault(updateEx.Message, updateEx.ReasonList), updateEx.Message);
                }
            }
        }

        /// <summary>
        /// Set the specified Contact as the Primary Contact for a Subject at an Outlet.
        /// </summary>
        /// <param name="outletId">The Id of the MediaOutlet</param>
        /// <param name="contactId">The Id of the MediaContact</param>
        /// <param name="subjectId">The Id of the MediaEmployeeSubject</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        public void SetSubjectPrimaryContact(string outletId, string contactId, int subjectId, string sessionKey) {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdmin(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Updating of MediaEmployee data denied.");

            using (var uow = new UnitOfWork()) {
                var employeeBL = new MediaEmployeeBL(uow);

                try {
                    employeeBL.SetSubjectPrimaryContact(outletId, contactId, subjectId, session.UserId);
                }
                catch (UpdateException updateEx) {
                    throw new FaultException<MediaUpdateValidationFault>(new MediaUpdateValidationFault(updateEx.Message, updateEx.ReasonList), updateEx.Message);
                }
            }
        }*/

        #endregion

        #region ----- MediaContactsService Lookup/Fetch functions

        /// <summary>
        /// Get a Contact Role.
        /// </summary>
        /// <param name="roleId">The Id of the Contact Role</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The Contact Role requested.</returns>
        public ContactRole GetContactRole(int roleId, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to ContactRole with Id {0} denied.", roleId));

            using (var uow = new UnitOfWork())
            {
                var workBL = new ContactRoleBL(uow);
                var work = workBL.Get(roleId);

                if (work == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("ContactRole with Id {0} not found.", roleId));

                return work;
            }
        }

        /// <summary>
        /// Get all Contact Roles.
        /// </summary>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A List of Contact Roles.</returns>
        public List<ContactRole> GetContactRoles(string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Access to ContactRoles denied.");

            using (var uow = new UnitOfWork())
            {
                var workBL = new ContactRoleBL(uow);
                return workBL.GetAll();
            }
        }

        /// <summary>
        /// Get a Outlet Type.
        /// </summary>
        /// <param name="outletTypeId">The Id of the Outlet Type</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The Outlet Type requested.</returns>
        public OutletType GetOutletType(int outletTypeId, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to OutletType with Id {0} denied.", outletTypeId));

            using (var uow = new UnitOfWork())
            {
                var outletBL = new OutletTypeBL(uow);
                var outlet = outletBL.Get(outletTypeId);

                if (outlet == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("OutletType with Id {0} not found.", outletTypeId));

                return outlet;
            }
        }

        /// <summary>
        /// Get all Outlet Types.
        /// </summary>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A List of Outlet Types.</returns>
        public List<OutletType> GetOutletTypes(string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Access to OutletTypes denied.");

            using (var uow = new UnitOfWork())
            {
                var outletBL = new OutletTypeBL(uow);
                return outletBL.GetAll();
            }
        }

        /// <summary>
        /// Get a News Focus.
        /// </summary>
        /// <param name="newsFocusId">The Id of the News Focus</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The News Focus requested.</returns>
        public NewsFocus GetNewsFocus(int newsFocusId, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to NewsFocus with Id {0} denied.", newsFocusId));

            using (var uow = new UnitOfWork())
            {
                var focusBL = new NewsFocusBL(uow);
                var focus = focusBL.Get(newsFocusId);

                if (focus == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("NewsFocus with Id {0} not found.", newsFocusId));

                return focus;
            }
        }

        /// <summary>
        /// Get all News Focus.
        /// </summary>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A List of News Focus.</returns>
        public List<NewsFocus> GetNewsFocuses(string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Access to NewsFocuses denied.");

            using (var uow = new UnitOfWork())
            {
                var focusBL = new NewsFocusBL(uow);
                return focusBL.GetAll();
            }
        }

        /// <summary>
        /// Get a Outlet Frequency.
        /// </summary>
        /// <param name="frequencyId">The Id of the Outlet Frequency</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The Outlet Frequency requested.</returns>
        public OutletFrequency GetOutletFrequency(int frequencyId, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to OutletFrequency with Id {0} denied.", frequencyId));

            using (var uow = new UnitOfWork())
            {
                var freqBL = new OutletFrequencyBL(uow);
                var freq = freqBL.Get(frequencyId);

                if (freq == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("OutletFrequency with Id {0} not found.", frequencyId));

                return freq;
            }
        }

        /// <summary>
        /// Get all Outlet Frequencies.
        /// </summary>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A List of Outlet Frequencies.</returns>
        public List<OutletFrequency> GetOutletFrequencies(string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Access to OutletFrequencies denied.");

            using (var uow = new UnitOfWork())
            {
                var freqBL = new OutletFrequencyBL(uow);
                return freqBL.GetAll();
            }
        }

        /// <summary>
        /// Get a Product Type.
        /// </summary>
        /// <param name="productTypeId">The Id of the Product Type</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The Product Type requested.</returns>
        public ProductType GetProductType(int productTypeId, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to ProductType with Id {0} denied.", productTypeId));

            using (var uow = new UnitOfWork())
            {
                var productTypeBL = new ProductTypeBL(uow);
                var pt = productTypeBL.Get(productTypeId);

                if (pt == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("ProductType with Id {0} not found.", productTypeId));

                return pt;
            }
        }

        /// <summary>
        /// Get all Product Types.
        /// </summary>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A List of Product Types.</returns>
        public List<ProductType> GetProductTypes(string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Access to ProductTypes denied.");

            using (var uow = new UnitOfWork())
            {
                var productTypeBL = new ProductTypeBL(uow);
                return productTypeBL.GetAll();
            }
        }

        /// <summary>
        /// Get all Product Types for a specified Outlet Type.
        /// </summary>
        /// <param name="outletTypeId">The Outlet Type Id to use for finding product types.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A List of Product Types.</returns>
        public List<ProductType> GetProductTypesByOutletType(int outletTypeId, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to ProductType with OutletTypeId {0} denied.", outletTypeId));

            using (var uow = new UnitOfWork())
            {
                var productTypeBL = new ProductTypeBL(uow);
                return productTypeBL.GetByOutletType(outletTypeId);
            }
        }

        /// <summary>
        /// Get a Station Format.
        /// </summary>
        /// <param name="stationFormatId">The Id of the Station Format</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The Station Format requested.</returns>
        public StationFormat GetStationFormat(int stationFormatId, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to StationFormat with Id {0} denied.", stationFormatId));

            using (var uow = new UnitOfWork())
            {
                var statBL = new StationFormatBL(uow);
                var stat = statBL.Get(stationFormatId);

                if (stat == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("StationFormat with Id {0} not found.", stationFormatId));

                return stat;
            }
        }

        /// <summary>
        /// Get all Station Formats.
        /// </summary>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A List of Station Formats.</returns>
        public List<StationFormat> GetStationFormats(string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Access to StationFormats denied.");

            using (var uow = new UnitOfWork())
            {
                var statBL = new StationFormatBL(uow);
                return statBL.GetAll();
            }
        }

        /// <summary>
        /// Get a Subject.
        /// </summary>
        /// <param name="subjectId">The Id of the Subject</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The Subject requested.</returns>
        public Subject GetSubject(int subjectId, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to Subjject with Id {0} denied.", subjectId));

            using (var uow = new UnitOfWork())
            {
                var statBL = new SubjectBL(uow);
                var stat = statBL.Get(subjectId);

                if (stat == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Subject with Id {0} not found.", subjectId));

                return stat;
            }
        }

        /// <summary>
        /// Get all Subjects.
        /// </summary>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A List of Subjects.</returns>
        public List<Subject> GetSubjects(string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Access to Subject denied.");

            using (var uow = new UnitOfWork())
            {
                var statBL = new SubjectBL(uow);
                return statBL.GetAll();
            }
        }

        /// <summary>
        /// Get a Subject Group.
        /// </summary>
        /// <param name="subjectGroupId">The Id of the Subject Group</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The Station Format requested.</returns>
        public SubjectGroup GetSubjectGroup(int subjectGroupId, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to SubjectGroup with Id {0} denied.", subjectGroupId));

            using (var uow = new UnitOfWork())
            {
                var statBL = new SubjectGroupBL(uow);
                var stat = statBL.Get(subjectGroupId);

                if (stat == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("SubjectGroup with Id {0} not found.", subjectGroupId));

                return stat;
            }
        }

        /// <summary>
        /// Get all Subject Groups.
        /// </summary>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A List of Subject Groups.</returns>
        public List<SubjectGroup> GetSubjectGroups(string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Access to SubjectGroups denied.");

            using (var uow = new UnitOfWork())
            {
                var statBL = new SubjectGroupBL(uow);
                return statBL.GetAll();
            }
        }

        /// <summary>
        /// Get a Working Language.
        /// </summary>
        /// <param name="languageId">The Id of the Working Language</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The Working Language requested.</returns>
        public WorkingLanguage GetWorkingLanguage(int languageId, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to WorkingLanguage with Id {0} denied.", languageId));

            using (var uow = new UnitOfWork())
            {
                var workBL = new WorkingLanguageBL(uow);
                var work = workBL.Get(languageId);

                if (work == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("WorkingLanguage with Id {0} not found.", languageId));

                return work;
            }
        }

        /// <summary>
        /// Get all Working Language.
        /// </summary>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A List of Working Languages.</returns>
        public List<WorkingLanguage> GetWorkingLanguages(string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Access to WorkingLanguages denied.");

            using (var uow = new UnitOfWork())
            {
                var workBL = new WorkingLanguageBL(uow);
                return workBL.GetAll();
            }
        }

        #endregion

        #region "Notes"

        /// <summary>
        /// Get outlet notes
        /// </summary>
        /// <param name="outletId">The id of the outlet</param>
        /// <param name="recordType">The type of the outlet</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A list a notes</returns>
        public List<Note> GetOutletNotes(string outletId, string recordType, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to outlet notes with Id {0} denied.", outletId));

            using (var uow = new UnitOfWork())
            {
                var noteBL = new NoteBL(uow);
                List<Note> notes = noteBL.GetAll(outletId, null, recordType, session.DebtorNumber, session.UserId);

                return notes;
            }
        }

        /// <summary>
        /// Get contact notes
        /// </summary>
        /// <param name="outletId">The outletId of the contact</param>
        /// /// <param name="contactId">The id of the contact</param>
        /// <param name="recordType">The type of the contact</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A list a notes</returns>
        public List<Note> GetContactNotes(string outletId, string contactId, string recordType, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to contact notes with Id {0} denied.", contactId));

            using (var uow = new UnitOfWork())
            {
                var noteBL = new NoteBL(uow);
                List<Note> notes = noteBL.GetAll(outletId, contactId, recordType, session.DebtorNumber, session.UserId);

                return notes;
            }
        }

        public void DeleteNote(int id, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            using (var uow = new UnitOfWork())
            {
                var noteBL = new NoteBL(uow);
                var note = noteBL.Get(id);
                // Make sure they have access to it.
                if (!AccessHelper.IsAccessAllowedToContactsData(note.DebtorNumber, note.UserId, note.IsPrivate, session))
                    throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to delete note with Id {0} denied.", note.Id));

                noteBL.Delete(id);
            }
        }

        public void PinUnpinNote(int id, bool pinned, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            using (var uow = new UnitOfWork())
            {
                var noteBL = new NoteBL(uow);
                var note = noteBL.Get(id);
                // Make sure they have access to it.
                if (!AccessHelper.IsAccessAllowedToContactsData(note.DebtorNumber, note.UserId, note.IsPrivate, session))
                    throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to pin/unpin note with Id {0} denied.", note.Id));

                noteBL.PinUnpin(id, pinned);
            }
        }

        public int AddNote(Note note, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(note.DebtorNumber, note.UserId, note.IsPrivate, session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to add note with debtornumber {0} denied.", note.DebtorNumber));

            using (var uow = new UnitOfWork())
            {
                var noteBL = new NoteBL(uow);
                return noteBL.Add(note);
            }
        }

        public void UpdateNote(Note note, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            using (var uow = new UnitOfWork())
            {
                var noteBL = new NoteBL(uow);
                var dbNote = noteBL.Get(note.Id);
                // Make sure they have access to it.
                if (!AccessHelper.IsAccessAllowedToContactsData(dbNote.DebtorNumber, dbNote.UserId, dbNote.IsPrivate, session))
                    throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to update note with Id {0} denied.", note.Id));

                noteBL.Update(note);
            }
        }

        public Note GetNote(int id, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            using (var uow = new UnitOfWork())
            {
                var noteBL = new NoteBL(uow);
                var note = noteBL.Get(id);

                // Make sure they have access to it.
                if (!AccessHelper.IsAccessAllowedToContactsData(note.DebtorNumber, note.UserId, note.IsPrivate, session))
                    throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to get note with Id {0} denied.", note.Id));

                return note;
            }
        }

        public void AddNoteBulk(NotesBulk notes, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(notes.DebtorNumber, notes.UserId, notes.IsPrivate, session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to add note with debtornumber {0} denied.", notes.DebtorNumber));

            using (var uow = new UnitOfWork())
            {
                var noteBL = new NoteBL(uow);
                noteBL.AddBulk(notes);
            }
        }

        #endregion

        #region ----- Media Contacts Export functions -----

        /// <summary>
        /// Gets a ContactsExport item by it's Id.
        /// </summary>
        /// <param name="exportId">The Id of the ContactsExport item.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The ContactsExport item requested.</returns>
        public MediaContactsExport GetExport(int exportId, string sessionKey)
        {
            using (var uow = new UnitOfWork())
            {
                var session = AccessHelper.GetSessionFromKey(sessionKey);
                var exportBL = new MediaContactsExportBL(uow);
                var export = exportBL.Get(exportId);

                if (export == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("MediaContactsExport with Id {0} not found.", exportId));

                // Make sure they have access to it.
                if (!AccessHelper.IsAccessAllowedToContactsData(export.DebtorNumber, export.UserId, export.IsPrivate, session))
                    throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to view Contacts Export for DebtorNumber {0} denied.", export.DebtorNumber));

                return export;
            }
        }

        /// <summary>
        /// Get a List of ContactsExport items by the DebtorNumber that a User is allowed to see.
        /// </summary>
        /// <param name="debtorNumber">The DebtorNumber of the ContactsListExport item to get.</param>
        /// <param name="userId">The User of the ContactsListExport item to get. Set to null to get all for a DebtorNumber.</param>
        /// <param name="type">The Type of Export, e.g. Lists or Tasks.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The ContactsExport items by the DebtorNumber accessible by the User.</returns>
        public List<MediaContactsExport> GetExports(string debtorNumber, int? userId, MediaContactsExportType type, string sessionKey)
        {
            using (var uow = new UnitOfWork())
            {
                var session = AccessHelper.GetSessionFromKey(sessionKey);
                var exportBL = new MediaContactsExportBL(uow);

                // Make sure they have access to it.
                if (!AccessHelper.IsAccessAllowedToContactsData(debtorNumber, userId, true, session))
                    throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to view Contacts Export for DebtorNumber {0} denied.", debtorNumber));

                if (userId.HasValue)
                    return exportBL.GetAllByDebtorNumberUserIdType(debtorNumber, userId.Value, type);
                else
                    return exportBL.GetAllByDebtorNumberType(debtorNumber, type);
            }
        }


        /// <summary>
        /// Add a new MediaContactsExport record to the database.
        /// </summary>
        /// <param name="export">The MediaContactsExport item to add to the database.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The Id of the new MediaContactsExport item.</returns>
        public int AddExports(MediaContactsExport export, string sessionKey)
        {
            using (var uow = new UnitOfWork())
            {
                var session = AccessHelper.GetSessionFromKey(sessionKey);
                var exportBL = new MediaContactsExportBL(uow);

                // Make sure they have access to it.
                if (!AccessHelper.IsAccessAllowedToContactsData(export.DebtorNumber, export.UserId, export.IsPrivate, session))
                    throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to add Contacts Export for DebtorNumber {0} denied.", export.DebtorNumber));

                return exportBL.Add(export, session.UserId);
            }
        }

        /// <summary>
        /// Update an existing MediaContactsExport record in the database.
        /// </summary>
        /// <param name="export">The MediaContactsExport item to update in the database.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        public void UpdateExports(MediaContactsExport export, string sessionKey)
        {
            using (var uow = new UnitOfWork())
            {
                var session = AccessHelper.GetSessionFromKey(sessionKey);
                var exportBL = new MediaContactsExportBL(uow);

                // Make sure they have access to it.
                if (!AccessHelper.IsAccessAllowedToContactsData(export.DebtorNumber, export.UserId, export.IsPrivate, session))
                    throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to update Contacts Export for DebtorNumber {0} denied.", export.DebtorNumber));

                exportBL.Update(export, session.UserId);
            }
        }

        /// <summary>
        /// Delete an existing MediaContactsExport record in the database.
        /// </summary>
        /// <param name="exportId">The Id of the MediaContactsExport record to delete.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        public void DeleteExports(int exportId, string sessionKey)
        {
            using (var uow = new UnitOfWork())
            {
                var session = AccessHelper.GetSessionFromKey(sessionKey);
                var exportBL = new MediaContactsExportBL(uow);
                var export = exportBL.Get(exportId);

                // Make sure they have access to it.
                if (!AccessHelper.IsAccessAllowedToContactsData(export.DebtorNumber, export.UserId, export.IsPrivate, session))
                    throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to delete Contacts Export for DebtorNumber {0} denied.", export.DebtorNumber));

                exportBL.Delete(exportId, session.UserId);
            }
        }

        #endregion

        #region ----- Media Database Log Get/Add/Update methods -----

        public int AddMediaDatabaseLog(MediaDatabaseLog entity, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdmin(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Adding of MediaDatabaseLog data denied.");

            using (var uow = new UnitOfWork())
            {
                var logBL = new MediaDatabaseLogBL(uow);
                return logBL.Add(entity);
            }
        }

        public void UpdateMediaDatabaseLog(MediaDatabaseLog entity, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdmin(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Updating of MediaDatabaseLog data denied.");

            using (var uow = new UnitOfWork())
            {
                var logBL = new MediaDatabaseLogBL(uow);
                logBL.Update(entity);
            }
        }

        public List<MediaDatabaseLog> GetMediaDatabaseLogs(string outletId, string contactId, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Access to MediaDatabaseLogs denied.");

            using (var uow = new UnitOfWork())
            {
                var logBL = new MediaDatabaseLogBL(uow);
                List<MediaDatabaseLog> log = logBL.GetMediaDatabaseLog(outletId, contactId);

                return log;
            }
        }

        public MediaDatabaseLog GetMediaDatabaseLogById(int id, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Access to MediaDatabaseLogs denied.");

            using (var uow = new UnitOfWork())
            {
                var logBL = new MediaDatabaseLogBL(uow);
                var log = logBL.GetMediaDatabaseLogById(id);

                if (log == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), "MediaDatabaseLog not found.");

                return log;
            }
        }

        #endregion

        #region Recently Viewed
        /// <summary>
        /// get recently viewed to show in admin website
        /// </summary>
        /// <param name="sessionKey"></param>

        /// <returns></returns>
        public List<MediaRecentlyViewedDto> GetMediaRecentlyViewed(string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Access to MediaDatabaseLog denied.");

            using (var uow = new UnitOfWork())
            {
                var logBl = new MediaDatabaseLogBL(uow);
                List<MediaRecentlyViewedDto> recentlyViewed = logBl.GetMediaRecentlyViewed(session.UserId);

                return recentlyViewed;
            }
        }
        #endregion

        #region Media Contacts Groups

        /// <summary>
        /// Add a new MediaContactGroup record to the database.
        /// </summary>
        /// <param name="entity">The MediaContactGroup item to add to the database.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The Id of the new MediaContactGroup item.</returns>
        public int AddMediaContactGroup(MediaContactGroup entity, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdminOrSameUser(entity.CreatedByUserId, session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to MediaContactGroups for UserId {0} denied.", entity.CreatedByUserId));

            using (var uow = new UnitOfWork())
            {
                var groupBL = new MediaContactGroupBL(uow);
                return groupBL.Add(entity, session.UserId);
            }
        }

        /// <summary>
        /// Get all MediaContactGroup records from the databsae that this User is able to see
        /// </summary>
        /// <param name="userId">The Id of the User to fetch Groups for.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A list of MediaContactGroup records accessible by the User.</returns>
        public List<MediaContactGroup> GetMediaContactGroups(int userId, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdminOrSameUser(userId, session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to MediaContactGroups for UserId {0} denied.", userId));

            using (var uow = new UnitOfWork())
            {
                var groupBL = new MediaContactGroupBL(uow);
                var groups = groupBL.GetAll(userId);

                return groups;
            }
        }

        /// <summary>
        /// Get all MediaContactGroup records from the databsae for a type (saved search/list/task) that this User is able to see
        /// </summary>
        /// <param name="type">The type of Groups to return.</param>
        /// <param name="userId">The Id of the User to fetch Groups for.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A list of MediaContactGroup records accessible by the User.</returns>
        public List<MediaContactGroup> GetMediaContactGroupsByType(MediaContactGroupType type, int userId, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdminOrSameUser(userId, session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to MediaContactGroups for UserId {0} denied.", userId));

            using (var uow = new UnitOfWork())
            {
                var groupBL = new MediaContactGroupBL(uow);
                var groups = groupBL.GetAllByType(type, userId);

                return groups;
            }
        }

        #endregion

        #region Media Contact Saved Searches

        /// <summary>
        /// Get all MediaContactSavedSearch records from the databsae that this User is able to see
        /// </summary>
        /// <param name="userId">The Id of the User to fetch Saved Searches for.</param>
        /// <param name="recentCount">If greater than 0 it returns only the amount requested, sorted by modifed date</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A list of MediaContactSavedSearch records accessible by the User.</returns>
        public List<MediaContactSavedSearch> GetMediaContactSavedSearches(int userId, int recentCount, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdminOrSameUser(userId, session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to MediaContactSavedSearches for UserId {0} denied.", userId));

            using (var uow = new UnitOfWork())
            {
                var searchBL = new MediaContactSavedSearchBL(uow);
                List<MediaContactSavedSearch> searches;

                if (recentCount > 0)
                    searches = searchBL.GetRecent(userId, recentCount);
                else
                    searches = searchBL.GetAll(userId);

                return searches;
            }
        }

        /// <summary>
        /// Get a MediaContactSavedSearch record from the database
        /// </summary>
        /// <param name="searchId">The Id of the SavedSearch to fetch.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A MediaContactSavedSearch record.</returns>
        public MediaContactSavedSearch GetMediaContactSavedSearch(int searchId, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            using (var uow = new UnitOfWork())
            {
                var searchBL = new MediaContactSavedSearchBL(uow);
                var search = searchBL.Get(searchId);

                if (search == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("MediaContactSavedSearch with Id {0} not found.", searchId));

                // Make sure they have access to it.
                if (!AccessHelper.IsAccessAllowedToContactsData(search.OwnerUser.DebtorNumber, search.OwnerUserId, search.IsPrivate, session))
                    throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to MediaContactSavedSearch with id {0} for UserId {1} denied.", searchId, session.UserId));

                return search;
            }
        }

        #endregion

        #region Media Contact List

        /// <summary>
        /// Get all MediaContactList records from the databsae that this User is able to see
        /// </summary>
        /// <param name="userId">The Id of the User to fetch Lists for.</param>
        /// <param name="recentCount">If greater than 0 it returns only the amount requested, sorted by modifed date</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A list of MediaContactList records accessible by the User.</returns>
        public List<MediaContactList> GetMediaContactLists(int userId, int recentCount, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdminOrSameUser(userId, session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to MediaContactLists for UserId {0} denied.", userId));

            using (var uow = new UnitOfWork())
            {
                var listBL = new MediaContactListBL(uow);
                List<MediaContactList> lists;

                if (recentCount > 0)
                    lists = listBL.GetRecent(userId, recentCount);
                else
                    lists = listBL.GetAll(userId);

                return lists;
            }
        }

        /// <summary>
        /// Get a MediaContactList record from the database
        /// </summary>
        /// <param name="listId">The Id of the List to fetch.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A MediaContactList record.</returns>
        public MediaContactList GetMediaContactList(int listId, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            using (var uow = new UnitOfWork())
            {
                var listBL = new MediaContactListBL(uow);
                var list = listBL.Get(listId);

                if (list == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("MediaContactList with Id {0} not found.", listId));

                // Make sure they have access to it.
                if (!AccessHelper.IsAccessAllowedToContactsData(list.OwnerUser.DebtorNumber, list.OwnerUserId, list.IsPrivate, session))
                    throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to MediaContactList with id {0} for UserId {1} denied.", listId, session.UserId));

                return list;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">The id of the list to fetch the list versions</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A list of MediaContactListVersion records</returns>
        public List<MediaContactListVersion> GetMediaContactListVersions(int id, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to GetMediaContactListVersions for UserId {0} denied.", session.UserId));

            using (var uow = new UnitOfWork())
            {
                var listVersionBL = new MediaContactListVersionBL(uow);
                var listVersions = listVersionBL.GetAllByListId(id);

                return listVersions;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listId"></param>
        /// <param name="listSetId"></param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        public void RestoreMediaContactListVersion(int listId, int listSetId, int userId, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to RestoreMediaContactListVersion for UserId {0} denied.", session.UserId));

            using (var uow = new UnitOfWork())
            {
                var listVersionBL = new MediaContactListVersionBL(uow);
                listVersionBL.Restore(listId, listSetId, userId);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <param name="userId">The Id of the User to fetch list contact replacements.</param>
        /// <param name="listId"></param>
        /// <param name="listSetId"></param>
        /// <param name="maxRows"></param>
        /// <returns></returns>
        public List<MediaContactLivingListContactReplacement> GetMediaContactLivingListContactReplacement(string sessionKey, int userId = 0, int listId = 0, int maxRows = 6)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to GetMediaContactLivingListContactReplacement for UserId {0} denied.", session.UserId));

            using (var uow = new UnitOfWork())
            {
                var listBL = new MediaContactLivingListBL(uow);
                var listReplacement = listBL.GetContactReplacements(userId, listId, maxRows);
                return listReplacement;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="recordId"></param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        public void LivingListAccept(int recordId, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to LivingListAccept for UserId {0} denied.", session.UserId));

            using (var uow = new UnitOfWork())
            {
                var livingListBl = new MediaContactLivingListBL(uow);
                livingListBl.AcceptReplacement(recordId, session.UserId);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="recordId"></param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        public void LivingListReject(int recordId, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to LivingListReject for UserId {0} denied.", session.UserId));

            using (var uow = new UnitOfWork())
            {
                var livingListBl = new MediaContactLivingListBL(uow);
                livingListBl.RejectReplacement(recordId, session.UserId);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listId">Id of the list that all its living list items will be accepted/rejected</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        public void LivingListAcceptRejectAll(int listId, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to LivingListAcceptRejectAll for UserId {0} denied.", session.UserId));

            using (var uow = new UnitOfWork())
            {
                var livingListBl = new MediaContactLivingListBL(uow);
                livingListBl.AcceptRejectAll(listId, session.UserId);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="recordId"></param>
        /// <param name="maxRows"></param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns></returns>
        public Dictionary<string, string> GetLivingListOutletContactsByRecordId(int recordId, int maxRows, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to GetLivingListOutletContactsByRecordId for UserId {0} denied.", session.UserId));

            using (var uow = new UnitOfWork())
            {
                var livingListBl = new MediaContactLivingListBL(uow);
                return livingListBl.GetOutletContactsByRecordId(recordId, maxRows);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="recordId"></param>
        /// <param name="newContactId"></param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        public void LivingListReplaceContact(int recordId, string newContactId, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to LivingListReplaceContact for UserId {0} denied.", session.UserId));

            using (var uow = new UnitOfWork())
            {
                var livingListBl = new MediaContactLivingListBL(uow);
                livingListBl.ReplaceContact(recordId, newContactId, session.UserId);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listId"></param>
        /// <param name="recordIds"></param>
        /// <param name="sessionKey"></param>
        public void DeleteFromList(int listId, List<int> recordIds, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to DeleteFromList for UserId {0} denied.", session.UserId));

            using (var uow = new UnitOfWork())
            {
                var listBL = new MediaContactListRecordBL(uow);
                listBL.DeleteFromList(listId, recordIds, session.UserId);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listId"></param>
        /// <param name="userId"></param>
        /// <param name="recordIds"></param>
        /// <param name="createNewVersion"></param>
        /// <param name="sessionKey"></param>
        public void AddToList(int listId, int userId, List<string> recordIds, bool createNewVersion, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to AddToList for UserId {0} denied.", session.UserId));

            using (var uow = new UnitOfWork())
            {
                var listBL = new MediaContactListRecordBL(uow);
                listBL.AddToList(listId, userId, recordIds, createNewVersion);
            }
        }

        public MediaContactPaginatedListRecordDetails GetPaginatedRecordsByListsetAndListId(int listId, int listSet, int pageNumber,
                                                                                  int recordsPerPage,
                                                                                  SortColumn pSortColumn,
                                                                                  SortDirection pSortDirection,
                                                                                  int userId, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to GetPaginatedRecordsByListsetAndListId for UserId {0} denied.", userId));

            using (var uow = new UnitOfWork())
            {
                var mediaContactsGroupRecordLstBl = new MediaContactListRecordBL(uow);
                var records = mediaContactsGroupRecordLstBl.GetPaginatedRecordsByListsetAndListId(listId, listSet, pageNumber,
                                                                                                  recordsPerPage,
                                                                                                  pSortColumn,
                                                                                                  pSortDirection, session.UserId);
                return records;
            }
        }

        #endregion

        #region Media Contact Task

        /// <summary>
        /// Get all MediaContactTask records from the databsae that this User is able to see
        /// </summary>
        /// <param name="userId">The Id of the User to fetch Tasks for.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A list of MediaContactTask records accessible by the User.</returns>
        public List<MediaContactTask> GetMediaContactTasks(int userId, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdminOrSameUser(userId, session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to MediaContactTasks for UserId {0} denied.", userId));

            using (var uow = new UnitOfWork())
            {
                var taskBL = new MediaContactTaskBL(uow);
                var tasks = taskBL.GetAll(userId);

                return tasks;
            }
        }

        /// <summary>
        /// Get a MediaContactTask record from the database
        /// </summary>
        /// <param name="taskId">The Id of the Task to fetch.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A MediaContactTask record.</returns>
        public MediaContactTask GetMediaContactTask(int taskId, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            using (var uow = new UnitOfWork())
            {
                var taskBL = new MediaContactTaskBL(uow);
                var task = taskBL.Get(taskId);

                if (task == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("MediaContactTask with Id {0} not found.", taskId));

                // Make sure they have access to it. Check against the OwnerUserId and CreatedByUserId as both have access.
                if (!AccessHelper.IsAccessAllowedToContactsData(task.OwnerUser.DebtorNumber, task.OwnerUserId, task.IsPrivate, session))
                {
                    if (!AccessHelper.IsAccessAllowedToContactsData(task.OwnerUser.DebtorNumber, task.CreatedByUserId, task.IsPrivate, session))
                    {
                        throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to MediaContactTask with id {0} for UserId {1} denied.", taskId, session.UserId));
                    }
                }

                return task;
            }
        }

        #endregion

        #region Media Movement fetch/add/update methods

        public List<MediaMovement> GetMediaMovements(bool includeHidden, int pageNumber, int recordsPerPage, string sessionKey, out int totalCount, out int totalPinned)
        {
            // Make sure it's a valid session
            AccessHelper.GetSessionFromKey(sessionKey);

            using (var uow = new UnitOfWork())
            {
                var movementBL = new MediaMovementBL(uow);

                return movementBL.GetAll(includeHidden, pageNumber, recordsPerPage, out totalCount, out totalPinned);
            }
        }

        public MediaMovement GetMediaMovement(int id, string sessionKey)
        {
            // Make sure it's a valid session
            AccessHelper.GetSessionFromKey(sessionKey);

            using (var uow = new UnitOfWork())
            {
                var movementBL = new MediaMovementBL(uow);

                return movementBL.Get(id);
            }
        }

        /// <summary>
        /// get the list of outlet's media movements including any of its contacts movements
        /// </summary>
        /// <param name="outletId"></param>
        /// <param name="pageNumber"></param>
        /// <param name="recordsPerPage"></param>
        /// <param name="sessionKey"></param>
        /// <returns></returns>
        public List<MediaMovement> GetOutletMediaMovements(string outletId, int pageNumber, int recordsPerPage, string sessionKey)
        {
            // Make sure it's a valid session
            AccessHelper.GetSessionFromKey(sessionKey);

            using (var uow = new UnitOfWork())
            {
                var movementBl = new MediaMovementBL(uow);

                return movementBl.GetByOutletId(outletId, pageNumber, recordsPerPage);
            }
        }

        /// <summary>
        /// get the list of contact's media movements
        /// </summary>
        /// <param name="contactId"></param>
        /// <param name="pageNumber"></param>
        /// <param name="recordsPerPage"></param>
        /// <param name="sessionKey"></param>
        /// <returns></returns>
        public List<MediaMovement> GetContactMediaMovements(string contactId, int pageNumber, int recordsPerPage, string sessionKey)
        {
            // Make sure it's a valid session
            AccessHelper.GetSessionFromKey(sessionKey);

            using (var uow = new UnitOfWork())
            {
                var movementBl = new MediaMovementBL(uow);

                return movementBl.GetByContactId(contactId, pageNumber, recordsPerPage);
            }
        }
        public bool AddMediaMovement(MediaMovement newMovement, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdmin(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Adding a new Media Movement data entry denied.");

            using (var uow = new UnitOfWork())
            {
                var movementBL = new MediaMovementBL(uow);

                var added = movementBL.Add(newMovement, session.UserId);

                return added;
            }
        }

        public bool UpdateMediaMovement(MediaMovement updatedMediaMovement, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdmin(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Updating Media Movement data entry denied.");

            using (var uow = new UnitOfWork())
            {
                var movementBL = new MediaMovementBL(uow);

                var updated = movementBL.Update(updatedMediaMovement, session.UserId);

                return updated;
            }
        }

        public bool DeleteMediaMovement(int entityId, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdmin(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Deleting Media Movement data entry denied.");

            using (var uow = new UnitOfWork())
            {
                var movementBL = new MediaMovementBL(uow);

                return movementBL.Delete(entityId);

            }
        }

        #endregion

        #region Priority Notice fetch/add/update methods

        public List<MediaPriorityNotice> GetMediaPriorityNotices(int pageNumber, int recordsPerPage, string sessionKey, out int totalCount)
        {
            // Make sure it's a valid session
            AccessHelper.GetSessionFromKey(sessionKey);

            using (var uow = new UnitOfWork())
            {
                var noticetBL = new MediaPriorityNoticeBL(uow);

                return noticetBL.GetAll(pageNumber, recordsPerPage, out totalCount);
            }
        }

        public MediaPriorityNotice GetMediaPriorityNotice(int? id, string contactId, string outletId, string sessionKey)
        {
            // Make sure it's a valid session
            AccessHelper.GetSessionFromKey(sessionKey);

            using (var uow = new UnitOfWork())
            {
                var noticeBL = new MediaPriorityNoticeBL(uow);

                if (id.HasValue)
                    return noticeBL.Get(id.Value);

                if (string.IsNullOrWhiteSpace(contactId))
                    return noticeBL.GetByOutletId(outletId);

                return noticeBL.GetByContactId(contactId);
            }
        }

        public bool AddMediaPriorityNotice(MediaPriorityNotice entity, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdmin(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Adding a new Priority Notice data entry denied.");

            using (var uow = new UnitOfWork())
            {
                var noticeBL = new MediaPriorityNoticeBL(uow);

                var added = noticeBL.Add(entity, session.UserId);

                return added;
            }
        }

        public bool UpdateMediaPriorityNotice(MediaPriorityNotice entity, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdmin(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Updating Priority Notice data entry denied.");

            using (var uow = new UnitOfWork())
            {
                var noticeBL = new MediaPriorityNoticeBL(uow);

                var updated = noticeBL.Update(entity, session.UserId);

                return updated;
            }
        }

        public bool DeleteMediaPriorityNotice(int entityId, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdmin(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Deleting Priority Notice data entry denied.");

            using (var uow = new UnitOfWork())
            {
                var noticeBL = new MediaPriorityNoticeBL(uow);

                return noticeBL.Delete(entityId);

            }
        }

        public void PinUnpinMediaMovement(int id, bool pinned, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdmin(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), $"{(pinned ? "Unpinning": "Pinning")} Media Movement denied.");

            using (var uow = new UnitOfWork())
            {
                var mvBl = new MediaMovementBL(uow);            
               
                mvBl.PinUnpin(id, pinned);
            }
        }

        #endregion

        #region "Log Search"

        public int MediaContactSearchLog(MediaContactSearchLog entity, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Logging Search denied.");

            using (var uow = new UnitOfWork())
            {
                var logBL = new MediaContactSearchLogBL(uow);
                return logBL.Add(entity);
            }
        }

        public List<MediaContactSearchLog> GetRecentSearchList(int maxRecords, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Logging Search denied.");

            using (var uow = new UnitOfWork())
            {
                var logBL = new MediaContactSearchLogBL(uow);
                return logBL.GetRecentSearchList(session.UserId, maxRecords);
            }
        }

        public MediaContactSearchLog GetRecentSearchById(int searchLogId, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Logging Search denied.");

            using (var uow = new UnitOfWork())
            {
                var logBL = new MediaContactSearchLogBL(uow);
                return logBL.GetRecentSearchById(searchLogId, session.UserId);
            }
        }

        #endregion

        #region "Log Export"
        public int MediaContactExportLog(MediaContactExportLog entity, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Logging Search denied.");

            using (var uow = new UnitOfWork())
            {
                var logBL = new MediaContactExportLogBL(uow);
                return logBL.Add(entity);
            }
        }

        #endregion

        #region "Log Contacts Viewed"

        public void LogMediaContactView(MediaContactViewDto entity, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Logging Media Contact denied.");

            using (var uow = new UnitOfWork())
            {
                var logBl = new MediaContactViewBL(uow);
                logBl.Add(entity);
            }
        }

        public void LogPrnContactView(PrnContactViewDto entity, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Logging Prn Contact denied.");

            using (var uow = new UnitOfWork())
            {
                var logBl = new PrnContactViewBL(uow);
                logBl.Add(entity);
            }
        }
        public List<MediaContactAlsoViewedDto> GetContactViewSummary(string contactId, string outletId, List<int> subjectCodeIds, List<int> continentIds, int maxCount, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Access Denied to View contacts Summary.");

            var prnContactsAlsoViewedList = new List<MediaContactAlsoViewedDto>();
            var mediaContactsAlsoViewedList = new List<MediaContactAlsoViewedDto>();

            using (var uow = new UnitOfWork())
            {
                var mediabl = new MediaContactViewBL(uow);
                mediaContactsAlsoViewedList = mediabl.GetMediaContactViewSummary(contactId, outletId, subjectCodeIds, maxCount);

                //If user has access to australasia only, don't fetch PRN data
                if (continentIds.Any(c => c != Constants.CONTINENT_AUSTRALASIA))
                {
                    var prnbl = new PrnContactViewBL(uow);
                    prnContactsAlsoViewedList = prnbl.GetPrnContactViewSummary(contactId,
                        outletId, subjectCodeIds, continentIds, maxCount);
                }
            }

            mediaContactsAlsoViewedList.AddRange(prnContactsAlsoViewedList);
            return mediaContactsAlsoViewedList.OrderByDescending(l => l.ViewCount).Take(maxCount).ToList();
        }
        #endregion

        #region "Log Outlets Viewed"

        public void LogMediaOutletView(MediaOutletViewDto entity, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Logging Media Outlet denied.");

            using (var uow = new UnitOfWork())
            {
                var logBl = new MediaOutletViewBL(uow);
                logBl.Add(entity);
            }
        }

        #endregion

        #region Documents

        public OmaDocument GetDocumentById(int documentId, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Access Denied to View contacts.");

            using (var uow = new UnitOfWork())
            {
                var bl = new OmaDocumentBL(uow);
                return bl.GetById(documentId, session.UserId, session.DebtorNumber);
            }
        }

        public OmaDocument GetDocumentFile(int documentId, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Access Denied to View contacts.");

            using (var uow = new UnitOfWork())
            {
                var bl = new OmaDocumentBL(uow);
                return bl.GetDocumentFile(documentId, session.UserId, session.DebtorNumber);
            }
        }

        public List<OmaDocument> GetDocuments(string outletId, string contactId, int recordType, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Access Denied to View contacts.");

            using (var uow = new UnitOfWork())
            {
                var bl = new OmaDocumentBL(uow);

                return bl.GetList(outletId, contactId, (RecordType)Enum.Parse(typeof(RecordType), recordType.ToString()), session.UserId, session.DebtorNumber);
            }
        }

        public void SaveOmaDocument(OmaDocument entity, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Access Denied to View contacts.");

            using (var uow = new UnitOfWork())
            {
                var bl = new OmaDocumentBL(uow);
                bl.Save(entity, session.UserId, session.DebtorNumber);
            }
        }

        public void DeleteOmaDocument(int documentId, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Access Denied to View contacts.");

            using (var uow = new UnitOfWork())
            {
                var bl = new OmaDocumentBL(uow);
                bl.Delete(documentId, session.UserId, session.DebtorNumber);
            }
        }

        #endregion

        #region Private records

        public List<int> GetOmaContactIds(int outletId, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Access Denied to View contacts.");

            using (var uow = new UnitOfWork())
            {
                return new OmaContactBL(uow).GetContactIds(outletId);
            }
        }

        #endregion

        #region Log removing outlets/contacts from a service

        public List<ServiceDeletedUserLog> GetServiceDeletedUserLog(string outletId, string contactId, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Get ServiceDeletedContactLog denied.");

            using (var uow = new UnitOfWork())
            {
                return new ServiceDeletedUserLogBL(uow).GetList(outletId, contactId);
            }
        }

        #endregion

        #region Draft

        public void ApproveMediaContactDraft(int draftId, MediaDraftQueue draftQueue, MediaContact contact, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdmin(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Approving of MediaContactDraft data denied.");

            using (var uow = new UnitOfWork())
            {
                try
                {
                    new MediaContactDraftBL(uow).ApproveOrDecline(
                        draftId,
                        draftQueue,
                        DataLayer.Common.DraftQueueStatus.Approved,
                        contact,
                        session.UserId);
                }
                catch (SubjectPrimaryContactRequiredException ex)
                {
                    throw new FaultException<SubjectPrimaryContactRequiredFault>(new SubjectPrimaryContactRequiredFault(ex.OutletId, ex.SubjectId, ex.Message), ex.Message);
                }
                catch (UpdateException updateEx)
                {
                    throw new FaultException<MediaUpdateValidationFault>(new MediaUpdateValidationFault(updateEx.Message, updateEx.ReasonList), updateEx.Message);
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw new FaultException<DataConcurrencyFault>(new DataConcurrencyFault(), DataConcurrencyFault.FaultDescription);
                }
            }
        }

        public void ApproveMediaEmployeeDraft(int draftId, MediaDraftQueue draftQueue, MediaEmployee employee, MediaEmployeeValidation validation, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdmin(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Approving of MediaEmployeeDraft data denied.");

            using (var uow = new UnitOfWork())
            {
                try
                {
                    new MediaEmployeeDraftBL(uow).ApproveOrDecline(
                        draftId,
                        draftQueue,
                        DataLayer.Common.DraftQueueStatus.Approved,
                        employee,
                        validation,
                        session.UserId);
                }
                catch (MultiplePrimaryContactsException ex)
                {
                    throw new FaultException<MediaUpdateValidationFault>(new MediaUpdateValidationFault(ex.OutletId, UpdateErrorType.OutletMultiplePrimaryContactsExist, ex.Message), ex.Message);
                }
                catch (OutletPrimaryContactRequiredException ex)
                {
                    throw new FaultException<MediaUpdateValidationFault>(new MediaUpdateValidationFault(ex.OutletId, UpdateErrorType.OutletPrimaryContactRequired, ex.Message), ex.Message);
                }
                catch (SubjectPrimaryContactRequiredException ex)
                {
                    throw new FaultException<SubjectPrimaryContactRequiredFault>(new SubjectPrimaryContactRequiredFault(ex.OutletId, ex.SubjectId, ex.Message), ex.Message);
                }
                catch (UpdateException updateEx)
                {
                    throw new FaultException<MediaUpdateValidationFault>(new MediaUpdateValidationFault(updateEx.Message, updateEx.ReasonList), updateEx.Message);
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw new FaultException<DataConcurrencyFault>(new DataConcurrencyFault(), DataConcurrencyFault.FaultDescription);
                }
            }
        }

        #endregion

        #region Media Contact Job History

        public List<MediaContactJobHistory> GetMediaContactJobHistories(string contactId, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), $"Access to MediaContact with Id {contactId} denied.");

            using (var uow = new UnitOfWork())
            {
                var contactJobHistoryBL = new MediaContactJobHistoryBL(uow);
                var list = contactJobHistoryBL.GetAll(contactId);

                return list;
            }
        }
        public MediaContactJobHistory GetMediaContactJobHistory(string contactId, int jobHistoryId, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), $"Access to MediaContact with Id {contactId} denied.");

            using (var uow = new UnitOfWork())
            {
                var contactJobHistoryBL = new MediaContactJobHistoryBL(uow);
                var entity = contactJobHistoryBL.GetById(jobHistoryId);

                return entity;
            }
        }

        public int AddMediaContactJobHistory(MediaContactJobHistory entity, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdmin(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Adding of MediaContactJobHistory data denied.");

            using (var uow = new UnitOfWork())
            {
                var contactJobHistoryBL = new MediaContactJobHistoryBL(uow);

                try
                {
                    return contactJobHistoryBL.Add(entity, session.UserId);
                }
                catch (UpdateException updateEx)
                {
                    throw new FaultException<MediaUpdateValidationFault>(new MediaUpdateValidationFault(updateEx.Message, updateEx.ReasonList), updateEx.Message);
                }
            }
        }

        public void UpdateMediaContactJobHistory(MediaContactJobHistory entity, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdmin(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Updating of MediaContactJobHistory data denied.");

            using (var uow = new UnitOfWork())
            {
                try
                {
                    new MediaContactJobHistoryBL(uow).Update(entity, session.UserId);
                }
                catch (UpdateException updateEx)
                {
                    throw new FaultException<MediaUpdateValidationFault>(new MediaUpdateValidationFault(updateEx.Message, updateEx.ReasonList), updateEx.Message);
                }
            }
        }

        public void UpdateMediaContactJobHistories(List<MediaContactJobHistory> entities, string contactId, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdmin(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Updating of MediaContactJobHistory data denied.");

            using (var uow = new UnitOfWork())
            {
                try
                {
                    new MediaContactJobHistoryBL(uow).UpdateBulk(entities, contactId, session.UserId);
                }
                catch (UpdateException updateEx)
                {
                    throw new FaultException<MediaUpdateValidationFault>(new MediaUpdateValidationFault(updateEx.Message, updateEx.ReasonList), updateEx.Message);
                }
            }
        }

        public void DeleteMediaContactJobHistory(int jobHistoryId, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdmin(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Updating of MediaContactJobHistory data denied.");

            using (var uow = new UnitOfWork())
            {
                try
                {
                    new MediaContactJobHistoryBL(uow).Delete(jobHistoryId, session.UserId);
                }
                catch (UpdateException updateEx)
                {
                    throw new FaultException<MediaUpdateValidationFault>(new MediaUpdateValidationFault(updateEx.Message, updateEx.ReasonList), updateEx.Message);
                }
            }
        }

        #endregion

        #region Media Contact Recent Changes

        public List<MediaContactRecentChange> GetMediaContactRecentlyAdded(int total, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), $"Access to recently updated media contacts denied.");

            using (var uow = new UnitOfWork())
            {
                var contactRecentChangesBL = new MediaContactRecentChangesBL(uow);
                var list = contactRecentChangesBL.GetMediaContactRecentlyAdded(total);

                return list;
            }
        }

        public List<MediaContactRecentChange> GetMediaContactRecentlyUpdated(int total, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), $"Access to recently updated media contacts denied.");

            using (var uow = new UnitOfWork())
            {
                var contactRecentChangesBL = new MediaContactRecentChangesBL(uow);
                var list = contactRecentChangesBL.GetMediaContactRecentlyUpdated(total);

                return list;
            }
        }

        #endregion
    }
}
