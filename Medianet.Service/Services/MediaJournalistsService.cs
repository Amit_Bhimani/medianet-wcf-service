﻿using System;
using System.Collections.Generic;
using System.Linq;
using Medianet.Wcf;
using Medianet.Service.Dto;
using Medianet.Service.BusinessLayer;
using Medianet.DataLayer;
using System.ServiceModel;
using Medianet.Service.Common;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using Medianet.Service.FaultExceptions;
using Medianet.Service.BusinessLayer.Exceptions;
using System.Configuration;
using System.Web;

namespace Medianet.Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [ErrorBehaviorAttribute(typeof(ServiceErrorHandler))]
    public class MediaJournalistsService : Contracts.IMediaJournalistsService
    {
        public MediaJournalistsService()
        {
            // The windows service host normally does this but we do it here when running in debug mode.
            if (!AutoMapperConfigurator.IsInitialized)
                AutoMapperConfigurator.Configure();
        }

        public List<MnjProfile> GetMNJUserProfiles(int userId)
        {
            using (var uow = new UnitOfWork())
            {
                return new MnjProfileBL(uow).GetByUserId(userId);
            }
        }

        public MnjProfile GetMNJUserProfile(int profileId, int userId)
        {
            using (var uow = new UnitOfWork())
            {
                var entity = new MnjProfileBL(uow).GetById(profileId);

                if (entity == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), $"Press Release Alert with Id {profileId} not found.");

                if (entity.UserID != userId)
                    throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), $"Access to Press Release Alert with Id {profileId} denied.");

                return entity;
            }
        }

        public void DeleteProfile(int profileId, int userId)
        {
            using (var uow = new UnitOfWork())
            {
                new MnjProfileBL(uow).DeleteProfile(profileId, userId);
            }
        }

        public int AddProfile(MnjProfile entity)
        {
            using (var uow = new UnitOfWork())
            {
                try
                {
                    return new MnjProfileBL(uow).AddProfile(entity);
                }
                catch (DbUpdateException updateEx)
                {
                    SqlException sqlException = updateEx.InnerException.InnerException as SqlException;
                    if (sqlException != null && sqlException.Number == 2601)
                    {
                        throw new FaultException<DuplicateNameFault>(new DuplicateNameFault(), updateEx.Message);
                    }
                    else
                    {
                        throw new FaultException<UpdateFault>(new UpdateFault(), updateEx.Message);
                    }
                }
                catch (DuplicateNameException dne)
                {
                    throw new FaultException<UpdateFault>(new UpdateFault(), dne.Message);
                }
                catch (Exception e)
                {
                    throw new FaultException<UpdateFault>(new UpdateFault(), e.Message);
                }
            }
        }

        public void EditProfile(MnjProfile entity)
        {
            using (var uow = new UnitOfWork())
            {
                try
                {
                    new MnjProfileBL(uow).EditProfile(entity);
                }
                catch (DbUpdateException updateEx)
                {
                    SqlException sqlException = updateEx.InnerException.InnerException as SqlException;
                    if (sqlException != null && sqlException.Number == 2601)
                    {
                        throw new FaultException<DuplicateNameFault>(new DuplicateNameFault(), updateEx.Message);
                    }
                    else
                    {
                        throw new FaultException<UpdateFault>(new UpdateFault(), updateEx.Message);
                    }
                }
                catch (DuplicateNameException dne)
                {
                    throw new FaultException<UpdateFault>(new UpdateFault(), dne.Message);
                }
                catch (Exception e)
                {
                    throw new FaultException<UpdateFault>(new UpdateFault(), e.Message);
                }
            }
        }

        public void SaveReleases(int userId, string releaseIds)
        {
            using (var uow = new UnitOfWork())
            {
                new MnjUserSavedReleasesBL(uow).SaveReleases(userId, releaseIds);
            }
        }

        public void DeleteSavedReleases(int userId, List<int> releaseIds)
        {
            using (var uow = new UnitOfWork())
            {
                new MnjUserSavedReleasesBL(uow).Delete(userId, releaseIds);
            }
        }

        /// <summary>
        /// Add or update a new MediaContactDraft and/or MediaEmployeeDraft to the database by Journalists.
        /// </summary>
        /// <param name="entity">The MediaEmployee details which we want to apply.</param>
        /// <param name="userId">The user id calling the method.</param>
        /// <returns>The Id of the MediaEmployeeDraft created.</returns>
        public void SaveMediaContactAndEmployeeDraft(MediaContactDraft contactDraft, MediaEmployeeDraft employeeDraft, string sessionKey)
        {
            string contactId = contactDraft == null ? employeeDraft.ContactId : contactDraft.ContactId;

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), $"Access to MediaContact for ContactId {contactId} denied.");

            var session = AccessHelper.GetSessionFromKey(sessionKey);

            using (var uow = new UnitOfWork())
            {
                try
                {
                    using (System.Transactions.TransactionScope tran = new System.Transactions.TransactionScope())
                    {
                        if (contactDraft != null)
                            new MediaContactDraftBL(uow).Save(contactDraft, session.UserId);

                        if (employeeDraft != null)
                            new MediaEmployeeDraftBL(uow).Save(employeeDraft, session.UserId);

                        tran.Complete();
                    }

                }
                catch (UpdateException updateEx)
                {
                    throw new FaultException<MediaUpdateValidationFault>(new MediaUpdateValidationFault(updateEx.Message, updateEx.ReasonList), updateEx.Message);
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw new FaultException<DataConcurrencyFault>(new DataConcurrencyFault(), DataConcurrencyFault.FaultDescription);
                }
            }
        }

        public JournalistEmployeeInfo GetJournalistEmployeeInfo(string contactId, string outletId, string sessionKey)
        {
            var contactService = new MediaContactsService();

            JournalistEmployeeInfo result = new JournalistEmployeeInfo
            {
                Employee = contactService.GetMediaEmployee(contactId, outletId, sessionKey)
            };

            using (var uow = new UnitOfWork())
            {
                var profile = new MediaContactProfileBL(uow).GetByContactId(contactId);
                result.Employee.MediaContact.Profile = profile == null ? null : profile.Content;

                result.ContactDraft = new MediaContactDraftBL(uow).GetByContactId(contactId);

                result.EmployeeDraft = new MediaEmployeeDraftBL(uow).Get(contactId, outletId);
            }

            return result;
        }

        public void UpdateSubscription(List<MnjProfile> profiles, Guid unsubscribeKey)
        {
            using (var uow = new UnitOfWork())
            {
                int? userId = new UserBL(uow).GetUserIdByUnsubscribeKey(unsubscribeKey);

                if (!userId.HasValue)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), $"Invalid UnsubscribeKey");

                new MnjProfileBL(uow).UpdateSubscription(profiles, userId.Value);
            }
        }

        public void CreateMNJLogin(string contactId, string sessionKey)
        {
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), $"Access to MediaContact with Id {contactId} denied.");

            using (var uow = new UnitOfWork())
            {
                var contactsService = new MediaContactsService();

                var account = contactsService.GetMediaContact(contactId, sessionKey);
                var employeeList = contactsService.GetMediaEmployeesByContactId(contactId, sessionKey);

                if (employeeList != null)
                    employeeList = employeeList.FindAll(m => m.RowStatus == EmployeeRowStatusType.Active);

                if (employeeList == null || !employeeList.Any())
                    throw new FaultException<ServiceFault>(new ServiceFault("Outlet is required"));

                var employee = employeeList.FirstOrDefault(m => !string.IsNullOrEmpty(m.EmailAddress));

                if (employee == null)
                    throw new FaultException<ServiceFault>(new ServiceFault("Email Address is required"));

                string logonName = $"{account.FirstName}{account.LastName}".Replace(" ", "");
                var userBL = new UserBL(uow);
                var users = userBL.GetAllByEmailAddress(employee.EmailAddress);

                if (users != null && users.Any())
                    throw new FaultException<ServiceFault>(new ServiceFault($"User with EmailAddress {employee.EmailAddress} already exists."));

                if (userBL.GetUserIdByContactId(contactId).HasValue)
                    throw new FaultException<ServiceFault>(new ServiceFault($"MNJ Login for this contact already exists."));

                if (!userBL.IsMnjLogonNameUnique(logonName))
                {
                    logonName = userBL.GenerateUniqueMNJLogonName(logonName);

                    if (string.IsNullOrEmpty(logonName))
                        throw new FaultException<ServiceFault>(new ServiceFault("Logon name already exists with MNJ Website access"));
                }

                byte[] salt = BusinessLayer.Common.PasswordHelper.GenerateSalt();
                User user = new User
                {
                    FirstName = account.FirstName,
                    LastName = account.LastName,
                    Position = employee.JobTitle,
                    DebtorNumber = BusinessLayer.Common.Constants.DEBTOR_NUMBER_Journalists,
                    EmailAddress = employee.EmailAddress,
                    TelephoneNumber = employee.PhoneNumber,
                    FaxNumber = employee.FaxNumber,
                    Salt = Convert.ToBase64String(salt),
                    ExpiryToken = Guid.NewGuid(),
                    HasDistributeWebAccess = false,
                    HasJournalistsWebAccess = true,
                    LogonName = logonName,
                    ContactID = contactId,
                    MustChangePassword = true,
                    UnsubscribeKey = Guid.NewGuid()
                };

                user.Id = userBL.Add(user);

                SendNewMnjAccountEmail(user);
            }
        }

        public void ResendWelcomeMNJEmail(int userId, string contactId, string sessionKey)
        {
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), $"Access to MediaContact with Id {contactId} denied.");

            using (var uow = new UnitOfWork())
            {
                User user = new UserBL(uow).Get(userId);

                SendNewMnjAccountEmail(user);
            }
        }

        private void SendNewMnjAccountEmail(User user)
        {
            using (var uow = new UnitOfWork())
            {
                var userBL = new UserBL(uow);
                string logonName = user.EmailAddress;

                var users = userBL.GetAllByEmailAddress(user.EmailAddress).Where(u => u.Id != user.Id).ToList();

                if (users.Count > 0)
                {
                    // The email address isn't unique, so give them a logon name to logon with instead of an email address
                    logonName = user.LogonName;
                }

                var url = string.Format("{0}?logonName={1}&expiryToken={2}",
                    ConfigurationManager.AppSettings["JournalistsNewAccountURL"], HttpUtility.UrlEncode(logonName), user.ExpiryToken);

                // Send an email to them with a link allowing them to change their password.
                EmailHelper.NewMnjAccountEmail(user.FirstName.ProperCase(), user.EmailAddress, url);
            }
        }

        public List<MnjProfile> GetProfilesByUnsubscribeKey(Guid unsubscribeKey)
        {
            using (var uow = new UnitOfWork())
            {
                int? userId = new UserBL(uow).GetUserIdByUnsubscribeKey(unsubscribeKey);

                if (!userId.HasValue)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), $"Invalid UnsubscribeKey");

                var list = GetMNJUserProfiles(userId.Value);

                return list;
            }
        }

        #region Media Contact Draft

        public MediaContactDraft GetMediaContactDraft(int queueId, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), $"Access to MediaContactDraft with QueueId {queueId} denied.");

            using (var uow = new UnitOfWork())
            {
                var Contact = new MediaContactDraftBL(uow).GetByQueueId(queueId);

                if (Contact == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), $"MediaContactDratft with QueueId {queueId} not found.");

                return Contact;
            }
        }

        public MediaContactDraft GetMediaContactDraft(string contactId, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), $"Access to MediaContactDraft with contactId {contactId} denied.");

            using (var uow = new UnitOfWork())
            {
                var contact = new MediaContactDraftBL(uow).GetByContactId(contactId);

                return contact;
            }
        }

        public void UpdateMediaContactDraft(MediaContactDraft entity, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdmin(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Updating of MediaContactDraft data denied.");

            using (var uow = new UnitOfWork())
            {
                try
                {
                    new MediaContactDraftBL(uow).Save(entity, session.UserId, true);
                }
                catch (SubjectPrimaryContactRequiredException ex)
                {
                    throw new FaultException<SubjectPrimaryContactRequiredFault>(new SubjectPrimaryContactRequiredFault(ex.OutletId, ex.SubjectId, ex.Message), ex.Message);
                }
                catch (UpdateException updateEx)
                {
                    throw new FaultException<MediaUpdateValidationFault>(new MediaUpdateValidationFault(updateEx.Message, updateEx.ReasonList), updateEx.Message);
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw new FaultException<DataConcurrencyFault>(new DataConcurrencyFault(), DataConcurrencyFault.FaultDescription);
                }
            }
        }

        public void DeclineMediaContactDraft(int id, MediaDraftQueue draftQueue, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdmin(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), $"Access to MediaContactDraft with Id {id} denied.");

            try
            {
                using (var uow = new UnitOfWork())
                {
                    new MediaContactDraftBL(uow).ApproveOrDecline(
                        id,
                        draftQueue,
                        DataLayer.Common.DraftQueueStatus.Declined,
                        null,
                        session.UserId);
                }
            }
            catch (UpdateException updateEx)
            {
                throw new FaultException<MediaUpdateValidationFault>(new MediaUpdateValidationFault(updateEx.Message, updateEx.ReasonList), updateEx.Message);
            }
            catch (DbUpdateConcurrencyException)
            {
                throw new FaultException<DataConcurrencyFault>(new DataConcurrencyFault(), DataConcurrencyFault.FaultDescription);
            }
        }

        #endregion

        #region Media Employee Draft

        public MediaEmployeeDraft GetMediaEmployeeDraft(int queueId, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), $"Access to MediaEmployeeDraft with QueueId {queueId} denied.");

            using (var uow = new UnitOfWork())
            {
                var draft = new MediaEmployeeDraftBL(uow).GetByQueueId(queueId);

                if (draft == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), $"MediaEmployeeDraft with QueueId {queueId} not found.");

                return draft;
            }
        }

        public MediaEmployeeDraft GetMediaEmployeeDraft(string contactId, string outletId, string sessionKey)
        {
            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(sessionKey))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), $"Access to MediaEmployeeDraft with contactId {contactId} denied.");

            using (var uow = new UnitOfWork())
            {
                var draft = new MediaEmployeeDraftBL(uow).Get(contactId, outletId);

                return draft;
            }
        }

        public void UpdateMediaEmployeeDraft(MediaEmployeeDraft entity, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdmin(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Updating of MediaEmployeeDraft data denied.");

            using (var uow = new UnitOfWork())
            {
                try
                {
                    new MediaEmployeeDraftBL(uow).Save(entity, session.UserId, true);
                }
                catch (SubjectPrimaryContactRequiredException ex)
                {
                    throw new FaultException<SubjectPrimaryContactRequiredException>(new SubjectPrimaryContactRequiredException(ex.OutletId, ex.SubjectId, ex.Message), ex.Message);
                }
                catch (UpdateException updateEx)
                {
                    throw new FaultException<MediaUpdateValidationFault>(new MediaUpdateValidationFault(updateEx.Message, updateEx.ReasonList), updateEx.Message);
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw new FaultException<DataConcurrencyFault>(new DataConcurrencyFault(), DataConcurrencyFault.FaultDescription);
                }
            }
        }

        public void DeclineMediaEmployeeDraft(int id, MediaDraftQueue draftQueue, string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAdmin(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), $"Access to MediaEmployeeDraft with Id {id} denied.");

            try
            {
                using (var uow = new UnitOfWork())
                {
                    new MediaEmployeeDraftBL(uow).ApproveOrDecline(
                        id,
                        draftQueue,
                        DataLayer.Common.DraftQueueStatus.Declined,
                        null,
                        null,
                        session.UserId);
                }
            }
            catch (UpdateException updateEx)
            {
                throw new FaultException<MediaUpdateValidationFault>(new MediaUpdateValidationFault(updateEx.Message, updateEx.ReasonList), updateEx.Message);
            }
            catch (DbUpdateConcurrencyException)
            {
                throw new FaultException<DataConcurrencyFault>(new DataConcurrencyFault(), DataConcurrencyFault.FaultDescription);
            }
        }

        #endregion

        #region Media Draft Queue

        /// <summary>
        /// Get All Drafts In Queue.
        /// </summary>
        public List<MediaDraftQueueView> GetAllDraftsInQueue(DateTime? startDate, DateTime? endDate, string searchText,
            int pageNumber, int recordsPerPage, SortColumn pSortColumn, SortDirection pSortDirection, int userId, string sessionKey, out int totalCount)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), $"Access to Drafts In Queue for UserId {session.UserId} denied.");

            using (var uow = new UnitOfWork())
            {
                var list = new MediaDraftQueueBL(uow).GetAllInQueue(startDate, endDate, searchText,
                    pageNumber, recordsPerPage, pSortColumn, pSortDirection, out totalCount);

                return list;
            }
        }

        public int GetPendingDraftsInQueueCount(string sessionKey)
        {
            var session = AccessHelper.GetSessionFromKey(sessionKey);

            // Make sure they have access to it.
            if (!AccessHelper.IsAccessAllowedToContactsData(session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), $"Access to Drafts In Queue for UserId {session.UserId} denied.");

            using (var uow = new UnitOfWork())
            {
                var count = new MediaDraftQueueBL(uow).GetPendingDraftsInQueueCount();

                return count;
            }
        }

        #endregion
    }
}
