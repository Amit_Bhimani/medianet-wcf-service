﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using AutoMapper;
using Medianet.Wcf;
using Medianet.Service.Contracts;
using Medianet.Service.Dto;
using Medianet.Service.Common;
using Medianet.Service.FaultExceptions;
using Medianet.DataLayer;
using Medianet.Service.BusinessLayer;
using Medianet.Service.BusinessLayer.Exceptions;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.Entity.Infrastructure;


namespace Medianet.Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [ErrorBehaviorAttribute(typeof(ServiceErrorHandler))]
    public class MonitoringService : IMonitoringService
    {
        public MonitoringService()
        {
            // The windows service host normally does this but we do it here when running in debug mode.
            if (!AutoMapperConfigurator.IsInitialized)
                AutoMapperConfigurator.Configure();
        }

        public List<MonitoringCampaign> GetCampaigns(int userId, string debtorNumber)
        {
            List<MonitoringCampaign> campaigns;
            using (var uow = new UnitOfWork())
            {
                var monitoringBL = new MonitoringBL(uow);
                campaigns = monitoringBL.GetCampaigns(userId, debtorNumber);
            }

            if (campaigns == null)
                throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Campaign with UserId {0} and DebtorNo {1} not found.", userId, debtorNumber));

            return campaigns;
        }

        public MonitoringCampaign GetCampaign(int campaignId)
        {
            MonitoringCampaign campaign;
            using (var uow = new UnitOfWork())
            {
                var monitoringBL = new MonitoringBL(uow);
                campaign = monitoringBL.GetCampaign(campaignId);
            }

            if (campaign == null)
                throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Campaign with Id {0} not found.", campaignId));

            return campaign;
        }

        public int AddCampaign(MonitoringCampaign campaign, string sessionKey)
        {
            int newId = 0;
            var session = AccessHelper.GetSessionFromKey(sessionKey);
            campaign.SearchCriteria.UserId = session.UserId;
            campaign.UserId = session.UserId;

            using (var uow = new UnitOfWork())
            {
                var monitoringBL = new MonitoringBL(uow);
                try
                {
                    newId = monitoringBL.AddCampaign(campaign);
                }
                catch (DbUpdateException updateEx)
                {
                    SqlException sqlException = updateEx.InnerException.InnerException as SqlException;
                    if (sqlException != null && sqlException.Number == 2601)
                    {
                        throw new FaultException<DuplicateNameFault>(new DuplicateNameFault(), updateEx.Message);
                    }
                    else
                    {
                        throw new FaultException<UpdateFault>(new UpdateFault(), updateEx.Message);
                    }
                }
                catch (DuplicateNameException dne)
                {
                    throw new FaultException<UpdateFault>(new UpdateFault(), dne.Message);
                }
                catch (Exception e)
                {
                    throw new FaultException<UpdateFault>(new UpdateFault(), e.Message);
                }
            }

            return newId;
        }

        public bool UpdateCampaign(MonitoringCampaign campaign)
        {
            bool updated = false;

            using (var uow = new UnitOfWork())
            {
                var monitoringBL = new MonitoringBL(uow);
                try
                {
                    monitoringBL.UpdateCampaign(campaign);
                    updated = true;
                }
                catch (DuplicateNameException dne)
                {
                    throw new FaultException<UpdateFault>(new UpdateFault(), dne.Message);
                }
                catch (Exception updateEx)
                {
                    throw new FaultException<UpdateFault>(new UpdateFault(), updateEx.Message);
                }
            }

            return updated;
        }

        public bool DeleteCampaign(int campaignId)
        {
            bool deleted = false;

            using (var uow = new UnitOfWork())
            {
                var monitoringBL = new MonitoringBL(uow);
                try
                {
                    monitoringBL.DeleteCampaign(campaignId);
                    deleted = true;
                }
                catch (Exception updateEx)
                {
                    throw new FaultException<UpdateFault>(new UpdateFault(), updateEx.Message);
                }
            }

            return deleted;
        }

        public int AddSearch(MonitoringSearch search, string sessionKey)
        {
            int newId = 0;
            var session = AccessHelper.GetSessionFromKey(sessionKey);
            search.UserId = session.UserId;
            using (var uow = new UnitOfWork()) 
            {
                var monitoringBL = new MonitoringBL(uow);
                try
                {
                    newId = monitoringBL.AddSearch(search);
                }
                catch (Exception updateEx)
                {
                    throw new FaultException<UpdateFault>(new UpdateFault(), updateEx.Message);
                }
            }

            return newId;
        }

        public MonitoringSearch GetSearch(int searchId)
        {
            MonitoringSearch search;
            using (var uow = new UnitOfWork()) 
            {
                var monitoringBL = new MonitoringBL(uow);
                search = monitoringBL.GetSearch(searchId);
            }

            if (search == null)
                throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("MonitoringSearch with Id {0} not found.", searchId));

            return search;

        }

        public int AddEmail(MonitoringEmail email, string sessionKey)
        {
            int newId = 0;
            var session = AccessHelper.GetSessionFromKey(sessionKey);
            email.UserId = session.UserId;
            email.SearchCriteria.UserId = session.UserId; 
            using (var uow = new UnitOfWork())
            {
                var monitoringBL = new MonitoringBL(uow);
                try
                {
                    newId = monitoringBL.AddEmail(email);
                }
                catch (Exception updateEx)
                {
                    throw new FaultException<UpdateFault>(new UpdateFault(), updateEx.Message);
                }
            }

            return newId;
        }

        public List<MonitoringEmail> GetEmailsByUser(int userId)
        {
            List<MonitoringEmail> emails;
            using (var uow = new UnitOfWork())
            {
                var monitoringBL = new MonitoringBL(uow);
                emails = monitoringBL.GetEmailByUser(userId);
            }

            return emails;
        }

        public List<MonitoringEmail> GetEmailsByCustomer(string debtorNumber)
        {
            List<MonitoringEmail> emails;
            using (var uow = new UnitOfWork())
            {
                var monitoringBL = new MonitoringBL(uow);
                emails = monitoringBL.GetEmailByCustomer(debtorNumber);
            }

            return emails;
        }

        public bool IsCampaignNameUnique(MonitoringCampaign campaign)
        {
            bool isUnique = false;
            using (var uow = new UnitOfWork())
            {
                var monitoringBL = new MonitoringBL(uow);
                isUnique = monitoringBL.IsCampaignNameUnique(campaign);
            }
            
            return isUnique;
        }

        public int AddAlert(MonitoringAlert alert)
        {
            int newId = 0;

            using (var uow = new UnitOfWork())
            {
                var monitoringBL = new MonitoringBL(uow);
                try
                {
                    newId = monitoringBL.AddAlert(alert);
                }
                catch (DbUpdateException updateEx)
                {
                    SqlException sqlException = updateEx.InnerException.InnerException as SqlException;
                    if (sqlException != null && sqlException.Number == 2601)
                    {
                        throw new FaultException<DuplicateNameFault>(new DuplicateNameFault(), updateEx.Message);
                    }
                    else
                    {
                        throw new FaultException<UpdateFault>(new UpdateFault(), updateEx.Message);
                    }
                }
                catch (DuplicateNameException dne)
                {
                    throw new FaultException<UpdateFault>(new UpdateFault(), dne.Message);
                }
                catch (Exception e)
                {
                    throw new FaultException<UpdateFault>(new UpdateFault(), e.Message);
                }
            }

            return newId;
        }

        public bool UpdateAlert(MonitoringAlert alert)
        {
            bool updated = false;

            using (var uow = new UnitOfWork())
            {
                var monitoringBL = new MonitoringBL(uow);
                try
                {
                    monitoringBL.UpdateAlert(alert);
                    updated = true;
                }
                catch (Exception updateEx)
                {
                    throw new FaultException<UpdateFault>(new UpdateFault(), updateEx.Message);
                }
            }

            return updated;
        }

        public bool DeleteAlert(int alertId)
        {
            bool deleted = false;

            using (var uow = new UnitOfWork())
            {
                var monitoringBL = new MonitoringBL(uow);
                try
                {
                    monitoringBL.DeleteAlert(alertId);
                    deleted = true;
                }
                catch (Exception updateEx)
                {
                    throw new FaultException<UpdateFault>(new UpdateFault(), updateEx.Message);
                }
            }

            return deleted;
        }

        public List<MonitoringCampaign> GetCampaignsByAlertFrequency(int frequencyId)
        {
            List<MonitoringCampaign> campaigns;
            using (var uow = new UnitOfWork())
            {
                var monitoringBL = new MonitoringBL(uow);
                campaigns = monitoringBL.GetCampaignsByAlertFrequency(frequencyId);
            }

            return campaigns;
        }

        public List<MonitoringCampaign> GetAllCampaigns()
        {
            List<MonitoringCampaign> campaigns;
            using (var uow = new UnitOfWork())
            {
                var monitoringBL = new MonitoringBL(uow);
                campaigns = monitoringBL.GetAllCampaigns();
            }

            return campaigns;
        }
    }
}
