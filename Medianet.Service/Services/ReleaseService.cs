﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Text.RegularExpressions;
using System.Reflection;
using Medianet.Wcf;
using Medianet.Service.Contracts;
using Medianet.Service.Dto;
using Medianet.Service.Common;
using Medianet.Service.FaultExceptions;
using Medianet.Service.BusinessLayer;
using Medianet.DataLayer;

namespace Medianet.Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [ErrorBehaviorAttribute(typeof(ServiceErrorHandler))]
    public class ReleaseService : IReleaseService
    {
        public ReleaseService()
        {
            // The windows service host normally does this but we do it here when running in debug mode.
            if (!AutoMapperConfigurator.IsInitialized)
                AutoMapperConfigurator.Configure();
        }

        /// <summary>
        /// Add a Release to the database with it's transactions and documents.
        /// </summary>
        /// <param name="releaseSummary">The Release to add to the database.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The Id of the new Release.</returns>
        public int AddReleaseFromSummary(ReleaseSummary releaseSummary, string sessionKey)
        {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);
            Release release;

            ReleaseHelper.LogXML<ReleaseSummary>(releaseSummary);

            // Make sure they have access to it.
            if (!AccessHelper.IsFullAccessAllowedToRelease(releaseSummary.DebtorNumber, releaseSummary.System, session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to creating a Release for DebtorNumber {0} denied.", releaseSummary.DebtorNumber));

            // If its an unverified release, make sure they havent exceeded the maximum release count.
            if (!releaseSummary.IsVerified)
            {
                if (ReleaseHelper.IsMaxUnverifiedReleases(session.User))
                {
                    throw new FaultException<MaximumReleaseCountFault>(new MaximumReleaseCountFault(), String.Format("Unable to submit further releases. Unverified user has already submitted {0} releases.", session.User.ReleaseCount));
                }
            }

            release = ReleaseHelper.GenerateReleaseFromSummary(releaseSummary, session);

         
            release.TransactionId = releaseSummary.TransactionId; // for ChargeBee TransactionId for Success
            release.PaymentMethod = session.Customer.AccountType.ToString(); //CustomerBillingType.Invoiced or CustomerBillingType.Creditcard

            using (var uow = new UnitOfWork())
            {
                var releaseBL = new ReleaseBL(uow);

                // If the release is unverified, check if we need to make it an onhold release
                if (!releaseSummary.IsVerified)
                {
                    // Check if we have a configured hold value
                    string hval = Medianet.Service.BusinessLayer.Common.AAPEnvironmentHelper.GetSetting("UnverifiedHoldMinutes");
                    int interval;
                    if (!string.IsNullOrWhiteSpace(hval) && int.TryParse(hval, out interval))
                    {
                        // Dont bother setting anything unless there is an actual hold interval configured
                        if (interval > 0)
                        {
                            release.HoldUntilDate = DateTime.Now.AddMinutes(interval);
                        }
                    }
                }

                release.Id = releaseBL.Add(release, session.UserId);
            }
            
            if (release.Id > 0 && !releaseSummary.IsVerified)
            {
                User usr;
                // Update the user release count
                using (var uow = new UnitOfWork())
                {
                    var userBL = new UserBL(uow);
                    usr = userBL.Get(session.UserId);
                    if (usr == null)
                        throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("User with Id {0} not found.", session.UserId));

                    usr.ReleaseCount += 1;
                    userBL.Update(usr, usr.Id);
                }
                // If the release created is unverified, send an email to ops to 
                // inform them that a release has been submitted that may need moderation/review.
                EmailHelper.UnverifiedReleaseSubmissionEmail(release.Id, releaseSummary, usr);
            }
            
            return release.Id;
        }

        /// <summary>
        /// Get a Release based on the Id.
        /// </summary>
        /// <param name="releaseId">The Id of the release to get.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The release as well as its Documents and Transactions.</returns>
        public Release GetReleaseById(int releaseId, string sessionKey)
        {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);
            Release release;

            using (var uow = new UnitOfWork())
            {
                var releaseBL = new ReleaseBL(uow);
                release = releaseBL.Get(releaseId);
            }

            if (release == null)
                throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Release with Id {0} not found.", releaseId));

            // Make sure they have access to it.
            if (!AccessHelper.IsFullAccessAllowedToRelease(release.DebtorNumber, release.System, session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to Release with Id {0} denied.", releaseId));

            return AdjustReleaseForTimezone(release, session.Customer.TimezoneCode);
        }

        /// <summary>
        /// Get public details of a Release based on the Id.
        /// </summary>
        /// <param name="releaseId">The Id of the release to get.</param>
        /// <param name="website">The website making the request. This affects the DistributedDate shown.</param>
        /// <param name="clientIp">The IP address of the requester. This is used for logging.</param>
        /// <param name="userAgent">The user agent of the requester. This is used for logging.</param>
        /// <param name="securityKey">The SecurityKey of the release. This is only needed if the release is not publicly available.</param>
        /// <returns>The release as well as its Documents and Transactions.</returns>
        public ReleasePublic GetPublicReleaseById(int releaseId, WebsiteType website, string clientIp, string userAgent, string securityKey)
        {
            ReleasePublic release;

            using (var uow = new UnitOfWork())
            {
                var releaseBL = new ReleaseBL(uow);
                release = releaseBL.GetPublic(releaseId, website, securityKey);

                if (release == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Release with Id {0} not found.", releaseId));

                releaseBL.RecordView(releaseId, clientIp, userAgent, website);
            }

            return release;
        }

        /// <summary>
        /// Get a list of Releases for the current Customer that have Results that are able to be shown.
        /// Results for AAP lists are usually not shown.
        /// </summary>
        /// <param name="startPos">The 0 based starting position, e.g. to return the 3rd page with a pageSize of 10 pass 30.</param>
        /// <param name="pageSize">The size of the page to return.</param>
        /// <param name="sortOrder">The order to sort the results by.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The list of Releases.</returns>
        public ReleasePage GetReleasesWithResultsToShow(int startPos, int pageSize, ReleaseSortOrderType sortOrder, string sessionKey)
        {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);
            ReleasePage page = new ReleasePage();

            page.StartPos = startPos;
            page.PageSize = pageSize;
            page.SortOrder = sortOrder;

            using (var uow = new UnitOfWork())
            {
                var releaseBL = new ReleaseBL(uow);

                page.Releases = releaseBL.GetAllByDebtorNumber(session.DebtorNumber, session.System, sortOrder, startPos, pageSize);
                page.TotalCount = releaseBL.GetCountByDebtorNumber(session.DebtorNumber, session.System);
            }

            page.Releases = AdjustReleasesForTimezone(page.Releases, session.Customer.TimezoneCode);

            return page;
        }

        /// <summary>
        /// Get a list of Feature Releases to be displayed on the website.
        /// </summary>
        /// <param name="webCategoryId">The Id of the Web Category to show feature stories for, or null for the default feature releases.</param>
        /// <returns>The list of Releases.</returns>
        public List<ReleaseSummaryPublic> GetPublicFeatureReleases(int? webCategoryId)
        {
            using (var uow = new UnitOfWork())
            {
                var releaseBL = new ReleaseBL(uow);

                return releaseBL.GetAllPublicFeatureReleases(webCategoryId);
            }
        }

        /// <summary>
        /// Fetches the URL of a web release (public).
        /// </summary>
        /// <param name="id">The Release ID we want a URL for.</param>
        /// <returns>A string containing the URL to the release. Blank string where the release is not found or unavailable.</returns>
        public string GetReleaseURL(int id)
        {
            string retval = string.Empty;
            try
            {
                using (var uow = new UnitOfWork())
                {
                    var releaseBL = new ReleaseBL(uow);
                    Release rel = releaseBL.Get(id);

                    if (rel != null && rel.WebDetails != null)
                    {
                        // make sure the release is transferred and available on the web
                        if (rel.WebDetails.ShouldShowOnPublic && rel.WebDetails.PublicDistributedDate.HasValue)
                        {
                            var urlh = new BusinessLayer.Common.UrlHelper();
                            retval = urlh.GetReleaseUrl(id);
                        }
                    }
                }
            }
            catch
            {
                retval = string.Empty;
            }

            return retval;
        }

        /// <summary>
        /// Get a list of public Releases to be displayed on the website.
        /// </summary>
        /// <param name="request">The search options.</param>
        /// <returns>The list of Releases.</returns>
        public ReleasePagePublic GetPublicReleases(ReleasePagePublicRequest request)
        {
            var pageData = new ReleasePagePublic();

            pageData.Page = request.PageNumber;
            pageData.PageSize = request.PageSize;

            if (request.Website == WebsiteType.Journalists)
            {
                if (!AccessHelper.IsAccessAllowedToJournalistsSearch(request.SessionKey))
                    throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), "Access to search Journalists website denied.");
            }

            using (var uow = new UnitOfWork())
            {
                var releaseBL = new ReleaseBL(uow);
                int totalCount;

                pageData.Releases = releaseBL.GetAllPublicByCriteria(request.Website, request.FromDate, request.ToDate, request.WebCategoryId, request.SearchText, request.MultimediaType,
                    request.ShowVideos, request.ShowAudios, request.ShowPhotos, request.ShowOthers, request.ShowMultimedia, request.PageNumber, request.PageSize, request.ShowMedianetOnly, out totalCount);
                pageData.TotalCount = totalCount;
            }

            return pageData;
        }

        /// <summary>
        /// Get the list of Transactions belonging to a Release.
        /// </summary>
        /// <param name="releaseId">The Id of the Release.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The list of Transactions.</returns>
        public List<Transaction> GetReleaseTransactions(int releaseId, string sessionKey)
        {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);

            using (var uow = new UnitOfWork())
            {
                var transBL = new TransactionBL(uow);
                return transBL.GetAllByReleaseId(releaseId);
            }
        }

        /// <summary>
        /// Get the statistics of all Transactions for a Release that have Results that are able to be shown.
        /// </summary>
        /// <param name="releaseId">The Id of the Release.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The list of Transaction statistics.</returns>
        public List<TransactionStatistics> GetReleaseTransactionStatistics(int releaseId, string sessionKey)
        {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);

            using (var uow = new UnitOfWork())
            {
                var transBL = new TransactionBL(uow);
                return transBL.GetAllStatisticsByReleaseId(releaseId);
            }
        }

        /// <summary>
        /// Get the list of Documents belonging to a Release.
        /// </summary>
        /// <param name="releaseId">The Id of the Release.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The list of Documents.</returns>
        public List<Document> GetReleaseDocuments(int releaseId, string sessionKey)
        {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);

            using (var uow = new UnitOfWork())
            {
                var docBL = new DocumentBL(uow);
                return docBL.GetAllByReleaseId(releaseId);
            }
        }

        /// <summary>
        /// Get the list of Results for a particular Transaction belonging to a release.
        /// </summary>
        /// <param name="releaseId">The Id of the Release.</param>
        /// <param name="transactionId">The Id of the Transaction.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The list of Results.</returns>
        public List<Result> GetReleaseResults(int releaseId, int transactionId, string sessionKey)
        {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);
            Transaction trans;
            List<Result> results;

            using (var uow = new UnitOfWork())
            {
                var transBL = new TransactionBL(uow);
                trans = transBL.Get(transactionId);
            }

            if (trans == null)
                throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Transaction with Id {0} not found.", transactionId));

            if (!AccessHelper.IsAccessAllowedToTransactionResults(trans, session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to Release with Id {0} denied.", releaseId));

            using (var uow = new UnitOfWork())
            {
                var resultBL = new ResultBL(uow);
                results = resultBL.GetAllByReleaseIdTransactionId(releaseId, transactionId);
            }

            AccessHelper.ProtectResults(results, trans);

            return AdjustResultsForTimezone(results, session.Customer.TimezoneCode);
        }

        /// <summary>
        /// Send a public Release to an email address.
        /// </summary>
        /// <param name="releaseId">The Id of the release to send.</param>
        /// <param name="website">The website making the request. This affects the DistributedDate shown.</param>
        /// <param name="emailAddress">The Email address to send to</param>
        /// <param name="securityKey">The SecurityKey of the release. This is only needed if the release is not publicly available.</param>
        public void EmailPublicReleaseById(int releaseId, WebsiteType website, string emailAddress, string securityKey)
        {
            ReleasePublic release;

            using (var uow = new UnitOfWork())
            {
                var releaseBL = new ReleaseBL(uow);
                release = releaseBL.GetPublic(releaseId, website, securityKey);
            }

            if (release == null)
                throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Release with Id {0} not found.", releaseId));

            // If the html content is just an iframe this won't work in all email clients
            // so replace with a link to the website.
            if (release.HtmlContent.IndexOf("iframe", StringComparison.CurrentCultureIgnoreCase) >= 0)
            {
                var helper = new Medianet.Service.BusinessLayer.Common.UrlHelper();
                release.HtmlContent = string.Format("Click <a href='{0}'>here</a> to view the release.", helper.GetReleaseUrl(release.Id));
            }

            EmailHelper.PublicReleaseEmail(release, emailAddress);
        }

        /// <summary>
        /// Initialise the creditcard payment of a Release.
        /// </summary>
        /// <param name="payment">The details of the Payment.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>A html form requesting creditcard information which will be posted to our creditcard provider for processing.</returns>
        public PaymentStartResponse InitialisePayment(PaymentStartRequest payment, string sessionKey)
        {
            FinancePaymentService.MNTransactionHeader finQuoteRequest;
            FinancePaymentService.PaymentPageRequest finPaymentRequest;
            FinancePaymentService.MedianetPaymentPage finPaymentResponse = new FinancePaymentService.MedianetPaymentPage();
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);
            Release release;
            ReleaseItemCounts releaseItems;

            NLog.LogManager.GetCurrentClassLogger().Log(NLog.LogLevel.Info, "InitialisePayment:SessionKey=" + sessionKey);
            ReleaseHelper.LogXML<PaymentStartRequest>(payment);

            // Only admins can create Releases for other people.
            if (!AccessHelper.IsFullAccessAllowedToRelease(payment.ReleaseQuote.DebtorNumber, payment.ReleaseQuote.System, session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access denied paying for Release for Customer {0}.", payment.ReleaseQuote.DebtorNumber));

            // If your not fully authorised, you cant submit a job where money is owing.
            if (!session.IsAuthorised)
                throw new FaultException<NotFullyAuthorisedFault>(new NotFullyAuthorisedFault(), string.Format("Please verify your credentials by logging in before payment can be processed."));

            // Create a Release and work out the number of wire words & attachments.
            release = ReleaseHelper.GenerateReleaseFromQuote(payment.ReleaseQuote, session);
            releaseItems = ReleaseHelper.GenerateReleaseItemCounts(release);

            if (ConfigurationManager.AppSettings["EnvironmentType"].ToString() != Constants2.EnvironmentLocal)
            {
                if (release.HasWireTransactions() && releaseItems.WireWordCount == 0)
                    throw new FaultException<FileNotFoundFault>(new FileNotFoundFault(), string.Format("Failed to find a " + BusinessLayer.Common.Constants.FILETYPE_TEXT + " Document for Wire distributions."));
            }
            // Generate a finance quote request object.
            finQuoteRequest = FinanceHelper.GenerateFinanceQuoteRequest(release, releaseItems, session.User);

            if (finQuoteRequest.MNTransactions.Length == 0)
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), "Cannot perform a creditcard transaction for a Release with no billable transactions.");

            // Generate a finance payment request object and attach a finance quote request to it.
            finPaymentRequest = new FinancePaymentService.PaymentPageRequest();
            finPaymentRequest.MnTransactionHeader = finQuoteRequest;
            finPaymentRequest.SystemCode = BusinessLayer.Common.Constants.FINANCE_SYSTEM_CODE;
            finPaymentRequest.CustomerId = payment.ReleaseQuote.DebtorNumber;
            finPaymentRequest.RedirectURL = payment.RedirectURL;
            finPaymentRequest.IPAddress = payment.IPAddress;

            // Initialise the payment.
            //finPaymentResponse = FinanceHelper.RequestCreditcardAccessCode(finPaymentRequest);

            // Generate a quote response and return it.
            ReleaseQuoteResponse quoteResponse = GenerateQuoteResponse(new List<TransactionSummary>(), releaseItems, FinanceHelper.QuoteRelease(finQuoteRequest));

            // For One Time on Package so Sum all Same Id Packages into one
            var transactions = new List<TransactionQuoteResponse>();
            foreach (var trans in quoteResponse.Transactions)
            {
                if (transactions.Exists(t => t.Reference == trans.Reference))
                {
                    var tran = transactions.Find(t => t.AccountCode == trans.AccountCode);
                    trans.Quantity = tran.Quantity + 1;
                    transactions.Remove(tran);
                }
                transactions.Add(trans);
            }
            quoteResponse.Transactions = transactions;
            //Get Payment Page From ChargeBee
            Medianet.Service.ChargeBeeService cb = new ChargeBeeService();
            finPaymentResponse.PaymentPageHTML = cb.HostedPageCheckoutOneTime(quoteResponse, finPaymentRequest.RedirectURL);
            // Generate a PaymentStartResponse and return it.
            return GeneratePaymentStartResponse(finPaymentResponse);
        }

        /// <summary>
        /// Check the success or failure of the creditcard payment for a Release.
        /// </summary>
        /// <param name="paymentAccessCode">The details of the Release.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The details of the quote.</returns>
        public PaymentEndResponse FinalisePayment(string paymentAccessCode, string sessionKey)
        {
            //FinancePaymentService.MedianetPaymentResult finPaymentResponse;
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);

            NLog.LogManager.GetCurrentClassLogger().Log(NLog.LogLevel.Info, "FinalisePayment:paymentAccessCode=" + paymentAccessCode + " :SessionKey=" + sessionKey);

            // If your not fully authorised, you cant submit a job where money is owing.
            if (!session.IsAuthorised)
                throw new FaultException<NotFullyAuthorisedFault>(new NotFullyAuthorisedFault(), string.Format("Please verify your credentials by logging in before payment can be processed."));

            if (string.IsNullOrWhiteSpace(paymentAccessCode))
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), "FinalisePayment can not be passed an empty paymentAccessCode.");

            // Request the result of the transaction from ChargeBee.
            Medianet.Service.ChargeBeeService cb = new ChargeBeeService();
            var transactionId = cb.AcknowledgePayment(paymentAccessCode);
            PaymentEndResponse paymentResponse = new PaymentEndResponse();
            if (transactionId != "")
            {
                paymentResponse.PaymentSucceeded = true;
                paymentResponse.PaymentAccessCode = paymentAccessCode;
                paymentResponse.ResponseCode = transactionId;
            }
            else
            {
                paymentResponse.PaymentSucceeded = false;
            }
            // Generate a PaymentEndResponse and return it.
            return paymentResponse;
        }

        /// <summary>
        /// Quote the price of a Release that has not yet been created.
        /// </summary>
        /// <param name="releaseQuote">The details of the Release.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The details of the quote.</returns>
        public ReleaseQuoteResponse QuoteRelease(ReleaseQuoteRequest releaseQuote, string sessionKey)
        {
            FinancePaymentService.MNTransactionHeader finQuoteRequest;
            FinancePaymentService.TransactionPriceResponse finQuoteResponse;
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);
            Release release;
            ReleaseItemCounts releaseItems;

            NLog.LogManager.GetCurrentClassLogger().Log(NLog.LogLevel.Info, "QuoteRelease:SessionKey=" + sessionKey);
            ReleaseHelper.LogXML<ReleaseQuoteRequest>(releaseQuote);

            if (!AccessHelper.IsFullAccessAllowedToRelease(releaseQuote.DebtorNumber, releaseQuote.System, session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access denied creating Release for Customer {0}.", releaseQuote.DebtorNumber));

            // Create a Release and work out the number of wire words & attachments.
            release = ReleaseHelper.GenerateReleaseFromQuote(releaseQuote, session);
            releaseItems = ReleaseHelper.GenerateReleaseItemCounts(release);

            if (ConfigurationManager.AppSettings["EnvironmentType"].ToString() != Constants2.EnvironmentLocal)
            {
                if (release.HasWireTransactions() && releaseItems.WireWordCount == 0)
                    throw new FaultException<FileNotFoundFault>(new FileNotFoundFault(), string.Format("Failed to find a " + BusinessLayer.Common.Constants.FILETYPE_TEXT + " Document for Wire distributions."));
            }

            finQuoteRequest = FinanceHelper.GenerateFinanceQuoteRequest(release, releaseItems, session.User);

            // Quote the job.
            finQuoteResponse = FinanceHelper.QuoteRelease(finQuoteRequest);

            // Generate a quote response and return it.
            return GenerateQuoteResponse(releaseQuote.Transactions, releaseItems, finQuoteResponse);
        }

        /// <summary>
        /// Quote the price of a Release entered on the calculator page.
        /// </summary>
        /// <param name="releaseCalc">The details of the release.</param>
        /// <param name="sessionKey">The SessionKey of the user calling the method.</param>
        /// <returns>The details of the quote.</returns>
        public ReleaseQuoteResponse CalculateRelease(ReleaseCalculatorRequest releaseCalc, string sessionKey)
        {
            FinancePaymentService.MNTransactionHeader finQuoteRequest;
            FinancePaymentService.TransactionPriceResponse finQuoteResponse;
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);
            Release release;
            ReleaseItemCounts releaseItems = new ReleaseItemCounts()
            {
                WireWordCount = releaseCalc.WordCount,
                AttachmentCount = releaseCalc.AttachmentCount,
            };

            ReleaseHelper.LogXML<ReleaseCalculatorRequest>(releaseCalc);

            if (!AccessHelper.IsFullAccessAllowedToRelease(releaseCalc.DebtorNumber, releaseCalc.System, session))
                throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access denied calculating Release for Customer {0}.", releaseCalc.DebtorNumber));

            // Create a Release and work out the number of wire words & attachments.
            release = ReleaseHelper.GenerateReleaseFromCalculator(releaseCalc, session);

            if (ConfigurationManager.AppSettings["EnvironmentType"].ToString() != Constants2.EnvironmentLocal)
            {
                if (release.HasWireTransactions() && releaseCalc.WordCount == 0)
                    throw new FaultException<FileNotFoundFault>(new FileNotFoundFault(), string.Format("Failed to find a word count for Wire distributions."));
            }

            finQuoteRequest = FinanceHelper.GenerateFinanceQuoteRequest(release, releaseItems, session.User);

            // Quote the job.
            finQuoteResponse = FinanceHelper.QuoteRelease(finQuoteRequest);

            // Generate a quote response and return it.
            return GenerateQuoteResponse(releaseCalc.Transactions, releaseItems, finQuoteResponse);
        }

        public ReleaseQuoteResponse CalculatePrice(Release release, string billerDebtorNumber)
        {
            FinancePaymentService.MNTransactionHeader finQuoteRequest;
            FinancePaymentService.TransactionPriceResponse finQuoteResponse;

            ReleaseItemCounts releaseItems = ReleaseHelper.GenerateReleaseItemCounts(release);

            ReleaseHelper.LogXML<Release>(release);

            if (release.HasWireTransactions() && releaseItems.WireWordCount == 0)
                throw new FaultException<FileNotFoundFault>(new FileNotFoundFault(), string.Format("Failed to find a word count for Wire distributions."));

            finQuoteRequest = FinanceHelper.GenerateFinanceQuoteRequest(release, releaseItems, new Dto.User { BillerDebtorNumber = billerDebtorNumber });

            // Quote the job.
            finQuoteResponse = FinanceHelper.QuoteRelease(finQuoteRequest);

            var transSummary = AutoMapper.Mapper.Map<List<Transaction>, List<TransactionSummary>>(release.Transactions);

            // Generate a quote response and return it.
            return GenerateQuoteResponse(transSummary, releaseItems, finQuoteResponse);
        }


        /// <summary>
        /// Initialise the creditcard payment of a Release.
        /// </summary>
        /// <param name="payment">The details of the Payment.</param>
        /// <returns>A html form requesting creditcard information which will be posted to our creditcard provider for processing.</returns>
        public PaymentStartResponse InitialiseCoursePayment(CoursePaymentStartRequest payment)
        {
            FinancePaymentService.MNTransactionHeader finQuoteRequest;
            FinancePaymentService.PaymentPageRequest finPaymentRequest;
            FinancePaymentService.MedianetPaymentPage finPaymentResponse;
            Release release;
            var counts = new ReleaseItemCounts() { AttachmentCount = payment.Seats, WireWordCount = payment.Seats, SeatCount = payment.Seats };

            NLog.LogManager.GetCurrentClassLogger().Log(NLog.LogLevel.Info, "InitialiseCoursePayment:DebtorNumber=" + payment.DebtorNumber);
            ReleaseHelper.LogXML<CoursePaymentStartRequest>(payment);

            // Create a fake Release for charging for the course.
            release = ReleaseHelper.GenerateReleaseForCourse(payment.ScheduleId, payment.DebtorNumber, payment.PromoCode);

            // Generate a finance quote request object.
            finQuoteRequest = FinanceHelper.GenerateFinanceQuoteRequest(release, counts, null, true);

            if (finQuoteRequest.MNTransactions.Length == 0)
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), "Cannot perform a creditcard transaction for a Release with no billable transactions.");

            // Generate a finance payment request object and attach a finance quote request to it.
            finPaymentRequest = new FinancePaymentService.PaymentPageRequest();
            finPaymentRequest.MnTransactionHeader = finQuoteRequest;
            finPaymentRequest.SystemCode = BusinessLayer.Common.Constants.FINANCE_SYSTEM_CODE;
            finPaymentRequest.CustomerId = payment.DebtorNumber;
            finPaymentRequest.RedirectURL = payment.RedirectURL;
            finPaymentRequest.IPAddress = payment.IPAddress;

            // Initialise the payment.
            finPaymentResponse = FinanceHelper.RequestCreditcardAccessCode(finPaymentRequest);

            // Generate a PaymentStartResponse and return it.
            return GeneratePaymentStartResponse(finPaymentResponse);
        }

        /// <summary>
        /// Check the success or failure of the creditcard payment for a Release.
        /// </summary>
        /// <param name="paymentAccessCode">The details of the Release.</param>
        /// <param name="debtorNumber"></param>
        /// <returns>The details of the quote.</returns>
        public PaymentEndResponse FinaliseCoursePayment(string paymentAccessCode, string debtorNumber)
        {
            FinancePaymentService.MedianetPaymentResult finPaymentResponse;

            NLog.LogManager.GetCurrentClassLogger().Log(NLog.LogLevel.Info, "FinaliseCoursePayment:DebtorNumber=" + debtorNumber);

            if (string.IsNullOrWhiteSpace(paymentAccessCode))
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), "FinaliseCoursePayment can not be passed an empty paymentAccessCode.");

            if (string.IsNullOrWhiteSpace(debtorNumber))
                throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), "FinaliseCoursePayment can not be passed an empty debtorNumber.");

            // Request the result of the creditcard transaction.
            finPaymentResponse = FinanceHelper.RequestCreditcardResult(paymentAccessCode);

            // Generate a PaymentEndResponse and return it.
            return GeneratePaymentEndResponse(finPaymentResponse);
        }

        public void RegisterForCourse(CourseRegistrationRequest account)
        {
            using (var uow = new UnitOfWork())
            {
                var courseBL = new TrainingCourseBL(uow);
                bool isOverBooked;
                bool isContentMissing = false;
                var enrolment = AutoMapper.Mapper.Map<CourseRegistrationRequest, TrainingCourseEnrolment>(account);

                enrolment.NumberOfAttendees = 1;
                enrolment.RowStatus = RowStatusType.Active;

                courseBL.RegisterForSchedule(enrolment);

                var schedule = courseBL.GetSchedule(enrolment.ScheduleId);

                if (schedule == null)
                    throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), string.Format("Course with ScheduleId {0} not found.", enrolment.ScheduleId));

                var course = courseBL.Get(schedule.TrainingCourseId);

                if (course == null)
                    throw new FaultException<InvalidParameterFault>(new InvalidParameterFault(), string.Format("Course with Id {0} not found.", schedule.TrainingCourseId));

                isOverBooked = (schedule.Enrolments.Count(e => e.RowStatus == RowStatusType.Active) > schedule.Seats);

                if (!string.IsNullOrWhiteSpace(account.DebtorNumber) && string.IsNullOrWhiteSpace(account.FirstName) && string.IsNullOrWhiteSpace(account.EmailAddress))
                {
                    // The details in the session  must have expired. Try to find the user details from their customer
                    var custBL = new CustomerBL(uow);
                    var cust = custBL.Get(account.DebtorNumber);

                    isContentMissing = true;

                    if (cust != null)
                    {
                        FinanceCustomerService.MedianetCustomer financeCust = FinanceHelper.GetCustomer(account.DebtorNumber);
                        account.CompanyName = cust.Name;

                        if (financeCust.PrimaryAddress != null)
                        {
                            account.FirstName = financeCust.PrimaryAddress.ContactPerson;
                            account.TelephoneNumber = financeCust.PrimaryAddress.Phone1;
                        }

                        if (financeCust.ToEmail != null)
                            account.EmailAddress = string.Join(";", financeCust.ToEmail);
                    }
                }

                EmailHelper.CourseRegistrationEmail(account, course.Title, schedule.CourseDate, schedule.LocationId, isOverBooked, isContentMissing, ConfigurationManager.AppSettings["CourseRegisterEmailToAddresses"]);
            }
        }

        public bool CancelRelease(int releaseId, string sessionKey)
        {
            DBSession session = AccessHelper.GetSessionFromKey(sessionKey);
            Release release;

            using (var uow = new UnitOfWork())
            {
                var releaseBL = new ReleaseBL(uow);
                release = releaseBL.Get(releaseId);

                if (release == null)
                    throw new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), string.Format("Release with Id {0} not found.", releaseId));

                // Make sure they have access to it.
                if (!AccessHelper.IsFullAccessAllowedToRelease(release.DebtorNumber, release.System, session))
                    throw new FaultException<AccessDeniedFault>(new AccessDeniedFault(), string.Format("Access to Release with Id {0} denied.", releaseId));

                releaseBL.CancelRelease(releaseId, session.UserId);
            }

            return true;
        }

        public LinqToTwitter.Status PushToTwitter(CasTwitterCategory twitterCat, string tweet, string shortUrl)
        {
            SocialMediaServices.Twitter.TwitterUtil twitter = new SocialMediaServices.Twitter.TwitterUtil(twitterCat.ConsumerKey,
               twitterCat.ConsumerKeySecret,
               twitterCat.AccessToken,
               twitterCat.AccessTokenSecret);

            var twitterStatus = twitter.PostTweet(tweet + " " + shortUrl ?? "");

            return twitterStatus;
        }

        public LinqToTwitter.Status Retweet(CasTwitterCategory twitterCat, LinqToTwitter.Status tweet)
        {
            SocialMediaServices.Twitter.TwitterUtil twitter = new SocialMediaServices.Twitter.TwitterUtil(
                twitterCat.ConsumerKey,
                twitterCat.ConsumerKeySecret,
                twitterCat.AccessToken,
                twitterCat.AccessTokenSecret);

            var twitterStatus = twitter.Retweet(tweet.StatusID);

            return twitterStatus;
        }

        public string GetShortUrlFromSocialMediaService(string fullUrl)
        {
            using (var uow = new UnitOfWork())
            {
                var environmentBL = new AAPEnvironmentBL(uow);

                SocialMediaServices.Url.ShortUrlUtil.AccessToken = environmentBL.Get("BitlyAccessToken").Value;
                SocialMediaServices.Url.ShortUrlUtil.AccessTokenForBackupUser = environmentBL.Get("BitlyAccessTokenBackup").Value;
                SocialMediaServices.Url.ShortUrlUtil.ApiShortenUrl = environmentBL.Get("BitlyApiShortenUrl").Value;

                return SocialMediaServices.Url.ShortUrlUtil.ShortenUrl(fullUrl);
            }
        }
   		
		public ReleasePagePublic GetWebReleaseByProfileId(int profileId, int itemsPerPage, int currentPage)
        {
            using (var uow = new UnitOfWork())
            {
                var result = new ReleaseBL(uow).GetWebReleaseByProfileId(profileId, itemsPerPage, currentPage);

                return result;
            }
        }

        public ReleasePagePublic GetSavedReleases(int userId, int itemsPerPage, int currentPage)
        {
            using (var uow = new UnitOfWork())
            {
                var result = new MnjUserSavedReleasesBL(uow).GetSavedReleases(userId, itemsPerPage, currentPage);

                return result;
            }
        }
        #region Private methods

        private Release AdjustReleaseForTimezone(Release release, string timezoneCode)
        {
            // If the timezone isn't sydney then adjust the dates.
            if (!string.IsNullOrWhiteSpace(timezoneCode) && timezoneCode != BusinessLayer.Common.Constants.TIMEZONE_SYD)
            {
                TimezoneHelper tzHelper = new TimezoneHelper(BusinessLayer.Common.Constants.TIMEZONE_SYD, timezoneCode);

                if (release.HoldUntilDate.HasValue) release.HoldUntilDate = tzHelper.AdjustDateTime(release.HoldUntilDate.Value);
                if (release.EmbargoUntilDate.HasValue) release.EmbargoUntilDate = tzHelper.AdjustDateTime(release.EmbargoUntilDate.Value);
                if (release.DistributedDate.HasValue) release.DistributedDate = tzHelper.AdjustDateTime(release.DistributedDate.Value);
                release.CreatedDate = tzHelper.AdjustDateTime(release.CreatedDate);

                if (release.WebDetails != null)
                {
                    if (release.WebDetails.PublicDistributedDate.HasValue) release.WebDetails.PublicDistributedDate = tzHelper.AdjustDateTime(release.WebDetails.PublicDistributedDate.Value);
                    if (release.WebDetails.JournalistsDistributedDate.HasValue) release.WebDetails.JournalistsDistributedDate = tzHelper.AdjustDateTime(release.WebDetails.JournalistsDistributedDate.Value);
                }
            }

            return release;
        }

        private List<Release> AdjustReleasesForTimezone(List<Release> releases, string timezoneCode)
        {
            // If the timezone isn't sydney then adjust the dates.
            if (!string.IsNullOrWhiteSpace(timezoneCode) && timezoneCode != BusinessLayer.Common.Constants.TIMEZONE_SYD)
            {
                TimezoneHelper tzHelper = new TimezoneHelper(BusinessLayer.Common.Constants.TIMEZONE_SYD, timezoneCode);

                foreach (var rel in releases)
                {
                    if (rel.HoldUntilDate.HasValue) rel.HoldUntilDate = tzHelper.AdjustDateTime(rel.HoldUntilDate.Value);
                    if (rel.EmbargoUntilDate.HasValue) rel.EmbargoUntilDate = tzHelper.AdjustDateTime(rel.EmbargoUntilDate.Value);
                    if (rel.DistributedDate.HasValue) rel.DistributedDate = tzHelper.AdjustDateTime(rel.DistributedDate.Value);
                    rel.CreatedDate = tzHelper.AdjustDateTime(rel.CreatedDate);

                    if (rel.WebDetails != null)
                    {
                        if (rel.WebDetails.PublicDistributedDate.HasValue) rel.WebDetails.PublicDistributedDate = tzHelper.AdjustDateTime(rel.WebDetails.PublicDistributedDate.Value);
                        if (rel.WebDetails.JournalistsDistributedDate.HasValue) rel.WebDetails.JournalistsDistributedDate = tzHelper.AdjustDateTime(rel.WebDetails.JournalistsDistributedDate.Value);
                    }
                }
            }

            return releases;
        }

        private List<Result> AdjustResultsForTimezone(List<Result> results, string timezoneCode)
        {
            // If the timezone isn't sydney then adjust the dates.
            if (!string.IsNullOrWhiteSpace(timezoneCode) && timezoneCode != BusinessLayer.Common.Constants.TIMEZONE_SYD)
            {
                TimezoneHelper tzHelper = new TimezoneHelper(BusinessLayer.Common.Constants.TIMEZONE_SYD, timezoneCode);

                foreach (var res in results)
                {
                    if (res.TransmittedTime.HasValue) res.TransmittedTime = tzHelper.AdjustDateTime(res.TransmittedTime.Value);
                    res.CreatedDate = tzHelper.AdjustDateTime(res.CreatedDate);
                }
            }

            return results;
        }

        private static PaymentStartResponse GeneratePaymentStartResponse(FinancePaymentService.MedianetPaymentPage finPaymentResponse)
        {
            PaymentStartResponse paymentResponse;

            // Map the TransactionPriceResponse object returned by Finance to a ReleaseQuoteResponse.
            paymentResponse = new PaymentStartResponse();
            //paymentResponse.ExGST = finQuoteResponse.ExGST;
            paymentResponse.Total = finPaymentResponse.TotalAmount;
            paymentResponse.PaymentAccessCode = finPaymentResponse.AccessCode;
            paymentResponse.PaymentForm = finPaymentResponse.PaymentPageHTML;
            paymentResponse.QuoteReferenceId = finPaymentResponse.LogID;

            return paymentResponse;
        }

        private static PaymentEndResponse GeneratePaymentEndResponse(FinancePaymentService.MedianetPaymentResult finPaymentResponse)
        {
            PaymentEndResponse paymentResponse;

            paymentResponse = new PaymentEndResponse();

            paymentResponse.Total = finPaymentResponse.TotalAmount;
            paymentResponse.PaymentAccessCode = finPaymentResponse.AccessCode;
            paymentResponse.TransactionRecordId = finPaymentResponse.EWayTransactionRecordId;
            paymentResponse.ResponseCode = finPaymentResponse.ResponseCode;
            paymentResponse.ResponseMessage = finPaymentResponse.ResponseMessage;
            paymentResponse.PaymentSucceeded = finPaymentResponse.PaymentSucceed;

            return paymentResponse;
        }
        private static ReleaseQuoteResponse GenerateQuoteResponse(List<TransactionSummary> transactions, ReleaseItemCounts releaseItems,
                                                          FinancePaymentService.TransactionPriceResponse finQuoteResponse)
        {
            ReleaseQuoteResponse quoteResponse;

            // Map the TransactionPriceResponse object returned by Finance to a ReleaseQuoteResponse.
            quoteResponse = new ReleaseQuoteResponse();
            quoteResponse.ContractExpired = finQuoteResponse.ContractExpired;
            quoteResponse.ExGST = finQuoteResponse.ExGST;
            quoteResponse.Total = finQuoteResponse.Total;
            quoteResponse.ReleaseWords = releaseItems.WireWordCount;
            quoteResponse.QuoteReferenceId = finQuoteResponse.LogID;
            quoteResponse.Transactions = new List<TransactionQuoteResponse>();
            quoteResponse.CustNum = finQuoteResponse.CustNum;
            if (finQuoteResponse.TransactionPriceDetails != null)
            {
                foreach (FinancePaymentService.TransactionPriceDetail pd in finQuoteResponse.TransactionPriceDetails)
                {
                    var transactionQuoteResponse = AutoMapper.Mapper.Map<FinancePaymentService.TransactionPriceDetail, TransactionQuoteResponse>(pd);
                    transactionQuoteResponse.intPrice = (int)Math.Round(transactionQuoteResponse.UnitPrice * 100); // For chargeBee Price
                    quoteResponse.Transactions.Add(transactionQuoteResponse);
                }
            }

            // Add any transactions that we didn't get a quote back for. This happens for services with no charge
            // and customers that finance don't have info for.
            foreach (var trans in transactions)
            {
                if (!quoteResponse.Transactions.Exists(t => t.SequenceNumber == trans.SequenceNumber))
                    quoteResponse.Transactions.Add(AutoMapper.Mapper.Map<TransactionSummary, TransactionQuoteResponse>(trans));
            }

            return quoteResponse;
        }

        #endregion
    }
}
