﻿namespace Medianet.Wcf
{
    using System;
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using System.ServiceModel.Dispatcher;    
    using Medianet.Service.Dto;
    using Medianet.Service.Common;
    using System.Net.Sockets;
    using NLog;
    using Medianet.Service.FaultExceptions;
    using System.Reflection;

    public class ServiceErrorHandler : IErrorHandler
    {        
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public void ProvideFault(Exception error, MessageVersion version, ref Message fault) {
            if (error is FaultException) return;

            var faultDetail = new ServiceFault
                              {Id = Guid.NewGuid(), MessageText = Constants2.GenericServiceErrorMessage};
            var fex = new FaultException<ServiceFault>(faultDetail, "Unhandled Application Fault",
                                                       new FaultCode("ApplicationFault"));

            var msgFault = fex.CreateMessageFault();
            fault = Message.CreateMessage(version, msgFault, Constants2.GenericServiceErrorFaultAction);
        }

        public bool HandleError(Exception error) {
            // Log all errors that have not inherited from DoNotLogFault.
            if (IsDoNotLogFault(error)) return true;

            Logger.LogException(LogLevel.Error, "Exception handled in " + typeof (ServiceErrorHandler).Name, error);

            return true;
        }

        public static bool IsDoNotLogFault(Exception error) {
            try {
                if (error is FaultException) {
                    Type exceptionType = error.GetType();
                    PropertyInfo prop = exceptionType.GetProperty("Detail");

                    if (prop != null) {
                        if (typeof(DoNotLogFault).IsAssignableFrom(prop.PropertyType)) return true;
                    }
                }
            }
            catch (Exception ex) {
                Logger.LogException(LogLevel.Error, "Failed to determine Exception type.", ex);
            }

            return false;
        }
    }
}
