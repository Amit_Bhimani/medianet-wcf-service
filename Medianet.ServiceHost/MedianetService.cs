﻿using System;
using System.ServiceModel;
using System.ServiceProcess;
using System.Threading;
using Ninject.Extensions.Wcf;
using NLog;
using Medianet.Service;
using Medianet.Service.Common;
using Medianet.NopCommerce.Service;

namespace Medianet.MedianetServiceHost
{
    public partial class MedianetService : ServiceBase
    {
        private readonly System.ServiceModel.ServiceHost _chargeBeeServiceHost;
        private readonly System.ServiceModel.ServiceHost _customerServiceHost;
        private readonly System.ServiceModel.ServiceHost _mediaContactsServiceHost;
        private readonly System.ServiceModel.ServiceHost _fileServiceHost;
        private readonly System.ServiceModel.ServiceHost _generalServiceHost;
        private readonly System.ServiceModel.ServiceHost _geographicServiceHost;
        private readonly System.ServiceModel.ServiceHost _listServiceHost;
        private readonly System.ServiceModel.ServiceHost _listEditServiceHost;
        private readonly System.ServiceModel.ServiceHost _releaseServiceHost;
        private readonly System.ServiceModel.ServiceHost _monitoringServiceHost;
        private readonly System.ServiceModel.ServiceHost _nopcommerceServiceHost;
        private readonly Logger _logger;
        private readonly System.ServiceModel.ServiceHost _mediaJournalistsServiceHost;

        public MedianetService(NinjectServiceHost<ChargeBeeService> chargeBeeCustomerServiceHost, NinjectServiceHost<CustomerService> customerServiceHost,
                               NinjectServiceHost<MediaContactsService> mediaContactsServiceHost,
                               NinjectServiceHost<FileService> fileServiceHost,
                               NinjectServiceHost<GeneralService> generalServiceHost,
                               NinjectServiceHost<GeographicService> geographicServiceHost,
                               NinjectServiceHost<ListService> listServiceHost,
                               NinjectServiceHost<ListEditService> listEditServiceHost,
                               NinjectServiceHost<ReleaseService> releaseServiceHost,
                               NinjectServiceHost<MonitoringService> monitoringServiceHost,
                               NinjectServiceHost<NopCommerceService> nopcommerceServiceHost,
                               NinjectServiceHost<MediaJournalistsService> medianetJournalistsService) {
            InitializeComponent();

            _chargeBeeServiceHost = chargeBeeCustomerServiceHost;
            _customerServiceHost = customerServiceHost;
            _mediaContactsServiceHost = mediaContactsServiceHost;
            _fileServiceHost = fileServiceHost;
            _generalServiceHost = generalServiceHost;
            _geographicServiceHost = geographicServiceHost;
            _listServiceHost = listServiceHost;
            _listEditServiceHost = listEditServiceHost;
            _releaseServiceHost = releaseServiceHost;
            _monitoringServiceHost = monitoringServiceHost;
            _nopcommerceServiceHost = nopcommerceServiceHost;
            _mediaJournalistsServiceHost = medianetJournalistsService;

            _logger = LogManager.GetCurrentClassLogger();
        }
        
        protected override void OnStart(string[] args) {
            try {
                // Start the WCF host.
                _logger.Info("Starting service host.");

                _chargeBeeServiceHost.Open();
                WriteLogEntriesForService(_chargeBeeServiceHost);

                _customerServiceHost.Open();
                WriteLogEntriesForService(_customerServiceHost);

                _mediaContactsServiceHost.Open();
                WriteLogEntriesForService(_mediaContactsServiceHost);

                _fileServiceHost.Open();
                WriteLogEntriesForService(_fileServiceHost);

                _generalServiceHost.Open();
                WriteLogEntriesForService(_generalServiceHost);

                _geographicServiceHost.Open();
                WriteLogEntriesForService(_geographicServiceHost);

                _listServiceHost.Open();
                WriteLogEntriesForService(_listServiceHost);

                _listEditServiceHost.Open();
                WriteLogEntriesForService(_listEditServiceHost);

                _releaseServiceHost.Open();
                WriteLogEntriesForService(_releaseServiceHost);

                _monitoringServiceHost.Open();
                WriteLogEntriesForService(_monitoringServiceHost);

                _nopcommerceServiceHost.Open();
                WriteLogEntriesForService(_nopcommerceServiceHost);

                _mediaJournalistsServiceHost.Open();
                WriteLogEntriesForService(_mediaJournalistsServiceHost);

                // Configure AutoMapper.
                AutoMapperConfigurator.Configure();

                _logger.Info("Service host started.");
            }
            catch (Exception ex) {
                _logger.ErrorException("Failed to start the service.", ex);
                Stop();
            }
        }

        protected override void OnStop() {
            try {
                _logger.Info("Stopping service host.");

                _chargeBeeServiceHost.Close();
                _customerServiceHost.Close();
                _mediaContactsServiceHost.Close();
                _fileServiceHost.Close();
                _generalServiceHost.Close();
                _geographicServiceHost.Close();
                _listServiceHost.Close();
                _listEditServiceHost.Close();
                _releaseServiceHost.Close();
                _monitoringServiceHost.Close();
                _nopcommerceServiceHost.Close();
                _mediaJournalistsServiceHost.Close();

                _logger.Info("Service host stopped.");
            }
            catch (Exception ex) {
                _logger.ErrorException("Failed to stop the service normally.", ex);
                Stop();
            }
        }

        private void WriteLogEntriesForService(ServiceHost servHost) {
            _logger.Info(string.Format("Endpoints {0}", servHost.Description.Endpoints.Count));

            foreach (System.ServiceModel.Description.ServiceEndpoint endpoint in servHost.Description.Endpoints) {
                _logger.Info(string.Format("  Endpoint: {0}", endpoint.Address));
            }
        }

    }
}
