﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using Ninject.Modules;

namespace Medianet.MedianetServiceHost
{
    class MedianetServiceModule : NinjectModule
    {
        private readonly string _serviceName;
        private readonly string _serviceDescription;

        public MedianetServiceModule() {
            // Read config file.
            _serviceName = ConfigurationManager.AppSettings["ServiceName"] ?? "Medianet Service Host";
            _serviceDescription = ConfigurationManager.AppSettings["ServiceDescription"] ?? "Medianet Service Host";
        }

        public override void Load() {
            Bind<MedianetServiceModule>().ToSelf();
            Bind<MedianetServiceInstaller>()
                .ToSelf()
                .WithConstructorArgument("serviceName", _serviceName)
                .WithConstructorArgument("serviceDescription", _serviceDescription);
        }
    }
}
