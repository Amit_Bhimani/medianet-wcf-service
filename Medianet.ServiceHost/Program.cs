﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using Ninject;
using System.ServiceProcess;
using System.Configuration;
using System.Net.Mail;

namespace Medianet.MedianetServiceHost
{
    class Program
    {
        private static Logger _logger;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args) {
            _logger = LogManager.GetCurrentClassLogger();
            _logger.Debug("Started main method.");

            // At least log unhandled exceptions.
            var currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += UnhandledExceptionHandler;

            var kernel = new StandardKernel(new MedianetServiceModule());

            try {
                if (args.Length == 1) {
                    var installer = kernel.Get<MedianetServiceInstaller>();

                    switch (args[0].ToLower()) {
                        case "-install":
                            installer.InstallService();
                            break;
                        case "-uninstall":
                            installer.StopService();
                            installer.UninstallService();
                            break;
                    }
                }
                else {
                    // Start the service.
                    ServiceBase.Run(kernel.Get<MedianetService>());
                }
            }
            catch (Exception ex) {
                _logger.ErrorException("Exception was raised.", ex);
            }

            _logger.Debug("Finished main method.");
        }

        /// <summary>
        /// Method to handle unhandled exceptions.
        /// </summary>
        static void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs args) {
            if (args != null && args.ExceptionObject != null) {
                var e = args.ExceptionObject as Exception;
                _logger.ErrorException("An unhandled exception was raised at the top level.", e);
            }

            var smtpServer = ConfigurationManager.AppSettings["MailServer"] ?? "mail-relay.aap.com.au";
            var client = new SmtpClient(smtpServer);
            var defaultMailToAddresses = "kkumar@aap.com.au, mbridge@aap.com.au, durquhart@aap.com.au";
            var configMailToAddresses = "";

            using (var message = new MailMessage()) {
                _logger.Debug("Sending email report.");

                if (string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["MailToAddresses"])) {
                    //log error
                    _logger.ErrorException("MailToAddresses aapsetting is either null or empty.", new FormatException("MailToAddresses aapsetting is either null or empty. Default mail addresses being used now are: " + defaultMailToAddresses + ". Please add MailToAddresses in the app.config."));
                }
                else {
                    configMailToAddresses = ConfigurationManager.AppSettings["MailToAddresses"];

                    try {
                        message.To.Add(configMailToAddresses);
                    }
                    catch (Exception ex) {
                        message.To.Add(defaultMailToAddresses);
                        _logger.ErrorException("MailToAddresses appsetting is not a valid csv. Default mail addresses being used now are: " + defaultMailToAddresses + ". Please fix MailToAddresses in the app.config.", ex);
                    }
                }

                message.From = new MailAddress(ConfigurationManager.AppSettings["MailFromAddresses"] ?? "medianet@aap.com.au");
                message.Subject = ConfigurationManager.AppSettings["MailMessageSubject"] ?? "MedianetServiceHost Crash Report";
                message.Body = ConfigurationManager.AppSettings["MailMessaegBodyPrefix"] ?? "Please restart the medianetServiceHost service and consult logs.";

                try {
                    client.Send(message);
                }
                catch (Exception ex) {
                    _logger.ErrorException("Failed to send email.", ex);
                }
            }
        }
    }
}
