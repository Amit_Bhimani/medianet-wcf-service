﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Medianet.Service;
using Medianet.Service.Contracts;
using Medianet.Service.Dto;
using Medianet.Service.Common;

namespace Medianet.Test
{
    /// <summary>
    /// Summary description for AutomapperTest
    /// </summary>
    [TestClass]
    public class AutomapperTest
    {
        public AutomapperTest() {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes

        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext) {
            if (!AutoMapperConfigurator.IsInitialized)
                AutoMapperConfigurator.Configure();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void AAPEnvironment_MappingWorks() {
            AAPEnvironment entity1;
            AAPEnvironment entity2;
            Model.Entities.AAPEnvironment dbEntity;

            entity1 = CreateAAPEnvironment();

            dbEntity = AutoMapper.Mapper.Map<AAPEnvironment, Model.Entities.AAPEnvironment>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.AAPEnvironment, AAPEnvironment>(dbEntity);

            Assert.IsTrue(ObjectHelper<AAPEnvironment>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.Application = "different";
            Assert.IsFalse(ObjectHelper<AAPEnvironment>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void City_MappingWorks() {
            City entity1;
            City entity2;
            Model.Entities.City dbEntity;

            entity1 = CreateCity();

            dbEntity = AutoMapper.Mapper.Map<City, Model.Entities.City>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.City, City>(dbEntity);

            Assert.IsTrue(ObjectHelper<City>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.Name = "different";
            Assert.IsFalse(ObjectHelper<City>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void Continent_MappingWorks() {
            Continent entity1;
            Continent entity2;
            Model.Entities.Continent dbEntity;

            entity1 = CreateContinent();

            dbEntity = AutoMapper.Mapper.Map<Continent, Model.Entities.Continent>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.Continent, Continent>(dbEntity);

            Assert.IsTrue(ObjectHelper<Continent>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.Name = "different";
            Assert.IsFalse(ObjectHelper<Continent>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void Country_MappingWorks() {
            Country entity1;
            Country entity2;
            Model.Entities.Country dbEntity;

            entity1 = CreateCountry();

            dbEntity = AutoMapper.Mapper.Map<Country, Model.Entities.Country>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.Country, Country>(dbEntity);

            Assert.IsTrue(ObjectHelper<Country>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.Name = "different";
            Assert.IsFalse(ObjectHelper<Country>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void Customer_MappingWorks()
        {
            Customer entity1;
            Customer entity2;
            Model.Entities.Customer dbEntity;

            entity1 = CreateCustomer();

            dbEntity = AutoMapper.Mapper.Map<Customer, Model.Entities.Customer>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.Customer, Customer>(dbEntity);

            Assert.IsTrue(ObjectHelper<Customer>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            Assert.IsFalse(ObjectHelper<Customer>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void CustomerBillingCode_MappingWorks()
        {
            CustomerBillingCode entity1;
            CustomerBillingCode entity2;
            Model.Entities.CustomerBillingCode dbEntity;

            entity1 = CreateCustomerBillingCode();

            dbEntity = AutoMapper.Mapper.Map<CustomerBillingCode, Model.Entities.CustomerBillingCode>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.CustomerBillingCode, CustomerBillingCode>(dbEntity);

            Assert.IsTrue(ObjectHelper<CustomerBillingCode>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.Name = "different";
            Assert.IsFalse(ObjectHelper<CustomerBillingCode>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void CustomerRegion_MappingWorks()
        {
            CustomerRegion entity1;
            CustomerRegion entity2;
            Model.Entities.CustomerRegion dbEntity;

            entity1 = CreateCustomerRegion();

            dbEntity = AutoMapper.Mapper.Map<CustomerRegion, Model.Entities.CustomerRegion>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.CustomerRegion, CustomerRegion>(dbEntity);

            Assert.IsTrue(ObjectHelper<CustomerRegion>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.DebtorNumber = "different";
            Assert.IsFalse(ObjectHelper<CustomerRegion>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void DBSession_MappingWorks() {
            DBSession entity1;
            DBSession entity2;
            Model.Entities.DBSession dbEntity;

            entity1 = CreateDBSession();

            dbEntity = AutoMapper.Mapper.Map<DBSession, Model.Entities.DBSession>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.DBSession, DBSession>(dbEntity);

            Assert.IsTrue(ObjectHelper<DBSession>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.DebtorNumber = "different";
            Assert.IsFalse(ObjectHelper<DBSession>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void Document_MappingWorks() {
            Document entity1;
            Document entity2;
            Model.Entities.Document dbEntity;

            entity1 = CreateDocument();

            dbEntity = AutoMapper.Mapper.Map<Document, Model.Entities.Document>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.Document, Document>(dbEntity);

            Assert.IsTrue(ObjectHelper<Document>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.ContentType = "different";
            Assert.IsFalse(ObjectHelper<Document>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void InboundFile_MappingWorks() {
            InboundFile entity1;
            InboundFile entity2;
            Model.Entities.InboundFile dbEntity;

            entity1 = CreateInboundFile();

            dbEntity = AutoMapper.Mapper.Map<InboundFile, Model.Entities.InboundFile>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.InboundFile, InboundFile>(dbEntity);

            Assert.IsTrue(ObjectHelper<InboundFile>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.FileExtension = "different";
            Assert.IsFalse(ObjectHelper<InboundFile>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void ListCategory_MappingWorks() {
            ListCategory entity1;
            ListCategory entity2;
            Model.Entities.ListCategory dbEntity;

            entity1 = CreateListCategory();

            dbEntity = AutoMapper.Mapper.Map<ListCategory, Model.Entities.ListCategory>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.ListCategory, ListCategory>(dbEntity);

            Assert.IsTrue(ObjectHelper<ListCategory>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.Comment = "different";
            Assert.IsFalse(ObjectHelper<ListCategory>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void ListComment_MappingWorks() {
            ListComment entity1;
            ListComment entity2;
            Model.Entities.ListComment dbEntity;

            entity1 = CreateListComment();

            dbEntity = AutoMapper.Mapper.Map<ListComment, Model.Entities.ListComment>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.ListComment, ListComment>(dbEntity);

            Assert.IsTrue(ObjectHelper<ListComment>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.Comment = "different";
            Assert.IsFalse(ObjectHelper<ListComment>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void ListDistribution_MappingWorks() {
            ListDistribution entity1;
            ListDistribution entity2;
            Model.Entities.ListDistribution dbEntity;

            entity1 = CreateListDistribution();

            dbEntity = AutoMapper.Mapper.Map<ListDistribution, Model.Entities.ListDistribution>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.ListDistribution, ListDistribution>(dbEntity);

            Assert.IsTrue(ObjectHelper<ListDistribution>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.OutletName = "different";
            Assert.IsFalse(ObjectHelper<ListDistribution>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void ListTicker_MappingWorks() {
            ListTicker entity1;
            ListTicker entity2;
            Model.Entities.ListTicker dbEntity;

            entity1 = CreateListTicker();

            dbEntity = AutoMapper.Mapper.Map<ListTicker, Model.Entities.ListTicker>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.ListTicker, ListTicker>(dbEntity);

            Assert.IsTrue(ObjectHelper<ListTicker>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.ListCategoryName = "different";
            Assert.IsFalse(ObjectHelper<ListTicker>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void MediaContact_MappingWorks() {
            MediaContact entity1;
            MediaContact entity2;
            Model.Entities.MediaContact dbEntity;

            entity1 = CreateMediaContact();

            dbEntity = AutoMapper.Mapper.Map<MediaContact, Model.Entities.MediaContact>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.MediaContact, MediaContact>(dbEntity);

            Assert.IsTrue(ObjectHelper<MediaContact>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.FirstName = "different";
            Assert.IsFalse(ObjectHelper<MediaContact>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void MediaContactGroup_MappingWorks()
        {
            MediaContactGroup entity1;
            MediaContactGroup entity2;
            Model.Entities.MediaContactGroup dbEntity;

            entity1 = CreateMediaContactGroup();

            dbEntity = AutoMapper.Mapper.Map<MediaContactGroup, Model.Entities.MediaContactGroup>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.MediaContactGroup, MediaContactGroup>(dbEntity);

            Assert.IsTrue(ObjectHelper<MediaContactGroup>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.Name = "different";
            Assert.IsFalse(ObjectHelper<MediaContactGroup>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void MediaContactList_MappingWorks()
        {
            MediaContactList entity1;
            MediaContactList entity2;
            Model.Entities.MediaContactList dbEntity;

            entity1 = CreateMediaContactList();

            dbEntity = AutoMapper.Mapper.Map<MediaContactList, Model.Entities.MediaContactList>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.MediaContactList, MediaContactList>(dbEntity);

            Assert.IsTrue(ObjectHelper<MediaContactList>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.Name = "different";
            Assert.IsFalse(ObjectHelper<MediaContactList>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void MediaContactSavedSearch_MappingWorks()
        {
            MediaContactSavedSearch entity1;
            MediaContactSavedSearch entity2;
            Model.Entities.MediaContactSavedSearch dbEntity;

            entity1 = CreateMediaContactSavedSearch();

            dbEntity = AutoMapper.Mapper.Map<MediaContactSavedSearch, Model.Entities.MediaContactSavedSearch>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.MediaContactSavedSearch, MediaContactSavedSearch>(dbEntity);

            Assert.IsTrue(ObjectHelper<MediaContactSavedSearch>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.Name = "different";
            Assert.IsFalse(ObjectHelper<MediaContactSavedSearch>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void MediaContactReminderType_MappingWorks()
        {
            MediaContactReminderType entity1;
            MediaContactReminderType entity2;
            Model.Entities.MediaContactReminderType dbEntity;

            entity1 = CreateMediaContactReminderType();

            dbEntity = AutoMapper.Mapper.Map<MediaContactReminderType, Model.Entities.MediaContactReminderType>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.MediaContactReminderType, MediaContactReminderType>(dbEntity);

            Assert.IsTrue(ObjectHelper<MediaContactReminderType>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.Name = "different";
            Assert.IsFalse(ObjectHelper<MediaContactReminderType>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void MediaContactsExport_MappingWorks() {
            MediaContactsExport entity1;
            MediaContactsExport entity2;
            Model.Entities.MediaContactsExport dbEntity;

            entity1 = CreateMediaContactsExport();

            dbEntity = AutoMapper.Mapper.Map<MediaContactsExport, Model.Entities.MediaContactsExport>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.MediaContactsExport, MediaContactsExport>(dbEntity);

            Assert.IsTrue(ObjectHelper<MediaContactsExport>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.ExportFields = "different";
            Assert.IsFalse(ObjectHelper<MediaContactsExport>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void MediaContactTask_MappingWorks()
        {
            MediaContactTask entity1;
            MediaContactTask entity2;
            Model.Entities.MediaContactTask dbEntity;

            entity1 = CreateMediaContactTask();

            dbEntity = AutoMapper.Mapper.Map<MediaContactTask, Model.Entities.MediaContactTask>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.MediaContactTask, MediaContactTask>(dbEntity);

            Assert.IsTrue(ObjectHelper<MediaContactTask>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.Name = "different";
            Assert.IsFalse(ObjectHelper<MediaContactTask>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void MediaContactTaskCategory_MappingWorks()
        {
            MediaContactTaskCategory entity1;
            MediaContactTaskCategory entity2;
            Model.Entities.MediaContactTaskCategory dbEntity;

            entity1 = CreateMediaContactTaskCategory();

            dbEntity = AutoMapper.Mapper.Map<MediaContactTaskCategory, Model.Entities.MediaContactTaskCategory>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.MediaContactTaskCategory, MediaContactTaskCategory>(dbEntity);

            Assert.IsTrue(ObjectHelper<MediaContactTaskCategory>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.Name = "different";
            Assert.IsFalse(ObjectHelper<MediaContactTaskCategory>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void MediaEmployee_MappingWorks() {
            MediaEmployee entity1;
            MediaEmployee entity2;
            Model.Entities.MediaEmployee dbEntity;

            entity1 = CreateMediaEmployee();

            dbEntity = AutoMapper.Mapper.Map<MediaEmployee, Model.Entities.MediaEmployee>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.MediaEmployee, MediaEmployee>(dbEntity);

            // These get mapped to empty lists so null them out before comparing.
            entity2.Subjects = null;
            entity2.Roles = null;

            Assert.IsTrue(ObjectHelper<MediaEmployee>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.AddressLine1 = "different";
            Assert.IsFalse(ObjectHelper<MediaEmployee>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void MediaOutlet_MappingWorks() {
            MediaOutlet entity1;
            MediaOutlet entity2;
            Model.Entities.MediaOutlet dbEntity;

            entity1 = CreateMediaOutlet();

            dbEntity = AutoMapper.Mapper.Map<MediaOutlet, Model.Entities.MediaOutlet>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.MediaOutlet, MediaOutlet>(dbEntity);

            // These get mapped to empty lists so null them out before comparing.
            entity2.Subjects = null;
            entity2.WorkingLanguages = null;
            entity2.StationFormats = null;

            Assert.IsTrue(ObjectHelper<MediaOutlet>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.AddressLine1 = "different";
            Assert.IsFalse(ObjectHelper<MediaOutlet>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void MediaStatistics_MappingWorks() {
            MediaStatistics entity1;
            MediaStatistics entity2;
            Model.Entities.MediaStatistics dbEntity;

            entity1 = CreateMediaStatistics();

            dbEntity = AutoMapper.Mapper.Map<MediaStatistics, Model.Entities.MediaStatistics>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.MediaStatistics, MediaStatistics>(dbEntity);

            Assert.IsTrue(ObjectHelper<MediaStatistics>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.AustralasiaToday = 111111111;
            Assert.IsFalse(ObjectHelper<MediaStatistics>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void PackageList_MappingWorks() {
            PackageList entity1;
            PackageList entity2;
            Model.Entities.PackageList dbEntity;

            entity1 = CreatePackageList();

            dbEntity = AutoMapper.Mapper.Map<PackageList, Model.Entities.PackageList>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.PackageList, PackageList>(dbEntity);

            Assert.IsTrue(ObjectHelper<PackageList>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.AccountCode = "different";
            Assert.IsFalse(ObjectHelper<PackageList>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void Recipient_MappingWorks() {
            Recipient entity1;
            Recipient entity2;
            Model.Entities.Recipient dbEntity;

            entity1 = CreateRecipient();

            dbEntity = AutoMapper.Mapper.Map<Recipient, Model.Entities.Recipient>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.Recipient, Recipient>(dbEntity);

            Assert.IsTrue(ObjectHelper<Recipient>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.AddressLine1 = "different";
            Assert.IsFalse(ObjectHelper<Recipient>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void RegistrationValidation_MappingWorks()
        {
            RegistrationValidation entity1;
            RegistrationValidation entity2;
            Model.Entities.RegistrationValidation dbEntity;

            entity1 = CreateRegistrationValidation();

            dbEntity = AutoMapper.Mapper.Map<RegistrationValidation, Model.Entities.RegistrationValidation>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.RegistrationValidation, RegistrationValidation>(dbEntity);

            Assert.IsTrue(ObjectHelper<RegistrationValidation>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.CompanyName = "different";
            Assert.IsFalse(ObjectHelper<RegistrationValidation>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void ReleaseUnverifiedPreview_MappingWorks()
        {
            ReleaseUnverifiedPreview entity1;
            ReleaseUnverifiedPreview entity2;
            Model.Entities.ReleaseUnverifiedPreview dbEntity;

            entity1 = CreateReleaseUnverifiedPreview();

            dbEntity = AutoMapper.Mapper.Map<ReleaseUnverifiedPreview, Model.Entities.ReleaseUnverifiedPreview>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.ReleaseUnverifiedPreview, ReleaseUnverifiedPreview>(dbEntity);

            Assert.IsTrue(ObjectHelper<ReleaseUnverifiedPreview>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.Headline = "different";
            Assert.IsFalse(ObjectHelper<ReleaseUnverifiedPreview>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void SalesRegion_MappingWorks() {
            SalesRegion entity1;
            SalesRegion entity2;
            Model.Entities.SalesRegion dbEntity;

            entity1 = CreateSalesRegion();

            dbEntity = AutoMapper.Mapper.Map<SalesRegion, Model.Entities.SalesRegion>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.SalesRegion, SalesRegion>(dbEntity);

            Assert.IsTrue(ObjectHelper<SalesRegion>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.Name = "different";
            Assert.IsFalse(ObjectHelper<SalesRegion>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void ServiceList_MappingWorks() {
            ServiceList entity1;
            ServiceList entity2;
            Model.Entities.ServiceList dbEntity;

            entity1 = CreateServiceList();

            dbEntity = AutoMapper.Mapper.Map<ServiceList, Model.Entities.ServiceList>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.ServiceList, ServiceList>(dbEntity);

            Assert.IsTrue(ObjectHelper<ServiceList>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.AccountCode = "different";
            Assert.IsFalse(ObjectHelper<ServiceList>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void State_MappingWorks() {
            State entity1;
            State entity2;
            Model.Entities.State dbEntity;

            entity1 = CreateState();

            dbEntity = AutoMapper.Mapper.Map<State, Model.Entities.State>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.State, State>(dbEntity);

            Assert.IsTrue(ObjectHelper<State>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.Name = "different";
            Assert.IsFalse(ObjectHelper<State>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void TempFile_MappingWorks() {
            TempFile entity1;
            TempFile entity2;
            Model.Entities.TempFile dbEntity;

            entity1 = CreateTempFile();

            dbEntity = AutoMapper.Mapper.Map<TempFile, Model.Entities.TempFile>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.TempFile, TempFile>(dbEntity);

            Assert.IsTrue(ObjectHelper<TempFile>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.Id = 593757;
            Assert.IsFalse(ObjectHelper<TempFile>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void TopPerformingList_MappingWorks()
        {
            TopPerformingList entity1;
            TopPerformingList entity2;
            Model.Entities.TopPerformingList dbEntity;

            entity1 = CreateTopPerformingList();

            dbEntity = AutoMapper.Mapper.Map<TopPerformingList, Model.Entities.TopPerformingList>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.TopPerformingList, TopPerformingList>(dbEntity);

            Assert.IsTrue(ObjectHelper<TopPerformingList>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.ServiceId = 593757;
            Assert.IsFalse(ObjectHelper<TopPerformingList>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void Transaction_MappingWorks() {
            Transaction entity1;
            Transaction entity2;
            Model.Entities.Transaction dbEntity;

            entity1 = CreateTransaction();

            dbEntity = AutoMapper.Mapper.Map<Transaction, Model.Entities.Transaction>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.Transaction, Transaction>(dbEntity);

            Assert.IsTrue(ObjectHelper<Transaction>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.AccountCode = "different";
            Assert.IsFalse(ObjectHelper<Transaction>.Compare(entity1, entity2) == 0);
        }

        //[TestMethod]
        //public void TransactionQuoteResponse_MappingWorks() {
        //    TransactionQuoteResponse entity1;
        //    TransactionQuoteResponse entity2;
        //    MNPricingService.TransactionPriceDetail dbEntity;

        //    entity1 = CreateWebCategory();

        //    dbEntity = AutoMapper.Mapper.Map<TransactionQuoteResponse, MNPricingService.TransactionPriceDetail>(entity1);
        //    entity2 = AutoMapper.Mapper.Map<MNPricingService.TransactionPriceDetail, TransactionQuoteResponse>(dbEntity);

        //    Assert.IsTrue(ObjectHelper<TransactionQuoteResponse>.Compare(entity1, entity2) == 0);

        //    // Just to make sure the comparer works change a value and compare again.
        //    entity2.AccountCode = "different";
        //    Assert.IsFalse(ObjectHelper<TransactionQuoteResponse>.Compare(entity1, entity2) == 0);
        //}

        [TestMethod]
        public void User_MappingWorks() {
            User entity1;
            User entity2;
            Model.Entities.User dbEntity;

            entity1 = CreateUser();

            dbEntity = AutoMapper.Mapper.Map<User, Model.Entities.User>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.User, User>(dbEntity);

            Assert.IsTrue(ObjectHelper<User>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.ShortName = "different";
            Assert.IsFalse(ObjectHelper<User>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void WebCategory_MappingWorks() {
            WebCategory entity1;
            WebCategory entity2;
            Model.Entities.WebCategory dbEntity;

            entity1 = CreateWebCategory();

            dbEntity = AutoMapper.Mapper.Map<WebCategory, Model.Entities.WebCategory>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.WebCategory, WebCategory>(dbEntity);

            Assert.IsTrue(ObjectHelper<WebCategory>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.Name = "different";
            Assert.IsFalse(ObjectHelper<WebCategory>.Compare(entity1, entity2) == 0);
        }

        [TestMethod]
        public void WebRelease_MappingWorks() {
            WebRelease entity1;
            WebRelease entity2;
            Model.Entities.WebRelease dbEntity;

            entity1 = CreateWebRelease();

            dbEntity = AutoMapper.Mapper.Map<WebRelease, Model.Entities.WebRelease>(entity1);
            entity2 = AutoMapper.Mapper.Map<Model.Entities.WebRelease, WebRelease>(dbEntity);

            Assert.IsTrue(ObjectHelper<WebRelease>.Compare(entity1, entity2) == 0);

            // Just to make sure the comparer works change a value and compare again.
            entity2.Headline = "different";
            Assert.IsFalse(ObjectHelper<WebRelease>.Compare(entity1, entity2) == 0);
        }

        #region Private methods

        private AAPEnvironment CreateAAPEnvironment() {
            return new AAPEnvironment() {
                UserId = 5836,
                Application = "Application",
                Type = "Type",
                Value = "Value",
                Description = "Description"
            };
        }

        private City CreateCity() {
            return new City() {
                Id = 5836,
                Name = "Name",
                StateId = 745,
                CountryId = 3957,
                ContinentId = 9573
            };
        }

        private Continent CreateContinent() {
            return new Continent() {
                Id = 5836,
                Name = "Name",
                DataModuleId = 745
            };
        }

        private Country CreateCountry() {
            return new Country() {
                Id = 5836,
                Name = "Name",
                ContinentId = 9573,
                DataModuleId = 745
            };
        }

        private Customer CreateCustomer()
        {
            return new Customer() {
                DebtorNumber = "DebtorNumber",
                Name = "Name",
                LogonName = "LogonName",
                FilerCode = "FilerCode",
                PIN = "PIN",
                SalesEmailAddress = "SalesEmailAddress",
                DefaultCoverpageId = 5387,
                DefaultOriginatorEmailAddress = "DefaultOriginatorEmailAddress",
                ReportSendMethod = ReportType.Both,
                ReportResultsToSend = ResultsType.All,
                ShouldSendResultsData = true,
                OptOutMethod = OptoutType.Automatic,
                ShouldSendOptOutForFax = true,
                ShouldSendOptOutForEmail = false,
                ShouldSendOptOutForSMS = true,
                TimezoneCode = "Timezone",
                Timezone = null,    // This doesn't get mapped back to a db object so leave it at null.
                AccountType = CustomerBillingType.InvoicedNotApproved,
                MustUseCreditcard = false,
                MediaDirectorySystem = MediaDirectoryType.MediAtlas,
                MediaContactsLicenseCount = 1495,
                Comment = "Comment",
                CanExportContacts = false,
                IsVerified = true, // This does not get mapped as it is based off the DebtorNumber. Leave it as true.
                SalesRegions = new List<CustomerRegion>(),
                TrendWatcherExpiryDate = DateTime.Now.AddDays(745),
                CreatedDate = DateTime.Now.AddMonths(2),
                CreatedByUserId = 45778,
                LastModifiedDate = DateTime.Now.AddMonths(3),
                LastModifiedByUserId = 39568,
                RowStatus = RowStatusType.Cancelled,
                DefaultSearchCountry = 0
            };
        }

        private CustomerBillingCode CreateCustomerBillingCode()
        {
            return new CustomerBillingCode()
            {
                Id = 896637,
                DebtorNumber = "DebtorNumber",
                Name = "Name",
                SequenceNumber = 456
            };
        }

        private CustomerRegion CreateCustomerRegion()
        {
            return new CustomerRegion()
            {
                RegionId = 45894,
                DebtorNumber = "DebtorNumber"
            };
        }

        private DBSession CreateDBSession() {
            return new DBSession() {
                Key = "896637",
                UserId = 5836,
                DebtorNumber = "DebtorNumber",
                System = SystemType.Contacts,
                RowStatus = RowStatusType.Deleted,
                LoginDate = DateTime.Now.AddMonths(1),
                LastAccessedDate = DateTime.Now.AddMonths(2),
                Customer = CreateCustomer(),
                User = CreateUser()
            };
        }

        private Document CreateDocument() {
            return new Document() {
                Id = 5836,
                ReleaseId = 36434,
                SequenceNumber = 8645,
                FileExtension = "FileExtension",
                IsDerived = true,
                OriginalFilename = "OriginalFilename",
                Description = "Description",
                CanDistributeViaEmailBody = true,
                CanDistributeViaEmailAttachment = false,
                CanDistributeViaWire = true,
                CanDistributeViaVoice = false,
                CanDistributeViaWeb = true,
                CanDistributeViaSMS = false,
                IsMailMergeDataSource = true,
                ItemCount = 104734,
                Width = 856365,
                Height = 2305,
                IsTabulated = true,
                MediaReferenceId = "MediaReferenceId",
                ContentType = "ContentType",
                CreatedDate = DateTime.Now.AddMonths(1),
                CreatedByUserId = 948566,
                LastModifiedDate = DateTime.Now.AddMonths(2),
                LastModifiedByUserId = 37678
                //MessageFileId = 98753,
                //TempFileId = 975443,
                //InternalFilePath = "InternalFilePath",
                //InternalFileName = "InternalFileName"
            };
        }

        private InboundFile CreateInboundFile() {
            return new InboundFile() {
                Id = 23486,
                Caller = "Caller",
                DistributionType = DistributionType.Internet,
                FileExtension = "FileExtension",
                OriginalFilename = "OriginalFilename",
                OwnerUserId = 5763,
                InboundNumber = "InboundNumber",
                ForwardedDate = DateTime.Now.AddDays(356),
                ForwardedCount = 78556,
                ItemCount = 2345,
                FinanceFileId = 86633,
                FileSize = 906747,
                Length = 3458,
                Status = InboundFileStatusType.Ready,
                Type = MessageType.FaxRelease,
                CreatedDate = DateTime.Now.AddDays(786),
                CreatedByUserId = 7556,
                LastModifiedDate = DateTime.Now.AddDays(12),
                LastModifiedByUserId = 4565
            };
        }

        private ListCategory CreateListCategory() {
            return new ListCategory() {
                Id = 8976,
                Name = "Name",
                Comment = "Comment",
                ParentId = 498673,
                SequenceNumber = 8457356,
                IsVisible = true,
                IsRadioListCategory = false
            };
        }

        private ListComment CreateListComment() {
            return new ListComment() {
                ServiceId = 9578346,
                PackageId = 237576,
                Comment = "Comment",
                CreatedDate = DateTime.Now.AddMonths(1),
                CreatedByUserId = 204584
            };
        }

        private ListDistribution CreateListDistribution() {
            return new ListDistribution() {
                Id = 4736,
                ServiceId = 184763,
                OutletName = "OutletName"
            };
        }

        private ListTicker CreateListTicker() {
            return new ListTicker() {
                ListCategoryId = 7857,
                ListCategoryParentSequenceNumber = 9684,
                ListCategorySequenceNumber = 27453,
                ListCategoryName = "ListCategoryName",
                ListId = 34945,
                ListSequenceNumber = 9045,
                ListDistributionType = DistributionType.Unknown,
                ListDescription = "ListDescription"
            };
        }

        private MediaContact CreateMediaContact() {
            return new MediaContact() {
                Id = "AAPC1234",
                Prefix = "Prefix",
                FirstName = "FirstName",
                MiddleName = "MiddleName",
                LastName = "LastName",
                Profile = string.Empty,
                InternalNotes = "InternalNotes",
                CreatedDate = DateTime.Now,
                LastModifiedDate = DateTime.Now.AddDays(43),
                LastModifiedByUserId = 4583,
                LockedByUserId = 4583,
                DeletedDate = DateTime.Now.AddDays(43),
                RowStatus = ContactRowStatusType.Deleted
            };
        }

        private MediaContactGroup CreateMediaContactGroup()
        {
            return new MediaContactGroup()
            {
                Id = 458973,
                Name = "Name",
                GroupType = MediaContactGroupType.Task,
                CreatedByUserId = 4583,
                IsActive = true,
                CreatedDate = DateTime.Now,
                LastModifiedDate = DateTime.Now.AddDays(43)
            };
        }

        private MediaContactList CreateMediaContactList()
        {
            return new MediaContactList()
            {
                Id = 458973,
                Name = "Name",
                GroupId = 457646,
                OwnerUserId = 4583,
                IsPrivate = false,
                IsActive = true,
                CreatedDate = DateTime.Now,
                LastModifiedDate = DateTime.Now.AddDays(43)
            };
        }

        private MediaContactSavedSearch CreateMediaContactSavedSearch()
        {
            return new MediaContactSavedSearch()
            {
                Id = 458973,
                Name = "Name",
                GroupId = 345534,
                OwnerUserId = 4583,
                Context = MediaContactSearchContext.Both,
                SearchCriteria = "SearchCriteria",
                IsPrivate = false,
                IsActive = true,
                CreatedDate = DateTime.Now,
                LastModifiedDate = DateTime.Now.AddDays(43)
            };
        }

        private MediaContactReminderType CreateMediaContactReminderType()
        {
            return new MediaContactReminderType()
            {
                Id = 458973,
                Name = "Name",
                Minutes = 4583,
                IsActive = true,
                CreatedDate = DateTime.Now
            };
        }

        private MediaContactsExport CreateMediaContactsExport() {
            return new MediaContactsExport() {
                Id = 7664,
                Name = "Name",
                DebtorNumber = "DebtorNumber",
                UserId = 5836,
                ExportFields = "ExportFields",
                IsPrivate = true,
                IsDeleted = false,
                ExportType = MediaContactsExportType.Lists,
                CreatedDate = DateTime.Now.AddHours(12),
                CreatedByUserId = 4757356,
                LastModifiedDate = DateTime.Now.AddHours(4),
                LastModifiedByUserId = 94572
            };
        }

        private MediaContactTask CreateMediaContactTask()
        {
            return new MediaContactTask()
            {
                Id = 458973,
                Name = "Name",
                Description = "Description",
                GroupId = 457646,
                ReminderTypeId = 64345,
                NotificationType = MediaContactNotificationType.Email,
                DueDate = DateTime.Now.AddHours(455),
                StatusType = MediaContactTaskStatusType.OnHold,
                CategoryId = 84343,
                IsEmailSent = true,
                IsSmsSent = false,
                OwnerUserId = 4583,
                IsPrivate = false,
                IsActive = true,
                CreatedDate = DateTime.Now,
                CreatedByUserId = 135853,
                LastModifiedDate = DateTime.Now.AddDays(43)
            };
        }

        private MediaContactTaskCategory CreateMediaContactTaskCategory()
        {
            return new MediaContactTaskCategory()
            {
                Id = 458973,
                Name = "Name",
                OwnerUserId = 4583,
                IsActive = true,
                CreatedDate = DateTime.Now,
                LastModifiedDate = DateTime.Now.AddDays(43)
            };
        }

        private MediaEmployee CreateMediaEmployee() {
            return new MediaEmployee() {
                OutletId = "AAPO1234",
                ContactId = "AAPC1234",
                JobTitle = "JobTitle",
                AddressLine1 = "AddressLine1",
                AddressLine2 = "AddressLine2",
                CityId = null, //This is not part of the DB so it won't come back
                PhoneNumber = "PhoneNumber",
                FaxNumber = "FaxNumber",
                MobileNumber = "MobileNumber",
                EmailAddress = "EmailAddress",
                PreferredDeliveryMethod = PreferredDeliveryType.Fax,
                OutletName = "OutletName",
                CountryId = 564,
                IsPrimaryNewsContact = true,
                HasReceivedLegals = false,
                TrackingReference = "TrackingReference",
                BlogURL = "BlogURL",
                CreatedDate = DateTime.Now,
                LastModifiedDate = DateTime.Now.AddDays(43),
                LastModifiedByUserId = 4583,
                DeletedDate = DateTime.Now.AddDays(43),
                RowStatus = EmployeeRowStatusType.Deleted
            };
        }

        private MediaOutlet CreateMediaOutlet() {
            return new MediaOutlet() {
                Id = "AAPO1234",
                Name = "Name",
                HasParent = true,
                ParentId = "ParentId",
                OwnershipCompany = "OwnershipCompany",
                AddressLine1 = "AddressLine1",
                AddressLine2 = "AddressLine2",
                CityId = null, //This is not part of the DB so it won't come back
                CountryId = 564,
                PostalAddressLine1 = "AddressLine1",
                PostalAddressLine2 = "AddressLine2",
                PostalCityId = null, //This is not part of the DB so it won't come back
                PostalCountryId = 564,
                PhoneNumber = "PhoneNumber",
                FaxNumber = "FaxNumber",
                EmailAddress = "EmailAddress",
                WebSite = "WebSite",
                Facebook = "Facebook",
                LinkedIn = "LinkedIn",
                Twitter = "Twitter",
                BlogURL = "BlogURL",
                OutletTypeId = 562,
                ProductTypeId = 560,
                PreferredDeliveryMethod = PreferredDeliveryType.Fax,
                Circulation = 345,
                Audience = 9489,
                CallLetters = "CallLetters",
                YearEstablished = "YearEstablished",
                StationFrequency = "StationFrequency",
                CopyPrice = "CopyPrice",
                FrequencyId = 843,
                NewsFocusId = 154,
                TrackingReference = "TrackingReference",
                SendToOMA = true,
                P16 = "P16",
                M16 = "M16",
                F16 = "F16",
                GBs = "GBs",
                ABs = "ABs",
                Profile = string.Empty,
                InternalNotes = "InternalNotes",
                CreatedDate = DateTime.Now,
                LastModifiedDate = DateTime.Now.AddDays(43),
                LastModifiedByUserId = 4583,
                LockedByUserId = 4583,
                DeletedDate = DateTime.Now.AddDays(43),
                RowStatus = OutletRowStatusType.Deleted
            };
        }

        private MediaStatistics CreateMediaStatistics() {
            return new MediaStatistics() {
                AustralasiaToday = 49834,
                AustralasiaLastWeek = 908489,
                AustralasiaYearToDate = 2485,
                InternationalToday = 7695,
                InternationalLastWeek = 889573,
                InternationalYearToDate = 9068473
            };
        }

        private PackageList CreatePackageList() {
            return new PackageList() {
                Id = 4736,
                DebtorNumber = "DebtorNumber",
                System = SystemType.Journalists,
                SequenceNumber = 2495,
                TransactionType = DistributionType.Voice,
                AccountCode = "AccountCode",
                SelectionDescription = "SelectionDescription",
                ListNumber = 848273,
                RowStatus = RowStatusType.Cancelled,
                ShouldCharge = true,
                ShouldDistribute = false,
                ShouldSendResultsToOwner = true,
                ShouldUseSpecialAddressText = false,
                Introduction = "Introduction",
                Category1Id = 290584,
                Category2Id = 857632,
                IsVisible = true,
                Cost = 0.345M,
                CoverPageId = 90583,
                IsTopValue = true,
                CreatedDate = DateTime.Now.AddHours(34),
                CreatedByUserId = 284742,
                LastModifiedDate = DateTime.Now.AddHours(7),
                LastModifiedByUserId = 794578
            };
        }

        private Recipient CreateRecipient() {
            return new Recipient() {
                Id = 93856,
                ReferenceId = "ReferenceId",
                Company = "Company",
                JobTitle = "JobTitle",
                Title = "Title",
                FirstName = "FirstName",
                MiddleName = "MiddleName",
                LastName = "LastName",
                PhoneNumber = "PhoneNumber",
                AddressLine1 = "AddressLine1",
                AddressLine2 = "AddressLine2",
                Address = "Address"
            };
        }

        private RegistrationValidation CreateRegistrationValidation()
        {
            return new RegistrationValidation()
            {
                CompanyName = "CompanyName",
                CreatedDate = DateTime.Now,
                EmailAddress = "acme@aap.com.au",
                FirstName = "Acme",
                LastName = "Rabbit",
                Password = "abc123",
                TelephoneNumber = "94811111",
                System = SystemType.Medianet,
                Token = Guid.NewGuid()
            };
        }

        private ReleaseUnverifiedPreview CreateReleaseUnverifiedPreview()
        {
            return new ReleaseUnverifiedPreview()
            {
                Headline = "Headline",
                Organisation = "Company Name",
                ReleaseTempFileId = 99,
                WebCategoryId = 9,
                WhitePaperTempFileId = 98,
                Token = Guid.NewGuid()
            };
        }

        private SalesRegion CreateSalesRegion()
        {
            return new SalesRegion()
            {
                Id = 5836,
                Name = "Name",
                SalesEmail = "SalesEmail",
                AccountManagerName = "AccountManagerName",
                AccountManagerImageUrl = "AccountManagerImageUrl",
                CreatedDate = DateTime.Now,
                CreatedByUserId = 632,
                LastModifiedDate = DateTime.Now,
                LastModifiedByUserId = 285
            };
        }

        private ServiceList CreateServiceList() {
            return new ServiceList() {
                Id = 10473,
                DebtorNumber = "DebtorNumber",
                System = SystemType.Medianet,
                DistributionType = Service.Common.DistributionType.Package,
                SequenceNumber = 678,
                TransactionType = DistributionType.Voice,
                AccountCode = "AccountCode",
                SelectionDescription = "SelectionDescription",
                ListNumber = 234,
                AddressCount = 1534,
                ShouldUpdateAddresses = false,
                ShouldCharge = true,
                ShouldDistribute = false,
                ShouldSendResultsToOwner = true,
                ShouldUseSpecialAddressText = false,
                PageAdjustment = 14,
                Introduction = "Introduction",
                Destinations = "Destinations",
                CategoryId = 145,
                IsVisible = true,
                EditorialCategory = "EditorialCategory",
                IsOnlineDB = false,
                CoverPageId = 1367,
                CreatedDate = DateTime.Now.AddHours(34),
                CreatedByUserId = 284742,
                LastModifiedDate = DateTime.Now.AddHours(7),
                LastModifiedByUserId = 2474
            };
        }

        private State CreateState() {
            return new State() {
                Id = 5836,
                Name = "Name",
                CountryId = 9573
            };
        }

        private TempFile CreateTempFile() {
            return new TempFile() {
                Id = 5836,
                SessionKey = "69047",
                ParentId = 483727,
                FileExtension = "FileExtension",
                OriginalFilename = "OriginalFilename",
                IsWireConversion = false,
                IsWebConversion = true,
                ContentType = "ContentType",
                FileSize = 947362,
                ItemCount = 47523,
                CreatedDate = DateTime.Now
            };
        }
        private TopPerformingList CreateTopPerformingList()
        {
            return new TopPerformingList()
            {
                Period = PeriodType.Week,
                ServiceId = 45665,
                LastPeriodTotal = 46734,
                PreviousPeriodTotal = 56454,
                PercentageImprovement = 12345,
                CreatedDate = DateTime.Now
            };
        }

        private Transaction CreateTransaction() {
            return new Transaction() {
                Id = 95684,
                ReleaseId = 958475,
                ParentTransactionId = 968474,
                SequenceNumber = 3475,
                GroupNumber = 694,
                PackageId = 478745,
                ServiceId = 5947,
                DebtorNumber = "DebtorNumber",
                System = SystemType.Admin,
                DistributionType = DistributionType.Voice,
                IsSingleRecipient = true,
                IsMailMerge = false,
                Status = TransactionStatusType.New,
                TransactionType = DistributionType.Unknown,
                AccountCode = "AccountCode",
                SelectionDescription = "SelectionDescription",
                PackageDescription = "PackageDescription",
                ListNumber = 69574,
                AddressCount = 733,
                ShouldCharge = true,
                ShouldDistribute = false,
                DistributionSystem = DistributionSystemType.MMS,
                ShouldSendResultsToOwner = true,
                ShouldUseSpecialAddressText = false,
                PageAdjustment = 645,
                Introduction = "Introduction",
                SingleRecipientAddress = "SingleRecipientAddress",
                Destinations = "Destinations",
                EditorialCategory = "EditorialCategory",
                CoverPageId = 457833,
                CreatedDate = DateTime.Now,
                CreatedByUserId = 5742,
                LastModifiedDate = DateTime.Now.AddDays(43),
                LastModifiedByUserId = 4583
            };
        }

        private User CreateUser() {
            return new User() {
                Id = 95684,
                LogonName = "LogonName",
                DebtorNumber = "DebtorNumber",
                OutletID = "OutletID",
                ShortName = "ShortName",
                IVRPin = "IVRPin",
                DefaultBillingRef = "DefaultBillingRef",
                DefaultJobPriority = ReleasePriorityType.High,
                ShouldDeduplicateJobs = false,
                MustChangePassword = false,
                FirstName = "FirstName",
                LastName = "LastName",
                EmailAddress = "EmailAddress",
                FaxNumber = "FaxNumber",
                ReportSendMethod = ReportType.Fax,
                ReportResultsToSend = ResultsType.All,
                MNUserAccessRights = AdminAccessType.Trainee,
                HasDistributeWebAccess = true,
                HasMessageConnectWebAccess = false,
                MessageConnectAccessRights = MessageConnectAccessType.None,
                HasJournalistsWebAccess = true,
                HasAdminWebAccess = false,
                HasContactsWebAccess = true,
                ContactsAccessRights = ContactsAccessType.Supervisor,
                HasContactsReportAccess = false,
                HasReleaseWatchAccess = true,
                CanSendFax = false,
                CanSendEmail = true,
                CanSendSMS = false,
                CanSendVoice = true,
                CanSendMailMerge = false,
                LastLogonDate = DateTime.Now.AddHours(45),
                LogonFailureCount = 64,
                RecentLogonCount = 466,
                CreatedDate = DateTime.Now,
                CreatedByUserId = 5742,
                LastModifiedDate = DateTime.Now.AddDays(43),
                LastModifiedByUserId = 4583,
                ExpiryToken = Guid.NewGuid(),
                UnsubscribeKey = Guid.NewGuid(),
                RowStatus = RowStatusType.Deleted
            };
        }

        private WebCategory CreateWebCategory() {
            return new WebCategory() {
                Id = 5836,
                Name = "Name",
                IsHighPriority = true
            };
        }

        private WebRelease CreateWebRelease() {
            return new WebRelease() {
                ReleaseId = 5836,
                Headline = "Headline",
                Organisation = "Organisation",
                PrimaryWebCategoryId = 67867,
                SecondaryWebCategoryId = 45765,
                ShouldShowOnPublic = true,
                ShouldShowOnJournalists = false,
                PublicDistributedDate = DateTime.Now,
                JournalistsDistributedDate = DateTime.Now.AddHours(346754),
                WasSentToJournalists = false,
                ShortUrl = "ShortUrl",
                Summary = "Summary",
                HasVideo = true,
                HasAudio = false,
                HasPhoto = true,
                HasOther = false,
                MultimediaType = MultimediaType.Multimedia,
                IsVerified = true
            };
        }

        #endregion
    }
}

public static class ObjectHelper<T>
{
    public static int Compare(T x, T y) {
        Type type = typeof(T);
        PropertyInfo[] properties = type.GetProperties(); // (BindingFlags.DeclaredOnly | BindingFlags.Public);
        FieldInfo[] fields = type.GetFields(); // (BindingFlags.DeclaredOnly | BindingFlags.Public);
        int compareValue = 0;

        foreach (PropertyInfo property in properties) {
            //IComparable valx = property.GetValue(x, null) as IComparable;
            //if (valx == null)
            //    continue;
            object valx = property.GetValue(x, null);
            IComparable valxComp = valx as IComparable;
            object valy = property.GetValue(y, null);

            if (valx == null && valy == null)
                continue;

            if (valx == null || valy == null)
                return 1;

            // We can't compare these objects so just ignore it.
            if (valxComp == null)
                continue;

            compareValue = valxComp.CompareTo(valy);
            if (compareValue != 0)
                return compareValue;
        }
        foreach (FieldInfo field in fields) {
            //IComparable valx = field.GetValue(x) as IComparable;
            //if (valx == null)
            //    continue;
            object valx = field.GetValue(x);
            IComparable valxComp = valx as IComparable;
            object valy = field.GetValue(y);

            if (valx == null && valy == null)
                continue;

            if (valx == null || valy == null)
                return 1;

            // We can't compare these objects so just ignore it.
            if (valxComp == null)
                continue;

            compareValue = valxComp.CompareTo(valy);
            if (compareValue != 0)
                return compareValue;
        }

        return compareValue;
    }
}
