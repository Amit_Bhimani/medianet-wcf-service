﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Medianet.Service;
using Medianet.Service.Contracts;
using Medianet.Service.Dto;
using Medianet.Service.Common;
using Medianet.Model;
using Medianet.Service.FaultExceptions;
using Medianet.DataLayer;
using Medianet.Service.BusinessLayer;
using Medianet.DataLayer.Repositories;

namespace Medianet.Test
{
    public class Common
    {
        public static DBSession CreateImpersonatingSession(int userId, SystemType system)
        {
            using (var uow = new UnitOfWork()) {
                var sessionBL = new DBSessionBL(uow);
                var userBL = new UserBL(uow);
                var user = userBL.Get(userId);

                return sessionBL.CreateSession(user, system, false, true, false);
            }
        }

        public static void CleanupSession(int userId, SystemType system) {
            var dbContext = new MedianetContext();
            string dbSystem = ((char)system).ToString();
            List<Model.Entities.DBSession> dbEntities = dbContext.DBSessions.Where(e => e.UserId == userId && e.System == dbSystem && e.IsImpersonating == false).ToList();

            foreach (var s in dbEntities)
                dbContext.Entry(s).State = System.Data.EntityState.Deleted;

            dbContext.SaveChanges();
        }

        public static void ResetUserReleaseCount(int userId)
        {
            var dbContext = new MedianetContext();
            var dbUser = dbContext.Users.Where(u => u.Id == userId).FirstOrDefault();
            if (dbUser != null)
            {
                dbUser.ReleaseCount = 0;
                dbContext.SaveChanges();
            }
        }

        public static string GetTrainingCourseId()
        {
            var dbContext = new MedianetContext();
            var dbEntities = dbContext.TrainingCourses.ToList();

            foreach (var course in dbEntities)
            {
                if (course.Schedules != null)
                {
                    foreach (var schedule in course.Schedules)
                    {
                        if (schedule.Enrolments != null)
                        {
                            if (schedule.Enrolments.Any(e => e.Status == 1))
                                return course.Id;
                        }
                    }
                }
            }

            return null;
        }

        #region Release generation code

        public static ReleaseQuoteRequest GenerateReleaseQuote(DBSession session) {
            //ReleaseService rService = new ReleaseService();
            ReleaseQuoteRequest release;

            release = new ReleaseQuoteRequest();
            release.DebtorNumber = session.DebtorNumber;
            release.System = SystemType.Medianet;
            release.ReleaseDescription = "My release";
            release.Priority = ReleasePriorityType.Standard;
            release.MultimediaType = MultimediaType.None;

            return release;
        }

        public static ReleaseCalculatorRequest GenerateReleaseCalculator(DBSession session) {
            //ReleaseService rService = new ReleaseService();
            ReleaseCalculatorRequest release;

            release = new ReleaseCalculatorRequest();
            release.DebtorNumber = session.DebtorNumber;
            release.System = SystemType.Medianet;
            release.Priority = ReleasePriorityType.Standard;
            release.MultimediaType = MultimediaType.None;

            return release;
        }

        public static ReleaseSummary GenerateReleaseSummary(DBSession session, bool createWebTransaction, bool IsVerifiedRelease = true)
        {
            //ReleaseService rService = new ReleaseService();
            ReleaseSummary release;

            release = new ReleaseSummary();
            release.DebtorNumber = session.DebtorNumber;
            release.System = SystemType.Medianet;
            release.ReleaseDescription = "Headline entered by user";
            release.Organisation = "Organisation entered by user";
            release.CustomerReference = "Customer Reference";
            release.BillingCode = "Billing code";
            release.HoldUntilDate = DateTime.Now.AddMonths(6);
            release.Priority = ReleasePriorityType.Standard;

            release.UseCustomerEmailAddress = true;
            if (release.UseCustomerEmailAddress) {
                release.EmailFromName = "David Urquhart";
                release.EmailFromAddress = "david.urquhart@aap.com.au";
            }
            else {
                release.EmailFromName = string.Empty;
                release.EmailFromAddress = string.Empty;
            }

            if (session.User.ReportSendMethod == ReportType.Email || session.User.ReportSendMethod == ReportType.Both)
                release.ReportEmailAddress = session.User.EmailAddress;

            release.ReportResultsToSend = session.User.ReportResultsToSend;

            release.MultimediaType = MultimediaType.None;
            release.QuoteReferenceId = 5246;
            //release.IsMailMergeRelease = false;

            if (createWebTransaction)
            {
                release.WebCategoryId = 3;
                release.IsVerified = IsVerifiedRelease;
            }
            else
                release.IsVerified = true;

            return release;
        }

        #endregion

        #region Transaction generation code

        public static TransactionSummary GenerateServiceTransaction(int serviceId, int sequenceNumber) {
            TransactionSummary trans;

            trans = new TransactionSummary();
            trans.SequenceNumber = sequenceNumber;
            trans.ServiceId = serviceId;

            return trans;
        }

        public static TransactionSummary GenerateMailmergeTransaction(DistributionType distType, int sequenceNumber) {
            TransactionSummary trans;

            trans = new TransactionSummary();
            trans.SequenceNumber = sequenceNumber;
            trans.DistributionType = distType;
            trans.IsMailMerge = true;

            return trans;
        }

        public static TransactionSummary GeneratePackageTransaction(int packageId, int sequenceNumber) {
            TransactionSummary trans;

            trans = new TransactionSummary();
            trans.SequenceNumber = sequenceNumber;
            trans.PackageId = packageId;

            return trans;
        }

        public static TransactionSummary GenerateExtraEmailRecipientTransaction(string addresss, DistributionType distType, int sequenceNumber) {
            TransactionSummary trans;

            // Example code to add an extra recipient.
            trans = new TransactionSummary();
            trans.SequenceNumber = sequenceNumber;
            trans.DistributionType = distType;
            trans.IsSingleRecipient = true;
            trans.SingleRecipientAddress = addresss;

            return trans;
        }

        public static TransactionSummary GenerateWebTransaction(int serviceId, int sequenceNumber)
        {
            TransactionSummary trans;

            // Example code to add a web transaction.
            trans = new TransactionSummary();
            trans.SequenceNumber = sequenceNumber;
            trans.DistributionType = DistributionType.Internet;
            trans.ServiceId = serviceId;

            return trans;
        }

        public static TransactionSummary GenerateAttachmentTransaction(int sequenceNumber) {
            TransactionSummary trans;

            // Example code to add a web transaction.
            trans = new TransactionSummary();
            trans.SequenceNumber = sequenceNumber;
            trans.DistributionType = DistributionType.Unknown;
            trans.IsAttachmentFee = true;

            return trans;
        }

        public static TransactionSummary GenerateTwitterTransaction(int sequenceNumber)
        {
            TransactionSummary trans;

            // Example code to add an attachment charge transaction.
            trans = new TransactionSummary();
            trans.SequenceNumber = sequenceNumber;
            trans.DistributionType = DistributionType.Twitter;
            trans.ServiceId = Medianet.Service.BusinessLayer.Common.Constants.TWITTER_SERVICE_ID;
            trans.TwitterText = "This is my tweet";
            trans.TwitterCategoryId = 4;

            return trans;
        }

        public static TransactionSummary GenerateAnrTransaction(int sequenceNumber)
        {
            TransactionSummary trans;

            // Example code to add an ANR charge transaction.
            trans = new TransactionSummary();
            trans.SequenceNumber = sequenceNumber;
            trans.DistributionType = DistributionType.Unknown;
            trans.IsAnrFee = true;

            return trans;
        }

        #endregion

        #region Document generation code

        public static DocumentSummary GenerateReleaseDocument(int sequenceNumber, DBSession session) {
            var fService = new FileService();
            string filePath;
            var tfs = new TempFileUploadRequest();
            TempFileUploadResponse tfr;
            DocumentSummary doc;

            filePath = Path.GetFullPath("TestFiles\\Release_2_pages.doc");
            tfs.ContentType = "application/msword";
            tfs.FileName = Path.GetFileName(filePath);
            tfs.SessionKey = session.Key;
            tfs.ConvertToWire = false;
            tfs.FileData = File.OpenRead(filePath);
            tfr = fService.UploadTempFileForConversion(tfs);
            tfs.FileData.Close();

            doc = new DocumentSummary();
            doc.SequenceNumber = sequenceNumber;
            doc.CanDistributeViaWeb = true;
            doc.TempFileId = tfr.TempFileId;

            return doc;
        }

        public static DocumentSummary GenerateMailmergeFaxReleaseDocument(int sequenceNumber, DBSession session) {
            var fService = new FileService();
            string filePath;
            var tfs = new TempFileUploadRequest();
            TempFileUploadResponse tfr;
            DocumentSummary doc;

            filePath = Path.GetFullPath("TestFiles\\mailmerge_2_pages.doc");
            tfs.ContentType = "application/msword";
            tfs.FileName = Path.GetFileName(filePath);
            tfs.SessionKey = session.Key;
            tfs.ConvertToWire = false;
            tfs.FileData = System.IO.File.OpenRead(filePath);
            tfr = fService.UploadTempFile(tfs);
            tfs.FileData.Close();

            doc = new DocumentSummary();
            doc.SequenceNumber = sequenceNumber;
            doc.TempFileId = tfr.TempFileId;

            return doc;
        }

        public static DocumentSummary GenerateMailmergeSmsReleaseDocument(int sequenceNumber, DBSession session) {
            var fService = new FileService();
            string filePath;
            var tfs = new TempFileUploadRequest();
            TempFileUploadResponse tfr;
            DocumentSummary doc;

            filePath = Path.GetFullPath("TestFiles\\mailmerge_sms.txt");
            tfs.ContentType = "text/plain";
            tfs.FileName = Path.GetFileName(filePath);
            tfs.SessionKey = session.Key;
            tfs.FileData = System.IO.File.OpenRead(filePath);
            tfr = fService.UploadTempFile(tfs);
            tfs.FileData.Close();

            doc = new DocumentSummary();
            doc.SequenceNumber = sequenceNumber;
            doc.CanDistributeViaSMS = true;
            doc.TempFileId = tfr.TempFileId;

            return doc;
        }

        public static DocumentSummary GenerateMailmergeFaxDataSourceDocument(int sequenceNumber, DBSession session) {
            var fService = new FileService();
            string filePath;
            var tfs = new TempFileUploadRequest();
            TempFileUploadResponse tfr;
            DocumentSummary doc;

            filePath = Path.GetFullPath("TestFiles\\mailmerge_639.csv");
            tfs.ContentType = "text/csv";
            tfs.FileName = Path.GetFileName(filePath);
            tfs.SessionKey = session.Key;
            tfs.FileData = System.IO.File.OpenRead(filePath);
            tfr = fService.UploadTempFile(tfs);
            tfs.FileData.Close();

            doc = new DocumentSummary();
            doc.SequenceNumber = sequenceNumber;
            doc.IsMailMergeDataSource = true;
            doc.TempFileId = tfr.TempFileId;

            return doc;
        }

        public static DocumentSummary GenerateMailmergeSmsDataSourceDocument(int sequenceNumber, DBSession session) {
            var fService = new FileService();
            string filePath;
            var tfs = new TempFileUploadRequest();
            TempFileUploadResponse tfr;
            DocumentSummary doc;

            filePath = Path.GetFullPath("TestFiles\\mailmerge_sms_48.csv");
            tfs.ContentType = "text/csv";
            tfs.FileName = Path.GetFileName(filePath);
            tfs.SessionKey = session.Key;
            tfs.FileData = System.IO.File.OpenRead(filePath);
            tfr = fService.UploadTempFile(tfs);
            tfs.FileData.Close();

            doc = new DocumentSummary();
            doc.SequenceNumber = sequenceNumber;
            doc.IsMailMergeDataSource = true;
            doc.TempFileId = tfr.TempFileId;

            return doc;
        }

        public static DocumentSummary GenerateInvalidMailmergeFaxDataSourceDocument(int sequenceNumber, DBSession session) {
            var fService = new FileService();
            string filePath;
            var tfs = new TempFileUploadRequest();
            TempFileUploadResponse tfr;
            DocumentSummary doc;

            filePath = Path.GetFullPath("TestFiles\\mailmerge_sms_48.csv");
            tfs.ContentType = "text/csv";
            tfs.FileName = Path.GetFileName(filePath);
            tfs.SessionKey = session.Key;
            tfs.FileData = System.IO.File.OpenRead(filePath);
            tfr = fService.UploadTempFile(tfs);
            tfs.FileData.Close();

            doc = new DocumentSummary();
            doc.SequenceNumber = sequenceNumber;
            doc.IsMailMergeDataSource = true;
            doc.TempFileId = tfr.TempFileId;

            return doc;
        }

        public static DocumentSummary GenerateAttachmentDocument(int sequenceNumber, DBSession session) {
            var fService = new FileService();
            string filePath;
            var tfs = new TempFileUploadRequest();
            TempFileUploadResponse tfr;
            DocumentSummary doc;

            filePath = Path.GetFullPath("TestFiles\\ImageTest.jpg");
            tfs.ContentType = "image/jpeg";
            tfs.FileName = Path.GetFileName(filePath);
            tfs.SessionKey = session.Key;
            tfs.FileData = System.IO.File.OpenRead(filePath);
            tfr = fService.UploadTempFile(tfs);
            tfs.FileData.Close();

            // Example code to add an attachment document.
            doc = new DocumentSummary();
            doc.SequenceNumber = sequenceNumber;
            doc.CanDistributeViaEmailAttachment = true;
            doc.Description = "Description entered by user";
            doc.TempFileId = tfr.TempFileId;

            return doc;
        }

        public static DocumentSummary GenerateWireEmailDocument(int sequenceNumber, DBSession session) {
            var fService = new FileService();
            string filePath;
            var tfs = new TempFileUploadRequest();
            TempFileUploadResponse tfr;
            DocumentSummary doc;

            filePath = Path.GetFullPath("TestFiles\\HtmlTest.html");
            tfs.ContentType = "text/html";
            tfs.FileName = Path.GetFileName(filePath);
            tfs.SessionKey = session.Key;
            tfs.ConvertToWire = true;
            tfs.FileData = System.IO.File.OpenRead(filePath);
            tfr = fService.UploadTempFileForConversion(tfs);
            tfs.FileData.Close();

            doc = new DocumentSummary();
            doc.SequenceNumber = sequenceNumber;
            doc.CanDistributeViaEmailBody = true;
            doc.CanDistributeViaWire = true;
            doc.TempFileId = tfr.TempFileId;

            return doc;
        }

        public static DocumentSummary GenerateAudioAttachmentDocument(int sequenceNumber, DBSession session) {
            var fService = new FileService();
            string filePath;
            var tfs = new TempFileUploadRequest();
            TempFileUploadResponse tfr;
            DocumentSummary doc;

            filePath = Path.GetFullPath("TestFiles\\AudioTest.WAV");
            tfs.ContentType = "audio/wav";
            tfs.FileName = Path.GetFileName(filePath);
            tfs.SessionKey = session.Key;
            tfs.FileData = System.IO.File.OpenRead(filePath);
            tfr = fService.UploadTempFile(tfs);
            tfs.FileData.Close();

            doc = new DocumentSummary();
            doc.SequenceNumber = sequenceNumber;
            doc.CanDistributeViaEmailAttachment = true;
            //doc.Description = "ANR TRANSCRIPT"; // No longer supported on Distribution website.
            doc.TempFileId = tfr.TempFileId;

            return doc;
        }

        public static DocumentSummary GenerateAudioAttachmentDocument(int messageId, int sequenceNumber, DBSession session) {
            DocumentSummary doc;

            doc = new DocumentSummary();
            doc.SequenceNumber = sequenceNumber;
            doc.CanDistributeViaEmailAttachment = true;
            doc.InboundFileId = messageId;

            return doc;
        }

        #endregion
    }
}
