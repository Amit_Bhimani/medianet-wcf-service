﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Test
{
    class Constants
    {
        public const string debtor_number_default = "10";

        public const int account_user_id = 5;
        public const string account_emailaddress = "durquhart@aap.com.au";
        public const string account_username = "heliaFathi";
        public const string account_companylogon = "mnetops";
        public const string account_password = "password.1";

        public const int account_alt_user_id = 4588;
        public const string account_alt_emailaddress = "ksammut@aap.com.au";
        public const string account_alt_password = "password.1";

        public const string account_billed_emailaddress = "elsa.evers@greenpeace.org";
        public const string account_billed_password = "password.1";
        //public const string account_billed_emailaddress = "scott.crebbin@me.com";
        //public const string account_billed_password = "password";

        public const int service_major_metro_all_states_fax = 630;
        public const int service_major_metro_all_states_email = 20368;
        public const int service_aap_daily_diary_email = 104566;
        public const int package_federal = 4881;
        public const int service_all_states_and_territories_wire = 100001;
        public const int service_all_states_and_territories_audio_wire = 123888;
        public const int service_talkback_weekdays_email = 134825;

        public const int mediadatabase_update_log_id = 1386921;
        public const int outlet_type_id = 418;
        public const int product_type_id = 598;
        public const int news_focus_id = 631;
        public const int frequency_id = 8031;
        public const int working_language_id = 266;
        public const int station_format_id = 2437;
        public const int subject_id = 16135;
        public const int subject_groupid = 16100;
        public const int contactrole_id = 659;
        public const string outlet_partial_name = "Aust";
        public const string outlet_id = "AAPO14435";
        public const string contact_id = "";
        public const int customer_release_id = 726702;

        public const int aap_release_id = 734682;
        public const string aap_release_securitykey = "2194930";
        public const int release_id_not_public = 777966;
        public const int document_id_public = 2626726;
        public const int aap_transaction_id = 2125413;
        public const int results_release_id = 623879;
        public const int results_transaction_id = 1695901;
        public const int results_distribution_id = 9441965;
        public const int inbound_file_id = 83098;
    }
}
