﻿using Medianet.Service.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Medianet.Test
{
    
    
    /// <summary>
    ///This is a test class for ExtensionMethodsTest and is intended
    ///to contain all ExtensionMethodsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ExtensionMethodsTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for ConvertToAscii
        ///</summary>
        [TestMethod()]
        public void ConvertToAsciiTest() {
            string value = "Wasn’t “testing” ‘testing’ test… ™";
            string expected = "Wasn't \"testing\" 'testing' test... (TM)";
            string actual;

            actual = value.ConvertToAscii();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Truncate
        ///</summary>
        [TestMethod()]
        public void TruncateTest_InputLonger_Truncates() {
            string value = "1234567890";
            int maxLength = 5;
            string actual;

            actual = ExtensionMethods.Truncate(value, maxLength);
            Assert.IsTrue(actual.Length == maxLength);
        }

        /// <summary>
        ///A test for Truncate
        ///</summary>
        [TestMethod()]
        public void TruncateTest_InputShorter_DoesntChange() {
            string value = "1234567890";
            int maxLength = 15;
            string actual;

            actual = ExtensionMethods.Truncate(value, maxLength);
            Assert.IsTrue(actual.Length == value.Length);
        }

        /// <summary>
        ///A test for ConvertToAscii
        ///</summary>
        [TestMethod()]
        public void TrimOrEmptuIfNullTest()
        {
            string value = " AWasn’t thdada dd*  ";
            string expected = "AWasn’t thdada dd*";
            string actual;

            actual = value.TrimAndEmptyIfNull();
            Assert.AreEqual(expected, actual);

            string valueNull = null;
            string expectedEmpty = string.Empty;
            string actualEmpty;
            actualEmpty = valueNull.TrimAndEmptyIfNull();
            Assert.AreEqual(expectedEmpty, actualEmpty);

        }

    }
}
