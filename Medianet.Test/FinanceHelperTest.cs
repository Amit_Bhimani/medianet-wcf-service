﻿using System.Collections.Generic;
using Medianet.Service.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using Medianet.Service.Dto;

namespace Medianet.Test
{
    
    
    /// <summary>
    ///This is a test class for FinanceHelperTest and is intended
    ///to contain all FinanceHelperTest Unit Tests
    ///</summary>
    [TestClass()]
    public class FinanceHelperTest
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes

        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            if (!AutoMapperConfigurator.IsInitialized)
                AutoMapperConfigurator.Configure();
        }

        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        ///A test for FormatNumberForFinance
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Medianet.Service.dll")]
        public void FormatNumberForFinance_RemovesNonNumericCharactersAndAddsCountryCode() {
            string strNumber = "(02) 9322gh_~~@$#%+=^$8000";
            string expected = "61293228000";
            string actual;
            actual = FinanceHelper.FormatNumberForFinance(strNumber);
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        //public void GenerateFinanceQuoteRequest_TwitterAndWire_TwitterNotCharged()
        //{
        //    Release release;
        //    ReleaseSummary relSummary;
        //    var releaseItems = new ReleaseItemCounts { AttachmentCount = 1, FaxPageCount = 1, WireWordCount = 200};
        //    var session = new DBSession { DebtorNumber = "10", User = new User { DebtorNumber = "10", ReportSendMethod = ReportType.Email } };
        //    Medianet.Service.FinancePaymentService.MNTransactionHeader quoteRequest;
        //    Transaction twitterTrans;
        //    relSummary = Common.GenerateReleaseSummary(session, true);

        //    relSummary.Transactions = new List<TransactionSummary>();
        //    relSummary.Transactions.Add(Common.GenerateTwitterTransaction(1));
        //    relSummary.Transactions.Add(Common.GenerateServiceTransaction(Constants.service_all_states_and_territories_wire, 2));
        //    relSummary.Documents = new List<DocumentSummary>();

        //    release = ReleaseHelper.GenerateReleaseFromSummary(relSummary, session);
        //    twitterTrans = release.Transactions.First(t => t.DistributionType == DistributionType.Twitter);

        //    quoteRequest = FinanceHelper.GenerateFinanceQuoteRequest(release, releaseItems, session.User);

        //    Assert.IsFalse(quoteRequest.MNTransactions.ToList().Exists(t => t.ListCode == twitterTrans.AccountCode));
        //}

        [TestMethod()]
        public void GenerateFinanceQuoteRequest_TwitterOnly_TwitterCharged()
        {
            Release release;
            ReleaseSummary relSummary;
            var releaseItems = new ReleaseItemCounts { AttachmentCount = 1, WireWordCount = 200 };
            var session = new DBSession { DebtorNumber = "10", User = new User { DebtorNumber = "10", ReportSendMethod = ReportType.Email } };
            Medianet.Service.FinancePaymentService.MNTransactionHeader quoteRequest;
            Transaction twitterTrans;
            relSummary = Common.GenerateReleaseSummary(session, true);

            relSummary.Transactions = new List<TransactionSummary>();
            relSummary.Transactions.Add(Common.GenerateTwitterTransaction(1));
            relSummary.Documents = new List<DocumentSummary>();

            release = ReleaseHelper.GenerateReleaseFromSummary(relSummary, session);
            twitterTrans = release.Transactions.First(t => t.DistributionType == DistributionType.Twitter);

            quoteRequest = FinanceHelper.GenerateFinanceQuoteRequest(release, releaseItems, session.User);

            Assert.IsTrue(quoteRequest.MNTransactions.ToList().Exists(t => t.ListCode == twitterTrans.AccountCode));
        }
    }
}
