﻿using System.Collections.Generic;
using System.ServiceModel;
using Medianet.Service;
using Medianet.Service.Common;
using Medianet.Service.Dto;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Medianet.Test
{
    
    
    /// <summary>
    ///This is a test class for ReleaseHelperTest and is intended
    ///to contain all ReleaseHelperTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ReleaseHelperTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:

        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            if (!AutoMapperConfigurator.IsInitialized)
                AutoMapperConfigurator.Configure();
        }

        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for TruncateToLength
        ///</summary>
        [TestMethod()]
        public void TruncateToLengthTest_InputTooLong_TruncatedToLastWord() {
            string input = "This is a test of truncating a string.";
            int length = 25;
            string expected = "This is a test of...";
            string actual;

            actual = ReleaseHelper.TruncateToLength(input, length);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TruncateToLengthTest_InputTooLongButNoSpaces_TruncatedToExactLength() {
            string input = "This_is_a_test_of_truncating_a_string.";
            int length = 25;
            string expected = "This_is_a_test_of_trun...";
            string actual;

            actual = ReleaseHelper.TruncateToLength(input, length);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TruncateToLengthTest_InputNotTooLong_TextDoesntChange() {
            string input = "This is a test of truncating a string.";
            int length = 38;
            string expected = "This is a test of truncating a string.";
            string actual;

            actual = ReleaseHelper.TruncateToLength(input, length);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AddReleaseFromSummary_PublicWebTransaction_ShouldShowOnPublicTicked()
        {
            ReleaseSummary release;
            var session = new DBSession { DebtorNumber = "10", System = SystemType.Medianet, UserId = 5, User = new User { Id = 5, DebtorNumber = "10" } };

            release = Common.GenerateReleaseSummary(session, true);
            release.Transactions = new List<TransactionSummary>();
            release.Transactions.Add(Common.GenerateWebTransaction(Medianet.Service.BusinessLayer.Common.Constants.PUBLIC_WEBSITE_SERVICE_ID, 1));
            release.Documents = new List<DocumentSummary>();

            var x = ReleaseHelper.GenerateReleaseFromSummary(release, session);

            Assert.IsTrue(x.WebDetails != null && x.WebDetails.ShouldShowOnPublic && !x.WebDetails.ShouldShowOnJournalists);
        }

        [TestMethod]
        public void AddReleaseFromSummary_JournalistsWebTransaction_ShouldShowOnJournalistsTicked()
        {
            ReleaseSummary release;
            var session = new DBSession { DebtorNumber = "10", System = SystemType.Medianet, UserId = 5, User = new User { Id = 5, DebtorNumber = "10" } };

            release = Common.GenerateReleaseSummary(session, true);
            release.Transactions = new List<TransactionSummary>();
            release.Transactions.Add(Common.GenerateWebTransaction(Medianet.Service.BusinessLayer.Common.Constants.MNJ_WEBSITE_SERVICE_ID, 1));
            release.Documents = new List<DocumentSummary>();

            var x = ReleaseHelper.GenerateReleaseFromSummary(release, session);

            Assert.IsTrue(!x.WebDetails.ShouldShowOnPublic && x.WebDetails.ShouldShowOnJournalists);
        }

        [TestMethod]
        public void AddReleaseFromSummary_EmailTransaction_Should_Not_Have_ShowOnJournalistTicked_Or_ShowOnWebsiteTicked()
        {
            ReleaseSummary release;
            var session = new DBSession { DebtorNumber = "10", System = SystemType.Medianet, UserId = 5, User = new User { Id = 5, DebtorNumber = "10" } };

            release = Common.GenerateReleaseSummary(session, true);
            release.Transactions = new List<TransactionSummary>();
            release.Transactions.Add(Common.GenerateExtraEmailRecipientTransaction("hpatel@aap.com.au", DistributionType.Email, 1));
            release.Documents = new List<DocumentSummary>();

            var x = ReleaseHelper.GenerateReleaseFromSummary(release, session);

            Assert.IsTrue(!x.WebDetails.ShouldShowOnPublic && !x.WebDetails.ShouldShowOnJournalists);

            
        }

    }
}
