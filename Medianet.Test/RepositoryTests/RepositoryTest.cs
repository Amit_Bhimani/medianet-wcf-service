﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Medianet.Service;
using Medianet.Service.Contracts;
using Medianet.Service.Common;
using Medianet.DataLayer;
///using Medianet.DataLayer.Repositories;
using Medianet.Model;
using Medianet.Service.BusinessLayer;
using Medianet.Model.Entities;
using Medianet.DataLayer.Repositories;

namespace Medianet.Test
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class RepositoryTest
    {
        public RepositoryTest() {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //

        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext) {
            if (!AutoMapperConfigurator.IsInitialized)
                AutoMapperConfigurator.Configure();
        }

        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        #region CityRepository

        [TestMethod]
        public void CityRepository_GetAll_ReturnsItems() {
            List<City> cities;

            using (var uow = new Medianet.DataLayer.UnitOfWork()) {
                var repo = new CityRepository(uow);
                cities = repo.GetAll();
            }

            Assert.IsTrue(cities != null && cities.Count > 0 && cities[0].CityId > 0);
        }

        [TestMethod]
        public void CityRepository_GetById_ReturnsItem() {
            City city;

            using (var uow = new Medianet.DataLayer.UnitOfWork()) {
                var repo = new CityRepository(uow);
                city = repo.GetById(270022);
            }

            Assert.IsTrue(city != null && city.CityId > 0);
        }

        [TestMethod]
        public void CityRepository_GetAllByAny_ReturnsItems() {
            List<City> cities;

            using (var uow = new Medianet.DataLayer.UnitOfWork()) {
                var repo = new CityRepository(uow);
                cities = repo.GetAllByAny(30, 210, 39000, "Sy");
            }

            Assert.IsTrue(cities != null && cities.Count > 0 && cities[0].CityId > 0);
        }

        #endregion

        #region ContinentRepository

        [TestMethod]
        public void ContinentRepository_GetAll_ReturnsItems() {
            List<Continent> continent;

            using (var uow = new Medianet.DataLayer.UnitOfWork()) {
                var repo = new ContinentRepository(uow);
                continent = repo.GetAll();
            }

            Assert.IsTrue(continent != null && continent.Count > 0 && continent[0].Id > 0);
        }

        [TestMethod]
        public void ContinentRepository_GetById_ReturnsItem() {
            Continent continent;

            using (var uow = new Medianet.DataLayer.UnitOfWork()) {
                var repo = new ContinentRepository(uow);
                continent = repo.GetById(30);
            }

            Assert.IsTrue(continent != null && continent.Id > 0);
        }

        #endregion

        #region CountryRepository

        [TestMethod]
        public void CountryRepository_GetAll_ReturnsItems() {
            List<Country> countries;

            using (var uow = new Medianet.DataLayer.UnitOfWork()) {
                var repo = new CountryRepository(uow);
                countries = repo.GetAll();
            }

            Assert.IsTrue(countries != null && countries.Count > 0 && countries[0].MediaAtlasCountryId > 0);
        }

        #endregion

        #region CustomerRepository tests

        [TestMethod]
        public void CustomerRepository_GetByDebtorNumber_ReturnsOneItem() {
            Customer cust;

            using (var uow = new UnitOfWork()) {
                var repo = new CustomerRepository(uow);
                cust = repo.GetByDebtorNumber("10");
            }

            Assert.IsNotNull(cust);
        }

        //[TestMethod]
        //public void CustomerRepository_Delete_Works() {
        //    List<ServiceList> services;

        //    using (var repo = new CustomerRepository()) {
        //        repo.Delete("05698", 5);
        //    }
        //}

        #endregion

        #region DBSessionRepository tests

        //[TestMethod]
        //public void DBSessionRepository_GetById_AlsoFetchesCustomerAndUser() {
        //    DBSession dbSession;

        //    using (var repo = new DBSessionRepository()) {
        //        dbSession = repo.GetById(180494);
        //    }

        //    Assert.IsTrue(dbSession.Customer != null && dbSession.Customer.DebtorNumber == dbSession.DebtorNumber &&
        //                  dbSession.User != null && dbSession.User.Id == dbSession.UserId);
        //}

        [TestMethod]
        public void DBSessionRepository_GetByKey_ReturnsNothingIfUserIsInactive() {
            CustomerService cService = new CustomerService();
            Medianet.Service.Dto.DBSession session;

            using (var uow = new UnitOfWork()) {
                var userBL = new UserBL(uow);
                // Make sure the user is active.
                Medianet.Service.Dto.User u = userBL.Get(5);
                u.RowStatus = RowStatusType.Active;
                userBL.Update(u, u.Id);

                // Logon.
                session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, Constants.account_password, SystemType.Medianet);

                // Now set it to inactive.
                u.RowStatus = RowStatusType.Inactive;
                userBL.Update(u, u.Id);

                try {
                    var dbRepo = new DBSessionRepository(uow);
                    int id = int.Parse(session.Key);
                    DBSession dbSession;

                    // The session is active, but the user is now inactive, so this should return null.
                    dbSession = dbRepo.GetActiveById(id);

                    Assert.IsNull(dbSession);
                }
                finally {
                    // Set it back to active.
                    u.RowStatus = RowStatusType.Active;
                    userBL.Update(u, u.Id);
                }
            }

        }

        #endregion

        #region InboundFileRepository tests

        [TestMethod]
        public void InboundFileRepository_GetAllByDebtorNumberType_ReturnsItems() {
            List<InboundFile> items;

            using (var uow = new Medianet.DataLayer.UnitOfWork()) {
                var repo = new InboundFileRepository(uow);
                items = repo.GetAllByDebtorNumberType("10", ((char)MessageType.RadioRelease).ToString(), 
                    ((char)InboundFileStatusType.Ready).ToString(), (int)InboundFileSortOrderType.Id, 0, 10);
            }

            Assert.IsTrue(items != null && items.Count > 0);
        }

        #endregion

        #region ListCategoryRepository tests

        [TestMethod]
        public void ListCategoryRepository_GetAll_ReturnsItems() {
            List<ListCategory> lCategory;

            using (var uow = new UnitOfWork()) {
                var repo = new ListCategoryRepository(uow);
                lCategory = repo.GetAll();
            }

            Assert.IsTrue(lCategory != null && lCategory.Count > 0);
        }

        #endregion

        #region MediaOutletRepository tests

        [TestMethod]
        public void MediaOutletRepository_SetOutletLogo_Works()
        {
            List<Result> results;

            using (var uow = new Medianet.DataLayer.UnitOfWork())
            {
                var repo = new MediaOutletRepository(uow);
                var origOutlet = repo.GetById("AAPO58");

                try
                {
                    repo.SetOutletLogo("AAPO58", "test.jpg", 5);
                    var modifiedOutlet = repo.GetById("AAPO58");

                    Assert.AreEqual("test.jpg", modifiedOutlet.LogoFileName);
                }
                finally
                {
                    // Set it back to what it was before
                    repo.SetOutletLogo("AAPO58", origOutlet.LogoFileName, 5);
                }
            }
        }

        #endregion

        #region RegistrationValidation tests
        [TestMethod]
        public void Registration_GetByToken_ReturnsItem()
        {
            RegistrationValidation rego;

            using (var uow = new Medianet.DataLayer.UnitOfWork())
            {
                Random r = new Random();
                string randomString = r.Next().ToString();
                RegistrationValidation account = new RegistrationValidation();

                account.EmailAddress = "david." + randomString + "@aap.com.au";
                account.CompanyName = "UnitTest" + randomString;
                account.FirstName = "David";
                account.LastName = randomString;
                account.Password = "testPassword";
                account.TelephoneNumber = "94811111";
                account.CreatedDate = DateTime.Now;
                account.System = "M";
                account.Token = Guid.NewGuid();
                ReleaseUnverifiedPreview rel = new ReleaseUnverifiedPreview() { Headline = "Headline", Organisation = "Company", ReleaseTempFileId = 99, WebCategoryId = 9, WhitePaperTempFileId = 98 };
                var repo = new RegistrationValidationRepository(uow);
                var rupr = new ReleaseUnverifiedPreviewRepository(uow);
                account.Token = repo.Add(account);
                rel.Token = account.Token;
                rupr.Add(rel);

                rego = repo.Get(rel.Token);
            }

            Assert.IsTrue(rego != null && rego.Password != null);
        }
        #endregion

        #region ReleaseRepository tests

        [TestMethod]
        public void ReleaseRepository_GetById_ReturnsItem() {
            Release release;

            using (var uow = new Medianet.DataLayer.UnitOfWork()) {
                var repo = new ReleaseRepository(uow);

                release = repo.GetById(Constants.customer_release_id);
            }

            Assert.IsTrue(release != null && release.CreatedByUser != null);
        }

        [TestMethod]
        public void ReleaseRepository_GetAllByDebtorNumberWithCustomerLists_ReturnsItems() {
            List<Release> releases;

            using (var uow = new Medianet.DataLayer.UnitOfWork()) {
                var repo = new ReleaseRepository(uow);
                List<string> statuses = new List<string> {((char)ReleaseStatusType.Accounts).ToString(),
                                                        ((char)ReleaseStatusType.Resulted).ToString(),
                                                        ((char)ReleaseStatusType.Invoiced).ToString()};

                releases = repo.GetAllByDebtorNumber("10", ((char)SystemType.Medianet).ToString(), statuses, (int)ReleaseSortOrderType.CreatedDate, 0, 25);
            }

            Assert.IsTrue(releases != null && releases.Count > 0);
        }

        [TestMethod]
        public void ReleaseRepository_GetReleasesCountWithResultsToShow_ReturnsItems() {
            int releaseCount;

            using (var uow = new Medianet.DataLayer.UnitOfWork()) {
                var repo = new ReleaseRepository(uow);
                List<string> statuses = new List<string> {((char)ReleaseStatusType.Accounts).ToString(),
                                                        ((char)ReleaseStatusType.Resulted).ToString(),
                                                        ((char)ReleaseStatusType.Invoiced).ToString()};

                releaseCount = repo.GetCountByDebtorNumber("10", ((char)SystemType.Medianet).ToString(), statuses);
            }

            Assert.IsTrue(releaseCount > 0);
        }

        [TestMethod]
        public void ReleaseRepository_OpenRelease_Works() {
            Release release;

            using (var uow = new Medianet.DataLayer.UnitOfWork()) {
                var repo = new ReleaseRepository(uow);

                release = repo.GetById(Constants.customer_release_id);
                repo.OpenRelease(release.Id, release.Status, 5);
            }
        }

        [TestMethod]
        public void ReleaseRepository_CloseRelease_Works() {
            Release release;

            // Make sure the release is already closed.
            var dbContext = new MedianetContext();
            release = dbContext.Releases.Where(r => r.Id == Constants.customer_release_id).First();
            release.IsOpen = 0;
            dbContext.SaveChanges();

            using (var uow = new Medianet.DataLayer.UnitOfWork()) {
                var repo = new ReleaseRepository(uow);

                release = repo.GetById(Constants.customer_release_id);
                repo.OpenRelease(release.Id, release.Status, 5);
                repo.CloseRelease(release.Id);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ReleaseRepository_OpenRelease_ReleaseAlreadyOpen_ThrowsException() {
            Release release;

            using (var uow = new Medianet.DataLayer.UnitOfWork()) {
                var repo = new ReleaseRepository(uow);

                try {
                    release = repo.GetById(Constants.customer_release_id);
                    repo.OpenRelease(release.Id, release.Status, 5);
                    repo.OpenRelease(release.Id, release.Status, 5);
                }
                finally {
                    try {
                        repo.CloseRelease(Constants.customer_release_id);
                    }
                    catch (Exception) {
                        // Ignore the error.
                    }
                }
            }
        }

        #endregion

        #region ResultRepository tests

        [TestMethod]
        public void ResultRepository_GetAllByReleaseId_ReturnsItem() {
            List<Result> results;

            using (var uow = new Medianet.DataLayer.UnitOfWork()) {
                var repo = new ResultRepository(uow);
                results = repo.GetAllByReleaseIdTransactionId(854906, 2586293);
            }

            Assert.IsTrue(results.Count > 0);
        }

        #endregion

        #region ServiceListRepository tests

        [TestMethod]
        public void ServiceRepository_GetNextListNumber_NoItemsFound_Returns1() {
            int nextListNumber;

            using (var uow = new Medianet.DataLayer.UnitOfWork()) {
                var repo = new ServiceListRepository(uow);
                nextListNumber = repo.GetNextListNumber("XXXX1234", ((char)DistributionType.Fax).ToString(), ((char)SystemType.Medianet).ToString());
            }

            Assert.IsTrue(nextListNumber == 1);
        }

        [TestMethod]
        public void ServiceRepository_GetNextSequenceNumber_NoItemsFound_Returns1() {
            short nextListNumber;

            using (var uow = new Medianet.DataLayer.UnitOfWork()) {
                var repo = new ServiceListRepository(uow);
                nextListNumber = repo.GetNextSequenceNumber("XXXX1234", ((char)DistributionType.Fax).ToString(), ((char)SystemType.Medianet).ToString());
            }

            Assert.IsTrue(nextListNumber == 1);
        }

        [TestMethod]
        public void ServiceRepository_GetNextRecipientSequenceNumber_NoItemsFound_Returns1() {
            int nextListNumber;

            using (var uow = new Medianet.DataLayer.UnitOfWork()) {
                var repo = new ServiceListRepository(uow);
                nextListNumber = repo.GetNextSequenceNumber(919191919);
            }

            Assert.IsTrue(nextListNumber == 1);
        }

        [TestMethod]
        public void GetOutletDistributionByType()
        {
            using (var uow = new Medianet.DataLayer.UnitOfWork())
            {
                var repo = new ServiceListRepository(uow);
                var results = repo.GetOutletOrEmployeeDistributionByType("AAPO1745",null,"E","1",((char)SystemType.Medianet).ToString());
                Assert.IsTrue(results != null);
            }
        }

        [TestMethod]
        public void SearchAapListsByOutlet()
        {
            using (var uow = new Medianet.DataLayer.UnitOfWork())
            {
                var repo = new ServiceListRepository(uow);
                var results = repo.SearchAapListsByOutletId("AAPO1745", Medianet.Service.BusinessLayer.Common.Constants.DEBTOR_NUMBER_AAP);
                Assert.IsTrue(results != null && results.Count > 0);
            }
        }

        #endregion

        #region ServiceListRepository Old tests

        [TestMethod]
        public void ServiceRepository_GetAllByDebtorNumber_OnlyGetsRowsForDebtorNumberSystem() {
            List<ServiceList> services;
            string system = ((char)SystemType.Medianet).ToString();

            using (var uow = new UnitOfWork()) {
                var repo = new ServiceListRepository(uow);
                services = repo.GetAllByDebtorNumber("10", system);
            }

            Assert.IsTrue(services != null && services.Where(s => s.DebtorNumber != "10").Count() == 0
                                           && services.Where(s => s.System != system).Count() == 0);
        }

        //[TestMethod]
        //public void ServiceListRepository_DeleteRecipient_Works() {
        //    Recipient recip = new Recipient();
        //    int id;
        //    //bool error = false;

        //    recip.FirstName = "david";
        //    recip.MiddleName = "T";
        //    recip.LastName = "smith";
        //    recip.Company = "AAP";
        //    recip.Address = string.Empty;
        //    recip.AddressLine1 = string.Empty;
        //    recip.AddressLine2 = string.Empty;
        //    recip.JobTitle = string.Empty;
        //    recip.PhoneNumber = string.Empty;
        //    recip.ReferenceId = string.Empty;
        //    recip.Title = string.Empty;

        //    //try {
        //        // Do it in the one repository.
        //        using (var repo = new ServiceListRepository()) {
        //            id = repo.AddRecipient(recip);

        //            repo.DeleteRecipient(id);
        //        }

        //        // Do it in seperate repositoiries.
        //        using (var repo = new ServiceListRepository()) {
        //            id = repo.AddRecipient(recip);
        //        }
        //        using (var repo = new ServiceListRepository()) {
        //            repo.DeleteRecipient(id);
        //        }
        //    //}
        //    //catch (Exception) {
        //    //    error = true;
        //    //}

        //    //Assert.IsTrue(!error);
        //}

        #endregion

        #region StateRepository

        [TestMethod]
        public void StateRepository_GetAll_ReturnsItems() {
            List<State> states;

            using (var uow = new Medianet.DataLayer.UnitOfWork()) {
                var repo = new StateRepository(uow);
                states = repo.GetAll();
            }

            Assert.IsTrue(states != null && states.Count > 0 && states[0].StateId > 0);
        }

        #endregion

        #region TransactionRepository

        [TestMethod]
        public void TransactionRepository_GetAllByReleaseId_ReturnsItems() {
            List<Transaction> transactions;

            using (var uow = new Medianet.DataLayer.UnitOfWork())
            {
                var repo = new TransactionRepository(uow);
                transactions = repo.GetAllByReleaseId(735542);
            }

            Assert.IsTrue(transactions != null && transactions.Count > 0);
        }

        [TestMethod]
        public void TransactionRepository_GetReleaseTransactionStatistics_ReturnsItems() {
            List<Model.Entities.TransactionStatistics> transactions;

            using (var uow = new Medianet.DataLayer.UnitOfWork())
            {
                var repo = new TransactionRepository(uow);
                transactions = repo.GetAllStatisticsByReleaseId(735542);
            }

            Assert.IsTrue(transactions != null && transactions.Count > 0);
        }
        
        #endregion

        #region WebReleaseRepository tests

        [TestMethod]
        public void WebReleaseRepository_GetById_Works() {
            WebRelease release;

            using (var uow = new Medianet.DataLayer.UnitOfWork())
            {
                var repo = new WebReleaseRepository(uow);
                release = repo.GetById(735481);
            }

            Assert.IsNotNull(release);
        }

        #endregion

        #region Media Contact Group Record List tests
        [TestMethod]
        public void MediaContactGroupRecordListRepository_GetByListIdAndListSet_ReturnsItems()
        {
            List<MediaContactListRecordDetails> lGroupRecordList;

            using (var uow = new UnitOfWork())
            {
                var repo = new MediaContactListRecordRepository(uow);
                lGroupRecordList = repo.GetByListIdAndListSet(67512,0,5,100);
            }

            Assert.IsTrue(lGroupRecordList != null && lGroupRecordList.Count > 0);
        }

        #endregion

        #region Media Movement tests

        [TestMethod]
        public void MediaContactMovementRepository_GetAll_ReturnsItems()
        {
            List<MediaContactMovement> results;
            int totalCount;
            int pageSize = 1;

            using (var uow = new Medianet.DataLayer.UnitOfWork())
            {
                var repo = new MediaMovementRepository(uow);
                results = repo.GetAll(true, 1, pageSize);
                totalCount = repo.GetAllCount(true);
            }

            Assert.IsTrue(results.Count > 0 && results.Count <= pageSize);
            Assert.IsTrue(totalCount >= results.Count);
        }

        [TestMethod]
        public void MediaContactMovementRepository_GetByOutletId_ReturnsItem()
        {
            List<MediaContactMovement> results;
            
            using (var uow = new Medianet.DataLayer.UnitOfWork())
            {
               var repo = new MediaMovementRepository(uow);
               results = repo.GetByOutletId("AAPO140", 1, 4);
            }

            Assert.IsTrue(results.Count > 0);

        }

        [TestMethod]
        public void MediaContactMovementRepository_GetByContactId_ReturnsItem()
        {
            List<MediaContactMovement> results;
           
            using (var uow = new Medianet.DataLayer.UnitOfWork())
            {
                var repo = new MediaMovementRepository(uow);
                results = repo.GetByContactId("AAPC15917", 1, 4);
            }

            Assert.IsTrue(results.Count > 0);

        }
        #endregion
    }
}
