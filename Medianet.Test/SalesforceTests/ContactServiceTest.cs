﻿using System;
using System.Configuration;
using System.Linq;
using System.Runtime.InteropServices;
using Medianet.Salesforce.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Medianet.Test.SalesforceTests
{
    [TestClass]
    public class ContactServiceTest
    {
        private static ForceSettings _forceSettings;

        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            _forceSettings = new ForceSettings()
            {
                ClientId = ConfigurationManager.AppSettings["SalesForceClientId"],
                ClientSecret = ConfigurationManager.AppSettings["SalesForceClientSecret"],
                UserName = ConfigurationManager.AppSettings["SalesForceUserName"],
                Password = ConfigurationManager.AppSettings["SalesForcePassword"],
                Url = ConfigurationManager.AppSettings["SalesForceUrl"]
            };
        }

        [TestMethod]
        public void GetContacts_Works()
        {
            var contactService = new ForceContactService(_forceSettings);
            var contacts = contactService.GetContacts("hpatel@aap.com.au", "10").Result;

            Assert.IsTrue(contacts.Count == 1);
            Assert.IsTrue(contacts[0].Name.Contains("Hitesh"));
        }


        [TestMethod]
        public void UpdateContactSubject_Works()
        {
            var contactService = new ForceContactService(_forceSettings);
            var contacts = contactService.GetContacts("hpatel@aap.com.au", "10").Result;

            Assert.IsTrue(contacts.Count == 1);

            var currentSubjects = contacts[0].Subject_Category_of_Interest__c.Split(Convert.ToChar(";")).ToList();


            var generalService = new ForceGeneralService(_forceSettings);
            var subjects = generalService.GetPickListValues("Contact", "Subject_Category_of_Interest__c").Result;
            if (subjects.Count == currentSubjects.Count)
            {
                currentSubjects.Clear();

            }
            else
            {
                // add a subject
                var added = false;
                foreach (var subject in subjects)
                {
                    
                    if (!added && !currentSubjects.Contains(subject))
                    {
                        currentSubjects.Add(subject);
                        added = true;
                    }
                }
            }
            var newCount = currentSubjects.Count;

            // Act
            var updated = contactService.UpdateSubjectCategory(contacts[0].Id, currentSubjects).Result;
            Assert.IsTrue(updated);
            var updatedcontact = contactService.GetContacts("hpatel@aap.com.au", "10").Result;
            var newSubjects = updatedcontact[0].Subject_Category_of_Interest__c.Split(Convert.ToChar(";")).ToList();

            Assert.IsTrue(newCount == newSubjects.Count);
        }



    }
}
