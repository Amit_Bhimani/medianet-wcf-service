﻿using System;
using System.Configuration;
using Medianet.Salesforce.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Medianet.Test.SalesforceTests
{
    [TestClass]
    public class GeneralServiceTest
    {
        private static ForceSettings _forceSettings;

        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            _forceSettings = new ForceSettings()
            {
                ClientId = ConfigurationManager.AppSettings["SalesForceClientId"],
                ClientSecret = ConfigurationManager.AppSettings["SalesForceClientSecret"],
                UserName = ConfigurationManager.AppSettings["SalesForceUserName"],
                Password = ConfigurationManager.AppSettings["SalesForcePassword"],
                Url = ConfigurationManager.AppSettings["SalesForceUrl"]
            };
        }

        [TestMethod]
        public void Picklist_CanBeFetched()
        {
            var contactService = new ForceGeneralService(_forceSettings);
            var subjects = contactService.GetPickListValues("Contact", "Subject_Category_of_Interest__c").Result;
            Assert.IsTrue(subjects.Count > 0);
        }
    }
}
