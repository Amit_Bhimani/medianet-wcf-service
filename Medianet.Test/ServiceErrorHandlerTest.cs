﻿using Medianet.Wcf;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.ServiceModel;
using Medianet.Service.FaultExceptions;
using System.Collections.Generic;

namespace Medianet.Test
{
    
    
    /// <summary>
    ///This is a test class for ServiceErrorHandlerTest and is intended
    ///to contain all ServiceErrorHandlerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ServiceErrorHandlerTest
    {

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for IsDoNotLogFault
        ///</summary>
        [TestMethod()]
        public void IsDoNotLogFaultTest_ApplicationException_ReturnsFalse() {
            var error = new ApplicationException();
            Assert.IsFalse(ServiceErrorHandler.IsDoNotLogFault(error));
        }

        [TestMethod()]
        public void IsDoNotLogFaultTest_KeyNotFoundFault_ReturnsFalse() {
            var error = new FaultException<KeyNotFoundFault>(new KeyNotFoundFault(), "Test");
            Assert.IsFalse(ServiceErrorHandler.IsDoNotLogFault(error));
        }

        [TestMethod()]
        public void IsDoNotLogFaultTest_LogonLicensesExceededFault_ReturnsTrue() {
            var error = new FaultException<LogonLicensesExceededFault>(new LogonLicensesExceededFault(new List<Service.Dto.DBSession>(), 1), "Test");

            Assert.IsTrue(ServiceErrorHandler.IsDoNotLogFault(error));
        }

        [TestMethod()]
        public void IsDoNotLogFaultTest_FaultWithNoDetail_ReturnsFalse() {
            var error = new FaultException(new FaultReason("Test"));
            Assert.IsFalse(ServiceErrorHandler.IsDoNotLogFault(error));
        }
    }
}
