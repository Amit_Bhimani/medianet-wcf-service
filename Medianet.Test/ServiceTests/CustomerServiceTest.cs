﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.ServiceModel;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Medianet.Service;
using Medianet.Service.Contracts;
using Medianet.Service.Dto;
using Medianet.Service.Common;
using Medianet.Model;
using Medianet.Service.FaultExceptions;
using System.Configuration;
using Medianet.Service.BusinessLayer.Common;
using Medianet.Service.BusinessLayer;
using Medianet.DataLayer;

namespace Medianet.Test
{
    /// <summary>
    ///This is a test class for CustomerServiceTest and is intended
    ///to contain all CustomerServiceTest Unit Tests
    ///</summary>
    [TestClass()]
    public class CustomerServiceTest
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes

        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            if (!AutoMapperConfigurator.IsInitialized)
                AutoMapperConfigurator.Configure();
        }

        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        [TestMethod()]
        public void IsUniqueEmailTest_ReturnsTrueIfUnique()
        {
            CustomerService cService = new CustomerService();
            Random r = new Random();
            string emailAddress = string.Format("dave{0}@aap.com.au", r.Next().ToString());

            Assert.IsTrue(cService.IsUniqueEmail(emailAddress));
        }

        [TestMethod()]
        public void IsUniqueEmailTest_ReturnsFaseIfNotUnique()
        {
            CustomerService cService = new CustomerService();

            Assert.IsFalse(cService.IsUniqueEmail(Constants.account_billed_emailaddress));
        }

        [TestMethod]
        public void Login_Succeeds()
        {
            CustomerService cService = new CustomerService();
            DBSession session;

            // Make sure no session exists first.
            Common.CleanupSession(Constants.account_user_id, SystemType.Medianet);

            session = cService.Login(Constants.account_username, Constants.account_companylogon, Constants.account_password, SystemType.Medianet);
            Assert.IsTrue(IsValidSession(session));
        }

        [TestMethod]
        public void LoginUsingUsernameAtCompanylogon_Succeeds()
        {
            CustomerService cService = new CustomerService();
            DBSession session;

            // Make sure no session exists first.
            Common.CleanupSession(Constants.account_user_id, SystemType.Medianet);

            session = cService.LoginUsingEmailAddress(Constants.account_username + "@" + Constants.account_companylogon, Constants.account_password, SystemType.Medianet);
            Assert.IsTrue(IsValidSession(session));
        }

        [TestMethod]
        public void LoginToAdminSystem_Succeeds()
        {
            CustomerService cService = new CustomerService();
            DBSession session;

            // Make sure no session exists first.
            Common.CleanupSession(Constants.account_user_id, SystemType.Admin);

            session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, Constants.account_password, SystemType.Admin);
            Assert.IsTrue(IsValidSession(session));
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException<InvalidLoginFault>))]
        public void LoginWithWrongPassword_ThrowsInvalidLoginFault()
        {
            CustomerService cService = new CustomerService();
            DBSession session;

            session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, "wrongpassword", SystemType.Medianet);
        }

        [TestMethod]
        public void LoginAgain_SameSessionIdIsUsed()
        {
            CustomerService cService = new CustomerService();
            DBSession session;
            string oldSessionKey;

            session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, Constants.account_password, SystemType.Medianet);
            oldSessionKey = session.Key;
            cService.Logout(oldSessionKey);
            session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, Constants.account_password, SystemType.Medianet);

            Assert.IsTrue(oldSessionKey == session.Key);
        }

        [TestMethod]
        public void Logout_SessionIsDisabled()
        {
            CustomerService cService = new CustomerService();
            DBSession session;

            session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, Constants.account_password, SystemType.Medianet);
            cService.Logout(session.Key);
            session = cService.ValidateSession(session.Key);

            Assert.IsTrue(session == null);
        }

        [TestMethod]
        public void ValidateSession_LastAccessedDateChanges()
        {
            CustomerService cService = new CustomerService();
            DBSession session;

            session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, Constants.account_password, SystemType.Medianet);
            System.Threading.Thread.Sleep(500);

            cService.ValidateSession(session.Key);

            var dbContext = new MedianetContext();
            int sessionId = int.Parse(session.Key);
            string system = ((char)SystemType.Medianet).ToString();
            List<Model.Entities.DBSession> dbEntities = dbContext.DBSessions.Where(s => s.Id == sessionId && s.System == system).ToList();

            Assert.IsTrue(dbEntities[0].LastAccessedDate > session.LastAccessedDate);
        }

        [TestMethod]
        public void LoginUsingEmailAddress_Succeeds()
        {
            CustomerService cService = new CustomerService();
            DBSession session;

            // Make sure no session exists first.
            Common.CleanupSession(Constants.account_user_id, SystemType.Medianet);

            session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, Constants.account_password, SystemType.Medianet);
            Assert.IsTrue(IsValidSession(session));
        }

        [TestMethod]
        public void LoginPartialAuthorisation_Succeeds()
        {
            CustomerService cService = new CustomerService();
            DBSession session;

            // Make sure no session exists first.
            Common.CleanupSession(Constants.account_user_id, SystemType.Medianet);

            session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, Constants.account_password, SystemType.Medianet);
            Assert.IsTrue(IsValidSession(session));
            cService.Logout(session.Key);

            // now do the partial authorisation part.
            session = cService.LoginPartialAuthorisation(Constants.account_emailaddress, session.User.ExpiryToken, SystemType.Medianet);
            Assert.IsTrue(IsValidSession(session) && !session.IsAuthorised);
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException<InvalidLoginFault>))]
        public void LoginPartialAuthorisation_ThrowsInvalidLoginFault()
        {
            CustomerService cService = new CustomerService();
            DBSession session;
            // Make sure no session exists first.
            Common.CleanupSession(Constants.account_user_id, SystemType.Medianet);

            session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, Constants.account_password, SystemType.Medianet);
            Assert.IsTrue(IsValidSession(session));
            cService.Logout(session.Key);
            // now do the partial authorisation part.
            session = cService.LoginPartialAuthorisation(Constants.account_emailaddress, Guid.NewGuid(), SystemType.Medianet);
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException<InvalidLoginFault>))]
        public void LoginUsingEmailAddressWithWrongPassword_ThrowsInvalidLoginFault()
        {
            CustomerService cService = new CustomerService();
            DBSession session;

            session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, "wrongpassword", SystemType.Medianet);
        }

        [TestMethod]
        public void GetAccount_Succeeds()
        {
            CustomerService cService = new CustomerService();
            DBSession session;
            Account account;

            session = cService.LoginUsingEmailAddress(Constants.account_billed_emailaddress, Constants.account_billed_password, SystemType.Medianet);

            account = cService.GetAccount(session.UserId, session.Key);

            Assert.IsTrue(account != null && string.IsNullOrWhiteSpace(account.Password) && account.Timezone != null);
        }

        [TestMethod]
        public void GetAccount_Debtor10_Succeeds()
        {
            CustomerService cService = new CustomerService();
            DBSession session;
            Account account;

            session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, Constants.account_password, SystemType.Medianet);

            account = cService.GetAccount(session.UserId, session.Key);

            Assert.IsTrue(account != null && string.IsNullOrWhiteSpace(account.Password));
        }

        [TestMethod]
        public void AddAccount_Succeeds()
        {
            CustomerService cService = new CustomerService();
            var account = new NewAccountRequest();
            Random r = new Random();
            string randomString = r.Next().ToString();
            string debtorNumber;
            List<IndustryCode> industryCodes;

            industryCodes = cService.GetIndustryCodes();

            account.FirstName = "david";
            account.LastName = randomString;
            account.CompanyName = "AAP " + randomString;
            account.IndustryCode = industryCodes[0].Code;
            account.ContactPosition = "Developer";
            account.AddressLine1 = "1 George St";
            account.AddressLine2 = "Sydney";
            account.City = "Sydney";
            account.State = "NSW";
            account.Postcode = "2000";
            account.Country = "Australia";
            account.EmailAddress = "david." + randomString + "@aap.com.au";
            account.TelephoneNumber = "02 9322 8000";
            account.FaxNumber = "02 9322 8086";
            account.ABN = "";
            account.TimezoneCode = Medianet.Service.BusinessLayer.Common.Constants.TIMEZONE_SYD;
            account.System = SystemType.Medianet;
            account.Password = "password1";
            account.AccountType = CustomerBillingType.Creditcard;

            debtorNumber = cService.AddAccount(account);

            Assert.IsTrue(!string.IsNullOrWhiteSpace(debtorNumber));
        }

        [TestMethod]
        public void AddAccountWithNoFaxOrAddress2_Succeeds()
        {
            CustomerService cService = new CustomerService();
            var account = new NewAccountRequest();
            Random r = new Random();
            string randomString = r.Next().ToString();
            string debtorNumber;
            List<IndustryCode> industryCodes;

            industryCodes = cService.GetIndustryCodes();

            account.FirstName = "david";
            account.LastName = randomString;
            account.CompanyName = "AAP " + randomString;
            account.IndustryCode = industryCodes[0].Code;
            account.ContactPosition = "Developer";
            account.AddressLine1 = "1 George St";
            //account.AddressLine2 = "Sydney";   commented to check optional fields work
            account.City = "Sydney";
            account.State = "NSW";
            account.Postcode = "2000";
            account.Country = "Australia";
            account.EmailAddress = "david." + randomString + "@aap.com.au";
            account.TelephoneNumber = "02 9322 8000";
            //account.FaxNumber = "02 9322 8086"; commented to check optional fields work
            account.ABN = "";
            account.TimezoneCode = Medianet.Service.BusinessLayer.Common.Constants.TIMEZONE_SYD;
            account.System = SystemType.Medianet;
            account.Password = "password1";
            account.AccountType = CustomerBillingType.Creditcard;

            debtorNumber = cService.AddAccount(account);

            Assert.IsTrue(!string.IsNullOrWhiteSpace(debtorNumber));
        }

        [TestMethod]
        public void AddRegistrationValidation_Succeeds()
        {
            CustomerService cService = new CustomerService();
            Random r = new Random();
            string randomString = r.Next().ToString();
            Guid token;

            token = cService.AddRegistrationValidation("david." + randomString + "@aap.com.au", "David", SystemType.Medianet);

            Assert.IsTrue(token != null);
        }

        [TestMethod]
        public void GetRegistrationValidation_Succeeds()
        {
            CustomerService cService = new CustomerService();
            Random r = new Random();
            string randomString = r.Next().ToString();
            string emailAddress = "david." + randomString + "@aap.com.au";
            string firstName = "david";
            Guid token;
            var rego = new RegistrationValidation();

            token = cService.AddRegistrationValidation(emailAddress, firstName, SystemType.Medianet);
            Assert.IsTrue(token != null);

            rego = cService.GetRegistrationValidation(token);

            Assert.IsTrue(rego != null);
            Assert.AreEqual(SystemType.Medianet, rego.System);
            Assert.AreEqual(emailAddress, rego.EmailAddress);
            Assert.AreEqual(firstName, rego.FirstName);
        }

        [TestMethod]
        public void GetGetIndustryCodes_Succeeds()
        {
            CustomerService cService = new CustomerService();
            List<IndustryCode> retval;

            retval = cService.GetIndustryCodes();

            Assert.IsTrue(retval != null);
        }

        //[TestMethod]
        //public void GetRegistrationValidation()
        //{
        //    CustomerService cService = new CustomerService();
        //    var rego = new RegistrationValidation();

        //    Guid token = new Guid("C92F49DF-72F7-47BC-B0B3-FC6396256052");

        //    rego = cService.GetRegistrationValidation(token);

        //    Assert.IsTrue(rego != null);
        //}

        [TestMethod]
        public void UpdateAccount_Succeeds()
        {
            CustomerService cService = new CustomerService();
            DBSession session;
            Account account;
            Account newAccount;
            Random r = new Random();

            session = cService.LoginUsingEmailAddress(Constants.account_billed_emailaddress, Constants.account_billed_password, SystemType.Medianet);

            account = cService.GetAccount(session.UserId, session.Key);

            account.CompanyAddress.AddressLine3 = "Level " + r.Next(100).ToString();
            account.TimezoneCode = "SYD";
            cService.UpdateAccount(account, session.Key);

            newAccount = cService.GetAccount(session.UserId, session.Key);

            Assert.IsTrue(newAccount.CompanyAddress.AddressLine3 == account.CompanyAddress.AddressLine3);
        }

        // This test no longer fails in the Finance system.
        //[TestMethod]
        //[ExpectedException(typeof(FaultException<UpdateFault>))]
        //public void UpdateAccount_ChangeNameOfInvoiceCustomer_ThrowsException() {
        //    CustomerService cService = new CustomerService();
        //    DBSession session;
        //    Account account;
        //    Random r = new Random();

        //    session = cService.LoginUsingEmailAddress(Constants.account_billed_emailaddress, Constants.account_billed_password, SystemType.Medianet);

        //    account = cService.GetAccount(session.UserId, session.Key);

        //    account.CompanyName = "New name is " + r.Next(100).ToString();
        //    cService.UpdateAccount(account, session.Key);
        //}

        [TestMethod]
        [ExpectedException(typeof(FaultException<LogonLicensesExceededFault>))]
        public void Login_NoContactsSessionsLeft_ThrowsError()
        {
            CustomerService cService = new CustomerService();
            var dbContext = new MedianetContext();
            Medianet.Model.Entities.Customer dbCust;
            List<Medianet.Model.Entities.DBSession> dbSessions;
            DBSession session;
            short licenseCount;

            // Log everyone out of the Medianet Contacts website for DebtorNumber 10.
            dbSessions = dbContext.DBSessions.Where(s => s.DebtorNumber == "10" && s.RowStatus == "A" && s.System == "P" && s.IsImpersonating == false).ToList();
            foreach (var s in dbSessions)
            {
                s.RowStatus = ((char)RowStatusType.Inactive).ToString();
            }
            dbContext.SaveChanges();

            // Set the max license count to 1 so we can test more easily.
            dbCust = dbContext.Customers.Where(c => c.DebtorNumber == "10").First();
            licenseCount = dbCust.MediaContactsLicenseCount;
            dbCust.MediaContactsLicenseCount = 1;
            dbContext.SaveChanges();

            // Login to the Contacts website to use up the 1 license.
            session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, Constants.account_password, SystemType.Contacts);
            System.Threading.Thread.Sleep(500);

            try
            {
                // Attempt to logon using a different user. This should fail.
                session = cService.LoginUsingEmailAddress(Constants.account_alt_emailaddress, Constants.account_alt_password, SystemType.Contacts);
            }
            finally
            {
                dbCust.MediaContactsLicenseCount = licenseCount;
                dbContext.SaveChanges();
                cService.Logout(session.Key);
            }
        }

        [TestMethod]
        public void SendForgotPasswordEmail_Succeeds()
        {
            CustomerService cService = new CustomerService();

            cService.SendForgotPasswordEmail(Constants.account_emailaddress, SystemType.Medianet);
        }

        [TestMethod]
        public void SendForgotPasswordEmail_LoginUsingUsernameAtCompanyName_Succeeds()
        {
            CustomerService cService = new CustomerService();

            cService.SendForgotPasswordEmail(Constants.account_username + "@" + Constants.account_companylogon, SystemType.Medianet);
        }

        [TestMethod]
        public void SendForgotPasswordEmail_LoginToJournalistsWithOnlyUsername_Succeeds()
        {
            CustomerService cService = new CustomerService();

            cService.SendForgotPasswordEmail(Constants.account_username, SystemType.Journalists);
        }

        [TestMethod]
        public void Get_User_list_By_DebtorNo_Succeeds()
        {
            CustomerService cService = new CustomerService();
            DBSession session;
            session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, Constants.account_password, SystemType.Medianet);

            var users = cService.GetAllUsersByCustomer(Constants.debtor_number_default, session.Key);

            Assert.IsNotNull(users);
            Assert.IsTrue(users.Count > 0);
            Assert.IsTrue(users.Where(u => u.RowStatus == RowStatusType.Inactive).Count() == 0);
            Assert.IsTrue(users.Where(u => u.RowStatus == RowStatusType.Deleted).Count() == 0);
            Assert.IsTrue(users.Where(u => u.RowStatus == RowStatusType.Active).Count() == users.Count);
            Assert.IsTrue(users.Where(u => u.EmailAddress == null).Count() == 0);


        }

        [TestMethod]
        public void GetCustomerBillingCodes_Succeeds()
        {
            CustomerService cService = new CustomerService();
            List<CustomerBillingCode> retval;
            DBSession session;
            session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, Constants.account_password, SystemType.Medianet);

            retval = cService.GetCustomerBillingCodes(Constants.debtor_number_default, session.Key);

            Assert.IsTrue(retval != null);
        }

        [TestMethod]
        public void AcknowledgeUserMessageRead()
        {
            CustomerService cService = new CustomerService();
            var dbContext = new MedianetContext();
            DBSession session;
            Medianet.Model.Entities.User dbUser;

            session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, Constants.account_password, SystemType.Medianet);

            // Set the HasViewedDistributionMessage flag to false manually first.
            dbUser = dbContext.Users.Where(u => u.Id == session.UserId).FirstOrDefault();
            Assert.IsNotNull(dbUser);
            dbUser.HasViewedDistributionMessage = false;
            dbContext.SaveChanges();

            cService.AcknowledgeUserMessageRead(session.UserId, SystemType.Medianet, session.Key);

            dbContext.Entry(dbUser).Reload();

            Assert.IsNotNull(dbUser);
            Assert.IsTrue(dbUser.HasViewedDistributionMessage.Value);
        }

        [TestMethod]
        public void AcknowledgeTermsRead()
        {
            CustomerService cService = new CustomerService();
            var dbContext = new MedianetContext();
            DBSession session;
            Medianet.Model.Entities.Customer dbCustomer;

            session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, Constants.account_password, SystemType.Medianet);

            // Set the HasViewedDistributionMessage flag to false manually first.
            dbCustomer = dbContext.Customers.FirstOrDefault(c => c.DebtorNumber == session.DebtorNumber);
            Assert.IsNotNull(dbCustomer);
            dbCustomer.HasViewedTermsAndConditionsM = false;
            dbContext.SaveChanges();

            cService.AcknowledgeTermsReadAndLogin(Constants.account_emailaddress, Constants.account_password, SystemType.Medianet);

            dbContext.Entry(dbCustomer).Reload();

            Assert.IsNotNull(dbCustomer);
            Assert.IsTrue(dbCustomer.HasViewedTermsAndConditionsM.Value);
        }

        [TestMethod]
        public void GetCustomerStatistics_Succeeds()
        {
            CustomerService cService = new CustomerService();
            CustomerStatistic retval;
            DBSession session;
            session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, Constants.account_password, SystemType.Medianet);

            retval = cService.GetCustomerStatistics(Constants.debtor_number_default, session.Key);

            Assert.IsTrue(retval != null);
        }

        [TestMethod()]
        public void HashPassword()
        {
            byte[] salt = PasswordHelper.GenerateSalt();
            string sSalt = Convert.ToBase64String(salt);
            string pass1 = PasswordHelper.HashPassword("password.1", salt);

            Assert.IsNotNull(pass1);
        }

        [TestMethod()]
        public void CompareHashedPasswords() {
            byte[] salt = PasswordHelper.GenerateSalt();
            string pass1 = PasswordHelper.HashPassword("password.1", salt);
            bool passwordsEqual = PasswordHelper.ComparePasswords(PasswordHelper.HashPassword("password.1", salt), pass1);

            Assert.IsTrue(passwordsEqual);
        }

        [TestMethod()]
        public void AcknowledgeTermsRead_Works()
        {
            using (var uow = new UnitOfWork())
            {
                CustomerBL custBl = new CustomerBL(uow);

                custBl.AcknowledgeTermsRead(Constants.account_user_id, "David Urquhart", Constants.account_emailaddress, SystemType.Medianet);
            }
        }

        #region Private methods

        private bool IsValidSession(DBSession session)
        {
            return (session != null && !string.IsNullOrWhiteSpace(session.Key) && session.Key != "0" &&
                    session.RowStatus == RowStatusType.Active && session.User != null && session.Customer != null &&
                    session.Customer.Timezone != null);
        }

        #endregion
    }
}
