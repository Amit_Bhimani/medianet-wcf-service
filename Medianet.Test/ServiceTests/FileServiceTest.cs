﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.ServiceModel;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Medianet.Service;
using Medianet.Service.Contracts;
using Medianet.Service.Dto;
using Medianet.Service.Common;
using Medianet.Model;
using Medianet.Service.FaultExceptions;

namespace Medianet.Test
{
    /// <summary>
    ///This is a test class for FileServiceTest and is intended
    ///to contain all FileServiceTest Unit Tests
    ///</summary>
    [TestClass()]
    public class FileServiceTest
    {
        public static DBSession _session;

        private const string UNVERIFIED_TEMP_SESSION = "UnverifiedTempFileImpersonationKey";

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes

        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext) {
            if (!AutoMapperConfigurator.IsInitialized)
                AutoMapperConfigurator.Configure();

            //var cService = new CustomerService();
            //_session = cService.LoginUsingEmailAddress(Constants.account_alt_emailaddress, Constants.account_alt_password, SystemType.Medianet);

            // Create an impersonating session so another test doesn't log us out.
            _session = Common.CreateImpersonatingSession(Constants.account_alt_user_id, SystemType.Medianet);

        }

        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        [TestMethod]
        [DeploymentItem("TestFiles", "TestFiles")]
        public void UploadTempFile_Works() {
            var cService = new CustomerService();
            var fService = new FileService();
            string filePath = Path.GetFullPath("TestFiles\\Release.docx");
            DBSession session;
            var tfs = new TempFileUploadRequest();
            var tfr = new TempFileUploadResponse();

            session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, Constants.account_password, SystemType.Medianet);

            tfs.ContentType = "application/msword";
            tfs.FileName = "test.docx";
            tfs.SessionKey = session.Key;
            tfs.FileData = System.IO.File.OpenRead(filePath);

            tfr = fService.UploadTempFile(tfs);

            Assert.IsTrue(tfr != null && tfr.TempFileId > 0);
        }

        [TestMethod]
        [DeploymentItem("TestFiles", "TestFiles")]
        public void UploadTempFileUnverified_Works()
        {
            var cService = new CustomerService();
            var fService = new FileService();
            string filePath = Path.GetFullPath("TestFiles\\Release.docx");
            var tfs = new TempFileUploadRequest();
            var tfr = new TempFileUploadResponse();

            int UnverifiedKey = int.Parse(System.Configuration.ConfigurationManager.AppSettings[UNVERIFIED_TEMP_SESSION]);
            tfs.ContentType = "application/msword";
            tfs.FileName = "test.docx";
            tfs.SessionKey = UnverifiedKey.ToString();
            tfs.FileData = System.IO.File.OpenRead(filePath);

            tfr = fService.UploadTempFileUnverified(tfs);

            Assert.IsTrue(tfr != null && tfr.TempFileId > 0);
        }

        [TestMethod]
        [DeploymentItem("TestFiles", "TestFiles")]
        public void GetTempFileContent_Works() {
            var cService = new CustomerService();
            var fService = new FileService();
            DBSession session;
            DocumentSummary doc;
            var fcRequest = new FileContentRequest();
            var fcResponse = new FileContentResponse();

            session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, Constants.account_password, SystemType.Medianet);

            // Fidst upload a file.
            doc = Common.GenerateAttachmentDocument(1, session);

            fcRequest.FileId = doc.TempFileId.Value;
            fcRequest.SessionKey = session.Key;

            fcResponse = fService.GetTempFileContent(fcRequest);

            Assert.IsTrue(fcResponse != null && fcResponse.FileData.Length > 0);
        }

        [TestMethod]
        [DeploymentItem("TestFiles", "TestFiles")]
        public void GetTempFileDerivedContent_Works() {
            var fService = new FileService();
            var gService = new GeneralService();
            string filePath = Path.GetFullPath("TestFiles\\Release.docx");
            DocumentSummary relDocument;
            DocumentSummary dataDocument;
            var fcRequest = new DerivedFileContentRequest();
            FileContentResponse fcResponse;

            relDocument = Common.GenerateMailmergeFaxReleaseDocument(1, _session);
            dataDocument = Common.GenerateMailmergeFaxDataSourceDocument(2, _session);

          
            fcRequest.FileId = relDocument.TempFileId.Value;
            fcRequest.SessionKey = _session.Key;

            fcResponse = fService.GetTempFileDerivedContent(fcRequest);

            Assert.IsTrue(fcResponse != null && fcResponse.FileData.Length > 0 && fcResponse.FileExtension == "tif");
        }

        [TestMethod]
        public void GetInboundFileContent_Works() {
            var cService = new CustomerService();
            var fService = new FileService();
            DBSession session;
            var fcRequest = new FileContentRequest();
            var fcResponse = new FileContentResponse();
            string outputFile;
            FileInfo info;

            session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, Constants.account_password, SystemType.Medianet);

            fcRequest.FileId = 83104;
            fcRequest.SessionKey = session.Key;

            fcResponse = fService.GetInboundFileContent(fcRequest);

            // Copy to the file system to make sure the file is ok.
            outputFile = Path.GetTempFileName();

            try {
                using (var fs = File.Create(outputFile)) {
                    fcResponse.FileData.CopyTo(fs);
                }

                info = new FileInfo(outputFile);

                Assert.IsTrue(fcResponse != null && fcResponse.FileData.Length > 0 && info.Length == fcResponse.FileData.Length);
            }
            finally {
                if (File.Exists(outputFile))
                    File.Delete(outputFile);
            }
        }

        [TestMethod]
        public void GetThumbnailContent_Works() {
            var fService = new FileService();
            var fcRequest = new FileContentRequest();
            var fcResponse = new FileContentResponse();
            string outputFile;
            FileInfo info;

            fcRequest.FileId = 4008;
            fcResponse = fService.GetThumbnailContent(fcRequest);

            // Copy to the file system to make sure the file is ok.
            outputFile = Path.GetTempFileName();

            try {
                using (var fs = File.Create(outputFile)) {
                    fcResponse.FileData.CopyTo(fs);
                }

                info = new FileInfo(outputFile);

                Assert.IsTrue(fcResponse != null && fcResponse.FileData.Length > 0 && info.Length == fcResponse.FileData.Length);
            }
            finally {
                if (File.Exists(outputFile))
                    File.Delete(outputFile);
            }
        }

        [TestMethod]
        public void GetDocumentContent_ByReleaseIdSeqNoSecurityKey_Works() {
            var dcRequest = new DocumentContentRequest();

            dcRequest.ReleaseId = Constants.aap_release_id;
            dcRequest.SequenceNumber = -1; // -1 is the Release document.
            dcRequest.SecurityKey = Constants.aap_release_securitykey;

            ValidateDocumentDownloads(dcRequest);
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException<AccessDeniedFault>))]
        public void GetDocumentContent_ByReleaseIdSeqNoSecurityKey_FailsIfInvalidSecurityKeyAndNotPublic() {
            var dcRequest = new DocumentContentRequest();

            dcRequest.ReleaseId = Constants.release_id_not_public;
            dcRequest.SequenceNumber = -1; // -1 is the Release document.
            dcRequest.SecurityKey = "wrong key";

            ValidateDocumentDownloads(dcRequest);
        }

        [TestMethod]
        public void GetDocumentContent_ByReleaseIdSeqNoSessionKeyKey_Works() {
            var cService = new CustomerService();
            DBSession session;
            var dcRequest = new DocumentContentRequest();

            session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, Constants.account_password, SystemType.Medianet);

            dcRequest.ReleaseId = 734682;
            dcRequest.SequenceNumber = -1; // -1 is the Release document.
            dcRequest.SessionKey = session.Key;

            ValidateDocumentDownloads(dcRequest);
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException<AccessDeniedFault>))]
        public void GetDocumentContent_ByReleaseIdSeqNoSessionKeyKey_FailsIfSessionIsWrongCustomerAndNotPublic() {
            var cService = new CustomerService();
            DBSession session;
            var dcRequest = new DocumentContentRequest();

            session = cService.LoginUsingEmailAddress(Constants.account_billed_emailaddress, Constants.account_billed_password, SystemType.Medianet);

            dcRequest.ReleaseId = Constants.release_id_not_public;
            dcRequest.SequenceNumber = -1; // -1 is the Release document.
            dcRequest.SessionKey = session.Key;

            ValidateDocumentDownloads(dcRequest);
        }

        [TestMethod]
        public void GetDocumentContent_ByDocumentIdOnly_WorksIfPublic() {
            var cService = new CustomerService();
            var dcRequest = new DocumentContentRequest();

            dcRequest.DocumentId = Constants.document_id_public;

            ValidateDocumentDownloads(dcRequest);
        }

        [TestMethod]
        public void GetWebsiteLogo_Works() {
            var fService = new FileService();
            var fcRequest = new WebContentRequest();
            var fcResponse = new FileContentResponse();
            string outputFile;
            FileInfo info;

            fcRequest.WebsiteId = 10;

            fcResponse = fService.GetWebsiteLogo(fcRequest);

            // Copy to the file system to make sure the file is ok.
            outputFile = Path.GetTempFileName();

            try {
                using (var fs = File.Create(outputFile)) {
                    fcResponse.FileData.CopyTo(fs);
                }

                info = new FileInfo(outputFile);

                Assert.IsTrue(fcResponse != null && fcResponse.FileData.Length > 0 && info.Length == fcResponse.FileData.Length);
            }
            finally {
                if (File.Exists(outputFile))
                    File.Delete(outputFile);
            }
        }

        private void ValidateDocumentDownloads(DocumentContentRequest fcRequest) {
            var fService = new FileService();
            var fcResponse = new FileContentResponse();
            string outputFile;
            FileInfo info;

            fcResponse = fService.GetDocumentContent(fcRequest);

            // Copy to the file system to make sure the file is ok.
            outputFile = Path.GetTempFileName();

            try {
                using (var fs = File.Create(outputFile)) {
                    fcResponse.FileData.CopyTo(fs);
                }

                info = new FileInfo(outputFile);

                Assert.IsTrue(fcResponse != null && fcResponse.FileData.Length > 0 && info.Length == fcResponse.FileData.Length);
            }
            finally {
                if (File.Exists(outputFile))
                    File.Delete(outputFile);
            }
        }
    }
}
