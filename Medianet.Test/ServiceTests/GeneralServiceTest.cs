﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.ServiceModel;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Medianet.Service;
using Medianet.Service.Contracts;
using Medianet.Service.Dto;
using Medianet.Service.Common;
using Medianet.Model;
using Medianet.Service.FaultExceptions;

namespace Medianet.Test
{
    
    
    /// <summary>
    ///This is a test class for GeneralServiceTest and is intended
    ///to contain all GeneralServiceTest Unit Tests
    ///</summary>
    [TestClass()]
    public class GeneralServiceTest
    {
        public static DBSession _session;

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes

        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext) {
            if (!AutoMapperConfigurator.IsInitialized)
                AutoMapperConfigurator.Configure();

            //var cService = new CustomerService();
            //_session = cService.LoginUsingEmailAddress(Constants.account_alt_emailaddress, Constants.account_alt_password, SystemType.Medianet);

            // Create an impersonating session so another test doesn't log us out.
            _session = Common.CreateImpersonatingSession(Constants.account_alt_user_id, SystemType.Medianet);
        }

        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        [TestMethod]
        public void GetInboundFileByDebtorNumber_Succeeds() {
            var gService = new GeneralService();
            var cService = new CustomerService();
            InboundFilePage page;
            DBSession session;

            session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, Constants.account_password, SystemType.Medianet);

            page = gService.GetInboundFilesByDebtorNumber(session.DebtorNumber, MessageType.RadioRelease, 0, 10, InboundFileSortOrderType.Id, session.Key);

            Assert.IsTrue(page.InboundFiles != null && page.InboundFiles.Count > 0 && page.TotalCount >= page.InboundFiles.Count && page.InboundFiles.Count <= page.PageSize);
        }

        [TestMethod]
        public void GetMediaStatistics_Succeeds() {
            var gService = new GeneralService();
            MediaStatistics stats;

            stats = gService.GetMediaStatistics();

            Assert.IsTrue(stats != null && stats.InternationalYearToDate > 0);
        }

        /// <summary>
        ///A test for GetTimezones
        ///</summary>
        [TestMethod()]
        public void GetTimezones_Succeeds() {
            var gService = new GeneralService();
            List<Timezone> timezones;
            //var x = TimeZoneInfo.GetSystemTimeZones();
            //TimeZoneInfo.FindSystemTimeZoneById("(UTC+10:00) Canberra, Melbourne, Sydney");

            timezones = gService.GetTimezones();

            Assert.IsTrue(timezones != null && timezones.Count > 0);
        }

        [TestMethod()]
        public void RecordEmailOpenedTest_NoDistributionId_Works() {
            var gService = new GeneralService();
            var rService = new ReleaseService();
            var cService = new CustomerService();
            DBSession session;
            List<Result> results1, results2;

            session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, Constants.account_password, SystemType.Medianet);

            results1 = rService.GetReleaseResults(Constants.aap_release_id, Constants.aap_transaction_id, session.Key);
            gService.RecordEmailOpened(Constants.aap_release_id, Constants.aap_transaction_id, null);
            results2 = rService.GetReleaseResults(Constants.aap_release_id, Constants.aap_transaction_id, session.Key);

            Assert.AreEqual(results1[0].OpenCount + 1, results2[0].OpenCount);
        }

        [TestMethod()]
        public void RecordEmailOpenedTest_Works() {
            var gService = new GeneralService();
            var rService = new ReleaseService();
            var cService = new CustomerService();
            DBSession session;
            List<Result> results1, results2;

            session = cService.LoginUsingEmailAddress(Constants.account_billed_emailaddress, Constants.account_billed_password, SystemType.Medianet);

            results1 = rService.GetReleaseResults(Constants.results_release_id, Constants.results_transaction_id, session.Key);
            gService.RecordEmailOpened(Constants.results_release_id, Constants.results_transaction_id, Constants.results_distribution_id);
            results2 = rService.GetReleaseResults(Constants.results_release_id, Constants.results_transaction_id, session.Key);

            Assert.AreEqual(results1.Where(r => r.DistributionId == Constants.results_distribution_id).First().OpenCount + 1,
                            results2.Where(r => r.DistributionId == Constants.results_distribution_id).First().OpenCount);
        }

        /// <summary>
        ///A test for RenameInboundFileById
        ///</summary>
        [TestMethod()]
        public void RenameInboundFileByIdTest() {
            var gService = new GeneralService();
            var cService = new CustomerService();
            InboundFile file;
            string newName;
            DBSession session;

            session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, Constants.account_password, SystemType.Medianet);

            file = gService.GetInboundFileById(Constants.inbound_file_id, session.Key);
            newName = string.Format("new-{0}{1}", Guid.NewGuid().ToString().Truncate(25), Path.GetExtension(file.OriginalFilename));

            gService.RenameInboundFileById(Constants.inbound_file_id, newName, session.Key);
            file = gService.GetInboundFileById(Constants.inbound_file_id, session.Key);

            Assert.IsTrue(file != null && file.OriginalFilename == newName);
        }

        /// <summary>
        ///A test for GetWebCategories
        ///</summary>
        [TestMethod()]
        public void GetWebCategoriesTest() {
            var gService = new GeneralService();
            List<WebCategory> categories;

            categories = gService.GetWebCategories();

            Assert.IsTrue(categories != null && categories.Count > 0);
        }

        /// <summary>
        ///A test for GetTwitterCategories
        ///</summary>
        [TestMethod()]
        public void GetTwitterCategoriesTest() {
            var gService = new GeneralService();
            List<TwitterCategory> categories;

            categories = gService.GetTwitterCategories();

            Assert.IsTrue(categories != null && categories.Count > 0);
        }


        /// <summary>
        ///A test for GetCareers
        ///</summary>
        [TestMethod()]
        public void GetCareers_Works()
        {
            var gService = new GeneralService();
            Career careers;

            careers = gService.GetCareers();

            Assert.IsTrue(careers != null && !string.IsNullOrWhiteSpace(careers.Description));
        }

        /// <summary>
        ///A test for GetTrainingCourse
        ///</summary>
        [TestMethod()]
        public void GetTrainingCourse_Works()
        {
            var gService = new GeneralService();
            var courseId = Common.GetTrainingCourseId(); 
            TrainingCourse entity;

            entity = gService.GetTrainingCourse(courseId);

            Assert.IsTrue(entity != null && entity.Schedules.Count > 0 && entity.Schedules[0].Enrolments.Count > 0);
        }
    }
}
