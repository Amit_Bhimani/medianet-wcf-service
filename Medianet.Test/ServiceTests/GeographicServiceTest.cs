﻿using Medianet.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Medianet.Service.Dto;
using System.Collections.Generic;

namespace Medianet.Test
{
    
    
    /// <summary>
    ///This is a test class for GeographicServiceTest and is intended
    ///to contain all GeographicServiceTest Unit Tests
    ///</summary>
    [TestClass()]
    public class GeographicServiceTest
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        [TestMethod]
        public void GetCityById_Succeeds() {
            var gService = new GeographicService();
            City city;

            city = gService.GetCityById(270022);

            Assert.IsTrue(city != null && city.Id > 0);
        }

        [TestMethod]
        public void GetCities_Succeeds() {
            var gService = new GeographicService();
            List<City> cities;

            cities = gService.GetCities();

            Assert.IsTrue(cities != null && cities.Count > 0 && cities[0].Id > 0);
        }

        [TestMethod]
        public void GetCitiesByAny_AllSupplied_Succeeds() {
            var gService = new GeographicService();
            List<City> cities;

            cities = gService.GetCitiesByAny(30, 210, 39000, "Sy");

            Assert.IsTrue(cities != null && cities.Count > 0 && cities[0].Id > 0);
        }

        [TestMethod]
        public void GetCitiesByAny_NullContinent_Succeeds() {
            var gService = new GeographicService();
            List<City> cities;

            cities = gService.GetCitiesByAny(null, 210, 39000, "Sy");

            Assert.IsTrue(cities != null && cities.Count > 0 && cities[0].Id > 0);
        }

        [TestMethod]
        public void GetCitiesByAny_NullCityName_Succeeds() {
            var gService = new GeographicService();
            List<City> cities;

            cities = gService.GetCitiesByAny(30, 210, 39000, null);

            Assert.IsTrue(cities != null && cities.Count > 0 && cities[0].Id > 0);
        }

        [TestMethod]
        public void GetContinentById_Succeeds() {
            var gService = new GeographicService();
            Continent continent;

            continent = gService.GetContinentById(30);

            Assert.IsTrue(continent != null && continent.Id > 0);
        }

        [TestMethod]
        public void GetContinents_Succeeds() {
            var gService = new GeographicService();
            List<Continent> continents;

            continents = gService.GetContinents();

            Assert.IsTrue(continents != null && continents.Count > 0 && continents[0].Id > 0);
        }

        [TestMethod]
        public void GetCountryById_Succeeds() {
            var gService = new GeographicService();
            Country country;

            country = gService.GetCountryById(210);

            Assert.IsTrue(country != null && country.Id > 0);
        }

        [TestMethod]
        public void GetCountries_Succeeds() {
            var gService = new GeographicService();
            List<Country> countries;

            countries = gService.GetCountries();

            Assert.IsTrue(countries != null && countries.Count > 0 && countries[0].Id > 0);
        }

        [TestMethod]
        public void GetStates_Succeeds() {
            var gService = new GeographicService();
            List<State> states;

            states = gService.GetStates();

            Assert.IsTrue(states != null && states.Count > 0 && states[0].Id > 0);
        }
    }
}
