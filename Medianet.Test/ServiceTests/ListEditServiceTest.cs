﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.ServiceModel;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Medianet.Service;
using Medianet.Service.Contracts;
using Medianet.Service.Dto;
using Medianet.Service.Common;
using Medianet.Model;
using Medianet.Service.FaultExceptions;

namespace Medianet.Test
{
    /// <summary>
    ///This is a test class for ListEditServiceTest and is intended
    ///to contain all ListEditServiceTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ListEditServiceTest
    {
        public static DBSession _session;
        public static int _customerServiceListId;

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes

        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext) {
            if (!AutoMapperConfigurator.IsInitialized)
                AutoMapperConfigurator.Configure();

            //var cService = new CustomerService();
            //_session = cService.LoginUsingEmailAddress(Constants.account_alt_emailaddress, Constants.account_alt_password, SystemType.Medianet);

            // Create an impersonating session so another test doesn't log us out.
            _session = Common.CreateImpersonatingSession(Constants.account_alt_user_id, SystemType.Medianet);

            // Look for a particular service and get it's Id. If we can't find it then create a new service.
            // This service will be used by several tests.
            var lService = new ListService();

            var services = lService.GetServiceListsByDebtorNumber(Constants.debtor_number_default, _session.Key);
            var service = services.Where(s => s.SelectionDescription.Equals("Daves automated test list")).FirstOrDefault();

            if (service == null) {
                var lEditService = new ListEditService();
                var serviceSummary = new ServiceListUpload();

                serviceSummary.DistributionType = DistributionType.Email;
                serviceSummary.SelectionDescription = "Daves automated test list";
                serviceSummary.Recipients = new List<Recipient>();
                serviceSummary.Recipients.Add(new Recipient() { Address = "dsmith@aap.com.au", Company = "AAP", FirstName = "David", LastName = "Smith", AddressLine1 = "3 Rider Bvd" });

                _customerServiceListId = lEditService.AddServiceListFromSummary(serviceSummary, _session.Key);
            }
            else
                _customerServiceListId = service.Id;
        }

        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        [TestMethod]
        public void ListEditService_AddServiceListFromSummary_Works() {
            var lService = new ListService();
            var lEditService = new ListEditService();
            ServiceListUpload serviceSummary = new ServiceListUpload();
            int serviceId;
            ServiceList service;
            List<Recipient> recipients;

            serviceSummary.DistributionType = DistributionType.Email;
            serviceSummary.SelectionDescription = "Daves test list " + new Random().Next(9999999);
            serviceSummary.Recipients = new List<Recipient>();
            serviceSummary.Recipients.Add(new Recipient() { Address = "dsmith@aap.com.au", Company = "AAP", FirstName = "David", LastName = "Smith", AddressLine1 = "3 Rider Bvd" });
            serviceSummary.Recipients.Add(new Recipient() { Address = "jsmith@aap.com.au", Company = "AAP", FirstName = "John", LastName = "Smith", AddressLine1 = "3 Rider Bvd" });

            serviceId = lEditService.AddServiceListFromSummary(serviceSummary, _session.Key);

            service = lService.GetServiceListById(serviceId, _session.Key);
            recipients = lService.GetServiceListRecipients(serviceId, _session.Key);

            Assert.IsTrue(serviceId > 0 && service.ShouldCharge && service.ShouldDistribute && recipients.Count == 2);
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException<InvalidParameterFault>))]
        public void ListEditService_AddServiceListFromSummary_EmptyName_ThrowsException() {
            var lEditService = new ListEditService();
            ServiceListUpload serviceSummary = new ServiceListUpload();
            int serviceId;

            serviceSummary.DistributionType = DistributionType.Email;
            serviceSummary.SelectionDescription = "";
            serviceSummary.Recipients = new List<Recipient>();
            serviceSummary.Recipients.Add(new Recipient() { Address = "dsmith@aap.com.au", Company = "AAP", FirstName = "David", LastName = "Smith", AddressLine1 = "3 Rider Bvd" });
            serviceSummary.Recipients.Add(new Recipient() { Address = "jsmith@aap.com.au", Company = "AAP", FirstName = "John", LastName = "Smith", AddressLine1 = "3 Rider Bvd" });

            serviceId = lEditService.AddServiceListFromSummary(serviceSummary, _session.Key);
        }

        [TestMethod]
        public void ListEditService_RenameServiceList_Works() {
            var lService = new ListService();
            var lEditService = new ListEditService();
            ServiceListUpload serviceSummary = new ServiceListUpload();
            string newName = "Test by Dave";
            ServiceList service1;
            ServiceList service2;

            service1 = lService.GetServiceListById(_customerServiceListId, _session.Key);
            lEditService.RenameServiceList(_customerServiceListId, newName, _session.Key);
            service2 = lService.GetServiceListById(_customerServiceListId, _session.Key);
            lEditService.RenameServiceList(_customerServiceListId, service1.SelectionDescription, _session.Key);

            Assert.IsTrue(service2.SelectionDescription == newName && service2.LastModifiedDate > service1.LastModifiedDate);
        }

        [TestMethod]
        public void ListEditService_DeleteServiceList_Works() {
            var lService = new ListService();
            var lEditService = new ListEditService();
            ServiceList insertedService;
            ServiceListUpload serviceSummary = new ServiceListUpload();
            int serviceId;
            ServiceList service;
            bool wasDeleted = false;

            serviceSummary.DistributionType = DistributionType.Email;
            serviceSummary.SelectionDescription = "Daves test list " + new Random().Next(9999999);
            serviceSummary.Recipients = new List<Recipient>();
            serviceSummary.Recipients.Add(new Recipient() { Address = "dsmith@aap.com.au", Company = "AAP", FirstName = "David", LastName = "Smith", AddressLine1 = "3 Rider Bvd" });
            serviceSummary.Recipients.Add(new Recipient() { Address = "jsmith@aap.com.au", Company = "AAP", FirstName = "John", LastName = "Smith", AddressLine1 = "3 Rider Bvd" });

            serviceId = lEditService.AddServiceListFromSummary(serviceSummary, _session.Key);
            insertedService = lService.GetServiceListById(serviceId, _session.Key);

            lEditService.DeleteServiceList(serviceId, _session.Key);

            try {
                service = lService.GetServiceListById(serviceId, _session.Key);
            }
            catch (FaultException<KeyNotFoundFault>) {
                wasDeleted = true;
            }

            Assert.IsTrue(insertedService != null && wasDeleted);
        }

        [TestMethod]
        public void ListEditService_AddServiceListRecipient_Works() {
            var lService = new ListService();
            var lEditService = new ListEditService();
            ServiceListUpload serviceSummary = new ServiceListUpload();
            string emailAddress = new Random().Next(999999999).ToString() + "@aap.com.au";
            var recipient = new Recipient() { Address = emailAddress, Company = "AAP", FirstName = "David", LastName = "Smith", AddressLine1 = "3 Rider Bvd" };
            List<Recipient> recipients;
            int recipientId;

            recipientId = lEditService.AddServiceListRecipient(_customerServiceListId, recipient, _session.Key);

            recipients = lService.GetServiceListRecipients(_customerServiceListId, _session.Key);
            Assert.IsTrue(recipientId > 0 && recipients.Exists(r => r.Id == recipientId));
        }

        [TestMethod]
        public void ListEditService_UpdateAllServiceListRecipients_Works() {
            var lService = new ListService();
            var lEditService = new ListEditService();
            ServiceListUpload serviceSummary = new ServiceListUpload();
            List<Recipient> recipients;
            int serviceId;

            // First add a service so we can update later.
            serviceSummary.DistributionType = DistributionType.Email;
            serviceSummary.SelectionDescription = "Daves test list " + new Random().Next(9999999);
            serviceSummary.Recipients = new List<Recipient>();
            serviceSummary.Recipients.Add(new Recipient() { Address = "dsmith@aap.com.au", Company = "AAP", FirstName = "David", LastName = "Smith", AddressLine1 = "3 Rider Bvd" });
            serviceSummary.Recipients.Add(new Recipient() { Address = "jsmith@aap.com.au", Company = "AAP", FirstName = "John", LastName = "Smith", AddressLine1 = "3 Rider Bvd" });
            serviceId = lEditService.AddServiceListFromSummary(serviceSummary, _session.Key);

            recipients = new List<Recipient>();
            recipients.Add(new Recipient() { Address = "djones@aap.com.au", Company = "AAP", FirstName = "David", LastName = "Jones", AddressLine1 = "3 Rider Bvd" });
            recipients.Add(new Recipient() { Address = "jjones@aap.com.au", Company = "AAP", FirstName = "John", LastName = "Jones", AddressLine1 = "3 Rider Bvd" });
            recipients.Add(new Recipient() { Address = "pjones@aap.com.au", Company = "AAP", FirstName = "Peter", LastName = "Jones", AddressLine1 = "3 Rider Bvd" });

            lEditService.UpdateAllServiceListRecipients(serviceId, recipients, _session.Key);

            // Fetch all the recipients again and make sure we have only 3 recipients and all have a surname of Jones.
            recipients = lService.GetServiceListRecipients(serviceId, _session.Key);
            Assert.IsTrue(recipients.Count == 3 && !recipients.Exists(r => r.LastName != "Jones"));
        }

        [TestMethod]
        public void ListEditService_UpdateServiceListRecipient_Works() {
            var lService = new ListService();
            var lEditService = new ListEditService();
            ServiceListUpload serviceSummary = new ServiceListUpload();
            string emailAddress = new Random().Next(999999999).ToString() + "@aap.com.au";
            var recipient = new Recipient() { Address = emailAddress, Company = "AAP", FirstName = "David", LastName = "Smith", AddressLine1 = "3 Rider Bvd" };
            List<Recipient> recipients;

            // Add a new recipient.
            recipient.Id = lEditService.AddServiceListRecipient(_customerServiceListId, recipient, _session.Key);

            // Update the Recipient FirstName to "John".
            recipient.FirstName = "John";
            lEditService.UpdateServiceListRecipient(_customerServiceListId, recipient, _session.Key);

            recipients = lService.GetServiceListRecipients(_customerServiceListId, _session.Key);
            Assert.IsTrue(recipient.Id > 0 && recipients.Exists(r => r.Id == recipient.Id && r.Address == emailAddress && r.FirstName == "John"));
        }

        [TestMethod]
        public void ListEditService_DeleteServiceListRecipient_Works() {
            var lService = new ListService();
            var lEditService = new ListEditService();
            ServiceListUpload serviceSummary = new ServiceListUpload();
            string emailAddress = new Random().Next(999999999).ToString() + "@aap.com.au";
            var recipient = new Recipient() { Address = emailAddress, Company = "AAP", FirstName = "David", LastName = "Smith", AddressLine1 = "3 Rider Bvd" };
            List<Recipient> recipientsBefore;
            List<Recipient> recipientsAfter;
            int recipientId;

            recipientId = lEditService.AddServiceListRecipient(_customerServiceListId, recipient, _session.Key);
            recipientsBefore = lService.GetServiceListRecipients(_customerServiceListId, _session.Key);

            lEditService.DeleteServiceListRecipient(_customerServiceListId, recipientId, _session.Key);

            recipientsAfter = lService.GetServiceListRecipients(_customerServiceListId, _session.Key);
            Assert.IsTrue(recipientId > 0 && recipientsBefore.Exists(r => r.Id == recipientId && r.Address == emailAddress)
                                          && !recipientsAfter.Exists(r => r.Id == recipientId && r.Address == emailAddress));
        }
    }
}
