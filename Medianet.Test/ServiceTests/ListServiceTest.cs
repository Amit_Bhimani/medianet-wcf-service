﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.ServiceModel;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Medianet.Service;
using Medianet.Service.Contracts;
using Medianet.Service.Dto;
using Medianet.Service.Common;
using Medianet.Model;
using Medianet.Service.FaultExceptions;
using Medianet.DataLayer;

namespace Medianet.Test
{
    /// <summary>
    ///This is a test class for ListServiceTest and is intended
    ///to contain all ListServiceTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ListServiceTest
    {
        public static DBSession _session;

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes

        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext) {
            if (!AutoMapperConfigurator.IsInitialized)
                AutoMapperConfigurator.Configure();

            //var cService = new CustomerService();
            //_session = cService.LoginUsingEmailAddress(Constants.account_alt_emailaddress, Constants.account_alt_password, SystemType.Medianet);

            // Create an impersonating session so another test doesn't log us out.
            _session = Common.CreateImpersonatingSession(Constants.account_alt_user_id, SystemType.Medianet);
        }

        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        [TestMethod]
        public void ListService_GetServiceListById_ValidServiceId_Works() {
            var lService = new ListService();
            ServiceList list;

            list = lService.GetServiceListById(Constants.service_major_metro_all_states_email, _session.Key);

            Assert.IsTrue(list != null && list.Id == Constants.service_major_metro_all_states_email);
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException<KeyNotFoundFault>))]
        public void ListService_GetServiceListById_InvalidServiceId_ThrowsKeyNotFoundFault() {
            var lService = new ListService();
            ServiceList list;

            list = lService.GetServiceListById(99999999, _session.Key);
        }

        [TestMethod]
        public void ListService_GetServiceListsByDebtorNumber_Works() {
            var lService = new ListService();
            List<ServiceList> lists;

            lists = lService.GetServiceListsByDebtorNumberSystem(_session.DebtorNumber, _session.System, _session.Key);

            foreach (ServiceList list in lists) {
                if (list.DebtorNumber != _session.DebtorNumber || list.System != _session.System)
                    throw new Exception("Wrong DebtorNumber or System.");
            }

            Assert.IsTrue(lists != null && lists.Count > 0);
        }

        [TestMethod]
        public void ListService_GetServiceListsByDebtorNumberDistributionType_Works() {
            var lService = new ListService();
            List<ServiceListSummary> lists;

            lists = lService.GetServiceListsByDebtorNumberSystemDistributionType(_session.DebtorNumber, _session.System, DistributionType.Fax, 0, 500, _session.Key).Services;

            foreach (ServiceListSummary list in lists) {
                if (list.DistributionType != DistributionType.Fax)
                    throw new Exception("This was supposed to be a fax.");
            }

            Assert.IsTrue(lists != null);
        }

        [TestMethod]
        public void ListService_GetServiceListPageByDebtorNumberDistributionType_Works() {
            var lService = new ListService();
            ServiceListSummaryPage lists;
            int pageSize = 5;

            lists = lService.GetServiceListsByDebtorNumberSystemDistributionType(_session.DebtorNumber, _session.System, DistributionType.Fax, 0, pageSize, _session.Key);

            Assert.AreEqual(pageSize, lists.Services.Count);
        }

        [TestMethod]
        public void ListService_GetPackageListById_ValidPackageId_Works() {
            var lService = new ListService();
            PackageList list;

            list = lService.GetPackageListById(Constants.package_federal, _session.Key);

            Assert.IsTrue(list != null && list.Id == Constants.package_federal);
        }

        [TestMethod]
        public void ListService_GetTopValuePackageLists_Works()
        {
            var lService = new ListService();
            List<PackageList> lists;

            lists = lService.GetTopValuePackageLists(_session.Key);

            Assert.IsTrue(lists != null);
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException<KeyNotFoundFault>))]
        public void ListService_GetPackageListById_InvalidPackageId_ThrowsKeyNotFoundFault() {
            var lService = new ListService();
            PackageList list;

            list = lService.GetPackageListById(99999999, _session.Key);
        }

        //[TestMethod]
        //public void ListService_GetPackageListsByDebtorNumber_Works() {
        //    var lService = new ListService();
        //    List<PackageList> lists;

        //    lists = lService.GetPackageListsByDebtorNumber(_session.DebtorNumber, _session.Key);

        //    foreach (PackageList list in lists) {
        //        if (list.DebtorNumber != _session.DebtorNumber || list.System != _session.System)
        //            throw new Exception("Wrong DebtorNumber or System.");
        //    }

        //    Assert.IsTrue(lists != null && lists.Count > 0);
        //}

        [TestMethod]
        public void ListService_GetListCategories_Works() {
            var lService = new ListService();
            List<ListCategory> lists;

            lists = lService.GetListCategories(_session.Key);

            Assert.IsTrue(lists != null && lists.Count > 0);
        }

        [TestMethod]
        public void ListService_GetServiceCommentById_Works() {
            var lService = new ListService();
            ListComment comment;

            comment = lService.GetServiceCommentById(Constants.service_all_states_and_territories_audio_wire, _session.Key);

            Assert.IsTrue(comment.ServiceId == Constants.service_all_states_and_territories_audio_wire);
        }

        [TestMethod]
        public void ListService_GetPackageCommentById_Works() {
            var lService = new ListService();
            ListComment comment;

            comment = lService.GetPackageCommentById(Constants.package_federal, _session.Key);

            Assert.IsTrue(comment.PackageId == Constants.package_federal);
        }

        [TestMethod]
        public void ListService_GetServiceRecipientOutletNamesById_Works() {
            var lService = new ListService();
            List<string> outlets;

            outlets = lService.GetServiceRecipientOutletNamesById(Constants.service_major_metro_all_states_email, _session.Key);

            Assert.IsTrue(outlets != null && outlets.Count > 0);
        }

        [TestMethod]
        public void ListService_GetPackageRecipientOutletsById_Works() {
            var lService = new ListService();
            List<string> outlets;

            outlets = lService.GetPackageRecipientOutletsById(Constants.package_federal, _session.Key);

            Assert.IsTrue(outlets != null && outlets.Count > 0);
        }

        [TestMethod]
        public void ListService_GetServiceListInfoByIds_Works() {
            var lService = new ListService();
            List<int> services = new List<int>();
            List<ListInformation> info;

            services.Add(Constants.service_major_metro_all_states_fax);
            services.Add(Constants.service_major_metro_all_states_email);

            info = lService.GetServiceListInfoByIds(services, _session.Key);

            Assert.IsTrue(info != null && info.Count == 2 && info[0].OutletNames.Count > 0 && info[1].OutletNames.Count > 0);
        }

        [TestMethod]
        public void ListService_GetPackageListInfoByIds_Works() {
            var lService = new ListService();
            List<int> packages = new List<int>();
            List<ListInformation> info;

            packages.Add(Constants.package_federal);

            info = lService.GetPackageListInfoByIds(packages, _session.Key);

            Assert.IsTrue(info != null && info.Count == 1 && info[0].OutletNames.Count > 0);
        }

        [TestMethod]
        public void ListService_GetNextServiceListNumber_Works() {
            var lService = new ListService();
            int nextListNo;

            nextListNo = lService.GetNextServiceListNumber(DistributionType.Fax, _session.Key);

            Assert.IsTrue(nextListNo > 0);
        }

        [TestMethod]
        public void ListService_GetTicker_Works() {
            var lService = new ListService();
            List<ListTicker> lists;

            lists = lService.GetTicker();

            Assert.IsTrue(lists != null && lists.Count > 0);
        }

        [TestMethod]
        public void ListService_GetServiceListByDebtorNumberOutletIdContactId()
        {
            var lService = new ListService();

            var services =
                lService.GetServiceListByDebtorNumberOutletIdContactId(
                    Medianet.Service.BusinessLayer.Common.Constants.DEBTOR_NUMBER_AAP, Constants.outlet_id, null,
                    _session.Key);
            Assert.IsTrue(services.Count > 0);

            
        }

        [TestMethod]
        public void ListService_GetTopPerformingLists_Works()
        {
            var lService = new ListService();
            List<TopPerformingList> lists;

            lists = lService.GetTopPerformingLists(PeriodType.Week, 10);

            Assert.IsTrue(lists != null && lists.Count > 0);

            foreach (var l in lists)
            {
                Assert.AreEqual(l.Period, PeriodType.Week);
            }
        }

        [TestMethod]
        public void ListService_UpdateAapServiceByAccountCode_UpdateWorks()
        {
            var lService = new ListService();

            using (var uow = new UnitOfWork())
            {
                Random r = new Random();
                string newDesc = string.Format("New SelectionDescription - {0}", r.Next());
                var serviceBl = new Medianet.Service.BusinessLayer.ServiceListBL(uow);
                var service = serviceBl.GetAll(Medianet.Service.BusinessLayer.Common.Constants.DEBTOR_NUMBER_AAP, SystemType.Medianet)
                    .Where(s => s.DistributionType == DistributionType.Email).FirstOrDefault();

                Assert.IsNotNull(service);

                try
                {
                    lService.UpdateAapListByAccountCode(string.Format("S{0}", service.Id), newDesc, service.DistributionType.ToString());

                    // Now check it was changed
                    var updatedEntity = serviceBl.Get(service.Id);
                    Assert.IsNotNull(updatedEntity);
                    Assert.AreEqual(newDesc, updatedEntity.SelectionDescription);

                    // Check it was changed in nopcommerce too.
                    var nopcService = new Medianet.NopCommerce.Service.NopCommerceService();
                    var nopcProduct = nopcService.GetProduct(service.Id);
                    Assert.AreEqual(newDesc, nopcProduct.Name);
                }
                finally
                {
                    // Update the service back to what it was.
                    serviceBl.Update(service, 0);
                    uow.Save();
                }
            }
        }

        [TestMethod]
        public void ListService_UpdateAapServiceByAccountCode_AddWorks()
        {
            var lService = new ListService();

            using (var uow = new UnitOfWork())
            {
                Random r = new Random();
                int randomNum = r.Next();
                string accountCode = string.Format("S{0}", randomNum);
                string newDesc = string.Format("New SelectionDescription - {0}", r.Next());
                var serviceBl = new Medianet.Service.BusinessLayer.ServiceListBL(uow);
                var service = serviceBl.GetAllByDebtorNumberAccountCode(Medianet.Service.BusinessLayer.Common.Constants.DEBTOR_NUMBER_AAP, accountCode).FirstOrDefault();

                Assert.IsNull(service);

                try
                {
                    lService.UpdateAapListByAccountCode(accountCode, newDesc, DistributionType.Email.ToString());

                    // Now check it was created
                    service = serviceBl.GetAllByDebtorNumberAccountCode(Medianet.Service.BusinessLayer.Common.Constants.DEBTOR_NUMBER_AAP, accountCode).FirstOrDefault();
                    Assert.IsNotNull(service);
                    Assert.AreEqual(newDesc, service.SelectionDescription);
                }
                finally
                {
                    // Delete the service that we added.
                    serviceBl.Delete(service.Id, 0);
                    uow.Save();
                }
            }
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException<InvalidParameterFault>))]
        public void ListService_UpdateAapServiceByAccountCode_ChangeOfDistributionTypeFails()
        {
            var lService = new ListService();         

            using (var uow = new UnitOfWork())
            {
                var serviceBl = new Medianet.Service.BusinessLayer.ServiceListBL(uow);
                var service = serviceBl.GetAll(Medianet.Service.BusinessLayer.Common.Constants.DEBTOR_NUMBER_AAP, SystemType.Medianet)
                    .Where(s => s.DistributionType == DistributionType.Email).FirstOrDefault();

                Assert.IsNotNull(service);

                lService.UpdateAapListByAccountCode(string.Format("S{0}", service.Id), "New Description", DistributionType.Fax.ToString());
            }
        }

        [TestMethod]
        public void ListService_UpdateAapPackageByAccountCode_UpdateWorks()
        {
            var lService = new ListService();

            using (var uow = new UnitOfWork())
            {
                Random r = new Random();
                string newDesc = string.Format("New SelectionDescription - {0}", r.Next());
                var packageBl = new Medianet.Service.BusinessLayer.PackageListBL(uow);
                var package = packageBl.GetAllByDebtorNumberSystem(Medianet.Service.BusinessLayer.Common.Constants.DEBTOR_NUMBER_AAP, SystemType.Medianet).FirstOrDefault();

                Assert.IsNotNull(package);

                try
                {
                    lService.UpdateAapListByAccountCode(string.Format("P{0}", package.Id), newDesc, DistributionType.Package.ToString());

                    // Now check it was changed
                    var updatedEntity = packageBl.Get(package.Id);
                    Assert.IsNotNull(updatedEntity);
                    Assert.AreEqual(newDesc, updatedEntity.SelectionDescription);

                    // Check it was changed in nopcommerce too.
                    var nopcService = new Medianet.NopCommerce.Service.NopCommerceService();
                    var nopcProduct = nopcService.GetProduct(package.Id + 10000000);
                    Assert.AreEqual(newDesc, nopcProduct.Name);
                }
                finally
                {
                    // Update the package back to what it was.
                    packageBl.Update(package, 0);
                    uow.Save();
                }
            }
        }

        [TestMethod]
        public void ListService_UpdateAapPackageByAccountCode_AddWorks()
        {
            var lService = new ListService();

            using (var uow = new UnitOfWork())
            {
                Random r = new Random();
                int randomNum = r.Next();
                string accountCode = string.Format("P{0}", randomNum);
                string newDesc = string.Format("New SelectionDescription - {0}", r.Next());
                var packageBl = new Medianet.Service.BusinessLayer.PackageListBL(uow);
                var package = packageBl.GetAllByDebtorNumberAccountCode(Medianet.Service.BusinessLayer.Common.Constants.DEBTOR_NUMBER_AAP, accountCode).FirstOrDefault();

                Assert.IsNull(package);

                try
                {
                    lService.UpdateAapListByAccountCode(accountCode, newDesc, DistributionType.Package.ToString());

                    // Now check it was created
                    package = packageBl.GetAllByDebtorNumberAccountCode(Medianet.Service.BusinessLayer.Common.Constants.DEBTOR_NUMBER_AAP, accountCode).FirstOrDefault();
                    Assert.IsNotNull(package);
                    Assert.AreEqual(newDesc, package.SelectionDescription);
                }
                finally
                {
                    // Delete the package that we added.
                    packageBl.Delete(package.Id, 0);
                    uow.Save();
                }
            }
        }
    }
}
