﻿using System.Data.Objects;
using System.Text;
using Medianet.Model;
using Medianet.Service;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Medianet.Service.Common;
using Medianet.Service.Dto;
using System.Collections.Generic;
using Medianet.Service.FaultExceptions;
using System.ServiceModel;

namespace Medianet.Test
{
    
    
    /// <summary>
    ///This is a test class for MediaContactsServiceTest and is intended
    ///to contain all MediaContactsServiceTest Unit Tests
    ///</summary>
    [TestClass()]
    public class MediaContactsServiceTest
    {
        public static DBSession _session;
        public static DBSession _altSession;
        public static DBSession _adminSession;

        public static int _privateExportId;

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes

        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext) {
            if (!AutoMapperConfigurator.IsInitialized)
                AutoMapperConfigurator.Configure();

            var cService = new CustomerService();

            _session = Common.CreateImpersonatingSession(Constants.account_user_id, SystemType.Contacts);
            _altSession = Common.CreateImpersonatingSession(Constants.account_alt_user_id, SystemType.Contacts);
            _adminSession = Common.CreateImpersonatingSession(Constants.account_alt_user_id, SystemType.Admin);

            var mcService = new MediaContactsService();
            var export = new MediaContactsExport();

            export.Name = "Export test " + new Random().Next().ToString();
            export.DebtorNumber = _session.DebtorNumber;
            export.UserId = _session.UserId;
            export.ExportFields = "";
            export.IsPrivate = true;
            export.IsDeleted = false;
            export.ExportType = MediaContactsExportType.Lists;

            _privateExportId = mcService.AddExports(export, _session.Key);
        }

        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //

        [TestCleanup()]
        public void MyTestCleanup() {
            var mcService = new MediaContactsService();

            mcService.DeleteExports(_privateExportId, _session.Key);
        }
        
        #endregion

        [TestMethod]
        public void MediaContactsService_GetExports_Works() {
            var mcService = new MediaContactsService();
            List<MediaContactsExport> entities;

            entities = mcService.GetExports(_session.DebtorNumber, _session.UserId, MediaContactsExportType.Lists, _session.Key);

            foreach (var export in entities) {
                if (export.DebtorNumber != _session.DebtorNumber)
                    throw new Exception(string.Format("DebtorNumber of {0} should not be seen by DebtorNumber {1}", export.DebtorNumber, _session.DebtorNumber));
                if (export.UserId != _session.UserId && export.IsPrivate == true)
                    throw new Exception(string.Format("Private export from User {0} should not be seen by User {1}", export.UserId, _session.UserId));
            }
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException<KeyNotFoundFault>))]
        public void MediaContactsService_GetExportById_InvalidServiceId_ThrowsKeyNotFoundFault() {
            var mcService = new MediaContactsService();
            MediaContactsExport entity;

            entity = mcService.GetExport(99999999, _session.Key);
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException<AccessDeniedFault>))]
        public void MediaContactsService_GetExport_OtherUsersPrivateRecord_ThrowsAccessDeniedFault() {
            var mcService = new MediaContactsService();
            MediaContactsExport entity;

            entity = mcService.GetExport(_privateExportId, _altSession.Key);
        }

        /// <summary>
        ///A test for AddExports
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(FaultException<AccessDeniedFault>))]
        public void MediaContactsService_AddExports_OtherUsersPrivateRecord_ThrowsAccessDeniedFault() {
            var mcService = new MediaContactsService();
            var entity = new MediaContactsExport();

            entity.Name = "Export test " + new Random().Next().ToString();
            entity.DebtorNumber = _session.DebtorNumber;
            entity.UserId = _session.UserId;
            entity.ExportFields = "";
            entity.IsPrivate = true;
            entity.IsDeleted = false;
            entity.ExportType = MediaContactsExportType.Lists;

            _privateExportId = mcService.AddExports(entity, _altSession.Key);
        }

        [TestMethod]
        public void MediaContactsService_GetAll_OutletTypes_Works()
        {
            var mcService = new MediaContactsService();
            List<OutletType> entities;

            entities = mcService.GetOutletTypes(_adminSession.Key);

            Assert.IsTrue(entities != null && entities.Count > 0);
        }

        [TestMethod]
        public void MediaContactsService_GetOutletType_Works()
        {
            var mcService = new MediaContactsService();
            OutletType entity;

            entity = mcService.GetOutletType(Constants.outlet_type_id, _adminSession.Key);

            Assert.IsTrue(entity != null);
        }

        [TestMethod]
        public void MediaContactsService_GetAll_ProductTypes_Works()
        {
            var mcService = new MediaContactsService();
            List<ProductType> entities;

            entities = mcService.GetProductTypes(_adminSession.Key);

            Assert.IsTrue(entities != null && entities.Count > 0);
        }

        [TestMethod]
        public void MediaContactsService_GetProductTypesByOutletType_Works()
        {
            var mcService = new MediaContactsService();
            List<ProductType> entities;

            entities = mcService.GetProductTypesByOutletType(Constants.outlet_type_id, _adminSession.Key);

            Assert.IsTrue(entities != null && entities.Count > 0);
        }

        [TestMethod]
        public void MediaContactsService_GetProductType_Works()
        {
            var mcService = new MediaContactsService();
            ProductType entity;

            entity = mcService.GetProductType(Constants.product_type_id, _adminSession.Key);

            Assert.IsTrue(entity != null);
        }

        [TestMethod]
        public void MediaContactsService_GetAll_NewsFocuses_Works()
        {
            var mcService = new MediaContactsService();
            List<NewsFocus> entities;

            entities = mcService.GetNewsFocuses(_adminSession.Key);

            Assert.IsTrue(entities != null && entities.Count > 0);
        }

        [TestMethod]
        public void MediaContactsService_GetNewsFocus_Works()
        {
            var mcService = new MediaContactsService();
            NewsFocus entity;

            entity = mcService.GetNewsFocus(Constants.news_focus_id, _adminSession.Key);

            Assert.IsTrue(entity != null);
        }

        [TestMethod]
        public void MediaContactsService_GetAll_OutletFrequencies_Works()
        {
            var mcService = new MediaContactsService();
            List<OutletFrequency> entities;

            entities = mcService.GetOutletFrequencies(_adminSession.Key);

            Assert.IsTrue(entities != null && entities.Count > 0);
        }

        [TestMethod]
        public void MediaContactsService_GetOutletFrequency_Works()
        {
            var mcService = new MediaContactsService();
            OutletFrequency entity;

            entity = mcService.GetOutletFrequency(Constants.frequency_id, _adminSession.Key);

            Assert.IsTrue(entity != null);
        }

        [TestMethod]
        public void MediaContactsService_GetAll_WorkingLanguages_Works()
        {
            var mcService = new MediaContactsService();
            List<WorkingLanguage> entities;

            entities = mcService.GetWorkingLanguages(_adminSession.Key);

            Assert.IsTrue(entities != null && entities.Count > 0);
        }

        [TestMethod]
        public void MediaContactsService_GetWorkingLanguage_Works()
        {
            var mcService = new MediaContactsService();
            WorkingLanguage entity;

            entity = mcService.GetWorkingLanguage(Constants.working_language_id, _adminSession.Key);

            Assert.IsTrue(entity != null);
        }

        [TestMethod]
        public void MediaContactsService_GetAll_StationFormats_Works()
        {
            var mcService = new MediaContactsService();
            List<StationFormat> entities;

            entities = mcService.GetStationFormats(_adminSession.Key);

            Assert.IsTrue(entities != null && entities.Count > 0);
        }

        [TestMethod]
        public void MediaContactsService_GetStationFormat_Works()
        {
            var mcService = new MediaContactsService();
            StationFormat entity;

            entity = mcService.GetStationFormat(Constants.station_format_id, _adminSession.Key);

            Assert.IsTrue(entity != null);
        }

        [TestMethod]
        public void MediaContactsService_GetAll_Subject_Works()
        {
            var mcService = new MediaContactsService();
            List<Subject> entities;

            entities = mcService.GetSubjects(_adminSession.Key);

            Assert.IsTrue(entities != null && entities.Count > 0);
        }

        [TestMethod]
        public void MediaContactsService_GetSubject_Works()
        {
            var mcService = new MediaContactsService();
            Subject entity;

            entity = mcService.GetSubject(Constants.subject_id, _adminSession.Key);

            Assert.IsTrue(entity != null);
        }

        [TestMethod]
        public void MediaContactsService_GetAll_SubjectGroups_Works()
        {
            var mcService = new MediaContactsService();
            List<SubjectGroup> entities;

            entities = mcService.GetSubjectGroups(_adminSession.Key);

            Assert.IsTrue(entities != null && entities.Count > 0);
        }

        [TestMethod]
        public void MediaContactsService_GetSubjectGroup_Works()
        {
            var mcService = new MediaContactsService();
            SubjectGroup entity;

            entity = mcService.GetSubjectGroup(Constants.subject_groupid, _adminSession.Key);

            Assert.IsTrue(entity != null);
        }

        [TestMethod]
        public void MediaContactsService_GetAll_ContactRoles_Works()
        {
            var mcService = new MediaContactsService();
            List<ContactRole> entities;

            entities = mcService.GetContactRoles(_adminSession.Key);

            Assert.IsTrue(entities != null && entities.Count > 0);
        }

        [TestMethod]
        public void MediaContactsService_GetContactRole_Works()
        {
            var mcService = new MediaContactsService();
            ContactRole entity;

            entity = mcService.GetContactRole(Constants.contactrole_id, _adminSession.Key);

            Assert.IsTrue(entity != null);
        }

        [TestMethod]
        public void MediaContactsService_GetEmployee_Works()
        {
            var mcService = new MediaContactsService();
            MediaEmployee entity;

            entity = mcService.GetMediaEmployee("AAPC16738", "AAPO1", _adminSession.Key);

            Assert.IsTrue(entity != null);
        }

                [TestMethod]
        public void GetMediaEmployeesByContactId_Works()
        {
            var mcService = new MediaContactsService();
            List<MediaEmployee> entities;

            entities = mcService.GetMediaEmployeesByContactId("AAPC16738", _adminSession.Key);

            Assert.IsTrue(entities != null && entities.Count > 0);
        }

        [TestMethod]
        public void MediaContactsService_GetOutlet_Works()
        {
            var mcService = new MediaContactsService();
            MediaOutlet entity;

            entity = mcService.GetMediaOutlet(Constants.outlet_id, _adminSession.Key);

            Assert.IsTrue(entity != null);
        }

        [TestMethod]
        public void MediaContactsService_GetOutletByName_Works()
        {
            var mcService = new MediaContactsService();
            List<MediaOutlet> entity;

            entity = mcService.GetMediaOutletsByName(Constants.outlet_partial_name, _adminSession.Key);

            Assert.IsTrue(entity != null);
        }

        [TestMethod]
        public void MediaContactsService_GetContact_Works()
        {
            var mcService = new MediaContactsService();
            MediaContact entity;

            entity = mcService.GetMediaContact("AAPC16738", _adminSession.Key);

            Assert.IsTrue(entity != null);
        }

        [TestMethod]
        public void MediaContactsService_UpdateContact_Works()
        {
            var mcService = new MediaContactsService();
            MediaContact entity;

            entity = mcService.GetMediaContact("AAPC56141", _adminSession.Key);
            entity.Id = entity.Id.Trim();
            entity.MiddleName = "BILLY";
            entity.InternalNotes = "TEST INTERNAL NOTE" + DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");
            entity.Profile = "Profile Altered: " + DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");

            mcService.UpdateMediaContact(entity, _adminSession.Key);

            Assert.IsTrue(entity != null);
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException<DataConcurrencyFault>))]
        public void MediaContactsService_UpdateContact_ConcurrencyCheck()
        {
            var mcService = new MediaContactsService();
            MediaContact entity1;
            MediaContact entity2;

            entity1 = mcService.GetMediaContact("AAPC56141", _adminSession.Key);
            System.Threading.Thread.Sleep(1000);
            entity2 = mcService.GetMediaContact("AAPC56141", _adminSession.Key);

            entity2.Id = entity2.Id.Trim();
            entity2.MiddleName = "BILLY 2";
            entity2.InternalNotes = "TEST INTERNAL NOTE" + DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");
            entity2.Profile = "Profile Altered: " + DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");

            System.Threading.Thread.Sleep(1000);
            mcService.UpdateMediaContact(entity2, _adminSession.Key);
            System.Threading.Thread.Sleep(1000);

            entity1.Id = entity1.Id.Trim();
            entity1.MiddleName = "BILLY 1";
            entity1.InternalNotes = "TEST INTERNAL NOTE" + DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");
            entity1.Profile = "Profile Altered: " + DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");

            System.Threading.Thread.Sleep(1000);
            mcService.UpdateMediaContact(entity1, _adminSession.Key);

            //Assert.IsTrue(entity1 != null);
        }

        [TestMethod]
        public void MediaContactsService_AddContact_Works()
        {
            var mcService = new MediaContactsService();
            MediaContact entity = new MediaContact();
            Random r = new Random();

            entity.LastName = string.Format("WEBBER ({0})", r.Next().ToString());
            entity.MiddleName = "";
            entity.FirstName = "MARK";
            entity.DeletedDate = null;
            entity.Prefix = "Mr";
            entity.RowStatus = ContactRowStatusType.Active;
            entity.InternalNotes = "TEST INTERNAL NOTE" + DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");
            entity.LastModifiedDate = DateTime.Now;
            entity.Profile = "Profile Altered: " + DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");

            mcService.AddMediaContact(entity, _adminSession.Key);

            Assert.IsTrue(entity != null);
        }

        [TestMethod]
        public void UpdateMediaOutlet_Works()
        {
            var mcService = new MediaContactsService();
            MediaOutlet entity;

            entity = mcService.GetMediaOutlet(Constants.outlet_id, _adminSession.Key);

            entity.AddressLine2 = "NSW, AUSTRALIA";
            entity.InternalNotes = "TEST INTERNAL NOTE" + DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");
            entity.Profile = "Profile Altered: " + DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");

            mcService.UpdateMediaOutlet(entity, null, _adminSession.Key);

            Assert.IsTrue(entity != null);
        }
        [TestMethod]
        public void UpdateMediaOutlet_ServiceListChangeWorks()
        {
            var mcService = new MediaContactsService();
            MediaOutlet entity;

            entity = mcService.GetMediaOutlet("AAPO1", _adminSession.Key);

            entity.FaxNumber = "0293228086";
            mcService.UpdateMediaOutlet(entity, null, _adminSession.Key);
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException<MediaUpdateValidationFault>))]
        public void UpdateMediaOutlet_EmptyFaxNumberButInFaxLists_ThrowsError()
        {
            var mcService = new MediaContactsService();
            MediaOutlet entity;

            entity = mcService.GetMediaOutlet("AAPO1", _adminSession.Key);

            entity.FaxNumber = "";
            mcService.UpdateMediaOutlet(entity, null, _adminSession.Key);
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException<DataConcurrencyFault>))]
        public void UpdateMediaOutlet_ConcurrencyCheck()
        {
            var mcService = new MediaContactsService();
            MediaOutlet entity;
            MediaOutlet entity2;

            entity = mcService.GetMediaOutlet(Constants.outlet_id, _adminSession.Key);
            entity2 = mcService.GetMediaOutlet(Constants.outlet_id, _adminSession.Key);

            entity2.AddressLine2 = "NSW, AUSTRALIA";
            entity2.InternalNotes = "TEST INTERNAL NOTE" + DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");
            entity2.Profile = "Profile Altered: " + DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");
            mcService.UpdateMediaOutlet(entity2, null, _adminSession.Key);

            entity.AddressLine2 = "NSW, AUSTRALIA";
            entity.InternalNotes = "TEST INTERNAL NOTE" + DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");
            entity.Profile = "Profile Altered: " + DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");
            mcService.UpdateMediaOutlet(entity, null, _adminSession.Key);

        }

        [TestMethod]
        public void AddMediaOutlet_Works()
        {
            var mcService = new MediaContactsService();
            MediaOutlet entity = new MediaOutlet();
            Random r = new Random();

            entity.ABs = "";
            entity.AddressLine1 = "123 Acme Road";
            entity.Audience = null;
            entity.BlogURL = "";
            entity.CallLetters = "";
            entity.Circulation = null;
            entity.CityId = 40768;
            entity.ContinentId = 30;
            entity.CopyPrice = "";
            entity.CountryId = 210;
            entity.CreatedDate = DateTime.Now;
            entity.DeletedDate = null;
            entity.EmailAddress = "testing@acme.com";
            entity.F16 = "";
            entity.Facebook = "";
            entity.FaxNumber = "";
            entity.FrequencyId = 253;
            entity.GBs = "";
            entity.HasParent = false;
            entity.Id = "";
            entity.LastModifiedByUserId = 5;
            entity.LastModifiedDate = DateTime.Now;
            entity.LinkedIn = "";
            entity.LockedByUserId = null;
            entity.M16 = "";
            entity.Name = string.Format("ACME TESTING COMPANY PTY LTD ({0})", r.Next().ToString());
            entity.NewsFocusId = 630;
            entity.OutletTypeId = 431;
            entity.OwnershipCompany = "";
            entity.P16 = "";
            entity.ParentId = "";
            entity.PhoneNumber = "9999 9876";
            entity.PostCode = "2000";
            entity.PreferredDeliveryMethod = PreferredDeliveryType.Email;
            entity.ProductTypeId = 615;
            entity.RowStatus = OutletRowStatusType.Active;
            entity.SendToOMA = false;
            entity.StationFrequency = "666";
            entity.TrackingReference = "";
            entity.Twitter = "";
            entity.User = null;
            entity.WebSite = "";
            entity.YearEstablished = "2013";
            entity.AddressLine2 = "NSW, AUSTRALIA";
            entity.InternalNotes = "TEST INTERNAL NOTE" + DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");

            entity.Profile = "Profile Altered: " + DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");

            entity.StationFormats = new List<MediaOutletStationFormat>();
            entity.StationFormats.Add(new MediaOutletStationFormat { StationFormatId = 3036 });
            entity.StationFormats.Add(new MediaOutletStationFormat { StationFormatId = 3037 });
            entity.StationFormats.Add(new MediaOutletStationFormat { StationFormatId = 3038 });

            entity.WorkingLanguages = new List<MediaOutletWorkingLanguage>();
            entity.WorkingLanguages.Add(new MediaOutletWorkingLanguage { WorkingLanguageId = 265 });
            entity.WorkingLanguages.Add(new MediaOutletWorkingLanguage { WorkingLanguageId= 281 });
            entity.WorkingLanguages.Add(new MediaOutletWorkingLanguage { WorkingLanguageId = 503032 });

            //lists.Subjects = new List<MediaOutletSubject>();
            //lists.Subjects.Add(new MediaOutletSubject { SubjectId = 16134 });
            //lists.Subjects.Add(new MediaOutletSubject { SubjectId = 16135 });
            //lists.Subjects.Add(new MediaOutletSubject { SubjectId = 16136 });

            mcService.AddMediaOutlet(entity, _adminSession.Key);

            Assert.IsTrue(entity != null);
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException<DataConcurrencyFault>))]
        public void UpdateMediaEmployee_ConcurrencyCheck()
        {
            var mcService = new MediaContactsService();
            MediaEmployee entity;
            MediaEmployee entity2;

            entity = mcService.GetMediaEmployee("AAPC16738", "AAPO1", _adminSession.Key);
            entity2 = mcService.GetMediaEmployee("AAPC16738", "AAPO1", _adminSession.Key);

            entity2.JobTitle = "ACME WAREHOUSE MANAGER 2";
            entity2.AddressLine1 = "123 MAIN STREET 2";

            mcService.UpdateMediaEmployee(entity2, null, _adminSession.Key);

            entity.JobTitle = "ACME WAREHOUSE MANAGER";
            entity.AddressLine1 = "123 MAIN STREET";

            mcService.UpdateMediaEmployee(entity, null, _adminSession.Key);
        }

        [TestMethod]
        public void UpdateMediaEmployee_Works()
        {
            var mcService = new MediaContactsService();
            MediaEmployee entity;

            entity = mcService.GetMediaEmployee("AAPC16738", "AAPO1", _adminSession.Key);

            entity.JobTitle = "ACME WAREHOUSE MANAGER";
            entity.AddressLine1 = "123 MAIN STREET";

            //lists.EmployeeRoles.Add(new ContactRole { Id = 648 });
            //lists.EmployeeRoles.RemoveAt(0);
            //lists.EmployeeRoles.Add(new ContactRole { Id = 644, Name="Columnist" });
            //lists.EmployeeRoles.Add(new ContactRole { Id = 645, Name="Diarist" });

            //entity.Subjects.Clear();
            //entity.Subjects.Add(new MediaEmployeeSubject { ContactId = entity.ContactId, IsPrimaryContact = false, OutletId = entity.OutletId, SubjectId = 16134 });
            //entity.Subjects.Add(new MediaEmployeeSubject { ContactId = entity.ContactId, IsPrimaryContact = false, OutletId = entity.OutletId, SubjectId = 16135 });
            //entity.Subjects.Add(new MediaEmployeeSubject { ContactId = entity.ContactId, IsPrimaryContact = false, OutletId = entity.OutletId, SubjectId = 16136 });

            mcService.UpdateMediaEmployee(entity, null, _adminSession.Key);

            Assert.IsTrue(entity != null);
        }

        [TestMethod]
        public void UpdateMediaEmployee_ServiceListChangeWorks()
        {
            var mcService = new MediaContactsService();
            MediaEmployee entity;

            entity = mcService.GetMediaEmployee("AAPC16738", "AAPO1", _adminSession.Key);

            entity.FaxNumber = "0293228086";
            mcService.UpdateMediaEmployee(entity, null, _adminSession.Key);
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException<MediaUpdateValidationFault>))]
        public void UpdateMediaEmployee_EmptyFaxNumberButInFaxLists_ThrowsError()
        {
            var mcService = new MediaContactsService();
            MediaEmployee entity;

            entity = mcService.GetMediaEmployee("AAPC16738", "AAPO1", _adminSession.Key);

            entity.FaxNumber = "";
            mcService.UpdateMediaEmployee(entity, null, _adminSession.Key);
        }

        [TestMethod]
        public void MediaContactsService_AddUpdateLog_Works()
        {
            var mcService = new MediaContactsService();
            MediaDatabaseLog mdbl = new MediaDatabaseLog();
            mdbl.Accessed = DateTime.Now;
            mdbl.OutletID = Constants.outlet_id;
            mdbl.FieldsUpdated = "Subjects,";
            mdbl.UserID = _adminSession.UserId;

           int addedItem = mcService.AddMediaDatabaseLog(mdbl,_adminSession.Key);
        }

        [TestMethod]
        public void MediaContactsService_UpdateUpdateLog_Works()
        {
            var mcService = new MediaContactsService();
            MediaDatabaseLog mdbl = new MediaDatabaseLog();
            mdbl.Id = Constants.mediadatabase_update_log_id;
            mdbl.Accessed = DateTime.Now;
            mdbl.FieldsUpdated = "Subjects,Address,";

            mcService.UpdateMediaDatabaseLog(mdbl, _adminSession.Key);
        }

        [TestMethod]
        public void AddMediaContactGroup_Works()
        {
            var mcService = new MediaContactsService();
            var entity = new MediaContactGroup();
            List<MediaContactGroup> groups;
            Random r = new Random();
            int groupId;

            entity.Name = string.Format("Group ({0})", r.Next().ToString());
            entity.GroupType = MediaContactGroupType.List;
            entity.IsActive = true;
            entity.CreatedDate = DateTime.Now;
            entity.CreatedByUserId = _session.UserId;
            entity.LastModifiedDate = entity.CreatedDate;

            groupId = mcService.AddMediaContactGroup(entity, _session.Key);

            groups = mcService.GetMediaContactGroups(_session.UserId, _session.Key);

            Assert.IsTrue(groups.Exists(g => g.Id == groupId && g.Name == entity.Name));
        }

        [TestMethod]
        public void GetMediaContactGroups_OnlyReturnsAllowedRows()
        {
            var mcService = new MediaContactsService();
            List<MediaContactGroup> groups;
            var dbContext = new MedianetContext();
            List<int> userIds = dbContext.Users.Where(u => u.DebtorNumber == _session.DebtorNumber).Select(u => u.Id).ToList();

            groups = mcService.GetMediaContactGroups(_session.UserId, _session.Key);

            foreach (var g in groups)
            {
                Assert.IsTrue(g.CreatedByUserId == _session.UserId || userIds.Contains(g.CreatedByUserId));
            }
        }

        [TestMethod]
        public void GetMediaContactGroupsByType_SavedSearches_OnlyReturnsAllowedRows()
        {
            var mcService = new MediaContactsService();
            var groups = mcService.GetMediaContactGroupsByType(MediaContactGroupType.SavedSearch, _session.UserId, _session.Key);
            var dbContext = new MedianetContext();
            List<int> userIds = dbContext.Users.Where(u => u.DebtorNumber == _session.DebtorNumber).Select(u => u.Id).ToList();

            foreach (var g in groups)
            {
                Assert.IsTrue(g.CreatedByUserId == _session.UserId || userIds.Contains(g.CreatedByUserId));
            }
        }

        [TestMethod]
        public void GetMediaContactGroupsByType_Lists_OnlyReturnsAllowedRows()
        {
            var mcService = new MediaContactsService();
            var groups = mcService.GetMediaContactGroupsByType(MediaContactGroupType.List, _session.UserId, _session.Key);
            var dbContext = new MedianetContext();
            List<int> userIds = dbContext.Users.Where(u => u.DebtorNumber == _session.DebtorNumber).Select(u => u.Id).ToList();

            foreach (var g in groups)
            {
                Assert.IsTrue(g.CreatedByUserId == _session.UserId || userIds.Contains(g.CreatedByUserId));
            }
        }

        [TestMethod]
        public void GetMediaContactGroupsByType_Tasks_OnlyReturnsAllowedRows()
        {
            var mcService = new MediaContactsService();
            var groups = mcService.GetMediaContactGroupsByType(MediaContactGroupType.Task, _session.UserId, _session.Key);
            var dbContext = new MedianetContext();
            List<int> userIds = dbContext.Users.Where(u => u.DebtorNumber == _session.DebtorNumber).Select(u => u.Id).ToList();

            foreach (var g in groups)
            {
                Assert.IsTrue(g.CreatedByUserId == _session.UserId || userIds.Contains(g.CreatedByUserId));
            }
        }

        [TestMethod]
        public void GetMediaContactSavedSearches_Works()
        {
            var mcService = new MediaContactsService();
            var searches = mcService.GetMediaContactSavedSearches(_session.UserId, 0, _session.Key);
            var dbContext = new MedianetContext();

            foreach (var s in searches)
            {
                Assert.IsTrue(s.OwnerUserId == _session.UserId || (s.OwnerUser.DebtorNumber == _session.User.DebtorNumber && !s.IsPrivate));
            }
        }

        [TestMethod]
        public void GetMediaContactSavedSearch_Works()
        {
            var mcService = new MediaContactsService();
            var searches = mcService.GetMediaContactSavedSearches(_session.UserId, 0, _session.Key);
            var dbContext = new MedianetContext();

            var search = mcService.GetMediaContactSavedSearch(searches[0].Id, _session.Key);
            Assert.IsTrue(search != null && search.OwnerUser != null && search.Group != null);
        }

        [TestMethod]
        public void GetMediaContactLists_Works()
        {
            var mcService = new MediaContactsService();
            var lists = mcService.GetMediaContactLists(_session.UserId, 0, _session.Key);
            var dbContext = new MedianetContext();
            List<int> userIds = dbContext.Users.Where(u => u.DebtorNumber == _session.DebtorNumber).Select(u => u.Id).ToList();

            foreach (var l in lists)
            {
                Assert.IsTrue(l.OwnerUserId == _session.UserId || (userIds.Contains(l.OwnerUserId) && !l.IsPrivate));
            }
        }

        [TestMethod]
        public void GetMediaContactList_Works()
        {
            var mcService = new MediaContactsService();
            var lists = mcService.GetMediaContactLists(_session.UserId, 0, _session.Key);
            var dbContext = new MedianetContext();

            var list = mcService.GetMediaContactList(lists[0].Id, _session.Key);
            Assert.IsTrue(list != null && list.OwnerUser != null && list.Group != null);
        }

        [TestMethod]
        public void GetMediaContactTasks_Works()
        {
            var mcService = new MediaContactsService();
            var tasks = mcService.GetMediaContactTasks(_session.UserId, _session.Key);
            var dbContext = new MedianetContext();
            List<int> userIds = dbContext.Users.Where(u => u.DebtorNumber == _session.DebtorNumber).Select(u => u.Id).ToList();

            foreach (var t in tasks)
            {
                Assert.IsTrue(t.CreatedByUserId == _session.UserId
                    || t.OwnerUserId == _session.UserId
                    || !t.IsPrivate && (userIds.Contains(t.CreatedByUserId) || userIds.Contains(t.OwnerUserId)));
            }
        }

        [TestMethod]
        public void GetMediaContactTask_Works()
        {
            var mcService = new MediaContactsService();
            var tasks = mcService.GetMediaContactTasks(_session.UserId, _session.Key);

            var task = mcService.GetMediaContactTask(tasks[0].Id, _session.Key);
            Assert.IsTrue(task != null && task.OwnerUser != null && task.CreatedByUser != null && task.Group != null);
        }

        [TestMethod]
        public void MediaContact_GetPaginatedRecordsByListsetAndListId_ReturnsData()
        {
            var mcService = new MediaContactsService();
            var dta = mcService.GetPaginatedRecordsByListsetAndListId(67512, 0,0,100,SortColumn.OutletName,
                                                                      SortDirection.Asc, _session.UserId, _session.Key);
            Assert.IsTrue(dta.ListRecord.Count > 0 && dta.TotalRecordsCount > 0);

        }

        [TestMethod]
        public void MediaContact_GetMediaContactListVersions_ReturnsData()
        {
            var mcService = new MediaContactsService();

            // Get all lists version data for this user to find a list with version data we can use for this test
            var dbContext = new MedianetContext();
            var versions = (from l in dbContext.MediaContactLists
                           join u1 in dbContext.Users on l.OwnerUserId equals u1.Id
                           join u2 in dbContext.Users on u1.DebtorNumber equals u2.DebtorNumber
                           join v in dbContext.MediaContactListVersions on l.Id equals v.ListId
                           where u2.Id == _session.UserId
                                 && l.IsActive == true
                                 && (l.OwnerUserId == _session.UserId || l.IsPrivate == false)
                           select v).ToList();
            Assert.IsTrue(versions != null && versions.Count > 0);

            var dta = mcService.GetMediaContactListVersions(versions[0].ListId, _session.Key);

            // This call returns the current version even when we have no version data for this list,
            // so count should be > 1 when we have version data
            Assert.IsTrue(dta.Count > 1 && dta[0].Version == 0 && dta[0].ListId == versions[0].ListId && dta[1].ListId == versions[0].ListId);

        }

        //[TestMethod]
        //public void MediaContact_GetLivingListOutletContactsByRecordId_ReturnsData()
        //{
        //    var mcService = new MediaContactsService();
        //    var dta = mcService.GetLivingListOutletContactsByRecordId(49511388, 50, _session.Key);
        //    Assert.IsTrue(dta.Count > 0);
        //}

        [TestMethod]
        public void MediaContactsService_GetDatabaseLog_Works()
        {
            var mcService = new MediaContactsService();
            List<MediaDatabaseLog> dbEntities;

            dbEntities = mcService.GetMediaDatabaseLogs(Constants.outlet_id, Constants.contact_id, _adminSession.Key);

            var groups = dbEntities.GroupBy(g => new { accessed = g.Accessed, user = g.UserID }).OrderByDescending(g =>g.Key.accessed)
                .Select( a => new {accessed = a.Key.accessed, user = a.Key.user ,
                     Joined = string.Join("", a.Select(s=> s.FieldsUpdated))});
            
            Assert.IsTrue(dbEntities != null);
        }

        [TestMethod]
        public void MediaContactsService_GetDatabaseLogRecentlyViewed_Works()
        {
            var mcService = new MediaContactsService();

            var recentlyViewed = mcService.GetMediaRecentlyViewed(_adminSession.Key);

            Assert.IsTrue(recentlyViewed != null);
        }

        [TestMethod]
        public void GetMediaContactRecentlyUpdated_Works()
        {
            var mcService = new MediaContactsService();
            var recentlyUpdated = mcService.GetMediaContactRecentlyUpdated(10, _session.Key);

            Assert.IsTrue(recentlyUpdated != null);
        }

        [TestMethod]
        public void LogMediaOutletView_Works()
        {
            var mcService = new MediaContactsService();
            var view = new MediaOutletViewDto()
            {
                OutletId = "AAPO24",
                UserId = 5,
                IpAddress = "192.168.220.1",
                DateViewed = DateTime.Now
            };

            mcService.LogMediaOutletView(view, _session.Key);
        }

        //This test will never be able to be run more than once so it is an invalid test.
        //[TestMethod]
        //public void MediaContactsService_AddEmployee_Works()
        //{
        //    var mcService = new MediaContactsService();
        //    MediaEmployee entity = new MediaEmployee();

        //    entity.OutletId = "AAPO1";
        //    entity.ContactId = "AAPC56122";
        //    entity.HasReceivedLegals = true;
        //    entity.IsPrimaryNewsContact = false;
        //    entity.JobTitle = "";
        //    entity.MobileNumber = "0412 345 678";
        //    entity.OutletName = "ACME";
        //    entity.ReplacedByContactId = "";
        //    entity.ReplacedDate = null;
        //    entity.AddressLine1 = "123 Acme Road";
        //    entity.BlogURL = "";
        //    entity.CityId = 17;
        //    entity.CountryId = 210;
        //    entity.Date = DateTime.Now;
        //    entity.DeletedDate = null;
        //    entity.EmailAddress = "testing@acme.com";
        //    entity.Facebook = "";
        //    entity.FaxNumber = "";
        //    entity.LastModifiedByUserId = 5;
        //    entity.LastModifiedDate = DateTime.Now;
        //    entity.LinkedIn = "";
        //    entity.LockedByUserId = null;
        //    entity.PhoneNumber = "9999 9876";
        //    entity.PostCode = "2000";
        //    entity.PreferredDeliveryMethod = PreferredDeliveryType.Email;
        //    entity.RowStatus = EmployeeRowStatusType.Active;
        //    entity.TrackingReference = "";
        //    entity.Twitter = "";
        //    entity.User = null;
        //    entity.AddressLine2 = "NSW, AUSTRALIA";

        //    entity.Subjects = new List<MediaEmployeeSubject>();
        //    entity.Subjects.Add(new MediaEmployeeSubject { SubjectId = 16134, OutletId=entity.OutletId, ContactId=entity.ContactId, IsPrimaryContact=false });
        //    entity.Subjects.Add(new MediaEmployeeSubject { SubjectId = 16135, OutletId=entity.OutletId, ContactId=entity.ContactId, IsPrimaryContact=false });
        //    entity.Subjects.Add(new MediaEmployeeSubject { SubjectId = 16136, OutletId=entity.OutletId, ContactId=entity.ContactId, IsPrimaryContact=false });

        //    entity.Roles = new List<MediaEmployeeRole>();
        //    entity.Roles.Add(new MediaEmployeeRole { RoleId = 643, ContactId = entity.ContactId, OutletId = entity.OutletId });
        //    entity.Roles.Add(new MediaEmployeeRole { RoleId = 644, ContactId = entity.ContactId, OutletId = entity.OutletId });
        //    entity.Roles.Add(new MediaEmployeeRole { RoleId = 645, ContactId = entity.ContactId, OutletId = entity.OutletId });

        //    mcService.AddMediaEmployee(entity, _adminSession.Key);

        //    Assert.IsTrue(entity != null);
        //}

        /*

        /// <summary>
        ///A test for UpdateExports
        ///</summary>
        [TestMethod()]
        public void UpdateExportsTest() {
            MediaContactsService target = new MediaContactsService(); // TODO: Initialize to an appropriate value
            MediaContactsExport export = null; // TODO: Initialize to an appropriate value
            string sessionKey = string.Empty; // TODO: Initialize to an appropriate value
            target.UpdateExports(export, sessionKey);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }*/
    }
}
