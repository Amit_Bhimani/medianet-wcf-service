﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.ServiceModel;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Medianet.Service;
using Medianet.Service.Contracts;
using Medianet.Service.Dto;
using Medianet.Service.Common;
using Medianet.Model;
using Medianet.Service.FaultExceptions;
using Medianet.Service.BusinessLayer.Common;

namespace Medianet.Test
{
    
    
    /// <summary>
    ///This is a test class for ReleaseServiceTest and is intended
    ///to contain all ReleaseServiceTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ReleaseServiceTest
    {
        public static DBSession _session;

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes

        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext) {
            if (!AutoMapperConfigurator.IsInitialized)
                AutoMapperConfigurator.Configure();

            //var cService = new CustomerService();
            //_session = cService.LoginUsingEmailAddress(Constants.account_alt_emailaddress, Constants.account_alt_password, SystemType.Medianet);

            // Create an impersonating session so another test doesn't log us out.
            _session = Common.CreateImpersonatingSession(Constants.account_alt_user_id, SystemType.Medianet);
        }

        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        [TestMethod]
        public void EmailStringParse_Works()
        {
            string emails = "abc@acme.com.au";//;def@acme.com.au;ghi@acme.com.au";
            string[] addresses = emails.Split(new Char[] { ';' });

            Assert.IsTrue(addresses.Length > 0);
        }

        [TestMethod]
        [DeploymentItem("TestFiles", "TestFiles")]
        public void AddReleaseFromSummary_FaxEmailWire_Works() {
            ReleaseSummary release;
            int releaseId;
            var rService = new ReleaseService();
            var cService = new CustomerService();
            DBSession session;

            session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, Constants.account_password, SystemType.Medianet);
            release = Common.GenerateReleaseSummary(session, true);

            release.Transactions = new List<TransactionSummary>();
            release.Transactions.Add(Common.GenerateServiceTransaction(Constants.service_major_metro_all_states_fax, 1));
            release.Transactions.Add(Common.GenerateServiceTransaction(Constants.service_major_metro_all_states_email, 2));
            release.Transactions.Add(Common.GeneratePackageTransaction(Constants.package_federal, 3));
            release.Transactions.Add(Common.GenerateExtraEmailRecipientTransaction("david.urquhart@aap.com.au", DistributionType.Email, 4));
            release.Transactions.Add(Common.GenerateWebTransaction(Medianet.Service.BusinessLayer.Common.Constants.PUBLIC_WEBSITE_SERVICE_ID, 5));
            release.Transactions.Add(Common.GenerateTwitterTransaction(6));
            release.Transactions.Add(Common.GenerateAttachmentTransaction(7));

            release.Documents = new List<DocumentSummary>();
            release.Documents.Add(Common.GenerateReleaseDocument(1, session));
            release.Documents.Add(Common.GenerateAttachmentDocument(2, session));
            release.Documents.Add(Common.GenerateWireEmailDocument(3, session));

            try {
                releaseId = rService.AddReleaseFromSummary(release, session.Key);
            }
            catch (FaultException ex) {
                throw new Exception("Error adding release. Please try again later.", ex);
            }

            Assert.IsTrue(releaseId > 0);
        }

        [TestMethod]
        [DeploymentItem("TestFiles", "TestFiles")]
        public void AddReleaseFromSummary_UnverifiedRelease_Works()
        {
            ReleaseSummary release;
            int releaseId;
            var rService = new ReleaseService();
            var cService = new CustomerService();
            DBSession session;

            session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, Constants.account_password, SystemType.Medianet);
            Common.ResetUserReleaseCount(session.User.Id);

            release = Common.GenerateReleaseSummary(session, true, false);
            release.Transactions = new List<TransactionSummary>();
            release.Transactions.Add(Common.GenerateWebTransaction(Medianet.Service.BusinessLayer.Common.Constants.PUBLIC_WEBSITE_SERVICE_ID, 1));

            release.Documents = new List<DocumentSummary>();
            release.Documents.Add(Common.GenerateWireEmailDocument(1, session));

            try
            {
                releaseId = rService.AddReleaseFromSummary(release, session.Key);
            }
            catch (FaultException ex)
            {
                throw new Exception("Error adding release. Please try again later.", ex);
            }

            Assert.IsTrue(releaseId > 0);
        }

        [TestMethod]
        [DeploymentItem("TestFiles", "TestFiles")]
        public void AddReleaseFromSummary_UnverifiedReleaseWithWhitepaper_Works()
        {
            ReleaseSummary release;
            int releaseId;
            var rService = new ReleaseService();
            var cService = new CustomerService();
            DBSession session;

            session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, Constants.account_password, SystemType.Medianet);
            Common.ResetUserReleaseCount(session.User.Id);

            release = Common.GenerateReleaseSummary(session, true, false);

            //Override the Multimedia Type to be a white paper.
            release.MultimediaType = MultimediaType.WhitePaper;

            release.Transactions = new List<TransactionSummary>();
            release.Transactions.Add(Common.GenerateWebTransaction(Medianet.Service.BusinessLayer.Common.Constants.PUBLIC_WEBSITE_SERVICE_ID, 1));

            release.Documents = new List<DocumentSummary>();
            release.Documents.Add(Common.GenerateWireEmailDocument(1, session));

            try
            {
                releaseId = rService.AddReleaseFromSummary(release, session.Key);
            }
            catch (FaultException ex)
            {
                throw new Exception("Error adding release. Please try again later.", ex);
            }

            Assert.IsTrue(releaseId > 0);
        }

        [TestMethod]
        [DeploymentItem("TestFiles", "TestFiles")]
        public void AddReleaseFromSummary_FaxEmailWire_SummaryTextIsPopulated() {
            ReleaseSummary release;
            int releaseId;
            var rService = new ReleaseService();
            var cService = new CustomerService();
            DBSession session;

            session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, Constants.account_password, SystemType.Medianet);
            release = Common.GenerateReleaseSummary(session, true);

            release.Transactions = new List<TransactionSummary>();
            release.Transactions.Add(Common.GenerateWebTransaction(Medianet.Service.BusinessLayer.Common.Constants.PUBLIC_WEBSITE_SERVICE_ID, 1));

            release.Documents = new List<DocumentSummary>();
            release.Documents.Add(Common.GenerateWireEmailDocument(1, session));

            releaseId = rService.AddReleaseFromSummary(release, session.Key);
            var rel = rService.GetReleaseById(releaseId, session.Key);
            Assert.IsTrue(!string.IsNullOrEmpty(rel.WebDetails.Summary) && rel.WebDetails.Summary.Length <= ReleaseHelper.MAX_WEB_RELEASE_SUMMARY_LENGTH);
        }

        [TestMethod]
        public void AddReleaseFromSummary_SMS_Works() {
            ReleaseSummary release;
            int releaseId;
            var rService = new ReleaseService();
            var cService = new CustomerService();
            DBSession session;

            session = cService.LoginUsingEmailAddress(Constants.account_billed_emailaddress, Constants.account_billed_password, SystemType.Medianet);
            release = Common.GenerateReleaseSummary(session, false);
            release.SMSText = "This is an SMS test.";

            release.Transactions = new List<TransactionSummary>();
            release.Transactions.Add(Common.GenerateServiceTransaction(138193, 1));
            release.Transactions.Add(Common.GenerateExtraEmailRecipientTransaction("0411333445", DistributionType.SMS, 2));

            try {
                releaseId = rService.AddReleaseFromSummary(release, session.Key);
            }
            catch (FaultException ex) {
                throw new Exception("Error adding release. Please try again later.", ex);
            }

            Assert.IsTrue(releaseId > 0);
        }

        [TestMethod]
        [DeploymentItem("TestFiles", "TestFiles")]
        public void AddReleaseFromSummary_AudioNews_Works() {
            ReleaseSummary release;
            int releaseId;
            var rService = new ReleaseService();
            var cService = new CustomerService();
            DBSession session;

            session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, Constants.account_password, SystemType.Medianet);
            release = Common.GenerateReleaseSummary(session, true);
            release.MultimediaType = MultimediaType.Audio;

            release.Transactions = new List<TransactionSummary>();
            release.Transactions.Add(Common.GenerateServiceTransaction(Constants.service_all_states_and_territories_audio_wire, 1));
            release.Transactions.Add(Common.GenerateServiceTransaction(Constants.service_talkback_weekdays_email, 2));
            release.Transactions.Add(Common.GenerateExtraEmailRecipientTransaction("urquhartd@aap.com.au", DistributionType.Email, 3));
            release.Transactions.Add(Common.GenerateWebTransaction(Medianet.Service.BusinessLayer.Common.Constants.PUBLIC_WEBSITE_SERVICE_ID, 4));
            release.Transactions.Add(Common.GenerateWebTransaction(Medianet.Service.BusinessLayer.Common.Constants.MNJ_WEBSITE_SERVICE_ID, 5));

            release.Documents = new List<DocumentSummary>();
            release.Documents.Add(Common.GenerateReleaseDocument(1, session));
            release.Documents.Add(Common.GenerateAudioAttachmentDocument(2, session));
            release.Documents.Add(Common.GenerateAudioAttachmentDocument(91394, 3, session));
            release.Documents.Add(Common.GenerateWireEmailDocument(4, session));

            try {
                releaseId = rService.AddReleaseFromSummary(release, session.Key);
            }
            catch (FaultException ex) {
                throw new Exception("Error adding release. Please try again later.", ex);
            }

            Assert.IsTrue(releaseId > 0);
        }

        //[TestMethod]
        //[DeploymentItem("TestFiles", "TestFiles")]
        //public void AddReleaseFromSummary_SMS_Mailmerge_Works() {
        //    ReleaseSummary release;
        //    int releaseId;
        //    var rService = new ReleaseService();
        //    var cService = new CustomerService();
        //    DocumentSummary relDocument;
        //    DocumentSummary dataDocument;
        //    DBSession session;

        //    session = cService.Login(Constants.account_username, Constants.account_companylogon, Constants.account_password, SystemType.Medianet);
        //    release = GenerateReleaseSummary(session);
        //    release.IsMailMergeRelease = true;

        //    release.Transactions = new List<TransactionSummary>();
        //    release.Transactions.Add(GenerateMailmergeTransaction(DistributionType.SMS, 1));

        //    release.Documents = new List<DocumentSummary>();
        //    relDocument = GenerateMailmergeSmsReleaseDocument(1, session);
        //    release.Documents.Add(relDocument);
        //    dataDocument = GenerateMailmergeSmsDataSourceDocument(2, session);
        //    release.Documents.Add(dataDocument);

        //    try {
        //        releaseId = rService.AddReleaseFromSummary(release, session.Key);
        //    }
        //    catch (FaultException ex) {
        //        throw new Exception("Error adding release. Please try again later.", ex);
        //    }

        //    Assert.IsTrue(releaseId > 0);
        //}

        [TestMethod]
        public void GetReleaseById_FetchesUserSummaryAndWebDetails() {
            Release release;
            var rService = new ReleaseService();

            release = rService.GetReleaseById(Constants.aap_release_id, _session.Key);

            Assert.IsTrue(release != null && release.CreatedByUser != null && release.WebDetails != null);
        }

        [TestMethod]
        public void GetReleaseById_ReleaseHasZeroCreatedByUserId_StillWorks() {
            Release release;
            var rService = new ReleaseService();

            release = rService.GetReleaseById(711192, _session.Key);

            Assert.IsTrue(release != null && release.CreatedByUser == null);
        }

        [TestMethod]
        public void GetReleaseById_OldReleaseWithPaddedDebtorNumber_Works() {
            Release release;
            var rService = new ReleaseService();
            var cService = new CustomerService();
            DBSession session;

            session = cService.LoginUsingEmailAddress(Constants.account_billed_emailaddress, Constants.account_billed_password, SystemType.Medianet);
            release = rService.GetReleaseById(424097, session.Key);

            Assert.IsTrue(release != null);
        }

        [TestMethod]
        public void GetReleasesWithResultsToShow_Works() {
            var rService = new ReleaseService();
            ReleasePage page;

            page = rService.GetReleasesWithResultsToShow(0, 25, ReleaseSortOrderType.CreatedDate, _session.Key);

            Assert.IsTrue(page.Releases != null && page.Releases.Count > 0 && 
                page.TotalCount >= page.Releases.Count && 
                page.Releases.Count <= page.PageSize &&
                !page.Releases.Exists(r => r.CreatedByUserId == 0 && r.CreatedByUser != null));
        }

        [TestMethod]
        public void GetReleaseDocuments_Works()
        {
            List<Document> documents;
            var rService = new ReleaseService();

            documents = rService.GetReleaseDocuments(735542, _session.Key);

            Assert.IsTrue(documents != null && documents.Count > 0);
        }

        [TestMethod]
        public void GetReleaseTransactionStatistics_Works() {
            var rService = new ReleaseService();
            List<TransactionStatistics> transList;
            var cService = new CustomerService();
            DBSession session;

            session = cService.LoginUsingEmailAddress(Constants.account_billed_emailaddress, Constants.account_billed_password, SystemType.Medianet);

            transList = rService.GetReleaseTransactionStatistics(634584, session.Key);

            Assert.IsTrue(transList != null && transList.Count > 0);
        }

        [TestMethod]
        [DeploymentItem("TestFiles", "TestFiles")]
        public void AddReleaseFromSummary_DifferentTimezone_CalculatesCorrectHoldDate() {
            ReleaseSummary relSummary;
            Release release;
            int releaseId;
            var rService = new ReleaseService();
            CustomerService cService = new CustomerService();
            DBSession session;
            DateTime holdDate = DateTime.Now.AddMonths(6);
            DateTime embargoDate;

            // The hold date should only be significant to minutes.
            holdDate = new DateTime(holdDate.Year, holdDate.Month, holdDate.Day, holdDate.Hour, holdDate.Minute, 0);
            embargoDate = holdDate.AddHours(1);

            session = cService.LoginUsingEmailAddress(Constants.account_billed_emailaddress, Constants.account_billed_password, SystemType.Medianet);

            // Create a release on hold.
            relSummary = Common.GenerateReleaseSummary(session, true);
            relSummary.HoldUntilDate = holdDate;
            relSummary.EmbargoUntilDate = embargoDate;
            relSummary.TimezoneCode = "PER";

            relSummary.Transactions = new List<TransactionSummary>();
            relSummary.Transactions.Add(Common.GenerateExtraEmailRecipientTransaction("david.urquhart@aap.com.au", DistributionType.Email, 4));
            relSummary.Documents = new List<DocumentSummary>();
            relSummary.Documents.Add(Common.GenerateWireEmailDocument(3, session));

            releaseId = rService.AddReleaseFromSummary(relSummary, session.Key);

            // Get the relesase back from the database.
            release = rService.GetReleaseById(releaseId, session.Key);

            TimezoneHelper tzHelper = new TimezoneHelper("PER", Medianet.Service.BusinessLayer.Common.Constants.TIMEZONE_SYD);

            Assert.IsTrue(release.HoldUntilDate.Value != holdDate && release.HoldUntilDate.Value == tzHelper.AdjustDateTime(holdDate));
            Assert.IsTrue(release.EmbargoUntilDate.Value != embargoDate && release.EmbargoUntilDate.Value == tzHelper.AdjustDateTime(embargoDate));
        }

        [TestMethod]
        public void GetPublicFeatureReleases_Works() {
            List<ReleaseSummaryPublic> releases;
            var rService = new ReleaseService();

            releases = rService.GetPublicFeatureReleases(null);

            Assert.IsTrue(releases != null && releases.Count > 0 && !string.IsNullOrEmpty(releases[0].Headline));
        }

        [TestMethod]
        public void GetPublicFeatureReleases_NonExistantCategory_ReturnsDefaultFeatureReleases() {
            List<ReleaseSummaryPublic> releases1;
            List<ReleaseSummaryPublic> releases2;
            var rService = new ReleaseService();

            releases1 = rService.GetPublicFeatureReleases(null);
            releases2 = rService.GetPublicFeatureReleases(99999999);

            foreach (var rel in releases1) {
                Assert.IsTrue(releases2.Exists(r => r.Id == rel.Id));
            }
        }

        [TestMethod]
        public void GetPublicReleases_Works() {
            ReleasePagePublic releasesPage;
            var rService = new ReleaseService();
            var requestParams = new ReleasePagePublicRequest();

            requestParams.Website = WebsiteType.Public;
            requestParams.FromDate = DateTime.Parse("1 Jan 2013");
            requestParams.ToDate = DateTime.Parse("30 July 2013");
            requestParams.MultimediaType = MultimediaType.None;
            requestParams.ShowVideos = true;
            requestParams.ShowAudios = true;
            requestParams.ShowPhotos = true;
            requestParams.ShowOthers = true;
            requestParams.ShowMultimedia = true;
            requestParams.PageNumber = 1;
            requestParams.PageSize = 20;

            releasesPage = rService.GetPublicReleases(requestParams);

            Assert.IsTrue(releasesPage != null && releasesPage.Releases.Count > 0 && !string.IsNullOrEmpty(releasesPage.Releases[0].Headline));
        }

        [TestMethod]
        public void GetPublicReleases_ReturnsSecurityKeyForJournalistsOnly()
        {
            ReleasePagePublic publicReleasesPage;
            ReleasePagePublic mnjReleasesPage;
            var rService = new ReleaseService();
            var requestParams = new ReleasePagePublicRequest();
            var cService = new CustomerService();
            DBSession session;

            requestParams.Website = WebsiteType.Public;
            requestParams.FromDate = DateTime.Now.AddMonths(-6);
            requestParams.ToDate = DateTime.Now;
            requestParams.MultimediaType = MultimediaType.None;
            requestParams.ShowVideos = true;
            requestParams.ShowAudios = true;
            requestParams.ShowPhotos = true;
            requestParams.ShowOthers = true;
            requestParams.ShowMultimedia = true;
            requestParams.PageNumber = 1;
            requestParams.PageSize = 5;

            publicReleasesPage = rService.GetPublicReleases(requestParams);

            session = cService.LoginUsingEmailAddress(Constants.account_emailaddress, Constants.account_password, SystemType.Journalists);

            requestParams.Website = WebsiteType.Journalists;
            requestParams.SessionKey = session.Key;
            mnjReleasesPage = rService.GetPublicReleases(requestParams);

            foreach (var release in publicReleasesPage.Releases) {
                Assert.IsTrue(string.IsNullOrEmpty(release.SecurityKey));
            }

            foreach (var release in mnjReleasesPage.Releases)
            {
                Assert.IsTrue(!string.IsNullOrEmpty(release.SecurityKey));
            }
        }

        //[TestMethod]
        //public void GetPublicReleases_IncludeUnverified_WhitepapersOnly_Works()
        //{
        //    ReleasePagePublic releasesPage;
        //    var rService = new ReleaseService();
        //    var requestParams = new ReleasePagePublicRequest();

        //    requestParams.FromDate = DateTime.Parse("1 Jan 2013");
        //    requestParams.ToDate = DateTime.Parse("30 Dec 2013");
        //    requestParams.MultimediaType = MultimediaType.WhitePaper;
        //    requestParams.ShowUnverified = true;
        //    requestParams.ShowVideos = true;
        //    requestParams.ShowAudios = true;
        //    requestParams.ShowPhotos = true;
        //    requestParams.ShowOthers = true;
        //    requestParams.ShowMultimedia = true;
        //    requestParams.PageNumber = 1;
        //    requestParams.PageSize = 20;
            
        //    releasesPage = rService.GetPublicReleases(requestParams);

        //    Assert.IsTrue(releasesPage != null && releasesPage.Releases.Count > 0 && !string.IsNullOrEmpty(releasesPage.Releases[0].Headline));
        //}

        [TestMethod]
        public void GetPublicReleases_RequestPageTwo_Works() {
            ReleasePagePublic releasesPage40;
            ReleasePagePublic releasesPage20;
            var rService = new ReleaseService();
            var requestParams = new ReleasePagePublicRequest();

            requestParams.Website = WebsiteType.Public;
            requestParams.FromDate = DateTime.Parse("1 Jan 2013");
            requestParams.ToDate = DateTime.Parse("30 July 2013");
            requestParams.MultimediaType = MultimediaType.None;
            requestParams.ShowVideos = true;
            requestParams.ShowAudios = true;
            requestParams.ShowPhotos = true;
            requestParams.ShowOthers = true;
            requestParams.ShowMultimedia = true;
            requestParams.PageNumber = 1;
            requestParams.PageSize = 40;

            releasesPage40 = rService.GetPublicReleases(requestParams);

            requestParams.PageNumber = 2;
            requestParams.PageSize = 20;

            releasesPage20 = rService.GetPublicReleases(requestParams);

            for(int i = 0; i < releasesPage20.Releases.Count; i++) {
                if (releasesPage20.Releases[i].Id != releasesPage40.Releases[i+20].Id)
                    throw new Exception(string.Format("Item {0} on page 2 was expected to have a ReleaseId of {1} but instead was {2}.", i, releasesPage40.Releases[i+20], releasesPage20.Releases[i].Id));
            }
        }

        [TestMethod]
        public void GetPublicReleaseById_Works() {
            ReleasePublic release;
            var rService = new ReleaseService();

            release = rService.GetPublicReleaseById(777390, WebsiteType.Public, string.Empty, "unit test", null); //"6872893"); //777989); //777546);

            Assert.IsTrue(release != null && !string.IsNullOrEmpty(release.Headline));
        }

        [TestMethod]
        public void GetPublicReleaseById_HiddenButWithSecurityKey_StillWorks() {
            ReleasePublic release;
            var rService = new ReleaseService();

            release = rService.GetPublicReleaseById(777855, WebsiteType.None, string.Empty, "unit test", "7341819");

            Assert.IsTrue(release != null && !string.IsNullOrEmpty(release.Headline));
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException<KeyNotFoundFault>))]
        public void GetPublicReleaseById_HiddenAndWithNoSecurityKey_ThrowsError() {
            ReleasePublic release;
            var rService = new ReleaseService();

            release = rService.GetPublicReleaseById(777855, WebsiteType.None, string.Empty, "unit test", null);
        }

        [TestMethod]
        public void GetPublicReleaseById_VnrRelease_AllHaveCorrectDownloadPath() {
            ReleasePublic release;
            var rService = new ReleaseService();
            var helper = new UrlHelper();

            release = rService.GetPublicReleaseById(634160, WebsiteType.Public, string.Empty, "unit test", null);

            if (release.Videos != null) {
                foreach (var doc in release.Videos)
                    Assert.IsTrue(doc.DownloadUrl.StartsWith(helper.WieckVideoUrl));
            }
            if (release.Audios != null) {
                foreach (var doc in release.Audios)
                    Assert.IsFalse(string.IsNullOrEmpty(doc.DownloadUrl));
            }
            if (release.Images != null) {
                foreach (var doc in release.Images)
                    Assert.IsFalse(string.IsNullOrEmpty(doc.DownloadUrl));
            }
            if (release.Attachments != null) {
                foreach (var doc in release.Attachments)
                    Assert.IsFalse(string.IsNullOrEmpty(doc.DownloadUrl));
            }
        }

        [TestMethod]
        public void EmailPublicReleaseById_Works() {
            var rService = new ReleaseService();

            rService.EmailPublicReleaseById(777390, WebsiteType.Public, "durquhart@aap.com.au", null);
        }

        [TestMethod]
        [DeploymentItem("TestFiles", "TestFiles")]
        public void QuoteRelease_FaxEmailWire_Works()
        {
            ReleaseQuoteRequest release;
            ReleaseQuoteResponse response;
            var rService = new ReleaseService();
            var cService = new CustomerService();
            DBSession session;

            session = cService.LoginUsingEmailAddress(Constants.account_billed_emailaddress, Constants.account_billed_password, SystemType.Medianet);
            release = Common.GenerateReleaseQuote(session);

            release.Transactions = new List<TransactionSummary>();
            release.Transactions.Add(Common.GenerateServiceTransaction(Constants.service_major_metro_all_states_fax, 1));
            release.Transactions.Add(Common.GenerateServiceTransaction(Constants.service_major_metro_all_states_email, 2));
            release.Transactions.Add(Common.GeneratePackageTransaction(Constants.package_federal, 3));
            release.Transactions.Add(Common.GenerateServiceTransaction(Constants.service_aap_daily_diary_email, 4));
            release.Transactions.Add(Common.GenerateExtraEmailRecipientTransaction("david.urquhart@aap.com.au", DistributionType.Email, 5));
            release.Transactions.Add(Common.GenerateWebTransaction(Medianet.Service.BusinessLayer.Common.Constants.PUBLIC_WEBSITE_SERVICE_ID, 6));
            release.Transactions.Add(Common.GenerateTwitterTransaction(7));
            release.Transactions.Add(Common.GenerateWebTransaction(Medianet.Service.BusinessLayer.Common.Constants.MNJ_WEBSITE_SERVICE_ID, 8));
            release.Transactions.Add(Common.GenerateAttachmentTransaction(9));

            release.Documents = new List<DocumentSummary>();
            release.Documents.Add(Common.GenerateReleaseDocument(1, session));
            release.Documents.Add(Common.GenerateAttachmentDocument(2, session));
            release.Documents.Add(Common.GenerateWireEmailDocument(3, session));

            try
            {
                response = rService.QuoteRelease(release, session.Key);
            }
            catch (FaultException ex)
            {
                throw new Exception("Error quoting release. Please try again later.", ex);
            }

            Assert.IsTrue(response.ExGST > 0);
        }

        [TestMethod]
        [DeploymentItem("TestFiles", "TestFiles")]
        public void QuoteRelease_WireOnly_Works()
        {
            ReleaseQuoteRequest release;
            ReleaseQuoteResponse response;
            var rService = new ReleaseService();
            var cService = new CustomerService();
            DBSession session;

            session = cService.LoginUsingEmailAddress(Constants.account_billed_emailaddress, Constants.account_billed_password, SystemType.Medianet);
            release = Common.GenerateReleaseQuote(session);

            release.Transactions = new List<TransactionSummary>();
            release.Transactions.Add(Common.GenerateServiceTransaction(Constants.service_all_states_and_territories_wire, 1));

            release.Documents = new List<DocumentSummary>();
            release.Documents.Add(Common.GenerateWireEmailDocument(1, session));

            try
            {
                response = rService.QuoteRelease(release, session.Key);
            }
            catch (FaultException ex)
            {
                throw new Exception("Error quoting release. Please try again later.", ex);
            }

            Assert.IsTrue(response.ExGST > 0);
        }

        [TestMethod]
        public void CalculateRelease_FaxEmailWire_Works()
        {
            ReleaseCalculatorRequest release;
            ReleaseQuoteResponse response;
            var rService = new ReleaseService();
            var cService = new CustomerService();
            DBSession session;

            session = cService.LoginUsingEmailAddress(Constants.account_billed_emailaddress, Constants.account_billed_password, SystemType.Medianet);
            release = Common.GenerateReleaseCalculator(session);

            release.Transactions = new List<TransactionSummary>();
            release.Transactions.Add(Common.GenerateServiceTransaction(Constants.service_major_metro_all_states_fax, 1));
            release.Transactions.Add(Common.GenerateServiceTransaction(Constants.service_major_metro_all_states_email, 2));
            release.Transactions.Add(Common.GeneratePackageTransaction(Constants.package_federal, 3));
            release.Transactions.Add(Common.GenerateServiceTransaction(Constants.service_aap_daily_diary_email, 4));
            release.Transactions.Add(Common.GenerateExtraEmailRecipientTransaction("david.urquhart@aap.com.au", DistributionType.Email, 5));
            release.Transactions.Add(Common.GenerateWebTransaction(Medianet.Service.BusinessLayer.Common.Constants.PUBLIC_WEBSITE_SERVICE_ID, 6));
            release.Transactions.Add(Common.GenerateWebTransaction(Medianet.Service.BusinessLayer.Common.Constants.MNJ_WEBSITE_SERVICE_ID, 7));
            release.Transactions.Add(Common.GenerateTwitterTransaction(8));

            release.PageCount = 2;
            release.WordCount = 274;
            release.AttachmentCount = 1;

            try
            {
                response = rService.CalculateRelease(release, session.Key);
            }
            catch (FaultException ex)
            {
                throw new Exception("Error quoting release. Please try again later.", ex);
            }

            Assert.IsTrue(response.ExGST > 0);
        }

        [TestMethod]
        [DeploymentItem("TestFiles", "TestFiles")]
        public void InitialisePayment_FaxEmailWire_Works()
        {
            PaymentStartRequest payment;
            ReleaseQuoteRequest release;
            PaymentStartResponse response;
            var rService = new ReleaseService();
            var cService = new CustomerService();
            DBSession session;

            session = cService.LoginUsingEmailAddress(Constants.account_billed_emailaddress, Constants.account_billed_password, SystemType.Medianet);
            release = Common.GenerateReleaseQuote(session);

            release.Transactions = new List<TransactionSummary>();
            release.Transactions.Add(Common.GenerateServiceTransaction(Constants.service_major_metro_all_states_fax, 1));
            release.Transactions.Add(Common.GenerateServiceTransaction(Constants.service_major_metro_all_states_email, 2));
            release.Transactions.Add(Common.GeneratePackageTransaction(Constants.package_federal, 3));
            release.Transactions.Add(Common.GenerateServiceTransaction(Constants.service_aap_daily_diary_email, 4));
            release.Transactions.Add(Common.GenerateExtraEmailRecipientTransaction("david.urquhart@aap.com.au", DistributionType.Email, 5));
            release.Transactions.Add(Common.GenerateWebTransaction(Medianet.Service.BusinessLayer.Common.Constants.PUBLIC_WEBSITE_SERVICE_ID, 6));
            release.Transactions.Add(Common.GenerateWebTransaction(Medianet.Service.BusinessLayer.Common.Constants.MNJ_WEBSITE_SERVICE_ID, 7));
            release.Transactions.Add(Common.GenerateTwitterTransaction(8));

            release.Documents = new List<DocumentSummary>();
            release.Documents.Add(Common.GenerateReleaseDocument(1, session));
            release.Documents.Add(Common.GenerateAttachmentDocument(2, session));
            release.Documents.Add(Common.GenerateWireEmailDocument(3, session));

            payment = new PaymentStartRequest();
            payment.ReleaseQuote = release;
            payment.IPAddress = "192.168.220.75";
            payment.RedirectURL = "http://nowhere.com.au/MyPage";

            try
            {
                response = rService.InitialisePayment(payment, session.Key);
            }
            catch (FaultException ex)
            {
                throw new Exception("Error quoting release. Please try again later.", ex);
            }

            Assert.IsTrue(!string.IsNullOrWhiteSpace(response.PaymentAccessCode) && response.QuoteReferenceId > 0);
        }

        [TestMethod]
        [DeploymentItem("TestFiles", "TestFiles")]
        [ExpectedException(typeof(FaultException<InvalidParameterFault>))]
        public void InitialisePayment_NoBillableTransactions_ErrorThrown()
        {
            PaymentStartRequest payment;
            ReleaseQuoteRequest release;
            PaymentStartResponse response;
            var rService = new ReleaseService();
            var cService = new CustomerService();
            DBSession session;

            session = cService.LoginUsingEmailAddress(Constants.account_billed_emailaddress, Constants.account_billed_password, SystemType.Medianet);
            release = Common.GenerateReleaseQuote(session);

            release.Transactions = new List<TransactionSummary>();

            release.Documents = new List<DocumentSummary>();
            release.Documents.Add(Common.GenerateReleaseDocument(1, session));
            release.Documents.Add(Common.GenerateWireEmailDocument(2, session));

            payment = new PaymentStartRequest();
            payment.ReleaseQuote = release;
            payment.IPAddress = "192.168.220.75";
            payment.RedirectURL = "http://nowhere.com.au/MyPage";

            response = rService.InitialisePayment(payment, session.Key);
        }
    }
}
