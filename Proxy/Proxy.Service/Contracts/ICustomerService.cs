﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Reflection;
using System.Security.Permissions;

namespace Proxy.Service.Contracts
{
    [ServiceContract(Namespace = "http://medianet.com.au/services")]
    interface ICustomerService
    {
        [OperationContract]
        [FaultContract(typeof(InternalCustomerService.ServiceFault))]
        [FaultContract(typeof(InternalCustomerService.NonUniqueFault))]
        string AddAccount(InternalCustomerService.NewAccountRequest account);

        //[OperationContract]
        //[FaultContract(typeof(ServiceFault))]
        //void Update(Customer customer);

        [OperationContract]
        [FaultContract(typeof(InternalCustomerService.ServiceFault))]
        [FaultContract(typeof(InternalCustomerService.AccessDeniedFault))]
        InternalCustomerService.Customer GetCustomerByDebtorNumber(string debtorNumber, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(InternalCustomerService.ServiceFault))]
        [FaultContract(typeof(InternalCustomerService.InvalidLogonFault))]
        InternalCustomerService.DBSession Login(string username, string companyLogon, string password, InternalCustomerService.SystemType system);

        [OperationContract]
        [FaultContract(typeof(InternalCustomerService.ServiceFault))]
        [FaultContract(typeof(InternalCustomerService.InvalidLogonFault))]
        InternalCustomerService.DBSession LoginUsingSession(string sessionKey, InternalCustomerService.SystemType system);

        [OperationContract]
        [FaultContract(typeof(InternalCustomerService.ServiceFault))]
        InternalCustomerService.DBSession ValidateSession(string sessionKey);

        [OperationContract]
        [FaultContract(typeof(InternalCustomerService.ServiceFault))]
        void Logout(string sessionKey);

        [OperationContract]
        [FaultContract(typeof(InternalCustomerService.ServiceFault))]
        [FaultContract(typeof(InternalCustomerService.AccessDeniedFault))]
        List<InternalCustomerService.User> GetAllUsersByCustomer(string debtorNumber, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(InternalCustomerService.ServiceFault))]
        InternalCustomerService.User GetUserById(int userId, string sessionKey);

        //[OperationContract]
        //[FaultContract(typeof(InternalCustomerService.ServiceFault))]
        //int AddUser(InternalCustomerService.User user);

        [OperationContract]
        [FaultContract(typeof(InternalCustomerService.ServiceFault))]
        void UpdateUserProfile(InternalCustomerService.UserProfile user);
    }
}
