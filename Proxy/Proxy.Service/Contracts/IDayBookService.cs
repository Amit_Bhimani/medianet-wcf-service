﻿namespace Proxy.Service.Contracts
{
    using System.Collections.Generic;
    using System.ServiceModel;
    using System;

    [ServiceContract(Namespace = "http://medianet.com.au/services")]
    public interface IDayBook
    {
        [OperationContract]
        [FaultContract(typeof(InternalDayBookService.ServiceFault))]
        List<InternalDayBookService.MediaAgenda> GetMediaAgendas(
            int forLastInDays = 0,
            int maxNumberOfItems = 0
        );

        [OperationContract]
        [FaultContract(typeof(InternalDayBookService.ServiceFault))]
        List<InternalDayBookService.TrainingCourse> GetActiveMediaTrainings();

        [OperationContract]
        [FaultContract(typeof(InternalDayBookService.ServiceFault))]
        InternalDayBookService.Career GetCareers();
    }

}
