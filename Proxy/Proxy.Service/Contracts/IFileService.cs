﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Reflection;
using System.Security.Permissions;
using System.IO;

namespace Proxy.Service.Contracts
{
    [ServiceContract(Namespace = "http://medianet.com.au/services")]
    interface IFileService
    {
        [OperationContract]
        [FaultContract(typeof(InternalFileService.ServiceFault))]
        [FaultContract(typeof(InternalFileService.AccessDeniedFault))]
        InternalFileService.TempFileResponse UploadTempFile(InternalFileService.TempFileStream fileStream);

        [OperationContract]
        [FaultContract(typeof(InternalFileService.ServiceFault))]
        [FaultContract(typeof(InternalFileService.AccessDeniedFault))]
        InternalFileService.TempFileResponse UploadTempFileForConversion(InternalFileService.TempFileStream fileStream);

        [OperationContract]
        [FaultContract(typeof(InternalFileService.ServiceFault))]
        [FaultContract(typeof(InternalFileService.AccessDeniedFault))]
        Stream GetTempFileContent(int id, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(InternalFileService.ServiceFault))]
        [FaultContract(typeof(InternalFileService.AccessDeniedFault))]
        Stream GetDocumentContent(int id, string sessionKey);
    }
}
