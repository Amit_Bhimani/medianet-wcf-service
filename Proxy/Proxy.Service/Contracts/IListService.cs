﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Reflection;
using System.Security.Permissions;

namespace Proxy.Service.Contracts
{
    [ServiceContract(Namespace = "http://medianet.com.au/services")]
    interface IListService
    {
        [OperationContract]
        [FaultContract(typeof(InternalListService.ServiceFault))]
        [FaultContract(typeof(InternalListService.AccessDeniedFault))]
        InternalListService.ServiceList GetServiceListById(int serviceListId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(InternalListService.ServiceFault))]
        [FaultContract(typeof(InternalListService.AccessDeniedFault))]
        List<InternalListService.ServiceList> GetServiceListsByDebtorNumber(string debtorNumber, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(InternalListService.ServiceFault))]
        [FaultContract(typeof(InternalListService.AccessDeniedFault))]
        InternalListService.PackageList GetPackageListById(int packageListId, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(InternalListService.ServiceFault))]
        [FaultContract(typeof(InternalListService.AccessDeniedFault))]
        List<InternalListService.PackageList> GetPackageListsByDebtorNumber(string debtorNumber, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(InternalListService.ServiceFault))]
        [FaultContract(typeof(InternalListService.AccessDeniedFault))]
        List<InternalListService.ListCategory> GetListCategories(string sessionKey);

        [OperationContract]
        [FaultContract(typeof(InternalListService.ServiceFault))]
        InternalListService.ListComment GetServiceCommentById(int id, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(InternalListService.ServiceFault))]
        InternalListService.ListComment GetPackageCommentById(int id, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(InternalListService.ServiceFault))]
        [FaultContract(typeof(InternalListService.AccessDeniedFault))]
        [FaultContract(typeof(InternalListService.KeyNotFoundFault))]
        List<string> GetServiceRecipientOutletsById(int id, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(InternalListService.ServiceFault))]
        [FaultContract(typeof(InternalListService.AccessDeniedFault))]
        [FaultContract(typeof(InternalListService.KeyNotFoundFault))]
        List<string> GetPackageRecipientOutletsById(int id, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(InternalListService.ServiceFault))]
        [FaultContract(typeof(InternalListService.AccessDeniedFault))]
        [FaultContract(typeof(InternalListService.InUseFault))]
        int AddServiceRecipient(int serviceId, InternalListService.Recipient recipient, string sessionKey);
    }
}
