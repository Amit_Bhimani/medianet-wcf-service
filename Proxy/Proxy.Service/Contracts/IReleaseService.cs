﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Reflection;
using System.Security.Permissions;

namespace Proxy.Service.Contracts
{
    [ServiceContract(Namespace = "http://medianet.com.au/services")]
    interface IReleaseService
    {
        [OperationContract]
        [FaultContract(typeof(InternalListService.ServiceFault))]
        int AddReleaseFromSummary(InternalReleaseService.ReleaseSummary release, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(InternalListService.ServiceFault))]
        InternalReleaseService.Release GetReleaseById(int releaseId, string sessionKey);

        //[OperationContract]
        //[FaultContract(typeof(ServiceFault))]
        //[PrincipalPermission(SecurityAction.Demand, Role = "Administrators")]
        //List<ReleaseQueue> GetQueue(bool showProcessed, bool showHoldIndefinately);

        [OperationContract]
        [FaultContract(typeof(InternalListService.ServiceFault))]
        InternalReleaseService.ReleaseQuoteResponse QuoteRelease(InternalReleaseService.ReleaseQuoteRequest release, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(InternalListService.ServiceFault))]
        InternalReleaseService.PaymentStartResponse InitialisePayment(InternalReleaseService.PaymentStartRequest payment, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(InternalReleaseService.ServiceFault))]
        InternalReleaseService.PaymentEndResponse FinalisePayment(string paymentAccessCode, string sessionKey);

        [OperationContract]
        [FaultContract(typeof(InternalReleaseService.ServiceFault))]
        int CountMailmergeRecipients(int tempFileId, InternalReleaseService.DistributionType distType);

        [OperationContract]
        [FaultContract(typeof(InternalReleaseService.ServiceFault))]
        List<InternalReleaseService.WebCategory> GetWebCategories(string sessionKey);
    }
}
