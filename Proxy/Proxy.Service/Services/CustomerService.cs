﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Proxy.Service.Contracts;

namespace Proxy.Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class CustomerService : ICustomerService
    {
        /// <summary>
        /// Adds a new Customer to the database.
        /// </summary>
        /// <param name="customer">A Customer object containing all the Customer details.</param>
        /// <returns>The DebtorNumber generated for this customer.</returns>
        public string AddAccount(InternalCustomerService.NewAccountRequest account) {
            using (var svc = new InternalCustomerService.CustomerServiceClient()) {
                return svc.AddAccount(account);
            }
        }

        //public void Update(Customer customer) {
        //    throw new NotImplementedException();
        //}

        /// <summary>
        /// Gets the details of a Custoemr.
        /// </summary>
        /// <param name="debtorNumber">The DebtorNumber of the Customer to get.</param>
        /// <param name="sessionKey">The Session Key of the User making the request.</param>
        /// <returns>The details of the Customer.</returns>
        public InternalCustomerService.Customer GetCustomerByDebtorNumber(string debtorNumber, string sessionKey) {
            using (var svc = new InternalCustomerService.CustomerServiceClient()) {
                return svc.GetCustomerByDebtorNumber(debtorNumber, sessionKey);
            }
        }

        /// <summary>
        /// Logs a user in to the specified System.
        /// </summary>
        /// <param name="username">The username of the User.</param>
        /// <param name="companyLogonsystem being logged in to.">The logon name of the Company the User belongs to.</param>
        /// <param name="passwordthe ">The password of the User.</param>
        /// <param name="system">The system being logged in to.</param>
        /// <returns>A DBSession object containing the User and Customer details.</returns>
        public InternalCustomerService.DBSession Login(string username, string companyLogon, string password, InternalCustomerService.SystemType system) {
            using (var svc = new InternalCustomerService.CustomerServiceClient()) {
                return svc.Login(username, companyLogon, password, system);
            }
        }

        /// <summary>
        /// Logs a user in to the specified System using a session created after logging into another system.
        /// </summary>
        /// <param name="sessionKey">The Session Key to use to login to another System.</param>
        /// <param name="system">The system being logged in to.</param>
        /// <returns>A DBSession object containing the User and Customer details.</returns>
        public InternalCustomerService.DBSession LoginUsingSession(string sessionKey, InternalCustomerService.SystemType system) {
            using (var svc = new InternalCustomerService.CustomerServiceClient()) {
                return svc.LoginUsingSession(sessionKey, system);
            }
        }

        /// <summary>
        /// Validates the DBSession entry and sets the LastAccessedDate to the current time.
        /// </summary>
        /// <param name="sessionKey">The Key of the DBSession to validate.</param>
        /// <returns>The DBSession object containing the User and Customer details.</returns>
        public InternalCustomerService.DBSession ValidateSession(string sessionKey) {
            using (var svc = new InternalCustomerService.CustomerServiceClient()) {
                return svc.ValidateSession(sessionKey);
            }
        }

        /// <summary>
        /// Log the user out by setting the DBSession to inactive.
        /// </summary>
        /// <param name="sessionKey">The Key of the DBSession to set to inactive.</param>
        public void Logout(string sessionKey) {
            using (var svc = new InternalCustomerService.CustomerServiceClient()) {
                svc.Logout(sessionKey);
            }
        }

        /// <summary>
        /// Gets the details of all Users that belong to a Customer.
        /// </summary>
        /// <param name="debtorNumber">The DebtorNumber of the Customer to filter users by.</param>
        /// <param name="sessionKey">The Session Key of the User making the request.</param>
        /// <returns>The details of all the Users found.</returns>
        public List<InternalCustomerService.User> GetAllUsersByCustomer(string debtorNumber, string sessionKey) {
            using (var svc = new InternalCustomerService.CustomerServiceClient()) {
                return svc.GetAllUsersByCustomer(debtorNumber, sessionKey).ToList();
            }
        }

        /// <summary>
        /// Gets the details of a User.
        /// </summary>
        /// <param name="userId">The Id of the User to get.</param>
        /// <param name="sessionKey">The Session Key of the User making the request.</param>
        /// <returns>The details of the User.</returns>
        public InternalCustomerService.User GetUserById(int userId, string sessionKey) {
            using (var svc = new InternalCustomerService.CustomerServiceClient()) {
                return svc.GetUserById(userId, sessionKey);
            }
        }

        /// <summary>
        /// Adds a new User to the database.
        /// </summary>
        /// <param name="user">The details of the User.</param>
        /// <returns>The Id of the new User.</returns>
        //public int AddUser(InternalCustomerService.User user) {
        //    using (var svc = new InternalCustomerService.CustomerServiceClient()) {
        //        return svc.AddUser(user);
        //    }
        //}

        /// <summary>
        /// Updates the basic profile details of a User.
        /// </summary>
        /// <param name="user">The basic profile details of the User.</param>
        public void UpdateUserProfile(InternalCustomerService.UserProfile user) {
            using (var svc = new InternalCustomerService.CustomerServiceClient()) {
                svc.UpdateUserProfile(user);
            }
        }
    }
}
