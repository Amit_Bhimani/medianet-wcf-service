﻿namespace Proxy.Service
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.ServiceModel;
    using Proxy.Service.Contracts;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class DayBookService : IDayBook
    {
        public InternalDayBookService.Career GetCareers() {
            using (var svc = new InternalDayBookService.DayBookClient()) {
                return svc.GetCareers();
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="forLastInDays">0 for this will return the latest MediaAgenda.</param>
        /// <param name="maxNumberOfItems">defaults to 1.</param>
        /// <returns></returns>
        public List<InternalDayBookService.MediaAgenda> GetMediaAgendas(int forLastInDays = 0, int maxNumberOfItems = 1) {
            using (var svc = new InternalDayBookService.DayBookClient()) {
                return svc.GetMediaAgendas(forLastInDays, maxNumberOfItems).ToList();
            }
        }

        public List<InternalDayBookService.TrainingCourse> GetActiveMediaTrainings() {
            using (var svc = new InternalDayBookService.DayBookClient()) {
                return svc.GetActiveMediaTrainings().ToList();
            }
        }
    }
}
