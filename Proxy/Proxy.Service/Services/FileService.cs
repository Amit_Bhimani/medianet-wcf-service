﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Text.RegularExpressions;
using System.Reflection;
using System.IO;
using Proxy.Service.Contracts;

namespace Proxy.Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class FileService : IFileService
    {
        /// <summary>
        /// Uploads a temporary file to the system.
        /// </summary>
        /// <param name="fileStream">An object containing the file name and stream.</param>
        /// <returns>The TempFileId inside a TempFileResponse object.</returns>
        public InternalFileService.TempFileResponse UploadTempFile(InternalFileService.TempFileStream fileStream) {
            var x = new InternalFileService.TempFileResponse();

            using (var svc = new InternalFileService.FileServiceClient()) {
                x.PageCount = svc.UploadTempFile(fileStream.ContentType, fileStream.FileName, fileStream.SessionKey, fileStream.FileData, out x.TempFileId, out x.WordCount);
            }
            return x;
        }

        /// <summary>
        /// Uploads a temporary file to the system and converts it to all possible formats.
        /// </summary>
        /// <param name="fileStream">An object containing the file name and stream.</param>
        /// <returns>The TempFileId, number of fax pages and wire words.</returns>
        public InternalFileService.TempFileResponse UploadTempFileForConversion(InternalFileService.TempFileStream fileStream) {
            var x = new InternalFileService.TempFileResponse();

            using (var svc = new InternalFileService.FileServiceClient()) {
                x.PageCount = svc.UploadTempFileForConversion(fileStream.ContentType, fileStream.FileName, fileStream.SessionKey, fileStream.FileData, out x.TempFileId, out x.WordCount);
            }
            return x;
        }

        /// <summary>
        /// Gets the content of a TempFile.
        /// </summary>
        /// <param name="id">The Id of the TempFile.</param>
        /// <returns>The stream containing the content of the TempFile.</returns>
        public Stream GetTempFileContent(int id, string sessionKey) {
            using (var svc = new InternalFileService.FileServiceClient()) {
                return svc.GetTempFileContent(id, sessionKey);
            }
        }

        /// <summary>
        /// Gets the content of a Document.
        /// </summary>
        /// <param name="id">The Id of the Document.</param>
        /// <returns>The stream containing the content of the Document.</returns>
        public Stream GetDocumentContent(int id, string sessionKey) {
            using (var svc = new InternalFileService.FileServiceClient()) {
                return svc.GetDocumentContent(id, sessionKey);
            }
        }
    }
}
