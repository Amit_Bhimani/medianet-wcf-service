﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Proxy.Service.Contracts;

namespace Proxy.Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class ListService : IListService
    {
        /// <summary>
        /// Gets a ServiceList by it's Id. Only ServiceLists owned by AAP or the customer are returned.
        /// </summary>
        /// <param name="serviceId">The Id of the ServiceList to get.</param>
        /// <param name="sessionKey">The SessionId of the user requesting the details.</param>
        /// <returns>The details of the ServiceList.</returns>
        public InternalListService.ServiceList GetServiceListById(int serviceId, string sessionKey) {
            using (var svc = new InternalListService.ListServiceClient()) {
                return svc.GetServiceListById(serviceId, sessionKey);
            }
        }

        /// <summary>
        /// Gets a List of ServiceLists by the DebtorNumber.
        /// </summary>
        /// <param name="id">The DebtorNumber of the ServiceLists to get.</param>
        /// <param name="sessionKey">The SessionId of the user requesting the details.</param>
        /// <returns>The ServiceLists owned by the DebtorNumber.</returns>
        public List<InternalListService.ServiceList> GetServiceListsByDebtorNumber(string debtorNumber, string sessionKey) {
            using (var svc = new InternalListService.ListServiceClient()) {
                return svc.GetServiceListsByDebtorNumber(debtorNumber, sessionKey).ToList();
            }
        }

        /// <summary>
        /// Gets a PackageList by it's Id. Only PackageLists owned by AAP or the customer are returned.
        /// </summary>
        /// <param name="packageId">The Id of the PackageList to get.</param>
        /// <param name="sessionKey">The SessionId of the user requesting the details.</param>
        /// <returns>The details of the PackageList.</returns>
        public InternalListService.PackageList GetPackageListById(int packageId, string sessionKey) {
            using (var svc = new InternalListService.ListServiceClient()) {
                return svc.GetPackageListById(packageId, sessionKey);
            }
        }

        /// <summary>
        /// Gets a List of PackageLists by the DebtorNumber.
        /// </summary>
        /// <param name="id">The DebtorNumber of the PackageLists to get.</param>
        /// <param name="sessionKey">The SessionId of the user requesting the details.</param>
        /// <returns>The PackageLists owned by the DebtorNumber.</returns>
        public List<InternalListService.PackageList> GetPackageListsByDebtorNumber(string debtorNumber, string sessionKey) {
            using (var svc = new InternalListService.ListServiceClient()) {
                return svc.GetPackageListsByDebtorNumber(debtorNumber, sessionKey).ToList();
            }
        }

        /// <summary>
        /// Gets all List Categories.
        /// </summary>
        /// <returns>A list of all ListCategory items.</returns>
        public List<InternalListService.ListCategory> GetListCategories(string sessionKey) {
            using (var svc = new InternalListService.ListServiceClient()) {
                return svc.GetListCategories(sessionKey).ToList();
            }
        }

        /// <summary>
        /// Get the Comment for a ServiceList
        /// </summary>
        /// <param name="serviceId">The Id of the ServiceList.</param>
        /// <param name="sessionKey">The SessionId of the user requesting the details.</param>
        /// <returns>The Comment.</returns>
        public InternalListService.ListComment GetServiceCommentById(int serviceId, string sessionKey) {
            using (var svc = new InternalListService.ListServiceClient()) {
                return svc.GetServiceCommentById(serviceId, sessionKey);
            }
        }

        /// <summary>
        /// Get the Comment for a PackageList
        /// </summary>
        /// <param name="packageId">The Id of the PackageList.</param>
        /// <param name="sessionKey">The SessionId of the user requesting the details.</param>
        /// <returns>The Comment.</returns>
        public InternalListService.ListComment GetPackageCommentById(int packageId, string sessionKey) {
            using (var svc = new InternalListService.ListServiceClient()) {
                return svc.GetPackageCommentById(packageId, sessionKey);
            }
        }

        /// <summary>
        /// Get the Outlet names of all recipients for this ServiceList.
        /// </summary>
        /// <param name="serviceId">The Id of the ServiceList.</param>
        /// <param name="sessionKey">The SessionId of the user requesting the details.</param>
        /// <returns>The list of Outlet names.</returns>
        public List<string> GetServiceRecipientOutletsById(int serviceId, string sessionKey) {
            using (var svc = new InternalListService.ListServiceClient()) {
                return svc.GetServiceRecipientOutletsById(serviceId, sessionKey).ToList();
            }
        }

        /// <summary>
        /// Get the Outlet names of all recipients for this PackageList.
        /// </summary>
        /// <param name="packageId">The Id of the PackageList.</param>
        /// <param name="sessionKey">The SessionId of the user requesting the details.</param>
        /// <returns>The list of Outlet names.</returns>
        public List<string> GetPackageRecipientOutletsById(int packageId, string sessionKey) {
            using (var svc = new InternalListService.ListServiceClient()) {
                return svc.GetPackageRecipientOutletsById(packageId, sessionKey).ToList();
            }
        }

        public int AddServiceRecipient(int serviceId, InternalListService.Recipient recipient, string sessionKey) {
            using (var svc = new InternalListService.ListServiceClient()) {
                return svc.AddServiceRecipient(serviceId, recipient, sessionKey);
            }
        }

        //public void AddRecipientsBulk(List<InternalListService.Recipient> recipients, int serviceId, string sessionKey) {
        //}
    }
}
