﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Text.RegularExpressions;
using System.Reflection;
using Proxy.Service.Contracts;

namespace Proxy.Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class ReleaseService : IReleaseService
    {
        /// <summary>
        /// Adds a Release to the database as well as it's transactions and documents.
        /// </summary>
        /// <param name="release">The Release to add to the database.</param>
        /// <returns>The Id of the new Release.</returns>
        public int AddReleaseFromSummary(InternalReleaseService.ReleaseSummary release, string sessionKey) {
            using (var svc = new InternalReleaseService.ReleaseServiceClient()) {
                return svc.AddReleaseFromSummary(release, sessionKey);
            }
        }

        /// <summary>
        /// Gets a Release based on the Id passed.
        /// </summary>
        /// <param name="releaseId">The Id of the release to get.</param>
        /// <returns>The release as well as its Documents and Transactions.</returns>
        public InternalReleaseService.Release GetReleaseById(int releaseId, string sessionKey) {
            using (var svc = new InternalReleaseService.ReleaseServiceClient()) {
                return svc.GetReleaseById(releaseId, sessionKey);
            }
        }

        //public List<ReleaseQueue> GetQueue(bool showProcessed, bool showHoldIndefinately) {
        //    throw new NotImplementedException();
        //}

        /// <summary>
        /// Quote the price of a Release that has not yet been created.
        /// </summary>
        /// <param name="release">The details of the Release.</param>
        /// <returns>The details of the quote.</returns>
        public InternalReleaseService.ReleaseQuoteResponse QuoteRelease(InternalReleaseService.ReleaseQuoteRequest release, string sessionKey) {
            using (var svc = new InternalReleaseService.ReleaseServiceClient()) {
                return svc.QuoteRelease(release, sessionKey);
            }
        }

        /// <summary>
        /// Initialise the creditcard payment of a Release.
        /// </summary>
        /// <param name="release">The details of the Release.</param>
        /// <returns>The details of the quote.</returns>
        public InternalReleaseService.PaymentStartResponse InitialisePayment(InternalReleaseService.PaymentStartRequest payment, string sessionKey) {
            using (var svc = new InternalReleaseService.ReleaseServiceClient()) {
                return svc.InitialisePayment(payment, sessionKey);
            }
        }

        /// <summary>
        /// Finalise the creditcard payment of a Release.
        /// </summary>
        /// <param name="release">The details of the Release.</param>
        /// <returns>The details of the quote.</returns>
        public InternalReleaseService.PaymentEndResponse FinalisePayment(string paymentAccessCode, string sessionKey) {
            using (var svc = new InternalReleaseService.ReleaseServiceClient()) {
                return svc.FinalisePayment(paymentAccessCode, sessionKey);
            }
        }

        /// <summary>
        /// Get the number of recipients of a mailmerge file.
        /// </summary>
        /// <param name="tempFileId">The Id of the csv file already uploaded to the server.</param>
        /// <param name="distType">The distribution type.</param>
        /// <returns>The number of recipients in the csv file.</returns>
        public int CountMailmergeRecipients(int tempFileId, InternalReleaseService.DistributionType distType) {
            using (var svc = new InternalReleaseService.ReleaseServiceClient()) {
                return svc.CountMailmergeRecipients(tempFileId, distType);
            }
        }

        /// <summary>
        /// Gets all Web Categories.
        /// </summary>
        /// <returns>A list of all WebCategory items.</returns>
        public List<InternalReleaseService.WebCategory> GetWebCategories(string sessionKey) {
            using (var svc = new InternalReleaseService.ReleaseServiceClient()) {
                return svc.GetWebCategories(sessionKey).ToList();
            }
        }

        //public List<InternalReleaseService.Release> GetReleaseReport(int startPos, int pageSize, string sessionKey) {
        //    using (var svc = new InternalReleaseService.ReleaseServiceClient()) {
        //        return svc.GetReleaseReport(startPos, pageSize, sessionKey).ToList();
        //    }
        //}
    }
}
