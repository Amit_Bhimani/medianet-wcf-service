﻿using System;
using System.ServiceModel;
using System.ServiceProcess;
using System.Threading;
using System.ServiceModel.Routing;
using Ninject.Extensions.Wcf;
using NLog;
//using Proxy.Service;

namespace Medianet.MedianetServiceHost
{
    public partial class MedianetService : ServiceBase
    {
        private ServiceHost _routerServiceHost;
        private readonly Logger _logger;

        public MedianetService() {
            InitializeComponent();

            _logger = LogManager.GetCurrentClassLogger();
        }
        
        protected override void OnStart(string[] args) {
            try {
                // Start the WCF host.
                _logger.Info("Starting service host.");

                _routerServiceHost = new ServiceHost(typeof(RoutingService));
                _routerServiceHost.Open();
                WriteLogEntriesForService(_routerServiceHost);

                _logger.Info("Service host started.");
            }
            catch (Exception ex) {
                _logger.ErrorException("Failed to start the service.", ex);
                Stop();
            }
        }

        protected override void OnStop() {
            try {
                _logger.Info("Stopping service host.");

                _routerServiceHost.Close();

                _logger.Info("Service host stopped.");
            }
            catch (Exception ex) {
                _logger.ErrorException("Failed to stop the service normally.", ex);
                Stop();
            }
        }

        private void WriteLogEntriesForService(ServiceHost servHost) {
            _logger.Info(string.Format("Endpoints {0}", servHost.Description.Endpoints.Count));

            foreach (System.ServiceModel.Description.ServiceEndpoint endpoint in servHost.Description.Endpoints) {
                _logger.Info(string.Format("  Endpoint: {0}", endpoint.Address));
            }
        }

    }
}
