﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using System.Diagnostics;
using NLog;


namespace Medianet.MedianetServiceHost
{
    [RunInstaller(true)]
    public partial class MedianetServiceInstaller : System.Configuration.Install.Installer
    {
        private readonly ServiceInstaller _serviceInstaller;
        private readonly ServiceProcessInstaller _processInstaller;

        private readonly string _serviceName;
        private readonly string _serviceDescription;

        private static Logger _logger;

        public MedianetServiceInstaller(string serviceName, string serviceDescription) {
            _logger = LogManager.GetCurrentClassLogger();

            _serviceDescription = serviceDescription;
            _serviceName = serviceName;
            InitializeComponent();

            try {
                _processInstaller = new ServiceProcessInstaller();
                _serviceInstaller = new ServiceInstaller();

                // Service will run under system account
                _processInstaller.Account = ServiceAccount.LocalSystem;

                // Service will have Start Type of Manual
                _serviceInstaller.StartType = ServiceStartMode.Manual;

                _serviceInstaller.ServiceName = serviceName;
                _serviceInstaller.Description = serviceDescription;

                Installers.Add(_serviceInstaller);
                Installers.Add(_processInstaller);
            }
            catch { }
        }

        public bool IsInstalled() {
            using (var controller = new ServiceController(_serviceName)) {
                try {
                    var status = controller.Status;
                }
                catch {
                    return false;
                }
            }
            return true;
        }

        public bool IsRunning() {
            using (var controller = new ServiceController(_serviceName)) {
                if (!IsInstalled())
                    return false;
                return controller.Status == ServiceControllerStatus.Running;
            }
        }

        public MedianetServiceInstaller GetInstaller() {
            var installer = new MedianetServiceInstaller(_serviceName, _serviceDescription);
            return installer;
        }

        public void UninstallService() {
            if (!IsInstalled())
                return;

            try {
                var ti = new TransactedInstaller();
                MedianetServiceInstaller installer = GetInstaller();
                ti.Installers.Add(installer);

                var ctx = new InstallContext();
                ctx.Parameters.Add("assemblypath", System.Reflection.Assembly.GetExecutingAssembly().Location);
                ti.Context = ctx;

                var ht = new Hashtable();
                ti.Uninstall(ht);                
            }
            catch (Exception ex) {
                _logger.ErrorException("Error caught while trying to uninstall the service.", ex);
            }
        }

        public void StopService() {
            if (IsInstalled())
                return;

            using (var controller = new ServiceController(_serviceName)) {
                try {
                    if (controller.Status != ServiceControllerStatus.Stopped) {
                        controller.Stop();
                        controller.WaitForStatus(ServiceControllerStatus.Stopped, TimeSpan.FromSeconds(10));
                    }
                }
                catch (Exception e) {
                    _logger.ErrorException("Error caught while trying to stop the service.", e);
                }
            }
        }

        public void InstallService() {
            if (IsInstalled())
                return;

            try {
                var ti = new TransactedInstaller();
                var installer = GetInstaller();
                ti.Installers.Add(installer);

                var ctx = new InstallContext();
                ctx.Parameters.Add("assemblypath", System.Reflection.Assembly.GetExecutingAssembly().Location);
                ti.Context = ctx;

                var ht = new Hashtable();
                ti.Install(ht);
            }
            catch(Exception ex) {
                _logger.ErrorException("Error caught while trying to install the service.", ex);
            }
        }
    }
}
