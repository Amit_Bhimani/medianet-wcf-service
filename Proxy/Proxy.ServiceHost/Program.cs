﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using Ninject;
using System.ServiceProcess;
using System.Configuration;
using System.Net.Mail;

namespace Medianet.MedianetServiceHost
{
    class Program
    {
        private static Logger _logger;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args) {
            _logger = LogManager.GetCurrentClassLogger();
            _logger.Debug("Started main method.");

            // At least log unhandled exceptions.
            var currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += UnhandledExceptionHandler;

            var kernel = new StandardKernel(new MedianetServiceModule());

            try {
                if (args.Length == 1) {
                    var installer = kernel.Get<MedianetServiceInstaller>();

                    switch (args[0].ToLower()) {
                        case "-install":
                            installer.InstallService();
                            break;
                        case "-uninstall":
                            installer.StopService();
                            installer.UninstallService();
                            break;
                    }
                }
                else {
                    // Start the service.
                    ServiceBase.Run(kernel.Get<MedianetService>());
                }
            }
            catch (Exception ex) {
                _logger.ErrorException("Exception was raised.", ex);
            }

            _logger.Debug("Finished main method.");
        }

        /// <summary>
        /// Method to handle unhandled exceptions.
        /// </summary>
        static void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs args) {
            if (args != null && args.ExceptionObject != null) {
                var e = args.ExceptionObject as Exception;
                _logger.ErrorException("An unhandled exception was raised at the top level.", e);
            }
        }
    }
}
